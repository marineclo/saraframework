//
//  UISegmentedControl+Extension.swift
//  SARA Croisiere
//
//  Created by Marine on 11.05.20.
//  Copyright © 2020 GraniteApps. All rights reserved.
//

import Foundation
import UIKit

extension UISegmentedControl {
    func customize() {
        if #available(iOS 13.0, *) {
//            self.backgroundColor = Utils.primaryColor
            self.selectedSegmentTintColor = Utils.primaryColor
            let selectedAttributes = [
                NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 12),
                NSAttributedString.Key.foregroundColor: UIColor.white
            ]
            setTitleTextAttributes(selectedAttributes, for: .selected)
        } else {
            self.tintColor = Utils.primaryColor
        }
    }
}
