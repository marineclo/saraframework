//
//  Double+Extension.swift
//  SARA Croisiere
//
//  Created by Marine IL on 10.12.19.
//  Copyright © 2019 GraniteApps. All rights reserved.
//

import Foundation

extension Double {
    func toInt() -> Int? {
        if self > Double(Int.min) && self < Double(Int.max) {
            return Int(self)
        } else {
            return nil
        }
    }
    var decimalDegree: Double {
        let stringValue = String(self)
        let index = stringValue.firstIndex(of: ".") ?? stringValue.endIndex
        let beginning = stringValue[..<index]
        let ind = beginning.index(beginning.endIndex, offsetBy: -2)
        let degree = Double(beginning[..<ind]) ?? 0.0
        let minutes = Double(stringValue[ind...]) ?? 0.0
        return degree + (minutes / 60)
    }
}
