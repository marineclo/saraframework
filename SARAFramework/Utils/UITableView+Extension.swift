//
//  UITableView+Extension.swift
//  SARA Croisiere
//
//  Created by Rose on 10/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    public func customize() {
        self.layer.borderColor = Utils.secondaryColor.cgColor
        self.layer.borderWidth = 1
        if #available(iOS 13.0, *) {
            self.backgroundColor = UIColor.systemBackground
        } else {
            // Fallback on earlier versions
            self.backgroundColor = Utils.reverseColor
        }        
        //self.layer.cornerRadius = 10
    }
}
