//
//  TabsMenuViewController.swift
//  TestTabs
//
//  Created by Tristan Derbanne on 28.01.19.
//  Copyright © 2019 Tristan Derbanne. All rights reserved.
//

import UIKit

public protocol TabsMenuViewControllerDelegate: class {
    func tabsMenuViewController(_ controller: TabsMenuViewController, didSelectMenuAtIndex index: Int)
    func tabsMenuViewController(_ controller: TabsMenuViewController, didDeSelectMenuAtIndex index: Int)
}

public class TabsMenuViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    var collectionView: UICollectionView?
    
    // MARK: - Settings
    public weak var delegate: TabsMenuViewControllerDelegate?
    
    var equalMenuItemsSize: Bool = true {
        didSet {
            if isViewLoaded {
                self.collectionView?.collectionViewLayout.invalidateLayout()
            }
        }
    }
    
    var visibleItemsCount: Int? = 5 {
        didSet {
            if isViewLoaded {
                self.updateMaxItemSize()
                self.collectionView?.collectionViewLayout.invalidateLayout()
            }
        }
    }
    
    var menuItems: [String]? {
        didSet {
            if isViewLoaded {
                self.updateMaxItemSize()
                self.collectionView?.reloadData()
                var indexPath: IndexPath? = nil
                if _selectedItemIndex != nil {
                    if menuItems == nil {
                        _selectedItemIndex = nil
                    } else if _selectedItemIndex! >= menuItems!.count {
                        _selectedItemIndex = menuItems!.count - 1
                    }
                    indexPath = IndexPath(item: _selectedItemIndex!, section: 0)
                }
                self.collectionView?.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionView.ScrollPosition.centeredHorizontally)
            }
        }
    }
    
    var accessibilityMenuItems: [String]? {
        didSet {
            guard isViewLoaded else {
                return
            }
            
            // No menu items, we'll discard the accessibility menu items
            guard self.menuItems != nil else {
                return
            }
            
            // No accessibility items: we'll remove all accessibility labels
            if accessibilityMenuItems == nil {
                for cell in collectionView!.visibleCells {
                    (cell as! TabsMenuCollectionViewCell).menuItemLabel.accessibilityLabel = nil
                }
                return
            }
            
            // Safety check: there should be as many accessibility menu items as there are menu items
            guard accessibilityMenuItems!.count == menuItems!.count else {
                return
            }
            
            // Let's update all accessibility labels
            for cell in collectionView!.visibleCells {
                let i = collectionView!.indexPath(for: cell)!.item
                (cell as! TabsMenuCollectionViewCell).menuItemLabel.accessibilityLabel = self.accessibilityMenuItems![i]
            }
        }
    }
    
    private var indexToStatusImage: [Int: UIImage] = [:]
    
    func setStatusImage(_ image: UIImage?, forItemAtIndex index: Int) {
        self.indexToStatusImage[index] = image
        if let cell = collectionView!.cellForItem(at: IndexPath(item: index, section: 0)) as? TabsMenuCollectionViewCell {
            cell.statusImage = image
        }
    }
    
    // Updated each time the menu items are updated.
    // Let us know what's the maixmum width necessary for the longest menu item.
    fileprivate var maxItemSize: CGFloat = 0
    
    fileprivate func updateMaxItemSize() {
        guard collectionView != nil else { return }
        // Update max item size according to all menu items
        maxItemSize = 0
        menuItems?.forEach {
            let size = ($0 as NSString).size(withAttributes: [NSAttributedString.Key.font: self.menuItemFont])
            if size.width > maxItemSize {
                maxItemSize = size.width
            }
        }
        
        // If setting items' width to max item size would allow displaying
        // more items than visibleItemsCount then we should increase the max size.
        if visibleItemsCount != nil && visibleItemsCount! > 0 {
            let availableWidth = self.collectionView!.frame.size.width - collectionViewSectionInsets.left - collectionViewSectionInsets.right
            let size = CGFloat(availableWidth) / CGFloat(visibleItemsCount!)
            if maxItemSize < size {
                maxItemSize = size
            }
        }
        
    }
    
    public var menuBackgroundColor: UIColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) {
        didSet {
            if isViewLoaded {
                self.collectionView?.backgroundColor = menuBackgroundColor
            }
        }
    }
    
    public var menuItemsColor: UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) {
        didSet {
            if isViewLoaded {
                self.collectionView?.reloadData()
            }
        }
    }
    
    var menuItemFont = UIFont(name: "HelveticaNeue", size: 14)! {
        didSet {
            if isViewLoaded {
                self.collectionView?.reloadData()
            }
        }
    }
    
    fileprivate var _selectedItemIndex: Int?
    var selectedItemIndex: Int? {
        get {
            return _selectedItemIndex
        }
        set(newValue) {
            if isViewLoaded {
                var selectedIndexPath: IndexPath? = nil
                _selectedItemIndex = newValue
                // Safety check
                if newValue != nil {
                    guard newValue! >= 0 else {
                        return
                    }
                    guard selectedItemIndex! < (menuItems?.count ?? 0) else {
                        return
                    }
                    selectedIndexPath = IndexPath(item: newValue!, section: 0)
                }
                _selectedItemIndex = newValue!
                self.collectionView?.selectItem(at: selectedIndexPath, animated: true, scrollPosition: .centeredHorizontally)
            }
        }
    }

    // MARK: - Collection view data source & delegate
    fileprivate static let TabsMenuCellIdentifier = "TabMenuCell"
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return (menuItems?.isEmpty ?? true) ? 0 : 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuItems!.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TabsMenuViewController.TabsMenuCellIdentifier, for: indexPath) as! TabsMenuCollectionViewCell
        
        cell.selectionView.backgroundColor = self.menuItemsColor
        cell.menuItemLabel.text = self.menuItems![indexPath.item]
        cell.menuItemLabel.textColor = menuItemsColor
        
        // Status image
        cell.statusImage = self.indexToStatusImage[indexPath.row]
        
        if self.accessibilityMenuItems != nil {
            cell.menuItemLabel.accessibilityLabel = accessibilityMenuItems![indexPath.row]
        }
        
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        self._selectedItemIndex = indexPath.row
        delegate?.tabsMenuViewController(self, didSelectMenuAtIndex: indexPath.item)
    }
    
    public func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if _selectedItemIndex == indexPath.item {
            self._selectedItemIndex = nil
        }
        delegate?.tabsMenuViewController(self, didDeSelectMenuAtIndex: indexPath.item)
    }
    
    // MARK: - Collection flow layout delegate
    fileprivate var collectionViewSectionInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = collectionView.frame.size.height
        if equalMenuItemsSize {
            return CGSize(width: maxItemSize, height: height)
        }
        else {
            let size = (self.menuItems![indexPath.item] as NSString).size(withAttributes: [NSAttributedString.Key.font: self.menuItemFont])
            return CGSize(width: size.width, height: height)
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return collectionViewSectionInsets
    }
    
    // MARK: - View controller overrides
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        // Collection View
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        
        self.collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        self.view.addSubview(collectionView!)
        
        self.collectionView!.register(TabsMenuCollectionViewCell.self, forCellWithReuseIdentifier: TabsMenuViewController.TabsMenuCellIdentifier)
        self.collectionView!.delegate = self
        self.collectionView!.dataSource = self
        self.collectionView!.allowsMultipleSelection = false
        self.collectionView!.backgroundColor = Utils.primaryColor
        self.collectionView!.alwaysBounceVertical = false
        self.collectionView!.showsHorizontalScrollIndicator = false
        self.updateMaxItemSize()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        if let i = self._selectedItemIndex {
            self.collectionView?.selectItem(at: IndexPath(item: i, section: 0), animated: true, scrollPosition: .centeredHorizontally)
        }
    }
    
    public override func viewWillLayoutSubviews() {
        self.collectionView?.frame = self.view.frame
        self.collectionView?.collectionViewLayout.invalidateLayout()
    }

}

// MARK: - Tab menu collection view cell
class TabsMenuCollectionViewCell: UICollectionViewCell {
    var menuItemLabel: UILabel!
    var selectionView: UIView!
    private var statusImageView: UIImageView?
    
    private static let selectionBarHeight: CGFloat = 5
    private static let statusImageViewSize: CGFloat = 10
    
    var statusImage: UIImage? {
        didSet {
            if statusImage == nil {
                statusImageView?.removeFromSuperview()
                statusImageView = nil
            }
            else {
                if statusImageView == nil {
                    statusImageView = UIImageView()
                    statusImageView!.contentMode = .scaleAspectFit
                    self.contentView.addSubview(statusImageView!)
                }
                statusImageView!.image = statusImage
                self.setNeedsLayout()
            }
        }
    }
    
    fileprivate func setupCell() {
        self.menuItemLabel = UILabel()
        menuItemLabel!.textAlignment = .center
        self.selectionView = UIView()
        self.isSelected = false
        self.contentView.addSubview(menuItemLabel)
        self.contentView.addSubview(selectionView)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupCell()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupCell()
    }
    
    override func layoutSubviews() {
        self.selectionView.frame = CGRect(x: 0, y: self.frame.size.height - TabsMenuCollectionViewCell.selectionBarHeight, width: self.frame.size.width, height: TabsMenuCollectionViewCell.selectionBarHeight)

        self.menuItemLabel.frame = CGRect(x: 0, y: TabsMenuCollectionViewCell.statusImageViewSize, width: self.frame.size.width, height: self.frame.size.height - 5 - TabsMenuCollectionViewCell.selectionBarHeight - TabsMenuCollectionViewCell.statusImageViewSize)
        
        self.statusImageView?.frame = CGRect(x: 0.5 * (self.frame.size.width - TabsMenuCollectionViewCell.statusImageViewSize), y: 0, width: TabsMenuCollectionViewCell.statusImageViewSize, height: TabsMenuCollectionViewCell.statusImageViewSize)
    }
    
    override func prepareForReuse() {
        self.menuItemLabel.text = ""
        self.isSelected = false
        self.statusImage = nil
    }
    
    override var isSelected: Bool {
        didSet {
            self.selectionView.isHidden = !isSelected
        }
    }
    
}
