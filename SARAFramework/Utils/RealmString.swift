//
//  RealmString.swift
//  SARA Croisiere
//
//  Created by Rose on 07/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import RealmSwift

//TODO: - Vérifier si ce fichier peut être supprimé
/// RealmString class provides the possibility to save String object in Realm list.
final public class RealmString: Object {
    /// Value of the string
    @objc dynamic public var value = ""
}
