//
//  UIResponder+Extension.swift
//  SARAFramework
//
//  Created by Marine on 23.07.21.
//

import Foundation

extension UIResponder {
    // to look for a specific parent in the view hierarchy
    func nextFirstResponder(where condition: (UIResponder) -> Bool ) -> UIResponder? {
        guard let next = next else { return nil }
        if condition(next) { return next }
        else { return next.nextFirstResponder(where: condition) }
    }
}
