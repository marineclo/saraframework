//
//  UIImage+Extension.swift
//  SARA Croisiere
//
//  Created by Marine on 24.04.20.
//  Copyright © 2020 GraniteApps. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {

   func maskWithColor(color: UIColor) -> UIImage? {
        let maskImage = cgImage!

        let width = size.width
        let height = size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)

        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!

        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(color.cgColor)
        context.fill(bounds)

        if let cgImage = context.makeImage() {
            let coloredImage = UIImage(cgImage: cgImage)
            return coloredImage
        } else {
            return nil
        }
    }
    
    func resize(width: Int = 32, height: Int = 32) -> UIImage? {
        let pinImage = self
        let size = CGSize(width: width, height: height)
        UIGraphicsBeginImageContext(size)
        pinImage.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    public class func bundledImage(named: String) -> UIImage? {
        let image = UIImage(named: named)
        if image == nil {
            print("====== \(UIImage.classForCoder())")
            return UIImage(named: named, in: Bundle(for: UIImage.classForCoder()), compatibleWith: nil)
        }
        return image
    }
}
