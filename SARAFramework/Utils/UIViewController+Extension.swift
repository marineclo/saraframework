//
//  UIViewController+Extension.swift
//  SARAFramework
//
//  Created by Marine on 20.07.22.
//

import Foundation
import UIKit
import FirebaseAuth

extension UIViewController{
    func handleError(_ error: Error) {
        let bundle = NavigationManager.shared.bundle
        let err = error as NSError
        let errorCode = AuthErrorCode(_nsError: err)  // Error for Firebase Auth
//            print(errorCode.errorMessage)
            let alert = Utils.showAlert(with: NSLocalizedString("error", bundle: bundle, comment: ""), message: NSLocalizedString(errorCode.errorMessage, bundle: bundle, comment: ""))
            
            self.present(alert, animated: true, completion: nil)

        
    }
}
