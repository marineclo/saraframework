//
//  UIView+Extension.swift
//  SARA Croisiere
//
//  Created by Marine on 19.05.20.
//  Copyright © 2020 GraniteApps. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    public func customizeView(cornerRadius: CGFloat = 5) {
        self.layer.borderWidth = 1
        self.layer.borderColor = Utils.secondaryColor.cgColor
        self.layer.cornerRadius = cornerRadius
    }
    
    /** Loads instance from nib with the same name. */
    func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
}
