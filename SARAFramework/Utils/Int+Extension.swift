//
//  Int+Extension.swift
//  SARA Croisiere
//
//  Created by Marine IL on 10.12.19.
//  Copyright © 2019 GraniteApps. All rights reserved.
//

import Foundation

extension Int {
    func meterToMile() -> Int {
        return Int(round(Float(self) / 1_609.344))
    }
    
    func mileToMeter() -> Int {
        return Int(round(Float(self) * 1_609.344))
    }
    
    func degreesToRadians() -> Double {
        return Double(self) * Double.pi / Double(180)
    }
    
    func oppositeWind() -> Int {
        if self >= 180 {
            return abs(180 - self)
        } else {
            return 180 + self
        }
    }
    
    func plus90Wind() -> Int {
        if self + 90 >= 360 {
            return self + 90 - 360
        } else {
            return self + 90
        }
    }
    
    func minus90Wind() -> Int {
        if self - 90 < 0 {
            return self - 90 + 360
        } else {
            return self - 90
        }
    }
    
    /*func kmhToKnots() -> Float {
     return round(Float(self) / 1.852)
     }
     
     func knotsToKmh() -> Float {
     return round(Float(self) * 1.852)
     }
     
     func msToKmh() -> Float {
     return round(Float(self) * 3.6)
     }
     
     func msToKnots() -> Float {
     return round(Float(self) * 1.943_844)
     }*/
}
