//
//  Utils.swift
//  SARA Croisiere
//
//  Created by Rose on 02/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

public protocol LocalizableString {
    func aboutText() -> String
}

public struct Utils {
    
    public static var localizableProtocol: LocalizableString?
    
    public static func showAlert(with title: String, message: String) -> UIAlertController {
        let bundle = NavigationManager.shared.bundle
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: NSLocalizedString("ok", bundle: bundle, comment: ""), style: .default, handler: nil)
        alertController.addAction(action)
        return alertController
    }
    
    public static func showCancelAlert(controllerToDismiss: UIViewController) -> UIAlertController {
        let bundle = NavigationManager.shared.bundle
        let alertController = UIAlertController(title: NSLocalizedString("warning", bundle: bundle , comment: ""), message: NSLocalizedString("warning_cancel_modifications", bundle: bundle, comment: ""), preferredStyle: .alert)
        let yesAction = UIAlertAction(title: NSLocalizedString("yes", bundle: bundle, comment: ""), style: .default) { _ in
            controllerToDismiss.dismiss(animated: true, completion: nil)
        }
        let noAction = UIAlertAction(title: NSLocalizedString("cancel", bundle: bundle, comment: ""), style: .cancel, handler: nil)
        alertController.addAction(noAction)
        alertController.addAction(yesAction)
        return alertController
    }
    
    public static func randomInt(min: Int, max: Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }
    
    // MARK: - Colors
    //#BAEFD5: vert
    //#0C293D: bleu
    //#FF5500: orange
    public static let primaryColor = #colorLiteral(red: 0.04705882353, green: 0.1607843137, blue: 0.2392156863, alpha: 1)
//    static let primaryColor = UIColor(red: 0.016, green: 0.16, blue: 0.25, alpha: 1.0)
//    static let secondaryColor = UIColor(red: 0.72, green: 0.94, blue: 0.83, alpha: 1.0)
    public static var secondaryColor = #colorLiteral(red: 0.7294117647, green: 0.937254902, blue: 0.8352941176, alpha: 1)
    public static let disabledColor = UIColor(red: 0.75, green: 0.75, blue: 0.75, alpha: 1.0)
    static let reverseColor: UIColor = .white
//    static let headerNavivagtionColor = UIColor(red: 0.31, green: 0.40, blue: 0.45, alpha: 1)
    static let headerNavivagtionColor = #colorLiteral(red: 0.3098039216, green: 0.4, blue: 0.4509803922, alpha: 1)
    
    public static var tintIcon: UIColor = {
        if #available(iOS 13, *) {
            return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                if UITraitCollection.userInterfaceStyle == .dark {
                    /// Return the color for Dark Mode
                    return secondaryColor
                } else {
                    /// Return the color for Light Mode
                    return primaryColor
                }
            }
        } else {
            /// Return a fallback color for iOS 12 and lower.
            return primaryColor
        }
    }()
    
    // Map color
    static let mapAnnotationPointColor = #colorLiteral(red: 0.6588235294, green: 0.7607843137, blue: 0.8235294118, alpha: 1)
    static let mapAnnotationMarkColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
    static let courseColor = #colorLiteral(red: 1, green: 0.3333333333, blue: 0, alpha: 1)
    static let traceColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
    static let crossingPointColor = #colorLiteral(red: 0, green: 0.5690457821, blue: 0.5746168494, alpha: 1)
        
    public static func tupleDistance(floatDistance: Float?) -> (Float, NavigationUnit?)?{
        guard floatDistance != nil else {
            return nil
        }
        if floatDistance! < 1000 {
            return (floatDistance!, .meter)
        }
        if Settings.shared.distanceUnit?.unit == .meter {
            let formattedDistance = (floatDistance! / 1000).formatted
            let formattedUnit = NavigationUnit.kiloMeter
            return (formattedDistance, formattedUnit)
        }
        if Settings.shared.distanceUnit?.unit == .mile {
            let formattedDistance = floatDistance!.meterToMile().formatted
            let formattedUnit = NavigationUnit.mile
            return (formattedDistance, formattedUnit)
        }
        return nil
    }
    
    public static func accessiblePageMenuItems(originalMenuItems: [String], selectedItemIndex: Int) -> [String] {
        let bundle = NavigationManager.shared.bundle
        var accessibilityMenuLabels: [String] = []
        for i in 0..<originalMenuItems.count {
            var accessibilityLabel: String = ""
            if i == selectedItemIndex {
                accessibilityLabel += NSLocalizedString("selected", bundle: bundle, comment: "") + " "
            }
            accessibilityLabel += NSLocalizedString("menu", bundle: bundle, comment: "") + " " + originalMenuItems[i] + " " + NSLocalizedString("page", bundle: bundle, comment: "") + " \(i + 1) " + NSLocalizedString("on", bundle: bundle, comment: "") + " \(originalMenuItems.count)"
            accessibilityMenuLabels.append(accessibilityLabel)
        }
        return accessibilityMenuLabels
    }
    
    public static func formattedFloatAnnonce(float: Float, unit: String) -> String {
        let intValue = Int(float)
        let f = Int(round(modf(float).1 * 10))
        return "\(intValue) \(unit) \(abs(f))"
    }
    
    public static var releaseVersionNumber: String? {
        return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    }
    public static var buildVersionNumber: String? {
        return Bundle.main.infoDictionary?["CFBundleVersion"] as? String
    }
    
    public static func equals(location1: CLLocation?, location2: CLLocation?) -> Bool {
        if location1?.coordinate.latitude == location2?.coordinate.latitude && location1?.coordinate.longitude == location2?.coordinate.longitude {
            return true
        }
        return false
    }
    
    public static func bundle(anyClass: AnyClass, nameBundle: String? = nil) -> Bundle {
        let myBundle = Bundle(for: anyClass)

        guard let resourceBundleURL = myBundle.url(
                forResource: nameBundle != nil ? nameBundle : "SARAFrameworkRessources", withExtension: "bundle")
            else { fatalError("SARAFrameworkRessources.bundle not found!") }

        guard let resourceBundle = Bundle(url: resourceBundleURL)
            else { fatalError("Cannot access SARAFrameworkRessources.bundle!") }

        return resourceBundle
    }
    
    public static func localizedString(forKey key: String, app: String) -> String {
        var result = Bundle.main.localizedString(forKey: key, value: nil, table: nil)

        if result == key {
            result = Bundle.main.localizedString(forKey: key, value: nil, table: app)
        }

        return result
    }
    
    public static func degreeToTime(val1: Int) -> Int {
        var val1Time = 0
        if val1 < 16 { //[0, 14]
            return 12
        } else if val1 < 46 { //[16, 45]
            return 1
        } else if val1 < 76 { //[46, 75]
            return 2
        } else if val1 < 106 { //[76, 105]
            return 3
        } else if val1 < 136 { //[106, 135]
            return 4
        } else if val1 < 166 { //[136, 165]
            return 5
        } else if val1 < 196 { //[166, 195]
            return 6
        } else if val1 < 226 { //[196, 225]
            return 7
        } else if val1 < 256 { //[226, 255]
            return 8
        } else if val1 < 286 { //[256, 285]
            return 9
        } else if val1 < 316 { //[286, 315]
            return 10
        } else if val1 < 346 { //[316, 345]
            return 11
        } else {
            return 12 //[345, 360]
        }
    }
    
    public static func coordinatesZone(zone: String?) -> Int {
        let bundle = NavigationManager.shared.bundle
        var r = 1
        // South: the latitude is negative, West: the longitude is negative
        if zone == NSLocalizedString("south_initial", bundle: bundle, comment: "") || zone == NSLocalizedString("west_initial", bundle: bundle, comment: ""){
            r = -1
        }
        return r
    }
    
    public static func getLatitudeZone(r: Bool) -> (String, String) {
        let bundle = NavigationManager.shared.bundle
        let latZone = r ? NSLocalizedString("north", bundle: bundle, comment: "") : NSLocalizedString("south", bundle: bundle, comment: "")
        let latZoneInitial = r ? NSLocalizedString("north_initial", bundle: bundle, comment: "") : NSLocalizedString("south_initial", bundle: bundle, comment: "")
        return (latZone, latZoneInitial)
    }
    
    public static func getLongitudeZone(r: Bool) -> (String, String) {
        let bundle = NavigationManager.shared.bundle
        let longZone = r ? NSLocalizedString("east", bundle: bundle, comment: "") : NSLocalizedString("west", bundle: bundle, comment: "")
        let longZoneInitial = r ? NSLocalizedString("east_initial", bundle: bundle, comment: "") : NSLocalizedString("west_initial", bundle: bundle, comment: "")
        return (longZone, longZoneInitial)
    }
    
    // Check validity of any textfield of the view
    //
    // - Parameters:
    //   - string: New string to be tested
    //   - isDecimal: Should be a decimal number or not
    // - Returns: Validity of the string
    public static func checkValidity(string: String, isDecimal: Bool) -> Bool {
        
        let NB_MAX_BF = 3
        let NB_MAX_BF2 = Settings.shared.coordinatesUnit?.unit == .decimalDegrees ? 3 : 2
        let NB_MAX_AF = Settings.shared.coordinatesUnit?.unit == .decimalDegrees ? 6 : 3
        
        // Set pattern depending on if we're on degrees or minutes
        let pattern = isDecimal ? String(format: "^(-)?([0-9]{1,%d})?(\\.([0-9]{1,%d})?)?$", NB_MAX_BF2, NB_MAX_AF) : String(format: "^([0-9]{1,%d})?$", NB_MAX_BF)
        
        let regex = try! NSRegularExpression(pattern: pattern, options: [])
        
        // Look for matches
        let matches = regex.matches(in: string, options: [], range: NSRange(location: 0, length: string.count))
        
        return matches.count > 0
    }
}
