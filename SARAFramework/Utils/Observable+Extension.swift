//
//  Observable+Extension.swift
//  SARA Croisiere
//
//  Created by Marine on 11.06.20.
//  Copyright © 2020 GraniteApps. All rights reserved.
//

import Foundation
import RxSwift

extension ObservableType {
    
    // Use previous value with observable
    func withPrevious() -> Observable<(Element?, Element)> {
        return scan([], accumulator: { (previous, current) in
            Array(previous + [current]).suffix(2)
        })
            .map({ (arr) -> (previous: Element?, current: Element) in
                (arr.count > 1 ? arr.first : nil, arr.last!)
            })
    }
}
