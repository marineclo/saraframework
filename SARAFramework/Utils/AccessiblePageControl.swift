//
//  AccessiblePageControl.swift
//  SARA Croisiere
//
//  Created by Rose on 05/01/2018.
//  Copyright © 2018 Rose. All rights reserved.
//

import Foundation
import UIKit

public class AccessiblePageControl: UIPageControl {
    
    public override func accessibilityIncrement() {
        self.currentPage += 1
        self.sendActions(for: .valueChanged)
    }
    
    public override func accessibilityDecrement() {
        self.currentPage += 1
        self.sendActions(for: .valueChanged)
    }
}
