//
//  UITextField+Extension.swift
//  SARA Croisiere
//
//  Created by Rose on 11/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    func customize() {
        self.borderStyle = .roundedRect
        self.layer.borderColor = Utils.secondaryColor.cgColor
        self.layer.borderWidth = 2
        self.layer.cornerRadius = 10
        self.textColor = Utils.primaryColor
    }
}
