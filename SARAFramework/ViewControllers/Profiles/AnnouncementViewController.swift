//
//  AnnouncementViewController.swift
//  SARA Croisiere
//
//  Created by Rose on 30/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import RxOptional

public class AnnouncementViewController: UIViewController {

    // MARK: - Variables
    var settingsType: AnnouncementViewModel.SettingsType = .unknown
    let bundle = Utils.bundle(anyClass: AnnouncementViewController.self)
    
    // MARK: - UI outlets
    let stackView = UIStackView()
    let disposeBag = DisposeBag()
    weak var viewModel: AnnouncementViewModel?
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var enableSwitch: UISwitch!
    @IBOutlet fileprivate weak var scrollView: UIScrollView!
    
    // MARK: - View controller overrides
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.bindViewModel()
    }
    
    override public func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.contentSize = CGSize(width: stackView.frame.width, height: stackView.frame.height)
    }
    
    // MARK: - Setting up the UI
    private func bindViewModel() {
        guard self.viewModel != nil else {
            return
        }
        
        switch settingsType {
        case .voice:
            self.setupVoiceSettings()
            break
        case .gps:
            self.setupCompassSettings()
            break
        case .route:
            self.setupRouteSettings()
            break
        case .navigationCenter:
            self.setupNavigationcenterSettings()
            break
        case .telltales:
            self.setupTelltalesSettings()
            break
        case .carto:
            self.setupCartoSettings()
            break
        default:
            return
        }
    }
    
    private func setupUI() {
        title = NSLocalizedString("announcement_profile_title", bundle: bundle, comment: "")
        
        // Create stack view in scrollview
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        scrollView.addSubview(stackView)
        stackView.spacing = 15
        
        // Header
        self.headerLabel.superview?.backgroundColor = Utils.primaryColor
        self.headerLabel.accessibilityTraits = .header
        
        // Switch
        self.enableSwitch.isHidden = true
        self.enableSwitch.layer.borderWidth = 1
        self.enableSwitch.layer.borderColor = Utils.secondaryColor.cgColor
        self.enableSwitch.layer.cornerRadius = 16
        self.enableSwitch.onTintColor = Utils.secondaryColor
        
        // Pin edges to the scrollview
        stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        stackView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
    }
    
    
    // Voice settings
    private func setupVoiceSettings() {
        
        guard let index = self.viewModel?.profile?.profileTypes.firstIndex(where: { $0 is ProfileVarious }) else {
            return
        }
        
        // Voice
        guard let voiceView = Bundle(for: VoiceProfileView.self).loadNibNamed(String(describing: VoiceProfileView.self), owner: nil, options: nil)?.first as? VoiceProfileView else {
            fatalError()
        }
        
        self.headerLabel.text = NSLocalizedString("various", bundle: bundle, comment: "")
        
        let pVoice = self.viewModel?.profile?.profileTypes[index] as! ProfileVarious
        if Language.isManVoiceAvailable() {
            // Man or woman
            if pVoice.voice.value == "ManVoice" {
                voiceView.voiceSegmentedControl.selectedSegmentIndex = 0
            } else {
                voiceView.voiceSegmentedControl.selectedSegmentIndex = 1
            }
            
            voiceView.voiceSegmentedControl.rx.value
                .bind(onNext: {[weak self] (voiceInt) in
                    if voiceInt == 0 {
                        (self?.viewModel!.profile?.profileTypes[index] as! ProfileVarious).voice.accept("ManVoice")
                    } else {
                        (self?.viewModel!.profile?.profileTypes[index] as! ProfileVarious).voice.accept("WomanVoice")
                    }
                })
                .disposed(by: disposeBag)
        } else {
            voiceView.voiceSegmentedControl.selectedSegmentIndex = 1
            voiceView.voiceSegmentedControl.isEnabled = false
        }
        
        
        // Vocal rate slider
        
        // Bind setting variable of the view to the profile value
        // This way, we update the profile direclty each time any setting view is modified
        voiceView.vocalRateSliderView.customize(with: SliderInfos(title: NSLocalizedString("settings_vocalrate_header", bundle: bundle, comment: ""), unit: .percent, value: pVoice.vocalRate.value, minValue: 45, maxValue: 65, step: 1, isContinuous: false))
        voiceView.vocalRateSliderView.settingsValue
            .asObservable()
            .bind(to: pVoice.vocalRate)
            .disposed(by: disposeBag)
        
        // Battery level
        if let announcesParameters = viewModel?.profile?.announcementParameters(profileSettingsType: .batteryLevel) {
            for parameter in announcesParameters {
                guard let view = Bundle(for: ProfileSettingView.self).loadNibNamed(String(describing: ProfileSettingView.self), owner: nil, options: nil)?.first as? ProfileSettingView else {
                    fatalError()
                }
                self.customizeSettingsView(view: view, for: parameter)
                stackView.addArrangedSubview(view)
            }
        }
        
        self.stackView.addArrangedSubview(voiceView)
    }

    // GPS / Compass settings
    private func setupCompassSettings() {
        guard let announcesParameters = viewModel?.profile?.announcementParameters(profileSettingsType: .gps) else {
            return
        }
        
        self.headerLabel.text = NSLocalizedString("gps_title", bundle: bundle, comment: "") + " / " + NSLocalizedString("compass", bundle: bundle, comment: "")
        
        
        for parameter in announcesParameters {
            guard let view = Bundle(for: ProfileSettingView.self).loadNibNamed(String(describing: ProfileSettingView.self), owner: nil, options: nil)?.first as? ProfileSettingView else {
                fatalError()
            }
            self.customizeSettingsView(view: view, for: parameter)
            stackView.addArrangedSubview(view)
        }
    }
    
    // Route settings
    private func setupRouteSettings() {
        guard let announcesParameters = viewModel?.profile?.announcementParameters(profileSettingsType: .route) else {
            return
        }
        if let title = AnnouncementManager.shared.announcementDelegate?.profileRouteTitle() {
            self.headerLabel.text = title
        } else {
            self.headerLabel.text = ""
        }
        
        for parameter in announcesParameters {
            guard let view = Bundle(for: ProfileSettingView.self).loadNibNamed(String(describing: ProfileSettingView.self), owner: nil, options: nil)?.first as? ProfileSettingView else {
                fatalError()
            }
            self.customizeSettingsView(view: view, for: parameter)
            stackView.addArrangedSubview(view)
        }
    }
    
    // Navigation Center
    private func setupNavigationcenterSettings() {
        guard let announcesParameters = viewModel?.profile?.announcementParameters(profileSettingsType: .navigationCenter) else {
            return
        }
        
        self.headerLabel.text = NSLocalizedString("navigation_center_title", bundle: bundle, comment: "")
        
        for parameter in announcesParameters {
            guard let view = Bundle(for: ProfileSettingView.self).loadNibNamed(String(describing: ProfileSettingView.self), owner: nil, options: nil)?.first as? ProfileSettingView else {
                fatalError()
            }
            self.customizeSettingsView(view: view, for: parameter)
            stackView.addArrangedSubview(view)
        }
    }
    
    // Telltales settings
    private func setupTelltalesSettings() {
        guard let announcesParameters = viewModel?.profile?.announcementParameters(profileSettingsType: .telltales) else {
            return
        }
        
        self.headerLabel.text = NSLocalizedString("telltales_title", bundle: bundle, comment: "")
        
        for parameter in announcesParameters {
            guard let view = Bundle(for: ProfileSettingView.self).loadNibNamed(String(describing: ProfileSettingView.self), owner: nil, options: nil)?.first as? ProfileSettingView else {
                fatalError()
            }
            self.customizeSettingsView(view: view, for: parameter)
            stackView.addArrangedSubview(view)
        }
    }
    
    // Carto settings
    private func setupCartoSettings() {
        guard let announcesParameters = viewModel?.profile?.announcementParameters(profileSettingsType: .carto) else {
            return
        }
        
        self.headerLabel.text = NSLocalizedString("beacons", bundle: bundle, comment: "")
        self.enableSwitch.isHidden = false
        
        guard let index = self.viewModel?.profile?.profileTypes.firstIndex(where: { $0 is ProfileCarto }) else {
            return
        }
        guard let pCarto = viewModel?.profile?.profileTypes.first(where: { $0 is ProfileCarto}) as? ProfileCarto else {
            return
        }
        self.enableSwitch.isOn = pCarto.enableBeacons.value
        
        self.enableSwitch.rx.value.subscribe(onNext: { isOn in
            if isOn {
                for parameter in announcesParameters {
                    guard let view = Bundle(for: ProfileSettingView.self).loadNibNamed(String(describing: ProfileSettingView.self), owner: nil, options: nil)?.first as? ProfileSettingView else {
                        fatalError()
                    }
                    self.customizeSettingsView(view: view, for: parameter)
                    self.stackView.addArrangedSubview(view)
                }
            } else {
                self.stackView.subviews.forEach({ $0.removeFromSuperview() })
            }
            (self.viewModel!.profile?.profileTypes[index] as! ProfileCarto).enableBeacons = BehaviorRelay(value: isOn)
        }).disposed(by: self.disposeBag)
    }
    
    private func customizeSettingsView(view: ProfileSettingView, for parameter: AnnounceParameter) {
        view.setView(parameter: parameter)
        view.delegate = self
        // Bind setting variable of the view to the profile value
        // This way, we update the profile direclty each time any setting view is modified
        view.settingVariable
            .asObservable()
            .filterNil()
            .bind(to: parameter.settingVariable)
            .disposed(by: disposeBag)
    }
}

// MARK: - Profile setting delegate implementation

extension AnnouncementViewController: ProfileSettingDelegate {
    func shouldResizeContentView() {
        
        // Resize contentview properly with stack view new height
        scrollView.contentSize = CGSize(width: stackView.frame.width, height: stackView.frame.height)
//        UIView.animate(withDuration: 1, animations: {
            self.view.layoutSubviews()
//        })
    }
}
