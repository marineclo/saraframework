//
//  ProfilesViewController.swift
//  SARA Croisiere
//
//  Created by Rose on 29/10/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

public class ProfilesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet fileprivate weak var tableView: UITableView!
    @IBOutlet private weak var createProfileButton: UIButton!
    
    let bundle = Utils.bundle(anyClass: ProfilesViewController.self)
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        title = NSLocalizedString("menu_announces", bundle: bundle, comment: "")
        createProfileButton.customizePrimary()
        createProfileButton.setTitle(NSLocalizedString("create_profile_title", bundle: bundle, comment: ""), for: .normal)
        createProfileButton.accessibilityLabel = NSLocalizedString("create_profile_title", bundle: bundle, comment: "")
        
        self.tableView.customize()
        
        self.tableView.register(UINib(nibName: "SelectButtonCell", bundle: Bundle(for: ProfilesViewController.classForCoder())), forCellReuseIdentifier: "profileCellIdentifier")
        self.tableView.register(UINib(nibName: "HeaderTableView", bundle: Bundle(for: ProfilesViewController.classForCoder())), forHeaderFooterViewReuseIdentifier: "HeaderTableView")
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(handleModalDismissed), name: NSNotification.Name(rawValue:"modalProfileEditionIsDimissed"), object: nil)
        tableView.reloadData()
    }
    
    @objc func handleModalDismissed() {
      self.tableView?.reloadData()
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let realm = try! Realm()
        return realm.objects(ProfileRealm.self).count
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "profileCellIdentifier") as? SelectButtonCell else {
            return UITableViewCell()
        }
        
        cell.delegate = self
        cell.titleLabel?.font = UIFont.preferredFont(forTextStyle: .body)
        if let currentProfile = Profile.getProfileAtIndex(indexPath) {
            cell.idElementSelected = currentProfile.id
            cell.titleLabel?.text = currentProfile.name
            cell.selectButton?.tintColor = Utils.tintIcon
            cell.selectButton?.setImage(UIImage(named: "circle", in: bundle, compatibleWith: nil), for: .normal)
            cell.selectButton?.setImage(UIImage(named: "globe", in: bundle, compatibleWith: nil), for: .disabled)
            if currentProfile.id == User.shared.profile.id {
                cell.selectButton?.accessibilityLabel = "\(NSLocalizedString("activated_profile", bundle: bundle, comment: "")) \(currentProfile.name)"
                cell.selectButton?.isEnabled = false
            } else {
                cell.selectButton?.isEnabled = true
                cell.selectButton?.accessibilityLabel = "\(NSLocalizedString("activate_profile", bundle: bundle, comment: "")) \(currentProfile.name)"
            }
        }
        cell.accessibilityTraits = .button
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Annonces", bundle: Bundle(for: ProfilesViewController.classForCoder()))
        guard let controller = storyboard.instantiateViewController(withIdentifier: "ProfileEditionViewController") as? ProfileEditionViewController,
            let profile = Profile.getProfileAtIndex(indexPath) else {
            return
        }

        controller.viewModel.profile = profile
        self.present(controller, animated: true, completion: nil)
    }
    
    public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            guard let profileRealm = Profile.getProfileRealmAtIndex(indexPath) else {
                return
            }
            
            // If the profile is selected, not possible to delete it
            if profileRealm.isSelected {
                self.present(Utils.showAlert(with: NSLocalizedString("warning", bundle: bundle, comment: ""), message: NSLocalizedString("delete_current_profile", bundle: bundle, comment: "")), animated: false)
            } else {
                // Delete profile
                profileRealm.delete()
                tableView.deleteRows(at: [indexPath], with: .automatic)
                AnnouncementManager.shared.resetCurrentState()
            }
        }
    }
        
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = self.tableView?.dequeueReusableHeaderFooterView(withIdentifier: "HeaderTableView" ) as! HeaderTableView
        returnedView.customizeHeader(titleName: NSLocalizedString("profiles_list", bundle: bundle, comment: ""))
        return returnedView
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
        
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "createNewProfile" {
            if let controller = segue.destination as? ProfileEditionViewController {
                controller.viewModel.profile = Settings.shared.defaultProfile()
            }
        }
    }
}

extension ProfilesViewController: SelectButtonCellDelegate {
    public func didSelectCell(id: String) {
        let realm = try! Realm()
        if let realmProfile = realm.object(ofType: ProfileRealm.self, forPrimaryKey: id) {
            User.shared.profile = realmProfile.profile
            
            realm.objects(ProfileRealm.self).forEach({ (profile) in
                
                try! realm.write {
                    profile.isSelected = false
                }
            })
            
            try! realm.write {
                realmProfile.isSelected = true
            }
            print("Navigation Profile")
            AnnouncementManager.shared.resetCurrentState()
            tableView.reloadData()
        }
    }
}
