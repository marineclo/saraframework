//
//  ProfileEditionViewController.swift
//  SARA Croisiere
//
//  Created by Tristan Derbanne on 13.03.19.
//  Copyright © 2019 Rose. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ProfileEditionViewController: UIViewController, TabsMenuViewControllerDelegate, UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    // MARK: - UI outlets and variabkles
    let disposeBag = DisposeBag()
    let viewModel = AnnouncementViewModel()
    let bundle = Utils.bundle(anyClass: ProfileEditionViewController.self)
    
    @IBOutlet weak var titleItem: UINavigationItem!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profileNameTextField: UITextField!
    
    private var tabsMenuController: TabsMenuViewController?
    @IBOutlet weak var tabsMenuContainerView: UIView!
    
    // MARK: - UI Setup and bindings
    private func bindViewModel() {
        self.profileNameTextField.text = self.viewModel.profile?.name
        self.profileNameTextField.accessibilityLabel = self.profileNameTextField.text! + ", " + NSLocalizedString("profile_name", bundle: bundle, comment: "")
        
        
        // Bind textfield to the name of the profile
        self.profileNameTextField.rx
            .text
            .filterNil()
            .distinctUntilChanged().subscribe(onNext: {[weak self] (name) in
                guard let weakSelf = self else { return }
                self?.viewModel.profile?.name = name
                self?.profileNameTextField.accessibilityLabel = name + ", " + NSLocalizedString("profile_name", bundle: weakSelf.bundle, comment: "")
            })
            .disposed(by: disposeBag)
        
        // Disable validate button if no name was given to the profile
        self.profileNameTextField
            .rx.text
            .map { $0?.isNotEmpty ?? false }
            .bind(to: self.doneButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
    }
    
    private func setupUI() {
        cancelButton.title = NSLocalizedString("cancel", bundle: bundle, comment: "")
        doneButton.title = NSLocalizedString("ok", bundle: bundle, comment: "")
        titleItem.title = self.viewModel.profile?.name == "" ? NSLocalizedString("create_profile_title", bundle: bundle, comment: "") : self.viewModel.profile?.name
        nameLabel.text = NSLocalizedString("name", bundle: bundle, comment: "")
        
        // Building profile settings view controllers
        let storyboard = UIStoryboard(name: "Annonces", bundle: Bundle(for: ProfileEditionViewController.classForCoder()))
        var menuItems: [String] = []
        
        // GPS / Compass
        if self.viewModel.profile != nil && self.viewModel.profile!.profileTypes.contains(where: {$0 is ProfileGPS}) {
            if let compassAnnouncementViewController = storyboard.instantiateViewController(withIdentifier: "AnnouncementViewController") as? AnnouncementViewController {
                compassAnnouncementViewController.settingsType = .gps
                compassAnnouncementViewController.viewModel = self.viewModel
                settingsViewControllers.append(compassAnnouncementViewController)
                menuItems.append(NSLocalizedString("gps_title", bundle: bundle, comment: ""))
            }
        }
        
        // Route
        if self.viewModel.profile != nil && self.viewModel.profile!.profileTypes.contains(where: {$0 is ProfileRoute}) {
            if let compassAnnouncementViewController = storyboard.instantiateViewController(withIdentifier: "AnnouncementViewController") as? AnnouncementViewController {
                compassAnnouncementViewController.settingsType = .route
                compassAnnouncementViewController.viewModel = self.viewModel
                settingsViewControllers.append(compassAnnouncementViewController)
                if let title = AnnouncementManager.shared.announcementDelegate?.profileRouteTitle() {
                    menuItems.append(title)
                } else {
                    menuItems.append("")
                }
            }
        }
        
        // Navigation Center
        if self.viewModel.profile != nil && self.viewModel.profile!.profileTypes.contains(where: {$0 is ProfileCentrale}) {
            if let navigationCenterAnnouncementViewController = storyboard.instantiateViewController(withIdentifier: "AnnouncementViewController") as? AnnouncementViewController {
                navigationCenterAnnouncementViewController.settingsType = .navigationCenter
                navigationCenterAnnouncementViewController.viewModel = self.viewModel
                settingsViewControllers.append(navigationCenterAnnouncementViewController)
                menuItems.append(NSLocalizedString("navigation_center_title", bundle: bundle, comment: ""))
            }
        }
        
        // Carto
        if self.viewModel.profile != nil && self.viewModel.profile!.profileTypes.contains(where: {$0 is ProfileCarto}) {
            if let cartoAnnouncementViewController = storyboard.instantiateViewController(withIdentifier: "AnnouncementViewController") as? AnnouncementViewController {
                cartoAnnouncementViewController.settingsType = .carto
                cartoAnnouncementViewController.viewModel = self.viewModel
                settingsViewControllers.append(cartoAnnouncementViewController)
                menuItems.append(NSLocalizedString("menu_carto", bundle: bundle, comment: ""))
            }
        }
        
        // Various
        if self.viewModel.profile != nil && self.viewModel.profile!.profileTypes.contains(where: {$0 is ProfileVarious}) {
            if let voiceAnnouncementViewController = storyboard.instantiateViewController(withIdentifier: "AnnouncementViewController") as? AnnouncementViewController {
                voiceAnnouncementViewController.settingsType = .voice
                voiceAnnouncementViewController.viewModel = self.viewModel
                settingsViewControllers.append(voiceAnnouncementViewController)
                menuItems.append(NSLocalizedString("various", bundle: bundle, comment: ""))
            }
        }
        
        // Telltales
        if self.viewModel.profile != nil && self.viewModel.profile!.profileTypes.contains(where: {$0 is ProfileTelltale}) {
            if let telltalesAnnouncementViewController = storyboard.instantiateViewController(withIdentifier: "AnnouncementViewController") as? AnnouncementViewController {
                telltalesAnnouncementViewController.settingsType = .telltales
                telltalesAnnouncementViewController.viewModel = self.viewModel
                settingsViewControllers.append(telltalesAnnouncementViewController)
                menuItems.append(NSLocalizedString("telltales_title", bundle: bundle, comment: ""))
            }
        }
    
        // Tabs menu controller
        self.tabsMenuController = TabsMenuViewController()
        self.addChild(tabsMenuController!)
        tabsMenuContainerView.addSubview(tabsMenuController!.view)
        self.tabsMenuController!.view.frame = CGRect(x: 0, y: 0, width: self.tabsMenuContainerView.frame.size.width, height: self.tabsMenuContainerView.frame.size.height)
        self.tabsMenuController!.menuItems = menuItems
        self.tabsMenuController!.accessibilityMenuItems = Utils.accessiblePageMenuItems(originalMenuItems: menuItems, selectedItemIndex: 0)
        self.tabsMenuController!.visibleItemsCount = menuItems.count
        self.tabsMenuController!.selectedItemIndex = 0
        self.tabsMenuController!.delegate = self
        if #available(iOS 13.0, *) {
            if self.traitCollection.userInterfaceStyle == .dark {
                self.tabsMenuController!.menuItemsColor = UIColor.label
            } else {
                self.tabsMenuController!.menuItemsColor = Utils.primaryColor
            }
            self.tabsMenuController!.menuBackgroundColor = UIColor.systemBackground
        } else {
            // Fallback on earlier versions
            self.tabsMenuController!.menuBackgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            self.tabsMenuController!.menuItemsColor = Utils.primaryColor
        }
        
        self.settingsPageViewController?.setViewControllers([settingsViewControllers.first!], direction: .forward, animated: false, completion: nil)
        self.settingsPageViewController?.dataSource = self
        self.settingsPageViewController?.delegate = self
    }
    
    // MARK: - Saving or cancelling changes
    /// Before going back to previous controller, check if user is willing to
    /// Cancel its modifications
    
    @IBAction func cancelAndDismiss(_ sender: Any) {
        // Display popup to warn of the lost changes
        let alertController = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: NSLocalizedString("warning_cancel_modifications", bundle: bundle, comment: ""), preferredStyle: .alert)
        let yesAction = UIAlertAction(title: NSLocalizedString("yes", bundle: bundle, comment: ""), style: .default) { [weak self] _ in
            self?.dismiss(animated: true, completion:{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalProfileEditionIsDimissed"), object: nil)
            } )
        }
        let noAction = UIAlertAction(title: NSLocalizedString("cancel", bundle: bundle, comment: ""), style: .cancel, handler: nil)
        alertController.addAction(noAction)
        alertController.addAction(yesAction)
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func validate(_ sender: Any) {
        viewModel.profile?.save()
        navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalProfileEditionIsDimissed"), object: nil)
        })
    }
    
    // MARK: - Page view controller data source & delegate
    private var settingsPageViewController: UIPageViewController?
    private var settingsViewControllers: [UIViewController] = []
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        // Changing accessibility label of previously selected tabs menu
        if let i = self.tabsMenuController?.selectedItemIndex {
            tabsMenuController!.accessibilityMenuItems?[i] = NSLocalizedString("menu", bundle: bundle, comment: "") + " " + tabsMenuController!.menuItems![i] + " " + NSLocalizedString("page", bundle: bundle, comment: "") + " \(i + 1) " + NSLocalizedString("on", bundle: bundle, comment: "") + " \(tabsMenuController!.menuItems!.count)"
        }
        // Selecting corresponding tabs menu and updating accessibility labbel
        if let i = settingsViewControllers.firstIndex(of: pendingViewControllers.first!) {
            if self.tabsMenuController != nil {
                self.tabsMenuController!.selectedItemIndex = i
                self.tabsMenuController!.accessibilityMenuItems?[i] = NSLocalizedString("selected", bundle: bundle, comment: "") + " " + NSLocalizedString("menu", bundle: bundle, comment: "") + " " + tabsMenuController!.menuItems![i] + " " + NSLocalizedString("page", bundle: bundle, comment: "") + " \(i + 1) " + NSLocalizedString("on", bundle: bundle, comment: "") + " \(tabsMenuController!.menuItems!.count)"
            }

        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let i = settingsViewControllers.firstIndex(of: viewController) {
            if i > 0 {
                return settingsViewControllers[i - 1]
            }
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let i = settingsViewControllers.firstIndex(of: viewController) {
            if i < (settingsViewControllers.count - 1) {
                return settingsViewControllers[i + 1]
            }
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if let i = settingsViewControllers.firstIndex(of: pageViewController.viewControllers!.first!) {
            self.tabsMenuController?.selectedItemIndex = i
        }
    }
    
    // MARK: - Tabs menu view controller delegate
    func tabsMenuViewController(_ controller: TabsMenuViewController, didSelectMenuAtIndex index: Int) {
        guard index < settingsViewControllers.count else {
            return
        }
        
        var direction: UIPageViewController.NavigationDirection = .forward
        if let currentVC = settingsPageViewController?.viewControllers?.first {
            if let currentVCIndex = settingsViewControllers.firstIndex(of: currentVC) {
                if currentVCIndex > index {
                    direction = .reverse
                }
            }
        }
        
        // Updating accessibility label
        controller.accessibilityMenuItems?[index] = NSLocalizedString("selected", bundle: bundle, comment: "") + " " + NSLocalizedString("menu", bundle: bundle, comment: "") + " " + controller.menuItems![index] + " " + NSLocalizedString("page", bundle: bundle, comment: "") + " \(index + 1) " + NSLocalizedString("on", bundle: bundle, comment: "") + " \(controller.menuItems!.count)"
        
        self.settingsPageViewController?.setViewControllers([settingsViewControllers[index]], direction: direction, animated: true, completion: nil)
    }
    
    func tabsMenuViewController(_ controller: TabsMenuViewController, didDeSelectMenuAtIndex index: Int) {
        // Updating accessibility label
        controller.accessibilityMenuItems?[index] = NSLocalizedString("menu", bundle: bundle, comment: "") + " " + controller.menuItems![index] + " " + NSLocalizedString("page", bundle: bundle, comment: "") + " \(index + 1) " + NSLocalizedString("on", bundle: bundle, comment: "") + " \(controller.menuItems!.count)"
    }
    
    // MARK: - View controller overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.bindViewModel()
        // Close keyboard when tap on the view
        let tapGesture = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
    }
    
    // MARK; - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ProfileSettingsSegue" {
            self.settingsPageViewController = segue.destination as? UIPageViewController
//            if let pageViewController = segue.destination as? UIPageViewController {
//                self.settingsPageViewController = pageViewController
//            }
        }
        if segue.identifier == "profileSettingsTabsMenuSegue" {
            guard let controller = segue.destination as? TabsMenuViewController else {
                return
            }
            self.tabsMenuController = controller
        }
    }
}
