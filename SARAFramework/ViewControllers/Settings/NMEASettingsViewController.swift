//
//  NMEASettingsViewController.swift
//  SARA Croisiere
//
//  Created by Marine IL on 19.03.19.
//  Copyright © 2019 Rose. All rights reserved.
//

import UIKit
import RxSwift

public class NMEASettingsViewController: UIViewController, UITextFieldDelegate {
    
    // MARK: - UI outlets
    @IBOutlet weak var activatedLabel: UILabel!
    @IBOutlet weak var hostLabel: UILabel!
    @IBOutlet weak var portLabel: UILabel!
    
    @IBOutlet weak var activatedSwitch: UISwitch!
    @IBOutlet weak var hostTxtField: UITextField!
    @IBOutlet weak var portTxtField: UITextField!
    
    @IBOutlet weak var gpsLabel: UILabel!
    @IBOutlet public var gpsSourceSegmentedControl: UISegmentedControl!
    // Network protocol
    @IBOutlet weak var protocolLabel: UILabel!
    @IBOutlet weak var networkProtocolSegmentedControl: UISegmentedControl!
    @IBOutlet weak var trueWindLabel: UILabel!
    @IBOutlet weak var trueWindSegmentedControl: UISegmentedControl!
    
    @IBOutlet weak var wifiStatusImageView: UIImageView!
    @IBOutlet weak var wifiSSIDLabel: UILabel!
    @IBOutlet weak var gotoWifiSettingsButton: UIButton!
    
    @IBOutlet weak var loggingButton: UIButton!
    
    @IBOutlet weak var connexionStatusLabel: UILabel!
    @IBOutlet weak var connexionMessageLabel: UILabel!
    @IBOutlet weak var saveLogs: UISwitch!
    
    // MARK: - IBActions
    @IBAction func enabledNmea(_ sender: UISwitch) {
        Settings.shared.setNmeaEnabled(value: sender.isOn)
        if sender.isOn {
            NmeaManager.default.connect()
            sender.accessibilityLabel = NSLocalizedString("disable_central", bundle: bundle, comment: "")
        }
        else {
            NmeaManager.default.disconnect()
            sender.accessibilityLabel = NSLocalizedString("enable_central", bundle: bundle, comment: "")
        }
    }
    
    @IBAction func gotoWifiSettings(_ sender: Any) {
        let url = URL(string: "App-Prefs:root=WIFI")!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func networkProtocolChanged(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            Settings.shared.setNmeaTCPEnabled(value: false)
            NmeaManager.default.ipProtocol = .udp
        }
        if sender.selectedSegmentIndex == 1 {
            Settings.shared.setNmeaTCPEnabled(value: true)
            NmeaManager.default.ipProtocol = .tcp
        }
    }
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: NMEASettingsViewController.classForCoder())
    let disposeBag = DisposeBag()
    
    private var ssid: String? {
        didSet {
            self.wifiSSIDLabel.text = ssid
            if ssid != nil {
                self.wifiStatusImageView.image = UIImage(named: "wifi-on", in: bundle, compatibleWith: nil)
                self.gotoWifiSettingsButton.isHidden = true
                self.wifiSSIDLabel.accessibilityLabel = NSLocalizedString("wifi", bundle: bundle, comment: "") + ssid!
            }
            else {
                self.wifiStatusImageView.image = UIImage(named: "wifi-off", in: bundle, compatibleWith: nil)
                self.gotoWifiSettingsButton.isHidden = false
                self.wifiSSIDLabel.accessibilityLabel = NSLocalizedString("wifi_not_connected", bundle: bundle, comment: "")
            }
        }
    }
    
    // MARK: - Views controllers override
    public override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "\(NSLocalizedString("menu_nmea", bundle: bundle, comment: ""))"
        
        activatedLabel.text = NSLocalizedString("activate_nmea", bundle: bundle, comment: "")
        activatedLabel.isAccessibilityElement = false
        hostLabel.text = NSLocalizedString("host_nmea", bundle: bundle, comment: "")
        portLabel.text = NSLocalizedString("port_nmea", bundle: bundle, comment: "")
        
        activatedSwitch.isOn = Settings.shared.nmeaEnabled
        if activatedSwitch.isOn {
            activatedSwitch.accessibilityLabel = NSLocalizedString("disable_central", bundle: bundle, comment: "")
        } else {
            activatedSwitch.accessibilityLabel = NSLocalizedString("enable_central", bundle: bundle, comment: "")
        }
        
        activatedSwitch.tintColor = Utils.secondaryColor
        activatedSwitch.onTintColor = Utils.primaryColor
        
        hostTxtField.text = Settings.shared.nmeaHost
        portTxtField.text = "\(Settings.shared.nmeaPort)"
        
        protocolLabel.text = NSLocalizedString("protocol", bundle: bundle, comment: "")
        networkProtocolSegmentedControl.customize()
        
        loggingButton.setTitle(NSLocalizedString("display_nmea", bundle: bundle, comment: ""), for: .normal)
        loggingButton.customizeSecondary()
        
        if Settings.shared.nmeaTCPEnabled {
            networkProtocolSegmentedControl.selectedSegmentIndex = 1
        }
        
        gpsLabel.text = NSLocalizedString("gps_title", bundle: bundle, comment: "")
        gpsLabel.accessibilityTraits = .header
//        gpsSourceSegmentedControl.tintColor = Utils.primaryColor
        gpsSourceSegmentedControl.setTitle(NSLocalizedString("phone", bundle: bundle, comment: ""), forSegmentAt: 0)
        gpsSourceSegmentedControl.setTitle(NSLocalizedString("navigation_center_title", bundle: bundle, comment: ""), forSegmentAt: 1)
        gpsSourceSegmentedControl.selectedSegmentIndex = Settings.shared.usePhoneGPS ? 0 : 1
        gpsSourceSegmentedControl.customize()
        
        self.gpsSourceSegmentedControl.rx.selectedSegmentIndex.subscribe (onNext: { index in
            Settings.shared.setUsePhoneGPS(value: (index == 0) ? true : false)
            LocationManager.shared.locationVariable.accept(nil)
        }).disposed(by: disposeBag)
        
        self.trueWindLabel.text = NSLocalizedString("true_wind_calculation", bundle: bundle, comment: "")
        self.trueWindLabel.accessibilityTraits = .header
        self.trueWindSegmentedControl.setTitle(NSLocalizedString("speed_through_water_abb", bundle: bundle, comment: ""), forSegmentAt: 0)
        self.trueWindSegmentedControl.setTitle(NSLocalizedString("speed_over_ground_abb", bundle: bundle, comment: ""), forSegmentAt: 1)
        self.trueWindSegmentedControl.selectedSegmentIndex = Settings.shared.useSpeedoForTrueWind ? 0 : 1
        self.trueWindSegmentedControl.customize()
        
        self.trueWindSegmentedControl.rx.selectedSegmentIndex.subscribe (onNext: { index in
            Settings.shared.setUseSpeedoForTrueWind(value: (index == 0) ? true : false)
        }).disposed(by: disposeBag)
        
        self.gotoWifiSettingsButton.accessibilityLabel = NSLocalizedString("wifi_settings", bundle: bundle, comment: "")
        self.gotoWifiSettingsButton.setTitle(NSLocalizedString("wifi_settings", comment: ""), for: .normal)
        
        self.ssid = NmeaManager.default.currentSSID.value

        NmeaManager.default.currentSSID.asObservable().bind(onNext: { [weak self] (newSSID) in
            self?.ssid = newSSID
        }).disposed(by: disposeBag)
                
        connexionStatusLabel.text = NSLocalizedString("connexion_status", bundle: bundle, comment: "")
        
        self.connexionMessageLabel.text = NmeaManager.default.connexionStatus.value ?? NSLocalizedString("disconnected", bundle: bundle, comment: "")
        connexionStatusLabel.accessibilityLabel = NSLocalizedString("connexion_status", bundle: bundle, comment: "") + (connexionMessageLabel.text ?? "")
        NmeaManager.default.connexionStatus.asObservable().bind(onNext: { [weak self] (status) in
            guard let weakself = self else { return }
            self?.connexionMessageLabel.text = status ?? NSLocalizedString("disconnected", bundle: weakself.bundle, comment: "")
            self?.connexionStatusLabel.accessibilityLabel = NSLocalizedString("connexion_status", bundle: weakself.bundle, comment: "") + (self?.connexionMessageLabel.text ?? "")
        }).disposed(by: disposeBag)
        self.connexionMessageLabel.isAccessibilityElement = false
        
        // Close keyboard when tap on the view
        let tapGesture = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
        
        if NmeaManager.default.isLoggingOn {
            saveLogs.isOn = true
        }
        //TODO:  For AppStore version
        saveLogs.isHidden = true
        NmeaManager.default.isLoggingOn = false
    }
    

    // MARK: - Data validation
    func verifyWhileTyping(test: String) -> Bool {
        let pattern_1 = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])[.]){0,3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])?$"
        let regexText_1 = NSPredicate(format: "SELF MATCHES %@", pattern_1)
        let result_1 = regexText_1.evaluate(with: test)
        return result_1
    }
    
    func isValidIP(s: String) -> Bool {
        let parts = s.components(separatedBy: ".")
        let nums = parts.compactMap { Int($0) }
        return parts.count == 4 && nums.count == 4 && nums.filter { $0 >= 0 && $0 < 256}.count == 4
    }
    
    // MARK: - TextField Delegate
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        var isValid = false
        if let text = textField.text {
            if textField == hostTxtField {
                isValid = isValidIP(s: text)
                if isValid {
                    Settings.shared.setHost(value: text)
//                    let realm = try! Realm()
//                    try! realm.write {
//                        Settings.shared.nmeaHost = text
//                    }
                    NmeaManager.default.host = text
                }
            } else if textField == portTxtField {
                isValid = true
                if let port = UInt16(text) {
                    Settings.shared.setPort(value: Int(port))
//                    let realm = try! Realm()
//                    try! realm.write {
//                        Settings.shared.nmeaPort = Int(port)
//                    }
                    NmeaManager.default.port = port
                }
            }
            if isValid {
                textField.layer.borderWidth = 0
                textField.resignFirstResponder()
            } else {
                textField.layer.borderWidth = 1
                textField.layer.cornerRadius = 4
                textField.layer.masksToBounds = true
                textField.layer.borderColor = UIColor.red.cgColor
            }
        }
    }

    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: textField)
        return true
    }
    
    @IBAction func saveLogsAction(_ sender: UISwitch) {
        if sender.isOn {
            NmeaManager.default.stopLogs()
            NmeaManager.default.startLogs()
        } else {
            NmeaManager.default.stopLogs()
        }
    }
    
}
