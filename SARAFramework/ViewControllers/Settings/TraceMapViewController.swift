//
//  TraceMapViewController.swift
//  SARA Croisiere
//
//  Created by Marine on 24.04.20.
//  Copyright © 2020 GraniteApps. All rights reserved.
//

import UIKit
import MapKit
import RxSwift

class TraceMapViewController: UIViewController, MKMapViewDelegate, UITextFieldDelegate {

    // MARK: - IBOutlets
    @IBOutlet weak var saveButtonItem: UIBarButtonItem!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var deleteButton: UIButton!
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: TraceMapViewController.classForCoder())
    var urlTrace: URL?
    let disposeBag = DisposeBag()
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.saveButtonItem.title = NSLocalizedString("ok", bundle: bundle, comment: "")
        self.nameLabel.text = NSLocalizedString("name", bundle: bundle, comment: "")
        self.mapView.delegate = self
        
        self.deleteButton.setTitle(NSLocalizedString("delete", bundle: bundle, comment: ""), for: .normal)
        self.deleteButton.setTitleColor(#colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1), for: .normal)
        self.deleteButton.accessibilityLabel = NSLocalizedString("delete", bundle: bundle, comment: "")
        self.deleteButton.customizeSecondary()
        
        guard let url = urlTrace else {
            return
        }
        let annotations = TraceManager.shared.readFile(fileURL: url)
        self.nameTextField.text = url.deletingPathExtension().lastPathComponent
        self.nameTextField
            .rx.text
            .map { $0?.isNotEmpty ?? false }
            .bind(to: saveButtonItem.rx.isEnabled )
            .disposed(by: disposeBag)
        
        if let annotationsToAdd = annotations {
            self.mapView.addAnnotations(annotationsToAdd.filter({$0.type == .mark || $0.type == .coursePoint}))
            // Connect all the user Locations using Poly line.
            var userLocations: [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
            for annotation in annotationsToAdd.filter({$0.type == .userLocation}) {
                userLocations.append(annotation.coordinate)
            }
            let polylineUserLocation = MKPolyline(coordinates: userLocations, count: userLocations.count)
            polylineUserLocation.title = "userLocation"
            mapView.addOverlay(polylineUserLocation)
            
            // line between double mark
            let markAnnotation = annotationsToAdd.filter({$0.type == .mark})
            for i in stride(from: 1, to: markAnnotation.count - 1, by: 1) {
                let title1 = markAnnotation[i-1].title!.components(separatedBy: "-")[0]
                let title2 = markAnnotation[i].title!.components(separatedBy: "-")[0]
                if  title1 == title2 { // Same mark
                    var markCoordinates: [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
                    markCoordinates.append(markAnnotation[i-1].coordinate)
                    markCoordinates.append(markAnnotation[i].coordinate)
                    let polylineMark = MKPolyline(coordinates: markCoordinates, count: markCoordinates.count)
                    polylineMark.title = "mark"
                    mapView.addOverlay(polylineMark)
                }
            }
            //Zoom to fit the route
            var annotationsToZoom: [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
            for annotation in annotationsToAdd {
                annotationsToZoom.append(annotation.coordinate)
            }
            let polylineZoom = MKPolyline(coordinates: annotationsToZoom, count: annotationsToZoom.count)
            MapHelper.zoomToPolyLine(map: self.mapView, polyLine: polylineZoom, animated: false)
        }
        
        // Close keyboard when tap on the view
        let tapGesture = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
    }
        
    func mapViewWillStartRenderingMap(_ mapView: MKMapView) {
        self.activityIndicator.startAnimating()
        self.activityIndicator.isHidden = false
    }
    
    func mapViewDidFinishRenderingMap(_ mapView: MKMapView, fullyRendered: Bool) {
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
    }
    
    // MARK: -
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        return MapHelper.annotationView(mapView: mapView, annotation: annotation)
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        return MapHelper.overlayRenderer(overlay: overlay)
    }
    
    // MARK: - IBAction
    @IBAction func deleteTrace(_ sender: Any) {
        guard let url = urlTrace else {
            return
        }
        let alertController = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: NSLocalizedString("warning_delete_track", bundle: bundle, comment: ""), preferredStyle: .alert)
        let yesAction = UIAlertAction(title: NSLocalizedString("yes", bundle: bundle, comment: ""), style: .default) { _ in
            TraceManager.shared.deleteFile(fileURL: url)
            self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: nil)
        }
        let noAction = UIAlertAction(title: NSLocalizedString("cancel", bundle: bundle, comment: ""), style: .cancel, handler: nil)
        alertController.addAction(noAction)
        alertController.addAction(yesAction)
        present(alertController, animated: true, completion: nil)
    }
    
    // Rename filename
    @IBAction func saveName(_ sender: Any) {
        guard let newName = nameTextField.text, let extensionPath = urlTrace?.pathExtension else {
            return
        }
        // If name change, rename it
        if newName != urlTrace?.deletingPathExtension().lastPathComponent {
            var rv = URLResourceValues()
            rv.name = newName + extensionPath
            try? urlTrace?.setResourceValues(rv)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - TextField
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: textField)
        return false
    }
}
