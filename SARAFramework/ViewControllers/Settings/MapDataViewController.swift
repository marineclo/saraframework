//
//  MapDataViewController.swift
//  SARAFramework
//
//  Created by Marine on 31.05.21.
//  Copyright © 2021 GraniteApps. All rights reserved.

import UIKit
import RxSwift
import MapKit

class MapDataViewController: UIViewController, UITableViewDelegate, CoordinatesViewDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var zoneToDownloadLabel: UILabel!
    @IBOutlet weak var zoneToDownloadView: WaypointView!
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var zoneTableView: UITableView!
    @IBOutlet weak var deleteAllButton: UIButton!
    
    @IBOutlet weak var activityIndicatorView: ActivityIndicatorView!
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: MapDataViewController.classForCoder())
    let viewModel = MapDataViewModel()
    let disposeBag = DisposeBag()
    var selectedItem: DownloadedZone?
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("menu_map_data", bundle: bundle, comment: "")
        
        zoneToDownloadLabel.text = NSLocalizedString("zone_to_download", bundle: bundle, comment: "")
        zoneToDownloadLabel.accessibilityLabel = NSLocalizedString("zone_to_download", bundle: bundle, comment: "")
        // Waypoint view
        setupWaypointView()
        // Buttons
        setupButtons()
        // TableView
        bindTableView()
        self.zoneTableView.register(UINib(nibName: "HeaderTableView", bundle: Bundle(for: MapDataViewController.classForCoder())), forHeaderFooterViewReuseIdentifier: "HeaderTableView")
        self.zoneTableView.customize()
        if #available(iOS 15, *) { // To remove gap between header and table view
            self.zoneTableView.sectionHeaderTopPadding = 0
        }
        // ActivityIndicator
        self.activityIndicatorView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(handleNotification(note:)), name: NSNotification.Name(rawValue:"modalSelectOnMapIsDimissed"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleNotification(note:)), name: NSNotification.Name(rawValue:"modalChooseObjectIsDimissed"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleNotification(note:)), name: NSNotification.Name(rawValue:"modalZoneVisualisationIsDimissed"), object: nil)
        //  Scroll keyboard
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
        
    @objc func handleNotification(note: Notification) {
        switch note.name {
        case NSNotification.Name(rawValue:"modalSelectOnMapIsDimissed"):
            let annotation = note.userInfo!["annotation"] as! MKAnnotation
            let gpsPosition = GPSPosition(latitude: Float(annotation.coordinate.latitude), longitude: Float(annotation.coordinate.longitude))
            self.zoneToDownloadView.coordinatesView.fillCoordinates(with: gpsPosition, auto: true)
        case NSNotification.Name(rawValue:"modalChooseObjectIsDimissed"):
            let object = note.userInfo!["object"] as! NSObject
            var gpsPosition: GPSPosition?
            if let beacon = object as? Beacon {
                gpsPosition = beacon.gpsPosition
            } else if let point = object as? PointRealm {
                gpsPosition = point.getCoordinates(method: .middle)
            }
            if gpsPosition != nil  {
                self.zoneToDownloadView.coordinatesView.fillCoordinates(with: gpsPosition!, auto: true)
            }
        case NSNotification.Name(rawValue:"modalZoneVisualisationIsDimissed"):
            if let item = selectedItem, let index = self.viewModel.downloadedZones.value.firstIndex(of: item) {
                self.zoneTableView.deselectRow(at: IndexPath(row: index, section: 0), animated: true)
            }
        default:
            return
        }
    }
    
    func setupWaypointView() {
        // Add new button for existings points
        let pointsListButton = UIButton()
        pointsListButton.setImage(UIImage(named: "list", in: bundle, compatibleWith: nil), for: .normal)
        pointsListButton.accessibilityLabel = NSLocalizedString("use_existing_points", bundle: bundle, comment: "")
        pointsListButton.addTarget(self, action: #selector(selectCoordinatesAction), for: .touchUpInside)
        pointsListButton.tintColor = Utils.tintIcon
        pointsListButton.tag = 0
        self.zoneToDownloadView.coordinatesView.setButtonOtherMethod(button: pointsListButton)
        // Add new button for existings beacons
        let beaconsListButton = UIButton()
        beaconsListButton.setImage(UIImage(named: "beacon", in: bundle, compatibleWith: nil), for: .normal)
        beaconsListButton.accessibilityLabel = NSLocalizedString("use_existing_beacons", bundle: bundle, comment: "")
        beaconsListButton.addTarget(self, action: #selector(selectCoordinatesAction), for: .touchUpInside)
        beaconsListButton.tintColor = Utils.tintIcon
        beaconsListButton.tag = 1
        self.zoneToDownloadView.coordinatesView.setButtonOtherMethod(button: beaconsListButton)
        self.zoneToDownloadView.coordinatesView.delegate = self
        // TODO: handle different unit: km or miles
        self.zoneToDownloadView.sliderView.customize(with: SliderInfos(title: NSLocalizedString("radius", bundle: bundle, comment: ""), unit: Settings.shared.distanceUnit?.unit, value: 10, minValue: 10, maxValue: 100, step: 1, isContinuous: false))
        self.zoneToDownloadView.coordinatesView.observableCoordinates()
            .bind(to: self.downloadButton.rx.isEnabled)
    }
    
    func setupButtons() {
        self.downloadButton.customizeSecondary()
        self.downloadButton.setTitle(NSLocalizedString("download", bundle: bundle, comment: ""), for: .normal)
        self.downloadButton.accessibilityLabel = NSLocalizedString("download", bundle: bundle, comment: "")
        
        self.deleteAllButton.customizeSecondary()
        self.deleteAllButton.setTitle(NSLocalizedString("delete_all", bundle: bundle, comment: ""), for: .normal)
        self.deleteAllButton.accessibilityLabel = NSLocalizedString("delete_all", bundle: bundle, comment: "")
    }
    
    func bindTableView() {
        self.viewModel.downloadedZones.bind(to: zoneTableView.rx.items(cellIdentifier: "zoneCell", cellType: UITableViewCell.self)) { (row, item, cell) in
            guard let zone = item as? DownloadedZone else { return }
            cell.textLabel?.text = zone.name
            cell.tintColor = Utils.tintIcon
            cell.accessibilityLabel = "\(zone.name)"
        }.disposed(by: disposeBag)
        self.viewModel.downloadedZones
            .map({$0.count > 0})
            .bind(onNext: { hasDownloadedZones in
                if hasDownloadedZones {
                    self.deleteAllButton.setTitleColor(#colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1), for: .normal)
                } else {
                    self.deleteAllButton.setTitleColor(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), for: .normal)
                }
                self.deleteAllButton.isEnabled = hasDownloadedZones
                
            }).disposed(by: disposeBag)
        self.zoneTableView.rx
            .itemDeleted
            .subscribe(onNext: { indexPath in
                let alertController = UIAlertController(title: NSLocalizedString("warning", bundle: self.bundle, comment: ""), message: NSLocalizedString("warning_delete_zone", bundle: self.bundle, comment: ""), preferredStyle: .alert)
                let yesAction = UIAlertAction(title: NSLocalizedString("yes", bundle: self.bundle, comment: ""), style: .default) { _ in
                    self.viewModel.deleteZone(at: indexPath)
                }
                let noAction = UIAlertAction(title: NSLocalizedString("cancel", bundle: self.bundle, comment: ""), style: .cancel, handler: nil)
                alertController.addAction(noAction)
                alertController.addAction(yesAction)
                self.present(alertController, animated: true, completion: nil)
            }).disposed(by: disposeBag)
        self.zoneTableView.rx
            .modelSelected(DownloadedZone.self)
            .subscribe(onNext: { item in
                self.selectedItem = item
                let storyboard = UIStoryboard(name: "Settings", bundle: Bundle(for: MapDataViewController.classForCoder()))
                guard let zoneViewController = storyboard.instantiateViewController(withIdentifier: "zoneVisualisationVC") as? ZoneVisualisationViewController else {
                        return
                }
                let sortedBeacons = Array(item.beacons).sorted(by: { (b1, b2) in
                    guard let currentLoc = LocationManager.shared.getCurrentPosition() else {
                        return false
                    }
                    guard let d1 = b1.distance(currentLoc: currentLoc) else {
                        return false
                    }
                    guard let d2 = b2.distance(currentLoc: currentLoc) else {
                        return false
                    }
                    return d1 <= d2
                })
                zoneViewController.beacons = sortedBeacons
                self.present(zoneViewController, animated: false, completion: nil)
            }).disposed(by: disposeBag)
//        self.zoneTableView.rx
//            .itemSelected
//            .subscribe(onNext: { indexPath in
//
//            })
    }
    
    func elements(isEnabled: Bool) {
        zoneToDownloadView.elements(isUserAccessibilityEnabled: isEnabled)
        downloadButton.isAccessibilityElement = isEnabled
        downloadButton.isUserInteractionEnabled = isEnabled
        zoneTableView.isAccessibilityElement = isEnabled
        zoneTableView.isUserInteractionEnabled = isEnabled
        deleteAllButton.isAccessibilityElement = isEnabled
        deleteAllButton.isUserInteractionEnabled = isEnabled
    }
    
    // MARK: - IBActions
    @objc func selectCoordinatesAction(sender: UIButton!) {
        let storyboard = UIStoryboard(name: "Utils", bundle: bundle)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "SelectTableViewController") as? SelectTableViewController else {
                return
        }
        switch sender.tag {
        case 0: // Select existings points
            print("")
            controller.titleNavigationBar = NSLocalizedString("menu_points", bundle: bundle, comment: "")
            controller.titleTableHeader = NSLocalizedString("points_list", bundle: bundle, comment: "")
            controller.objects = PointRealm.getSortedPointsByProximity()
        case 1: // Select existings beacons
            print("")
            controller.titleNavigationBar = NSLocalizedString("beacons", bundle: bundle, comment: "")
            controller.titleTableHeader = NSLocalizedString("beacons_list", bundle: bundle, comment: "")
            controller.objects = Beacon.getSortedBeaconsByProximity()
        default:
            break
        }
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func downloadAction(_ sender: UIButton) {
        // Get coordinates and radius
        guard let coordinates = self.zoneToDownloadView.coordinatesView.getCoordinates() else {
            return
        }
        let radius = self.zoneToDownloadView.sliderView.settingsValue.value
        self.view.alpha = 0.5
        self.activityIndicatorView.isHidden = false
        activityIndicatorView.setText(text: NSLocalizedString("download_progress", bundle: bundle, comment: ""))
        elements(isEnabled: false)
        activityIndicatorView.startAnimating()
        DispatchQueue.global(qos: .background).async {
            // do your job here
            let downloadedZone = self.viewModel.downloadBeacons(coordinates: coordinates, radius: radius)
            DispatchQueue.main.async {
                // update ui here
                self.viewModel.saveDownloadedBeacons(downloadedZone: downloadedZone)
                self.activityIndicatorView.stopAnimating()
                self.elements(isEnabled: true)
                self.activityIndicatorView.isHidden = true
                self.view.alpha = 1
                var message = ""
                if downloadedZone != nil {
                    let numberBeacons = downloadedZone!.beacons.count
                    if numberBeacons > 0 {
                        message = String(format: NSLocalizedString("download_success", bundle: self.bundle, comment: ""), String(numberBeacons))
                        
                    } else {
                        message = NSLocalizedString("no_beacons_in_zone", bundle: self.bundle, comment: "")
                    }
                } else {
                    message = NSLocalizedString("download_fail", bundle: self.bundle, comment: "")
                }
                let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
                let okAction = UIAlertAction(title: NSLocalizedString("ok", bundle: self.bundle, comment: ""), style: .cancel, handler: nil)
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
                
            }
        }
    }
    
    @IBAction func deleteAllAction(_ sender: UIButton) {
        // Warning - delete all downloaded zone in the database
        let alertController = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: NSLocalizedString("warning_delete_all_zones", bundle: bundle, comment: ""), preferredStyle: .alert)
        let yesAction = UIAlertAction(title: NSLocalizedString("yes", bundle: bundle, comment: ""), style: .default) { _ in
            self.viewModel.deleteAll()
        }
        let noAction = UIAlertAction(title: NSLocalizedString("cancel", bundle: bundle, comment: ""), style: .cancel, handler: nil)
        alertController.addAction(noAction)
        alertController.addAction(yesAction)
        present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - UITableViewDelegate
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = self.zoneTableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderTableView" ) as! HeaderTableView
        returnedView.customizeHeader(titleName: NSLocalizedString("downloaded_zones_list", bundle: bundle, comment: ""))
        return returnedView
    }

    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }

    public func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    // MARK: - CoordinatesViewDelegate
    func openSelectOnMap(tagView: Int?) {
        let storyboard = UIStoryboard(name: "Route", bundle: bundle)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "SelectOnMapVC") as? SelectPositionOnMapViewController else {
                return
        }
        if let (latitude, longitude) = self.zoneToDownloadView.coordinatesView.getCoordinates() {
            let annotation = MapAnnotation(latitude: Double(latitude), longitude: Double(longitude), type: .mark, isCurrent: true)
                controller.annotation = annotation
        } else {
            let annotation = MapAnnotation(latitude: Double.nan, longitude: Double.nan, type: .mark, isCurrent: true)
            controller.annotation = annotation
        }
        controller.radius = Double(self.zoneToDownloadView.sliderView.settingsValue.value.mileToMeter())
        self.present(controller, animated: true, completion: nil)
    }
    
    func useCurrentPosition(tagView: Int?, gpsPosition: GPSPosition) {
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when) {
            UIAccessibility.post(notification: UIAccessibility.Notification.announcement, argument: NSLocalizedString("coordinates_used_for_this_point", bundle: self.bundle, comment: ""))
        }
    }
}
