//
//  NmeaLogsViewController.swift
//  SARA Croisiere
//
//  Created by Tristan Derbanne on 20.03.19.
//  Copyright © 2019 Rose. All rights reserved.
//

import UIKit
import MessageUI

protocol NmeaLogger: class {
    func add(_ nmeaMessage: String)
}
class NmeaLogsViewController: UIViewController, NmeaLogger {
    
    // MARK: - UI outlets and models
    @IBOutlet weak var logsTextView: UITextView!
    @IBOutlet weak var logsNavigationItem: UINavigationItem!
    @IBOutlet weak var handleTraceButton: UIBarButtonItem!
    
    let bundle = Utils.bundle(anyClass: NmeaLogsViewController.classForCoder())
    
    private var loggingEnabled: Bool = true
    
    private var nmeaMessages: [String] = []
    
    private func scrollToBottom() {
        if logsTextView.text.count > 0 {
            let location = logsTextView.text.count - 1
            let bottom = NSMakeRange(location, 1)
            logsTextView.scrollRangeToVisible(bottom)
        }
    }
    
    func add(_ nmeaMessage: String) {
        if loggingEnabled {
            nmeaMessages.append(nmeaMessage)
            logsTextView.text += nmeaMessage
            self.scrollToBottom()
        }
    }
    
    // MARK: - Actions
    @IBAction func displayActionMenu(_ sender: Any) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        if loggingEnabled {
            actionSheet.addAction(UIAlertAction(title: NSLocalizedString("pause", bundle: bundle, comment: ""), style: .default, handler: { (_) in
                self.loggingEnabled = false
            }))
        }
        else {
            actionSheet.addAction(UIAlertAction(title: NSLocalizedString("start_logs", bundle: bundle, comment: ""), style: .default, handler: { (_) in
                self.loggingEnabled = true
            }))
        }
        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("send_by_email", bundle: bundle, comment: ""), style: .default, handler: { (_) in
            self.sendLogByEmail()
        }))
        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("clear", bundle: bundle, comment: ""), style: .destructive, handler: { (_) in
            self.nmeaMessages.removeAll()
            self.logsTextView.text = ""
        }))
        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("cancel", bundle: bundle, comment: ""), style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    private func sendLogByEmail() {
        if MFMailComposeViewController.canSendMail() {
            let data = logsTextView.text.data(using: .utf8)
            
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            if data != nil {
                mail.addAttachmentData(data!, mimeType: "application/text", fileName: "nmea.log")
                mail.setSubject("My NMEA log")
                mail.setMessageBody("<p>Here is your NMEA log</p>", isHTML: true)
            }
            else {
                mail.setSubject("Sorry!")
                mail.setMessageBody("but there was an error :(", isHTML: true)
            }
            present(mail, animated: true, completion: nil)
        } else {
            // show failure alert
            let alert = UIAlertController(title: "Error", message: "Mail couldn't be sent", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", bundle: bundle, comment: ""), style: .default, handler: { (_) in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: - View controller overrides
    override func viewDidLoad() {
        super.viewDidLoad()

        logsNavigationItem.title = NSLocalizedString("nmea_trace_title", bundle: bundle, comment: "")
        handleTraceButton.accessibilityLabel = NSLocalizedString("handle_nmea", bundle: bundle, comment: "")
        
        logsTextView.text = ""
        NmeaManager.default.logger = self
    }
    
}


// MARK: - MFMailComposeViewControllerDelegate implementation
extension NmeaLogsViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        if result == MFMailComposeResult.failed {
            let alert = UIAlertController(title: "Error", message: "Failed to send mail: " + (error?.localizedDescription ?? ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", bundle: bundle, comment: ""), style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
