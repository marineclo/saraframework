//
//  ZoneVisualisationViewController.swift
//  SARAFramework
//
//  Created by Marine on 14.09.21.
//

import UIKit
import MapKit
import RealmSwift

class ZoneVisualisationViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, MKMapViewDelegate, UISearchBarDelegate, HeaderTableViewDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var titleNavBar: UINavigationItem!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var viewButton: UIBarButtonItem!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet var searchBarConstraint: NSLayoutConstraint!
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: ZoneVisualisationViewController.classForCoder())
    var beacons: [Beacon] = []
    var filteredBeacons: [Beacon] = []
    var displayMode: Bool = false {
        didSet{
            if displayMode {
                tableView.isHidden = true
                viewButton.image = UIImage(named: "list", in: bundle, compatibleWith: nil)
                viewButton.accessibilityLabel = NSLocalizedString("list", bundle: bundle, comment: "")
                mapView.isHidden = false
            } else {
                tableView.isHidden = false
                mapView.isHidden = true
                viewButton.image = UIImage(named: "map", in: bundle, compatibleWith: nil)
                viewButton.accessibilityLabel = NSLocalizedString("map", bundle: bundle, comment: "")
            }
        }
    }
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.titleNavBar.title = NSLocalizedString("beacons", bundle: bundle, comment: "")
        self.cancelButton.title = NSLocalizedString("back", bundle: bundle, comment: "")
        self.viewButton.tintColor = Utils.primaryColor
        self.viewButton.accessibilityLabel = NSLocalizedString("map", bundle: bundle, comment: "")
        self.viewButton.image = UIImage(named: "map", in: bundle, compatibleWith: nil)
        
        self.tableView.customize()
        self.tableView.register(UINib(nibName: "HeaderTableView", bundle: Bundle(for: ZoneVisualisationViewController.classForCoder())), forHeaderFooterViewReuseIdentifier: "HeaderTableView")
        if #available(iOS 15, *) { // To remove gap between header and table view
            self.tableView.sectionHeaderTopPadding = 0
        }
        
        searchBarConstraint.isActive = false
        searchBar.isHidden = true
        
        filteredBeacons = beacons
        displayMode = false
        
        //Zoom to fit the route
        var annotations: [MapAnnotation] = []
        var annotationsToZoom: [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
        beacons.forEach({
            if let annotation = MapHelper.getAnnotationFromBeacon(beacon: $0) {
                annotations.append(annotation)
                annotationsToZoom.append(annotation.coordinate)
            }
        })
        mapView.addAnnotations(annotations)

        let polylineZoom = MKPolyline(coordinates: annotationsToZoom, count: annotationsToZoom.count)
        MapHelper.zoomToPolyLine(map: mapView, polyLine: polylineZoom, animated: false)
    }
    
    // MARK: - IBActions
    @IBAction func cancelAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: false, completion: {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalZoneVisualisationIsDimissed"), object: nil)
        })
    }
    
    @IBAction func switchViewAction(_ sender: Any) {
        displayMode = !displayMode
    }
    
    // MARK: - UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredBeacons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "beaconCell", for: indexPath) as? UITableViewCell else {
            return UITableViewCell()
        }
        let beacon = filteredBeacons[indexPath.row]
        cell.textLabel?.text = "\(beacon.type.description) \(beacon.name)"
        cell.selectionStyle = .none
        cell.tintColor = Utils.tintIcon
        cell.accessibilityLabel = "\(beacon.type.description) \(beacon.name)"
        return cell
    }
    
    // MARK: - UITableViewDelegate
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderTableView" ) as! HeaderTableView
        returnedView.customizeHeader(titleName: NSLocalizedString("beacons_list", bundle: bundle, comment: ""), image: UIImage(named: "search", in: bundle, compatibleWith: nil), accessibleText: NSLocalizedString("search", bundle: bundle, comment: ""), buttonIsAccessible: true)
        returnedView.delegate = self
        return returnedView
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    //MARK:- MapViewDelegate methods
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        return MapHelper.annotationView(mapView: mapView, annotation: annotation)
    }
    
    // MARK: - HeaderTableViewDelegate
    func clickOnImage() {
        // Display search bar
        searchBarConstraint.isActive = true
        searchBar.isHidden = false
        searchBar.becomeFirstResponder()
    }
    
    // MARK: - UISearchBarDelegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredBeacons = searchText.isEmpty ? beacons : beacons.filter({ (beacon) -> Bool in
            let text = "\(beacon.type.description) \(beacon.name)"
            return text.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        })
        tableView.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        DispatchQueue.main.async {
            if let cancelButton : UIButton = searchBar.value(forKey: "cancelButton") as? UIButton{
                cancelButton.isEnabled = true
                cancelButton.isAccessibilityElement = true
            }
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.resignFirstResponder()
        searchBarConstraint.isActive = false
        searchBar.isHidden = true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
