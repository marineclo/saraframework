//
//  DeveloperModeViewController.swift
//  SARAFramework
//
//  Created by Marine on 23.07.21.
//

import UIKit
import MapKit
import RxSwift

class DeveloperModeViewController: UIViewController, CoordinatesViewDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var enablePointView: AccessibilitySwitchView!
    @IBOutlet weak var referencePointView: CoordinatesView!
    @IBOutlet weak var applyButton: UIButton!
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: DeveloperModeViewController.classForCoder())
    let disposeBag = DisposeBag()
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("menu_developer", bundle: bundle, comment: "")
        
        self.enablePointView.titleLabel.text = NSLocalizedString("reference_point", bundle: bundle, comment: "")
        self.enablePointView.titleLabel.textAlignment = .left
        self.enablePointView.settingSwitch.isOn = Settings.shared.developerMode
        self.enablePointView.settingSwitch.tintColor = Utils.secondaryColor
        self.enablePointView.settingSwitch.onTintColor = Utils.primaryColor
        
        referencePointView.customizeView()
        referencePointView.delegate = self
        setupReferencePointView()
        
        applyButton.customizeSecondary()
        applyButton.setTitle(NSLocalizedString("apply", bundle: bundle, comment: ""), for: .normal)
        applyButton.accessibilityLabel = NSLocalizedString("apply", bundle: bundle, comment: "")
        
        if Settings.shared.developerMode {
            guard let latitude = LocationManager.shared.getCurrentPosition()?.coordinate.latitude, let longitude = LocationManager.shared.getCurrentPosition()?.coordinate.longitude else { return }
            referencePointView.fillCoordinates(with: GPSPosition(latitude: Float(latitude), longitude: Float(longitude)), auto: true)
        }
        
        Observable.combineLatest(self.enablePointView.settingSwitch.rx.value.asObservable(),
                                 self.referencePointView.observableCoordinates())
            .map({ enablePoint, filledCoordinates -> Bool in
                return enablePoint && filledCoordinates
            }).subscribe(onNext: { enable in
                self.applyButton.isEnabled = enable
                if !enable {
                    Settings.shared.developerMode = enable
                }
            })
            .disposed(by: disposeBag)
        
        applyButton.rx.tap
            .subscribe(onNext: {
                Settings.shared.developerMode = true
                guard let coordinates = self.referencePointView.getCoordinates() else {return}
                AnnouncementManager.shared.resetCurrentState()
                LocationManager.shared.locationVariable.accept(CLLocation(latitude: CLLocationDegrees(coordinates.0), longitude: CLLocationDegrees(coordinates.1)))
            })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(handleNotification(note:)), name: NSNotification.Name(rawValue:"modalSelectOnMapIsDimissed"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleNotification(note:)), name: NSNotification.Name(rawValue:"modalChooseObjectIsDimissed"), object: nil)
    }
    
    @objc func handleNotification(note: Notification) {
        switch note.name {
        case NSNotification.Name(rawValue:"modalSelectOnMapIsDimissed"):
            let annotation = note.userInfo!["annotation"] as! MKAnnotation
            let gpsPosition = GPSPosition(latitude: Float(annotation.coordinate.latitude), longitude: Float(annotation.coordinate.longitude))
            self.referencePointView.fillCoordinates(with: gpsPosition, auto: true)
        case NSNotification.Name(rawValue:"modalChooseObjectIsDimissed"):
            let object = note.userInfo!["object"] as! NSObject
            var gpsPosition: GPSPosition?
            if let beacon = object as? Beacon {
                gpsPosition = beacon.gpsPosition
            } else if let point = object as? PointRealm {
                gpsPosition = point.getCoordinates(method: .middle)
            }
            if gpsPosition != nil  {
                self.referencePointView.fillCoordinates(with: gpsPosition!, auto: true)
            }
        default:
            return
        }
    }
    
    func setupReferencePointView() {
        // Add new button for existings points
        let pointsListButton = UIButton()
        pointsListButton.setImage(UIImage(named: "list", in: bundle, compatibleWith: nil), for: .normal)
        pointsListButton.accessibilityLabel = NSLocalizedString("use_existing_points", bundle: bundle, comment: "")
        pointsListButton.addTarget(self, action: #selector(selectCoordinatesAction), for: .touchUpInside)
        pointsListButton.tintColor = Utils.tintIcon
        pointsListButton.tag = 0
        self.referencePointView.setButtonOtherMethod(button: pointsListButton)
        // Add new button for existings beacons
        let beaconsListButton = UIButton()
        beaconsListButton.setImage(UIImage(named: "beacon", in: bundle, compatibleWith: nil), for: .normal)
        beaconsListButton.accessibilityLabel = NSLocalizedString("use_existing_beacons", bundle: bundle, comment: "")
        beaconsListButton.addTarget(self, action: #selector(selectCoordinatesAction), for: .touchUpInside)
        beaconsListButton.tintColor = Utils.tintIcon
        beaconsListButton.tag = 1
        self.referencePointView.setButtonOtherMethod(button: beaconsListButton)
        self.referencePointView.delegate = self
    }
    
    // MARK: - IBActions
    @objc func selectCoordinatesAction(sender: UIButton!) {
        let storyboard = UIStoryboard(name: "Utils", bundle: bundle)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "SelectTableViewController") as? SelectTableViewController else {
                return
        }
        switch sender.tag {
        case 0: // Select existings points
            controller.titleNavigationBar = NSLocalizedString("menu_points", bundle: bundle, comment: "")
            controller.titleTableHeader = NSLocalizedString("points_list", bundle: bundle, comment: "")
            controller.objects = PointRealm.getSortedPointsByProximity()
        case 1: // Select existings beacons
            controller.titleNavigationBar = NSLocalizedString("beacons", bundle: bundle, comment: "")
            controller.titleTableHeader = NSLocalizedString("beacons_list", bundle: bundle, comment: "")
            controller.objects = Beacon.getSortedBeaconsByProximity()
        default:
            break
        }
        self.present(controller, animated: true, completion: nil)
    }
    
    // MARK: - CoordinatesViewDelegate
    func openSelectOnMap(tagView: Int?) {
        let storyboard = UIStoryboard(name: "Route", bundle: bundle)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "SelectOnMapVC") as? SelectPositionOnMapViewController else {
                return
        }
        if let (latitude, longitude) = self.referencePointView.getCoordinates() {
            let annotation = MapAnnotation(latitude: Double(latitude), longitude: Double(longitude), type: .coursePoint, isCurrent: true)
                controller.annotation = annotation
        } else {
            let annotation = MapAnnotation(latitude: Double.nan, longitude: Double.nan, type: .coursePoint, isCurrent: true)
            controller.annotation = annotation
        }
        self.present(controller, animated: true, completion: nil)
    }
    
    func useCurrentPosition(tagView: Int?, gpsPosition: GPSPosition) {
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when) {
            UIAccessibility.post(notification: UIAccessibility.Notification.announcement, argument: NSLocalizedString("coordinates_used_for_this_point", bundle: self.bundle, comment: ""))
        }
    }
}
