//
//  UserViewController.swift
//  SARA Croisiere
//
//  Created by Rose on 02/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

public class UserViewController: UIViewController, UITextFieldDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet private weak var emailTxtField: UITextField!
    @IBOutlet private weak var passwordTxtField: UITextField!
    
    @IBOutlet private weak var createAccountButton: UIButton!
    @IBOutlet private weak var loginButton: UIButton!
    @IBOutlet weak var resetPasswordButton: UIButton!
    
    @IBOutlet private weak var connectedLabel: UILabel!
    @IBOutlet private weak var logoutButton: UIButton!
    @IBOutlet weak var deleteAccountButton: UIButton!
    
    @IBOutlet private weak var nonConnectedView: UIView!
    @IBOutlet private weak var connectedView: UIView!
    
    //MARK: - Variables
    let bundle = Utils.bundle(anyClass: UserViewController.classForCoder())
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        if let email = User.shared.email.value {
            self.connectedView.isHidden = false
            self.title = NSLocalizedString("my_account", bundle: bundle, comment: "")
            self.connectedLabel.text = String(format: NSLocalizedString("logged_in_with_mail", bundle: bundle, comment: ""), email)
            // For Voice Over to say the dot in email adress
            let attributedString = NSAttributedString(string: String(format: NSLocalizedString("logged_in_with_mail", bundle: bundle, comment: ""), email), attributes: [.accessibilitySpeechPunctuation: true])
            self.connectedLabel.accessibilityAttributedLabel = attributedString
            self.logoutButton.customizeSecondary()
            self.logoutButton.setTitle(NSLocalizedString("log_out", bundle: bundle, comment: ""), for: .normal)
            self.logoutButton.accessibilityLabel = NSLocalizedString("log_out", bundle: bundle, comment: "")
            self.deleteAccountButton.customizeSecondary()
            self.deleteAccountButton.setTitle(NSLocalizedString("delete_account", bundle: bundle, comment: ""), for: .normal)
            self.deleteAccountButton.accessibilityLabel = NSLocalizedString("delete_account", bundle: bundle, comment: "")
        } else {
            self.nonConnectedView.isHidden = false
            self.title = NSLocalizedString("login", bundle: bundle, comment: "")
            self.emailLabel.text = NSLocalizedString("email", bundle: bundle, comment: "")
            self.emailLabel.isAccessibilityElement = false
            self.emailTxtField.accessibilityLabel = NSLocalizedString("email", bundle: bundle, comment: "")
            
            self.passwordLabel.text = NSLocalizedString("password", bundle: bundle, comment: "")
            self.passwordLabel.isAccessibilityElement = false
            self.passwordTxtField.accessibilityLabel = NSLocalizedString("password", bundle: bundle, comment: "")
            Observable.combineLatest(self.emailTxtField.rx.text, self.passwordTxtField.rx.text) { (email, pwd) in
                return email != nil && !email!.isEmpty && email!.isEmail && pwd != nil && !pwd!.isEmpty
            }.bind(to: self.createAccountButton.rx.isEnabled, self.loginButton.rx.isEnabled)
            
            self.emailTxtField.rx.text
                .asObservable()
                .map({$0 != nil && $0!.isNotEmpty && $0!.isEmail})
                .bind(to: self.resetPasswordButton.rx.isEnabled)
            
            self.createAccountButton.customizeSecondary()
            self.loginButton.customizeSecondary()
            self.resetPasswordButton.customizeSecondary()
            self.createAccountButton.setTitle(NSLocalizedString("sign_up", bundle: bundle, comment: ""), for: .normal)
            self.loginButton.setTitle(NSLocalizedString("log_in", bundle: bundle, comment: ""), for: .normal)
            self.resetPasswordButton.setTitle(NSLocalizedString("forgot_password", bundle: bundle, comment: ""), for: .normal)
            self.createAccountButton.accessibilityLabel = NSLocalizedString("sign_up", bundle: bundle, comment: "")
            self.loginButton.accessibilityLabel = NSLocalizedString("log_in", bundle: bundle, comment: "")
            self.resetPasswordButton.accessibilityLabel = NSLocalizedString("forgot_password", bundle: bundle, comment: "")
            
//            self.emailTxtField.rx.text
//                .asObservable()
//                .map({$0 != nil && !$0!.isEmpty && $0!.isEmail})
//                .subscribe(onNext: { value in
//                    print(value)
//                    self.emailTxtField.layer.borderColor = value ? UIColor.lightGray.cgColor : UIColor.red.cgColor
//                    self.emailTxtField.layer.masksToBounds = true
//                    self.emailTxtField.layer.borderWidth = value ? 0.25 : 1.0
//                    self.emailTxtField.layer.cornerRadius = 5
//                })
//            self.passwordTxtField.rx.text
//                .asObservable()
//                .map({$0 != nil && !$0!.isEmpty})
//                .subscribe(onNext: { value in
//                    print(value)
//                    self.passwordTxtField.layer.borderColor = value ? UIColor.lightGray.cgColor : UIColor.red.cgColor
//                    self.passwordTxtField.layer.borderWidth = value ? 0.25 : 1.0
//                    self.passwordTxtField.layer.cornerRadius = 5
//                })
        }
    }
    
    // MARK: - IBActions
    @IBAction func login(_ sender: Any) {
        guard let email = emailTxtField.text,
            let password = passwordTxtField.text else {
                return
        }
        
        UserManager.shared.login(email: email, password: password, successHandler: { 
            print("Success on login")
            User.shared = User(email: email, password: password)
            self.navigationController?.popViewController(animated: true)
        }, errorHandler: { (error) in
            print("Error on login : \(error.localizedDescription)")
            self.handleError(error)
        })
    }
    
    @IBAction func createAccount(_ sender: Any) {
        guard let email = emailTxtField.text,
            let password = passwordTxtField.text else {
                return
        }
        
        UserManager.shared.createUser(email: email, password: password, successHandler: { 
            print("Success on creating an account")
            
            self.navigationController?.popViewController(animated: true)
        }, errorHandler: { (error) in
            print("Error on login : \(error.localizedDescription)")
            self.handleError(error)
        })
    }
    
    @IBAction func logout(_ sender: Any) {
        UserManager.shared.logout(successHandler: { 
            print("Success on logout")
            User.shared = User()
            User.shared.email.accept(nil)
            User.shared.password = nil
            self.navigationController?.popViewController(animated: true)
        }, errorHandler: { (error) in
            print("Error on logout : \(error.localizedDescription)")
            self.handleError(error)
        })
    }
    
    @IBAction func resetPassword(_ sender: Any) {
        guard let email = emailTxtField.text else { return }
        UserManager.shared.resetPassword(email: email, successHandler: {
            // Add Alert
            let alert = Utils.showAlert(with: NSLocalizedString("forgot_password", bundle: self.bundle, comment: ""), message: NSLocalizedString("email_send_password", bundle: self.bundle, comment: ""))
            self.present(alert, animated: false)
        }, errorHandler: { (error) in
            print("Error on reset Password : \(error.localizedDescription)")
            self.handleError(error)
        })
    }
    
    @IBAction func deleteAccount(_ sender: Any) {
        UserManager.shared.deleteAccount(successHandler: {
            // Add Alert
            let alertController = UIAlertController(title: NSLocalizedString("account_deleted", bundle: self.bundle, comment: ""), message: NSLocalizedString("account_deleted_success", bundle: self.bundle, comment: ""), preferredStyle: .alert)
            let action = UIAlertAction(title: NSLocalizedString("ok", bundle: self.bundle, comment: ""), style: .default, handler: { _ in 
                User.shared = User()
                User.shared.email.accept(nil)
                User.shared.password = nil
                self.navigationController?.popViewController(animated: true)
            })
            alertController.addAction(action)
            self.present(alertController, animated: false)
        }, errorHandler: { (error) in
            self.handleError(error)
        })
    }
    
    // MARK: - UITextFieldDelegate
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        let attributedString = NSAttributedString( // For Voice Over to say the dot in email adress
            string: textField.text ?? "", attributes: [.accessibilitySpeechPunctuation: true]
        )
        textField.accessibilityAttributedValue = attributedString
        UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: textField)
        return true
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        if self.emailTxtField == textField { // Check that the email is valid
            if let txt = textField.text, txt.isNotEmpty, !txt.isEmail {
                let alert = Utils.showAlert(with: NSLocalizedString("warning", bundle: self.bundle, comment: ""), message: NSLocalizedString("invalid_email", bundle: self.bundle, comment: ""))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}
