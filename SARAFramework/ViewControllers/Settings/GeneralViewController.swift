//
//  GeneralViewController.swift
//  SARA Croisiere
//
//  Created by Rose on 02/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift
import RealmSwift
import RxOptional

public class GeneralViewController: UITableViewController, SegmentedControlView {
    
    // MARK: - IBOutlets
    @IBOutlet private weak var isConnectedLabel: UILabel!
    @IBOutlet private weak var connectedMail: UILabel!
    
    @IBOutlet private weak var distanceRateSlider: UIAccessibilitySlider!
    @IBOutlet private weak var distanceTxtLabel: UILabel!
    @IBOutlet private weak var distanceUnitLabel: UILabel!
    @IBOutlet weak var distanceDecreaseButton: UIButton!
    @IBOutlet weak var distanceIncreaseButton: UIButton!
    
    @IBOutlet private weak var unitHeaderLabel: UILabel!
    @IBOutlet private weak var averageHeaderLabel: UILabel!
    @IBOutlet private weak var distanceThresholdHeaderLabel: UILabel!

    @IBOutlet weak var resetSpeedLabel: UILabel!
    
    @IBOutlet weak var resetSpeedButton: UIButton!
    @IBOutlet weak var userCell: UITableViewCell!
    @IBOutlet weak var resetCell: UITableViewCell!
    @IBOutlet weak var resetLabel: UILabel!
    
    @IBOutlet weak var courseCompassCell: UITableViewCell!
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: GeneralViewController.classForCoder())
    let disposeBag = DisposeBag()
    var displayValidationPoint: Bool = true
    
    // MARK: -
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("menu_general", bundle: bundle, comment: "")
        
        // TO REMOVE
//        userCell.isUserInteractionEnabled = false
//        userCell.contentView.alpha = 0.2
//        userCell.accessibilityLabel = "\(NSLocalizedString("settings_user_title", bundle: bundle, comment: "")) \(NSLocalizedString("dimmed", bundle: bundle, comment: ""))"
        
        updateUserInfos()
        performBindings()
   
        unitHeaderLabel.text = NSLocalizedString("settings_unit_header", bundle: bundle, comment: "")
        averageHeaderLabel.text = NSLocalizedString("settings_averages_header", bundle: bundle, comment: "")
        
        resetCell.selectionStyle = .none
        resetLabel.isAccessibilityElement = false
        resetSpeedLabel.text = NSLocalizedString("settings_reset_values", bundle: bundle, comment: "")
        resetSpeedButton.customizePrimary()
        resetSpeedButton.setTitle(NSLocalizedString("settings_reset", bundle: bundle, comment: ""), for: .normal)
        resetSpeedButton.accessibilityLabel = NSLocalizedString("settings_reset_button", bundle: bundle, comment: "")
        
        distanceThresholdHeaderLabel.text = NSLocalizedString("settings_threshold_header", bundle: bundle, comment: "")
        
        distanceRateSlider.tintColor = Utils.primaryColor
        distanceDecreaseButton.customizeSliderButton()
        distanceIncreaseButton.customizeSliderButton()
        
        guard let view = Bundle(for: GeneralViewController.self).loadNibNamed("UnitSettingsView", owner: nil, options: nil)?.first as? UnitSettingsView else {
            fatalError()
        }
        view.setLabel(text: NSLocalizedString("calculate_relative_bearing", bundle: bundle, comment: ""))
        let titleArray = [NSLocalizedString("course_over_ground", bundle: bundle, comment: ""), NSLocalizedString("compass", bundle: bundle, comment: "")]
        let selectedIndex = Settings.shared.useCompass ? 1 : 0
        view.setSegmentedControl(titleArray: titleArray, selectedIndex: selectedIndex)
        view.segmentedViewDelegate = self
        courseCompassCell.addSubview(view)
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateUserInfos()
    }
    
    // MARK: - Methods
    func updateUserInfos() {
        if let email = User.shared.email.value {
            self.isConnectedLabel.text = NSLocalizedString("logged_in", bundle: bundle, comment: "")
            self.connectedMail.text = email
            self.connectedMail.isAccessibilityElement = false
            self.isConnectedLabel.isAccessibilityElement = false
            // For Voice Over to say the dot in email adress
            let attributedString = NSAttributedString(string: "\(NSLocalizedString("logged_in", bundle: bundle, comment: "")) \(email)", attributes: [.accessibilitySpeechPunctuation: true])
//            self.connectedMail.accessibilityAttributedLabel = attributedString
            
            userCell.accessibilityLabel = "\(NSLocalizedString("logged_in", bundle: bundle, comment: "")) \(email)"
            userCell.accessibilityAttributedLabel = attributedString
        } else {
            self.isConnectedLabel.text = NSLocalizedString("not_logged_in", bundle: bundle, comment: "")
            self.connectedMail.text = nil
        }
        
        
        distanceRateSlider.distanceValidationSliderSetUp(valueLabel: distanceTxtLabel, value: Settings.shared.minimumDistanceThreshold)
        
        self.distanceUnitLabel.text = NavigationUnit.meter.abbreviation
        self.distanceUnitLabel.accessibilityLabel = NavigationUnit.meter.description
    }
    
    public func performBindings() {
        self.distanceRateSlider.rx
            .value
            .distinctUntilChanged()
            .map({ [weak self] (value) -> String in
                let stringValue = "\(Int(value))"
                self?.distanceRateSlider.accessibilityValue = stringValue
                return stringValue
            })
            .bind(to: self.distanceTxtLabel.rx.text)
            .disposed(by: disposeBag)
        
        self.distanceRateSlider.rx
            .value
            .distinctUntilChanged()
            .skip(1)
            .map { String(format: "%.1f", $0) }
            .map { Float($0) }
            .filterNil()
            .subscribe(onNext: { (distance) in
                let meterDistance = distance.rounded()
                let realm = try! Realm()
                try! realm.write {
                    Settings.shared.minimumDistanceThreshold = meterDistance
                }
            }).disposed(by: disposeBag)
        
        self.distanceIncreaseButton.rx.tap.bind { [weak self] in
            guard let weakSelf = self else { return }
            weakSelf.distanceRateSlider.accessibilityIncrement()
            }.disposed(by: disposeBag)
        
        self.distanceDecreaseButton.accessibilityLabel = NSLocalizedString("minus", bundle: bundle, comment: "")
        self.distanceDecreaseButton.rx.tap.bind { [weak self] in
            guard let weakSelf = self else { return }
            weakSelf.distanceRateSlider.accessibilityDecrement()
            }.disposed(by: disposeBag)
        
        self.resetSpeedButton.rx.tap.bind {
            NmeaManager.default.resetMaxSpeeds()
            LocationManager.shared.resetAverages()
            let when = DispatchTime.now() + 1
            DispatchQueue.main.asyncAfter(deadline: when) {
                UIAccessibility.post(notification: UIAccessibility.Notification.announcement, argument: NSLocalizedString("settings_reset_zero", bundle: self.bundle, comment: ""))
            }
//            self.resetSpeedButton.accessibilityLabel = NSLocalizedString("settings_reset", comment: "")
        }.disposed(by: disposeBag)
    }
    
    public func setDisplayValidationPoint(value: Bool) {
        self.displayValidationPoint = value
    }
    
    // MARK: - TableView Delegate / DataSource
    public override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return NSLocalizedString("settings_user_title", bundle: bundle, comment: "")
        case 1:
            return NSLocalizedString("settings_announces_parameter", bundle: bundle, comment: "")
        case 2:
            return NSLocalizedString("settings_app_parameter", bundle: bundle, comment: "")
        case 3:
            return NSLocalizedString("settings_default_values", bundle: bundle, comment: "")
        default:
            return ""
        }
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = super.tableView(tableView, numberOfRowsInSection: section)
        if !displayValidationPoint && section == 2 { // // For SARA Race, we don't need the validation point info
            // remove the first cell
            return count - 1
        }
        return count
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if !displayValidationPoint && indexPath.section == 2 && indexPath.row == 0 { // For SARA Race, we don't need the validation point info
            // remove the first cell by skipping it and returning the second cell in its place
            return super.tableView(tableView, cellForRowAt: IndexPath(row: indexPath.row + 1, section: 2))
        }
        return super.tableView(tableView, cellForRowAt: indexPath)
    }
    
    // MARK: - SegmentedControlViewDelegate
    public func valueChanged(selectedIndex: Int) {
        Settings.shared.setUseCompass(value: selectedIndex == 1)
    }
    
}
