//
//  AboutViewController.swift
//  SARA Croisiere
//
//  Created by Marine on 27.11.20.
//  Copyright © 2020 GraniteApps. All rights reserved.
//

import UIKit
import WebKit

class AboutViewController: UIViewController, WKNavigationDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var navigationTitle: UINavigationItem!
    @IBOutlet weak var webView: WKWebView!
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: AboutViewController.classForCoder())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationTitle.title = NSLocalizedString("menu_about", bundle: bundle, comment: "")
        // Do any additional setup after loading the view.
        
        webView.navigationDelegate = self
        webView.loadHTMLString(String(format: Utils.localizableProtocol?.aboutText() ?? "", Utils.releaseVersionNumber!), baseURL: Bundle.main.bundleURL)
        
//        webView.loadHTMLString(String(format: NSLocalizedString("about", bundle: bundle, comment: ""), Utils.releaseVersionNumber!), baseURL: Bundle.main.bundleURL)
        webView.isOpaque = false
        webView.backgroundColor = UIColor.clear
    }
    
    // MARK: - WKNavigationDelegate
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == .linkActivated {
            guard let url = navigationAction.request.url else {
                print("Link is not a url")
                decisionHandler(.allow)
                return
            }
            if url.absoluteString.hasPrefix("file:") {
                print("Open link locally")
                decisionHandler(.allow)
            } else if url.absoluteString.hasPrefix("mailto:") {
                print("Send email locally")
//                sendEmail()
//                decisionHandler(.allow)
            } else {
                print("Open link locally")
                decisionHandler(.allow)
            }
        } else {
            print("not a user click")
            decisionHandler(.allow)
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        let cssString = "@media (prefers-color-scheme: dark) {body {color: white;}a:link {color: #0096e2;}a:visited {color: #9d57df;}}"
        let jsString = "var style = document.createElement('style'); style.innerHTML = '\(cssString)'; document.head.appendChild(style);"
        webView.evaluateJavaScript(jsString, completionHandler: nil)
    }
}
