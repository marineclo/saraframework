//
//  UnitsViewController.swift
//  SARA Croisiere
//
//  Created by Rose on 27/01/2018.
//  Copyright © 2018 Rose. All rights reserved.
//

import Foundation
import UIKit

public class UnitsViewController: UIViewController {
    //var viewModel: UnitsViewModel?
    // MARK: - Variables
    
    
    // MARK: - IBOutlets
    @IBOutlet weak private var stackView: UIStackView?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("units_management", bundle: Utils.bundle(anyClass: UnitsViewController.self), comment: "")
        
        for unit in UnitType.all() {
            guard let view = Bundle(for: UnitsViewController.self).loadNibNamed("UnitSettingsView", owner: nil, options: nil)?.first as? UnitSettingsView else {
                fatalError()
            }
            view.customize(with: unit)
            stackView?.addArrangedSubview(view)
        }
    }
}
