//
//  TracesListViewController.swift
//  SARA Croisiere
//
//  Created by Marine on 24.04.20.
//  Copyright © 2020 GraniteApps. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class TracesListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, SelectButtonCellDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var exportButton: UIBarButtonItem!
    @IBOutlet weak var titleNavigation: UINavigationItem!
    @IBOutlet weak var sensitivityView: SliderSettingView!
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: TracesListViewController.classForCoder())
    let bundleView = Bundle(for: SliderSettingView.self)
    var selectedTracesForExport: BehaviorRelay<[Int]> = BehaviorRelay(value: [])
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.customize()
        self.tableView.register(UINib(nibName: "HeaderTableView", bundle: bundleView), forHeaderFooterViewReuseIdentifier: "HeaderTableView")
        self.tableView.register(UINib(nibName: "SelectButtonCell", bundle: bundleView), forCellReuseIdentifier: "traceCellIdentifier")
        if #available(iOS 15, *) { // To remove gap between header and table view
            self.tableView.sectionHeaderTopPadding = 0
        }
        self.titleNavigation.title = NSLocalizedString("tracks", bundle: bundle, comment: "")
        
        guard let view = bundleView.loadNibNamed("SliderSettingView", owner: nil, options: nil)?.first as? SliderSettingView else {
            fatalError()
        }
        let parameter: AveragesInfos =
            AveragesInfos(title: NSLocalizedString("sensitivity", bundle: bundle, comment: ""), unit: NavigationUnit.meter, isContinuous: false, type: .trace, minValue: 1, maxValue: 1000)
        view.customize(with: parameter)
        self.sensitivityView.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.topAnchor.constraint(equalTo: sensitivityView.topAnchor).isActive = true
        view.leadingAnchor.constraint(equalTo: sensitivityView.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: sensitivityView.trailingAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: sensitivityView.bottomAnchor).isActive = true
        
        selectedTracesForExport.asObservable()
            .subscribe(onNext: { (array) in
                if array.count > 0 {
                    self.exportButton.isEnabled = true
                    self.exportButton.accessibilityLabel = String(format: NSLocalizedString("export_tracks", bundle: self.bundle, comment: ""), array.count)
                } else {
                    self.exportButton.isEnabled = false
                    self.exportButton.accessibilityLabel = String(format: NSLocalizedString("export_tracks", bundle: self.bundle, comment: ""), array.count)
                }
            }).disposed(by: disposeBag)
        
        // TODO: - Disable button for version 1
//        self.exportButton.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }
    
    // MARK: - Tableview
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TraceManager.shared.getAllTracesFiles()?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "traceCellIdentifier") as? SelectButtonCell else {
            return UITableViewCell()
        }
        
        cell.delegate = self
        cell.titleLabel?.font = UIFont.preferredFont(forTextStyle: .body)
        if let urls = TraceManager.shared.getAllTracesFiles() {
            cell.titleLabel?.text = urls[indexPath.row].deletingPathExtension().lastPathComponent
        }
        cell.idElementSelected = String(indexPath.row)
        if selectedTracesForExport.value.contains(indexPath.row) {
            cell.selectButton?.setImage(UIImage(named: "globe", in: bundle, compatibleWith: nil), for: .normal)
            cell.selectButton?.accessibilityLabel = NSLocalizedString("selected", bundle: bundle, comment: "")
        } else {
            cell.selectButton?.setImage(UIImage(named: "circle", in: bundle, compatibleWith: nil), for: .normal)
            cell.selectButton?.accessibilityLabel = NSLocalizedString("add_export", bundle: bundle, comment: "")
        }
        cell.selectButton?.tintColor = Utils.tintIcon
        return cell
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderTableView" ) as! HeaderTableView
        returnedView.customizeHeader(titleName: NSLocalizedString("menu_traces", bundle: bundle, comment: ""))
        return returnedView
    }

    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Settings", bundle: bundle)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "TraceMapViewController") as? TraceMapViewController,
            let urls = TraceManager.shared.getAllTracesFiles() else {
            return
        }
        controller.urlTrace = urls[indexPath.row]
        self.navigationController?.pushViewController(controller, animated: true)
    }
        
    // MARK: - SelectButtonCellDelegate
    func didSelectCell(id: String) {
        guard let rowNumber = Int(id) else {
            return
        }
        if let index = selectedTracesForExport.value.firstIndex(of: rowNumber) {
            var newArray = selectedTracesForExport.value
            newArray.remove(at: index)
            selectedTracesForExport.accept(newArray)
        } else {
            var newArray = selectedTracesForExport.value
            newArray.append(rowNumber)
            selectedTracesForExport.accept(newArray)
        }
        tableView.reloadRows(at: [IndexPath(row: rowNumber, section: 0)], with: .automatic)
    }
    
    @IBAction func exportAction(_ sender: Any) {
        guard let urls = TraceManager.shared.getAllTracesFiles() else {
            return
        }
        var items: [URL] = []
        self.selectedTracesForExport.value.forEach({
            if let urlPath = TraceManager.shared.convertToGPX(fileURL: urls[$0]) { // Add GPX file
                items.append(urlPath)
            }
            items.append(urls[$0]) //TODO:  Add text files: for debug remove for next version
        })
        
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present(ac, animated: true)
    }
    
}
