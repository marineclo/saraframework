//
//  AveragesViewController.swift
//  SARA Croisiere
//
//  Created by Rose on 07/12/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import UIKit

public class AveragesViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak private var scrollView: UIScrollView!
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: AveragesViewController.classForCoder())
    let bundleView = Bundle(for: AveragesViewController.self)
    private var stackView = UIStackView()
    
    private var averages: [AveragesInfos] =
        [AveragesInfos(title: NSLocalizedString("average_speed", bundle: Utils.bundle(anyClass: AveragesViewController.classForCoder()), comment: ""), unit: NavigationUnit.second, isContinuous: false, type: .speed, minValue: 0, maxValue: 30), AveragesInfos(title: NSLocalizedString("average_course", bundle: Utils.bundle(anyClass: AveragesViewController.classForCoder()), comment: ""), unit: NavigationUnit.second, isContinuous: false, type: .course, minValue: 0, maxValue: 30)]
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("averages_management", bundle: bundle, comment: "")
        
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        scrollView.addSubview(stackView)
        stackView.spacing = 15
        
        stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        stackView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        
        for parameter in self.averages {
            guard let view = bundleView.loadNibNamed("SliderSettingView", owner: nil, options: nil)?.first as? SliderSettingView else {
                fatalError()
            }
            view.customize(with: parameter)
            stackView.addArrangedSubview(view)
        }
    }
}

struct AveragesInfos {
    let title: String
    let unit: NavigationUnit?
    let isContinuous: Bool
    let type: AverageType
    let minValue: Float
    let maxValue: Float
    
    init(title: String, unit: NavigationUnit?, isContinuous: Bool, type: AverageType, minValue: Float, maxValue: Float) {
        self.title = title
        self.unit = unit
        self.isContinuous = isContinuous
        self.type = type
        self.minValue = minValue
        self.maxValue = maxValue
    }
}

enum AverageType {
    case course
    case speed
    case trace
}
