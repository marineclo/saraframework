//
//  TellTalesSettingsViewController.swift
//  SARA Croisiere
//
//  Created by Tristan Derbanne on 11.07.19.
//  Copyright © 2019 Rose. All rights reserved.
//

import UIKit
import RealmSwift

class TellTalesSettingsViewController: UIViewController, TellTaleManagerObserver, UITableViewDataSource {

    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: TellTalesSettingsViewController.classForCoder())
    
    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Table view data source & delegate
    
    // TODO: Update data model when we know what we expect.
    /* Data source model.
     For now: simply online telltales
    */
    private var tellTales: [TellTale] = TellTalesManager.default.tellTales
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tellTales.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TellTaleTableViewCell") as! TellTaleTableViewCell
        
        let tellTale = self.tellTales[indexPath.row]
        if !tellTale.name.isEmpty {
            cell.tellTaleNameLabel.text = tellTale.name
        }
        else {
            cell.tellTaleNameLabel.text = tellTale.id
        }
        
        cell.tellTaleIdNameLabel.text = tellTale.idName
        cell.isOnline = tellTale.isOnline
        
        cell.tellTaleSwitch.isHidden = !tellTale.isConfigured
        cell.tellTaleSwitch.isOn = tellTale.isEnabled
        cell.tellTaleSwitch.addTarget(self, action: #selector(switchTellTale(_:)), for: .valueChanged)
        
        cell.tellTaleSwitch.tintColor = Utils.secondaryColor
        cell.tellTaleSwitch.onTintColor = Utils.primaryColor
        
        // Accessibility
        cell.tellTaleNameLabel.accessibilityLabel = cellNameAccessibility(tellTale: tellTale)
        
        cell.tellTaleSwitch.accessibilityLabel = cellSwitchAccessibility(switchOn: cell.tellTaleSwitch.isOn)

        return cell
    }
    
    @objc private func switchTellTale(_ sender: UISwitch) {
        let switchPoint = sender.convert(sender.bounds.origin, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: switchPoint) {
            TellTalesManager.default.enableTelltale(self.tellTales[indexPath.row], enable: sender.isOn)
            let cell = self.tableView.cellForRow(at: indexPath) as! TellTaleTableViewCell
            cell.tellTaleNameLabel.accessibilityLabel = cellNameAccessibility(tellTale: self.tellTales[indexPath.row])
            
            cell.tellTaleSwitch.accessibilityLabel = cellSwitchAccessibility(switchOn: cell.tellTaleSwitch.isOn)
        }
    }
    
    private func cellNameAccessibility(tellTale: TellTale) -> String {
        var accessibilityLabel: String = NSLocalizedString("telltale", bundle: bundle, comment: "")
        if tellTale.isConfigured {
            accessibilityLabel += " " + tellTale.name + " " + tellTale.idName + " " + NSLocalizedString("configured", bundle: bundle, comment: "") + ", "
            if tellTale.isEnabled {
                accessibilityLabel += " " + NSLocalizedString("enabled", bundle: bundle, comment: "")
            }
            else {
                accessibilityLabel += " " + NSLocalizedString("disabled", bundle: bundle, comment: "")
            }
        }
        else {
            accessibilityLabel += " " + tellTale.id + " " + NSLocalizedString("not_configured", bundle: bundle, comment: "")
        }
        if tellTale.isOnline {
            accessibilityLabel += " " + NSLocalizedString("connected", bundle: bundle, comment: "")
        }
        else {
            accessibilityLabel += " " + NSLocalizedString("not_connected", bundle: bundle, comment: "")
        }
        return accessibilityLabel
    }
    
    private func cellSwitchAccessibility(switchOn: Bool) -> String {
        if switchOn {
            return NSLocalizedString("disable", bundle: bundle, comment: "") + " " + NSLocalizedString("telltale", bundle: bundle, comment: "")
        }
        else {
            return NSLocalizedString("enable", bundle: bundle, comment: "") + " " + NSLocalizedString("telltale", bundle: bundle, comment: "")
        }
    }
    
    // MARK: - View controller overrides
    override func viewDidLoad() {
        super.viewDidLoad()

        TellTalesManager.default.addObserver(self)
        
        // TODO: for testing purpose only. remove once tested
        let addTelltaleItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(createFakeTellTale))
        addTelltaleItem.isAccessibilityElement = true
        addTelltaleItem.accessibilityLabel = NSLocalizedString("add_fake_telltale", bundle: bundle, comment: "")
        
        self.navigationItem.setRightBarButton(addTelltaleItem, animated: false)
        
        self.navigationItem.title = "\(NSLocalizedString("telltales_title", bundle: bundle, comment: ""))"
        
        if #available(iOS 15, *) { // To remove gap between header and table view
            self.tableView.sectionHeaderTopPadding = 0
        }
    }
    
    @objc fileprivate func createFakeTellTale() {
        self.performSegue(withIdentifier: "configureFakeTelltale", sender: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tableView.reloadData()
        NotificationCenter.default.addObserver(self, selector: #selector(handleModalDismissed), name: NSNotification.Name(rawValue:"telltaleConfiguration"), object: nil)
    }
    
    deinit {
        TellTalesManager.default.removeObserver(self)
    }
    
    @objc func handleModalDismissed() {
      self.tableView?.reloadData()
    }
    
    // MARK: - Tell tale manager observer
    func tellTaleManager(didUpdateTellTales updatedTellTales: [TellTale]) {
        updatedTellTales.forEach {
            if let i = self.tellTales.firstIndex(of: $0) {
                let cell = self.tableView.cellForRow(at: IndexPath(row: i, section: 0)) as! TellTaleTableViewCell
                cell.isOnline = $0.isOnline
                cell.tellTaleSwitch.isHidden = !$0.isConfigured
                cell.tellTaleSwitch.isOn = $0.isEnabled
            }
            else {
                self.tellTales.append($0)
                self.tableView.insertRows(at: [IndexPath(row: self.tellTales.count - 1, section: 0)], with: .automatic)
            }
        }
    }
    
    func tellTaleManager(didDeleteTellTales deletedTellTales: [TellTale]) {
        for tellTale in deletedTellTales {
            if let i = self.tellTales.firstIndex(of: tellTale) {
                self.tellTales.remove(at: i)
                self.tableView.deleteRows(at: [IndexPath(row: i, section: 0)], with: .automatic)
            }
        }
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "configureTellTaleSegue" {
            let tellTale = self.tellTales[tableView.indexPathForSelectedRow!.row]
            (segue.destination as! TellTaleConfigurationViewController).tellTale = tellTale
        }
        if segue.identifier == "configureFakeTelltale" {
            (segue.destination as! TellTaleConfigurationViewController).tellTale = TellTalesManager.default.fakeTelltale()
        }
    }

}
