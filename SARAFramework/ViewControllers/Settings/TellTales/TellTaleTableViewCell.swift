//
//  TellTaleTableViewCell.swift
//  SARA Croisiere
//
//  Created by Tristan Derbanne on 11.07.19.
//  Copyright © 2019 Rose. All rights reserved.
//

import UIKit

class TellTaleTableViewCell: UITableViewCell {

    @IBOutlet weak var onlineIndicatorView: UIView!
    @IBOutlet weak var tellTaleNameLabel: UILabel!
    @IBOutlet weak var tellTaleIdNameLabel: UILabel!
    @IBOutlet weak var tellTaleSwitch: UISwitch!
    
//    private static let onlineColor = #colorLiteral(red: 0, green: 0.5603182912, blue: 0, alpha: 1)
    private static let onlineColor = Utils.secondaryColor
    private static let offlineColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
    
    var isOnline: Bool = false {
        didSet {
            if isOnline {
                self.onlineIndicatorView.backgroundColor = TellTaleTableViewCell.onlineColor

            }
            else {
                self.onlineIndicatorView.backgroundColor = TellTaleTableViewCell.offlineColor
            }
        }
    }
    
    var isEnabled: Bool = true {
        didSet {
            self.tellTaleSwitch.isOn = isEnabled
            self.tellTaleNameLabel.text = nil
            self.tellTaleIdNameLabel.text = nil
        }
    }
    
    override func prepareForReuse() {
        self.onlineIndicatorView.backgroundColor = TellTaleTableViewCell.offlineColor
        self.tellTaleSwitch.isHidden = true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.onlineIndicatorView.layer.cornerRadius = 0.5 * self.onlineIndicatorView.frame.size.height
        self.onlineIndicatorView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
