//
//  TellTaleConfigurationViewController.swift
//  SARA Croisiere
//
//  Created by Tristan Derbanne on 11.07.19.
//  Copyright © 2019 Rose. All rights reserved.
//

import UIKit

class TellTaleConfigurationViewController: UIViewController, UITextFieldDelegate {
    struct TelltaleSettings {
        var name: String = ""
        var idName: String = ""
        var medianSamplingTime: Int = 0
        var attachedThreshold: Int = 0
        var turbulentThreshold: Int = 0
        var angleComputationSlope: Float = 0
        var angleComputationYIntercept: Float = 0
        var statusMethod: String = TellTale.StatusMethod.mediane.rawValue
        
        init(tellTale: TellTale) {
            self.name = tellTale.name
            self.idName = tellTale.idName
            self.medianSamplingTime = tellTale.medianSamplingTime
            self.attachedThreshold = tellTale.attachedThreshold
            self.turbulentThreshold = tellTale.turbulentThreshold
            self.angleComputationSlope = tellTale.angleComputationSlope
            self.angleComputationYIntercept = tellTale.angleComputationYIntercept
            self.statusMethod = tellTale.statusMethod
        }
    }
    
    // MARK: - Variables and UI outlets
    let bundle = Utils.bundle(anyClass: TellTaleConfigurationViewController.classForCoder())
    var tellTale: TellTale?
    private var  settings: TelltaleSettings!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    // Name
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var tellTaleIdLabel: UILabel!
    @IBOutlet weak var subNameLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var subNameTextField: UITextField!
    @IBOutlet weak var tellTaleIdNameLabel: UILabel!
    
    @IBOutlet weak var basedOnMethodLabel: UILabel!
    @IBOutlet weak var methodCalculationButton: UIButton!
    // Median sampling time
    @IBOutlet weak var medianSamplingTimeView: UIView!
    
    @IBOutlet weak var medianSamplingTimeLabel: UILabel!
    @IBOutlet weak var medianSamplingTimeValueLabel: UILabel!
    @IBOutlet weak var medianSamplingTimeSlider: UIAccessibilitySlider!
    @IBOutlet weak var increaseMedianSamplingTimeButton: UIButton!
    @IBOutlet weak var decreaseMedianSamplingTimeButton: UIButton!
    
    // Telltales status thresholds
    @IBOutlet weak var statusThresholdView: UIView!
    
    @IBOutlet weak var tellTaleThresholdsLabel: UILabel!
    @IBOutlet weak var thresholdAutoSettingButton: UIButton!
    
    @IBOutlet weak var tellTaleThresholdAttachedLabel: UILabel!
    @IBOutlet weak var tellTaleThresholdAttachedValueLabel: UILabel!
    @IBOutlet weak var tellTaleThresholdAttachedSlider: UIAccessibilitySlider!
    @IBOutlet weak var tellTaleThresholdAttachedDecreaseButton: UIButton!
    @IBOutlet weak var tellTaleThresholdAttachedIncreaseButton: UIButton!
    
    
    @IBOutlet weak var tellTaleThresholdTurbulentLabel: UILabel!
    @IBOutlet weak var tellTaleThresholdTurbulentValueLabel: UILabel!
    @IBOutlet weak var tellTaleThresholdTurbulentSlider: UIAccessibilitySlider!
    @IBOutlet weak var tellTaleThresholdTurbulentDecreaseButton: UIButton!
    @IBOutlet weak var tellTaleThresholdTurbulentIncreaseButton: UIButton!
    
    // Telltales angle computation settings
    @IBOutlet weak var angleComputationSettingsView: UIView!
    
    @IBOutlet weak var angleComputationLabel: UILabel!
    @IBOutlet weak var slopeLabel: UILabel!
    @IBOutlet weak var slopeTextField: UITextField!
    @IBOutlet weak var yInterceptLabel: UILabel!
    @IBOutlet weak var yInterceptTextField: UITextField!
    
    // Delete button
    @IBOutlet weak var deleteTelltaleButton: UIButton!
    
    // MARK: - User interactions
    
    @IBAction func barButtonItemTapped(_ sender: UIBarButtonItem) {
        if sender == saveButton {
            self.nameTextField.resignFirstResponder()
            self.subNameTextField.resignFirstResponder()
            self.slopeTextField.resignFirstResponder()
            self.yInterceptTextField.resignFirstResponder()
            TellTalesManager.default.saveTelltale(tellTale!, settings: settings!)
        }
        self.dismiss(animated: true, completion: {
              NotificationCenter.default.post(name: NSNotification.Name(rawValue: "telltaleConfiguration"), object: nil)
        })
    }
    
    
    @IBAction func textFieldEditingChanged(_ sender: UITextField) {
        if sender == nameTextField {
            settings!.name = nameTextField.text ?? ""
            self.saveButton.isEnabled = !settings!.name.isEmpty
        }
        if sender == subNameTextField {
            settings!.idName = subNameTextField.text ?? ""
        }
    }
    
    @IBAction func textFieldEditingDidEnd(_ sender: UITextField) {
        if sender == slopeTextField {
            if let fString = slopeTextField.text, let f = Float(fString) {
                settings.angleComputationSlope = f
            }
        }
        if sender == yInterceptTextField {
            if let fString = yInterceptTextField.text, let f = Float(fString) {
                settings.angleComputationYIntercept = f
            }
        }
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        if sender == medianSamplingTimeSlider {
            self.setMedianSamplingTime(Int(sender.value.rounded()))
        }
        if sender == tellTaleThresholdAttachedSlider {
            self.setAttachedThresholdValue(Int(sender.value.rounded()))
        }
        if sender == tellTaleThresholdTurbulentSlider {
            self.setTurbulentThresholdValue(Int(sender.value.rounded()))
        }
    }
    
    @IBAction func valueButtonTapped(_ sender: UIButton) {
        if sender == decreaseMedianSamplingTimeButton {
            self.setMedianSamplingTime(settings!.medianSamplingTime - TellTale.medianSamplingTimeStep)
        }
        if sender == increaseMedianSamplingTimeButton {
            self.setMedianSamplingTime(settings!.medianSamplingTime + TellTale.medianSamplingTimeStep)
        }
        if sender == tellTaleThresholdAttachedDecreaseButton {
            self.setAttachedThresholdValue(settings!.attachedThreshold - TellTale.statusThresholdStep)
        }
        if sender == tellTaleThresholdAttachedIncreaseButton {
            self.setAttachedThresholdValue(settings!.attachedThreshold + TellTale.statusThresholdStep)
        }
        if sender == tellTaleThresholdTurbulentDecreaseButton {
            self.setTurbulentThresholdValue(settings!.turbulentThreshold - TellTale.statusThresholdStep)
        }
        if sender == tellTaleThresholdTurbulentIncreaseButton {
            self.setTurbulentThresholdValue(settings!.turbulentThreshold + TellTale.statusThresholdStep)
        }
    }
    
    @IBAction func autoSettingButtonTapped(_ sender: Any) {
        guard let median = tellTale!.medianAngle?.rounded() else {
            // TODO: Notify user?
            return
        }
        let min = Int(median) - 2
        let max = Int(median) + 2
        guard min >= TellTale.attachedThresholdMin else { return }
        guard min <= TellTale.attachedThresholdMax else { return }
        guard max <= TellTale.turbulentThresholdMax else { return }
        guard max >= TellTale.turbulentThresholdMin else { return }
        self.setAttachedThresholdValue(Int(median) - 2, discardWrongValues: false)
        self.setTurbulentThresholdValue(Int(median) + 2, discardWrongValues: false)
    }
    
    @IBAction func deleteButtonTapped(_ sender: Any) {
        let alertController = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: NSLocalizedString("warning_delete_telltale", bundle: bundle, comment: ""), preferredStyle: .alert)
        let yesAction = UIAlertAction(title: NSLocalizedString("yes", bundle: bundle, comment: ""), style: .default) { _ in
            TellTalesManager.default.deleteTellTale(self.tellTale!)
            self.dismiss(animated: true, completion: {
                  NotificationCenter.default.post(name: NSNotification.Name(rawValue: "telltaleConfiguration"), object: nil)
            })
        }
        let noAction = UIAlertAction(title: NSLocalizedString("cancel", bundle: bundle, comment: ""), style: .cancel, handler: nil)
        alertController.addAction(noAction)
        alertController.addAction(yesAction)
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func methodChanged(_ sender: Any) {
        let methodMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        TellTale.StatusMethod.allCases.forEach({
            let statusMethod = $0
            let methodAction = UIAlertAction(title: $0.description, style: .default, handler: { [weak self] _ in
                guard let weakSelf = self else { return }
                self?.settings.statusMethod = statusMethod.rawValue
                self?.methodCalculationButton.setTitle(statusMethod.description, for: .normal)
                self?.methodCalculationButton.accessibilityLabel = "\(NSLocalizedString("based_on_telltales", bundle: weakSelf.bundle, comment: "")) \(statusMethod.description)"
                UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: sender)
            })
            methodMenu.addAction(methodAction)
        })
        
        // Adding cancel action to go back
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", bundle: bundle, comment: ""), style: .cancel, handler: {_ in
            UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: sender)
        })
        cancelAction.setValue(UIColor.red, forKey: "titleTextColor")
        methodMenu.addAction(cancelAction)
        
        // Present action sheet
        present(methodMenu, animated: true, completion: nil)
    }
    
    // MARK: - Setting values helpers
    
    private func setMedianSamplingTime(_ time: Int) {
        guard time >= TellTale.medianSamplingTimeMin else { return }
        guard time <= TellTale.medianSamplingTimeMax else { return }
        self.medianSamplingTimeValueLabel.text = "\(time)s"
        self.medianSamplingTimeSlider.setValue(Float(time), animated: false)
        self.medianSamplingTimeSlider.accessibilityValue = "\(time)\(NSLocalizedString("unit_seconds", bundle: bundle, comment: "")))"
        self.medianSamplingTimeLabel.accessibilityLabel = "\(NSLocalizedString("median_sampling_time", bundle: bundle, comment: "")) \(time)\(NSLocalizedString("unit_seconds", bundle: bundle, comment: "")))"
        self.settings!.medianSamplingTime = time
    }
    
    private func setAttachedThresholdValue(_ value: Int, discardWrongValues: Bool = true) {
        if discardWrongValues {
            if value < TellTale.attachedThresholdMin ||
                value > TellTale.attachedThresholdMax ||
                value >= settings!.turbulentThreshold {
                self.tellTaleThresholdAttachedSlider.setValue(Float(settings!.attachedThreshold), animated: false)
                return
            }
        }
        self.tellTaleThresholdAttachedValueLabel.text = "\(value)°"
        self.settings!.attachedThreshold = value
        self.tellTaleThresholdAttachedSlider.setValue(Float(value), animated: false)
        self.tellTaleThresholdAttachedLabel.accessibilityLabel = "\(NSLocalizedString("telltales_attached_threshold", bundle: bundle, comment: "")) \(value)°"
        self.tellTaleThresholdAttachedSlider.accessibilityValue = "\(value))°"
    }
    
    private func setTurbulentThresholdValue(_ value: Int, discardWrongValues: Bool = true) {
        if discardWrongValues {
            if value < TellTale.attachedThresholdMin ||
                value > TellTale.attachedThresholdMax ||
                value <= settings!.attachedThreshold {
                self.tellTaleThresholdTurbulentSlider.setValue(Float(settings!.turbulentThreshold), animated: false)
                return
            }
        }
        self.tellTaleThresholdTurbulentValueLabel.text = "\(value)°"
        self.tellTaleThresholdTurbulentSlider.setValue(Float(value), animated: false)
        self.tellTaleThresholdTurbulentLabel.accessibilityLabel = "\(NSLocalizedString("telltales_turbulent_threshold", bundle: bundle, comment: "")) \(value))°"
        self.tellTaleThresholdTurbulentSlider.accessibilityValue = "\(value))°"
        self.settings!.turbulentThreshold = value
    }
    
    // MARK: - Text field delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: textField)
        return true
    }
    
    // MARK: - View controller overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let tellTale = self.tellTale else {
            self.view.isUserInteractionEnabled = false
            return
        }
        
        // Configuring views
        medianSamplingTimeView.layer.borderWidth = 1
        medianSamplingTimeView.layer.borderColor = Utils.secondaryColor.cgColor
        medianSamplingTimeView.layer.cornerRadius = 5
        statusThresholdView.layer.borderWidth = 1
        statusThresholdView.layer.borderColor = Utils.secondaryColor.cgColor
        statusThresholdView.layer.cornerRadius = 5
        angleComputationSettingsView.layer.borderWidth = 1
        angleComputationSettingsView.layer.borderColor = Utils.secondaryColor.cgColor
        angleComputationSettingsView.layer.cornerRadius = 5
        
        self.settings = TelltaleSettings(tellTale: tellTale)
        
        saveButton.isEnabled = !tellTale.name.isEmpty
        
        // Text field
        //self.navigationItem.title = tellTale.name
        self.tellTaleIdLabel.text = tellTale.id
        if tellTale.name.isEmpty {
            nameTextField.placeholder = NSLocalizedString("telltale_name", bundle: bundle, comment: "")
        }
        else {
            nameTextField.text = tellTale.name
        }
        if tellTale.idName.isNotEmpty {
            self.subNameTextField.text = tellTale.idName
        }
        self.basedOnMethodLabel.text = NSLocalizedString("based_on_telltales", bundle: bundle, comment: "")
        self.basedOnMethodLabel.isAccessibilityElement = false
        self.methodCalculationButton.customizePrimary()
        let methodCalculation = self.tellTale != nil ? self.tellTale!.statusMethodCal().description : TellTale.StatusMethod.mediane.description
        self.methodCalculationButton.setTitle(methodCalculation, for: .normal)
        self.methodCalculationButton.accessibilityLabel = "\(NSLocalizedString("based_on_telltales", bundle: bundle, comment: "")) \(methodCalculation)"
        
        // Median sampling time view
        self.medianSamplingTimeLabel.text = NSLocalizedString("median_sampling_time", bundle: bundle, comment: "")
        self.medianSamplingTimeSlider.minimumValue = 3
        self.medianSamplingTimeSlider.maximumValue = 30
        self.medianSamplingTimeSlider.tintColor = Utils.primaryColor
        self.setMedianSamplingTime(tellTale.medianSamplingTime)
        setupIncrementButton(self.decreaseMedianSamplingTimeButton)
        setupIncrementButton(self.increaseMedianSamplingTimeButton)
        
        // Telltales status thresholds
        self.thresholdAutoSettingButton.customizePrimary()
        
        self.tellTaleThresholdAttachedValueLabel.text = "\(tellTale.attachedThreshold)°"
        self.tellTaleThresholdAttachedSlider.minimumValue = Float(TellTale.attachedThresholdMin)
        self.tellTaleThresholdAttachedSlider.maximumValue = Float(TellTale.attachedThresholdMax)
        self.tellTaleThresholdAttachedSlider.tintColor = Utils.primaryColor
        self.setAttachedThresholdValue(tellTale.attachedThreshold)
        self.setupIncrementButton(self.tellTaleThresholdAttachedDecreaseButton)
        self.setupIncrementButton(self.tellTaleThresholdAttachedIncreaseButton)
        
        self.tellTaleThresholdTurbulentValueLabel.text = "\(tellTale.turbulentThreshold)°"
        self.tellTaleThresholdTurbulentSlider.minimumValue = Float(TellTale.turbulentThresholdMin)
        self.tellTaleThresholdTurbulentSlider.maximumValue = Float(TellTale.turbulentThresholdMax)
        self.tellTaleThresholdTurbulentSlider.tintColor = Utils.primaryColor
        self.setTurbulentThresholdValue(tellTale.turbulentThreshold)
        
        self.setupIncrementButton(self.tellTaleThresholdTurbulentDecreaseButton)
        self.setupIncrementButton(self.tellTaleThresholdTurbulentIncreaseButton)
        
        // Angle computation
        self.slopeTextField.text = "\(tellTale.angleComputationSlope)"
        self.yInterceptTextField.text = "\(tellTale.angleComputationYIntercept)"
        
        // Delete button
        deleteTelltaleButton.customizeSecondary()
        deleteTelltaleButton.setTitle(NSLocalizedString("delete", bundle: bundle, comment: ""), for: .normal)
        deleteTelltaleButton.setTitleColor(#colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1), for: .normal)
        deleteTelltaleButton.isHidden = !tellTale.isConfigured
        
        // Localize and make accessible
        self.localizationAndAccessibility()
        
        // Keyboard handling
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil) { (note) in
            let kbHeight = (note.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size.height
            self.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: kbHeight, right: 0)
        }
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil) { (note) in
            self.scrollView.contentInset = UIEdgeInsets.zero
        }
        // Close keyboard when tap on the view
        let tapGesture = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.scrollView.contentSize = self.containerView.bounds.size
    }
    
    private func setupIncrementButton(_ button: UIButton) {
        button.layer.cornerRadius = 0.5 * button.frame.size.height
        button.layer.masksToBounds = true
        button.layer.borderWidth = 1
        button.layer.borderColor = Utils.secondaryColor.cgColor
        button.tintColor = Utils.primaryColor
    }
    
    private func localizationAndAccessibility() {
        self.nameLabel.accessibilityTraits = .header
        self.nameLabel.text = NSLocalizedString("name", bundle: bundle, comment: "")
        self.nameLabel.accessibilityLabel = NSLocalizedString("telltale_name", bundle: bundle, comment: "")
        self.subNameLabel.text = NSLocalizedString("telltale_id_name", bundle: bundle, comment: "")
        
        self.tellTaleIdNameLabel.isAccessibilityElement = false
        self.tellTaleIdLabel.accessibilityLabel = "\(NSLocalizedString("telltale_id", bundle: bundle, comment: "")) \(self.tellTaleIdLabel.text!))"

        self.medianSamplingTimeLabel.accessibilityTraits = .header
        self.medianSamplingTimeLabel.text = NSLocalizedString("median_sampling_time", bundle: bundle, comment: "")
        self.medianSamplingTimeLabel.accessibilityLabel = "\(NSLocalizedString("median_sampling_time", bundle: bundle, comment: "")) \(Int(medianSamplingTimeSlider.value))\(NSLocalizedString("unit_seconds", bundle: bundle, comment: "")))"
        self.decreaseMedianSamplingTimeButton.accessibilityLabel = NSLocalizedString("minus", bundle: bundle, comment: "")
        self.medianSamplingTimeSlider.accessibilityValue = "\(Int(medianSamplingTimeSlider.value))\(NSLocalizedString("unit_seconds", bundle: bundle, comment: "")))"
        
        self.tellTaleThresholdsLabel.accessibilityTraits = .header
        self.tellTaleThresholdsLabel.text = NSLocalizedString("telltales_status_thresholds", bundle: bundle, comment: "")
        self.thresholdAutoSettingButton.accessibilityLabel = NSLocalizedString("telltales_automatic_config", bundle: bundle, comment: "")
        
        // Attached telltale
        self.tellTaleThresholdAttachedLabel.text = NSLocalizedString("telltales_attached_threshold", bundle: bundle, comment: "")
        self.tellTaleThresholdAttachedLabel.accessibilityLabel = "\(NSLocalizedString("telltales_attached_threshold", bundle: bundle, comment: "")) \(Int(tellTaleThresholdAttachedSlider.value))°"
        self.tellTaleThresholdAttachedSlider.accessibilityValue = "\(Int(tellTaleThresholdAttachedSlider.value))°"
        self.tellTaleThresholdAttachedDecreaseButton.accessibilityLabel = NSLocalizedString("minus", bundle: bundle, comment: "")
        // Turbulent telltale
        self.tellTaleThresholdTurbulentLabel.text = NSLocalizedString("telltales_turbulent_threshold", bundle: bundle, comment: "")
        self.tellTaleThresholdTurbulentLabel.accessibilityLabel = "\(NSLocalizedString("telltales_turbulent_threshold", comment: "")) \(Int(tellTaleThresholdTurbulentSlider.value))°"
        self.tellTaleThresholdTurbulentSlider.accessibilityValue = "\(Int(tellTaleThresholdTurbulentSlider.value))°"
        self.tellTaleThresholdTurbulentDecreaseButton.accessibilityLabel = NSLocalizedString("minus", bundle: bundle, comment: "")
        
        self.angleComputationLabel.accessibilityTraits = .header
        self.angleComputationLabel.text = NSLocalizedString("telltales_angle_computation", bundle: bundle, comment: "")
        self.slopeLabel.text = NSLocalizedString("slope", bundle: bundle, comment: "")
        self.yInterceptLabel.text = NSLocalizedString("yintercept", bundle: bundle, comment: "")
        
        //Delete button
        deleteTelltaleButton.accessibilityTraits = .header
        deleteTelltaleButton.accessibilityLabel = NSLocalizedString("delete", bundle: bundle, comment: "")
    }
    
}
