//
//  ReleverViewController.swift
//  SARA Croisiere
//
//  Created by Rose on 11/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

public class PlotViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet private weak var headerLabel: UILabel!
    @IBOutlet private weak var tableview: UITableView!
    @IBOutlet private weak var plotLabel: UILabel!
    
    @IBOutlet private weak var createPoint: UIButton!
    @IBOutlet private weak var refreshButton: UIButton!
    
    var viewModel = PlotPointsViewModel()
    
    let disposeBag = DisposeBag()
    let bundle = Utils.bundle(anyClass: PlotViewController.classForCoder())
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    private func setupUI() {
        self.title = NSLocalizedString("plot_title", bundle: bundle, comment: "")
        self.plotLabel.textColor = Utils.primaryColor
        self.headerLabel.textColor = Utils.primaryColor
        self.headerLabel.accessibilityTraits = UIAccessibilityTraits.header
        self.headerLabel.text = NSLocalizedString("points_list", bundle: bundle, comment: "")
        
        self.refreshButton.customizeSecondary()
        self.refreshButton.setTitle(NSLocalizedString("new_plot_title", bundle: bundle, comment: ""), for: .normal)
        self.refreshButton.accessibilityLabel = NSLocalizedString("new_plot_title", bundle: bundle, comment: "")
        self.refreshButton.accessibilityTraits = UIAccessibilityTraits.header
        
        self.createPoint.customizePrimary()
        self.createPoint.setTitle(NSLocalizedString("create_point_title", bundle: bundle, comment: ""), for: .normal)
        self.createPoint.accessibilityLabel = NSLocalizedString("create_point_title", bundle: bundle, comment: "")
        self.createPoint.accessibilityTraits = UIAccessibilityTraits.header
        
        let barButtonItem = UIBarButtonItem(title: NSLocalizedString("ok", bundle: bundle, comment: ""), style: .done, target: self, action: #selector(savePosition))
        barButtonItem.accessibilityHint = NSLocalizedString("plot_confirmation_accessibility_label", bundle: bundle, comment: "")
        barButtonItem.isEnabled = false
        self.navigationItem.setRightBarButton(barButtonItem, animated: true)
        self.tableview.customize()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.location = LocationManager.shared.getCurrentPosition()
        guard let safeLocation = self.viewModel.location else {
            return
        }
        
        self.plotLabel.text = String(format: "(%.4f, %.4f)", safeLocation.coordinate.latitude, safeLocation.coordinate.longitude)
        
        self.plotLabel.accessibilityLabel = String(format: "\(NSLocalizedString("latitude", bundle: bundle, comment: "")): %.4f, \(NSLocalizedString("longitude", bundle: bundle, comment: "")): %.4f)", safeLocation.coordinate.latitude, safeLocation.coordinate.longitude)
        
        tableview.reloadData()
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CoursePoint.numberOfPoints()
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "plotCellIdentifier") else {
            return UITableViewCell()
        }
        
        cell.accessoryType = .none
        cell.textLabel?.text = CoursePoint.getPointNameAtIndex(indexPath)
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableview.cellForRow(at: indexPath)
        cell?.accessoryType = .checkmark
        self.navigationItem.rightBarButtonItem?.isEnabled = true
    }
    
    public func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableview.cellForRow(at: indexPath)
        cell?.accessoryType = .none
        if self.tableview.indexPathsForSelectedRows == nil {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }
    
    @objc
    func savePosition() {
        if let selectedPaths = self.tableview.indexPathsForSelectedRows,
            let location = self.viewModel.location {
            for path in selectedPaths {
                self.viewModel.updateLocation(at: path, with: location)
                self.tableview.deselectRow(at: path, animated: true)
                tableView(self.tableview, didDeselectRowAt: path)
            }
            
            let alertController = UIAlertController(title: NSLocalizedString("done_plot", bundle: bundle, comment: ""), message: NSLocalizedString("successful_plot_modifications", bundle: bundle, comment: ""), preferredStyle: .alert)
            let alertAction = UIAlertAction(title: NSLocalizedString("ok", bundle: bundle, comment: ""), style: .default, handler: nil)
            alertController.addAction(alertAction)
            present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func refreshLocation(_ sender: Any?) {
        self.viewModel.location = LocationManager.shared.getCurrentPosition()
        guard let safeLocation = self.viewModel.location else {
            return
        }
        
        // Update label and accessibility label with the new coordinates
        self.plotLabel.text = String(format: "(%.4f, %.4f)", safeLocation.coordinate.latitude, safeLocation.coordinate.longitude)
        self.plotLabel.accessibilityLabel = String(format: "\(NSLocalizedString("latitude", bundle: bundle, comment: "")): %.4f, \(NSLocalizedString("longitude", bundle: bundle, comment: "")): %.4f)", safeLocation.coordinate.latitude, safeLocation.coordinate.longitude)
        
        // Wait a few seconds to send the notification, to prevent cutting the label speaking
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when) {
            UIAccessibility.post(notification: UIAccessibility.Notification.announcement, argument: NSLocalizedString("successful_plot", bundle: self.bundle, comment: ""))
        }
        
    }
    
    @IBAction func createPoint(_ sender: Any?) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "CreatePointViewController") as? CreatePointViewController,
            let location = self.viewModel.location else {
                return
        }
        // Set a fake point without a name to fill the view with current coordinates
        let gpsPos = GPSPosition(latitude: Float(location.coordinate.latitude), longitude: Float(location.coordinate.longitude))
        let fakePoint = CoursePoint(name: "", gpsPosition: gpsPos)
        controller.setPoint(fakePoint)
        self.present(controller, animated: true, completion: nil)
    }
}
