//
//  CartoViewController.swift
//  SARA Croisiere
//
//  Created by Marine IL on 20.03.19.
//  Copyright © 2019 GraniteApps. All rights reserved.
//

import UIKit
import MapKit
import RxSwift

public class TracerViewController: UIViewController, MKMapViewDelegate {

    // MARK:  - IBOutlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var recordTraceRace: AccessibilitySwitchView!
    @IBOutlet weak var recordTraceLabel: UILabel!
    @IBOutlet weak var traceManagerView: UIView!
    @IBOutlet weak var startStopButton: UIButton!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var elapsedTimeValue: UILabel!
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: TracerViewController.classForCoder())
    var course: Course?
    let disposeBag = DisposeBag()
    var courseSegmentCoord = [CLLocationCoordinate2D]()
    var annotationView: MKAnnotationView?
    var timer = Timer()
    var elapsedTime = 0
    var previousLocMap: CLLocation?
    public var isSARARace = false
    
    // MARK: - IBActions
    @IBAction func startStop(_ sender: Any) {
        if TraceManager.shared.recordingStatus == .idle { // Start recording
            self.mapView.removeOverlays(self.mapView.overlays.filter({$0.title == "trace"}))
            self.previousLocMap = LocationManager.shared.getCurrentPosition() // reset previous location with current position
            TraceManager.shared.startManager()
            TraceManager.shared.recordingStatus = .play
            runTimer()
        } else { // Stop recording
            stopRecording()
        }
        traceControl()
    }
    
    @IBAction func playPause(_ sender: Any) {
        if TraceManager.shared.recordingStatus == .play { // pause the recording
            TraceManager.shared.recordingStatus = .pause
            timer.invalidate()
        } else { // Resume recording
            TraceManager.shared.recordingStatus = .play
            runTimer()
        }
        traceControl()
    }
    
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        if isSARARace {
            self.recordTraceLabel.isHidden = true
            self.traceManagerView.isHidden = true
            self.recordTraceRace.titleLabel.text = NSLocalizedString("track_recording_label", bundle: bundle, comment: "")
            self.recordTraceRace.titleLabel.textAlignment = .left
            self.recordTraceRace.settingSwitch.isOn = Settings.shared.saveTrace
//            self.recordTraceRace.settingSwitch.tintColor = Utils.secondaryColor
            self.recordTraceRace.settingSwitch.onTintColor = Utils.secondaryColor
            self.recordTraceRace.settingSwitch.rx.value.asObservable()
                .bind(onNext: { value in
                    Settings.shared.setSaveTrace(value: value)
                })
                .disposed(by: disposeBag)
        } else {
            self.recordTraceRace.isHidden = true
            recordTraceLabel.text = NSLocalizedString("track_recording_label", bundle: bundle, comment: "")
            elapsedTimeValue.accessibilityLabel = "\(NSLocalizedString("duration", bundle: bundle, comment: "")) \(elapsedTimeValue.text!)"
            traceManagerView.customizeView()
            traceControl()
            
            self.startStopButton.tintColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            if #available(iOS 13.0, *) {
                self.playPauseButton.tintColor = UIColor.label
            } else {
                // Fallback on earlier versions
                self.playPauseButton.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
        }
        
        self.mapView.showsUserLocation = Settings.shared.usePhoneGPS

        // Do any additional setup after loading the view.
        NavigationManager.shared.route
            .asObservable()
            .subscribe(onNext: { [weak self] (route) in
                guard self != nil else { return }
                self!.mapView.removeAnnotations(self!.mapView.annotations.filter({$0 is MapAnnotation && (($0 as! MapAnnotation).type == .coursePoint || ($0 as! MapAnnotation).type == .mark)}))
                let overlaysToRemove = self!.mapView.overlays.filter({ ($0.title == "bisector" || $0.title == "crossingLineMark" || $0.title == "distanceThreshold" || $0.title == "course")})
                self!.mapView.removeOverlays(overlaysToRemove)
                self!.courseSegmentCoord = [] // empty the array for the segment
                if route.value != nil {
                    self!.course = route.value
                    if let _ = self!.course! as? CruiseCourse { // Display all the points and beacons in SARA Navigation
                        MapHelper.getAllMapAnnotations(map: self!.mapView, pointsDisplayed: self!.course?.points.map({$0.point}) ?? [])
                    }
                    let annotations = self!.getMapAnnotations()
                    self!.mapView.addAnnotations(annotations)

                    //Zoom to fit the route
                    var annotationsToZoom: [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
                    for annotation in annotations {
                        annotationsToZoom.append(annotation.coordinate)
                    }
                    if let userLocation = LocationManager.shared.locationVariable.value {
                        annotationsToZoom.append(userLocation.coordinate)
                    }
                    let polylineZoom = MKPolyline(coordinates: annotationsToZoom, count: annotationsToZoom.count)
                    MapHelper.zoomToPolyLine(map: self!.mapView, polyLine: polylineZoom, animated: false)

                    // Connect all the mappoints using Poly line.
                    if (self!.course.value as? CruiseCourse) != nil {
                        // Connect all the mappoints using Poly line.
                        let polyline = MKPolyline(coordinates: self!.courseSegmentCoord, count: self!.courseSegmentCoord.count)
                        polyline.title = "course"
                        self!.mapView.addOverlay(polyline)
                    }
                } else {
                    if !self!.isSARARace {
                        MapHelper.getAllMapAnnotationsThreadSafe(map: self!.mapView, pointsDisplayed: [])
                    }
                }
            }).disposed(by: disposeBag)
        
        LocationManager.shared.locationVariable
            .withPrevious()
            .subscribe(onNext: { [weak self] (previousLocation, newLocation) in
                guard newLocation != nil && self != nil else {
                    self!.mapView.removeAnnotations(self!.mapView.annotations.filter({$0 is MapAnnotation && ($0 as! MapAnnotation).type == .userLocation}))
                    return
                }
                if self!.previousLocMap == nil {
                    self!.previousLocMap = newLocation
                }
                if let previousPos = previousLocation, !Utils.equals(location1: newLocation, location2: previousPos) {
                    if TraceManager.shared.recordingStatus == .play, previousPos != nil, self!.previousLocMap != nil {
                        let distance = newLocation!.distance(from: self!.previousLocMap!)
                        if distance > Double(Settings.shared.traceSensitivity) {
                            let co = [self!.previousLocMap!.coordinate, newLocation!.coordinate]
                            let polylinePos = MKPolyline(coordinates: co, count: co.count)
                            polylinePos.title = "trace"
                            self!.mapView.addOverlay(polylinePos)
//                            let circle = MKCircle(center: newLocation!.coordinate, radius: CLLocationDistance(1))
//                            circle.title = "trace"
//                            self!.mapView.addOverlay(circle)
                            self!.previousLocMap = newLocation
                        }
                    }
                    // Add user position on map
                    MapHelper.userPosition(mapView: self!.mapView, newLocation: newLocation!)
                    if Settings.shared.usePhoneGPS && !Settings.shared.developerMode {
                        if self?.annotationView != nil { // rotate image
                            self?.annotationView!.transform = CGAffineTransform(rotationAngle: CGFloat(NavigationManager.shared.courseOverGround.value ?? 0) * .pi / 180)
                        }
                    }
                } else if previousLocation == nil {
                    // Add user position on map
                    MapHelper.userPosition(mapView: self!.mapView, newLocation: newLocation!)
                    if Settings.shared.usePhoneGPS && !Settings.shared.developerMode {
                        if self?.annotationView != nil { // rotate image
                            self?.annotationView!.transform = CGAffineTransform(rotationAngle: CGFloat(NavigationManager.shared.courseOverGround.value ?? 0) * .pi / 180)
                        }
                    }
                }
            }).disposed(by: disposeBag)
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        NavigationManager.shared.point  // Each time point change
            .asObservable()
            .subscribe(onNext: { [weak self] (anyPoint) in
                guard let safeSelf = self else { return }
                var overlayToRemove: [MKOverlay] = []
                if (NavigationManager.shared.route.value as? RaceCourse) != nil { // For SARA Race, remove old bisector, crossing line, U lines or course axis.
                    overlayToRemove = safeSelf.mapView.overlays.filter({ ($0.title == "bisector" || $0.title == "crossingLineMark" || $0.title == "U" || $0.title == "course")})
                    // Add course axis
                    if let index = NavigationManager.shared.currentPointIndex.value, index > 0 {
                        let arrayCoord = Array(safeSelf.courseSegmentCoord[index-1...index])
                        let polyline = MKPolyline(coordinates: arrayCoord, count: arrayCoord.count)
                        polyline.title = "course"
                        safeSelf.mapView.addOverlay(polyline)
                    }
                } else { // For SARA Nav, remove old bisector, crossing line, radius for distance Threshold.
                    overlayToRemove = safeSelf.mapView.overlays.filter({ ($0.title == "bisector" || $0.title == "crossingLineMark" || $0.title == "distanceThreshold" )})
                }
                safeSelf.mapView.removeOverlays(overlayToRemove)
                if let coursePoint = anyPoint?.point as? CoursePoint, let gpsPosition = coursePoint.gpsPosition {
                    let annotation = MapAnnotation(latitude: Double(gpsPosition.latitude), longitude: Double(gpsPosition.longitude), type: .coursePoint)
                    let circle = MKCircle(center: annotation.coordinate, radius: CLLocationDistance(coursePoint.distanceThreshold / 2))
                    circle.title = "distanceThreshold"
                    safeSelf.mapView.addOverlay(circle)
                } else if let mark = anyPoint?.point as? Mark { // Bissector
                    var coordMarks: [CLLocationCoordinate2D] = []
                    var bissectriceMarks: [CLLocationCoordinate2D] = []
                    mark.buoys.forEach({
                        if let gpsPosition = $0.gpsPosition {
                            let annotation = MapAnnotation(latitude: Double(gpsPosition.latitude), longitude: Double(gpsPosition.longitude), type: .mark)
                            annotation.title = "\(mark.name) - \(NSLocalizedString("unit_" + $0.toLetAt(inversed: (NavigationManager.shared.route.value as? CruiseCourse)?.reversed), bundle: safeSelf.bundle, comment: ""))"
                            coordMarks.append(annotation.coordinate)
                            if mark.markType == Mark.MarkType.simple.rawValue {
                                bissectriceMarks.append(annotation.coordinate)
                                if let bissectrice = CourseManager.shared.validationMarkData?.validationLineCoord {
                                    bissectriceMarks.append(bissectrice)
                                }
                                let polylineBissectrice = MKPolyline(coordinates: bissectriceMarks, count: bissectriceMarks.count)
                                polylineBissectrice.title = "bisector"
                                safeSelf.mapView.addOverlay(polylineBissectrice)
                            }
                        }
                    })
                    if coordMarks.count > 1 { // Crossing line: double, start, finish line
                        let polylineMark = MKPolyline(coordinates: coordMarks, count: coordMarks.count)
                        polylineMark.title = "crossingLineMark"
                        safeSelf.mapView.addOverlay(polylineMark)
                        // If it is a match race and start display U
                        if safeSelf.course!.isMatchRace() && mark.markType == Mark.MarkType.start.rawValue {
                            if let pinCoord = coordMarks.first, let gpsPosPin = CourseManager.shared.gpsPosUPin {
                                let pinULine = MKPolyline(coordinates: [pinCoord, CLLocationCoordinate2D(latitude: CLLocationDegrees(gpsPosPin.latitude), longitude: CLLocationDegrees(gpsPosPin.longitude))], count: 2)
                                pinULine.title = "U"
                                safeSelf.mapView.addOverlay(pinULine)
                            }
                            if let comCoord = coordMarks.last, let gpsPosCom = CourseManager.shared.gpsPosUCommitee {
                                let comULine = MKPolyline(coordinates: [comCoord, CLLocationCoordinate2D(latitude: CLLocationDegrees(gpsPosCom.latitude), longitude: CLLocationDegrees(gpsPosCom.longitude))], count: 2)
                                comULine.title = "U"
                                safeSelf.mapView.addOverlay(comULine)
                            }
                        }
                    }
                }
            }).disposed(by: disposeBag)
        
        if let loc = LocationManager.shared.getCurrentPosition() {
            MapHelper.centerMapOnLocation(map: self.mapView, location: loc)
        }
        
        NavigationManager.shared.hasStarted.asObservable()
            .withPrevious()
            .subscribe(onNext: { (previousValue, newValue) in
                if Settings.shared.saveTrace && (NavigationManager.shared.route.value == nil || previousValue != nil && previousValue! != newValue && newValue) { // Remove trace layer if new start or disable course
                    let overlayToRemove = self.mapView.overlays.filter({ $0.title == "trace" })
                    self.mapView.removeOverlays(overlayToRemove)
                }
            }).disposed(by: disposeBag)
    }
    
    //MARK: - Annotations
    
    func getMapAnnotations() -> [MapAnnotation] {
        if (NavigationManager.shared.route.value as? CruiseCourse) != nil { // For SARA Race we don't want the first segment
            if let userPosition = NavigationManager.shared.locationStartCourse {
                courseSegmentCoord.append(userPosition.coordinate) // To trace the first segment of the course between the user position when the course is activated and the first point
            } else if let userPosition = LocationManager.shared.getCurrentPosition() {
                courseSegmentCoord.append(userPosition.coordinate) // To trace the first segment of the course between the user position when the course is activated and the first point
            }
        }
        
        var annotations = [MapAnnotation]()
        //iterate and create annotations
        if let items = course?.points {
            for anyPoint in items {
                if let coursePoint = anyPoint.point as? CoursePoint, let gpsPosition = coursePoint.gpsPosition {
                    let annotation = MapAnnotation(latitude: Double(gpsPosition.latitude), longitude: Double(gpsPosition.longitude), type: .coursePoint, isCurrent: true)
                    annotation.title = "\(coursePoint.name)"
                    annotations.append(annotation)
                    courseSegmentCoord.append(annotation.coordinate) // To trace the segment
                } else if let mark = anyPoint.point as? Mark {
                    mark.buoys.forEach({
                        if let gpsPosition = $0.gpsPosition {
                            let annotation = MapAnnotation(latitude: Double(gpsPosition.latitude), longitude: Double(gpsPosition.longitude), type: .mark, isCurrent: true)
                            annotation.title = "\(mark.name) - \(NSLocalizedString("unit_" + $0.toLetAt(inversed: (NavigationManager.shared.route.value as? CruiseCourse)?.reversed), bundle: bundle, comment: ""))"
                            annotations.append(annotation)
                        }
                    })
                    if let gpsPos2 = mark.getCoordinates(method: .middle) {
                        courseSegmentCoord.append(CLLocationCoordinate2D(latitude: Double(gpsPos2.latitude), longitude: Double(gpsPos2.longitude))) // To trace the segment
                    }
                    
                }
            }
        }
        return annotations
    }

    //MARK: - MapViewDelegate methods
    public func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        return MapHelper.overlayRenderer(overlay: overlay)
    }

    public func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let annotationView = MapHelper.annotationView(mapView: mapView, annotation: annotation)
        if annotation is MKUserLocation {
            self.annotationView = annotationView
        }
        return annotationView
    }
    
    // MARK: - Functions
    func traceControl() {
        switch TraceManager.shared.recordingStatus {
        case .idle:
            startStopButton.setImage(UIImage(named: "start-trace", in: bundle, compatibleWith: nil), for: .normal)
            startStopButton.accessibilityLabel = NSLocalizedString("start_track", bundle: bundle, comment: "")
            playPauseButton.accessibilityLabel = NSLocalizedString("pause_track", bundle: bundle, comment: "")
            playPauseButton.isEnabled = false
            elapsedTimeValue.isEnabled = false
//            playPauseButton.isHidden = true
//            elapsedTimeValue.isHidden = true
        case .play:
            startStopButton.setImage(UIImage(named: "stop-trace", in: bundle, compatibleWith: nil), for: .normal)
            playPauseButton.setImage(UIImage(named: "pause-trace", in: bundle, compatibleWith: nil), for: .normal)
            startStopButton.accessibilityLabel = NSLocalizedString("stop_track", bundle: bundle, comment: "")
            playPauseButton.accessibilityLabel = NSLocalizedString("pause_track", bundle: bundle, comment: "")
            playPauseButton.isEnabled = true
            elapsedTimeValue.isEnabled = true
//            playPauseButton.isHidden = false
//            elapsedTimeValue.isHidden = false
        case .pause:
            startStopButton.setImage(UIImage(named: "stop-trace", in: bundle, compatibleWith: nil), for: .normal)
            playPauseButton.setImage(UIImage(named: "play-trace", in: bundle, compatibleWith: nil), for: .normal)
            startStopButton.accessibilityLabel = NSLocalizedString("stop_track", bundle: bundle, comment: "")
            playPauseButton.accessibilityLabel = NSLocalizedString("resume_track", bundle: bundle, comment: "")
            playPauseButton.isEnabled = true
            elapsedTimeValue.isEnabled = true
//            playPauseButton.isHidden = false
//            elapsedTimeValue.isHidden = false
        }
    }
    
    @objc func updateDuration() {
        elapsedTime = elapsedTime + 1
        elapsedTimeValue.text = timeString(time: TimeInterval(elapsedTime))
        elapsedTimeValue.accessibilityLabel = "\(NSLocalizedString("duration", bundle: bundle, comment: "")) \(elapsedTimeValue.text!)"
    }
    
    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
    
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateDuration)), userInfo: nil, repeats: true)
    }
    
    func stopRecording() {
        TraceManager.shared.recordingStatus = .idle
        TraceManager.shared.stopManager()
        timer.invalidate()
        elapsedTime = 0
        elapsedTimeValue.text = timeString(time: TimeInterval(0))
        elapsedTimeValue.accessibilityLabel = "\(NSLocalizedString("duration", bundle: bundle, comment: "")) \(elapsedTimeValue.text!)"
    }
}
