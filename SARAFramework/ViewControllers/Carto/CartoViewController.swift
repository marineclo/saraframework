//
//  CartoViewController.swift
//  SARAFramework
//
//  Created by Marine on 16.07.21.
//

import UIKit

class CartoViewController: UIViewController, TabsMenuViewControllerDelegate, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var pageViewContainer: UIView!
    @IBOutlet weak var tabsMenuContainer: UIView!
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: CartoViewController.classForCoder())
    private var pageViewController: UIPageViewController?
    private var tabsMenuController: TabsMenuViewController?
    private var pagesViewControllers: [UIViewController] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationBar.topItem?.title = NSLocalizedString("menu_carto", bundle: bundle, comment: "")
        
        // Tabs menu items
        let tabsMenuItems = [
            NSLocalizedString("menu_plotter", bundle: bundle, comment: ""),
            NSLocalizedString("menu_list", bundle: bundle, comment: ""),
        ]

        // Tabs menu
        self.tabsMenuController = TabsMenuViewController()
        self.addChild(tabsMenuController!)
        tabsMenuContainer.addSubview(tabsMenuController!.view)
        self.tabsMenuController!.view.frame = CGRect(x: 0, y: 0, width: self.tabsMenuContainer.frame.size.width, height: self.tabsMenuContainer.frame.size.height)
        self.tabsMenuController!.menuItems = tabsMenuItems
        self.tabsMenuController!.accessibilityMenuItems = Utils.accessiblePageMenuItems(originalMenuItems: tabsMenuItems, selectedItemIndex: 0)
        self.tabsMenuController!.visibleItemsCount = tabsMenuItems.count
        self.tabsMenuController!.selectedItemIndex = 0
        self.tabsMenuController!.delegate = self
        if #available(iOS 13.0, *) {
            if self.traitCollection.userInterfaceStyle == .dark {
                self.tabsMenuController!.menuItemsColor = UIColor.label
            } else {
                self.tabsMenuController!.menuItemsColor = Utils.primaryColor
            }
            self.tabsMenuController!.menuBackgroundColor = UIColor.systemBackground
        } else {
            // Fallback on earlier versions
            self.tabsMenuController!.menuBackgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            self.tabsMenuController!.menuItemsColor = Utils.primaryColor
        }
        
        // Embedded view controllers
        let storyboard = UIStoryboard(name: "Carto", bundle: Bundle(for: CartoViewController.classForCoder()))

        // Tracer
        if let tracerViewController = storyboard.instantiateViewController(withIdentifier: "TracerViewController") as? TracerViewController {
            pagesViewControllers.append(tracerViewController)
        }
        
        // List
        if let listTableViewController = storyboard.instantiateViewController(withIdentifier: "ListTableViewController") as? ListTableViewController {
            pagesViewControllers.append(listTableViewController)
        }
        // By default the refresh button is hidden
        enableRefreshButton(vc: pagesViewControllers.first!)
        
        self.pageViewController?.setViewControllers([pagesViewControllers.first!], direction: .forward, animated: false, completion: nil)
        self.pageViewController?.dataSource = self
        self.pageViewController?.delegate = self
    }
    
    func enableRefreshButton(vc: UIViewController) {
        if vc is ListTableViewController {
            let barButtonItem = UIBarButtonItem(title: NSLocalizedString("refresh", bundle: bundle, comment: ""), style: .done, target: self, action: #selector(refreshAction))
            barButtonItem.accessibilityLabel = NSLocalizedString("refresh", bundle: bundle, comment: "")
            self.navigationBar.items?.first?.rightBarButtonItem = barButtonItem
            self.navigationBar.items?.first?.rightBarButtonItem?.image = UIImage(named: "update", in: bundle, compatibleWith: nil)
            self.navigationBar.items?.first?.rightBarButtonItem?.tintColor = Utils.tintIcon
        } else {
            self.navigationBar.items?.first?.rightBarButtonItem = nil
        }
    }
    
    // MARK: - TabsMenuViewControllerDelegate
    func tabsMenuViewController(_ controller: TabsMenuViewController, didSelectMenuAtIndex index: Int) {
        guard index < pagesViewControllers.count else {
            return
        }
        
        var direction: UIPageViewController.NavigationDirection = .forward
        if let currentVC = pageViewController?.viewControllers?.first {
            if let currentVCIndex = pagesViewControllers.firstIndex(of: currentVC) {
                if currentVCIndex > index {
                    direction = .reverse
                }
            }
        }
        
        // Updating accessibility label
        controller.accessibilityMenuItems?[index] = NSLocalizedString("selected", bundle: bundle, comment: "") + " " + NSLocalizedString("menu", bundle: bundle, comment: "") + " " + controller.menuItems![index] + " " + NSLocalizedString("page", bundle: bundle, comment: "") + " \(index + 1) " + NSLocalizedString("on", bundle: bundle, comment: "") + " \(controller.menuItems!.count)"
        
        enableRefreshButton(vc: pagesViewControllers[index])
        
        self.pageViewController?.setViewControllers([pagesViewControllers[index]], direction: direction, animated: true, completion: nil)
    }
    
    func tabsMenuViewController(_ controller: TabsMenuViewController, didDeSelectMenuAtIndex index: Int) {
        guard index < pagesViewControllers.count else {
            return
        }
        // Updating accessibility label
        controller.accessibilityMenuItems?[index] = NSLocalizedString("menu", bundle: bundle, comment: "") + " " + controller.menuItems![index] + " " + NSLocalizedString("page", bundle: bundle, comment: "") + " \(index + 1) " + NSLocalizedString("on", bundle: bundle, comment: "") + " \(controller.menuItems!.count)"
        
//        enableRefreshButton(vc: pagesViewControllers[index])
    }
    
    // MARK: - UIPageViewControllerDelegate
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let i = pagesViewControllers.firstIndex(of: viewController) {
            if i > 0 {
                return pagesViewControllers[i - 1]
            }
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let i = pagesViewControllers.firstIndex(of: viewController) {
            if i < (pagesViewControllers.count - 1) {
                return pagesViewControllers[i + 1]
            }
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if let i = pagesViewControllers.firstIndex(of: pageViewController.viewControllers!.first!) {
            self.tabsMenuController?.selectedItemIndex = i
        }
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        // Changing accessibility label of previously selected tabs menu
        if let i = self.tabsMenuController?.selectedItemIndex {
            tabsMenuController!.accessibilityMenuItems?[i] = NSLocalizedString("menu", bundle: bundle, comment: "") + " " + tabsMenuController!.menuItems![i] + " " + NSLocalizedString("page", bundle: bundle, comment: "") + " \(i + 1) " + NSLocalizedString("on", bundle: bundle, comment: "") + " \(tabsMenuController!.menuItems!.count)"
//            enableRefreshButton(vc: pagesViewControllers[i])
        }
        // Selecting corresponding tabs menu and updating accessibility labbel
        if let i = self.pagesViewControllers.firstIndex(of: pendingViewControllers.first!) {
            if self.tabsMenuController != nil {
                self.tabsMenuController!.selectedItemIndex = i
                self.tabsMenuController!.accessibilityMenuItems?[i] = NSLocalizedString("selected", bundle: bundle, comment: "") + " " + NSLocalizedString("menu", bundle: bundle, comment: "") + " " + tabsMenuController!.menuItems![i] + " " + NSLocalizedString("page", bundle: bundle, comment: "") + " \(i + 1) " + NSLocalizedString("on", bundle: bundle, comment: "") + " \(tabsMenuController!.menuItems!.count)"
            }
            enableRefreshButton(vc: pagesViewControllers[i])
        }
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowCartoMenuPageVCSegue" {
            self.pageViewController = segue.destination as? UIPageViewController
        }
    }
    
    // MARK: - IBActions
    @objc func refreshAction(_ sender: UIBarButtonItem) {
        guard let i = self.tabsMenuController?.selectedItemIndex, let vc = self.pagesViewControllers[i] as? ListTableViewController else {
            return
        }
        vc.reload() // reload data
    }
}
