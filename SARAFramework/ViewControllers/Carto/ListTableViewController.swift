//
//  ListTableViewController.swift
//  SARAFramework
//
//  Created by Marine on 16.07.21.
//

import UIKit
import CoreLocation

class ListTableViewController: UITableViewController {

    // MARK: - Variables
    var elements : [(String, Double, Int)?] = []
    let bundle = Utils.bundle(anyClass: ListTableViewController.classForCoder())
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(UINib(nibName: "InformationCell", bundle: Bundle(for: ListTableViewController.classForCoder())), forCellReuseIdentifier: "informationCell")
        if #available(iOS 15, *) { // To remove gap between header and table view
            self.tableView?.sectionHeaderTopPadding = 0
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadData()
    }
    
    func loadData() {
        var points = PointRealm.getSortedPointsByProximity().map({ p -> (String, Double, Int)? in
            var p1GPSPosition: GPSPosition?
            var name: String = ""
            if let coursePoint = p as? CoursePoint {
                p1GPSPosition = coursePoint.gpsPosition
                name = "\(NSLocalizedString("waypoint", bundle: bundle, comment: "")) - \(coursePoint.name)"
            } else if let mark = p as? Mark {
                p1GPSPosition = mark.middlePoint()
                name = "\(NSLocalizedString("mark", bundle: bundle, comment: "")) - \(mark.name)"
            }
            guard let gpsPos = p1GPSPosition else {
                return nil
            }
            guard let currentPosition = LocationManager.shared.getCurrentPosition() else {
                return nil
            }
            let d = currentPosition.distance(from: CLLocation(latitude: CLLocationDegrees(gpsPos.latitude), longitude: CLLocationDegrees(gpsPos.longitude)))
            return (name, d, NavigationHelper.segmentOrientation(p1Location: currentPosition, p2Postion: p1GPSPosition!))
        })
        
        let beacons = Beacon.getSortedBeaconsByProximity().map({ b -> (String, Double, Int)? in
            guard let b1GPSPosition = b.gpsPosition else {
                return nil
            }
            guard let currentPosition = LocationManager.shared.getCurrentPosition() else {
                return nil
            }
            let d = currentPosition.distance(from: CLLocation(latitude: CLLocationDegrees(b1GPSPosition.latitude), longitude: CLLocationDegrees(b1GPSPosition.longitude)))
            let name = "\(b.type.description) - \(b.name)"
            return (name, d, NavigationHelper.segmentOrientation(p1Location: currentPosition, p2Postion: b1GPSPosition))
        })
        
        points.append(contentsOf: beacons)
        points.sort(by: { e1, e2 in
            guard e1 != nil else {
                return false
            }
            guard e2 != nil else {
                return false
            }
            return e1!.1 < e2!.1
        })
        // Get the first 30 elements
        elements = []
        if points.count > 30 {
            elements.append(contentsOf: Array(points[...30]))
        } else {
            elements.append(contentsOf: points)
        }
    }
    
    func reload() {
        loadData()
        tableView.reloadData()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // return the number of sections
        return elements.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return the number of rows
        return 2
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "informationCell", for: indexPath) as? InformationCell else {
            return UITableViewCell()
        }
        guard let data = elements[indexPath.section] else {
            return UITableViewCell()
        }
        // Configure the cell...
        // Infos = (Name of the info, value of the info, unit of the info)
        // Compute the location of the point given
        var infos:(String?, String?, NavigationUnit?)
        if indexPath.row == 0 {
            let distance = Float(data.1)
            if Settings.shared.distanceUnit.value?.unit == NavigationUnit.meter {
                infos = (NSLocalizedString("distance", bundle: bundle, comment: ""), "\(distance.roundDown)", .meter)
            } else {
                infos = (NSLocalizedString("distance", bundle: bundle, comment: ""), "\(distance.meterToMile())", .mile)
            }
        } else if indexPath.row == 1 {
            let orientation = data.2
            infos = (NSLocalizedString("azimuth", bundle: bundle, comment: ""), "\(orientation)", NavigationUnit.degrees)
        }
        cell.infos = (infos.0, infos.1, infos.2, nil, nil)
        return cell
    }

    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let data = elements[section] else {
            return nil
        }
        return data.0
    }
}
