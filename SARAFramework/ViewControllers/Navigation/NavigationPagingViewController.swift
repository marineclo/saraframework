//
//  NavigationPagingViewController.swift
//  SARA Croisiere
//
//  Created by Rose on 23/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

protocol PageControlValueDelegate: class {
    func pageControlChanged(to value: Int)
}

public class NavigationPagingViewController: UIPageViewController {
    
    var orderedControllers: [NavigationInformationsViewController] = []
    var orderedMenuTitles: [String] = [] {
        didSet {
            tabsMenuViewController?.menuItems = orderedMenuTitles
            tabsMenuViewController?.visibleItemsCount = orderedMenuTitles.count
        }
    }
    public weak var tabsMenuViewController: TabsMenuViewController? {
        didSet {
            tabsMenuViewController?.menuItems = self.orderedMenuTitles
            if let firstViewController = viewControllers?.first as? NavigationInformationsViewController,
                let index = orderedControllers.firstIndex(of: firstViewController) {
                tabsMenuViewController?.selectedItemIndex = index
            }
        }
    }
    fileprivate var currentViewController: Int = 0
    let bundle = Utils.bundle(anyClass: NavigationPagingViewController.classForCoder())
    let disposeBag = DisposeBag()
    
    // View controllers indexes
    public var infoTypes: [InfoType] = []
    
    public override func viewDidAppear(_ animated: Bool) {
        // When we set manually the tabBarController.selectedIndex, reload menu level 2
        if NavigationManager.shared.updateNavView {
            let controllerIndex = 0
            //Tabs menu status image
            self.setViewControllers([self.orderedControllers[controllerIndex]],
                                    direction: .forward,
                                    animated: true,
                                    completion: nil)
            self.tabsMenuViewController?.selectedItemIndex = 0
            currentViewController = 0
            NavigationManager.shared.updateNavView = false
        } else {
            self.tabsMenuViewController?.selectedItemIndex = currentViewController
        }
        
        self.tabsMenuViewController?.visibleItemsCount = self.orderedMenuTitles.count
        
        
        // Accessibility
        var accessibilityLabels: [String] = []
        let list = [Int](0...infoTypes.count - 1)
        list.forEach({
            self.updateMenuStatusImage(atIndex: $0) // Status image in tabs menu
            accessibilityLabels.append(self.accessibilityLabelForMenu(atIndex: $0)!)
        })
        self.tabsMenuViewController?.accessibilityMenuItems = accessibilityLabels
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dataSource = self
        self.delegate = self
        
        // Set the pages according InfoTypes
        infoTypes.forEach({
            setViews(infoType: $0)
        })
                
        let controllerIndex = 0
        // Tabs menu status image
        self.setViewControllers([self.orderedControllers[controllerIndex]],
                                direction: .forward,
                                animated: true,
                                completion: nil)
        currentViewController = 0
//        self.tabsMenuViewController?.selectedItemIndex = controllerIndex
//        self.tabsMenuViewController?.visibleItemsCount = self.orderedMenuTitles.count
        
        NavigationManager.shared.route
            .asObservable()
            .subscribe(onNext: { [weak self] (currentRoute) in
                guard self != nil else {
                    return
                }
                
                if let routeIndex = self?.infoTypes.firstIndex(of: .route) {
                    self!.updateMenuStatusImage(atIndex: routeIndex)
                    self!.tabsMenuViewController?.accessibilityMenuItems?[routeIndex] = self!.accessibilityLabelForMenu(atIndex: routeIndex)!
                }
                
                var controllerIndex = 0
                if currentRoute != nil {
                    controllerIndex = 0 //1
                }
                // Tabs menu status image
                // TODO: check why needs to be commented
//                self!.setViewControllers([self!.orderedControllers[controllerIndex]],
//                                        direction: .forward,
//                                        animated: true,
//                                        completion: nil)
                self!.tabsMenuViewController?.selectedItemIndex = controllerIndex
            }).disposed(by: disposeBag)
        
        // Tabs menu updates
        NmeaManager.default.connected.asObservable().subscribe(onNext: { [weak self] (connected) in
            guard self != nil,
                  self?.infoTypes.firstIndex(of: .navigationCenter) != nil else {
                return
            }
            if let navIndex = self?.infoTypes.firstIndex(of: .navigationCenter) {
                self!.updateMenuStatusImage(atIndex: navIndex)
                self!.tabsMenuViewController?.accessibilityMenuItems?[navIndex] = self!.accessibilityLabelForMenu(atIndex: navIndex)!
            }
            if let tellTalesIndex = self?.infoTypes.firstIndex(of: .telltales) {
                self!.updateMenuStatusImage(atIndex: tellTalesIndex)
                self!.tabsMenuViewController?.accessibilityMenuItems?[tellTalesIndex] = self!.accessibilityLabelForMenu(atIndex: tellTalesIndex)!
            }
        }).disposed(by: disposeBag)
    }
    
    fileprivate func setViews(infoType: InfoType) {
        let storyboard = UIStoryboard(name: "Navigation", bundle: bundle)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "NavigationInformationsViewController") as? NavigationInformationsViewController else {
            return
        }
        switch infoType {
        case .gps:
            // GPS: always displayed
            vc.type = .gps
            vc.viewModel.setUpGPS()
            self.orderedControllers.append(vc)
            self.orderedMenuTitles.append(NSLocalizedString("gps_title", bundle: bundle, comment: ""))
        case .route:
            // Route integration
            vc.viewModel.setUpRouteDataSource()
            vc.type = .route
            self.orderedControllers.append(vc)
//            self.orderedMenuTitles.append(NSLocalizedString("route_title", bundle: bundle, comment: ""))
            self.orderedMenuTitles.append(Utils.localizedString(forKey: "route_title", app: "SARANav"))
        case .navigationCenter:
            // Navigation Center integration
            vc.type = .navigationCenter
            vc.viewModel.setUpNavigationCenter()
            self.orderedControllers.append(vc)
            self.orderedMenuTitles.append(NSLocalizedString("navigation_center_title", bundle: bundle, comment: ""))
        case .telltales:
            // Telltales integration
            vc.type = .telltales
            vc.viewModel.setUpTelltales()
            self.orderedControllers.append(vc)
            self.orderedMenuTitles.append(NSLocalizedString("telltales_title", bundle: bundle, comment: ""))
        case .seascape:
            // Seascape integration
            vc.type = .seascape
            vc.viewModel.setUpGeographics()
            self.orderedControllers.append(vc)
            self.orderedMenuTitles.append(NSLocalizedString("seascape_title", bundle: bundle, comment: ""))
        case .current:
            // Info Marks
            vc.type = .current
            self.orderedControllers.append(vc)
            self.orderedMenuTitles.append(NSLocalizedString("current_title", bundle: bundle, comment: ""))
        case .statistic:
            // Statistic integration
            vc.type = .statistic
            vc.viewModel.setUpStatistic()
            self.orderedControllers.append(vc)
            self.orderedMenuTitles.append(NSLocalizedString("statistic_title", bundle: bundle, comment: ""))
        default:
            break
        }
    }
    
    // Update the tabs menu status image at the provided index
    fileprivate func updateMenuStatusImage(atIndex index: Int) {
        switch infoTypes[index] {
        case .route, .current:
            if NavigationManager.shared.route.value == nil {
                self.tabsMenuViewController?.setStatusImage(UIImage(named: "unplugged", in: bundle, compatibleWith: nil), forItemAtIndex: index)
            }
            else {
                self.tabsMenuViewController?.setStatusImage(UIImage(named: "plugged", in: bundle, compatibleWith: nil), forItemAtIndex: index)
            }
            break
        case .navigationCenter, .telltales:
            if NmeaManager.default.connected.value {
                self.tabsMenuViewController?.setStatusImage(UIImage(named: "plugged", in: bundle, compatibleWith: nil), forItemAtIndex: index)
            }
            else {
                self.tabsMenuViewController?.setStatusImage(UIImage(named: "unplugged", in: bundle, compatibleWith: nil), forItemAtIndex: index)
            }
            break
        case .seascape:
            Beacon.all().count > 0 ? self.tabsMenuViewController?.setStatusImage(UIImage(named: "plugged", in: bundle, compatibleWith: nil), forItemAtIndex: index) : self.tabsMenuViewController?.setStatusImage(UIImage(named: "unplugged", in: bundle, compatibleWith: nil), forItemAtIndex: index)
            break
        default:
            break
        }
    }
    
    // Returns the accessibility label for the provided menu index
    fileprivate func accessibilityLabelForMenu(atIndex index: Int) -> String? {
        let menusCount =  self.orderedMenuTitles.count
        var accessibilityLabel: String = ""
        if index == self.tabsMenuViewController?.selectedItemIndex {
            accessibilityLabel += NSLocalizedString("selected", bundle: bundle, comment: "")
        }
        accessibilityLabel += " " + NSLocalizedString("menu", bundle: bundle, comment: "")
        let infoType = infoTypes[index]
        switch infoType {
        case .gps:
            return accessibilityLabel + " " + NSLocalizedString("gps_title", bundle: bundle, comment: "") + " " + NSLocalizedString("page", bundle: bundle, comment: "") + " \(index + 1) " + NSLocalizedString("on", bundle: bundle, comment: "") + " \(menusCount)"
        case .route:
            accessibilityLabel += " " + Utils.localizedString(forKey: "route_title", app: "SARANav") + " " + NSLocalizedString("page", bundle: bundle, comment: "") + " \(index + 1) " + NSLocalizedString("on", bundle: bundle, comment: "") + " \(menusCount)"
            return NavigationManager.shared.route.value == nil ? accessibilityLabel + " " + Utils.localizedString(forKey: "no_activated_route", app: "SARANav") : accessibilityLabel
        case .navigationCenter:
            accessibilityLabel += " " + NSLocalizedString("navigation_center_title", bundle: bundle, comment: "") + " " + NSLocalizedString("page", bundle: bundle, comment: "") + " \(index + 1) " + NSLocalizedString("on", bundle: bundle, comment: "") + " \(menusCount)"
            return NmeaManager.default.connected.value ? accessibilityLabel : accessibilityLabel + " " + NSLocalizedString("not_connected", bundle: bundle, comment: "")
        case .telltales:
            accessibilityLabel += " " + NSLocalizedString("telltales_title", bundle: bundle, comment: "") + " " + NSLocalizedString("page", bundle: bundle, comment: "") + " \(index + 1) " + NSLocalizedString("on", bundle: bundle, comment: "") + " \(menusCount)"
            return NmeaManager.default.connected.value ? accessibilityLabel : accessibilityLabel + " " + NSLocalizedString("not_connected", bundle: bundle, comment: "")
        case .seascape:
            accessibilityLabel += " " + NSLocalizedString("seascape_title", bundle: bundle, comment: "") + " " + NSLocalizedString("page", bundle: bundle, comment: "") + " \(index + 1) " + NSLocalizedString("on", bundle: bundle, comment: "") + " \(menusCount)"
            return Beacon.all().count > 0 ? accessibilityLabel : accessibilityLabel + " " + NSLocalizedString("not_connected", bundle: bundle, comment: "")
        case .current:
            accessibilityLabel += " " + NSLocalizedString("current_title", bundle: bundle, comment: "") + " " + NSLocalizedString("page", bundle: bundle, comment: "") + " \(index + 1) " + NSLocalizedString("on", bundle: bundle, comment: "") + " \(menusCount)"
            return NavigationManager.shared.route.value == nil ? accessibilityLabel + " " + Utils.localizedString(forKey: "no_activated_course", app: "SARARegate") : accessibilityLabel
        case .statistic:
            return accessibilityLabel + " " + NSLocalizedString("statistic_title", bundle: bundle, comment: "") + " " + NSLocalizedString("page", bundle: bundle, comment: "") + " \(index + 1) " + NSLocalizedString("on", bundle: bundle, comment: "") + " \(menusCount)"
        default:
            return nil
        }
    }
}


extension NavigationPagingViewController: UIPageViewControllerDataSource {
    
    public func pageViewController(_ pageViewController: UIPageViewController,
                                   viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let infoController = viewController as? NavigationInformationsViewController, let viewControllerIndex = orderedControllers.firstIndex(of: infoController) else {
            return nil
        }
            
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedControllers.count > previousIndex else {
            return nil
        }
        return orderedControllers[previousIndex]
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController,
                                   viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let infoViewController = viewController as? NavigationInformationsViewController,
            let viewControllerIndex = orderedControllers.firstIndex(of: infoViewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedControllers.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        return orderedControllers[nextIndex]
    }
    
}

extension NavigationPagingViewController: PageControlValueDelegate {
    func pageControlChanged(to value: Int) {

        if value < orderedControllers.count {
            self.setViewControllers([orderedControllers[value]],
                                    direction: value > currentViewController ? .forward : .reverse,
                                    animated: true,
                                    completion: nil)
            self.currentViewController = value
        }
    }
}

extension NavigationPagingViewController: UIPageViewControllerDelegate {
    public func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
     
        if let firstViewController = viewControllers?.first as? NavigationInformationsViewController, let index = orderedControllers.firstIndex(of: firstViewController) {
            
            self.currentViewController = index
            self.tabsMenuViewController?.selectedItemIndex = index

            UIAccessibility.post(notification: UIAccessibility.Notification.announcement, argument: "\(self.orderedMenuTitles[index]) \(NSLocalizedString("page", bundle: bundle, comment: "")) \(index + 1) \(NSLocalizedString("on", bundle: bundle, comment: "")) \(self.orderedMenuTitles.count)")
        }
        
    }
    public func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        // Changing accessibility label of previously selected tabs menu
        if let i = self.tabsMenuViewController?.selectedItemIndex {
            tabsMenuViewController!.accessibilityMenuItems?[i] = NSLocalizedString("menu", bundle: bundle, comment: "") + " " + tabsMenuViewController!.menuItems![i] + " " + NSLocalizedString("page", bundle: bundle, comment: "") + " \(i + 1) " + NSLocalizedString("on", bundle: bundle, comment: "") + " \(tabsMenuViewController!.menuItems!.count)"
        }
        // Selecting corresponding tabs menu and updating accessibility labbel
        if let i = self.orderedControllers.firstIndex(of: pendingViewControllers.first as! NavigationInformationsViewController) {
            if self.tabsMenuViewController != nil {
                self.tabsMenuViewController!.selectedItemIndex = i
                self.tabsMenuViewController!.accessibilityMenuItems?[i] = NSLocalizedString("selected", bundle: bundle, comment: "") + " " + NSLocalizedString("menu", bundle: bundle, comment: "") + " " + tabsMenuViewController!.menuItems![i] + " " + NSLocalizedString("page", bundle: bundle, comment: "") + " \(i + 1) " + NSLocalizedString("on", bundle: bundle, comment: "") + " \(tabsMenuViewController!.menuItems!.count)"
            }
            
        }
    }
}

// MARK: - Tabs menu view controller delegate
extension NavigationPagingViewController: TabsMenuViewControllerDelegate {
    public func tabsMenuViewController(_ controller: TabsMenuViewController, didSelectMenuAtIndex index: Int) {
        self.pageControlChanged(to: index)
        if let accessibilityLabel = self.accessibilityLabelForMenu(atIndex: index) {
            controller.accessibilityMenuItems?[index] = accessibilityLabel
        }
    }
    
    public func tabsMenuViewController(_ controller: TabsMenuViewController, didDeSelectMenuAtIndex index: Int) {
        if let accessibilityLabel = self.accessibilityLabelForMenu(atIndex: index) {
            controller.accessibilityMenuItems?[index] = accessibilityLabel
        }
    }
}
