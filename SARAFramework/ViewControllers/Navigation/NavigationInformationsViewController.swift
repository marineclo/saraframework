//
//  NavigationInformationsViewController.swift
//  SARA Croisiere
//
//  Created by Rose on 23/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import CoreLocation

public enum InfoType {
    case current
    case gps
    case route
    case navigationCenter
    case telltales
    case seascape
    case statistic
}

public class NavigationInformationsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, TellTaleManagerObserver, MapDataManagerObserver {
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: NavigationInformationsViewController.classForCoder())
    var type: InfoType = .current
    var viewModel = NavigationViewModel()
    
    let disposeBag = DisposeBag()
    var disposables: [Disposable] = []
    
    private var tellTales: [TellTale] = []
    
    fileprivate var messageLabel: UILabel?
    
    // MARK: - IBOutlets
    @IBOutlet private weak var tableView: UITableView?
    
    // MARK: -
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView?.dataSource = self
        self.tableView?.delegate = self
        self.tableView?.register(UINib(nibName: "InformationCell", bundle: Bundle(for: NavigationInformationsViewController.classForCoder())), forCellReuseIdentifier: "InformationsCellIdentifier")
        self.tableView?.register(UINib(nibName: "SeascapeTableViewCell", bundle: Bundle(for: NavigationInformationsViewController.classForCoder())), forCellReuseIdentifier: "SeascapeCellIdentifier")
        self.tableView?.register(UINib(nibName: "NavigationHeaderTableView", bundle: Bundle(for: NavigationInformationsViewController.classForCoder())), forHeaderFooterViewReuseIdentifier: "NavigationHeaderTableViewIdentifier")
        
        self.setupUI()
        
        switch self.type {
        case .gps:
            self.setUpObservers()
        case .navigationCenter:
            self.subscribeNMEA()
        case .route, .current:
            NavigationManager.shared.point
                .asObservable()
                .subscribe(onNext: { [weak self] (point) in
                    // Need to reload datasource when the point changed
                    self?.resetView()
                    //AnnouncementManager.shared.resetCurrentState()
                }).disposed(by: disposeBag)
            NavigationManager.shared.middlePointChanged // if value changed (for double mark) need to update view (Header)
                .asObservable()
                .subscribe(onNext: { (value) in
                    if value {
                        self.tableView?.reloadData()
                    }
                }).disposed(by: disposeBag)
        case .seascape:
            self.tableView?.separatorColor = UIColor.lightGray
            self.setUpObservers()
            break
//            if let message = self.viewModel.messageText {
//                self.labelNoData(message: message)
//            }
        case .telltales:
            self.subscribeNMEA()
        case .statistic:
            self.resetView()
//            self.setUpObservers()
        }
        
//        NavigationManager.shared.shouldReloadUnits
//            .asObservable()
//            .filter({ $0 == true })
//            .subscribe(onNext: { [weak self] _ in
//                // Need to reload datasource when the units changed
//                self?.resetView()
////                AnnouncementManager.shared.resetCurrentState()
//            }).disposed(by: disposeBag)
    }
    
    /// Set up observers needed for each information view
    private func setUpObservers() {
        // Dispose previous disposables
        self.disposables.forEach {
            $0.dispose()
        }
        self.disposables.removeAll(keepingCapacity: true)
        
        switch self.type {
        case .current:
            self.observeGisementToCurrentPoint()
            self.observeDistanceToCurrentPoint()
            self.observeCMG()
            if NavigationManager.shared.route.value != nil {
                self.observeGPSAccuracy()
            }
            break
        case .gps:
            self.observeAverageCourse()
            self.observeAverageSpeed()
            self.observeMaxSpeed10s()
            self.observeMaxSpeed()
            self.observeGPSAccuracy()
            self.observeGPSCoordinate()
            break
        case .route:
            if NavigationManager.shared.point.value != nil {
                // Observe distance, bearing and cmg for current point
                self.observeDistanceToCurrentPoint()
                self.observeAzimutToCurrentPoint()
                self.observeGisementToCurrentPoint()
                self.observeCMG()
                self.observeGapToSegmentRoute()
                // if next point
                if NavigationManager.shared.isNotLastPoint.value {
                    self.observeDistanceToNextPoint()
                    self.observeAzimutToNextPoint()
                    self.observeGisementToNextPoint()
                    self.observeNextSegment()
                }
            }
            break
        case .navigationCenter:
            if NmeaManager.default.connected.value && NmeaManager.default.currentSSID.value != nil && NmeaManager.default.connexionStatus.value == NSLocalizedString("connected", bundle: bundle, comment: "")  {
                //Instruments
                self.observeDepth()
                self.observeMagneticHeading()
                self.observeSpeedThroughWater()
                self.observeMaxSpeed10sNav()
                self.observeMaxSpeedNav()
                //Vent réel
                self.observeWindDirection()
                self.observeSpeedTrueWind()
                self.observeAngleTrueWind()
                self.observeMaxSpeedTrueWind10s()
                self.observeMaxSpeedTrueWind()
                //Vent apparent
                self.observeSpeedRelativeWind()
                self.observeAngleRelativeWind()
                //Température
                self.observeWaterTemperature()
                self.observeAirTemperature()
            }
            break
        case .telltales:
            TellTalesManager.default.removeObserver(self)
            self.tellTales = []
            TellTalesManager.default.addObserver(self)
            break
        case .seascape:
            MapDataManager.shared.removeObserver(self)
            MapDataManager.shared.addObserver(self)
            break
        case .statistic:
            self.observeAverageCourse()
            self.observeAverageSpeed()
            self.observeMaxSpeed()
            if NavigationManager.shared.route.value == nil {
                self.observeGPSAccuracy()
            }
        }
    }
    
    private func resetView() {
        self.viewModel.dataSources.removeAll()
        self.viewModel.sectionHeaders.removeAll()
        self.messageLabel?.removeFromSuperview()
        if self.type == .gps {
            self.viewModel.setUpGPS()
        } else if self.type == .route {
            self.viewModel.setUpRouteDataSource()
        } else if self.type == .navigationCenter {
            self.viewModel.setUpNavigationCenter()
        } else if self.type == .telltales {
            self.viewModel.setUpTelltales()
        } else if self.type == .statistic {
            self.viewModel.setUpStatistic()
        } else if self.type == .seascape {
            self.viewModel.setUpGeographics()
        } else if self.type == .current {
            self.viewModel.setUpCurrent()
        }
        if let message = self.viewModel.messageText {
            self.labelNoData(message: message)
        }
        self.tableView?.reloadData()
        self.setUpObservers()
    }
    
    private func subscribeNMEA() {
        NmeaManager.default.currentSSID.asObservable()
            .subscribe(onNext: { [weak self] (ssid) in
                self?.resetView()
            }).disposed(by: disposeBag)

        NmeaManager.default.connected
            .asObservable()
            .withPrevious()
            .subscribe(onNext: { [weak self] (oldConnected, connected) in
//                print("=== connected: \(connected)")
                if oldConnected != connected {
                    self?.resetView()
                }
        }).disposed(by: disposeBag)
        
        // When connexion status change, reload view
        NmeaManager.default.connexionStatus.asObservable()
            .withPrevious()
            .subscribe(onNext: { [weak self] (oldConnected, connected) in
                self?.resetView()
            } ).disposed(by: disposeBag)
    }
    
    // MARK: - GPS
    
    /// Observe average speed and fill the cells which need it
    private func observeAverageSpeed() {
        LocationManager.shared
            .averageSpeedVariable
            .asObservable()
            .bind(onNext: { [weak self] (averageSpeed) in
                if let indexes = self?.viewModel.dictTypeIndex[.meanSOG], let info = self?.viewModel.dataSources[indexes.0][indexes.1] {
                    var value: String? = nil
                    if averageSpeed != nil {
                        value = "\(averageSpeed!.formatted)"
                    }
                    self?.viewModel.dataSources[indexes.0][indexes.1] = (info.0, value, info.2, nil, nil)
                    if let distanceCell = self?.tableView?.cellForRow(at: IndexPath(row: indexes.1, section: indexes.0)) as? InformationCell {
                        distanceCell.infos = (info.0, value, info.2, nil, nil)
                    }
                }
            }).disposed(by: disposeBag)
    }
    
    /// Observe average course and fill the cells which need it
    private func observeAverageCourse() {
        let disposable = LocationManager.shared
            .averageCourseVariable
            .asObservable()
            .bind(onNext: { [weak self] (averageCourse) in
                if let indexes = self?.viewModel.dictTypeIndex[.meanCOG], let info = self?.viewModel.dataSources[indexes.0][indexes.1] {
                    var value: String? = nil
                    if averageCourse != nil {
                        value = "\(averageCourse!)"
                    }
                    self?.viewModel.dataSources[indexes.0][indexes.1] = (info.0, value, info.2, nil, nil)
                    if let averageCourseCell = self?.tableView?.cellForRow(at: IndexPath(row: indexes.1, section: indexes.0)) as? InformationCell {
                        averageCourseCell.infos = (info.0, value, info.2, nil, nil)
                    }
                }
            })
        self.disposables.append(disposable)
        disposable.disposed(by: disposeBag)
    }
    
    // Observe max speed and fill the cells which need it
    private func observeMaxSpeed() {
        let disposable = LocationManager.shared.maxSpeedVariable
            .asObservable()
            .bind { [weak self] (maxSpeed) in
                if let indexes = self?.viewModel.dictTypeIndex[.maxSpeedOverGround], let info = self?.viewModel.dataSources[indexes.0][indexes.1] {
                    var value: String? = nil
                    if maxSpeed != nil {
                        value = "\(maxSpeed!)"
                    }
                    self?.viewModel.dataSources[indexes.0][indexes.1] = (info.0, value, info.2, nil, nil)
                    if let maxSpeedCell = self?.tableView?.cellForRow(at: IndexPath(row: indexes.1, section: indexes.0)) as? InformationCell {
                        maxSpeedCell.infos = (info.0, value, info.2, nil, nil)
                    }
                }
        }
        self.disposables.append(disposable)
        disposable.disposed(by: disposeBag)
    }
    
    // Observe max speed and fill the cells which need it
    private func observeMaxSpeed10s() {
        let disposable = LocationManager.shared.maxSpeed10sVariable
            .asObservable()
            .bind { [weak self] (maxSpeed10s) in
                if let indexes = self?.viewModel.dictTypeIndex[.maxSOG10s], let info = self?.viewModel.dataSources[indexes.0][indexes.1] {
                    var value: String? = nil
                    if maxSpeed10s != nil {
                        value = "\(maxSpeed10s!)"
                    }
                    self?.viewModel.dataSources[indexes.0][indexes.1] = (info.0, value, info.2, nil, nil)
                    if let maxSpeed10sCell = self?.tableView?.cellForRow(at: IndexPath(row: indexes.1, section: indexes.0)) as? InformationCell {
                        maxSpeed10sCell.infos = (info.0, value, info.2, nil, nil)
//                        maxSpeed10sCell.accessibilityLabel = "Vitesse fond max sur 10 secondes  \(maxSpeed10s) \(info.2)"
                    }
                }
            }
        self.disposables.append(disposable)
        disposable.disposed(by: disposeBag)
    }
    
    // Observe GPS accuracy and fill the cells which need it
    private func observeGPSAccuracy() {
        let disposable = LocationManager.shared.accuracyVariable
            .asObservable()
            .bind(onNext: { [weak self] (accuracy) in
                if let indexes = self?.viewModel.dictTypeIndex[.gpsPrecision], let info = self?.viewModel.dataSources[indexes.0][indexes.1] {
                    var value: String? = nil
                    if accuracy != nil {
                        value = "\(Int(accuracy!))"
                    }
                    self?.viewModel.dataSources[indexes.0][indexes.1] = (info.0, value, info.2, nil, nil)
                    if let accuracyCell = self?.tableView?.cellForRow(at: IndexPath(row: indexes.1, section: indexes.0)) as? InformationCell {
                        accuracyCell.infos = (info.0, value, info.2, nil, nil)
                    }
                }
            })
        self.disposables.append(disposable)
        disposable.disposed(by: disposeBag)
    }
    
    // Observe GPS accuracy and fill the cells which need it
    private func observeGPSCoordinate() {
        let disposable = LocationManager.shared.locationVariable
            .asObservable()
            .bind(onNext: { [weak self] (location) in
                guard let weakSelf = self else {
                    return
                }
                if let indexesLat = self?.viewModel.dictTypeIndex[.latitude], let indexesLon = self?.viewModel.dictTypeIndex[.longitude], let infoLat = self?.viewModel.dataSources[indexesLat.0][indexesLat.1], let infoLong = self?.viewModel.dataSources[indexesLon.0][indexesLon.1] {
                    var valueLat: String? = nil
                    var valueLong: String? = nil
                    var unitLat: NavigationUnit? = nil
                    var unitLong: NavigationUnit? = nil
                    if location != nil {
                        let latitude = Float(location!.coordinate.latitude)
                        let longitude = Float(location!.coordinate.longitude)
                        switch Settings.shared.coordinatesUnit?.unit {
                        case .minuteDecimalDegrees:
                            valueLat = "\(abs(latitude.coordinates.0))\(NSLocalizedString("unit_abb_degrees", bundle: weakSelf.bundle, comment: "")) \(abs(latitude.coordinates.1.rounded(toPlaces: 3)) )\(NSLocalizedString("unit_abb_minutes", bundle: weakSelf.bundle, comment: ""))"
                            unitLat = latitude.coordinates.0 > 0 ? NavigationUnit.north : NavigationUnit.south
                            valueLong = "\(abs(longitude.coordinates.0))\(NSLocalizedString("unit_abb_degrees", bundle: weakSelf.bundle, comment: "")) \(abs(longitude.coordinates.1.rounded(toPlaces: 3)))\(NSLocalizedString("unit_abb_minutes", bundle: weakSelf.bundle, comment: ""))"
                            unitLong = longitude.coordinates.0 > 0 ? NavigationUnit.east : NavigationUnit.west
                        case .degreesMinuteSecond:
                            let latDMS = latitude.coordinatesDMS
                            let longDMS = longitude.coordinatesDMS
                            valueLat = "\(abs(latDMS.0))\(NSLocalizedString("unit_abb_degrees", bundle: weakSelf.bundle, comment: "")) \(abs(latDMS.1) )\(NSLocalizedString("unit_abb_minutes", bundle: weakSelf.bundle, comment: "")) \(abs(latDMS.2.rounded(toPlaces: 3)) )\(NSLocalizedString("unit_abb_secondes", bundle: weakSelf.bundle, comment: ""))"
                            unitLat = latDMS.0 > 0 ? NavigationUnit.north : NavigationUnit.south
                            valueLong = "\(abs(longDMS.0))\(NSLocalizedString("unit_abb_degrees", bundle: weakSelf.bundle, comment: "")) \(abs(longDMS.1) )\(NSLocalizedString("unit_abb_minutes", bundle: weakSelf.bundle, comment: "")) \(abs(longDMS.2.rounded(toPlaces: 3)) )\(NSLocalizedString("unit_abb_secondes", bundle: weakSelf.bundle, comment: ""))"
                            unitLong = longDMS.0 > 0 ? NavigationUnit.east : NavigationUnit.west
                        default:
                            valueLat = "\(abs(latitude.rounded(toPlaces: 4)))\(NSLocalizedString("unit_abb_degrees", bundle: weakSelf.bundle, comment: ""))"
                            unitLat = latitude > 0 ? NavigationUnit.north : NavigationUnit.south
                            valueLong = "\(abs(longitude.rounded(toPlaces: 4)))\(NSLocalizedString("unit_abb_degrees", bundle: weakSelf.bundle, comment: ""))"
                            unitLong = longitude > 0 ? NavigationUnit.east : NavigationUnit.west
                        }
                    }
                    self?.viewModel.dataSources[indexesLat.0][indexesLat.1] = (infoLat.0, valueLat, unitLat, nil, nil)
                    self?.viewModel.dataSources[indexesLon.0][indexesLon.1] = (infoLong.0, valueLong, unitLong, nil, nil)
                    if let latitudeCell = self?.tableView?.cellForRow(at: IndexPath(row: indexesLat.1, section: indexesLat.0)) as? InformationCell, let longitudeCell = self?.tableView?.cellForRow(at: IndexPath(row: indexesLon.1, section: indexesLon.0)) as? InformationCell {
                        latitudeCell.infos = (infoLat.0, valueLat, unitLat, nil, nil)
                        longitudeCell.infos = (infoLong.0, valueLong, unitLong, nil, nil)
                    }
                }
            })
        self.disposables.append(disposable)
        disposable.disposed(by: disposeBag)
    }
    
    //MARK: - Course

    /// Observe CMG value and fill cells which need it
    ///
    /// - Parameter point: CoursePoint
    private func observeCMG() {
        let disposable = NavigationManager.shared.cmgToCurrentPoint.asObservable()
            .bind(onNext: { [weak self] (cmg) in
                if NavigationManager.shared.point.value != nil && NavigationManager.shared.route.value != nil {
                    if let indexes = self?.viewModel.dictTypeIndex[.CMG], let info = self?.viewModel.dataSources[indexes.0][indexes.1] {
                        var value1: String? = nil
                        var value2: String? = nil
                        var navUnit2: NavigationUnit? = nil
                        if cmg != nil {
                            if let cmg1 = cmg?.0 {
                                value1 = "\(cmg1)"
                            }
                            if let cmg2 = cmg?.1 {
                                value2 = "\(cmg2)"
                            }
                            let point = NavigationManager.shared.point.value?.point
                            if let mark = point as? Mark, mark.buoys.count == 2, !NavigationManager.shared.usingMiddlePoint {
                                navUnit2 = Settings.shared.speedUnit?.unit
                            } else {
                                navUnit2 = nil
                            }
                        }
                        if navUnit2 == nil { // CruisePoint or Simple mark
                            self?.viewModel.dataSources[indexes.0][indexes.1] = (info.0, value1, info.2, nil, nil)
                        } else {
                            self?.viewModel.dataSources[indexes.0][indexes.1] = (info.0, value2, navUnit2, value1, info.2)
                        }
                        if let cmgCell = self?.tableView?.cellForRow(at: IndexPath(row: indexes.1, section: indexes.0)) as? InformationCell {
                            if navUnit2 == nil {
                                cmgCell.infos = (info.0, value1, info.2, value2, navUnit2)
                            } else {
                                cmgCell.infos = (info.0, value2, navUnit2, value1, info.2)
                            }
                        }
                    }
                }
            })
        self.disposables.append(disposable)
        disposable.disposed(by: disposeBag)
    }
    
    /// Observer distance and fill the cells which need it
    private func observeDistanceToCurrentPoint() {
        let disposable = NavigationManager.shared.distanceToCurrentPoint.asObservable()
            .bind(onNext: { [weak self] (distance) in
                if NavigationManager.shared.point.value != nil {
                    if let indexes = self?.viewModel.dictTypeIndex[.distance], let info = self?.viewModel.dataSources[indexes.0][indexes.1] {
                        let section = indexes.0
                        let row = indexes.1
                        if distance != nil {
                            if distance!.0 != nil && distance!.0!.1 == .meter {
                                let d1 = distance!.0!.0.roundDown
                                var d2String: String? = nil
                                if distance!.1 != nil && distance!.1!.1 == .meter {
                                    d2String = "\(distance!.1!.0.roundDown)"
                                } else if distance!.1 != nil {
                                    d2String = "\(distance!.1!.0)"
                                }
                                if d2String == nil {
                                    self?.viewModel.dataSources[section][row] = (info.0, "\(d1)", distance!.0!.1!, nil, nil)
                                } else {
                                    self?.viewModel.dataSources[section][row] = (info.0, d2String, distance!.1?.1! ?? nil, "\(d1)", distance!.0!.1!)
                                }
                                
                                if let distanceCell = self?.tableView?.cellForRow(at: IndexPath(row: row, section: section)) as? InformationCell {
                                    if d2String == nil {
                                        distanceCell.infos = (info.0, "\(d1)", distance!.0!.1!, nil, nil)
                                    } else {
                                        distanceCell.infos = (info.0, d2String, distance!.1?.1! ?? nil, "\(d1)", distance!.0!.1!)
                                    }
                                }
                            } else {
                                var d2String: String? = nil
                                if distance!.1 != nil && distance!.1!.1 == .meter {
                                    d2String = "\(distance!.1!.0.roundDown)"
                                } else if distance!.1 != nil {
                                    d2String = "\(distance!.1!.0)"
                                }
                                if d2String == nil {
                                    self?.viewModel.dataSources[section][row] = (info.0, "\(distance!.0!.0)", distance!.0!.1, nil, nil)
                                } else {
                                    self?.viewModel.dataSources[section][row] = (info.0, d2String, distance!.1?.1! ?? nil, "\(distance!.0!.0)", distance!.0!.1)
                                }
                                if let distanceCell = self?.tableView?.cellForRow(at: IndexPath(row: row, section: section)) as? InformationCell {
                                    if d2String == nil {
                                        distanceCell.infos = (info.0, "\(distance!.0!.0)", distance!.0!.1, d2String, distance!.1?.1! ?? nil)
                                    } else {
                                        distanceCell.infos = (info.0, d2String, distance!.1?.1! ?? nil, "\(distance!.0!.0)", distance!.0!.1)
                                    }
                                }
                            }
                        } else {
                            self?.viewModel.dataSources[section][row] = (info.0, nil, Settings.shared.distanceUnit.value?.unit, nil, Settings.shared.distanceUnit.value?.unit)
                            if let distanceCell = self?.tableView?.cellForRow(at: IndexPath(row: row, section: section)) as? InformationCell {
                                distanceCell.infos = (info.0, nil, Settings.shared.distanceUnit.value?.unit, nil, Settings.shared.distanceUnit.value?.unit)
                            }
                        }
                    }
                }
            })
        self.disposables.append(disposable)
        disposable.disposed(by: disposeBag)
    }
    
    /// Observer distance to next point and fill the cells which need it
    private func observeDistanceToNextPoint() {
        let disposable = NavigationManager.shared.distanceToNextPoint.asObservable()
            .bind(onNext: { [weak self] (distance) in
                if NavigationManager.shared.getNextPointWithCoord() != nil {
                    let section = 2
                    let row = 2
                    if let info = self?.viewModel.dataSources[section][row] {
                        if distance != nil {
                            if distance!.0 != nil && distance!.0!.1 == .meter {
                                let d = distance!.0!.0.roundDown
                                var d2String: String? = nil
                                if distance!.1 != nil && distance!.1!.1 == .meter {
                                    d2String = "\(distance!.1!.0.roundDown)"
                                } else if distance!.1 != nil {
                                    d2String = "\(distance!.1!.0)"
                                }
                                if d2String == nil {
                                    self?.viewModel.dataSources[section][row] = (info.0, "\(d)", distance!.0!.1!, nil, nil)
                                } else {
                                    self?.viewModel.dataSources[section][row] = (info.0, d2String, distance!.1?.1! ?? nil, "\(d)", distance!.0!.1!)
                                }
                                if let distanceCell = self?.tableView?.cellForRow(at: IndexPath(row: row, section: section)) as? InformationCell {
                                    if d2String == nil {
                                        distanceCell.infos = (info.0, "\(d)", distance!.0!.1!, nil, nil)
                                    } else {
                                        distanceCell.infos = (info.0, d2String, distance!.1?.1! ?? nil, "\(d)", distance!.0!.1!)
                                    }
                                }
                            } else {
                                var d2String: String? = nil
                                if distance!.1 != nil && distance!.1!.1 == .meter {
                                    d2String = "\(distance!.1!.0.roundDown)"
                                } else if distance!.1 != nil {
                                    d2String = "\(distance!.1!.0)"
                                }
                                if d2String == nil {
                                    self?.viewModel.dataSources[section][row] = (info.0, "\(distance!.0!.0)", distance!.0!.1, nil, nil)
                                } else {
                                    self?.viewModel.dataSources[section][row] = (info.0, d2String, distance!.1?.1! ?? nil, "\(distance!.0!.0)", distance!.0!.1)
                                }
                                if let distanceCell = self?.tableView?.cellForRow(at: IndexPath(row: row, section: section)) as? InformationCell {
                                    if d2String == nil {
                                        distanceCell.infos = (info.0, "\(distance!.0!.0)", distance!.0!.1, d2String, distance!.1?.1! ?? nil)
                                    } else {
                                        distanceCell.infos = (info.0, d2String, distance!.1?.1! ?? nil, "\(distance!.0!.0)", distance!.0!.1)
                                    }
                                }
                            }
                        } else {
                            self?.viewModel.dataSources[section][row] = (info.0, nil, Settings.shared.distanceUnit.value?.unit, nil, Settings.shared.distanceUnit.value?.unit)
                            if let distanceCell = self?.tableView?.cellForRow(at: IndexPath(row: row, section: section)) as? InformationCell {
                                distanceCell.infos = (info.0, nil, Settings.shared.distanceUnit.value?.unit, nil, Settings.shared.distanceUnit.value?.unit)
                            }
                        }
                    }
                }
            })
        self.disposables.append(disposable)
        disposable.disposed(by: disposeBag)
    }
    
    /// Observer azimut and fill the cells which need it
    private func observeAzimutToCurrentPoint() {
        let disposable = NavigationManager.shared.azimutToCurrentPoint.asObservable()
            .bind(onNext: { [weak self] (bearing) in
                if NavigationManager.shared.point.value != nil {
                    if let info = self?.viewModel.dataSources[0][1] {
                        var value1: String? = nil
                        var value2: String? = nil
                        if bearing != nil {
                            if let b1 = bearing?.0 {
                                value1 = "\(b1)"
                            }
                            if let b2 = bearing?.1 {
                                value2 = "\(b2)"
                            }
                        }
                        if value2 == nil {
                            self?.viewModel.dataSources[0][1] = (info.0, value1, .degrees, nil, nil)
                        } else {
                            self?.viewModel.dataSources[0][1] = (info.0, value2, value2 == nil ? nil : .degrees, value1, .degrees)
                        }
                        if let azimutCell = self?.tableView?.cellForRow(at: IndexPath(row: 1, section: 0)) as? InformationCell {
                            if value2 == nil {
                                azimutCell.infos = (info.0, value1, .degrees, nil, nil)
                            } else {
                                azimutCell.infos = (info.0, value2, value2 == nil ? nil : .degrees, value1, .degrees)
                            }
                        }
                    }
                }
            })
        self.disposables.append(disposable)
        disposable.disposed(by: disposeBag)
    }
    
    /// Observer bearing and fill the cells which need it
    private func observeGisementToCurrentPoint() {
        let disposable = NavigationManager.shared.gisementToCurrentPoint.asObservable()
            .bind(onNext: { [weak self] (bearing) in
                if NavigationManager.shared.point.value != nil {
                    if let indexes = self?.viewModel.dictTypeIndex[.gisement], let info = self?.viewModel.dataSources[indexes.0][indexes.1] {
                        var value1: String? = nil
                        var value2: String? = nil
                        var navUnit2: NavigationUnit? = nil
                        if bearing != nil {
                            if let b1 = bearing?.0 {
                                value1 = "\(b1.0)"
                            }
                            if let b2 = bearing?.1 {
                                value2 = "\(b2.0)"
                                navUnit2 = bearing?.1?.1
                            } else {
                                let point = NavigationManager.shared.point.value?.point
                                if let mark = point as? Mark, mark.buoys.count == 2, !NavigationManager.shared.usingMiddlePoint  {
                                    navUnit2 = .spDegrees
                                } else {
                                    navUnit2 = nil
                                }
                            }
                        }
                        self?.viewModel.dataSources[indexes.0][indexes.1] = navUnit2 == nil ? (info.0, value1, bearing?.0?.1, nil, nil) : (info.0, value2, navUnit2, value1, bearing?.0?.1)
                        if let gisementCell = self?.tableView?.cellForRow(at: IndexPath(row: indexes.1, section: indexes.0)) as? InformationCell {
                            gisementCell.infos = navUnit2 == nil ? (info.0, value1, bearing?.0?.1, nil, nil) : (info.0, value2, navUnit2, value1, bearing?.0?.1)
                        }
                    }
                }
            })
        self.disposables.append(disposable)
        disposable.disposed(by: disposeBag)
    }
    
    /// Observer azimut and fill the cells which need it
    private func observeAzimutToNextPoint() {
        let disposable = NavigationManager.shared.azimutNextPoint.asObservable()
            .bind(onNext: { [weak self] (bearing) in
                if NavigationManager.shared.point.value != nil {
                    if let info = self?.viewModel.dataSources[2][1] {
                        var value1: String? = nil
                        var value2: String? = nil
                        if bearing != nil {
                            if let b1 = bearing?.0 {
                                value1 = "\(b1)"
                            }
                            if let b2 = bearing?.1 {
                                value2 = "\(b2)"
                            }
                        }
                        self?.viewModel.dataSources[2][1] = value2 == nil ? (info.0, value1, .degrees, nil, nil) : (info.0, value2, value2 == nil ? nil : .degrees, value1, .degrees)
                        if let azimutCell = self?.tableView?.cellForRow(at: IndexPath(row: 1, section: 2)) as? InformationCell {
                            azimutCell.infos = value2 == nil ? (info.0, value1, .degrees, nil, nil) : (info.0, value2, value2 == nil ? nil : .degrees, value1, .degrees)
                        }
                    }
                }
            })
        self.disposables.append(disposable)
        disposable.disposed(by: disposeBag)
    }
    
    /// Observer bearing and fill the cells which need it
    private func observeGisementToNextPoint() {
        let disposable = NavigationManager.shared.gisementNextPoint.asObservable()
            .bind(onNext: { [weak self] (bearing) in
                if NavigationManager.shared.point.value != nil {
                    if let info = self?.viewModel.dataSources[2][0] {
                        var value1: String? = nil
                        var value2: String? = nil
                        if bearing != nil {
                            if let b1 = bearing?.0 {
                                value1 = "\(b1.0)"
                            }
                            if let b2 = bearing?.1 {
                                value2 = "\(b2.0)"
                            }
                        }
                        self?.viewModel.dataSources[2][0] = value2 == nil ? (info.0, value1, bearing?.0?.1, nil, nil) : (info.0, value2, bearing?.1?.1, value1, bearing?.0?.1)
                        if let gisementCell = self?.tableView?.cellForRow(at: IndexPath(row: 0, section: 2)) as? InformationCell {
                            gisementCell.infos = value2 == nil ? (info.0, value1, bearing?.0?.1 ?? nil, nil, nil) : (info.0, value2, bearing?.1?.1 ?? nil, value1, bearing?.0?.1 ?? nil)
                        }
                    }
                }
            })
        self.disposables.append(disposable)
        disposable.disposed(by: disposeBag)
    }
    
    private func observeNextSegment() {
        let nextSegmentInfos = self.viewModel.nextSegment()
        let distance = nextSegmentInfos?.0
        var info = self.viewModel.dataSources[1][1]
        if distance != nil {
            if distance!.1 == .meter || distance!.1 == .kiloMeter {
                self.viewModel.dataSources[1][1] = (info.0, "\(distance!.0.roundDown)", distance!.1 ?? .meter, nil, nil)
                if let distanceCell = self.tableView?.cellForRow(at: IndexPath(row: 1, section: 1)) as? InformationCell {
                    distanceCell.infos = (info.0, "\(distance!.0.roundDown)", distance!.1 ?? .meter, nil, nil)
                }
            } else {
                self.viewModel.dataSources[1][1] = (info.0, "\(distance!.0)", distance?.1 ?? .meter, nil, nil)
                if let distanceCell = self.tableView?.cellForRow(at: IndexPath(row: 1, section: 1)) as? InformationCell {
                    distanceCell.infos = (info.0, "\(distance!.0)", distance?.1 ?? .meter, nil, nil)
                }
            }
        }
        let bearing = nextSegmentInfos?.1
        info = self.viewModel.dataSources[1][2]
        if bearing != nil {
            self.viewModel.dataSources[1][2] = (info.0, "\(bearing!)", .degrees, nil, nil)
            if let bearingCell = self.tableView?.cellForRow(at: IndexPath(row: 2, section: 1)) as? InformationCell {
                bearingCell.infos = (info.0, "\(bearing!)", .degrees, nil, nil)
            }
        } else {
            self.viewModel.dataSources[1][2] = (info.0, nil, .degrees, nil, nil)
            if let bearingCell = self.tableView?.cellForRow(at: IndexPath(row: 2, section: 1)) as? InformationCell {
                bearingCell.infos = (info.0, nil, .degrees, nil, nil)
            }
        }
    }
    
    /// Observer distance and fill the cells which need it
    private func observeGapToSegmentRoute() {
        let disposable = NavigationManager.shared.gap.asObservable()
            .bind(onNext: { [weak self] (distance) in
                if NavigationManager.shared.point.value != nil {
                    if let info = self?.viewModel.dataSources[0][4] {
                        if distance != nil {
                            if distance!.0 != nil && distance!.0!.1 == .meter {
                                let d = distance!.0!.0.roundDown
                                var d2String: String? = nil
                                if distance!.1 != nil && distance!.1!.1 == .meter {
                                    d2String = "\(distance!.1!.0.roundDown)"
                                } else if distance!.1 != nil {
                                    d2String = "\(distance!.1!.0)"
                                }
                                self?.viewModel.dataSources[0][4] = d2String == nil ? (info.0, "\(d)", distance!.0!.1!, nil, nil) : (info.0, d2String, distance!.1?.1 ?? nil, "\(d)", distance!.0!.1!)
                                if let distanceCell = self?.tableView?.cellForRow(at: IndexPath(row: 4, section: 0)) as? InformationCell {
                                    distanceCell.infos = d2String == nil ? (info.0, "\(d)", distance!.0!.1!, nil, nil) : (info.0, d2String, distance!.1?.1! ?? nil, "\(d)", distance!.0!.1!)
                                }
                            } else {
                                var d2String: String? = nil
                                if distance!.1 != nil && distance!.1!.1 == .meter {
                                    d2String = "\(distance!.1!.0.roundDown)"
                                } else if distance!.1 != nil {
                                    d2String = "\(distance!.1!.0)"
                                }
                                self?.viewModel.dataSources[0][4] = d2String == nil ? (info.0, "\(distance!.0!.0)", distance!.0!.1, nil, nil) : (info.0, d2String, distance!.1?.1 ?? nil, "\(distance!.0!.0)", distance!.0!.1)
                                if let distanceCell = self?.tableView?.cellForRow(at: IndexPath(row: 4, section: 0)) as? InformationCell {
                                    distanceCell.infos = d2String == nil ? (info.0, "\(distance!.0!.0)", distance!.0!.1, nil, nil) : (info.0, d2String, distance!.1?.1! ?? nil, "\(distance!.0!.0)", distance!.0!.1)
                                }
                            }
                        } else {
                            self?.viewModel.dataSources[0][4] = (info.0, nil, Settings.shared.distanceUnit.value?.unit, nil, Settings.shared.distanceUnit.value?.unit)
                            if let distanceCell = self?.tableView?.cellForRow(at: IndexPath(row: 4, section: 0)) as? InformationCell {
                                distanceCell.infos = (info.0, nil, Settings.shared.distanceUnit.value?.unit, nil, Settings.shared.distanceUnit.value?.unit)
                            }
                        }
                    }
                }
            })
        self.disposables.append(disposable)
        disposable.disposed(by: disposeBag)
    }
    
    //MARK: - Navigation Center
    
    // Observe Depth and fill the cells which need it
    private func observeDepth() {
        let disposable = NmeaManager.default.depthMeters
            .asObservable()
            .bind(onNext: { [weak self] (depth) in
                if let info = self?.viewModel.dataSources[0][0] {
                    var value:String? = nil
                    if depth != nil {
                        value = "\(depth!)"
                    }
                    self?.viewModel.dataSources[0][0] = (info.0, value, NavigationUnit.meter, nil, nil)
                    if let depthCell = self?.tableView?.cellForRow(at: IndexPath(row: 0, section: 0)) as? InformationCell {
                        depthCell.infos = (info.0, value, NavigationUnit.meter, nil, nil)
                    }
                }
            })
        self.disposables.append(disposable)
        disposable.disposed(by: disposeBag)
    }
    
    private func observeMagneticHeading() {
        let disposable = NmeaManager.default.headingMagneticDegrees
            .asObservable()
            .bind(onNext: { [weak self] (heading) in
                if let info = self?.viewModel.dataSources[0][1] {
                    let headingString = heading != nil ? "\(Int(heading!))" : nil
                    self?.viewModel.dataSources[0][1] = (info.0, headingString, .degrees, nil, nil)
                    if let headingCell = self?.tableView?.cellForRow(at: IndexPath(row: 1, section: 0)) as? InformationCell {
                        headingCell.infos = (info.0, headingString, .degrees, nil, nil)
                    }
                }
            })
        self.disposables.append(disposable)
        disposable.disposed(by: disposeBag)
    }
    
    private func observeSpeedThroughWater() {
        if Settings.shared.speedUnit?.unit == NavigationUnit.kmPerHour {
            let disposable = NmeaManager.default.speedThroughWaterKmPerHour
                .asObservable()
                .bind(onNext: { [weak self] (speed) in
                    if let info = self?.viewModel.dataSources[0][2] {
                        let speedString = speed != nil ? "\(speed!)" : nil
                        self?.viewModel.dataSources[0][2] = (info.0, speedString, .kmPerHour, nil, nil)
                        if let speedCell = self?.tableView?.cellForRow(at: IndexPath(row: 2, section: 0)) as? InformationCell {
                            speedCell.infos = (info.0, speedString, .kmPerHour, nil, nil)
                        }
                    }
                })
            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
        } else {
            let disposable = NmeaManager.default.speedThroughWaterKnots
                .asObservable()
                .bind(onNext: { [weak self] (speed) in
                    if let info = self?.viewModel.dataSources[0][2] {
                        let speedString = speed != nil ? "\(speed!)" : nil
                        self?.viewModel.dataSources[0][2] = (info.0, speedString, .knots, nil, nil)
                        if let speedCell = self?.tableView?.cellForRow(at: IndexPath(row: 2, section: 0)) as? InformationCell {
                            speedCell.infos = (info.0, speedString, .knots, nil, nil)
                        }
                    }
                })
            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
        }
    }
    
    private func observeMaxSpeed10sNav() {
        if Settings.shared.speedUnit?.unit == NavigationUnit.kmPerHour {
            let disposable = NmeaManager.default.maxSpeedThroughWater10sKmPerHour
                .asObservable()
                .bind(onNext: { [weak self] (speed) in
                    if let info = self?.viewModel.dataSources[0][3] {
                        let speedString = speed != nil ? "\(speed!)" : nil
                        self?.viewModel.dataSources[0][3] = (info.0, speedString, .kmPerHour, nil, nil)
                        if let speedCell = self?.tableView?.cellForRow(at: IndexPath(row: 3, section: 0)) as? InformationCell {
                            speedCell.infos = (info.0, speedString, .kmPerHour, nil, nil)
                        }
                    }
                })
            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
        } else {
            let disposable = NmeaManager.default.maxSpeedThroughWater10sKnots
                .asObservable()
                .bind(onNext: { [weak self] (speed) in
                    if let info = self?.viewModel.dataSources[0][3] {
                        let speedString = speed != nil ? "\(speed!)" : nil
                        self?.viewModel.dataSources[0][3] = (info.0, speedString, .knots, nil, nil)
                        if let speedCell = self?.tableView?.cellForRow(at: IndexPath(row: 3, section: 0)) as? InformationCell {
                            speedCell.infos = (info.0, speedString, .knots, nil, nil)
                        }
                    }
                })
            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
        }
    }
    
    private func observeMaxSpeedNav() {
        if Settings.shared.speedUnit?.unit == NavigationUnit.kmPerHour {
            let disposable = NmeaManager.default.maxSpeedThroughWaterKmPerHour
                .asObservable()
                .bind(onNext: { [weak self] (speed) in
                    if let info = self?.viewModel.dataSources[0][4] {
                        let speedString = speed != nil ? "\(speed!)" : nil
                        self?.viewModel.dataSources[0][4] = (info.0, speedString, .kmPerHour, nil, nil)
                        if let speedCell = self?.tableView?.cellForRow(at: IndexPath(row: 4, section: 0)) as? InformationCell {
                            speedCell.infos = (info.0, speedString, .kmPerHour, nil, nil)
                        }
                    }
                })
            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
        } else {
            let disposable = NmeaManager.default.maxSpeedThroughWaterKnots
                .asObservable()
                .bind(onNext: { [weak self] (speed) in
                    if let info = self?.viewModel.dataSources[0][4] {
                        let speedString = speed != nil ? "\(speed!)" : nil
                        self?.viewModel.dataSources[0][4] = (info.0, speedString, .knots, nil, nil)
                        if let speedCell = self?.tableView?.cellForRow(at: IndexPath(row: 4, section: 0)) as? InformationCell {
                            speedCell.infos = (info.0, speedString, .knots, nil, nil)
                        }
                    }
                })
            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
        }
    }
    
    // True Wind
    
    private func observeWindDirection() {
        let disposable = NmeaManager.default.windDirection
            .asObservable()
            .bind(onNext: { [weak self] (angle) in
                if let info = self?.viewModel.dataSources[1][0] {
                    let angleWind = angle != nil ? "\(Int(angle!))" : nil
                    self?.viewModel.dataSources[1][0] = (info.0, angleWind, .degrees, nil, nil)
                    if let angleCell = self?.tableView?.cellForRow(at: IndexPath(row: 0, section: 1)) as? InformationCell {
                        angleCell.infos = (info.0, angleWind, .degrees, nil, nil)
                    }
                }
            })
        self.disposables.append(disposable)
        disposable.disposed(by: disposeBag)
    }
    
    private func observeSpeedTrueWind() {
        if Settings.shared.speedUnit?.unit == .kmPerHour {
            let disposable = NmeaManager.default.windTrueSpeedKmPerHour
                .asObservable()
                .bind(onNext: { [weak self] (speed) in
                    if let info = self?.viewModel.dataSources[1][1] {
                        let speedString = speed != nil ? "\(speed!)" : nil
                        self?.viewModel.dataSources[1][1] = (info.0, speedString, .kmPerHour, nil, nil)
                        if let speedCell = self?.tableView?.cellForRow(at: IndexPath(row: 1, section: 1)) as? InformationCell {
                            speedCell.infos = (info.0, speedString, .kmPerHour, nil, nil)
                        }
                    }
                })
            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
        } else {
            let disposable = NmeaManager.default.windTrueSpeedKnots
                .asObservable()
                .bind(onNext: { [weak self] (speed) in
                    if let info = self?.viewModel.dataSources[1][1] {
                        let speedString = speed != nil ? "\(speed!)" : nil
                        self?.viewModel.dataSources[1][1] = (info.0, speedString, .knots, nil, nil)
                        if let speedCell = self?.tableView?.cellForRow(at: IndexPath(row: 1, section: 1)) as? InformationCell {
                            speedCell.infos = (info.0, speedString, .knots, nil, nil)
                        }
                    }
                })
            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
        }
    }
    
    private func observeAngleTrueWind() {
        let disposable = NmeaManager.default.windTrueAngleDegrees
            .asObservable()
            .bind(onNext: { [weak self] (angle) in
                if let info = self?.viewModel.dataSources[1][2] {
                    let angleWind = angle != nil ? "\(Int(angle!.0))" : nil
                    self?.viewModel.dataSources[1][2] = (info.0, angleWind, angle?.1, nil, nil)
                    if let angleCell = self?.tableView?.cellForRow(at: IndexPath(row: 2, section: 1)) as? InformationCell {
                        angleCell.infos = (info.0, angleWind, angle?.1, nil, nil)
                    }
                }
            })
        self.disposables.append(disposable)
        disposable.disposed(by: disposeBag)
    }
    
    private func observeMaxSpeedTrueWind10s() {
        if Settings.shared.speedUnit?.unit == .kmPerHour {
            let disposable = NmeaManager.default.maxSpeedWind10sKmPerHour
                .asObservable()
                .bind(onNext: { [weak self] (speed) in
                    if let info = self?.viewModel.dataSources[1][3] {
                        let speedString = speed != nil ? "\(speed!)" : nil
                        self?.viewModel.dataSources[1][3] = (info.0, speedString, .kmPerHour, nil, nil)
                        if let speedCell = self?.tableView?.cellForRow(at: IndexPath(row: 3, section: 1)) as? InformationCell {
                            speedCell.infos = (info.0, speedString, .kmPerHour, nil, nil)
                        }
                    }
                })
            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
        } else {
            let disposable = NmeaManager.default.maxSpeedWind10sKnots
                .asObservable()
                .bind(onNext: { [weak self] (speed) in
                    if let info = self?.viewModel.dataSources[1][3] {
                        let speedString = speed != nil ? "\(speed!)" : nil
                        self?.viewModel.dataSources[1][3] = (info.0, speedString, .knots, nil, nil)
                        if let speedCell = self?.tableView?.cellForRow(at: IndexPath(row: 3, section: 1)) as? InformationCell {
                            speedCell.infos = (info.0, speedString, .knots, nil, nil)
                        }
                    }
                })
            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
        }
    }
    
    private func observeMaxSpeedTrueWind() {
        if Settings.shared.speedUnit?.unit == .kmPerHour {
            let disposable = NmeaManager.default.maxSpeedWindKmPerHour
                .asObservable()
                .bind(onNext: { [weak self] (speed) in
                    if let info = self?.viewModel.dataSources[1][4] {
                        let speedString = speed != nil ? "\(speed!)" : nil
                        self?.viewModel.dataSources[1][4] = (info.0, speedString, .kmPerHour, nil, nil)
                        if let speedCell = self?.tableView?.cellForRow(at: IndexPath(row: 4, section: 1)) as? InformationCell {
                            speedCell.infos = (info.0, speedString, .kmPerHour, nil, nil)
                        }
                    }
                })
            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
        } else {
            let disposable = NmeaManager.default.maxSpeedWindKnots
                .asObservable()
                .bind(onNext: { [weak self] (speed) in
                    if let info = self?.viewModel.dataSources[1][4] {
                        let speedString = speed != nil ? "\(speed!)" : nil
                        self?.viewModel.dataSources[1][4] = (info.0, speedString, .knots, nil, nil)
                        if let speedCell = self?.tableView?.cellForRow(at: IndexPath(row: 4, section: 1)) as? InformationCell {
                            speedCell.infos = (info.0, speedString, .knots, nil, nil)
                        }
                    }
                })
            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
        }
    }
    
    // Relative Wind
    
    private func observeSpeedRelativeWind() {
        if Settings.shared.speedUnit?.unit == .kmPerHour {
            let disposable = NmeaManager.default.windRelativeSpeedKmPerHour
                .asObservable()
                .bind(onNext: { [weak self] (speed) in
                    if let info = self?.viewModel.dataSources[2][0] {
                        let speedString = speed != nil ? "\(speed!)" : nil
                        self?.viewModel.dataSources[2][0] = (info.0, speedString, .kmPerHour, nil, nil)
                        if let speedCell = self?.tableView?.cellForRow(at: IndexPath(row: 0, section: 2)) as? InformationCell {
                            speedCell.infos = (info.0, speedString, .kmPerHour, nil, nil)
                        }
                    }
                })
            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
        } else {
            let disposable = NmeaManager.default.windRelativeSpeedKnots
                .asObservable()
                .bind(onNext: { [weak self] (speed) in
                    if let info = self?.viewModel.dataSources[2][0] {
                        let speedString = speed != nil ? "\(speed!)" : nil
                        self?.viewModel.dataSources[2][0] = (info.0, speedString, .knots, nil, nil)
                        if let speedCell = self?.tableView?.cellForRow(at: IndexPath(row: 0, section: 2)) as? InformationCell {
                            speedCell.infos = (info.0, speedString, .knots, nil, nil)
                        }
                    }
                })
            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
        }
    }
    
    private func observeAngleRelativeWind() {
        let disposable = NmeaManager.default.windRelativeAngleDegrees
            .asObservable()
            .bind(onNext: { [weak self] (angle) in
                if let info = self?.viewModel.dataSources[2][1] {
                    if angle != nil {
                        self?.viewModel.dataSources[2][1] = (info.0, "\(Int(angle!.0))", angle!.1, nil, nil)
                        if let angleCell = self?.tableView?.cellForRow(at: IndexPath(row: 1, section: 2)) as? InformationCell {
                            angleCell.infos = (info.0, "\(Int(angle!.0))", angle!.1, nil, nil)
                        }
                    } else {
                        self?.viewModel.dataSources[2][1] = (info.0, nil, nil, nil, nil)
                        if let angleCell = self?.tableView?.cellForRow(at: IndexPath(row: 1, section: 2)) as? InformationCell {
                            angleCell.infos = (info.0, nil, nil, nil, nil)
                        }
                    }
                }
            })
        self.disposables.append(disposable)
        disposable.disposed(by: disposeBag)
    }
    
    // Temperature
    
    // Observe Temperature and fill the cells which need it
    private func observeWaterTemperature() {
        let disposable = NmeaManager.default.waterTemperatureCelcius
            .asObservable()
            .bind(onNext: { [weak self] (water) in
                if let info = self?.viewModel.dataSources[3][0] {
                    var value:String? = nil
                    if water != nil {
                        value = "\(water!.formatted)"
                    }
                    self?.viewModel.dataSources[3][0] = (info.0, value, info.2, nil, nil)
                    if let waterCell = self?.tableView?.cellForRow(at: IndexPath(row: 0, section: 3)) as? InformationCell {
                        waterCell.infos = (info.0, value, info.2, nil, nil)
                    }
                }
            })
        self.disposables.append(disposable)
        disposable.disposed(by: disposeBag)
    }
    
    // Observe Temperature and fill the cells which need it
    private func observeAirTemperature() {
        let disposable = NmeaManager.default.airTemperatureCelcius
            .asObservable()
            .bind(onNext: { [weak self] (water) in
                if let info = self?.viewModel.dataSources[3][1] {
                    let waterString = water != nil ? "\(water!)" : nil
                    self?.viewModel.dataSources[3][1] = (info.0, waterString, info.2, nil, nil)
                    if let waterCell = self?.tableView?.cellForRow(at: IndexPath(row: 1, section: 3)) as? InformationCell {
                        waterCell.infos = (info.0, waterString, info.2, nil, nil)
                    }
                }
            })
        self.disposables.append(disposable)
        disposable.disposed(by: disposeBag)
    }
    
    // MARK: - Telltales
    //Telltales manager observer
    func tellTaleManager(didUpdateTellTales updatedTellTales: [TellTale]) {
        self.messageLabel?.removeFromSuperview()
        self.messageLabel = nil
        self.tableView?.isHidden = false
        
        updatedTellTales.filter({$0.isConfigured}).forEach {
            if let sectionNb = self.tellTales.firstIndex(of: $0) {
                if !$0.isEnabled || !$0.isOnline {
                    self.tellTales.remove(at: sectionNb)
                    self.viewModel.dataSources.remove(at: sectionNb)
                    self.viewModel.sectionHeaders.removeValue(forKey: sectionNb)
                    self.tableView?.deleteSections([sectionNb], with: .none)
                } else {
//                    print("===\(self.dataSources)")
                    let infoStatus = self.viewModel.dataSources[sectionNb][0]
                    self.viewModel.dataSources[sectionNb][0] = (infoStatus.0, "\($0.status.description)", infoStatus.2, nil, nil)
                    if let statusCell = self.tableView?.cellForRow(at: IndexPath(row: 0, section: sectionNb)) as? InformationCell {
                        statusCell.infos = (infoStatus.0, "\($0.status.description)", infoStatus.2, nil, nil)
                    }
                    let infoAngle = self.viewModel.dataSources[sectionNb][1]
                    self.viewModel.dataSources[sectionNb][1] = (infoAngle.0, "\(Int($0.angle ?? 0))", infoAngle.2, nil, nil)
                    if let angleCell = self.tableView?.cellForRow(at: IndexPath(row: 1, section: sectionNb)) as? InformationCell {
                        angleCell.infos = (infoAngle.0, "\(Int($0.angle ?? 0))", infoAngle.2, nil, nil)
                    }
                    let infoMediane = self.viewModel.dataSources[sectionNb][2]
                    self.viewModel.dataSources[sectionNb][2] = (infoMediane.0, "\(Int($0.medianAngle ?? 0))", infoMediane.2, nil, nil)
                    if let medianeCell = self.tableView?.cellForRow(at: IndexPath(row: 2, section: sectionNb)) as? InformationCell {
                        medianeCell.infos = (infoMediane.0, "\(Int($0.medianAngle ?? 0))", infoMediane.2, nil, nil)
                    }
                    let infoAverage = self.viewModel.dataSources[sectionNb][3]
                    self.viewModel.dataSources[sectionNb][3] = (infoAverage.0, "\(Int($0.averageAngle ?? 0))", infoAverage.2, nil, nil)
                    if let averageCell = self.tableView?.cellForRow(at: IndexPath(row: 3, section: sectionNb)) as? InformationCell {
                        averageCell.infos = (infoAverage.0, "\(Int($0.averageAngle ?? 0))", infoAverage.2, nil, nil)
                    }
                    let infoStandardDev = self.viewModel.dataSources[sectionNb][4]
                    self.viewModel.dataSources[sectionNb][4] = (infoStandardDev.0, "\(Int($0.standardDeviationAngle ?? 0))", infoStandardDev.2, nil, nil)
                    if let standardDevCell = self.tableView?.cellForRow(at: IndexPath(row: 4, section: sectionNb)) as? InformationCell {
                        standardDevCell.infos = (infoStandardDev.0, "\(Int($0.standardDeviationAngle ?? 0))", infoStandardDev.2, nil, nil)
                    }
                    let infoVariationMax = self.viewModel.dataSources[sectionNb][5]
                    self.viewModel.dataSources[sectionNb][5] = (infoVariationMax.0, "\(Int($0.variationAngle ?? 0))", infoVariationMax.2, nil, nil)
                    if let variationMaxCell = self.tableView?.cellForRow(at: IndexPath(row: 5, section: sectionNb)) as? InformationCell {
                        variationMaxCell.infos = (infoVariationMax.0, "\(Int($0.variationAngle ?? 0))", infoVariationMax.2, nil, nil)
                    }
                    let infoMedianSamplingTime = self.viewModel.dataSources[sectionNb][6]
                    self.viewModel.dataSources[sectionNb][6] = (infoMedianSamplingTime.0, "\($0.medianSamplingTime)", infoMedianSamplingTime.2, nil, nil)
                    if let medianSamplingTimeCell = self.tableView?.cellForRow(at: IndexPath(row: 6, section: sectionNb)) as? InformationCell {
                        medianSamplingTimeCell.infos = (infoMedianSamplingTime.0, "\($0.medianSamplingTime.description)", infoMedianSamplingTime.2, nil, nil)
                    }
                    self.tellTales[sectionNb] = $0
                }
            } else {
                if $0.isEnabled && $0.isOnline {
                    var tellTaleDataSource: [(String?, String?, NavigationUnit?,String?, NavigationUnit?)] = []
                    tellTaleDataSource.append((NSLocalizedString("state", bundle: bundle, comment: ""), $0.status.description, nil, nil, nil))
                    tellTaleDataSource.append((NSLocalizedString("angle", bundle: bundle, comment: ""), "\(Int($0.angle ?? 0))", .degrees, nil, nil))
                    tellTaleDataSource.append((NSLocalizedString("mediane", bundle: bundle, comment: ""), "\(Int($0.medianAngle ?? 0))", .degrees, nil, nil))
                    tellTaleDataSource.append((NSLocalizedString("average", bundle: bundle, comment: ""), "\(Int($0.averageAngle ?? 0))", .degrees, nil, nil))
                    tellTaleDataSource.append((NSLocalizedString("standard_deviation", bundle: bundle, comment: ""), "\(Int($0.standardDeviationAngle ?? 0))", .degrees, nil, nil))
                    tellTaleDataSource.append((NSLocalizedString("variation_max", bundle: bundle, comment: ""), "\(Int($0.variationAngle ?? 0))", .degrees, nil, nil))
                    tellTaleDataSource.append((NSLocalizedString("sample", bundle: bundle, comment: ""), "\($0.medianSamplingTime)", .second, nil, nil))
                    self.tellTales.append($0)
                    self.viewModel.dataSources.append(tellTaleDataSource)
                    self.viewModel.sectionHeaders[self.viewModel.dataSources.count - 1] = ($0.name, nil, nil, nil)
                    self.tableView?.insertSections([self.viewModel.dataSources.count - 1], with: .none)
                }
            }
        }
        if self.tellTales.isEmpty {
            self.tableView?.isHidden = true
            labelNoData(message: NSLocalizedString("no_telltales_available", bundle: bundle, comment: ""))
            return
        }
    }
    
    func tellTaleManager(didDeleteTellTales deletedTellTales: [TellTale]) {
        deletedTellTales.forEach {
            if let sectionNb = self.tellTales.firstIndex(of: $0) {
                self.tellTales.remove(at: sectionNb)
                self.viewModel.dataSources.remove(at: sectionNb)
                self.viewModel.sectionHeaders.removeValue(forKey: sectionNb)
                self.tableView?.deleteSections([sectionNb], with: .none)
            }
        }
        if self.tellTales.isEmpty {
            self.tableView?.isHidden = true
            labelNoData(message: NSLocalizedString("no_telltales_available", bundle: bundle, comment: ""))
            return
        }
    }
    
    // MARK: - Seascape
    func mapDataInfo(beacons: [Beacon], currentLoc: CLLocation, section: Int) {
        beacons.enumerated().forEach({
            let infoBeacon = mapDataInfoDetails(beacon: $0.1, currentLoc: currentLoc)
            if self.viewModel.dataSources[section].count == 1 && self.viewModel.dataSources[section][0].0 == nil { // remove no beacons cell
                self.viewModel.dataSources[section].remove(at: 0)
                self.tableView?.deleteRows(at: [IndexPath(row: 0, section: section)], with: .none)
            }
            if self.viewModel.dataSources[section].count > $0.0 {
                self.viewModel.dataSources[section][$0.0] = infoBeacon
                if let beaconCell = self.tableView?.cellForRow(at: IndexPath(row: $0.0, section: section)) as? SeascapeTableViewCell {
                    beaconCell.infos = infoBeacon
                }
            } else {
                self.viewModel.dataSources[section].append(infoBeacon)
                self.tableView?.insertRows(at: [IndexPath(row: $0.0, section: section)], with: .none)
            }
        })
        if beacons.count < self.viewModel.dataSources[section].count && self.viewModel.dataSources[section][0].0 != nil{
            let numberElementsToRemove = self.viewModel.dataSources[section].count - beacons.count
            for i in 0..<numberElementsToRemove {
                let lastIndex = self.viewModel.dataSources[section].count - 1
                self.viewModel.dataSources[section].removeLast()
                self.tableView?.deleteRows(at: [IndexPath(row: lastIndex, section: section)], with: .none)
            }
        }
        if beacons.count == 0 && self.tableView?.numberOfRows(inSection: section) == 0 {
            if self.viewModel.dataSources[section].count == 0 {
                self.viewModel.dataSources[section].append((nil, nil, nil, nil, nil))
            }
            self.tableView?.insertRows(at: [IndexPath(row: 0, section: section)], with: .none)
        }
    }
    
    func mapDataInfoDetails(beacon: Beacon, currentLoc: CLLocation) ->  (String?, String?, NavigationUnit?, String?, NavigationUnit?) {
        var infoBeacon: (String?, String?, NavigationUnit?, String?, NavigationUnit?) = (nil, nil, nil, nil, nil)
        infoBeacon.0 = "\(beacon.type.description) - \(beacon.name)"
        if let distance = Utils.tupleDistance(floatDistance: beacon.distance(currentLoc: currentLoc)) {
            if distance.1 != nil && distance.1 == .meter {
                infoBeacon.1 = String(distance.0.roundDown)
            } else {
                infoBeacon.1 = String(distance.0)
            }
            infoBeacon.2 = distance.1
        }
        if let gisement = beacon.bearing(currentLoc: currentLoc)?[1] {
            infoBeacon.3 = String(gisement.0)
            infoBeacon.4 = gisement.1
        }
        return infoBeacon
    }
    
    // MapDataManagerObserver
    func mapDataManager(didUpdateBeacons beaconsFront: [Beacon], beaconsBehind: [Beacon]) {
        guard let currentLoc = LocationManager.shared.getCurrentPosition() else {
            return
        }
        if beaconsFront.count == 0 && beaconsBehind.count == 0 {
            if currentLoc.course == -1 {
                self.viewModel.messageText = NSLocalizedString("no_data_available", bundle: bundle, comment: "")
            } else {
                self.viewModel.messageText = NSLocalizedString("no_download_data", bundle: bundle, comment: "")
            }
            if self.tableView?.numberOfSections != 0 {
                self.labelNoData(message: self.viewModel.messageText!)
                // Remove sections
                self.viewModel.dataSources.removeAll()
                self.tableView?.deleteSections([0,1], with: .none)
            } else {
                self.labelNoData(message: self.viewModel.messageText!)
            }
            return
        } else {
            if self.viewModel.dataSources.count == 0 {
                self.viewModel.setUpGeographics()
                self.labelNoData(message: "")
                self.tableView?.insertSections([0,1], with: .none)
            }
            mapDataInfo(beacons: beaconsFront, currentLoc: currentLoc, section: 0)
            mapDataInfo(beacons: beaconsBehind, currentLoc: currentLoc, section: 1)
        }
    }
    
    func mapDataManager(didDeleteBeacons deletedBeacons: [Beacon]) {
        
    }
    
    //MARK: - Setup screens
    private func setupUI() {
        self.tableView?.backgroundColor = Utils.primaryColor
        self.view.backgroundColor = Utils.primaryColor
        self.tableView?.tableFooterView = UIView(frame: .zero)
    }
                
    public func labelNoData(message: String) {
        messageLabel?.removeFromSuperview()
        messageLabel = UILabel()
        messageLabel!.text = message
        messageLabel!.translatesAutoresizingMaskIntoConstraints = false
        messageLabel!.lineBreakMode = .byWordWrapping
        messageLabel!.numberOfLines = 0
        messageLabel!.textAlignment = .center
        messageLabel!.textColor = .white
        messageLabel!.font =  UIFont.systemFont(ofSize: 20)
        
        self.view.addSubview(messageLabel!)
        
        messageLabel!.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        messageLabel!.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        messageLabel!.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
    //MARK: - TableView
    public func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.dataSources.count
    }
        
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch self.type {
        case .route, .current:
            return 50
        case .navigationCenter, .telltales, .seascape:
            return 40
        default:
            return CGFloat.leastNonzeroMagnitude
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.type == .seascape {
            return 94
        }
        return 44
    }
        
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = self.tableView?.dequeueReusableHeaderFooterView(withIdentifier: "NavigationHeaderTableViewIdentifier" ) as! NavigationHeaderTableView
        headerView.customizeHeader()
        headerView.nameLabel.text = self.viewModel.sectionHeaders[section]?.0
        headerView.portLabel.text = self.viewModel.sectionHeaders[section]?.1
        headerView.starboardLabel.text = self.viewModel.sectionHeaders[section]?.2
        headerView.accessibilityTraits = .header

        if self.type == .telltales {
            let telltale = self.tellTales[section]
            headerView.accessibilityLabel = "\(telltale.name), \(telltale.status.description), \(Int(telltale.angle ?? 0)) \(NavigationUnit.degrees.description), \(NSLocalizedString("mediane", bundle: bundle, comment: "")) \(Int(telltale.medianAngle ?? 0)) \(NavigationUnit.degrees.description), \(NSLocalizedString("sample", bundle: bundle, comment: "")) \(telltale.medianSamplingTime) \(NavigationUnit.second.description)"
        } else {
            if let _ = self.viewModel.sectionHeaders[section]?.1, let _ = self.viewModel.sectionHeaders[section]?.2, let type = self.viewModel.sectionHeaders[section]?.3 {
                if NavigationManager.shared.usingMiddlePoint && section == 0 {
                    headerView.portLabel.text = NSLocalizedString("middle", bundle: bundle, comment: "")
                    headerView.starboardLabel.text = nil
                }
                headerView.accessibilityLabel = "\(self.viewModel.sectionHeaders[section]?.0 ?? ""). de type \(NSLocalizedString(type.lowercased(), bundle: bundle, comment: ""))"
            } else if let portLabel = self.viewModel.sectionHeaders[section]?.1 {
                headerView.accessibilityLabel = "\(self.viewModel.sectionHeaders[section]?.0 ?? ""). \(NSLocalizedString("toLetAt", bundle: bundle, comment: "")) \(portLabel)"
            } else {
                headerView.accessibilityLabel = "\(self.viewModel.sectionHeaders[section]?.0 ?? "")."
            }
            
        }
        return headerView
    }
        
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.dataSources[section].count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch self.type {
        case .seascape:
            if self.viewModel.dataSources[indexPath.section].count == 1 &&  self.viewModel.dataSources[indexPath.section][indexPath.row].0 == nil {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "noBeaconsCell") as? UITableViewCell else {
                    return UITableViewCell()
                }
                cell.backgroundColor = Utils.primaryColor
                cell.textLabel?.textColor = Utils.reverseColor
                cell.textLabel?.text = NSLocalizedString("no_beacons", bundle: bundle, comment: "")
                return cell
            }
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SeascapeCellIdentifier") as? SeascapeTableViewCell else {
                return UITableViewCell()
            }
            
            // Infos = (Name of the info, value of the info, unit of the info)
            let infos = self.viewModel.dataSources[indexPath.section][indexPath.row]
            cell.infos = infos
            cell.customizeCell()
            return cell
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "InformationsCellIdentifier") as? InformationCell else {
                return UITableViewCell()
            }
            
            // Infos = (Name of the info, value of the info, unit of the info)
            let infos = self.viewModel.dataSources[indexPath.section][indexPath.row]
            cell.infos = infos
            cell.customizeCell()
            return cell
        }
    }
}
