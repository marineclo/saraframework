//
//  SelectTableViewController.swift
//  SARAFramework
//
//  Created by Marine on 15.07.21.
//

import UIKit
import RxSwift
import RxCocoa

public class SelectTableViewController: UIViewController, SelectButtonCellDelegate, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, HeaderTableViewDelegate {

    // MARK: - IBOutlets
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var okButton: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet var noNavigationBarConstraint: NSLayoutConstraint!
    @IBOutlet var searchBarConstraint: NSLayoutConstraint!
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: SelectTableViewController.classForCoder())
    var selectedIndex = BehaviorRelay<IndexPath?>(value: nil)
    var objects: [AnyObject]?
    var titleNavigationBar: String?
    public var titleTableHeader: String?
    var filteredData: [AnyObject]?
    public var displayNavigationBar: Bool = true
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if self.parent as? AddPointSegmentedViewController != nil || !displayNavigationBar {
            self.navigationBar.isHidden = true
            self.noNavigationBarConstraint.isActive = false
        } else {
            self.noNavigationBarConstraint.isActive = true
            self.navigationBar.topItem?.title = titleNavigationBar
            self.cancelButton.title = NSLocalizedString("cancel", bundle: bundle, comment: "")
            self.okButton.title = NSLocalizedString("ok", bundle: bundle, comment: "")
        }
        
        self.tableView.customize()
        self.tableView.register(UINib(nibName: "HeaderTableView", bundle: Bundle(for: SelectTableViewController.classForCoder())), forHeaderFooterViewReuseIdentifier: "HeaderTableView")
        self.tableView?.register(UINib(nibName: "SelectButtonCell", bundle: Bundle(for: SelectTableViewController.classForCoder())), forCellReuseIdentifier: "chooseObjectCell")
        if #available(iOS 15, *) { // To remove gap between header and table view
            self.tableView?.sectionHeaderTopPadding = 0
        }
        
        searchBarConstraint.isActive = false
        searchBar.isHidden = true
        
        filteredData = objects
        
        selectedIndex.subscribe(onNext: { index in
            if let parentVC = self.parent as? AddPointSegmentedViewController {
                parentVC.validateButton.isEnabled = index != nil
            } else {
                self.okButton.isEnabled = index != nil
            }
        })
    }
    
    // MARK: - UITableView
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredData?.count ?? 0
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "chooseObjectCell") as? SelectButtonCell else {
            return UITableViewCell()
        }
        cell.delegate = self
        guard let obj = filteredData?[indexPath.row] else {
            return UITableViewCell()
        }
        if let beacon = obj as? Beacon {
            cell.titleLabel?.text = "\(beacon.type.description) \(beacon.name)"
        } else if let point = obj as? PointRealm {
            cell.titleLabel?.text = point.name
        }
        cell.idElementSelected = String(indexPath.row)
        cell.selectionStyle = .none
        let bundle = Utils.bundle(anyClass: self.classForCoder)
        if self.selectedIndex.value == indexPath {
            cell.selectButton?.setImage(UIImage(named: "globe", in: bundle, compatibleWith: nil), for: .normal)
        } else {
            cell.selectButton?.setImage(UIImage(named: "circle", in: bundle, compatibleWith: nil), for: .normal)
        }
        cell.selectButton?.tintColor = Utils.tintIcon
        cell.selectButton?.isAccessibilityElement = false
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedIndex.accept(indexPath)
        let bundle = Utils.bundle(anyClass: self.classForCoder)
        if let cell = tableView.cellForRow(at: indexPath) as? SelectButtonCell  {
            cell.selectButton?.setImage(UIImage(named: "globe", in: bundle, compatibleWith: nil), for: .normal)
        }
    }
    
    public func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        
        if let indexPathForSelectedRow = self.selectedIndex.value {
            // Deselect current selected row
            tableView.deselectRow(at: indexPathForSelectedRow, animated: false)
            if let cell = tableView.cellForRow(at: indexPathForSelectedRow) as? SelectButtonCell {
                cell.selectButton?.setImage(UIImage(named: "circle", in: Utils.bundle(anyClass: self.classForCoder), compatibleWith: nil), for: .normal)
            }
            
            // If we're on the same row, disable the validation
            if indexPathForSelectedRow == indexPath {
                self.selectedIndex.accept(nil)
                return nil
            }
        }
        return indexPath
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderTableView" ) as! HeaderTableView
        returnedView.customizeHeader(titleName: titleTableHeader ?? "")
        returnedView.customizeHeader(titleName: titleTableHeader ?? "", image: UIImage(named: "search", in: bundle, compatibleWith: nil), accessibleText: NSLocalizedString("search", bundle: bundle, comment: ""), buttonIsAccessible: true)
        returnedView.delegate = self
        return returnedView
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    // MARK: - IBActions
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func okAction(_ sender: Any) {
        if let selectedRow = self.selectedIndex.value?.row {
            let object = filteredData![selectedRow]
//            let when = DispatchTime.now() + 3
//            DispatchQueue.main.asyncAfter(deadline: when) {
//                UIAccessibility.post(notification: UIAccessibility.Notification.announcement, argument: String(format: NSLocalizedString("add_point_success", bundle: self.bundle, comment: ""), buoy.name))
//            }
            self.dismiss(animated: true, completion: {
                  NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalChooseObjectIsDimissed"), object: nil, userInfo: ["object" : object])
            })
        }
    }
    
    // MARK: - SelectButtonCellDelegate
    public func didSelectCell(id: String) {
        guard let idInt = Int(id) else {
            return
        }
        let indexPathSelected = IndexPath(item: idInt, section: 0)
        var indexToReload: [IndexPath]  = []
        indexToReload.append(indexPathSelected)
        if self.selectedIndex.value == indexPathSelected { // We are on the same row disable it
            self.selectedIndex.accept(nil)
        } else {
            if let oldSelectedIndex = self.selectedIndex.value {
                indexToReload.append(oldSelectedIndex)
            }
            self.selectedIndex.accept(indexPathSelected)
        }
        self.tableView.reloadRows(at: indexToReload, with: .none)
    }
    
    // MARK: - HeaderTableViewDelegate
    func clickOnImage() {
        // Display search bar
        searchBarConstraint.isActive = true
        searchBar.isHidden = false
        searchBar.becomeFirstResponder()
    }
    
    // MARK: - UISearchBarDelegate
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredData = searchText.isEmpty ? objects : objects?.filter({ (obj) -> Bool in
            var dataString: String = ""
            if let beacon = obj as? Beacon {
                dataString = "\(beacon.type.description) \(beacon.name)"
            } else if let point = obj as? PointRealm {
                dataString = point.name
            }
            return dataString.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        })
        tableView.reloadData()
    }
    
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    public func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        DispatchQueue.main.async {
            if let cancelButton : UIButton = searchBar.value(forKey: "cancelButton") as? UIButton{
                cancelButton.isEnabled = true
                cancelButton.isAccessibilityElement = true
            }
        }
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.resignFirstResponder()
        searchBarConstraint.isActive = false
        searchBar.isHidden = true
    }
    
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
