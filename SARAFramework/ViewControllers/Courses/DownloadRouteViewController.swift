//
//  DownloadRouteViewController.swift
//  SARAFramework
//
//  Created by Marine on 14.09.23.
//

import UIKit

public class DownloadRouteViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, FirebaseManagerDelegate {
        
    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchBarConstraint: NSLayoutConstraint!
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: DownloadRouteViewController.classForCoder())
    var myCourses: [Course]?
    var availableCourse: [Course]?
    let cellId = "cellId"

    public override func viewDidLoad() {
        super.viewDidLoad()
        
        FirebaseManager.shared.delegate = self
        
        // Tableview customize
        self.tableView.customize()
        self.tableView.register(UINib(nibName: "HeaderTableView", bundle: Bundle(for: SubscriptionViewController.classForCoder())), forHeaderFooterViewReuseIdentifier: "HeaderTableView")
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        if #available(iOS 15, *) { // To remove gap between header and table view
            self.tableView?.sectionHeaderTopPadding = 0
        }
        
        searchBarConstraint.isActive = false
        searchBar.isHidden = true
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        myCourses = FirebaseManager.shared.firebaseCourses.filter({ $0.status == .shared})
        availableCourse = FirebaseManager.shared.firebaseCourses.filter({ $0.status != .shared})
        self.tableView.reloadData()
    }
    
    // MARK: - Table view data source
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return myCourses?.count ?? 0
        } else if section == 1 {
            return availableCourse?.count ?? 0
        }
        return 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        let name = indexPath.section == 0 ? myCourses?[indexPath.row].name : availableCourse?[indexPath.row].name
        if #available(iOS 14.0, *) {
            var config = UIListContentConfiguration.cell()
            config.text = name
            cell.contentConfiguration = config
        } else { // TextLabel Deprecated ios 14
            // Fallback on earlier versions
            cell.textLabel?.text = name
        }
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Route", bundle: bundle)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "DownloadRouteDetailsViewController") as? DownloadRouteDetailsViewController else {
                return
        }
        if indexPath.section == 0 {
            vc.course = myCourses?[indexPath.row]
        } else {
            vc.course = availableCourse?[indexPath.row]
        }
        self.present(vc, animated: true, completion: nil)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderTableView" ) as! HeaderTableView
        let titleHeader = section == 0 ? Utils.localizedString(forKey: "my_routes", app: "SARANav") : Utils.localizedString(forKey: "other_routes", app: "SARANav")
        returnedView.customizeHeader(titleName: titleHeader ?? "", image: nil, accessibleText: nil, buttonIsAccessible: false)
        returnedView.titleHeader.accessibilityTraits = .header
        return returnedView
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    //MARK: - FirebaseManagerDelegate
    public func courseChanged(course: Course?) {
        //Do nothing
    }
    
    public func courseRemoved(courseId: String) { // It's the name that is passed
        // Delete course from the course
        if let index = self.myCourses?.firstIndex(where: { $0.name == courseId}) {
            self.myCourses?.remove(at: index)
            self.tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .none)
        } else if let index = self.availableCourse?.firstIndex(where: { $0.name == courseId}) {
            self.availableCourse?.remove(at: index)
            self.tableView.deleteRows(at: [IndexPath(row: index, section: 1)], with: .none)
        }
    }
    
    public func courseAdded(course: Course?) {
        guard course != nil else { return }
        if course!.status == .shared {
            self.myCourses?.insert(course!, at: 0)
            self.tableView.reloadSections(IndexSet(integer: 0), with: .none)
        } else {
            self.availableCourse?.insert(course!, at: 0)
            self.tableView.reloadSections(IndexSet(integer: 1), with: .none)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
