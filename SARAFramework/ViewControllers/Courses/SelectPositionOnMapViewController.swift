//
//  SelectPositionOnMapViewController.swift
//  SARA Croisiere
//
//  Created by Marine on 19.04.20.
//  Copyright © 2020 GraniteApps. All rights reserved.
//

import UIKit
import MapKit
import RxSwift

public class SelectPositionOnMapViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var validateButton: UIBarButtonItem!
    @IBOutlet weak var titleNavigation: UINavigationItem!
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: SelectPositionOnMapViewController.classForCoder())
    public var annotation: MapAnnotation?
    var annotationSecondBuoy: MapAnnotation?
    let disposeBag = DisposeBag()
    var point: PointRealm?
    var radius: Double?
    
    // MARK: - IBActions
    @IBAction func selectPoint(_ sender: UILongPressGestureRecognizer) {
        // Do not generate pins many times during long press.
        if sender.state != UIGestureRecognizer.State.began {
            return
        }
        
        // Get the coordinates of the point you pressed long.
        let location = sender.location(in: self.mapView)
        
        // Convert location to CLLocationCoordinate2D.
        let myCoordinate: CLLocationCoordinate2D = self.mapView.convert(location, toCoordinateFrom: self.mapView)
        
        // Generate pins.
        let myPin: MapAnnotation = MapAnnotation(latitude: myCoordinate.latitude, longitude: myCoordinate.longitude, type: annotation?.type, isCurrent: true)
        
        // Remove previous annotation
        self.mapView.removeAnnotations(self.mapView.annotations.filter({$0 is MapAnnotation && ($0 as! MapAnnotation).isCurrent}))
        
        // Remove previous radius
        if let overlayRadius = self.mapView.overlays.first(where: {$0.title == "distanceThreshold"}) {
            self.mapView.removeOverlay(overlayRadius)
        }
        
        // Added pin to MapView.
        self.mapView.addAnnotation(myPin)
        annotation = myPin
        
        // Add circle
        if radius != nil {
            let circle = MKCircle(center: myCoordinate, radius: radius!)
            circle.title = "distanceThreshold"
            self.mapView.addOverlay(circle)
        }
        
        // Enable ok button
        self.validateButton.isEnabled = true
    }

    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func validate(_ sender: Any) {
        if let annotation = annotation {
            self.dismiss(animated: true, completion: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalSelectOnMapIsDimissed"), object: nil, userInfo: ["annotation" : annotation])
            })
        }
    }
    
    // MARK: -
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // Set a delegate.
        self.mapView.delegate = self
        
        
        // Designate center point.
        var center: CLLocationCoordinate2D?
        if annotation != nil && !annotation!.longitude.isNaN && !annotation!.latitude.isNaN {
            center = CLLocationCoordinate2DMake(annotation!.coordinate.latitude, annotation!.coordinate.longitude)
            if point != nil {
                MapHelper.getAllMapAnnotations(map: self.mapView, pointsDisplayed: [point!])
            } else {
                MapHelper.getAllMapAnnotations(map: self.mapView, pointsDisplayed: [])
            }
            // Add circle for radius
            if radius != nil {
                let circle = MKCircle(center: annotation!.coordinate, radius: radius!)
                circle.title = "distanceThreshold"
                self.mapView.addOverlay(circle)
            }
            self.mapView.addAnnotation(annotation!)
        } else if let currentPosition = LocationManager.shared.getCurrentPosition() {
            center = CLLocationCoordinate2DMake(currentPosition.coordinate.latitude, currentPosition.coordinate.longitude)
            MapHelper.getAllMapAnnotations(map: self.mapView, pointsDisplayed: [])
        }
        if let annotation = annotationSecondBuoy {
            self.mapView.addAnnotation(annotation)
        }
        if center != nil {
            // Set center point in MapView.
            self.mapView.setCenter(center!, animated: true)
            
            // Specify the scale (display area).
            let mySpan: MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
            let myRegion: MKCoordinateRegion = MKCoordinateRegion(center: center!, span: mySpan)
            
            // Add region to MapView.
            self.mapView.region = myRegion
        }
        
        self.cancelButton.title = NSLocalizedString("cancel", bundle: bundle, comment: "")
        self.validateButton.title = NSLocalizedString("ok", bundle: bundle, comment: "")
        self.titleNavigation.title = NSLocalizedString("select_on_map", bundle: bundle, comment: "")
        
        // Disable validate button if there is no annotation
        self.validateButton.isEnabled = false
        
        LocationManager.shared.locationVariable
        .withPrevious()
        .subscribe(onNext: { [weak self] (previousLocation, newLocation) in
            guard newLocation != nil && self != nil else {
                self!.mapView.removeAnnotations(self!.mapView.annotations.filter({$0 is MKUserLocation && ($0 as! MapAnnotation).type == .userLocation}))
                return
            }
            if previousLocation != newLocation {
                if !Settings.shared.usePhoneGPS {
                    self!.mapView.showsUserLocation = false
                    // Remove annotation with previous position
                    let previousPosAnnotations = self!.mapView.annotations.filter({$0 is MapAnnotation && ($0 as! MapAnnotation).type == .userLocation})
                    self!.mapView.removeAnnotations(previousPosAnnotations)
                    
                    let annotation = MapAnnotation(latitude: Double(newLocation!.coordinate.latitude), longitude: Double(newLocation!.coordinate.longitude), type: .userLocation)
                    self!.mapView.addAnnotation(annotation)
                } else {
                    self!.mapView.showsUserLocation = true
                }
            }
        }).disposed(by: disposeBag)
    }
    
    public override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension SelectPositionOnMapViewController: MKMapViewDelegate {
    
    // Delegate method called when addAnnotation is done.
    public func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        return MapHelper.annotationView(mapView: mapView, annotation: annotation)
    }
    
    public func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        return MapHelper.overlayRenderer(overlay: overlay)
    }
}
