//
//  CreateMarkViewController.swift
//  SARA Croisiere
//
//  Created by Marine on 18.04.20.
//  Copyright © 2020 GraniteApps. All rights reserved.
//

import UIKit
import MapKit
import RxSwift

public class CreateMarkViewController: UIViewController, CoordinatesViewDelegate, UITextFieldDelegate, FirebaseManagerDelegate {
    
    // MARK: - IBOutlet
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var validateButton: UIBarButtonItem!
    @IBOutlet weak var titleNavigation: UINavigationItem!
    @IBOutlet weak var constraintNameLabel: NSLayoutConstraint!
    @IBOutlet weak var navigationBar: UINavigationBar!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var typeSegmentedControl: UISegmentedControl!
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var containerScrollView: UIView!
    
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: CreateMarkViewController.classForCoder())
    var currentMark: Mark?
    public var indexInRoute: Int?
    public var viewModel: CreateCourseViewModel?
    var isNewRecordFromNavigation: Bool = false
    var buoyTagView: Int?
    var activateButton: UIButton?
    var visualizeButton: UIButton?
    let disposeBag = DisposeBag()
    
    var temporaryMark: Mark {
        get {
            var buoysList: [Buoy] = []
            self.stackView.subviews.forEach({
                if let buoyView = $0 as? BuoyView {
                    let buoy = buoyView.createBuoyFromView()
                    buoysList.append(buoy)
                }
            })
            let markType: Mark.MarkType = getTypeFromIndex(index: self.typeSegmentedControl.selectedSegmentIndex)
            let mark = Mark(name: nameTextField.text ?? "", markType: markType, buoys: buoysList)
            mark.id = currentMark?.id ?? UUID().uuidString
            return mark
        }
    }
    
    // MARK: -
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        // To update view when the route changed
        FirebaseManager.shared.delegate = self
        
        self.nameLabel.text = NSLocalizedString("name", bundle: bundle, comment: "")
        self.nameLabel.isAccessibilityElement = false
        
        self.nameTextField.text = self.currentMark?.name
        self.nameTextField.accessibilityLabel = NSLocalizedString("name", bundle: bundle, comment: "") + (self.currentMark?.name ?? "")
        self.nameTextField.delegate = self
        
        // Bind the done button to the title of the point
        if self.parent as? AddPointSegmentedViewController != nil{
            self.navigationBar.isHidden = true
        } else {
            self.navigationBar.isHidden = false
            self.constraintNameLabel.isActive = false
            self.cancelButton.title = NSLocalizedString("cancel", bundle: bundle, comment: "")
            self.validateButton.title = NSLocalizedString("ok", bundle: bundle, comment: "")
            self.titleNavigation.title = self.currentMark != nil ? NSLocalizedString("edit_mark", bundle: bundle, comment: "") : NSLocalizedString("create_mark", bundle: bundle, comment: "")
        }
        
        // Type
        self.typeLabel.text = NSLocalizedString("type", bundle: bundle, comment: "")
        if (viewModel as? CreateRaceCourseViewModel) != nil { // SARA Regate: we need all the type
            self.typeSegmentedControl.removeAllSegments()
            Mark.MarkType.allCases.forEach({
                self.typeSegmentedControl.insertSegment(withTitle: $0.name, at: self.typeSegmentedControl.numberOfSegments, animated: false)
            })
            self.typeSegmentedControl.selectedSegmentIndex = 0
        } else {
            self.typeSegmentedControl.setTitle(NSLocalizedString("simple", bundle: bundle, comment: ""), forSegmentAt: 0)
            self.typeSegmentedControl.setTitle(NSLocalizedString("double", bundle: bundle, comment: ""), forSegmentAt: 1)
        }
        self.typeSegmentedControl.customize()
        
        // Reverse Mode
        if let mark = self.currentMark {
            self.typeSegmentedControl.selectedSegmentIndex = mark.indexType()
            if let viewModel = self.viewModel, let vm = viewModel as? CreateCruiseCourseViewModel, vm.reversedRoute {
                self.typeSegmentedControl.isEnabled = false
                let sortedViews = self.typeSegmentedControl.subviews.sorted( by: { $0.frame.origin.x < $1.frame.origin.x } )
                if self.typeSegmentedControl.selectedSegmentIndex == 0 {
                    sortedViews[1].accessibilityLabel = "\(NSLocalizedString("simple", bundle: bundle, comment: "")) \(NSLocalizedString("reversed_mode", bundle: bundle, comment: ""))"
                } else {
                    sortedViews[1].accessibilityLabel = "\(NSLocalizedString("double", bundle: bundle, comment: "")) \(NSLocalizedString("reversed_mode", bundle: bundle, comment: ""))"
                }
            }
        }
        
        setupViewForBuoys()
        
        setupButtons()
        
        if self.viewModel?.status == .subscribed || ((self.viewModel as? CreateRaceCourseViewModel) != nil && (self.viewModel as? CreateRaceCourseViewModel)!.course.id == NavigationManager.shared.route.value?.id) { // Disable editing for subscribed route or the route is the one activated
            self.nameTextField.isEnabled = false
            self.typeSegmentedControl.isEnabled = false
        }
        
        // Close keyboard when tap on the view
        let tapGesture = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(handleNotification(note:)), name: NSNotification.Name(rawValue:"modalSelectOnMapIsDimissed"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleNotification(note:)), name: NSNotification.Name(rawValue:"modalChooseObjectIsDimissed"), object: nil)
        //  Scroll keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        self.nameTextField
            .rx.text
            .map { $0?.isNotEmpty ?? false }
            .bind(to: self.parent as? AddPointSegmentedViewController != nil ? (self.parent as? AddPointSegmentedViewController)!.validateButton.rx.isEnabled : self.validateButton.rx.isEnabled )
            .disposed(by: disposeBag)
        setupObservable()
    }
    
    // MARK: -
    @objc func handleNotification(note: Notification) {
        switch note.name {
        case NSNotification.Name(rawValue:"modalSelectOnMapIsDimissed"):
            let annotation = note.userInfo!["annotation"] as! MKAnnotation
            let gpsPosition = GPSPosition(latitude: Float(annotation.coordinate.latitude), longitude: Float(annotation.coordinate.longitude))
            if let index = buoyTagView, let buoyView = self.stackView.arrangedSubviews[index] as? BuoyView {
                buoyView.coordinatesView.fillCoordinates(with: gpsPosition, auto: true)
            }
        case NSNotification.Name(rawValue:"modalChooseObjectIsDimissed"):
            let object = note.userInfo!["object"] as! NSObject
            if let beacon = object as? Beacon {
                if let gpsPosition = beacon.gpsPosition, let index = buoyTagView, let buoyView = self.stackView.arrangedSubviews[index] as? BuoyView {
                    buoyView.coordinatesView.fillCoordinates(with: gpsPosition, auto: true)
                }
                let name = beacon.name
                if let text = self.nameTextField.text, text.isNotEmpty { // If there is already a name, warning to ask if replace it
                    let alertController = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: String(format: NSLocalizedString("warning_replace_name", bundle: bundle, comment: ""), "\(name)"), preferredStyle: .alert)
                    let yesAction = UIAlertAction(title: NSLocalizedString("yes", bundle: bundle, comment: ""), style: .default) { _ in
                        self.nameTextField.text = "\(name)"
                        self.nameTextField.sendActions(for: .valueChanged) // Observable only fire if we tell that the value changed
                        UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: self.nameTextField)
                    }
                    let noAction = UIAlertAction(title: NSLocalizedString("no", bundle: bundle, comment: ""), style: .cancel, handler: nil)
                    alertController.addAction(noAction)
                    alertController.addAction(yesAction)
                    DispatchQueue.main.async {
                        self.present(alertController, animated: true, completion: nil)
                    }
                } else {
                    self.nameTextField.text = "\(name)"
                    self.nameTextField.sendActions(for: .valueChanged)
                    UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: self.nameTextField)
                }
            }
        default:
            return
        }
    }
    
    @objc func selectCoordinatesAction(sender: UIButton!) {
        if let buoyView = sender.nextFirstResponder { $0 is BuoyView } as? BuoyView {
            buoyTagView = buoyView.tag
        }
        let storyboard = UIStoryboard(name: "Utils", bundle: bundle)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "SelectTableViewController") as? SelectTableViewController else {
                return
        }
        if self.viewModel is CreateRaceCourseViewModel {
            // Select existings beacons
//            controller.titleNavigationBar = NSLocalizedString("beacons", bundle: bundle, comment: "")
//            controller.titleTableHeader = NSLocalizedString("beacons_list", bundle: bundle, comment: "")
//            controller.objects = Beacon.getSortedBeaconsByProximity()
        } else if self.viewModel is CreateCruiseCourseViewModel || self.viewModel == nil {
            // Select existings beacons
            controller.titleNavigationBar = NSLocalizedString("beacons", bundle: bundle, comment: "")
            controller.titleTableHeader = NSLocalizedString("beacons_list", bundle: bundle, comment: "")
            controller.objects = Beacon.getSortedBeaconsByProximity()
        }
        self.present(controller, animated: true, completion: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        else {
            // if keyboard size is not available for some reason, dont do anything
            return
        }
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height , right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        // reset back the content inset to zero after keyboard is gone
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    // MARK: - UI Methods
    func setupObservable() {
        var observableArray: [Observable<Bool>] = []
        self.stackView.arrangedSubviews.forEach({
            if let buoyView = $0 as? BuoyView {
                observableArray.append(buoyView.coordinatesView.observableCoordinates())
            }
        })
        if self.visualizeButton != nil && self.activateButton != nil {
            Observable.combineLatest(observableArray) { (arrayBool) in
                return !arrayBool.contains(false)
            }.bind(to: self.visualizeButton!.rx.isEnabled)
            .disposed(by: disposeBag)
            let nameObservable = nameTextField
                .rx.text
                .map { $0?.isNotEmpty ?? false }.asObservable()
            observableArray.append(nameObservable)
            Observable.combineLatest(observableArray) { (arrayBool) in
                return !arrayBool.contains(false)
            }.bind(to: self.activateButton!.rx.isEnabled)
            .disposed(by: disposeBag)
        }
    }
    
    func buttonBeacons() -> UIButton {
        // Add new button for existings beacons
        let beaconsListButton = UIButton()
        if self.viewModel is CreateRaceCourseViewModel {
            beaconsListButton.setImage(UIImage(named: "connected_buoy", in: bundle, compatibleWith: nil), for: .normal)
            beaconsListButton.accessibilityLabel = Utils.localizedString(forKey: "use_connected_buoy", app: "SARARegate")
            beaconsListButton.isEnabled = false
        } else if self.viewModel is CreateCruiseCourseViewModel || self.viewModel == nil{
            beaconsListButton.setImage(UIImage(named: "beacon", in: bundle, compatibleWith: nil), for: .normal)
            beaconsListButton.accessibilityLabel = NSLocalizedString("use_existing_beacons", bundle: bundle, comment: "")
        }
       
        beaconsListButton.addTarget(self, action: #selector(selectCoordinatesAction), for: .touchUpInside)
        beaconsListButton.tintColor = Utils.tintIcon
        beaconsListButton.tag = 1
        return beaconsListButton
    }
    
    func setupViewForBuoys(forChangeMarkType: Bool = false) {
        guard let buoy1View = Bundle(for: BuoyView.self).loadNibNamed(String(describing: BuoyView.self), owner: nil, options: nil)?.first as? BuoyView else {
            fatalError()
        }
        
        if self.typeSegmentedControl.selectedSegmentIndex == 0 {
            buoy1View.buoyTypeLabel.text = NSLocalizedString("simple_mark", bundle: bundle, comment: "")
            if let _ = self.currentMark, let viewModel = self.viewModel, let vm = viewModel as? CreateCruiseCourseViewModel, vm.reversedRoute { // Not possible to edit let at value when the course is in reversed mode.
                buoy1View.toLetAtControl.isEnabled = false
                buoy1View.reversed = true
            }
            if currentMark != nil {
                buoy1View.setBuoy(buoy: self.currentMark!.buoys[0])
            }
            buoy1View.tag = 0
            buoy1View.coordinatesView.tag = 0
            buoy1View.coordinatesView.setButtonOtherMethod(button: buttonBeacons())
            buoy1View.coordinatesView.delegate = self
            if self.viewModel?.status == .subscribed || ((self.viewModel as? CreateRaceCourseViewModel) != nil && (self.viewModel as? CreateRaceCourseViewModel)!.course.id == NavigationManager.shared.route.value?.id) { // Disable editing for subscribed route or the route is the one activated
                buoy1View.elements(isEnabled: false)
            }
            self.stackView.addArrangedSubview(buoy1View)
        } else {
            guard let buoy2View = Bundle(for: BuoyView.self).loadNibNamed(String(describing: BuoyView.self), owner: nil, options: nil)?.first as? BuoyView else {
                fatalError()
            }
            if self.typeSegmentedControl.selectedSegmentIndex == 1 { // Double
                if let _ = self.currentMark, let viewModel = self.viewModel, let vm = viewModel as? CreateCruiseCourseViewModel, vm.reversedRoute { // Not possible to edit let at value when the course is in reversed mode.
                    buoy1View.reversed = true
                    buoy2View.reversed = true
                    buoy1View.buoyTypeLabel.text = NSLocalizedString("starboard_mark", bundle: bundle, comment: "")
                    buoy2View.buoyTypeLabel.text = NSLocalizedString("port_mark", bundle: bundle, comment: "")
                } else {
                    buoy1View.buoyTypeLabel.text = NSLocalizedString("port_mark", bundle: bundle, comment: "")
                    buoy2View.buoyTypeLabel.text = NSLocalizedString("starboard_mark", bundle: bundle, comment: "")
                }
            } else if self.typeSegmentedControl.selectedSegmentIndex == 2 { // Start
                if let vm = self.viewModel as? CreateRaceCourseViewModel, vm.type.value == .matchRace {
                    buoy1View.buoyTypeLabel.text = Utils.localizedString(forKey: "blue_buoy", app: "SARARegate")
                    buoy2View.buoyTypeLabel.text = Utils.localizedString(forKey: "yellow_buoy", app: "SARARegate")
                } else {
                    buoy1View.buoyTypeLabel.text = NSLocalizedString("start_pin", bundle: bundle, comment: "")
                    buoy2View.buoyTypeLabel.text = NSLocalizedString("start_committee", bundle: bundle, comment: "")
                }
            } else { // Finish
                if let vm = self.viewModel as? CreateRaceCourseViewModel, vm.type.value == .matchRace {
                    buoy1View.buoyTypeLabel.text = Utils.localizedString(forKey: "yellow_buoy", app: "SARARegate")
                    buoy2View.buoyTypeLabel.text = Utils.localizedString(forKey: "blue_buoy", app: "SARARegate")
                } else {
                    buoy1View.buoyTypeLabel.text = NSLocalizedString("finish_committee", bundle: bundle, comment: "")
                    buoy2View.buoyTypeLabel.text = NSLocalizedString("finish_pin", bundle: bundle, comment: "")
                }
            }
            buoy1View.toLetAtControl.selectedSegmentIndex = 0
            buoy1View.toLetAtControl.isEnabled = false
            buoy1View.tag = 0
            buoy1View.coordinatesView.tag = 0
            buoy1View.coordinatesView.setButtonOtherMethod(button: buttonBeacons())
            buoy1View.coordinatesView.delegate = self
            buoy2View.toLetAtControl.selectedSegmentIndex = 1
            buoy2View.toLetAtControl.isEnabled = false
            buoy2View.tag = 1
            buoy2View.coordinatesView.tag = 1
            buoy2View.coordinatesView.setButtonOtherMethod(button: buttonBeacons())
            buoy2View.coordinatesView.delegate = self
            if currentMark != nil {
                if forChangeMarkType, self.currentMark!.buoys[0].toLetAt() == Buoy.ToLetAt.starboard.rawValue {
                    buoy2View.setBuoy(buoy: self.currentMark!.buoys[0])
                } else {
                    buoy1View.setBuoy(buoy: self.currentMark!.buoys[0])
                    if self.currentMark!.buoys.count > 1 {
                        buoy2View.setBuoy(buoy: self.currentMark!.buoys[1])
                    }
                }
            }
            if self.viewModel?.status == .subscribed || ((self.viewModel as? CreateRaceCourseViewModel) != nil && (self.viewModel as? CreateRaceCourseViewModel)!.course.id == NavigationManager.shared.route.value?.id) { // Disable editing for subscribed route or the route is the one activated
                buoy1View.elements(isEnabled: false)
                buoy2View.elements(isEnabled: false)
            }
            self.stackView.addArrangedSubview(buoy1View)
            self.stackView.addArrangedSubview(buoy2View)
            return
        }
    }
    
    func setupButtons() {
        if self.viewModel is CreateCruiseCourseViewModel || self.viewModel == nil {
            // Visualize
            guard let visualiseButton = Bundle(for: CreateMarkViewController.self).loadNibNamed(String(describing: ButtonView.self), owner: nil, options: nil)?.first as? ButtonView else {
                fatalError()
            }
            visualiseButton.button.setTitle(NSLocalizedString("visualize", bundle: bundle, comment: ""), for: .normal)
            visualiseButton.button.customizeSecondary()
            if self.currentMark?.buoys[0].gpsPosition == nil { // if no coordinate disable button
                visualiseButton.button.isEnabled = false
            }
            visualiseButton.button.addTarget(self, action: #selector(visualize(_:)), for: .touchUpInside)
            self.visualizeButton = visualiseButton.button
            stackView.addArrangedSubview(visualiseButton)
            
            // Activate
            guard let activateButton = Bundle(for: CreateMarkViewController.self).loadNibNamed(String(describing: ButtonView.self), owner: nil, options: nil)?.first as? ButtonView else {
                fatalError()
            }
            activateButton.button.setTitle(NSLocalizedString("activate", bundle: bundle, comment: ""), for: .normal)
            activateButton.button.customizeSecondary()
            if self.currentMark?.buoys[0].gpsPosition == nil { // if no coordinate disable button
                activateButton.button.isEnabled = false
            }
            self.activateButton = activateButton.button
            activateButton.button.addTarget(self, action: #selector(activate(_:)), for: .touchUpInside)
            stackView.addArrangedSubview(activateButton)
                        
            // Delete
            guard let deleteButton = Bundle(for: CreateMarkViewController.self).loadNibNamed(String(describing: ButtonView.self), owner: nil, options: nil)?.first as? ButtonView else {
                fatalError()
            }
            deleteButton.button.setTitle(NSLocalizedString("delete", bundle: bundle, comment: ""), for: .normal)
            deleteButton.button.customizeSecondary()
            if self.currentMark != nil {
                deleteButton.button.setTitleColor(#colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1), for: .normal)
            }
            deleteButton.button.isEnabled = self.currentMark != nil
            deleteButton.button.addTarget(self, action: #selector(deleteMark(_:)), for: .touchUpInside)
            stackView.addArrangedSubview(deleteButton)
        }
    }
    
    public func setMark(_ mark: Mark) {
        self.currentMark = mark
    }
    
    public func validate() -> Mark? {
        // Check if all coordinates and name are filled, otherwise, do not save it
        guard let name = nameTextField.text, !name.isEmpty else {
                let alert = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: NSLocalizedString("warning_no_point_name", bundle: bundle, comment: ""), preferredStyle: .alert)
                let alertAction = UIAlertAction(title: NSLocalizedString("ok", bundle: bundle, comment: ""), style: .default, handler: nil)
                alert.addAction(alertAction)
                present(alert, animated: true, completion: nil)
                return nil
        }
        var buoysList: [Buoy] = []
        self.stackView.subviews.forEach({
            if let buoyView = $0 as? BuoyView {
                let buoy = buoyView.createBuoyFromView()
                if self.typeSegmentedControl.selectedSegmentIndex == 0 && self.viewModel is CreateRaceCourseViewModel {
                    buoy.name = name // Add name of the mark for the buoy: for ex: windward instead of simple mark
                }
                buoysList.append(buoy)
            }
        })
        
        let markType: Mark.MarkType = getTypeFromIndex(index: self.typeSegmentedControl.selectedSegmentIndex)
        
        if let vM = self.viewModel as? CreateRaceCourseViewModel {
            let mark = Mark(id: self.currentMark?.id, name: name, markType: markType, buoys: buoysList)
            vM.updateMarkWith(mark: Mark(id: self.currentMark?.id, name: name, markType: markType, buoys: buoysList))
            return mark
        } else {
            // If the current point was already in the base
            if let safeMark = self.currentMark, safeMark.name.isNotEmpty, !isNewRecordFromNavigation {
                let mark = Mark(id: safeMark.id, name: name, markType: markType, buoys: buoysList)
                safeMark.update(with: mark, shared: false)
                if self.viewModel == nil, let course = NavigationManager.shared.route.value, let anyPoint = mark.anyPoint(), course.points.contains(anyPoint) { // We need to update the course if the mark is in the current course
                    NavigationManager.shared.setCurrentRoute(route: course)
                    if let currentPointIndex = NavigationManager.shared.currentPointIndex.value {
                        NavigationManager.shared.setPoint(with: currentPointIndex)
                    }
                }
                return safeMark
            } else {
                // If not, we must create a new point to persist it
                let mark = Mark(name: name, markType: markType, buoys: buoysList)
                mark.save()
                self.viewModel?.addToRoute(mark)
                return mark
            }
        }
    }
    
    func getTypeFromIndex(index: Int) -> Mark.MarkType {
        switch index {
        case 0:
            return Mark.MarkType.simple
        case 1:
            return Mark.MarkType.double
        case 2:
            return Mark.MarkType.start
        case 3:
            return Mark.MarkType.end
        default:
            return Mark.MarkType.simple
        }
    }
    
    // MARK: - IBAction
    @IBAction func changeMarkType(_ sender: UISegmentedControl) {
        self.stackView.subviews.forEach({ $0.removeFromSuperview() })
        setupViewForBuoys(forChangeMarkType: true)
        setupButtons()
        setupObservable()
    }
    
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: {
              NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalCreateMarkIsDimissed"), object: nil)
        })
    }
    
    @IBAction func validate(_ sender: Any) {
        let _ = self.validate()
        self.dismiss(animated: true, completion: {
              NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalCreateMarkIsDimissed"), object: nil)
        })
    }
    
    @objc func visualize(_ sender: UIButton) {
        let mark = temporaryMark

        // Embedded view controllers
        let storyboard = UIStoryboard(name: "Route", bundle: Bundle(for: CreateMarkViewController.classForCoder()))

        // Courses
        if let visualiseViewController = storyboard.instantiateViewController(withIdentifier: "PointVisualizationViewController") as? PointVisualizationViewController{
            visualiseViewController.point = mark
            visualiseViewController.viewModel = viewModel
            self.present(visualiseViewController, animated: false, completion: nil)
        }
    }
    
    @objc func activate(_ sender: UIButton) {
        var index: Int = 0
        if temporaryMark.markType == Mark.MarkType.simple.rawValue {
            index = 2
        } else {
            index = 3
        }
        guard let activateView = self.stackView.subviews[index] as? ButtonView  else {
            return
        }
        
        if let mark = self.currentMark, mark == NavigationManager.shared.point.value {
            if NavigationManager.shared.route.value != nil {
                NavigationManager.shared.setNextPoint()
            } else {
                NavigationManager.shared.setCurrentRoute(route: nil)
            }
            activateView.button.setTitle(NSLocalizedString("activate", bundle: bundle, comment: ""), for: .normal)
            return
        }
        // Save mark
        if let mark = self.validate(), let anyPoint = mark.anyPoint()  {
            activateView.button.setTitle(NSLocalizedString("deactivate", bundle: bundle, comment: ""), for: .normal)
            activateView.button.accessibilityLabel = NSLocalizedString("deactivate", bundle: bundle, comment: "")
            //Create new route with selected point
            let route = Course(id: "", name: "\(NSLocalizedString("to_a_mark", bundle: bundle, comment: "")): \(mark.name)", points: [anyPoint])
            NavigationManager.shared.updateNavView = true
            NavigationManager.shared.setCurrentRoute(route: route, isSaved: false)
            
            self.dismiss(animated: true, completion: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalEnablePoint"), object: nil)
            })
        }
    }
        
    @objc func deleteMark(_ sender: UIButton) {
        guard let mark = currentMark else {
            return
        }
        // Desactivate route when the point is used in the current route
        if let route = NavigationManager.shared.route.value, let anyPoint = mark.anyPoint(), route.points.contains(anyPoint) {
            let message = ((route as? CruiseCourse) != nil) ? Utils.localizedString(forKey: "warning_delete_used_mark", app: "SARANav") : Utils.localizedString(forKey: "warning_delete_used_mark", app: "SARARegate")
            let alertController = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: message, preferredStyle: .alert)
            let yesAction = UIAlertAction(title: NSLocalizedString("yes", bundle: bundle, comment: ""), style: .default) { _ in
                var currentPointIndex = NavigationManager.shared.currentPointIndex.value
                if currentPointIndex != nil
                    && mark == NavigationManager.shared.point.value
                    && route.points.count > 1 { // Get previous index of a mark with coordinates
                    currentPointIndex = NavigationManager.shared.getPreviousIndexWithCoord()
                }
                NavigationManager.shared.route.accept(nil)
                mark.deletePoint()
                if self.indexInRoute != nil { // Remove point in view Model
                    self.viewModel?.removeFromRoute(at: IndexPath(row: self.indexInRoute!, section: 0))
                }
                //If there is no point in the route: stop
                if currentPointIndex == nil || route.points.isEmpty || !Course.all().contains(route){
                    self.dismiss(animated: true, completion: {
                          NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalCreateMarkIsDimissed"), object: nil)
                    })
                    return
                }
                // Otherwise set the route
                NavigationManager.shared.setCurrentRoute(route: route)
                NavigationManager.shared.currentPointIndex.accept(currentPointIndex)
                self.dismiss(animated: true, completion: {
                      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalCreateMarkIsDimissed"), object: nil)
                })
            }
            let noAction = UIAlertAction(title: NSLocalizedString("no", bundle: bundle, comment: ""), style: .cancel, handler: nil)
            alertController.addAction(noAction)
            alertController.addAction(yesAction)
            present(alertController, animated: true, completion: nil)
        } else {
            // Check if the mark is used in at leat one route. In this case, different message.
            let message = mark.inARoute() ? String(format: AnnouncementManager.shared.announcementDelegate?.deleteMarkUsedRouteMessage() ?? "", mark.name) : NSLocalizedString("warning_delete_mark", bundle: bundle, comment: "")
            let alertController = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: message, preferredStyle: .alert)
            let yesAction = UIAlertAction(title: NSLocalizedString("yes", bundle: bundle, comment: ""), style: .default) { _ in
                mark.deletePoint()
                if self.indexInRoute != nil { // Remove point in view Model
                    self.viewModel?.removeFromRoute(at: IndexPath(row: self.indexInRoute!, section: 0))
                }
                self.dismiss(animated: true, completion: {
                      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalCreateMarkIsDimissed"), object: nil)
                })
            }
            let noAction = UIAlertAction(title: NSLocalizedString("no", bundle: bundle, comment: ""), style: .cancel, handler: nil)
            alertController.addAction(noAction)
            alertController.addAction(yesAction)
            present(alertController, animated: true, completion: nil)
        }
    }
    
    // MARK: - CoordinatesViewDelegate
    public func openSelectOnMap(tagView: Int?) {
        let storyboard = UIStoryboard(name: "Route", bundle: bundle)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "SelectOnMapVC") as? SelectPositionOnMapViewController else {
                return
        }
        buoyTagView = tagView
        self.stackView.arrangedSubviews.forEach({
            guard let buoyView = $0 as? BuoyView else {
                return
            }
            if let (latitude, longitude) = buoyView.coordinatesView.getCoordinates() {
                if buoyView.tag == buoyTagView {
                    let annotation = MapAnnotation(latitude: Double(latitude), longitude: Double(longitude), type: .mark, isCurrent: true)
                    controller.annotation = annotation
                } else {
                    let annotation = MapAnnotation(latitude: Double(latitude), longitude: Double(longitude), type: .mark, isCurrent: false)
                    controller.annotationSecondBuoy = annotation
                }
            } else if buoyView.tag == buoyTagView { // Add map annotation only for the current buoy.
                let annotation = MapAnnotation(latitude: Double.nan, longitude: Double.nan, type: .mark, isCurrent: true)
                controller.annotation = annotation
            }
        })
        self.present(controller, animated: true, completion: nil)
    }
    
    public func useCurrentPosition(tagView: Int?, gpsPosition: GPSPosition) {
        buoyTagView = tagView
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when) {
            if let name = self.nameTextField.text, name.isNotEmpty {
                UIAccessibility.post(notification: UIAccessibility.Notification.announcement, argument: NSLocalizedString("coordinates_used", bundle: self.bundle, comment: "") + name)
            } else {
                UIAccessibility.post(notification: UIAccessibility.Notification.announcement, argument: NSLocalizedString("coordinates_used_for_this_point", bundle: self.bundle, comment: ""))
            }
        }
    }
        
    // MARK: - TextField Delegate
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: textField)
        return false
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let specialCharacters = "!@#$%&*()+{}[]|\"<>,.~/:;?-=\\¥£•¢"
        let char = CharacterSet(charactersIn: specialCharacters)
        if let range = string.rangeOfCharacter(from: char) {
            let alert = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: NSLocalizedString("firebase_name_special_char", bundle: bundle, comment: ""), preferredStyle: .alert)
            let okAction = UIAlertAction(title: NSLocalizedString("ok", bundle: bundle, comment: ""), style: .cancel, handler: { _ in
                textField.text = (textField.text ?? "") + string.dropLast()
            })
            alert.addAction(okAction)
            self.present(alert, animated: false)
            return false
        }
        return true
    }
    
    // MARK: - FirebaseManagerDelegate
    public func courseChanged(course: Course?) { // Subscribed route has been modified
        // Check if the mark still exists
        guard course != nil else { return }
        if let mark = course?.points.first(where: {$0.primaryKey == temporaryMark.id})?.point as? Mark {
            setMark(mark)
            self.nameTextField.text = mark.name
            self.stackView.subviews.forEach({ $0.removeFromSuperview() })
            setupViewForBuoys(forChangeMarkType: true)
            setupButtons()
            setupObservable()
        } else {
            self.dismiss(animated: false)
        }
    }
    
    public func courseRemoved(courseId: String) {}
    
    public func courseAdded(course: Course?) {}
}
