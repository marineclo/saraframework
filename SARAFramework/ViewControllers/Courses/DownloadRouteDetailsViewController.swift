//
//  DownloadRouteDetailsViewController.swift
//  SARAFramework
//
//  Created by Marine on 15.09.23.
//

import UIKit

class DownloadRouteDetailsViewController: UIViewController {

    // MARK: - IBOutlets
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var titleNavigationItem: UINavigationItem!
    @IBOutlet weak var leftBarButtonItem: UIBarButtonItem!
    
    @IBOutlet weak var uploadDateLabel: UILabel!
    @IBOutlet weak var uploadDateValue: UILabel!
    @IBOutlet weak var pointsNbLabel: UILabel!
    @IBOutlet weak var pointsNbValue: UILabel!
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: DownloadRouteDetailsViewController.classForCoder())
    var course: Course?
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.leftBarButtonItem.title = NSLocalizedString("back", bundle: bundle, comment: "")
        self.titleNavigationItem.title = course?.name
        
        self.uploadDateLabel.text = NSLocalizedString("upload_date", bundle: bundle, comment: "")
        self.uploadDateValue.text = course?.date
        self.pointsNbLabel.text = NSLocalizedString("nb_points", bundle: bundle, comment: "")
        self.pointsNbValue.text = "\(self.course?.points.count ?? 0)"
        
        self.downloadButton.customizeSecondary()
        self.downloadButton.setTitle(NSLocalizedString("download", bundle: bundle, comment: ""), for: UIControl.State.normal)
        self.downloadButton.accessibilityLabel = NSLocalizedString("download", bundle: bundle, comment: "")
        
        self.deleteButton.isHidden = self.course?.status != .shared
        self.deleteButton.customizeSecondary()
        self.deleteButton.setTitleColor(#colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1), for: .normal)
        self.deleteButton.setTitle(NSLocalizedString("delete", bundle: bundle, comment: ""), for: UIControl.State.normal)
        self.deleteButton.accessibilityLabel = NSLocalizedString("delete", bundle: bundle, comment: "")
        
    }
    
    // MARK: - Functions
    // Download course and save it locally
    func downloadRouteAndSave() {
        guard let _course = self.course else { return }
        FirebaseManager.shared.retrieveCourse(courseId: _course.name, completion: { course in
            if course != nil {
                let alert = Utils.showAlert(with: "", message: Utils.localizedString(forKey: "firebase_success_download_save_route", app: "SARANav"))
                self.present(alert, animated: false)
            } else {
                let alert = Utils.showAlert(with: NSLocalizedString("error", bundle: self.bundle, comment: ""), message: Utils.localizedString(forKey: "firebase_error_download_save_route", app: "SARANav"))
                self.present(alert, animated: false)
            }
        })
    }
    
    // MARK: - IBActions
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: false)
    }
    
    @IBAction func downloadAction(_ sender: Any) {
        // Check if there is already a course with the same name locally
        guard let _course = self.course else { return }
        if FirebaseManager.shared.reachability.connection == .unavailable {
            let alert = Utils.showAlert(with: NSLocalizedString("warning", bundle: bundle, comment: ""), message: NSLocalizedString("no_internet_connection_no_sharing", bundle: bundle, comment: ""))
            present(alert, animated: false)
        } else {
            if let number = _course.getRealmCourseWithName(course: _course), number > 0 {
                let alert = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: Utils.localizedString(forKey: "firebase_warning_erase_route_local", app: "SARANav"), preferredStyle: .alert)
                let yesAction = UIAlertAction(title: NSLocalizedString("yes", bundle: bundle, comment: ""), style: .default, handler: { _ in
                    // Download course and save it locally
                    self.downloadRouteAndSave()
                })
                let noAction = UIAlertAction(title: NSLocalizedString("no", bundle: bundle, comment: ""), style: .cancel, handler: { _ in
                    //
                })
                alert.addAction(yesAction)
                alert.addAction(noAction)
                self.present(alert, animated: false)
            } else {
                // Download course and save it locally
                self.downloadRouteAndSave()
            }
        }
    }
    
    @IBAction func deleteAction(_ sender: Any) {
        guard let _course = self.course else { return }
        if FirebaseManager.shared.reachability.connection == .unavailable {
            let alert = Utils.showAlert(with: NSLocalizedString("warning", bundle: bundle, comment: ""), message: NSLocalizedString("no_internet_connection_no_sharing", bundle: bundle, comment: ""))
            present(alert, animated: false)
        } else {
            FirebaseManager.shared.unshareCourse(course: _course, successHandler: {
                let alert = UIAlertController(title: "", message: Utils.localizedString(forKey: "firebase_success_delete_route", app: "SARANav"), preferredStyle: .alert)
                let okAction = UIAlertAction(title: NSLocalizedString("ok", bundle: self.bundle, comment: ""), style: .cancel, handler: {_ in
                    self.dismiss(animated: false)
                })
                alert.addAction(okAction)
                self.present(alert, animated: false)
            }, errorHandler: { error in
                let alert = Utils.showAlert(with: "", message: "\(error.localizedDescription)")
                self.present(alert, animated: false)
            })
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
