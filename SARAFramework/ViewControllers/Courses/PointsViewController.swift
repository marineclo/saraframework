//
//  PointsViewController.swift
//  SARA Croisiere
//
//  Created by Rose on 12/11/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

public protocol SelectionPointDelegate {
    func selectionPoint(point: PointRealm)
}

public class PointsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SelectButtonCellDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var constraintTable: NSLayoutConstraint!
    @IBOutlet private var tableView: UITableView?
    @IBOutlet private weak var createPointButton: UIButton?
    @IBOutlet weak var editPointsButton: UIButton!
    @IBOutlet weak var desactivatePointButton: UIButton!
    
    //MARK: - Variables
    let bundle = Utils.bundle(anyClass: PointsViewController.classForCoder())
    
    // MARK: - IBActions
    @IBAction func editPoints(_ sender: Any) {
        if selectionMode {
            self.performSegue(withIdentifier: "createCoursePointSegue", sender: self)
        } else {
            let storyboard = UIStoryboard(name: "Route", bundle: bundle)
            guard let controller = storyboard.instantiateViewController(withIdentifier: "CreateMarkViewController") as? CreateMarkViewController else {
                    return
            }
            self.present(controller, animated: true, completion: nil)
        }
    }
        
    @IBAction func tapButton(_ sender: Any) {
        if selectionMode {
            if let currentPosition = LocationManager.shared.getCurrentPosition() {
                let alertController = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: NSLocalizedString("activate_mob_mode", bundle: bundle, comment: ""), preferredStyle: .alert)
                let action1 = UIAlertAction(title: NSLocalizedString("cancel", bundle: bundle, comment: ""), style: .cancel)
                let action2 = UIAlertAction(title: NSLocalizedString("yes", bundle: bundle, comment: ""), style: .default) { (action:UIAlertAction) in
                    
                    // Query Realm for all MOB: distanceThreshold 0
                    let gpsPos = GPSPosition(latitude: Float(currentPosition.coordinate.latitude), longitude: Float(currentPosition.coordinate.longitude))
                    let point = CoursePoint(name: NSLocalizedString("MOB", bundle: self.bundle, comment: ""), distanceThreshold: 0, gpsPosition: gpsPos)
                    point.saveMOB()
                    self.delegate?.selectionPoint(point: point)
                    self.dismiss(animated: true, completion: nil)
                }
                alertController.addAction(action1)
                alertController.addAction(action2)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func desactivatePointAction(_ sender: Any) {
        if selectionMode {
            if NavigationManager.shared.route.value != nil {
                NavigationManager.shared.setNextPoint()
                self.desactivatePointButton.isEnabled = false
            } else {
                NavigationManager.shared.route.accept(nil)
                self.desactivatePointButton.isEnabled = false
            }
        }
    }
    
    // MARK: - Segue
    public override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if !selectionMode && identifier == "createCoursePointSegue" {
            return true
        }
        return false
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if selectionMode && segue.identifier == "createCoursePointSegue" {
            guard let controller = segue.destination as? CreatePointViewController else {
                return
            }
            if let currentPosition = LocationManager.shared.getCurrentPosition() {
                // Query Realm
                let realm = try! Realm()
                var nameNb: Int?
                if let lastPointName = realm.objects(CoursePoint.self).filter({ $0.name.contains(NSLocalizedString("waypoint_abb", bundle: self.bundle, comment: ""))}).map({$0.name}).sorted().last {
                    nameNb = Int(lastPointName[5..<lastPointName.count]) ?? 0
                }
                let gpsPos = GPSPosition(latitude: Float(currentPosition.coordinate.latitude), longitude: Float(currentPosition.coordinate.longitude))
                let point = CoursePoint(name: "\(NSLocalizedString("waypoint_abb", bundle: bundle, comment: ""))\((nameNb ?? 0) + 1)", gpsPosition: gpsPos)
                controller.currentPoint = point
                controller.isNewRecordFromNavigation = true
            }
        }
    }
    
    // MARK: - Variables
    public var selectionMode = false {
        didSet {
            if isViewLoaded {
                if selectionMode {
                    self.createPointButton?.layer.borderColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
                    self.createPointButton?.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
                    self.createPointButton?.setTitle(NSLocalizedString("MOB_new", bundle: bundle, comment: ""), for: .normal)
                    self.createPointButton?.accessibilityLabel = NSLocalizedString("MOB_new", bundle: bundle, comment: "")
                    self.desactivatePointButton?.layer.borderColor = Utils.primaryColor.cgColor
                    self.desactivatePointButton?.backgroundColor = Utils.primaryColor
                    self.desactivatePointButton.isHidden = false
                    self.editPointsButton?.isHidden = false
                    self.desactivatePointButton?.setTitle(NSLocalizedString("desactivate_point", bundle: bundle, comment: ""), for: .normal)
                    self.desactivatePointButton?.accessibilityLabel = NSLocalizedString("desactivate_point", bundle: bundle, comment: "")
                    if NavigationManager.shared.route.value?.id == "" {
                        self.desactivatePointButton?.isEnabled = false
                    } else {
                        self.desactivatePointButton?.isEnabled = true
                    }
                } else {
                    self.createPointButton?.customize()
                    self.createPointButton?.setTitle(NSLocalizedString("create_point_title", bundle: bundle, comment: ""), for: .normal)
                    self.createPointButton?.accessibilityLabel = NSLocalizedString("create_point_title", bundle: bundle, comment: "")
                    self.desactivatePointButton.isHidden = true
                    self.editPointsButton?.customize()
                    self.editPointsButton?.setTitle(NSLocalizedString("create_mark", bundle: bundle, comment: ""), for: .normal)
                    self.editPointsButton?.accessibilityLabel = NSLocalizedString("create_mark", bundle: bundle, comment: "")
                }
            }
        }
    }
    
    public var delegate:SelectionPointDelegate?
    var sortedPoints:[PointRealm]?
    
    // MARK: -
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.createPointButton?.customizePrimary()
        self.editPointsButton.customizePrimary()
        self.desactivatePointButton.customizePrimary()
        sortedPoints = CoursePoint.getSortedPointsByProximity()
        if selectionMode {
            self.constraintTable.isActive = false
            self.createPointButton?.layer.borderColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
            self.createPointButton?.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
            self.createPointButton?.setTitle(NSLocalizedString("MOB_new", bundle: bundle, comment: ""), for: .normal)
            self.navigationItem.setLeftBarButton(UIBarButtonItem(title: NSLocalizedString("cancel", bundle: bundle, comment: ""), style: .done, target: self, action: #selector(onClose)), animated: false)
            self.createPointButton?.accessibilityLabel = NSLocalizedString("MOB_new", bundle: bundle, comment: "")
            self.desactivatePointButton?.layer.borderColor = Utils.primaryColor.cgColor
            self.desactivatePointButton?.backgroundColor = Utils.primaryColor
            self.editPointsButton?.setTitle(NSLocalizedString("create_point_title", bundle: bundle, comment: ""), for: .normal)
            self.editPointsButton?.accessibilityLabel = NSLocalizedString("create_point_title", bundle: bundle, comment: "")
            self.editPointsButton?.isHidden = false
            self.desactivatePointButton.isHidden = false
            self.desactivatePointButton?.setTitle(NSLocalizedString("desactivate_point", bundle: bundle, comment: ""), for: .normal)
            self.desactivatePointButton?.accessibilityLabel = NSLocalizedString("desactivate_point", bundle: bundle, comment: "")
            if NavigationManager.shared.route.value?.id == "" {
                self.desactivatePointButton?.isEnabled = true
            } else {
                self.desactivatePointButton?.isEnabled = false
            }
        } else {
            self.createPointButton?.customize()
            self.createPointButton?.setTitle(NSLocalizedString("create_point_title", bundle: bundle, comment: ""), for: .normal)
            self.createPointButton?.accessibilityLabel = NSLocalizedString("create_point_title", bundle: bundle, comment: "")
            self.desactivatePointButton.isHidden = true
            self.editPointsButton?.customize()
            self.editPointsButton?.setTitle(NSLocalizedString("create_mark", bundle: bundle, comment: ""), for: .normal)
            self.editPointsButton?.accessibilityLabel = NSLocalizedString("create_mark", bundle: bundle, comment: "")
        }
        self.tableView?.customize()
        
        self.title = NSLocalizedString("menu_points", bundle: bundle, comment: "")
        self.tableView?.tableFooterView = UIView(frame: .zero)
        let bundleView = Bundle(for: PointsViewController.self)
        self.tableView?.register(UINib(nibName: "SelectButtonCell", bundle: bundleView), forCellReuseIdentifier: "chooseCourseCellIdentifier")
        self.tableView?.register(UINib(nibName: "HeaderTableView", bundle: bundleView), forHeaderFooterViewReuseIdentifier: "HeaderTableView")
        if #available(iOS 15, *) { // To remove gap between header and table view
            self.tableView?.sectionHeaderTopPadding = 0
        }
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(handleModalDismissed), name: NSNotification.Name(rawValue:"modalCreatePointIsDimissed"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleModalDismissed), name: NSNotification.Name(rawValue:"modalCreateMarkIsDimissed"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onClose), name: NSNotification.Name(rawValue:"modalEnablePoint"), object: nil)
        sortedPoints = PointRealm.getSortedPointsByProximity()
        self.tableView?.reloadData()
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func handleModalDismissed() {
      sortedPoints = PointRealm.getSortedPointsByProximity()
      self.tableView?.reloadData()
    }
    
    @objc func onClose(){
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - TableView
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return sortedPoints?.count ?? 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "chooseCourseCellIdentifier") as? SelectButtonCell else {
            return UITableViewCell()
        }
        let point = sortedPoints?[indexPath.row]
        cell.delegate = self
        cell.titleLabel?.font = UIFont.preferredFont(forTextStyle: .body)
        cell.titleLabel?.text = point?.name
        cell.selectButton?.isAccessibilityElement = false
        cell.selectButton?.tintColor = Utils.tintIcon
        if let _ = point as? Mark, let image = UIImage(named: "buoys", in: bundle, compatibleWith: nil) {
            cell.selectButton?.setImage(image, for: .normal)
            cell.accessibilityLabel = "\(point?.name ?? ""). \(NSLocalizedString("mark", bundle: bundle, comment: ""))"
        } else if let p = point as? CoursePoint {
            if p.distanceThreshold == 0, let image = UIImage(named: "mob", in: bundle, compatibleWith: nil) {
                cell.selectButton?.setImage(image, for: .normal)
                cell.selectButton?.tintColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
            } else if let image = UIImage(named: "course-point", in: bundle, compatibleWith: nil) {
                cell.selectButton?.setImage(image, for: .normal)
            }
            cell.accessibilityLabel = "\(point?.name ?? ""). \(NSLocalizedString("waypoint", bundle: bundle, comment: ""))"
        }
        if selectionMode { // We are not in the edit mode
            if point?.getCoordinates(method: .middle) == nil { // No coordinate
                cell.isUserInteractionEnabled = false
                cell.contentView.alpha = 0.5
            } else {
                cell.isUserInteractionEnabled = true
                cell.contentView.alpha = 1
            }
        }
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if !selectionMode { // We want to edit the point
            let storyboard = UIStoryboard(name: "Route", bundle: bundle)
            if let point = sortedPoints?[indexPath.row] as? CoursePoint {
                guard let controller = storyboard.instantiateViewController(withIdentifier: "CreatePointViewController") as? CreatePointViewController else {
                        return
                }
                controller.setPoint(point)
                self.present(controller, animated: true, completion: nil)
            } else if let mark = sortedPoints?[indexPath.row] as? Mark {
                guard let controller = storyboard.instantiateViewController(withIdentifier: "CreateMarkViewController") as? CreateMarkViewController else {
                        return
                }
                controller.setMark(mark)
                self.present(controller, animated: true, completion: nil)
            }
        } else { // Activate point
            let point = sortedPoints![indexPath.row]
            delegate?.selectionPoint(point: point)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = self.tableView?.dequeueReusableHeaderFooterView(withIdentifier: "HeaderTableView" ) as! HeaderTableView
        returnedView.customizeHeader(titleName: NSLocalizedString("points_list", bundle: bundle, comment: ""))
        return returnedView
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    //MARK: - SelectButtonCellDelegate
    public func didSelectCell(id: String) {
        
    }
    
}
