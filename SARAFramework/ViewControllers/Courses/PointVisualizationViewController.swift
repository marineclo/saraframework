//
//  PointVisualizationViewController.swift
//  SARA Croisiere
//
//  Created by Marine IL on 18.07.19.
//  Copyright © 2019 Rose. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import RxSwift

class PointVisualizationViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, MKMapViewDelegate {

    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var mapButton: UIBarButtonItem!
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: PointVisualizationViewController.classForCoder())
    var point: PointRealm?
    
    // Connect all the mappoints using Poly line.
    var points: [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
    
    var viewModel: CreateCourseViewModel?
    
    let disposeBag = DisposeBag()
    
    var displayMode:Bool = false {
        didSet{
            if displayMode {
                tableView.isHidden = true
                self.navigationBar.items?.first?.rightBarButtonItems?.first?.image = UIImage(named: "list", in: bundle, compatibleWith: nil)
                self.navigationBar.items?.first?.rightBarButtonItems?.first?.accessibilityLabel = NSLocalizedString("list", bundle: bundle, comment: "")
                mapView.isHidden = false
            } else {
                tableView.isHidden = false
                mapView.isHidden = true
                self.navigationBar.items?.first?.rightBarButtonItems?.first?.image = UIImage(named: "map", in: bundle, compatibleWith: nil)
                self.navigationBar.items?.first?.rightBarButtonItems?.first?.accessibilityLabel = NSLocalizedString("map", bundle: bundle, comment: "")
            }
        }
    }
    
    @IBAction func switchDisplayMode(_ sender: Any) {
        displayMode = !displayMode
    }
    
    @IBAction func cancelButton(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationBar.items?.first?.title = point?.name
        self.navigationBar.items?.first?.leftBarButtonItems?.first?.title = NSLocalizedString("back", bundle: bundle, comment: "")
        self.navigationBar.items?.first?.leftBarButtonItems?.first?.style = .done
        self.navigationBar.items?.first?.rightBarButtonItems?.first?.tintColor = Utils.primaryColor
        self.navigationBar.items?.first?.rightBarButtonItems?.first?.accessibilityLabel = NSLocalizedString("map", bundle: bundle, comment: "")
        self.navigationBar.items?.first?.rightBarButtonItems?.first?.image = UIImage(named: "map", in: bundle, compatibleWith: nil)
        
        self.tableView.register(UINib(nibName: "InformationCell", bundle: Bundle(for: PointVisualizationViewController.classForCoder())), forCellReuseIdentifier: "InfosCellIdentifier")
        if #available(iOS 15, *) { // To remove gap between header and table view
            tableView.sectionHeaderTopPadding = 0
        }
        
        if !Settings.shared.usePhoneGPS {
            self.mapView.showsUserLocation = false
        } else {
            self.mapView.showsUserLocation = true
        }
        let pointsDisplayed = point != nil ? [point!] : []
        MapHelper.getAllMapAnnotations(map: self.mapView, pointsDisplayed: pointsDisplayed)
        let annotations = getMapAnnotations()
        mapView.addAnnotations(annotations)
        
        // Add validation circle
        if let coursePoint = point as? CoursePoint, let gpsPos = coursePoint.gpsPosition {
            let center = CLLocationCoordinate2DMake(CLLocationDegrees(gpsPos.latitude), CLLocationDegrees(gpsPos.longitude))
            
            let circle = MKCircle(center: center, radius: CLLocationDistance(coursePoint.distanceThreshold))
            circle.title = "distanceThreshold"
            self.mapView.addOverlay(circle)
        }
        
        mapView.delegate = self
        
        //Zoom to fit the route
        var annotationsToZoom: [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
        if LocationManager.shared.locationVariable.value != nil {
            annotationsToZoom.append(LocationManager.shared.locationVariable.value!.coordinate)
        }
        for annotation in annotations {
            annotationsToZoom.append(annotation.coordinate)
        }
        let polylineZoom = MKPolyline(coordinates: annotationsToZoom, count: annotationsToZoom.count)
        MapHelper.zoomToPolyLine(map: self.mapView, polyLine: polylineZoom, animated: false)
        
        // Do any additional setup after loading the view.
        LocationManager.shared.locationVariable
            .asObservable()
            .subscribe(onNext: { [weak self] (location) in
                guard self != nil else { return }
                self!.mapView.removeOverlays(self!.mapView.overlays.filter({$0.title == nil}))
                if location.value != nil {
                    
                    self!.points.append(location.value!.coordinate)

                    let polyline = MKPolyline(coordinates: self!.points, count: self!.points.count)
                    self!.mapView.addOverlay(polyline)
                    
                    // Add user position
                    MapHelper.userPosition(mapView: self!.mapView, newLocation: location!)

                } else {
                    self!.mapView.removeAnnotations(self!.mapView.annotations.filter({$0 is MapAnnotation && ($0 as! MapAnnotation).type == .userLocation}))
                }
            }).disposed(by: disposeBag)
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let mark = self.point as? Mark, mark.buoys.count > 1 {
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "InfosCellIdentifier") as? InformationCell else {
            return UITableViewCell()
        }
        guard let p1 = LocationManager.shared.locationVariable.value else {
            return UITableViewCell()
        }
        var gpsPosition: GPSPosition?
        if let p2 = self.point as? CoursePoint {
            gpsPosition = p2.gpsPosition
        } else if let p2 = self.point as? Mark {
            gpsPosition = p2.buoys[indexPath.section].gpsPosition
        }
        guard gpsPosition != nil else {
            return UITableViewCell()
        }
        // Configure the cell...
        // Infos = (Name of the info, value of the info, unit of the info)
        // Compute the location of the point given
        var infos:(String?, String?, NavigationUnit?)
        if indexPath.row == 0 {
            let distance = Utils.tupleDistance(floatDistance: NavigationHelper.getDistanceFrom(location: p1, to: gpsPosition))!
            if distance.1 == .meter {
                infos = (NSLocalizedString("distance", bundle: bundle, comment: ""), "\(distance.0.roundDown)", distance.1)
            } else {
                infos = (NSLocalizedString("distance", bundle: bundle, comment: ""), "\(distance.0)", distance.1)
            }
        } else if indexPath.row == 1 {
            let orientation = NavigationHelper.segmentOrientation(p1Location: p1, p2Postion: gpsPosition!)
            infos = (NSLocalizedString("azimuth", bundle: bundle, comment: ""), "\(orientation)", NavigationUnit.degrees)
        }
        cell.infos = (infos.0, infos.1, infos.2, nil, nil)

        return cell
    }
    
    // Override to support conditional editing of the table view.
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let p2 = self.point as? CoursePoint {
             return "\(p2.name)"
        } else if let mark = self.point as? Mark {
            let vm = viewModel as? CreateCruiseCourseViewModel
            return "\(mark.name) - \(NSLocalizedString("unit_" + mark.buoys[section].toLetAt(inversed: vm?.reversedRoute), bundle: bundle, comment: ""))"
        }
       return nil
    }
    
    //MARK:- Annotations
    
    func getMapAnnotations() -> [MapAnnotation] {
        var annotations = [MapAnnotation]()
        
        //Create annotations
        guard let p2 = point else {
            return []
        }
        if let coursePoint = p2 as? CoursePoint, let gpsPosition = coursePoint.gpsPosition {
            let type = coursePoint.distanceThreshold == 0 ? MapAnnotation.MapAnnotationType.mob : MapAnnotation.MapAnnotationType.coursePoint
            if let annotation = fillAnnotation(gpsPosition: gpsPosition, type: type) {
                annotation.title = "\(coursePoint.name)"
                annotations.append(annotation)
                points.append(annotation.coordinate)
            }
        } else if let mark = p2 as? Mark {
            var coordMarks: [CLLocationCoordinate2D] = []
            mark.buoys.forEach({
                if let gpsPosition = $0.gpsPosition {
                    if let annotation = fillAnnotation(gpsPosition: gpsPosition, type: .mark) {
                        let vm = viewModel as? CreateCruiseCourseViewModel
                        annotation.title = "\(mark.name) - \(NSLocalizedString("unit_" + $0.toLetAt(inversed: vm?.reversedRoute), bundle: bundle, comment: ""))"
                        annotations.append(annotation)
                        coordMarks.append(annotation.coordinate)
                    }
                }
            })
            let polylineMark = MKPolyline(coordinates: coordMarks, count: coordMarks.count)
            polylineMark.title = "crossingLineMark"
            mapView.addOverlay(polylineMark)
            let gpsPositionMiddle = mark.middlePoint()
            if gpsPositionMiddle != nil {
                let coords2D = CLLocationCoordinate2D(latitude: Double(gpsPositionMiddle!.latitude), longitude: Double(gpsPositionMiddle!.longitude))
                points.append(coords2D)
            }
        }
        return annotations
    }
    
    func fillAnnotation(gpsPosition: GPSPosition, type: MapAnnotation.MapAnnotationType) -> MapAnnotation? {
        guard let p1 = LocationManager.shared.locationVariable.value else {
            return nil
        }
        let lat = Double(gpsPosition.latitude)
        let long = Double(gpsPosition.longitude)
        let annotation = MapAnnotation(latitude: lat, longitude: long, type: type, isCurrent: true)
        let distance = Utils.tupleDistance(floatDistance: NavigationHelper.getDistanceFrom(location: p1, to: gpsPosition))!
        let orientation = NavigationHelper.segmentOrientation(p1Location: p1, p2Postion: gpsPosition)
        if distance.1 == .meter {
            annotation.subtitle = "\(NSLocalizedString("distance", bundle: bundle, comment: "")): \(distance.0.roundDown) \(distance.1!.abbreviation), \(NSLocalizedString("azimuth", bundle: bundle, comment: "")): \(orientation) \(NavigationUnit.degrees.abbreviation)"
        } else {
            annotation.subtitle = "\(NSLocalizedString("distance", bundle: bundle, comment: "")): \(distance.0) \(distance.1!.abbreviation), \(NSLocalizedString("azimuth", bundle: bundle, comment: "")): \(orientation) \(NavigationUnit.degrees.abbreviation)"
        }
        return annotation
    }
    
    //MARK:- MapViewDelegate methods
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        return MapHelper.overlayRenderer(overlay: overlay)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        return MapHelper.annotationView(mapView: mapView, annotation: annotation)
    }
}
