//
//  ChoosePointViewController.swift
//  SARA Croisiere
//
//  Created by Rose on 07/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

public class ChoosePointViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, SelectButtonCellDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: ChoosePointViewController.classForCoder())
    var sortedPoints:[PointRealm]?
    public var viewModel: CreateCourseViewModel?
    var selectedIndex: IndexPath?
    public var isMarks: Bool = false
    
    public override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.customize()
        self.tableView.register(UINib(nibName: "HeaderTableView", bundle: Bundle(for: ChoosePointViewController.classForCoder())), forHeaderFooterViewReuseIdentifier: "HeaderTableView")
        self.tableView?.register(UINib(nibName: "SelectButtonCell", bundle: Bundle(for: ChoosePointViewController.classForCoder())), forCellReuseIdentifier: "choosePointIdentifier")
        // To remove gap between header and table view
        if #available(iOS 15, *) {
            tableView.sectionHeaderTopPadding = 0
        }
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isMarks {
            // For SARA Race, display only marks of the course
            if let vm = viewModel as? CreateRaceCourseViewModel {
                sortedPoints = vm.pointRealmUnique(points: vm.getTempPoints())
            } else {
                let marks = Mark.getSortedMarksByProximity()
                sortedPoints = marks
            }
        } else {
            let coursePoints = CoursePoint.getSortedCoursePointsByProximity()
            sortedPoints = coursePoints?.filter({ $0.distanceThreshold != 0})
        }
        self.selectedIndex = nil
        if let parentVC = self.parent as? AddPointSegmentedViewController {
            parentVC.validateButton.isEnabled = false
        }
        self.tableView?.reloadData()
    }
        
    func validate() {
        if let selectedRow = self.selectedIndex?.row {
            let point = sortedPoints![selectedRow]
            if self.viewModel?.addToRoute(point) == true {
                let when = DispatchTime.now() + 3
                DispatchQueue.main.asyncAfter(deadline: when) {
                    UIAccessibility.post(notification: UIAccessibility.Notification.announcement, argument: String(format: NSLocalizedString("add_point_success", bundle: self.bundle, comment: ""), point.name))
                }
                self.dismiss(animated: true, completion: {
                      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalChoosePointIsDimissed"), object: nil)
                })
            } else {
                let alert = Utils.showAlert(with: NSLocalizedString("warning", bundle: bundle, comment: ""), message: NSLocalizedString("warning_same_point_consecutively", bundle: bundle, comment: ""))
                self.present(alert, animated: true, completion: nil)
                self.selectedIndex = nil
                self.tableView?.reloadData()
            }
        }        
    }
    
    // MARK: - Tableview
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortedPoints?.count ?? 0
    }
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "choosePointIdentifier") as? SelectButtonCell else {
            return UITableViewCell()
        }
        cell.delegate = self
        cell.titleLabel?.font = UIFont.preferredFont(forTextStyle: .body)
        cell.titleLabel?.text = sortedPoints![indexPath.row].name
        cell.idElementSelected = String(indexPath.row)
        cell.selectionStyle = .none
        let bundle = Utils.bundle(anyClass: self.classForCoder)
        if self.selectedIndex == indexPath {
            cell.selectButton?.setImage(UIImage(named: "globe", in: bundle, compatibleWith: nil), for: .normal)
        } else {
            cell.selectButton?.setImage(UIImage(named: "circle", in: bundle, compatibleWith: nil), for: .normal)
        }
        cell.selectButton?.tintColor = Utils.tintIcon
        cell.selectButton?.isAccessibilityElement = false
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedIndex = indexPath
        let bundle = Utils.bundle(anyClass: self.classForCoder)
        if let cell = tableView.cellForRow(at: indexPath) as? SelectButtonCell  {
            cell.selectButton?.setImage(UIImage(named: "globe", in: bundle, compatibleWith: nil), for: .normal)
        }
        if let parentVC = self.parent as? AddPointSegmentedViewController {
            parentVC.validateButton.isEnabled = true
        }
    }
    
    public func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        
        if let indexPathForSelectedRow = self.selectedIndex {
            
            // Deselect current selected row
            tableView.deselectRow(at: indexPathForSelectedRow, animated: false)
            if let cell = tableView.cellForRow(at: indexPathForSelectedRow) as? SelectButtonCell {
                cell.selectButton?.setImage(UIImage(named: "circle", in: Utils.bundle(anyClass: self.classForCoder), compatibleWith: nil), for: .normal)
            }
            
            // If we're on the same row, disable the validation
            if indexPathForSelectedRow == indexPath {
                self.selectedIndex = nil
                if let parentVC = self.parent as? AddPointSegmentedViewController {
                    parentVC.validateButton.isEnabled = false
                }
                return nil
            }
        }
        return indexPath
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderTableView" ) as! HeaderTableView
        if let _ = self.viewModel as? CreateRaceCourseViewModel {
            returnedView.customizeHeader(titleName: Utils.localizedString(forKey: "marks_list", app: "SARARegate"), buttonIsAccessible: false)
        } else {
            returnedView.customizeHeader(titleName: NSLocalizedString("points_list", bundle: bundle, comment: ""), buttonIsAccessible: false)
        }
        
//        if let image = UIImage(named: "map")?.withRenderingMode(.alwaysTemplate) {
//            returnedView.imageButton.setImage(image, for: .normal)
//            returnedView.imageButton.tintColor = Utils.secondaryColor
//            returnedView.imageButton.isAccessibilityElement = false
//        }
        return returnedView
    }

    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    // MARK: - SelectButtonCellDelegate
    public func didSelectCell(id: String) {
        guard let idInt = Int(id) else {
            return
        }
        let indexPathSelected = IndexPath(item: idInt, section: 0)
        var indexToReload: [IndexPath]  = []
        indexToReload.append(indexPathSelected)
        if self.selectedIndex == indexPathSelected { // We are on the same row disable it
            self.selectedIndex = nil
            if let parentVC = self.parent as? AddPointSegmentedViewController {
                parentVC.validateButton.isEnabled = false
            }
        } else {
            if let oldSelectedIndex = self.selectedIndex {
                indexToReload.append(oldSelectedIndex)
            }
            self.selectedIndex = indexPathSelected
            if let parentVC = self.parent as? AddPointSegmentedViewController {
                parentVC.validateButton.isEnabled = true
            }
        }
        self.tableView.reloadRows(at: indexToReload, with: .none)
    }
}
