//
//  CourseVisualizationTableViewController.swift
//  SARA Croisiere
//
//  Created by Marine IL on 13.03.19.
//  Copyright © 2019 Rose. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import RxSwift

public class CourseVisualizationViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, MKMapViewDelegate, FirebaseManagerDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var mapButton: UIBarButtonItem!
    @IBOutlet weak var navigationBar: UINavigationBar!
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: CourseVisualizationViewController.classForCoder())
    public var viewModel: CreateCourseViewModel?
    public var titleNavigation: String?
    let disposeBag = DisposeBag()
    
    var displayMode:Bool = false {
        didSet{
            if displayMode {
                tableView.isHidden = true
                self.navigationBar.items?.first?.rightBarButtonItems?.first?.image = UIImage(named: "list", in: bundle, compatibleWith: nil)
                self.navigationBar.items?.first?.rightBarButtonItems?.first?.accessibilityLabel = NSLocalizedString("list", bundle: bundle, comment: "")
                mapView.isHidden = false
            } else {
                tableView.isHidden = false
                mapView.isHidden = true
                self.navigationBar.items?.first?.rightBarButtonItems?.first?.image = UIImage(named: "map", in: bundle, compatibleWith: nil)
                self.navigationBar.items?.first?.rightBarButtonItems?.first?.accessibilityLabel = NSLocalizedString("map", bundle: bundle, comment: "")
            }
        }
    }
    
    var points: [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
    
    // MARK: - IBActions
    @IBAction func switchDisplayMode(_ sender: Any) {
        displayMode = !displayMode
    }
        
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        self.navigationBar.items?.first?.title = titleNavigation
        self.navigationBar.items?.first?.leftBarButtonItems?.first?.title = NSLocalizedString("back", bundle: bundle, comment: "")
        self.navigationBar.items?.first?.leftBarButtonItems?.first?.style = .done
        self.navigationBar.items?.first?.rightBarButtonItems?.first?.tintColor = Utils.primaryColor
        self.navigationBar.items?.first?.rightBarButtonItems?.first?.accessibilityLabel = NSLocalizedString("map", bundle: bundle, comment: "")
        self.navigationBar.items?.first?.rightBarButtonItems?.first?.image = UIImage(named: "map", in: bundle, compatibleWith: nil)
        
        self.tableView.register(UINib(nibName: "InformationCell", bundle: Bundle(for: CourseVisualizationViewController.classForCoder())), forCellReuseIdentifier: "InfosCellIdentifier")
        
        FirebaseManager.shared.delegate = self
        mapView.delegate = self
        
        self.loadViews()
        
        LocationManager.shared.locationVariable
        .withPrevious()
        .subscribe(onNext: { [weak self] (previousLocation, newLocation) in
            guard newLocation != nil && self != nil else {
                self!.mapView.removeAnnotations(self!.mapView.annotations.filter({$0 is MKUserLocation && ($0 as! MapAnnotation).type == .userLocation}))
                return
            }
            if previousLocation != newLocation { // Add user position
                MapHelper.userPosition(mapView: self!.mapView, newLocation: newLocation!)
            }
        }).disposed(by: disposeBag)
    }
    
    //
    func loadViews() {
        self.mapView.removeAnnotations(self.mapView.annotations)
        self.mapView.removeOverlays(self.mapView.overlays)
        
        self.viewModel?.getPointsWithCoord()
        if let _ = self.viewModel as? CreateCruiseCourseViewModel { // Display all the points and beacons in SARA Navigation
            MapHelper.getAllMapAnnotations(map: self.mapView, pointsDisplayed: self.viewModel?.pointsWithCoord ?? [])
        }
        
        let annotations = getMapAnnotations()
        mapView.addAnnotations(annotations)
        
        // Connect all the mappoints using Poly line.
        let polyline = MKPolyline(coordinates: points, count: points.count)
        polyline.title = "course"
        mapView.addOverlay(polyline)
        
        //Zoom to fit the route
        var annotationsToZoom: [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
        for annotation in annotations {
            annotationsToZoom.append(annotation.coordinate)
        }
        let polylineZoom = MKPolyline(coordinates: annotationsToZoom, count: annotationsToZoom.count)
        MapHelper.zoomToPolyLine(map: mapView, polyLine: polylineZoom, animated: false)
    }
    
    // MARK: - Table view data source

    public func numberOfSections(in tableView: UITableView) -> Int {
        self.viewModel?.getPointsWithCoord()
        return (self.viewModel?.pointsWithCoord?.count ?? 2) - 1
//        return (self.viewModel?.numberOfPoints() ?? 2 ) - 1
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "InfosCellIdentifier") as? InformationCell else {
            return UITableViewCell()
        }
        guard let p1 = self.viewModel?.pointsWithCoord?[indexPath.section], let p2 = self.viewModel?.pointsWithCoord?[indexPath.section + 1] else {
            return UITableViewCell()
        }
        let gpsPos1 = p1.getCoordinates(method: .middle)
        let gpsPos2 = p2.getCoordinates(method: .middle)
        
        // Configure the cell...
        // Infos = (Name of the info, value of the info, unit of the info)
        var infos:(String?, String?, NavigationUnit?)
        if indexPath.row == 0 {
            let distance =  Utils.tupleDistance(floatDistance: NavigationHelper.getDistanceFrom(gpsPosition1: gpsPos1, to: gpsPos2))!
            if distance.1 == .meter {
                infos = (NSLocalizedString("distance", bundle: bundle, comment: ""), "\(distance.0.roundDown)", distance.1)
            } else {
                infos = (NSLocalizedString("distance", bundle: bundle, comment: ""), "\(distance.0)", distance.1)
            }
        } else if indexPath.row == 1 {
            let orientation = NavigationHelper.segmentOrientation(position1: gpsPos1, position2: gpsPos2)
            infos = (NSLocalizedString("next_cardinal_orientation", bundle: bundle, comment: ""), "\(orientation)", NavigationUnit.degrees)
        }
        cell.infos = (infos.0, infos.1, infos.2, nil, nil)
//        cell.customizeCell()
        
        return cell
    }

    // Override to support conditional editing of the table view.
    public func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let p1 = self.viewModel?.pointsWithCoord?[section], let p2 = self.viewModel?.pointsWithCoord?[section + 1] else {
            return nil
        }
        return "\(p1.name) - \(p2.name)"
    }
    
    //MARK: - Annotations
    
    func getMapAnnotations() -> [MapAnnotation] {
        var annotations = [MapAnnotation]()
        
        // Get route and if it's a match race
        var _route: Course?
        var isMatchRace: Bool = false
        if let cc = viewModel as? CreateCruiseCourseViewModel {
            _route = cc.route
        } else if let rc = viewModel as? CreateRaceCourseViewModel {
            _route = rc.course
            isMatchRace = rc.type.value == .matchRace
        }
        
        //iterate and create annotations
        if let items = self.viewModel {
            var index = 0
            var point = items.getPointWithCoord(index)
            var nextPoint: PointRealm?
            while ( index < items.numberOfPoints() ?? 0 ) {
                if let p1 = point {
                    if let p2 = items.getPointWithCoord(index + 1) {
                        nextPoint = p2
                        // calculate with middle point in case of a mark
                        let gpsPos1 = p1.getCoordinates(method: .middle)
                        let gpsPos2 = p2.getCoordinates(method: .middle)
                        guard gpsPos1 != nil, gpsPos2 != nil else {
                            break
                        }
                        let distance =  Utils.tupleDistance(floatDistance: NavigationHelper.getDistanceFrom(gpsPosition1: gpsPos1, to: gpsPos2))!
                        let orientation = NavigationHelper.segmentOrientation(position1: gpsPos1, position2: gpsPos2)
                    
                        // Add coordinates in points array for the line between the differents points
                        if index == 0 {
                            points.append(CLLocationCoordinate2D(latitude: Double(gpsPos1!.latitude), longitude: Double(gpsPos1!.longitude)))
                        }
                        points.append(CLLocationCoordinate2D(latitude: Double(gpsPos2!.latitude), longitude: Double(gpsPos2!.longitude)))
                        
                        // add annotations
                        if let coursePoint = p1 as? CoursePoint, let gpsPosition = coursePoint.gpsPosition {
                            let annotation = MapAnnotation(latitude: Double(gpsPosition.latitude), longitude: Double(gpsPosition.longitude), type: .coursePoint, isCurrent: true)
                            annotation.title = "\(coursePoint.name) - \(p2.name)"
                            annotation.subtitle = getSubtitle(distance: distance, orientation: orientation)
                            annotations.append(annotation)
                            // Add validation point circle
                            let circle = MKCircle(center: annotation.coordinate, radius: CLLocationDistance(coursePoint.distanceThreshold / 2))
                            circle.title = "distanceThreshold"
                            mapView.addOverlay(circle)
                        } else if let mark = p1 as? Mark {
                            var coordMarks: [CLLocationCoordinate2D] = []
                            mark.buoys.forEach({
                                if let gpsPosition = $0.gpsPosition {
                                    let annotation = MapAnnotation(latitude: Double(gpsPosition.latitude), longitude: Double(gpsPosition.longitude), type: .mark, isCurrent: true)
                                    let vm = viewModel as? CreateCruiseCourseViewModel
                                    annotation.title = "\(mark.name)(\(NSLocalizedString("unit_" + $0.toLetAt(inversed: vm?.reversedRoute), bundle: bundle, comment: ""))) - \(p2.name)"
                                    annotation.subtitle = getSubtitle(distance: distance, orientation: orientation)
                                    annotations.append(annotation)
                                    coordMarks.append(annotation.coordinate)
                                    if mark.markType == Mark.MarkType.simple.rawValue {
                                        var posA: GPSPosition?
                                        if index == 0 && (_route as? RaceCourse) != nil { // It's the first mark of the course and there is a next one. Only for SARA Race, extension of segment first mark - second mark. Just diplax if there is a 2nd mark otherwise it depends to the boat location
                                            posA = p2.getCoordinates(method: .toLetAt, toLetAt: mark.buoys.first?.toLetAt(inversed: vm?.reversedRoute), route: _route, isMatchRace: isMatchRace)
                                        } else if index > 0, let p0 = items.getPointWithCoord(index - 1){
                                            posA = p0.getCoordinates(method: .toLetAt, toLetAt: mark.buoys.first?.toLetAt(inversed: vm?.reversedRoute), route: _route, isMatchRace: isMatchRace)
                                        }
                                        if posA != nil {
                                            let valMark = ValidationMarkData(posA: posA, posB: gpsPos1, posC: p2.getCoordinates(method: .toLetAt, toLetAt: mark.buoys.first?.toLetAt(inversed: vm?.reversedRoute), route: _route, isMatchRace: isMatchRace), toLetAt: mark.buoys.first?.toLetAt(inversed: vm?.reversedRoute), course: _route)
                                            valMark.validationLine()
                                            var bissectriceMarks: [CLLocationCoordinate2D] = []
                                            bissectriceMarks.append(annotation.coordinate)
                                            if let bissectrice = valMark.validationLineCoord {
                                                bissectriceMarks.append(bissectrice)
                                            }
                                            let polylineBissectrice = MKPolyline(coordinates: bissectriceMarks, count: bissectriceMarks.count)
                                            polylineBissectrice.title = "bisector"
                                            mapView.addOverlay(polylineBissectrice)
                                        }
                                    }
                                }
                            })
                            if coordMarks.count > 1 { // it's  double mark, add crossing line between the mark
                                let polylineMark = MKPolyline(coordinates: coordMarks, count: coordMarks.count)
                                polylineMark.title = "crossingLineMark"
                                mapView.addOverlay(polylineMark)
                            }
                        }
                    } else { // Last point
                        if let coursePoint = p1 as? CoursePoint, let gpsPosition = coursePoint.gpsPosition {
                            let annotation = MapAnnotation(latitude: Double(gpsPosition.latitude), longitude: Double(gpsPosition.longitude), type: .coursePoint, isCurrent: true)
                            annotation.title = "\(coursePoint.name)"
                            annotations.append(annotation)
                            // Add validation point circle
                            let circle = MKCircle(center: annotation.coordinate, radius: CLLocationDistance(coursePoint.distanceThreshold / 2))
                            circle.title = "distanceThreshold"
                            mapView.addOverlay(circle)
                        } else if let mark = p1 as? Mark {
                            var coordMarks: [CLLocationCoordinate2D] = []
                            mark.buoys.forEach({
                                if let gpsPosition = $0.gpsPosition {
                                    let annotation = MapAnnotation(latitude: Double(gpsPosition.latitude), longitude: Double(gpsPosition.longitude), type: .mark, isCurrent: true)
                                    let vm = viewModel as? CreateCruiseCourseViewModel
                                    annotation.title = "\(mark.name) - \(NSLocalizedString("unit_" + $0.toLetAt(inversed: vm?.reversedRoute), bundle: bundle, comment: ""))"
                                    annotations.append(annotation)
                                    coordMarks.append(annotation.coordinate)
                                    if index > 0, mark.markType == Mark.MarkType.simple.rawValue, let p0 = items.getPointWithCoord(index - 1) {
                                        var posC: GPSPosition?
                                        if (_route as? RaceCourse) != nil {
                                            // TODO: Check if's middle or toLetAt. Same for posA.
                                            posC = p0.getCoordinates(method: .middle, route: _route, isMatchRace: isMatchRace)
                                        }
                                        let valMark = ValidationMarkData(posA: p0.getCoordinates(method: .middle, route: _route, isMatchRace: isMatchRace), posB: gpsPosition, posC: posC, toLetAt: mark.buoys.first?.toLetAt(inversed: vm?.reversedRoute), course: _route)
                                        valMark.validationLine()
                                        var bissectriceMarks: [CLLocationCoordinate2D] = []
                                        bissectriceMarks.append(annotation.coordinate)
                                        if let bissectrice = valMark.validationLineCoord {
                                            bissectriceMarks.append(bissectrice)
                                        }
                                        let polylineBissectrice = MKPolyline(coordinates: bissectriceMarks, count: bissectriceMarks.count)
                                        polylineBissectrice.title = "bisector"
                                        mapView.addOverlay(polylineBissectrice)
                                    }
                                }
                            })
                            if coordMarks.count > 1 { // it's  double mark, add crossing line between the mark
                                let polylineMark = MKPolyline(coordinates: coordMarks, count: coordMarks.count)
                                polylineMark.title = "crossingLineMark"
                                mapView.addOverlay(polylineMark)
                            }
                        }
                    }
                    index += 1
                    point = nextPoint
                }
            }
        }
        return annotations
    }
    
    func getSubtitle(distance: (Float, NavigationUnit?), orientation: Int) -> String {
        if distance.1 == .meter {
            return "\(NSLocalizedString("distance", bundle: bundle, comment: "")): \(distance.0.roundDown) \(distance.1!.abbreviation), \(NSLocalizedString("next_cardinal_orientation", bundle: bundle, comment: "")): \(orientation) \(NavigationUnit.degrees.abbreviation)"
        } else {
            return "\(NSLocalizedString("distance", bundle: bundle, comment: "")): \(distance.0) \(distance.1!.abbreviation), \(NSLocalizedString("next_cardinal_orientation", bundle: bundle, comment: "")): \(orientation) \(NavigationUnit.degrees.abbreviation)"
        }
    }
        
    //MARK: - MapViewDelegate methods
    
    public func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        return MapHelper.overlayRenderer(overlay: overlay)
    }
    
    public func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        return MapHelper.annotationView(mapView: mapView, annotation: annotation)
    }
    
    // MARK: - FirebaseManagerDelegate
    public func courseChanged(course: Course?) {
        self.navigationBar.items?.first?.title = course?.name
        if let vm = self.viewModel as? CreateRaceCourseViewModel, let raceCourse = course as? RaceCourse {
            vm.updateViewModel(course: raceCourse)
        }
        self.loadViews()
        self.tableView.reloadData()
    }
    
    public func courseRemoved(courseId: String) {}
    
    public func courseAdded(course: Course?) {}
}
