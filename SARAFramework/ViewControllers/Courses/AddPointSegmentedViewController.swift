//
//  AddPointSegmentedViewController.swift
//  SARA Croisiere
//
//  Created by Marine on 20.04.20.
//  Copyright © 2020 GraniteApps. All rights reserved.
//

import UIKit

public class AddPointSegmentedViewController: UIViewController {

    // MARK:- Outlets
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var validateButton: UIBarButtonItem!
    @IBOutlet weak var titleNavigation: UINavigationItem!
    @IBOutlet weak var addPointFromSegmentedControl: UISegmentedControl!
    @IBOutlet weak var containerView: UIView!
    
    // MARK:- Variables
    let bundle = Utils.bundle(anyClass: AddPointSegmentedViewController.classForCoder())
    
    private lazy var firstViewController: UIViewController = {
        
        let viewController = self.viewControllersList[0]
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)

        return viewController
    }()

    private lazy var secondViewController: UIViewController = {
        
        let viewController = self.viewControllersList[1]
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)

        return viewController
    }()
    
    var selectedViewController: UIViewController?
    public var viewControllersList: [UIViewController] = []
    
    // MARK:- View Life Cycle Methods
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.cancelButton.title = NSLocalizedString("cancel", bundle: bundle, comment: "")
        self.validateButton.title = NSLocalizedString("ok", bundle: bundle, comment: "")
        if let _ = self.firstViewController as? CreatePointViewController {
            self.titleNavigation.title = NSLocalizedString("add_course_point", bundle: bundle, comment: "")
            self.addPointFromSegmentedControl.setTitle(NSLocalizedString("new", bundle: bundle, comment: ""), forSegmentAt: 0)
            self.addPointFromSegmentedControl.setTitle(NSLocalizedString("existing", bundle: bundle, comment: ""), forSegmentAt: 1)
            self.addPointFromSegmentedControl.setTitle(NSLocalizedString("shared", bundle: bundle, comment: ""), forSegmentAt: 2)
            self.addPointFromSegmentedControl.subviews[1].accessibilityLabel = "\(NSLocalizedString("point", bundle: bundle, comment: "")) \(NSLocalizedString("shared", bundle: bundle, comment: ""))"
            self.addPointFromSegmentedControl.setEnabled(false, forSegmentAt: 2)
        } else if let _ = self.firstViewController as? CreateMarkViewController {
            self.titleNavigation.title = NSLocalizedString("add_mark", bundle: bundle, comment: "")
            self.addPointFromSegmentedControl.setTitle(NSLocalizedString("new_fem", bundle: bundle, comment: ""), forSegmentAt: 0)
            self.addPointFromSegmentedControl.setTitle(NSLocalizedString("existing_fem", bundle: bundle, comment: ""), forSegmentAt: 1)
            self.addPointFromSegmentedControl.setTitle(NSLocalizedString("shared_fem", bundle: bundle, comment: ""), forSegmentAt: 2)
            self.addPointFromSegmentedControl.subviews[1].accessibilityLabel = "\(NSLocalizedString("mark", bundle: bundle, comment: "")) \(NSLocalizedString("shared_fem", bundle: bundle, comment: ""))"
            self.addPointFromSegmentedControl.setEnabled(false, forSegmentAt: 2)
        }
        
        self.addPointFromSegmentedControl.customize()
        
        self.setupView()
        if self.viewControllersList.count == 1 { // Point edition, disable segmented control
            self.addPointFromSegmentedControl.isEnabled = false
        }
    }
    
    // MARK:- Action Methods
    @IBAction func dismiss(_ sender: Any) {
        present(Utils.showCancelAlert(controllerToDismiss: self), animated: true, completion: {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalCreatePointIsDimissed"), object: nil)
        })
    }
    
    @IBAction func validate(_ sender: Any) {
        switch self.addPointFromSegmentedControl.selectedSegmentIndex {
        case 0:
            if let pointFirstVC = self.firstViewController as? CreatePointViewController {
                let _ = pointFirstVC.validate()
            } else if let markFirstVC = self.firstViewController as? CreateMarkViewController {
                let _ = markFirstVC.validate()
            }
            self.dismiss(animated: true, completion: {
                  NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalCreatePointIsDimissed"), object: nil)
            })
        case 1:
            if let secondVC = self.secondViewController as? ChoosePointViewController {
                secondVC.validate()
            }
        default:
            self.dismiss(animated: true, completion: {
                  NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalCreatePointIsDimissed"), object: nil)
            })
            break
        }
    }
    
    @IBAction func addPointFromSelection(_ sender: Any) {
        updateView()
    }
    
    // MARK:- Custom Methods
    private func add(asChildViewController viewController: UIViewController) {

        // Add Child View Controller
        addChild(viewController)

        // Add Child View as Subview
        containerView.addSubview(viewController.view)

        // Configure Child View
        viewController.view.frame = containerView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        // Notify Child View Controller
        viewController.didMove(toParent: self)
    }

    //----------------------------------------------------------------

    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParent: nil)

        // Remove Child View From Superview
        viewController.view.removeFromSuperview()

        // Notify Child View Controller
        viewController.removeFromParent()
    }

    //----------------------------------------------------------------

    private func updateView() {
        if let _selectedVC = self.selectedViewController {
            remove(asChildViewController: _selectedVC)
        }
        switch self.addPointFromSegmentedControl.selectedSegmentIndex {
        case 0:
            add(asChildViewController: firstViewController)
            self.selectedViewController = firstViewController
            break
        case 1:
            add(asChildViewController: secondViewController)
            self.selectedViewController = secondViewController
            break
        case 2:
            break
        default:
            break
        }
    }

    //----------------------------------------------------------------

    func setupView() {
        updateView()
    }
}
