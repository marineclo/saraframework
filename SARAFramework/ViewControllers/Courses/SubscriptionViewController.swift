//
//  SubscribedTableViewController.swift
//  SARAFramework
//
//  Created by Marine on 14.09.22.
//

import UIKit

public class SubscriptionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, SelectButtonCellDelegate, HeaderTableViewDelegate, FirebaseManagerDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var searchBarConstraint: NSLayoutConstraint! // Need to remove weak for constraint : https://stackoverflow.com/questions/38051879/why-weak-iboutlet-nslayoutconstraint-turns-to-nil-when-i-make-it-inactive
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: SubscriptionViewController.classForCoder())
    var courses: [Course]?
    var filteredCourses: [Course]?
    var selectedIndex: IndexPath?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        FirebaseManager.shared.delegate = self
        
        self.tableView.customize()
        self.tableView.register(UINib(nibName: "HeaderTableView", bundle: Bundle(for: SubscriptionViewController.classForCoder())), forHeaderFooterViewReuseIdentifier: "HeaderTableView")
        self.tableView?.register(UINib(nibName: "SelectButtonCell", bundle: Bundle(for: SubscriptionViewController.classForCoder())), forCellReuseIdentifier: "chooseObjectCell")
        if #available(iOS 15, *) { // To remove gap between header and table view
            self.tableView?.sectionHeaderTopPadding = 0
        }
        
        courses = FirebaseManager.shared.firebaseCourses
        filteredCourses = FirebaseManager.shared.firebaseCourses
        
        searchBarConstraint.isActive = false
        searchBar.isHidden = true
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }

    // MARK: - Table view data source
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredCourses?.count ?? 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "chooseObjectCell") as? SelectButtonCell else {
            return UITableViewCell()
        }
        cell.delegate = self
        guard let obj = filteredCourses?[indexPath.row], let course = obj as? Course else {
            return UITableViewCell()
        }
        cell.titleLabel?.text = course.name
        cell.idElementSelected = String(indexPath.row)
        cell.selectionStyle = .none
        let bundle = Utils.bundle(anyClass: self.classForCoder)
        if course.status == .subscribed /*|| self.selectedIndex.value == indexPath*/ {
            self.selectedIndex = indexPath
            cell.selectButton?.setImage(UIImage(named: "globe", in: bundle, compatibleWith: nil), for: .normal)
            cell.accessibilityLabel = "\(NSLocalizedString("unsubscribe", bundle: bundle, comment: "")) \(course.name)"
        } else {
            cell.selectButton?.setImage(UIImage(named: "circle", in: bundle, compatibleWith: nil), for: .normal)
            cell.accessibilityLabel = "\(NSLocalizedString("subscribe", bundle: bundle, comment: "")) \(course.name)"
        }
        cell.selectButton?.tintColor = Utils.tintIcon
        cell.selectButton?.isAccessibilityElement = false
        return cell
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let bundle = Utils.bundle(anyClass: self.classForCoder)
        if let cell = tableView.cellForRow(at: indexPath) as? SelectButtonCell  {
            cell.selectButton?.setImage(UIImage(named: "globe", in: bundle, compatibleWith: nil), for: .normal)
            if let idElementSelected = cell.idElementSelected { // Call the function with VoiceOver or when you click on the cell
                self.didSelectCell(id: idElementSelected)
            }
        }
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderTableView" ) as! HeaderTableView
        returnedView.customizeHeader(titleName: Utils.localizedString(forKey: "subscriptions_available", app: "SARARegate") ?? "", image: UIImage(named: "search", in: bundle, compatibleWith: nil), accessibleText: NSLocalizedString("search", bundle: bundle, comment: ""), buttonIsAccessible: true)
        returnedView.delegate = self
        return returnedView
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    // MARK: - Suscribe and Unsuscribe
    func subscribe(indexPathSelected: IndexPath, course: Course) {
        self.selectedIndex = indexPathSelected
        course.status = .subscribed
        FirebaseManager.shared.retrieveCourse(courseId: course.id, completion: { course in
            // Add observers to get notify if there is a change
            guard let _course = course else { return }
            FirebaseManager.shared.addObservers(course: _course)
            if _course.allPointsHasCoordinates() && _course.points.count != 0 { // Activate course only if there is points and all points have coordinates
                let alert = UIAlertController(title: NSLocalizedString("new_subscription", bundle: self.bundle, comment: ""), message: NSLocalizedString("new_subscription_activation", bundle: self.bundle, comment: ""), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("yes", bundle: self.bundle, comment: ""), style: .default, handler: {_ in
                    NavigationManager.shared.updateNavView = true
                    NavigationManager.shared.setCurrentRoute(route: course)
                }))
                alert.addAction(UIAlertAction(title: NSLocalizedString("no", bundle: self.bundle, comment: ""), style: .cancel, handler: nil))
                self.present(alert, animated: true)
            } else {
                let alert = UIAlertController(title: NSLocalizedString("new_subscription", bundle: self.bundle, comment: ""), message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("ok", bundle: self.bundle, comment: ""), style: .cancel, handler: nil))
                self.present(alert, animated: true)
            }
        })
    }

    func unsubscribedRoute(course: Course, completion: (() -> ())? = nil) {
        // Remove observers
        course.status = .none
        FirebaseManager.shared.removeObservers(path: course.id)
        let title = String(format: NSLocalizedString("unsubscribe_not_save", bundle: self.bundle, comment: ""), course.name)
        let alert = UIAlertController(title: title, message: NSLocalizedString("unsubscribe_save", bundle: self.bundle, comment: ""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("yes", bundle: self.bundle, comment: ""), style: .default, handler: {_ in
            // Save course with different ID in the database and add a star at the end of the name
            if let realmCourse = Course.getRealmCourse(courseId: course.id) {
                if let raceCourse = (realmCourse as? RaceCourse) {
                    raceCourse.saveCourseStar(course: raceCourse)
                }
                realmCourse.removeCourseWithPoints()
            }
            completion?()
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("no", bundle: self.bundle, comment: ""), style: .cancel, handler: { _ in
            // Delete route from database
            // Get course from Database and delete
            if let realmCourse = Course.getRealmCourse(courseId: course.id) {
                realmCourse.removeCourseWithPoints()
            }
            completion?()
        }))
        self.present(alert, animated: true)
    }
    
    // MARK: - SelectButtonCellDelegate
    public func didSelectCell(id: String) {
        guard let idInt = Int(id), let course = filteredCourses?[idInt] as? Course else {
            return
        }
        let indexPathSelected = IndexPath(item: idInt, section: 0)
        var indexToReload: [IndexPath]  = []
        indexToReload.append(indexPathSelected)
        
        if self.selectedIndex == indexPathSelected { // We are on the same row: unsubscribe
            self.selectedIndex = nil
            unsubscribedRoute(course: course, completion: nil)
        } else {
            if let oldSelectedIndex = self.selectedIndex.value { // There is already a subscribed course
                indexToReload.append(oldSelectedIndex)
                if let courseToUnsub = filteredCourses?[oldSelectedIndex.row] {
                    unsubscribedRoute(course: courseToUnsub, completion: {
                        self.subscribe(indexPathSelected: indexPathSelected, course: course)
                        self.tableView.reloadRows(at: indexToReload, with: .none)
                    })
                }
            } else {
                self.subscribe(indexPathSelected: indexPathSelected, course: course)
            }
        }
        self.tableView.reloadRows(at: indexToReload, with: .none)
    }
    
    // MARK: - HeaderTableViewDelegate
    func clickOnImage() {
        // Display search bar
        searchBarConstraint.isActive = true
        searchBar.isHidden = false
        searchBar.becomeFirstResponder()
    }
    
    // MARK: - UISearchBarDelegate
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredCourses = searchText.isEmpty ? courses : courses?.filter({ (obj) -> Bool in
            var dataString: String = ""
            if let course = obj as? Course {
                dataString = course.name
            }
            return dataString.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        })
        tableView.reloadData()
    }
    
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    public func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        DispatchQueue.main.async {
            if let cancelButton : UIButton = searchBar.value(forKey: "cancelButton") as? UIButton{
                cancelButton.isEnabled = true
                cancelButton.isAccessibilityElement = true
            }
        }
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.resignFirstResponder()
        searchBarConstraint.isActive = false
        searchBar.isHidden = true
    }
    
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    // MARK: - FirebaseManagerDelegate
    public func courseChanged(course: Course?) {
        guard course != nil else { return }
        // Update name
        if let indexFilter = self.filteredCourses?.firstIndex(where: {$0.id == course!.id}), let index = self.courses?.firstIndex(where: {$0.id == course!.id}) {
            self.courses?[index].name = course!.name
            self.filteredCourses?[indexFilter].name = course!.name
            self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
        }
    }
    
    public func courseRemoved(courseId: String) {
        // Delete course from the course
        if let indexFilter = self.filteredCourses?.firstIndex(where: {$0.id == courseId}), let index = self.courses?.firstIndex(where: {$0.id == courseId}) {
            self.courses?.remove(at: index)
            self.filteredCourses?.remove(at: indexFilter)
            self.tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .none)
            self.selectedIndex = nil
        }
    }
    
    public func courseAdded(course: Course?) {
        guard course != nil else { return }
        self.courses?.append(course!)
        self.filteredCourses?.append(course!)
        self.tableView.insertRows(at: [IndexPath(row: (self.filteredCourses?.count ?? 0) - 1, section: 0)], with: .none)
    }
}
