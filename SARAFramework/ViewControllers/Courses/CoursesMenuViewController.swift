//
//  CoursesMenuViewController.swift
//  SARA Croisiere
//
//  Created by Tristan Derbanne on 19.04.19.
//  Copyright © 2019 Rose. All rights reserved.
//

import UIKit
import RxCocoa

public class CoursesMenuViewController: UIViewController, TabsMenuViewControllerDelegate, UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    // MARK: - IBOutlets
    @IBOutlet weak var pageViewContainer: UIView!
    @IBOutlet weak var tabsMenuContainer: UIView!
    @IBOutlet weak var navigationBar: UINavigationBar!
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: CoursesMenuViewController.classForCoder())
    private var pageViewController: UIPageViewController?
    private var tabsMenuController: TabsMenuViewController?
    private var tabsMenuItems: [String] = []
    private var pagesViewControllers: [UIViewController] = []
    private var navBarTitle: String?
    
    // MARK: - View controller overrides
    public override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBarItem.selectedImage = UIImage(named: "itineraryTabBar", in: bundle, compatibleWith: nil)
        
        self.navigationBar.topItem?.title = self.navBarTitle
        
        // Tabs menu
        self.tabsMenuController = TabsMenuViewController()
        self.addChild(tabsMenuController!)
        tabsMenuContainer.addSubview(tabsMenuController!.view)
        self.tabsMenuController!.view.frame = CGRect(x: 0, y: 0, width: self.tabsMenuContainer.frame.size.width, height: self.tabsMenuContainer.frame.size.height)
        self.tabsMenuController!.menuItems = self.tabsMenuItems
        self.tabsMenuController!.accessibilityMenuItems = Utils.accessiblePageMenuItems(originalMenuItems: self.tabsMenuItems, selectedItemIndex: 0)
        self.tabsMenuController!.visibleItemsCount = self.tabsMenuItems.count
        self.tabsMenuController!.selectedItemIndex = 0
        self.tabsMenuController!.delegate = self
        if #available(iOS 13.0, *) {
            if self.traitCollection.userInterfaceStyle == .dark {
                self.tabsMenuController!.menuItemsColor = UIColor.label
            } else {
                self.tabsMenuController!.menuItemsColor = Utils.primaryColor
            }
            self.tabsMenuController!.menuBackgroundColor = UIColor.systemBackground
        } else {
            // Fallback on earlier versions
            self.tabsMenuController!.menuBackgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            self.tabsMenuController!.menuItemsColor = Utils.primaryColor
        }
                
        self.pageViewController?.setViewControllers([pagesViewControllers.first!], direction: .forward, animated: false, completion: nil)
        self.pageViewController?.dataSource = self
        self.pageViewController?.delegate = self
        
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        User.shared.email.asObservable()
            .subscribe(onNext: { [self] email in
                guard let vc = self.pagesViewControllers.last else { return }
                if email != nil {
                    vc.view.viewWithTag(100)?.removeFromSuperview()
                } else {
                    guard let labelView = Bundle(for: LabelView.self).loadNibNamed(String(describing: LabelView.self), owner: nil, options: nil)?.first as? LabelView else {
                        fatalError()
                    }
                    labelView.tag = 100
                    labelView.setLabel(text: NSLocalizedString("not_logged_in", bundle: self.bundle, comment: ""))
                    vc.view.addSubview(labelView)
                    labelView.translatesAutoresizingMaskIntoConstraints = false
                    labelView.topAnchor.constraint(equalTo: vc.view.topAnchor).isActive = true
                    labelView.leadingAnchor.constraint(equalTo: vc.view.leadingAnchor).isActive = true
                    labelView.trailingAnchor.constraint(equalTo: vc.view.trailingAnchor).isActive = true
                    labelView.bottomAnchor.constraint(equalTo: vc.view.bottomAnchor).isActive = true
                    
                    if let subscribeVC = vc as? SubscriptionViewController { // Empty list of courses in the subscrption view
                        subscribeVC.courses = []
                        subscribeVC.filteredCourses = []
                    }
                }
                
                let image = email != nil ? UIImage(named: "plugged", in: self.bundle, compatibleWith: nil) : UIImage(named: "unplugged", in: self.bundle, compatibleWith: nil)
                self.tabsMenuController?.setStatusImage(image, forItemAtIndex: self.tabsMenuItems.count - 1)
            })
    }
    
    // MARK: - Public setter functions
    public func setNavigationTitle(title: String) {
        self.navBarTitle = title
    }
    
    public func setTabMenuItems(items: [String]) {
        // Tabs menu items
        self.tabsMenuItems = items
    }
    
    public func setPagesViewController(viewControllers: [UIViewController]) {
        self.pagesViewControllers = viewControllers
    }
    
    // MARK: - Tabs menu view controller delegate
    public func tabsMenuViewController(_ controller: TabsMenuViewController, didSelectMenuAtIndex index: Int) {
        guard index < pagesViewControllers.count else {
            return
        }
        
        var direction: UIPageViewController.NavigationDirection = .forward
        if let currentVC = pageViewController?.viewControllers?.first {
            if let currentVCIndex = pagesViewControllers.firstIndex(of: currentVC) {
                if currentVCIndex > index {
                    direction = .reverse
                }
            }
        }
        
        // Updating accessibility label
        controller.accessibilityMenuItems?[index] = NSLocalizedString("selected", bundle: bundle, comment: "") + " " + NSLocalizedString("menu", bundle: bundle, comment: "") + " " + controller.menuItems![index] + " " + NSLocalizedString("page", bundle: bundle, comment: "") + " \(index + 1) " + NSLocalizedString("on", bundle: bundle, comment: "") + " \(controller.menuItems!.count)"
                
        self.pageViewController?.setViewControllers([pagesViewControllers[index]], direction: direction, animated: true, completion: nil)
    }
    
    public func tabsMenuViewController(_ controller: TabsMenuViewController, didDeSelectMenuAtIndex index: Int) {
        guard index < pagesViewControllers.count else {
            return
        }
        // Updating accessibility label
        controller.accessibilityMenuItems?[index] = NSLocalizedString("menu", bundle: bundle, comment: "") + " " + controller.menuItems![index] + " " + NSLocalizedString("page", bundle: bundle, comment: "") + " \(index + 1) " + NSLocalizedString("on", bundle: bundle, comment: "") + " \(controller.menuItems!.count)"
    }

    // MARK: - Page view data source & delegate
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let i = pagesViewControllers.firstIndex(of: viewController) {
            if i > 0 {
                return pagesViewControllers[i - 1]
            }
        }
        return nil
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let i = pagesViewControllers.firstIndex(of: viewController) {
            if i < (pagesViewControllers.count - 1) {
                return pagesViewControllers[i + 1]
            }
        }
        return nil
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if let i = pagesViewControllers.firstIndex(of: pageViewController.viewControllers!.first!) {
            self.tabsMenuController?.selectedItemIndex = i
        }
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        // Changing accessibility label of previously selected tabs menu
        if let i = self.tabsMenuController?.selectedItemIndex {
            tabsMenuController!.accessibilityMenuItems?[i] = NSLocalizedString("menu", bundle: bundle, comment: "") + " " + tabsMenuController!.menuItems![i] + " " + NSLocalizedString("page", bundle: bundle, comment: "") + " \(i + 1) " + NSLocalizedString("on", bundle: bundle, comment: "") + " \(tabsMenuController!.menuItems!.count)"
        }
        // Selecting corresponding tabs menu and updating accessibility labbel
        if let i = self.pagesViewControllers.firstIndex(of: pendingViewControllers.first!) {
            if self.tabsMenuController != nil {
                self.tabsMenuController!.selectedItemIndex = i
                self.tabsMenuController!.accessibilityMenuItems?[i] = NSLocalizedString("selected", bundle: bundle, comment: "") + " " + NSLocalizedString("menu", bundle: bundle, comment: "") + " " + tabsMenuController!.menuItems![i] + " " + NSLocalizedString("page", bundle: bundle, comment: "") + " \(i + 1) " + NSLocalizedString("on", bundle: bundle, comment: "") + " \(tabsMenuController!.menuItems!.count)"
            }
        }
    }
    
    // MARK: - Navigation
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowRoutesMenuPageVCSegue" {
            self.pageViewController = segue.destination as? UIPageViewController
        }
    }

}
