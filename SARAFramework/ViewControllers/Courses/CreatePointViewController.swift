//
//  CreatePointViewController.swift
//  SARA Croisiere
//
//  Created by Rose on 03/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import RxSwift
import RxCocoa
import RxOptional
import FirebaseAuth
import MapKit

public class CreatePointViewController: UIViewController, UITextFieldDelegate, CoordinatesViewDelegate {

    // MARK: - IBOutlets
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var constraintNameLabel: NSLayoutConstraint!
    @IBOutlet private weak var doneButton: UIBarButtonItem!
    @IBOutlet private weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var titleNavigation: UINavigationItem!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet private weak var nameTxtField: UITextField!
    
    @IBOutlet weak var waypointView: WaypointView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var visualizeButton: UIButton!
    @IBOutlet weak var activateButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
        
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: CreatePointViewController.classForCoder())
    let disposeBag = DisposeBag()
    public var viewModel: CreateCourseViewModel?
    public var indexInRoute: Int?
    var currentPoint: CoursePoint?
    var isNewRecordFromNavigation: Bool = false
    var temporaryPoint: CoursePoint {
        get {
            var gpsPos: GPSPosition?
            if let (latitude, longitude) = self.waypointView.coordinatesView.getCoordinates() {
                gpsPos = GPSPosition(latitude: latitude, longitude: longitude)
            }
            let distance = self.waypointView.sliderView.settingsValue.value
            let point = CoursePoint(name: nameTxtField.text ?? "", distanceThreshold: distance ?? Settings.shared.minimumDistanceThreshold, gpsPosition: gpsPos)
            point.id = currentPoint?.id ?? UUID().uuidString
            return point
        }
    }
    
    // MARK:- View Life Cycle Methods
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.nameLabel.text = NSLocalizedString("name", bundle: bundle, comment: "")
        self.nameLabel.isAccessibilityElement = false
        
        self.nameTxtField.delegate = self
        self.nameTxtField.text = currentPoint?.name
        self.nameTxtField.accessibilityLabel = NSLocalizedString("name", bundle: bundle, comment: "")
        // Close keyboard when tap on the view
        let tapGesture = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)

        // Waypoint view
        setupWaypointView()
                
        // Buttons
        setupButtons()
        
        // Bind the done button to the title of the point
        if self.parent as? AddPointSegmentedViewController != nil{
            self.navigationBar.isHidden = true
        } else {
            self.navigationBar.isHidden = false
            self.constraintNameLabel.isActive = false
            self.cancelButton.title = NSLocalizedString("cancel", bundle: bundle, comment: "")
            self.doneButton.title = NSLocalizedString("ok", bundle: bundle, comment: "")
            self.titleNavigation.title = self.currentPoint != nil ? NSLocalizedString("edit_point", bundle: bundle, comment: "") : NSLocalizedString("create_point_title", bundle: bundle, comment: "")
        }
        
        if currentPoint?.distanceThreshold == 0 { // It's a MOB, disable editing
            disableEditing()
        }
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(handleNotification(note:)), name: NSNotification.Name(rawValue:"modalSelectOnMapIsDimissed"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleNotification(note:)), name: NSNotification.Name(rawValue:"modalChooseObjectIsDimissed"), object: nil)
        //  Scroll keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        // Bind the done button to the title of the point
        nameTxtField
            .rx.text
            .map { $0?.isNotEmpty ?? false }
            .bind(to: self.parent as? AddPointSegmentedViewController != nil ? (self.parent as? AddPointSegmentedViewController)!.validateButton.rx.isEnabled : self.doneButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
        Observable.combineLatest(self.waypointView.coordinatesView.observableCoordinates(), self.nameTxtField.rx.text) { (coord, name) in
                return coord && name != nil && !name!.isEmpty
        }.bind(to: self.activateButton.rx.isEnabled)
        .disposed(by: disposeBag)
        self.waypointView.coordinatesView.observableCoordinates()
            .bind(to: self.visualizeButton.rx.isEnabled)
    }
        
    @objc func handleNotification(note: Notification) {
        switch note.name {
        case NSNotification.Name(rawValue:"modalSelectOnMapIsDimissed"):
            let annotation = note.userInfo!["annotation"] as! MKAnnotation
            let gpsPosition = GPSPosition(latitude: Float(annotation.coordinate.latitude), longitude: Float(annotation.coordinate.longitude))
            self.waypointView.coordinatesView.fillCoordinates(with: gpsPosition, auto: true)
        case NSNotification.Name(rawValue:"modalChooseObjectIsDimissed"):
            let object = note.userInfo!["object"] as! NSObject
            if let beacon = object as? Beacon {
                if let gpsPosition = beacon.gpsPosition {
                    self.waypointView.coordinatesView.fillCoordinates(with: gpsPosition, auto: true)
                }
                let name = beacon.name
                if let text = self.nameTxtField.text, text.isNotEmpty { // If there is already a name, warning to ask if replace it
                    let alertController = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: String(format: NSLocalizedString("warning_replace_name", bundle: bundle, comment: ""), "\(name)"), preferredStyle: .alert)
                    let yesAction = UIAlertAction(title: NSLocalizedString("yes", bundle: bundle, comment: ""), style: .default) { _ in
                        self.nameTxtField.text = "\(name)"
                        self.nameTxtField.sendActions(for: .valueChanged) // Observable only fire if we tell that the value changed
                        UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: self.nameTxtField)
                    }
                    let noAction = UIAlertAction(title: NSLocalizedString("no", bundle: bundle, comment: ""), style: .cancel, handler: nil)
                    alertController.addAction(noAction)
                    alertController.addAction(yesAction)
                    DispatchQueue.main.async {
                        self.present(alertController, animated: true, completion: nil)
                    }
                } else {
                    self.nameTxtField.text = "\(name)"
                    self.nameTxtField.sendActions(for: .valueChanged)
                    UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: self.nameTxtField)
                }
            }
        default:
            return
        }
    }
    
    public func setPoint(_ point: CoursePoint) {
        self.currentPoint = point
    }
    
    // MARK: - UI Methods
    func setupWaypointView() {
        // Add new button for existings beacons
        let beaconsListButton = UIButton()
        beaconsListButton.setImage(UIImage(named: "beacon", in: bundle, compatibleWith: nil), for: .normal)
        beaconsListButton.accessibilityLabel = NSLocalizedString("use_existing_beacons", bundle: bundle, comment: "")
        beaconsListButton.addTarget(self, action: #selector(selectCoordinatesAction), for: .touchUpInside)
        beaconsListButton.tintColor = Utils.tintIcon
        beaconsListButton.tag = 1
        self.waypointView.coordinatesView.setButtonOtherMethod(button: beaconsListButton)
        self.waypointView.coordinatesView.delegate = self
        if let gpsPos = self.currentPoint?.gpsPosition {
            self.waypointView.coordinatesView.fillCoordinates(with: gpsPos)
        }
        self.waypointView.sliderView.customize(with: SliderInfos(title: NSLocalizedString("distance_threshold_title", bundle: bundle, comment: ""), unit: NavigationUnit.meter, value: self.currentPoint?.distanceThreshold ?? 100, minValue: 1, maxValue: 1000, step: 10, isContinuous: false))
    }
    
    func setupButtons() {        
        activateButton.customizeSecondary()
        if let point = self.currentPoint, point == NavigationManager.shared.point.value {
            self.activateButton.setTitle(NSLocalizedString("deactivate", bundle: bundle, comment: ""), for: .normal)
            self.activateButton.accessibilityLabel = NSLocalizedString("deactivate", bundle: bundle, comment: "")
        } else {
            self.activateButton.setTitle(NSLocalizedString("activate", bundle: bundle, comment: ""), for: .normal)
            self.activateButton.accessibilityLabel = NSLocalizedString("activate", bundle: bundle, comment: "")
        }
        
        visualizeButton.customizeSecondary()
        visualizeButton.setTitle(NSLocalizedString("visualize", bundle: bundle, comment: ""), for: .normal)
        visualizeButton.accessibilityLabel = NSLocalizedString("visualize", bundle: bundle, comment: "")
        
        deleteButton.customizeSecondary()
        deleteButton.setTitle(NSLocalizedString("delete", bundle: bundle, comment: ""), for: .normal)
        deleteButton.accessibilityLabel = NSLocalizedString("delete", bundle: bundle, comment: "")
        if self.isNewRecordFromNavigation || self.currentPoint == nil{
            self.deleteButton.isEnabled = false
        } else {
            self.deleteButton.setTitleColor(#colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1), for: .normal)
            self.deleteButton.isEnabled = true
        }
    }
    
    func disableEditing() { // It's a MOB disable editing
        // Name
        self.nameTxtField.isEnabled = false
        // Waypoint
        self.waypointView.elements(isEnabled: false)
    }
    
    // MARK: - Segue
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "visualizePoint" {
            let point = temporaryPoint
            
            guard let destination = segue.destination as? PointVisualizationViewController else {
                return
            }
            destination.point = point
        }
    }
            
    // MARK: - IBActions
    @objc func selectCoordinatesAction(sender: UIButton!) {
        let storyboard = UIStoryboard(name: "Utils", bundle: bundle)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "SelectTableViewController") as? SelectTableViewController else {
                return
        }
        // Select existings beacons
        controller.titleNavigationBar = NSLocalizedString("beacons", bundle: bundle, comment: "")
        controller.titleTableHeader = NSLocalizedString("beacons_list", bundle: bundle, comment: "")
        controller.objects = Beacon.getSortedBeaconsByProximity()

        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: {
              NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalCreatePointIsDimissed"), object: nil)
        })
    }
    
    @IBAction func validate(_ sender: Any) {
        let _ = self.validate()
        self.dismiss(animated: true, completion: {
              NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalCreatePointIsDimissed"), object: nil)
        })
    }
    
    func validate() -> CoursePoint? {
        // Check if all coordinates and name are filled, otherwise, do not save it
        guard let name = nameTxtField.text else {
            let alert = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: NSLocalizedString("warning_no_point_name", bundle: bundle, comment: ""), preferredStyle: .alert)
            let alertAction = UIAlertAction(title: NSLocalizedString("ok", bundle: bundle, comment: ""), style: .default, handler: nil)
            alert.addAction(alertAction)
            present(alert, animated: true, completion: nil)
            return nil
        }
        var gpsPos: GPSPosition?
        if let (latitude, longitude) = self.waypointView.coordinatesView.getCoordinates() {
            gpsPos = GPSPosition(id: self.currentPoint?.gpsPosition?.id, latitude: latitude, longitude: longitude)
        }
        
        let distance = self.waypointView.sliderView.settingsValue.value
        
        let safeDistance = distance ?? Settings.shared.minimumDistanceThreshold
    
        // If the current point was already in the base
        if let safePoint = self.currentPoint, safePoint.name.isNotEmpty, !isNewRecordFromNavigation {
            let point = CoursePoint(id: safePoint.id ,name: name, distanceThreshold: safeDistance, gpsPosition: gpsPos)
            safePoint.update(with: point, shared: false)
            if self.viewModel == nil, let course = NavigationManager.shared.route.value, let anyPoint = point.anyPoint(), course.points.contains(anyPoint) { // We need to update the course if the point is in the current course
                NavigationManager.shared.setCurrentRoute(route: course)
                if let currentPointIndex = NavigationManager.shared.currentPointIndex.value {
                    NavigationManager.shared.setPoint(with: currentPointIndex)
                }
            }
            return safePoint
        } else {
            // If not, we must create a new point to persist it
            let point = CoursePoint(name: name, distanceThreshold: safeDistance, gpsPosition: gpsPos )
            point.save()
            self.viewModel?.addToRoute(point)
            return point
        }
    }
    
    @IBAction func activatePoint(_ sender: Any) {
        if let point = self.currentPoint, point == NavigationManager.shared.point.value {
            if NavigationManager.shared.route.value != nil {
                NavigationManager.shared.setNextPoint()
            } else {
                NavigationManager.shared.setCurrentRoute(route: nil)
            }
            self.activateButton.setTitle(NSLocalizedString("activate", bundle: bundle, comment: ""), for: .normal)
            return
        }
        // Save point
        if let point = self.validate(), let anyPoint = point.anyPoint()  {
            self.activateButton.setTitle(NSLocalizedString("deactivate", bundle: bundle, comment: ""), for: .normal)
            self.activateButton.accessibilityLabel = NSLocalizedString("deactivate", bundle: bundle, comment: "")
            //Create new route with selected point
            let route = Course(id: "", name: "\(NSLocalizedString("to_a_waypoint", bundle: bundle, comment: "")): \(point.name)", points: [anyPoint])
            NavigationManager.shared.updateNavView = true
            NavigationManager.shared.setCurrentRoute(route: route, isSaved: false)
            
            self.dismiss(animated: true, completion: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalEnablePoint"), object: nil)
            })
        }
    }
    
    @IBAction func deletePoint(_ sender: Any) {
        guard let point = currentPoint else {
            return
        }
        // Desactivate route when the point is used in the current route
        if let route = NavigationManager.shared.route.value, let anyPoint = point.anyPoint(), route.points.contains(anyPoint) {
            let message = Utils.localizedString(forKey: "warning_delete_used_waypoint", app: "SARANav")
            let alertController = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: message, preferredStyle: .alert)
            let yesAction = UIAlertAction(title: NSLocalizedString("yes", bundle: bundle, comment: ""), style: .default) { _ in
                var currentPointIndex = NavigationManager.shared.currentPointIndex.value
                if currentPointIndex != nil
                    && point == NavigationManager.shared.point.value
                    && route.points.count > 1 { // Get previous index of a point with coordinates
                    currentPointIndex = NavigationManager.shared.getPreviousIndexWithCoord()
                }
                NavigationManager.shared.route.accept(nil)
                point.deletePoint()
                if self.indexInRoute != nil { // Remove point in view Model
                    self.viewModel?.removeFromRoute(at: IndexPath(row: self.indexInRoute!, section: 0))
                }
                //If there is no point in the route: stop
                if currentPointIndex == nil || route.points.isEmpty || !Course.all().contains(route){
                    self.dismiss(animated: true, completion: {
                          NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalCreatePointIsDimissed"), object: nil)
                    })
                    return
                }
                // Otherwise set the route
                NavigationManager.shared.setCurrentRoute(route: route)
                NavigationManager.shared.currentPointIndex.accept(currentPointIndex)
                self.dismiss(animated: true, completion: {
                      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalCreatePointIsDimissed"), object: nil)
                })
            }
            let noAction = UIAlertAction(title: NSLocalizedString("no", bundle: bundle, comment: ""), style: .cancel, handler: nil)
            alertController.addAction(noAction)
            alertController.addAction(yesAction)
            present(alertController, animated: true, completion: nil)
        } else {
            // Check if the point is used in at leat one route. In this case, different message.
//            let message = point.inARoute() ? String(format: NSLocalizedString("warning_delete_point_route", bundle: bundle, comment: ""), point.name) : NSLocalizedString("warning_delete_waypoint", bundle: bundle, comment: "")
            let message = point.inARoute() ? String(format: Utils.localizedString(forKey: "warning_delete_waypoint_route", app: "SARANav"), point.name) : NSLocalizedString("warning_delete_waypoint", bundle: bundle, comment: "")
            let alertController = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: message, preferredStyle: .alert)
            let yesAction = UIAlertAction(title: NSLocalizedString("yes", bundle: bundle, comment: ""), style: .default) { _ in
                point.deletePoint()
                if self.indexInRoute != nil { // Remove point in view Model
                    self.viewModel?.removeFromRoute(at: IndexPath(row: self.indexInRoute!, section: 0))
                }
                self.dismiss(animated: true, completion: {
                      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalCreatePointIsDimissed"), object: nil)
                })
            }
            let noAction = UIAlertAction(title: NSLocalizedString("no", bundle: bundle, comment: ""), style: .cancel, handler: nil)
            alertController.addAction(noAction)
            alertController.addAction(yesAction)
            present(alertController, animated: true, completion: nil)
            
        }
    }
        
    // MARK: - CoordinatesViewDelegate
    public func openSelectOnMap(tagView: Int?) {
        let storyboard = UIStoryboard(name: "Route", bundle: bundle)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "SelectOnMapVC") as? SelectPositionOnMapViewController else {
                return
        }
        if let (latitude, longitude) = self.waypointView.coordinatesView.getCoordinates() {
            let annotation = MapAnnotation(latitude: Double(latitude), longitude: Double(longitude), type: .coursePoint, isCurrent: true)
            controller.annotation = annotation
        } else {
            let annotation = MapAnnotation(latitude: Double.nan, longitude: Double.nan, type: .coursePoint, isCurrent: true)
            controller.annotation = annotation
        }
        
        controller.radius = Double(self.waypointView.sliderView.settingsValue.value)
        
        self.present(controller, animated: true, completion: nil)
    }
    
    public func useCurrentPosition(tagView: Int?, gpsPosition: GPSPosition) {
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when) {
            if let name = self.nameTxtField.text, name.isNotEmpty {
                UIAccessibility.post(notification: UIAccessibility.Notification.announcement, argument: NSLocalizedString("coordinates_used", bundle: self.bundle, comment: "") + name)
            } else {
                UIAccessibility.post(notification: UIAccessibility.Notification.announcement, argument: NSLocalizedString("coordinates_used_for_this_point", bundle: self.bundle, comment: ""))
            }
        }
    }
    
    // MARK: - TextField Delegate
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.nameTxtField.accessibilityLabel = NSLocalizedString("name", bundle: bundle, comment: "")
        UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: textField)
        return false
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let specialCharacters = "!@#$%&*()+{}[]|\"<>,.~/:;?-=\\¥£•¢"
        let char = CharacterSet(charactersIn: specialCharacters)
        if let range = string.rangeOfCharacter(from: char) {
            let alert = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: NSLocalizedString("firebase_name_special_char", bundle: bundle, comment: ""), preferredStyle: .alert)
            let okAction = UIAlertAction(title: NSLocalizedString("ok", bundle: bundle, comment: ""), style: .cancel, handler: { _ in
                textField.text = (textField.text ?? "") + string.dropLast()
            })
            alert.addAction(okAction)
            self.present(alert, animated: false)
            return false
        }
        return true
    }
    
    // MARK: - Keyboard
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        else {
            // if keyboard size is not available for some reason, dont do anything
            return
        }
        
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height , right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        
        // reset back the content inset to zero after keyboard is gone
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
}
