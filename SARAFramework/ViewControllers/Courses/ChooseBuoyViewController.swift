//
//  ChooseBuoyViewController.swift
//  SARAFramework
//
//  Created by Marine on 20.04.21.
//

import UIKit
import RxSwift
import RxCocoa

public class ChooseBuoyViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, SelectButtonCellDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var okButton: UIBarButtonItem!
    @IBOutlet weak var noTabBarConstraint: NSLayoutConstraint!
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: ChooseBuoyViewController.classForCoder())
    public var buoys: [Buoy]?
    var selectedIndex = BehaviorRelay<IndexPath?>(value: nil)
    
    public override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let _ = self.parent as? AddPointSegmentedViewController {
            self.navigationBar.isHidden = true
            self.noTabBarConstraint.isActive = true
        } else {
            self.noTabBarConstraint.isActive = false
            self.navigationBar.topItem?.title = Utils.localizedString(forKey: "choose_buoy", app: "SARARegate")
            self.cancelButton.title = NSLocalizedString("cancel", bundle: bundle, comment: "")
            self.okButton.title = NSLocalizedString("ok", bundle: bundle, comment: "")
        }
        
        self.tableView.customize()
        self.tableView.register(UINib(nibName: "HeaderTableView", bundle: Bundle(for: ChooseBuoyViewController.classForCoder())), forHeaderFooterViewReuseIdentifier: "HeaderTableView")
        self.tableView?.register(UINib(nibName: "SelectButtonCell", bundle: Bundle(for: ChooseBuoyViewController.classForCoder())), forCellReuseIdentifier: "chooseBuoyCell")
        if #available(iOS 15, *) { // To remove gap between header and table view
            tableView.sectionHeaderTopPadding = 0
        }
        
        selectedIndex.subscribe(onNext: { index in
            if let parentVC = self.parent as? AddPointSegmentedViewController {
                parentVC.validateButton.isEnabled = index != nil
            } else {
                self.okButton.isEnabled = index != nil
            }
          })
    }
    
    // MARK: - UITableView
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return buoys?.count ?? 0
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "chooseBuoyCell") as? SelectButtonCell else {
            return UITableViewCell()
        }
        cell.delegate = self
        cell.titleLabel?.text = buoys![indexPath.row].name
        cell.idElementSelected = String(indexPath.row)
        cell.selectionStyle = .none
        let bundle = Utils.bundle(anyClass: self.classForCoder)
        if self.selectedIndex.value == indexPath {
            cell.selectButton?.setImage(UIImage(named: "globe", in: bundle, compatibleWith: nil), for: .normal)
        } else {
            cell.selectButton?.setImage(UIImage(named: "circle", in: bundle, compatibleWith: nil), for: .normal)
        }
        cell.selectButton?.tintColor = Utils.tintIcon
        cell.selectButton?.isAccessibilityElement = false
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedIndex.accept(indexPath)
        let bundle = Utils.bundle(anyClass: self.classForCoder)
        if let cell = tableView.cellForRow(at: indexPath) as? SelectButtonCell  {
            cell.selectButton?.setImage(UIImage(named: "globe", in: bundle, compatibleWith: nil), for: .normal)
        }
    }
    
    public func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        
        if let indexPathForSelectedRow = self.selectedIndex.value {
            // Deselect current selected row
            tableView.deselectRow(at: indexPathForSelectedRow, animated: false)
            if let cell = tableView.cellForRow(at: indexPathForSelectedRow) as? SelectButtonCell {
                cell.selectButton?.setImage(UIImage(named: "circle", in: Utils.bundle(anyClass: self.classForCoder), compatibleWith: nil), for: .normal)
            }
            
            // If we're on the same row, disable the validation
            if indexPathForSelectedRow == indexPath {
                self.selectedIndex.accept(nil)
                return nil
            }
        }
        return indexPath
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderTableView" ) as! HeaderTableView
        returnedView.customizeHeader(titleName: Utils.localizedString(forKey: "buoys_list", app: "SARARegate"))
        return returnedView
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    // MARK: - IBActions
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func okAction(_ sender: Any) {
        if let selectedRow = self.selectedIndex.value?.row {
            let buoy = buoys![selectedRow]
//            let when = DispatchTime.now() + 3
//            DispatchQueue.main.asyncAfter(deadline: when) {
//                UIAccessibility.post(notification: UIAccessibility.Notification.announcement, argument: String(format: NSLocalizedString("add_point_success", bundle: self.bundle, comment: ""), buoy.name))
//            }
            self.dismiss(animated: true, completion: {
                  NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalChooseBuoyIsDimissed"), object: nil, userInfo: ["buoy" : buoy])
            })
        }
    }
    
    // MARK: - SelectButtonCellDelegate
    public func didSelectCell(id: String) {
        guard let idInt = Int(id) else {
            return
        }
        let indexPathSelected = IndexPath(item: idInt, section: 0)
        var indexToReload: [IndexPath]  = []
        indexToReload.append(indexPathSelected)
        if self.selectedIndex.value == indexPathSelected { // We are on the same row disable it
            self.selectedIndex.accept(nil)
        } else {
            if let oldSelectedIndex = self.selectedIndex.value {
                indexToReload.append(oldSelectedIndex)
            }
            self.selectedIndex.accept(indexPathSelected)
        }
        self.tableView.reloadRows(at: indexToReload, with: .none)
    }
}
