//
//  SliderSetting.swift
//  SARA Croisiere
//
//  Created by Rose on 07/12/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import RealmSwift

public class SliderSettingView: UIView {
    
    // MARK: - IBOutlets
    @IBOutlet weak private var titleLabel: UILabel?
    @IBOutlet weak private var valueLabel: UILabel?
    @IBOutlet weak private var slider: UIAccessibilitySlider!
    @IBOutlet weak var decreaseButton: UIButton!
    @IBOutlet weak var increaseButton: UIButton!
    
    // Variables
    let bundle = Utils.bundle(anyClass: SliderSettingView.classForCoder())
    private var settingUnit: NavigationUnit?
    private var settingTitle: String?
    
    let disposeBag = DisposeBag()
    var infos: AveragesInfos?
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.borderWidth = 1
        layer.borderColor = Utils.secondaryColor.cgColor
        layer.cornerRadius = 5
        
        slider.tintColor = Utils.primaryColor
        valueLabel?.isAccessibilityElement = false
        
        decreaseButton.customizeSliderButton()
        decreaseButton.accessibilityLabel = NSLocalizedString("minus", bundle: bundle, comment: "")
        
        increaseButton.customizeSliderButton()
        increaseButton.accessibilityLabel = NSLocalizedString("plus", bundle: bundle, comment: "")
    }
 
    private var settingsValue: Float = 0 {
        didSet {
            self.updateSetting(value: settingsValue)
            let labelValue = (self.infos?.isContinuous ?? false) ? "\(settingsValue)" : "\(Int(settingsValue))"
            var abbValue = ""
            var unitValue = ""
            if let unit = self.settingUnit {
                abbValue = " \(unit.abbreviation)"
                unitValue = " \(unit.description)"
            }
            self.slider.accessibilityValue = labelValue +  unitValue
            self.valueLabel?.text = labelValue + abbValue
            guard let infos = self.infos else {
                return
            }
            self.titleLabel?.accessibilityLabel = "\(infos.title) \(labelValue) \(abbValue)"
        }
    }
    
    @IBAction func sliderValueDidChange(_ sender: Any) {
        self.settingsValue = slider.value
    }
    
    @IBAction func buttonSettingValueUpdate(_ sender: UIButton) {
        if sender == decreaseButton {
            if self.settingsValue > slider.minimumValue {
                self.settingsValue -= 1
                self.slider.value = self.settingsValue
            }
        }
        if sender == increaseButton {
            if self.settingsValue < slider.maximumValue {
                self.settingsValue += 1
                self.slider.value = self.settingsValue
            }
        }
    }
    
    func updateSetting(value: Float) {
        let realm = try! Realm()
        try! realm.write {
            guard let type = self.infos?.type else {
                return
            }
            switch type {
            case .course:
                Settings.shared.averageHeadingTime = Int(value)
            case .speed:
                Settings.shared.averageSpeedTime = Int(value)
            case .trace:
                Settings.shared.traceSensitivity = Int(value)
            }
        }
    }
    
    func customize(with averageInfos: AveragesInfos) {
        self.settingTitle = averageInfos.title
        self.settingUnit = averageInfos.unit
        
        self.titleLabel?.text = averageInfos.title
        var value = 0
        switch averageInfos.type {
        case .course:
            value = Settings.shared.averageHeadingTime
        case .speed:
            value = Settings.shared.averageSpeedTime
        case .trace:
            value = Settings.shared.traceSensitivity
        }
        self.settingsValue = Float(value)
        self.titleLabel?.accessibilityLabel = "\(averageInfos.title) \(value)\(averageInfos.unit?.description ?? "")"
        
        self.slider.minimumValue = averageInfos.minValue
        self.slider.maximumValue = averageInfos.maxValue
        self.slider.value = Float(value)

        self.infos = averageInfos
    }
    
}
