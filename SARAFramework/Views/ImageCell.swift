//
//  ImageCell.swift
//  SARA Croisiere
//
//  Created by Rose on 16/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import UIKit

public class ImageCell: UITableViewCell {
    
    @IBOutlet private weak var label: UILabel!
    @IBOutlet private weak var iconView: UIImageView!
    
    public func setTitle(title: String) {
        self.label.text = title
    }
    
    public func setImage(image: UIImage?) {
        self.iconView.image = image
        self.iconView.tintColor = Utils.tintIcon
    }
}
