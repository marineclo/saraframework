//
//  TimerView.swift
//  SARAFramework
//
//  Created by Marine on 14.04.21.
//

import UIKit
import RxSwift

public class TimerView: UIView {

    // MARK: - UIOutlets
    @IBOutlet var timerImageView: UIImageView!
    @IBOutlet var timerLabel: UILabel!
    @IBOutlet public var plusButton: UIButton!
    @IBOutlet public var minusButton: UIButton!
    
    // MARK: - Variables
    var contentView: UIView!
    
    // MARK: -
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    /// Set up xib
    func xibSetup() {
        contentView = loadViewFromNib()
        super.backgroundColor = nil
        contentView.backgroundColor = #colorLiteral(red: 0.3215686275, green: 0.6470588235, blue: 0.4862745098, alpha: 1)
        
        self.timerLabel.textColor = .white
        
        let bundle = Utils.bundle(anyClass: TimerView.classForCoder())
        self.timerImageView.image = UIImage(named: "timer", in: bundle, compatibleWith: nil)
        self.timerImageView.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        self.plusButton.setImage(UIImage(named: "plus", in: bundle, compatibleWith: nil), for: .normal)
        self.plusButton.tintColor = .white
        self.plusButton.accessibilityLabel = NSLocalizedString("increase_timer", bundle: bundle, comment: "")
        self.minusButton.setImage(UIImage(named: "minus", in: bundle, compatibleWith: nil), for: .normal)
        self.minusButton.tintColor = .white
        self.minusButton.accessibilityLabel = NSLocalizedString("decrease_timer", bundle: bundle, comment: "")
        
        // use bounds not frame or it'll be offset
        contentView.frame = bounds
        
        // Make the view stretch with containing view
        contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(contentView)
    }
    
    /// Load desired view from nib
    ///
    /// - Returns: UIView
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return view
    }
    
    public func setTimer(value: Int) {
        let seconds: Int = value % 60
        let minutes: Int = (value / 60) % 60
        timerLabel.text = String(format: "%02d : %02d", minutes, seconds)
        let bundle = Utils.bundle(anyClass: TimerView.classForCoder())
        // Accessibility
        if seconds == 0 {
            self.timerLabel.accessibilityLabel = "\(NSLocalizedString("timer", bundle: bundle, comment: "")) \(minutes) \(NSLocalizedString("unit_minutes", bundle: bundle, comment: ""))"
        } else {
            self.timerLabel.accessibilityLabel = "\(NSLocalizedString("timer", bundle: bundle, comment: "")) \(minutes) \(NSLocalizedString("unit_minutes", bundle: bundle, comment: "")) \(seconds)"
        }
    }
}
