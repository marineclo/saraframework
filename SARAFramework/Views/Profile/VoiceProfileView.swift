//
//  VoiceProfile.swift
//  SARA Croisiere
//
//  Created by Marine IL on 15.11.18.
//  Copyright © 2018 GraniteApps. All rights reserved.
//

import Foundation
import UIKit

public class VoiceProfileView: UIView {
    
    // MARK: - IBOutlets
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var voiceSegmentedControl: UISegmentedControl!
    @IBOutlet var vocalRateSliderView: CustomSliderView!
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: VoiceProfileView.classForCoder())
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.borderWidth = 1
        layer.borderColor = Utils.secondaryColor.cgColor
        layer.cornerRadius = 5
        
        title.text = NSLocalizedString("voice", bundle: bundle, comment: "")
        title.accessibilityLabel = title.text
        title.accessibilityTraits = UIAccessibilityTraits.header
        
        voiceSegmentedControl.setTitle(NSLocalizedString("male", bundle: bundle, comment: ""), forSegmentAt: 0)
        voiceSegmentedControl.setTitle(NSLocalizedString("female", bundle: bundle, comment: ""), forSegmentAt: 1)
        voiceSegmentedControl.customize()
    }
}
