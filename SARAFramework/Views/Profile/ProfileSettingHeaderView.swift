//
//  ProfileSettingHeaderView.swift
//  SARA Croisiere
//
//  Created by Marine IL on 19.02.19.
//  Copyright © 2019 Rose. All rights reserved.
//

import UIKit

class ProfileSettingHeaderView: UIView {

    @IBOutlet weak var title: UILabel!
    
    override func awakeFromNib() {
        self.backgroundColor = Utils.primaryColor
        self.title.textColor = Utils.reverseColor
    }
    
    public func customizeCell(parameter: String) {
        self.title.text = parameter
        self.title.accessibilityTraits = UIAccessibilityTraits.header
    }
}
