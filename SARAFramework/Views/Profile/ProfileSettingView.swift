//
//  ProfileSetting2View.swift
//  SARAFramework
//
//  Created by Marine on 09.09.21.
//  Copyright © 2021 GraniteApps. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

protocol ProfileSettingDelegate: class {
    func shouldResizeContentView()
}

class ProfileSettingView: UIView {
    
    // MARK: - IBOutlets
    @IBOutlet var title: UILabel!
    @IBOutlet var settingSwitch: UISwitch!
    @IBOutlet var viewSetting: UIView!
    @IBOutlet var thresholdSliderView: CustomSliderView!
    @IBOutlet var timeSliderView: CustomSliderView!
    
    @IBOutlet var settingViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var thresholdViewConstraint: NSLayoutConstraint!
    @IBOutlet var switchConstraint: NSLayoutConstraint!
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: ProfileSettingView.classForCoder())
    var settingVariable = BehaviorRelay<ProfileSetting?>(value: nil)
    let disposeBag = DisposeBag()
    weak var delegate: ProfileSettingDelegate?
    var thresholdUnit: NavigationUnit?
    var heightViewSetting: Int = 162
    let heightViewCustomSlider: Int = 81
    var heightToRemove: Int = 0
    
    // MARK: -
    public override func awakeFromNib() {
        self.layer.borderWidth = 1
        self.layer.borderColor = Utils.secondaryColor.cgColor
        self.layer.cornerRadius = 5
        
        self.title.accessibilityTraits = UIAccessibilityTraits.header
        
        // Color settings
        settingSwitch.tintColor = Utils.secondaryColor
        settingSwitch.onTintColor = Utils.primaryColor
        
        settingViewHeightConstraint.constant = 0
        viewSetting.isHidden = true
        
        Observable.combineLatest(thresholdSliderView.settingsValue.distinctUntilChanged(), timeSliderView.settingsValue.distinctUntilChanged(), settingSwitch.rx.value) { [weak self] (value, time, isActivated) -> ProfileSetting? in
            guard let weakSelf = self, let unit = weakSelf.thresholdUnit else { return nil }
            return ProfileSetting(isActivated: isActivated, threshold: weakSelf.transformToDefault(value: value, from: unit), timeThreshold: Int(time))
        }.bind(to: settingVariable)
        .disposed(by: disposeBag)
    }
    
    // MARK: - IBActions
    @IBAction func toggleSwitch(_ sender: UISwitch) {
        if sender.isOn {
            settingViewHeightConstraint.constant = CGFloat(heightViewSetting - heightToRemove)
            viewSetting.isHidden = false
        } else {
            settingViewHeightConstraint.constant = 0
            viewSetting.isHidden = true
        }
        self.settingSwitch.accessibilityLabel = title.text ?? ""
        self.title.accessibilityLabel = (self.title.text ?? "") + ", " + (sender.isOn ? NSLocalizedString("activated_parameter", bundle: bundle, comment: "") : NSLocalizedString("deactivated_parameter", bundle: bundle, comment: ""))
        
        delegate?.shouldResizeContentView()
        UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: nil)
    }
    
    // MARK: -
    public func setView(parameter: AnnounceParameter){
        let title = parameter.name
        let rightUnitSettingValue = transform(value: parameter.settingVariable.value, from: parameter.unit)
        self.settingVariable.accept(rightUnitSettingValue)
        
        self.title.text = title
        self.title.accessibilityLabel = title + ", " + (rightUnitSettingValue.isActivated ? NSLocalizedString("activated_parameter", bundle: bundle, comment: "") : NSLocalizedString("deactivated_parameter", bundle: bundle, comment: ""))
        
        self.settingSwitch.isOn = parameter.settingVariable.value.isActivated
        self.settingSwitch.sendActions(for: UIControl.Event.valueChanged)
        self.settingSwitch.accessibilityLabel = title
        if !parameter.shouldDisplaySwitch {
            self.switchConstraint.isActive = false
            self.settingSwitch.isHidden = true
            self.title.accessibilityLabel = title
        }
                
        self.thresholdUnit = parameter.unit
        if parameter.valueDisplayed {
            if parameter.valueDisabled {
                thresholdSliderView.sliderLabel.isHidden = false
                thresholdSliderView.sliderLabel.text = NSLocalizedString("no_announce_slider", bundle: bundle, comment: "")
                thresholdSliderView.slider.isHidden = true
                thresholdSliderView.decreaseButton.isHidden = true
                thresholdSliderView.increaseButton.isHidden = true
                thresholdSliderView.valueLabel.isHidden = true
                thresholdSliderView.titleLabel.text = parameter.valueTitle
            } else {
                let minValue = transform(thresholdLimit: parameter.minValue, from: parameter.unit)
                let maxValue = transform(thresholdLimit: parameter.maxValue, from: parameter.unit)
                thresholdSliderView.customize(with: SliderInfos(title: parameter.valueTitle, unit: parameter.unit, value: rightUnitSettingValue.threshold, minValue: minValue, maxValue: maxValue, step: parameter.step, isContinuous: !parameter.shouldDisplayInt))
            }
        } else {
            thresholdSliderView.isHidden = true
            heightToRemove = heightToRemove + heightViewCustomSlider
            thresholdViewConstraint.isActive = false
        }
        
        
        if parameter.timeDisplayed {
            timeSliderView.customize(with: SliderInfos(title: NSLocalizedString("time_threshold", bundle: bundle, comment: ""), unit: .second, value: Float(rightUnitSettingValue.timeThreshold), minValue: 1, maxValue: 60, step: 1, isContinuous: false))
        } else {
            timeSliderView.isHidden = true
            heightToRemove = heightToRemove + heightViewCustomSlider
            thresholdViewConstraint.isActive = true
        }
        if self.settingSwitch.isOn {
            settingViewHeightConstraint.constant = CGFloat(heightViewSetting - heightToRemove)
            viewSetting.isHidden = false
        } else {
            settingViewHeightConstraint.constant = 0
            viewSetting.isHidden = true
        }
    }
    
    func transform(value: ProfileSetting, from unit: NavigationUnit) -> ProfileSetting {
        switch unit {
        case .mile:
            return ProfileSetting(isActivated: value.isActivated, threshold: value.threshold.meterToMile(), timeThreshold: value.timeThreshold)
        case .kmPerHour:
            return ProfileSetting(isActivated: value.isActivated, threshold: value.threshold.knotsToKmh(), timeThreshold: value.timeThreshold)
        default:
            return value
        }
    }
    
    func transform(thresholdLimit: Float, from unit: NavigationUnit) -> Float {
        switch unit {
        case .mile:
            return thresholdLimit.meterToMile()
        case .kmPerHour:
            return Float(thresholdLimit.knotsToKmh().rounded(.up))
        default:
            return thresholdLimit.formatted
        }
    }
    
    func transformToDefault(value: Float, from unit: NavigationUnit) -> Float {
        switch unit {
        case .mile:
            return value.mileToMeter()
        case .kmPerHour:
            return value.kmhToKnots()
        default:
            return value
        }
    }
}
