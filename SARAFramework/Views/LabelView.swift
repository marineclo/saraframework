//
//  LabelView.swift
//  SARAFramework
//
//  Created by Marine on 21.07.22.
//

import UIKit

class LabelView: UIView {

    // MARK: - Outlets
    @IBOutlet var label: UILabel!
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: LabelView.classForCoder())
    
    // MARK: -
    public override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func setLabel(text: String) {
        self.label.text = text
    }
}
