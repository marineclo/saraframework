//
//  ImageLabel.swift
//  SARA Croisiere
//
//  Created by Rose on 24/07/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

/// Types of quickInfos needed at the top of the navigation controller
///
/// - course: Course Over Ground
/// - speed: Speed
/// - accuracy: GPS Accuracy
public enum QuickInfoType {
    case course, speed, accuracy, compass
}

public class ImageLabel: UIView {
    
    // MARK: - IBOutlets
    @IBOutlet private weak var icon: UIImageView!
    @IBOutlet private weak var label: UILabel!
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: ImageLabel.classForCoder())
    var contentView: UIView!
    let disposeBag = DisposeBag()
    public var value = BehaviorRelay<String>(value: "0")
    private var quickInfoTypeLabel: String = ""
    public var quickInfoUnit: NavigationUnit?
    
    public var quickInfoType: QuickInfoType? {
        didSet {
            // Set icon + type description
            if let safeType = quickInfoType {
                switch safeType {
                case .course:
                    icon.image = UIImage(named: "COG", in: bundle, compatibleWith: nil)
                    icon.tintColor = Utils.secondaryColor
                    quickInfoTypeLabel = NSLocalizedString("course_over_ground", bundle: bundle, comment: "")
                    quickInfoUnit = .degrees
                case .speed:
                    icon.image = UIImage(named: "speedometer", in: bundle, compatibleWith: nil)
                    icon.tintColor = Utils.secondaryColor
                    quickInfoTypeLabel = NSLocalizedString("speed_over_ground", bundle: bundle, comment: "")
                    quickInfoUnit = .knots
                case .accuracy:
                    icon.image = UIImage(named: "radar", in: bundle, compatibleWith: nil)
                    quickInfoTypeLabel = NSLocalizedString("gps_precision", bundle: bundle, comment: "")
                    quickInfoUnit = .meter
                case .compass:
                    icon.image = UIImage(named: "compass new", in: bundle, compatibleWith: nil)
                    icon.tintColor = Utils.secondaryColor
                    quickInfoTypeLabel = NSLocalizedString("compass", bundle: bundle, comment: "")
                    quickInfoUnit = .degrees
                
                }
            }
            //Init accessibility label
            accessibilityLabel = quickInfoTypeLabel + ", " + value.value + " " + (quickInfoUnit?.description ?? "")
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
        bindings()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
        bindings()
    }
    
    func bindings() {
        label.isAccessibilityElement = false
        self.isAccessibilityElement = true
        
        //Bind value of information to the label
        value
            .asObservable()
            .map({ [weak self] (value) -> String in
                guard let unit = self?.quickInfoUnit?.abbreviation else {
                    return value
                }
                if self?.quickInfoType == .speed && Settings.shared.speedUnit?.unit != self?.quickInfoUnit {
                    self?.quickInfoUnit = Settings.shared.speedUnit?.unit
                    return "\(value) \(Settings.shared.speedUnit?.unit.abbreviation ?? "")"
                }
                return "\(value) \(unit)"
            })
            .bind(to: label.rx.text)
            .disposed(by: disposeBag)
        
        // Update accessibility label each time the value changes
        value.asObservable()
            .subscribe(onNext: { [weak self] _ in
                guard let weakSelf = self else { return }
                if let label = self?.quickInfoTypeLabel,
                    let quickValue = self?.label.text,
                    let unit = self?.quickInfoUnit {
                    if quickValue.contains(NSLocalizedString("unavailable_nav_symbol", bundle: weakSelf.bundle, comment: ""))  {
                        self?.accessibilityLabel = label + ", " + NSLocalizedString("unavailable_nav", bundle: weakSelf.bundle, comment: "")
                    } else {
                        self?.accessibilityLabel = label + ", " + quickValue.replacingOccurrences(of: unit.abbreviation, with: unit.description)
                    }
                }
            })
            .disposed(by: disposeBag)
    }
    
    /// Set up xib
    func xibSetup() {
        contentView = loadViewFromNib()
        super.backgroundColor = nil
        
        // Rounded corners
        contentView.layer.cornerRadius = 10
        contentView.layer.masksToBounds = true
        
        // Border
        contentView.layer.borderColor = Utils.reverseColor.cgColor
        contentView.layer.borderWidth = 2
        
        self.label.textColor = Utils.reverseColor
        
        // use bounds not frame or it'll be offset
        contentView.frame = bounds
        
        // Make the view stretch with containing view
        contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(contentView)
    }
    
    /// Load desired view from nib
    ///
    /// - Returns: UIView
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return view
    }
}
