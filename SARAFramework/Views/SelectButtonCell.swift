//
//  SelectButtonCell.swift
//  SARA Croisiere
//
//  Created by Rose on 12/11/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import UIKit

public protocol SelectButtonCellDelegate: class {
    func didSelectCell(id: String)
}

public class SelectButtonCell: UITableViewCell {
    @IBOutlet public weak var selectButton: UIButton?
    @IBOutlet public weak var titleLabel: UILabel?
    @IBOutlet public weak var inverseImageView: UIImageView!
    @IBOutlet public weak var inversedImageConstraint: NSLayoutConstraint?
    
    public weak var delegate: SelectButtonCellDelegate?
    
    public var idElementSelected: String?
    
    @IBAction func didSelect() {
        guard let id = idElementSelected else {
            return
        }
        
        delegate?.didSelectCell(id: id)
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        selectButton?.setTitleColor(Utils.primaryColor, for: .normal)
        selectButton?.setTitleColor(Utils.primaryColor, for: .disabled)
    }
    
    public func setTitle(title: String?) {
        self.titleLabel?.text = title
    }
    public func setId(id: String?) {
        self.idElementSelected = id
    }
    public func setInverseImageView(image: UIImage? = nil, accessibilityLabel: String = "") {
        if image != nil {
            inverseImageView.isHidden = false
            inverseImageView.image = image
            inverseImageView.tintColor = Utils.secondaryColor
            inversedImageConstraint?.isActive = false
            titleLabel?.accessibilityLabel = "\(titleLabel?.text ?? ""), \(accessibilityLabel)"
        } else {
            inverseImageView.isHidden = true
            inversedImageConstraint?.isActive = true
            titleLabel?.accessibilityLabel = "\(titleLabel?.text ?? "")"
        }
    }
}
