//
//  CoordinatesView.swift
//  SARAFramework
//
//  Created by Marine on 19.04.21.
//

import UIKit
import RxSwift

public protocol CoordinatesViewDelegate: class {
    func openSelectOnMap(tagView: Int?)
    func useCurrentPosition(tagView: Int?, gpsPosition: GPSPosition)
}

public class CoordinatesView: UIView, UITextFieldDelegate {

    // MARK: - IBOutlets
    @IBOutlet var selectCoordStackView: UIStackView!
    @IBOutlet var useMyPositionButton: UIButton!
    @IBOutlet var selectOnMapButton: UIButton!
    @IBOutlet var latitudeLabel: UILabel!
    @IBOutlet var longitudeLabel: UILabel!
    @IBOutlet var latCoordinateView: UIView!
    @IBOutlet var longCoordinateView: UIView!
    
    // MARK: - IBActions
    @IBAction func useMyPosition(_ sender: Any) {
        if let location = LocationManager.shared.getCurrentPosition() {
            let gpsPosition = GPSPosition(latitude: Float(location.coordinate.latitude), longitude: Float(location.coordinate.longitude))
            fillCoordinates(with: gpsPosition, auto: true)
            delegate?.useCurrentPosition(tagView: self.tag, gpsPosition: gpsPosition)
        }
    }
    
    @IBAction func selectOnMapAction(_ sender: Any) {
        delegate?.openSelectOnMap(tagView: self.tag)
    }
    
    // MARK: - Variables
    var contentView: UIView!
    let bundle = Utils.bundle(anyClass: CoordinatesView.classForCoder())
    var gpsPosition: GPSPosition?
    public var delegate: CoordinatesViewDelegate?
    
    // MARK: -
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    // Set up xib
    func xibSetup() {
        contentView = loadViewFromNib()
        
        // Buttons
        setupButtons()
        // Coordinates
        setupCoordinates()
        // Fill coordinates
        if let gpsPos = gpsPosition {
            fillCoordinates(with: gpsPos)
        }
                
        // use bounds not frame or it'll be offset
        contentView.frame = bounds
        
        // Make the view stretch with containing view
        contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(contentView)
    }
    
    // Load desired view from nib
    //
    // - Returns: UIView
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return view
    }
    
    func setupButtons() {
        useMyPositionButton.setImage(UIImage(named: "userPosition", in: bundle, compatibleWith: nil), for: .normal)
        useMyPositionButton.tintColor = Utils.tintIcon
        useMyPositionButton.accessibilityLabel = NSLocalizedString("use_gps_location", bundle: bundle, comment: "")
        
        selectOnMapButton.setImage(UIImage(named: "map", in: bundle, compatibleWith: nil), for: .normal)
        selectOnMapButton.tintColor = Utils.tintIcon
        selectOnMapButton.accessibilityLabel = NSLocalizedString("select_on_map", bundle: bundle, comment: "")
        
        self.accessibilityElements = [self.selectCoordStackView]
    }
    
    func setupCoordinates() {
        var accessibleElementsOrder: [Any] = []
        self.latitudeLabel.text = NSLocalizedString("latitude", bundle: bundle, comment: "")
        self.longitudeLabel.text = NSLocalizedString("longitude", bundle: bundle, comment: "")
        var latView: UIView?
        var longView: UIView?
        if Settings.shared.coordinatesUnit?.unit == .minuteDecimalDegrees {
            latView = DMCoordinateView()
            longView = DMCoordinateView()
            
            // unwrap optional
            guard let _latView = latView as? DMCoordinateView, let _longView = longView as? DMCoordinateView else { return }
            _latView.isLatitude = true
            _latView.setupButton()
            _longView.setupButton()
            
            addCoordinateSystemView(parentView: self.latCoordinateView, childView: _latView)
            addCoordinateSystemView(parentView: self.longCoordinateView, childView: _longView)

            accessibleElementsOrder = [self.latitudeLabel]
            accessibleElementsOrder.append(contentsOf: _latView.accessibleElementsOrder())
            accessibleElementsOrder.append(self.longitudeLabel)
            accessibleElementsOrder.append(contentsOf: _longView.accessibleElementsOrder())
        } else if Settings.shared.coordinatesUnit?.unit == .degreesMinuteSecond {
            latView = DMSCoordinateView()
            longView = DMSCoordinateView()
            
            // unwrap optional
            guard let _latView = latView as? DMSCoordinateView, let _longView = longView as? DMSCoordinateView else { return }
            _latView.isLatitude = true
            _latView.setupButton()
            _longView.setupButton()
            
            addCoordinateSystemView(parentView: self.latCoordinateView, childView: _latView)
            addCoordinateSystemView(parentView: self.longCoordinateView, childView: _longView)

            accessibleElementsOrder = [self.latitudeLabel]
            accessibleElementsOrder.append(contentsOf: _latView.accessibleElementsOrder())
            accessibleElementsOrder.append(self.longitudeLabel)
            accessibleElementsOrder.append(contentsOf: _longView.accessibleElementsOrder())
        } else {
            latView = DDCoordinateView()
            longView = DDCoordinateView()
            
            // unwrap optional
            guard let _latView = latView as? DDCoordinateView, let _longView = longView as? DDCoordinateView else { return }
            _latView.isLatitude = true
            _latView.setupButton()
            _longView.setupButton()
            
            addCoordinateSystemView(parentView: self.latCoordinateView, childView: _latView)
            addCoordinateSystemView(parentView: self.longCoordinateView, childView: _longView)

            accessibleElementsOrder = [self.latitudeLabel]
            accessibleElementsOrder.append(contentsOf: _latView.accessibleElementsOrder())
            accessibleElementsOrder.append(self.longitudeLabel)
            accessibleElementsOrder.append(contentsOf: _longView.accessibleElementsOrder())
        }
        self.accessibilityElements?.append(contentsOf: accessibleElementsOrder)
    }
    
    func addCoordinateSystemView(parentView: UIView, childView: UIView) {
        parentView.subviews.forEach { v in
            v.removeFromSuperview()
        }
        // add the new view as a subview of the "holder" view
        parentView.addSubview(childView)
        // we need to give the new view auto-layout properties
        childView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            // constrain it to all 4 sides of the "holder" view
            childView.topAnchor.constraint(equalTo: parentView.topAnchor),
            childView.bottomAnchor.constraint(equalTo: parentView.bottomAnchor),
            childView.centerXAnchor.constraint(equalTo: parentView.centerXAnchor),
        ])
    }
    
    func elements(isUserEnabled: Bool) {
        latitudeLabel.isAccessibilityElement = isUserEnabled
        longitudeLabel.isAccessibilityElement = isUserEnabled
        guard let latView = latCoordinateView.subviews.first, let longView = longCoordinateView.subviews.first else { return }
        switch Settings.shared.coordinatesUnit?.unit {
        case .minuteDecimalDegrees:
            guard let latDMView = latView as? DMCoordinateView, let longDMView = longView as? DMCoordinateView else { return }
            latDMView.elements(isUserEnabled: isUserEnabled)
            longDMView.elements(isUserEnabled: isUserEnabled)
        case .degreesMinuteSecond:
            guard let latDMSView = latView as? DMSCoordinateView, let longDMSView = longView as? DMSCoordinateView else { return }
            latDMSView.elements(isUserEnabled: isUserEnabled)
            longDMSView.elements(isUserEnabled: isUserEnabled)
        case .decimalDegrees:
            guard let latDDView = latView as? DDCoordinateView, let longDDView = longView as? DDCoordinateView else { return }
            latDDView.elements(isUserEnabled: isUserEnabled)
            longDDView.elements(isUserEnabled: isUserEnabled)
        default:
            return
        }

        useMyPositionButton.isAccessibilityElement = isUserEnabled
        useMyPositionButton.isUserInteractionEnabled = isUserEnabled
        selectOnMapButton.isAccessibilityElement = isUserEnabled
        selectOnMapButton.isUserInteractionEnabled = isUserEnabled
        selectCoordStackView.isUserInteractionEnabled = isUserEnabled
        selectCoordStackView.arrangedSubviews.forEach({
            $0.isAccessibilityElement = isUserEnabled
            $0.isUserInteractionEnabled = isUserEnabled
        })
//        if isEnabled {
//            setupCoordinates()
//            setupButtons()
//        }
    }
    
    func elements(isEnabled: Bool) {
        guard let latView = latCoordinateView.subviews.first, let longView = longCoordinateView.subviews.first else { return }
        switch Settings.shared.coordinatesUnit?.unit {
        case .minuteDecimalDegrees:
            guard let latDMView = latView as? DMCoordinateView, let longDMView = longView as? DMCoordinateView else { return }
            latDMView.elements(isEnabled: isEnabled)
            longDMView.elements(isEnabled: isEnabled)
        case .degreesMinuteSecond:
            guard let latDMSView = latView as? DMSCoordinateView, let longDMSView = longView as? DMSCoordinateView else { return }
            latDMSView.elements(isEnabled: isEnabled)
            longDMSView.elements(isEnabled: isEnabled)
        case .decimalDegrees:
            guard let latDDView = latView as? DDCoordinateView, let longDDView = longView as? DDCoordinateView else { return }
            latDDView.elements(isEnabled: isEnabled)
            longDDView.elements(isEnabled: isEnabled)
        default:
            return
        }
        useMyPositionButton.isEnabled = isEnabled
        selectOnMapButton.isEnabled = isEnabled
        selectCoordStackView.arrangedSubviews.forEach({
            ($0 as? UIButton)?.isEnabled = isEnabled
        })
    }
    
    public func observableCoordinates() -> Observable<Bool> {
        guard let latView = latCoordinateView.subviews.first, let longView = longCoordinateView.subviews.first else { return Observable.create { observer in
            return Disposables.create()
        } }
        var observables: [Observable<String>] = []
        switch Settings.shared.coordinatesUnit?.unit {
        case .minuteDecimalDegrees:
            guard let latDMView = latView as? DMCoordinateView, let longDMView = longView as? DMCoordinateView else { return Observable.create { observer in
                return Disposables.create()
            } }
            observables.append(contentsOf: latDMView.observableCoordinates())
            observables.append(contentsOf: longDMView.observableCoordinates())
        case .degreesMinuteSecond:
            guard let latDMSView = latView as? DMSCoordinateView, let longDMSView = longView as? DMSCoordinateView else { return Observable.create { observer in
                return Disposables.create()
            } }
            observables.append(contentsOf: latDMSView.observableCoordinates())
            observables.append(contentsOf: longDMSView.observableCoordinates())
        default:
            guard let latDDView = latView as? DDCoordinateView, let longDDView = longView as? DDCoordinateView else { return Observable.create { observer in
                return Disposables.create()
            } }
            observables.append(contentsOf: latDDView.observableCoordinates())
            observables.append(contentsOf: longDDView.observableCoordinates())
        }
        return Observable.combineLatest(observables) { _ in
            return self.getCoordinates() != nil
        }
    }
    
    public func setButtonOtherMethod(button: UIButton) {
        self.selectCoordStackView.addArrangedSubview(button)
        NSLayoutConstraint.activate([
            button.widthAnchor.constraint(equalTo: selectOnMapButton.widthAnchor),
            button.heightAnchor.constraint(equalTo: button.widthAnchor)
        ])
    }
    
    public func fillCoordinates(with gpsPosition: GPSPosition, auto: Bool = false) {
        guard let latView = latCoordinateView.subviews.first, let longView = longCoordinateView.subviews.first else { return }
        let (latZone, latZoneInitial) = getLatitudeZone(r: gpsPosition.latitude > 0)
        let (longZone, longZoneInitial) = getLongitudeZone(r: gpsPosition.longitude > 0)
        switch (Settings.shared.coordinatesUnit?.unit) {
        case .decimalDegrees:
            guard let latDDView = latView as? DDCoordinateView, let longDDView = longView as? DDCoordinateView else { return }
            latDDView.fillCoordinates(with: gpsPosition.latitude, zone: (latZone, latZoneInitial), auto: auto)
            longDDView.fillCoordinates(with: gpsPosition.longitude, zone: (longZone, longZoneInitial), auto: auto)
        case .minuteDecimalDegrees:
            guard let latDMView = latView as? DMCoordinateView, let longDMView = longView as? DMCoordinateView else { return }
            latDMView.fillCoordinates(with: gpsPosition.latitude.coordinates, zone: (latZone, latZoneInitial), auto: auto)
            longDMView.fillCoordinates(with: gpsPosition.longitude.coordinates, zone: (longZone, longZoneInitial), auto: auto)
        case .degreesMinuteSecond:
            guard let latView = latCoordinateView.subviews.first, let latDMSView = latView as? DMSCoordinateView, let longView = longCoordinateView.subviews.first, let longDMSView = longView as? DMSCoordinateView else { return }
            latDMSView.fillCoordinates(with: gpsPosition.latitude.coordinatesDMS, zone: (latZone, latZoneInitial), auto: auto)
            longDMSView.fillCoordinates(with: gpsPosition.longitude.coordinatesDMS, zone: (longZone, longZoneInitial), auto: auto)
        default:
            return
        }
    }
        
    func getLatitudeZone(r: Bool) -> (String, String) {
        let latZone = r ? NSLocalizedString("north", bundle: bundle, comment: "") : NSLocalizedString("south", bundle: bundle, comment: "")
        let latZoneInitial = r ? NSLocalizedString("north_initial", bundle: bundle, comment: "") : NSLocalizedString("south_initial", bundle: bundle, comment: "")
        return (latZone, latZoneInitial)
    }
    
    func getLongitudeZone(r: Bool) -> (String, String) {
        let longZone = r ? NSLocalizedString("east", bundle: bundle, comment: "") : NSLocalizedString("west", bundle: bundle, comment: "")
        let longZoneInitial = r ? NSLocalizedString("east_initial", bundle: bundle, comment: "") : NSLocalizedString("west_initial", bundle: bundle, comment: "")
        return (longZone, longZoneInitial)
    }
    
    public func getCoordinates() -> (Float, Float)? {
        guard let latView = latCoordinateView.subviews.first, let longView = longCoordinateView.subviews.first  else { return nil }
        if Settings.shared.coordinatesUnit?.unit == .minuteDecimalDegrees {
            guard let latDMView = latView as? DMCoordinateView, let longDMView = longView as? DMCoordinateView, let latitude = latDMView.getCoordinate(), let longitude = longDMView.getCoordinate() else { return nil }
            return (latitude, longitude)
        } else if Settings.shared.coordinatesUnit?.unit == .degreesMinuteSecond {
            guard let latView = latCoordinateView.subviews.first, let latDMSView = latView as? DMSCoordinateView, let longView = longCoordinateView.subviews.first, let longDMSView = longView as? DMSCoordinateView, let latitude = latDMSView.getCoordinate(), let longitude = longDMSView.getCoordinate() else { return nil }
            return (latitude, longitude)
        } else {
            guard let latDDView = latView as? DDCoordinateView, let longDDView = longView as? DDCoordinateView, let latitude = latDDView.getCoordinate(), let longitude = longDDView.getCoordinate() else { return nil }
            return (latitude, longitude)
        }
    }
}
