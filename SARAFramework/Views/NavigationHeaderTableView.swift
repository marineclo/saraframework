//
//  NavigationHeaderTableView.swift
//  SARA Croisiere
//
//  Created by Marine on 05.05.20.
//  Copyright © 2020 GraniteApps. All rights reserved.
//

import UIKit

class NavigationHeaderTableView: UITableViewHeaderFooterView {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var portLabel: UILabel!
    @IBOutlet weak var starboardLabel: UILabel!
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func customizeHeader() {
        self.contentView.backgroundColor = Utils.headerNavivagtionColor
        self.nameLabel.textColor = Utils.reverseColor
        self.portLabel.textColor = Utils.reverseColor
        self.starboardLabel.textColor = Utils.reverseColor
//        self.nameLabel.font = UIFont.preferredFont(forTextStyle: .title2)
    }
}
