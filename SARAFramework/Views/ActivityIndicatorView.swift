//
//  ActivityIndicatorView.swift
//  SARAFramework
//
//  Created by Marine on 13.07.21.
//

import Foundation

class ActivityIndicatorView: UIView {
 
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var label: UILabel!
    
    // MARK: - Variables
    var contentView: UIView!
    let bundle = Utils.bundle(anyClass: ActivityIndicatorView.classForCoder())
    
    // MARK: -
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    // Set up xib
    func xibSetup() {
        contentView = loadViewFromNib()
        super.backgroundColor = nil
        
        //Setup view
        contentView.backgroundColor = .black
        contentView.alpha = 0.9
        contentView.layer.cornerRadius = 10
        contentView.layer.masksToBounds = true
        contentView.isAccessibilityElement = false
        
        label.textColor = .white
        
        activityIndicator.color = .lightGray
        activityIndicator.isAccessibilityElement = false
        
        // use bounds not frame or it'll be offset
        contentView.frame = bounds
        
        // Make the view stretch with containing view
        contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(contentView)
    }
    
    // Load desired view from nib
    //
    // - Returns: UIView
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return view
    }
    
    func setText(text: String) {
        label.text = text
    }
    
    func startAnimating() {
        UIAccessibility.post(notification: .layoutChanged, argument: label)
        label.becomeFirstResponder()
        activityIndicator.startAnimating()
    }
    
    func stopAnimating() {
        activityIndicator.stopAnimating()
        label.resignFirstResponder()
    }
}
