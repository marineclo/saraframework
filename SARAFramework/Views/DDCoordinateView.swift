//
//  DDCoordinateView.swift
//
//
//  Created by Marine on 17.07.23.
//

import UIKit
import RxSwift

class DDCoordinateView: UIView, UITextFieldDelegate {

    // MARK: - IBOutlets
    @IBOutlet var degreeTextField: UITextField!
    @IBOutlet var degreeLabel: UILabel!
    @IBOutlet var cardinalButton: UIButton!
    
    // MARK: - IBAction
    @IBAction func cardinalButtonAction(_ sender: UIButton) {
        if isLatitude && !self.doNothing{
            let (latZone, latZoneInital) = Utils.getLatitudeZone(r: sender.titleLabel?.text != NSLocalizedString("north_initial", bundle: bundle, comment: ""))
            sender.setTitle(latZoneInital, for: .normal)
            sender.accessibilityLabel = latZone
        } else if !isLatitude && !self.doNothing {
            let (longZone, longZoneInitial) = Utils.getLongitudeZone(r: sender.titleLabel?.text != NSLocalizedString("east_initial", bundle: bundle, comment: ""))
            sender.setTitle(longZoneInitial, for: .normal)
            sender.accessibilityLabel = longZone
        }
    }
    
    // MARK: - Variables
    var contentView: UIView!
    let bundle = Utils.bundle(anyClass: DDCoordinateView.classForCoder())
    var isLatitude = false
    var doNothing: Bool = false
    
    // MARK: -
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    // Set up xib
    func xibSetup() {
        contentView = loadViewFromNib()
        
        setupLabels()
        
        // use bounds not frame or it'll be offset
        contentView.frame = bounds
        
        // Make the view stretch with containing view
        contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(contentView)
    }
    
    // Load desired view from nib
    //
    // - Returns: UIView
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return view
    }
    
    func setupLabels() {
        self.degreeLabel.isAccessibilityElement = false
        self.degreeLabel.text = NSLocalizedString("unit_abb_degrees", bundle: bundle, comment: "")
        self.degreeTextField.accessibilityLabel = NSLocalizedString("unit_abb_degrees", bundle: bundle, comment: "")
    }

    func setupButton() {
        self.cardinalButton.customizeSecondary()
        if isLatitude {
            self.cardinalButton.setTitle(NSLocalizedString("north_initial", bundle: bundle, comment: ""), for: .normal)
            self.cardinalButton.accessibilityLabel = NSLocalizedString("north", bundle: bundle, comment: "")
        } else {
            self.cardinalButton.setTitle(NSLocalizedString("east_initial", bundle: bundle, comment: ""), for: .normal)
            self.cardinalButton.accessibilityLabel = NSLocalizedString("east", bundle: bundle, comment: "")
        }
    }
    
    func accessibleElementsOrder() -> [Any] {
        return [self.degreeTextField, self.cardinalButton]
    }
    
    func elements(isUserEnabled: Bool) {
        degreeTextField.isAccessibilityElement = isUserEnabled
        degreeTextField.isUserInteractionEnabled = isUserEnabled
        cardinalButton.isAccessibilityElement = isUserEnabled
        cardinalButton.isUserInteractionEnabled = isUserEnabled
    }
    
    func elements(isEnabled: Bool) {
        degreeTextField.isEnabled = isEnabled
        cardinalButton.isEnabled = isEnabled
    }
    
    func observableCoordinates() -> [Observable<String>] {
        let deg = self.degreeTextField.rx.text.orEmpty.distinctUntilChanged()
        return [deg]
    }
    
    func fillCoordinates(with coordinates: Float, zone: (String, String), auto: Bool = false) {
        self.degreeTextField.text = "\(abs(coordinates.rounded(toPlaces: 4)))"
        self.cardinalButton.setTitle(zone.1, for: .normal)
        self.cardinalButton.accessibilityLabel = zone.0

        if auto { // If coordinates are filled by automatic method: useUserPosition, selectOnMap, ConnectedBuoy we need to send actions for rx
            self.degreeTextField.sendActions(for: .valueChanged)
        }
    }
    
    func getCoordinate() -> Float? {
        guard let deg =  self.degreeTextField.text?.stringToFloat() else {
                return nil
        }
        let coordZone = Utils.coordinatesZone(zone: cardinalButton.titleLabel?.text)
        return deg  * Float(coordZone)
    }

    // MARK: - UITextFieldDelegate
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: textField)
        return false
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? ""
        
        if newString == "" { return true }
        
        // Check validity of the new string regarding pattern matching
        let isDecimal: Bool = textField == degreeTextField
        if !Utils.checkValidity(string: newString, isDecimal: isDecimal) {
            return false
        }

        return validateDecimalDegree(newString: newString, for: textField, originalString: string)
    }
    
    // The rules of the Decimal Degree
    func validateDecimalDegree(newString: String, for textField: UITextField, originalString: String) -> Bool {
        // Then check the validity of the new string, regarding
        if !isLatitude {
            if let floatText = Float(newString) {
                return abs(floatText) < 180
            } else {
                return originalString == "." || originalString == "-"  || newString == "-"
            }
        } else if isLatitude {
            if let floatText = Float(newString) {
                return abs(floatText) < 90
            } else {
                return originalString == "." || originalString == "-"  || newString == "-"
            }
        }
        return false
    }
    
}
