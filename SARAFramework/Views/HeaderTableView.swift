//
//  HeaderTableView.swift
//  SARA Croisiere
//
//  Created by Marine on 09.04.20.
//  Copyright © 2020 GraniteApps. All rights reserved.
//

import UIKit

protocol HeaderTableViewDelegate {
    func clickOnImage()
}

public class HeaderTableView: UITableViewHeaderFooterView {
    
    // MARK: - IBOutlets
    @IBOutlet weak var titleHeader: UILabel!
    @IBOutlet weak var imageButton: UIButton!
    
    // MARK: - Variables
    var delegate: HeaderTableViewDelegate?
    
    // MARK: - Init
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    public override func layoutSubviews() {
        self.accessibilityElements = [self.titleHeader, self.imageButton]
    }
    
    // MARK: - IBActions
    @IBAction func buttonAction(_ sender: UIButton) {
        delegate?.clickOnImage()
    }
    
    // MARK: - Functions
    public func customizeHeader(titleName: String, image: UIImage? = nil, accessibleText: String? = nil, buttonIsAccessible: Bool = false) {
        self.titleHeader.font = UIFont.preferredFont(forTextStyle: .headline)
        self.titleHeader.textColor = Utils.reverseColor
        self.titleHeader.text = titleName
        self.contentView.backgroundColor = Utils.primaryColor
        self.imageButton.isAccessibilityElement = buttonIsAccessible
        self.imageButton.accessibilityLabel = accessibleText
        self.imageButton.tintColor = Utils.secondaryColor
        self.imageButton.setImage(image, for: .normal)
    }
}
