//
//  UnitSettingsView.swift
//  SARA Croisiere
//
//  Created by Rose on 27/01/2018.
//  Copyright © 2018 Rose. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

public protocol SegmentedControlView {
    func valueChanged(selectedIndex: Int)
}

public class UnitSettingsView: UIView {
    
    // MARK: - IBOutlets
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var segmentedControl: UISegmentedControl!
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: UnitSettingsView.classForCoder())
    private var type: UnitType?
    public var segmentedViewDelegate: SegmentedControlView?
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        segmentedControl.customize()
        titleLabel.tintColor = Utils.primaryColor
    }
    
    func custumizeBorder() {
        layer.borderWidth = 1
        layer.borderColor = Utils.secondaryColor.cgColor
        layer.cornerRadius = 5
    }
    
    func customize(with unit: UnitType) {
        custumizeBorder()
        titleLabel.text = unit.description()
        titleLabel.accessibilityTraits = .header
        segmentedControl.removeAllSegments()
        type = unit
        
        var selectedOne: NavigationUnit? = .degrees
        switch unit {
        case .distance:
            selectedOne = Settings.shared.distanceUnit?.unit
        case .speed:
            selectedOne = Settings.shared.speedUnit?.unit
        case .coordinates:
            selectedOne = Settings.shared.coordinatesUnit?.unit
        }
        
        for element in unit.availables() {
            if unit.availables().count > 2 {
                segmentedControl.insertSegment(withTitle: element.abbreviation, at: 0, animated: false)
                segmentedControl.subviews[0].accessibilityLabel = element.description
            } else {
                if element == .spDegrees {
                    segmentedControl.insertSegment(withTitle: NSLocalizedString("unit_port_starboard_num", bundle: bundle, comment: ""), at: 0, animated: false)
                    segmentedControl.subviews[0].accessibilityLabel = NSLocalizedString("unit_port_starboard_num", bundle: bundle, comment: "")
                } else {
                    segmentedControl.insertSegment(withTitle: element.description, at: 0, animated: false)
                    segmentedControl.subviews[0].accessibilityLabel = element.description
                }
            }
            
            if element == selectedOne {
                segmentedControl.selectedSegmentIndex = 0
            }
        }
    }
    
    func setLabel(text: String) {
        titleLabel.text = text
        let font: UIFont = UIFont.systemFont(ofSize: 17)
        titleLabel.font = font
    }
    
    func setSegmentedControl(titleArray: [String], selectedIndex: Int) {
        titleArray.enumerated().forEach({
            segmentedControl.setTitle($1, forSegmentAt: $0)
        })
        segmentedControl.selectedSegmentIndex = selectedIndex
    }
    
    @IBAction func valueChanged(_ sender: Any) {
        
        let realm = try! Realm()
        try! realm.write {
            if let safeType = type, let rawUnit = type?.availables().reversed()[segmentedControl.selectedSegmentIndex].rawValue {
                switch safeType {
                case .distance:
                    Settings.shared.distanceUnit = NavigationUnitRealm(rawUnit: rawUnit)
                case .speed:
                    Settings.shared.speedUnit = NavigationUnitRealm(rawUnit: rawUnit)
                case .coordinates:
                    Settings.shared.coordinatesUnit = NavigationUnitRealm(rawUnit: rawUnit)
                }
                NavigationManager.shared.shouldReloadUnits.accept(true)
            }
        }
        segmentedViewDelegate?.valueChanged(selectedIndex: segmentedControl.selectedSegmentIndex)
    }
}
