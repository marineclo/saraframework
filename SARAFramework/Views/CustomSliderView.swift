//
//  CustomSliderView.swift
//  SARA Croisiere
//
//  Created by Marine on 08.04.20.
//  Copyright © 2020 GraniteApps. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

public class CustomSliderView: UIView {

    // MARK: - IBOutlets
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var valueLabel: UILabel!
    @IBOutlet var slider: UIAccessibilitySlider!
    @IBOutlet var decreaseButton: UIButton!
    @IBOutlet var increaseButton: UIButton!
    @IBOutlet var sliderLabel: UILabel!
    
    // MARK: - Variables
    var contentView: UIView!
    private var settingUnit: NavigationUnit?
    private var settingTitle: String?
    public var settingsValue = BehaviorRelay<Float>(value: 0)
    
    public var isEnabled: Bool {
        get {
            return self.isEnabled
        }
        set {
            self.decreaseButton.isEnabled = newValue
            self.increaseButton.isEnabled = newValue
            self.slider.isEnabled = newValue
            self.decreaseButton.isAccessibilityElement = newValue
            self.increaseButton.isAccessibilityElement = newValue
            self.slider.isAccessibilityElement = newValue
            guard let infos = self.infos else {
                return
            }
            self.titleLabel.isEnabled = newValue
            self.valueLabel.isEnabled = newValue
            if !newValue {
                self.titleLabel?.accessibilityLabel = "\(infos.title). \(NSLocalizedString("dimmed", bundle: Utils.bundle(anyClass: CustomSliderView.classForCoder()), comment: ""))"
            } else {
                guard let value = valueLabel.text, let valueFloat = Float(value) else { return }
                let labelValue = (self.infos?.isContinuous ?? false) ? "\(valueFloat.formatted)" : "\(Int(value) ?? 0)"
                var abbValue = ""
                var unitValue = ""
                if let unit = self.settingUnit {
                    abbValue = " \(unit.abbreviation)"
                    unitValue = " \(unit.description)"
                }
                guard let infos = self.infos else {
                    return
                }
                self.titleLabel?.accessibilityLabel = "\(infos.title). \(labelValue) \(abbValue)"
            }
        }
    }
    
    let disposeBag = DisposeBag()
    var infos: SliderInfos?
    
    // MARK: -
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    /// Set up xib
    func xibSetup() {
        contentView = loadViewFromNib()
        super.backgroundColor = nil
        
        let bundle = Utils.bundle(anyClass: CustomSliderView.classForCoder())
        slider.tintColor = Utils.primaryColor
        valueLabel.isAccessibilityElement = false
        
        sliderLabel.isHidden = true
        
        decreaseButton.customizeSliderButton()
        decreaseButton.accessibilityLabel = NSLocalizedString("minus", bundle: bundle, comment: "")
        
        increaseButton.customizeSliderButton()
        increaseButton.accessibilityLabel = NSLocalizedString("plus", bundle: bundle, comment: "")
        
        settingsValue.asObservable()
            .bind(onNext: { newVal in
                let labelValue = (self.infos?.isContinuous ?? false) ? "\(newVal.formatted)" : "\(Int(newVal))"
                var abbValue = ""
                var unitValue = ""
                if let unit = self.settingUnit {
                    abbValue = " \(unit.abbreviation)"
                    unitValue = " \(unit.description)"
                }
                self.slider.accessibilityValue = labelValue +  unitValue
                self.valueLabel.text = labelValue + abbValue
                guard let infos = self.infos else {
                    return
                }
                self.slider.value = newVal.formatted
                self.titleLabel?.accessibilityLabel = "\(infos.title) \(labelValue) \(abbValue)"
            })
        
        // use bounds not frame or it'll be offset
        contentView.frame = bounds
        
        // Make the view stretch with containing view
        contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(contentView)
    }
    
    /// Load desired view from nib
    ///
    /// - Returns: UIView
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return view
    }
        
    public func customize(with sliderInfos: SliderInfos, accessibilityTrait: UIAccessibilityTraits = .none) {
        self.settingTitle = sliderInfos.title
        self.settingUnit = sliderInfos.unit
        
        self.titleLabel?.text = sliderInfos.title
        self.titleLabel?.accessibilityTraits = accessibilityTrait
        self.settingsValue.accept(sliderInfos.value)
        let labelValue = (sliderInfos.isContinuous ?? false) ? "\(sliderInfos.value)" : "\(Int(sliderInfos.value))"
        self.titleLabel?.accessibilityLabel = "\(sliderInfos.title) \(labelValue)\(sliderInfos.unit?.description ?? "")"
        
        self.slider.minimumValue = sliderInfos.minValue
        self.slider.maximumValue = sliderInfos.maxValue
        self.slider.value = sliderInfos.value
        self.slider.step = Double(sliderInfos.step)
        
        self.infos = sliderInfos
    }
    
    func elements(isUserAccessibilityEnabled: Bool) {
        titleLabel.isAccessibilityElement = isUserAccessibilityEnabled
        slider.isAccessibilityElement = isUserAccessibilityEnabled
        slider.isUserInteractionEnabled = isUserAccessibilityEnabled
        decreaseButton.isUserInteractionEnabled = isUserAccessibilityEnabled
        decreaseButton.isAccessibilityElement = isUserAccessibilityEnabled
        increaseButton.isUserInteractionEnabled = isUserAccessibilityEnabled
        increaseButton.isAccessibilityElement = isUserAccessibilityEnabled
    }
    
    func elements(isEnabled: Bool) {
        slider.isEnabled = isEnabled
        decreaseButton.isEnabled = isEnabled
        increaseButton.isEnabled = isEnabled
    }
    
    // MARK: - IBActions
    @IBAction func sliderValueDidChange(_ sender: UISlider) {
//        print(sender.value)
        let roundedValue = round(sender.value / Float(slider.step)) * Float(slider.step)
        if roundedValue < slider.minimumValue {
            self.settingsValue.accept(slider.minimumValue)
        } else {
            self.settingsValue.accept(roundedValue)
        }
    }
    
    @IBAction func buttonValueDidChange(_ sender: UIButton) {
        if sender == decreaseButton {
            if self.settingsValue.value > slider.minimumValue {
                if self.settingsValue.value - Float(slider.step) < slider.minimumValue {
                    self.settingsValue.accept(slider.minimumValue)
                } else {
                    self.settingsValue.accept(self.settingsValue.value - Float(slider.step))
                }
                self.slider.value = self.settingsValue.value
            }
        }
        if sender == increaseButton {
            if self.settingsValue.value < slider.maximumValue {
                self.settingsValue.accept(round((self.settingsValue.value + Float(slider.step)) / Float(slider.step)) * Float(slider.step))
                self.slider.value = self.settingsValue.value
            }
        }
    }
}

public struct SliderInfos {
    let title: String
    let unit: NavigationUnit?
    let isContinuous: Bool // False: Int, True: Float
    let value: Float
    let minValue: Float
    let maxValue: Float
    let step: Float
    
    public init(title: String, unit: NavigationUnit?, value: Float, minValue: Float, maxValue: Float, step: Float = 1, isContinuous: Bool) {
        self.title = title
        self.unit = unit
        self.value = value
        self.minValue = minValue
        self.maxValue = maxValue
        self.step = step
        self.isContinuous = isContinuous
    }
}
