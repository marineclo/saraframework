//
//  AccessibilitySwitchView.swift
//  SARA_Framework
//
//  Created by Marine IL on 07.02.20.
//  Copyright © 2020 Marine. All rights reserved.
//

import UIKit

public protocol AccessibleSwitch {
    func switchAction()
}

public class AccessibilitySwitchView: UIView {
    
    // MARK: - Outlets
    @IBOutlet public weak var titleLabel: UILabel!
    @IBOutlet public weak var settingSwitch: UISwitch!
    
    // MARK: - Actions
    @IBAction func switchAction(_ sender: UISwitch) {
        self.delegate?.switchAction()
    }
    // MARK: - Variables
    public var delegate: AccessibleSwitch?
    
    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        let bundle = Bundle.init(for: AccessibilitySwitchView.self)
        if let viewsToAdd = bundle.loadNibNamed("AccessibilitySwitchView", owner: self, options: nil), let contentView = viewsToAdd.first as? UIView {
            addSubview(contentView)
            contentView.frame = self.bounds
            contentView.autoresizingMask = [.flexibleHeight,
                                            .flexibleWidth]
        }
        
        self.accessibilityElements = [titleLabel!, settingSwitch!]
    }
    
    // MARK: - Override
    public override var accessibilityTraits: UIAccessibilityTraits {
        get { return settingSwitch.accessibilityTraits }
        set {}
    }
    
    public override var accessibilityValue: String? {
        get { return settingSwitch.isOn ? "1" : "0"}
        set {}
    }
    
    public override var accessibilityLabel: String? {
        get { return titleLabel.text }
        set { }
    }
    
    //Function called by the system when a double tap occurs on the selected wrapper.
    public override func accessibilityActivate() -> Bool {
        settingSwitch.setOn(!settingSwitch.isOn, animated: false)
        settingSwitch.sendActions(for: .valueChanged)
        return true
    }
    
}
