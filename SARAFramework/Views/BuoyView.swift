//
//  BuoyView.swift
//  SARA Croisiere
//
//  Created by Marine on 18.04.20.
//  Copyright © 2020 GraniteApps. All rights reserved.
//

import UIKit

protocol BuoyViewDelegate: class {
    func openSelectOnMap(tagView: Int?)
    func useCurrentPosition(tagView: Int?, gpsPosition: GPSPosition)
}

class BuoyView: UIView, UITextFieldDelegate {

    // MARK: - IBOutlets
    @IBOutlet weak var buoyTypeLabel: UILabel!
    @IBOutlet var coordinatesView: CoordinatesView!
    @IBOutlet weak var buoyElementsView: UIView!
    @IBOutlet weak var toLetAtControl: UISegmentedControl!
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: BuoyView.classForCoder())
    var currentBuoy: Buoy?
    var delegate: BuoyViewDelegate?
    var reversed: Bool?
            
    // MARK: -
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        self.buoyTypeLabel.accessibilityTraits = .header
        
        self.toLetAtControl.setTitle(NSLocalizedString("unit_port", bundle: bundle, comment: ""), forSegmentAt: 0)
        self.toLetAtControl.setTitle(NSLocalizedString("unit_starboard", bundle: bundle, comment: ""), forSegmentAt: 1)
        self.toLetAtControl.customize()
        
        self.buoyElementsView.customizeView()
    }
    
    func setBuoy(buoy: Buoy) {
        self.currentBuoy = buoy
        if let gpsPos = buoy.gpsPosition {
            self.coordinatesView.fillCoordinates(with: gpsPos)
        }
        self.toLetAtControl.selectedSegmentIndex = buoy.toLetAt(inversed: reversed) == Buoy.ToLetAt.port.rawValue ? 0 : 1
    }
    
    func createBuoyFromView() -> Buoy {
        var gpsPos: GPSPosition?
        if let coordinates = self.coordinatesView.getCoordinates() {
            gpsPos = GPSPosition(id: self.currentBuoy?.gpsPosition?.id ,latitude: coordinates.0, longitude: coordinates.1)
        }
        var toLetAt: Buoy.ToLetAt
        if let _reversed = reversed, _reversed {
            toLetAt = self.toLetAtControl.selectedSegmentIndex == 0 ? .starboard : .port
        } else {
            toLetAt = self.toLetAtControl.selectedSegmentIndex == 0 ? .port : .starboard
        }
        let buoy = Buoy(id: self.currentBuoy?.id ,name: self.buoyTypeLabel.text ?? "", gpsPosition: gpsPos, toLetAt: toLetAt, type: self.currentBuoy?.type.value)
        return buoy
    }
    
    // Enable / Disable elements
    func elements(isEnabled: Bool) {
        toLetAtControl.isEnabled = isEnabled
        coordinatesView.elements(isEnabled: isEnabled)
    }
}
