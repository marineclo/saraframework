//
//  InformationCell.swift
//  SARA Croisiere
//
//  Created by Rose on 23/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import UIKit

public class InformationCell: UITableViewCell {
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: InformationCell.classForCoder())
    
    // MARK: - IBOutlets
    @IBOutlet private weak var infoTypeLabel: UILabel!
    @IBOutlet weak var infoValue1Label: UILabel!
    @IBOutlet weak var infoUnit1Label: UILabel!
    @IBOutlet private weak var infoValue2Label: UILabel!
    @IBOutlet weak var infoUnit2Label: UILabel!
    @IBOutlet var secondValueConstraint: NSLayoutConstraint! // Constraint to hide second values when no needs
    
    var infos: (String?, String?, NavigationUnit?, String?, NavigationUnit?)? {
        didSet {
            if let safeInfos = infos {
                // Update informations of the cell
                infoTypeLabel.text = safeInfos.0
                
                infoValue1Label.text = safeInfos.1 ?? NSLocalizedString("unavailable_nav_symbol", bundle: bundle, comment: "")
                infoUnit1Label.text = safeInfos.2?.abbreviation
                infoUnit1Label.isAccessibilityElement = false
                infoValue1Label.isAccessibilityElement = false
                infoTypeLabel.isAccessibilityElement = false
                if var safeValue = safeInfos.1 {
                    if let safeUnit = safeInfos.2 {
                        if safeValue.last == "'" {
                            let degrees = safeValue.split(separator: " ").first ?? ""
                            var minutes = String(safeValue.split(separator: " ").last?.split(separator: "'").first ?? "")
                            minutes = minutes.replacingOccurrences(of: ".", with: ",")
                            safeValue = "\(degrees) \(minutes) \(NSLocalizedString("unit_dm", bundle: bundle, comment: ""))"
                        }
                        infoValue1Label.accessibilityLabel = "\(safeValue) \(safeUnit.description)"
                    } else {
                        infoValue1Label.accessibilityLabel = "\(safeValue)"
                    }
                } else {
                    infoValue1Label.accessibilityLabel = NSLocalizedString("unavailable_nav", bundle: bundle, comment: "")
                }
                if safeInfos.3 == nil && safeInfos.4 == nil {
                    self.secondValueConstraint.isActive = true
                    self.infoValue2Label.isHidden = true
                    self.infoUnit2Label.isHidden = true
                    self.accessibilityLabel = "\(safeInfos.0!) \(infoValue1Label.accessibilityLabel!)"
                } else {
                    self.secondValueConstraint.isActive = false
                    self.accessibilityLabel = "\(safeInfos.0!)"
                    self.infoValue1Label.isAccessibilityElement = true
                    self.infoValue2Label.isHidden = false
                    self.infoUnit2Label.isHidden = false
                    infoValue2Label.text = safeInfos.3 ?? NSLocalizedString("unavailable_nav_symbol", bundle: bundle, comment: "")
                    infoUnit2Label.text = safeInfos.4?.abbreviation
                    infoUnit2Label.isAccessibilityElement = false
                    infoValue2Label.isAccessibilityElement = true
                    if let safeValue = safeInfos.3 {
                        if let safeUnit = safeInfos.4 {
                            infoValue2Label.accessibilityLabel = "\(safeValue) \(safeUnit.description)"
                        } else {
                            infoValue2Label.accessibilityLabel = "\(safeValue)"
                        }
                    } else {
                        infoValue2Label.accessibilityLabel = NSLocalizedString("unavailable_nav", bundle: bundle, comment: "")
                    }
                }
                
            }
        }
    }
    
//    var value: String? {
//        didSet {
//            infoValue2Label.text = value
//        }
//    }
    
    func customizeCell() {
        self.backgroundColor = Utils.primaryColor
        self.infoTypeLabel.textColor = Utils.reverseColor
        self.infoValue1Label.textColor = Utils.reverseColor
        self.infoUnit1Label.textColor = Utils.reverseColor
        self.infoValue2Label.textColor = Utils.reverseColor
        self.infoUnit2Label.textColor = Utils.reverseColor
        self.selectionStyle = .none
//        self.separatorInset = UIEdgeInsets.zero
//        self.layoutMargins = UIEdgeInsets.zero
    }
}
