//
//  DMSCoordinateView.swift
//
//
//  Created by Marine on 17.07.23.
//

import UIKit
import RxSwift

public class DMSCoordinateView: UIView, UITextFieldDelegate {

    // MARK: - IBOutlets
    @IBOutlet var degreeTextField: UITextField!
    @IBOutlet var degreeLabel: UILabel!
    @IBOutlet var minuteTextField: UITextField!
    @IBOutlet var minuteLabel: UILabel!
    @IBOutlet var secondTextField: UITextField!
    @IBOutlet var secondLabel: UILabel!
    @IBOutlet var cardinalButton: UIButton!
    
    // MARK: - 
    @IBAction func cardinalButtonAction(_ sender: UIButton) {
        if isLatitude && !self.doNothing{
            let (latZone, latZoneInital) = Utils.getLatitudeZone(r: sender.titleLabel?.text != NSLocalizedString("north_initial", bundle: bundle, comment: ""))
            sender.setTitle(latZoneInital, for: .normal)
            sender.accessibilityLabel = latZone
        } else if !isLatitude && !self.doNothing {
            let (longZone, longZoneInitial) = Utils.getLongitudeZone(r: sender.titleLabel?.text != NSLocalizedString("east_initial", bundle: bundle, comment: ""))
            sender.setTitle(longZoneInitial, for: .normal)
            sender.accessibilityLabel = longZone
        }
    }
    
    // MARK: - Variables
    var contentView: UIView!
    let bundle = Utils.bundle(anyClass: DMSCoordinateView.classForCoder())
    var isLatitude = false
    var doNothing: Bool = false
    
    // MARK: -
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    // Set up xib
    func xibSetup() {
        contentView = loadViewFromNib()
        
        setupLabels()
        
        // use bounds not frame or it'll be offset
        contentView.frame = bounds
        
        // Make the view stretch with containing view
        contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(contentView)
    }
    
    // Load desired view from nib
    //
    // - Returns: UIView
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return view
    }
    
    func setupLabels() {
        self.degreeLabel.isAccessibilityElement = false
        self.degreeLabel.text = NSLocalizedString("unit_abb_degrees", bundle: bundle, comment: "")
        self.degreeTextField.accessibilityLabel = NSLocalizedString("unit_abb_degrees", bundle: bundle, comment: "")
        self.minuteLabel.isAccessibilityElement = false
        self.minuteLabel.text = NSLocalizedString("unit_abb_minutes", bundle: bundle, comment: "")
        self.minuteTextField.accessibilityLabel = NSLocalizedString("unit_minutes", bundle: bundle, comment: "")
        self.secondLabel.isAccessibilityElement = false
        self.secondLabel.text = NSLocalizedString("unit_abb_secondes", bundle: bundle, comment: "")
        self.secondTextField.accessibilityLabel = NSLocalizedString("unit_seconds", bundle: bundle, comment: "")
    }
    
    func setupButton() {
        self.cardinalButton.customizeSecondary()
        if isLatitude {
            self.cardinalButton.setTitle(NSLocalizedString("north_initial", bundle: bundle, comment: ""), for: .normal)
            self.cardinalButton.accessibilityLabel = NSLocalizedString("north", bundle: bundle, comment: "")
        } else {
            self.cardinalButton.setTitle(NSLocalizedString("east_initial", bundle: bundle, comment: ""), for: .normal)
            self.cardinalButton.accessibilityLabel = NSLocalizedString("east", bundle: bundle, comment: "")
        }
    }
    
    func accessibleElementsOrder() -> [Any] {
        return [self.degreeTextField, self.minuteTextField, self.secondTextField, self.cardinalButton]
    }
    
    func elements(isUserEnabled: Bool) {
        degreeTextField.isAccessibilityElement = isUserEnabled
        degreeTextField.isUserInteractionEnabled = isUserEnabled
        minuteTextField.isAccessibilityElement = isUserEnabled
        minuteTextField.isUserInteractionEnabled = isUserEnabled
        secondTextField.isAccessibilityElement = isUserEnabled
        secondTextField.isUserInteractionEnabled = isUserEnabled
        cardinalButton.isAccessibilityElement = isUserEnabled
        cardinalButton.isUserInteractionEnabled = isUserEnabled
    }
    
    func elements(isEnabled: Bool) {
        degreeTextField.isEnabled = isEnabled
        minuteTextField.isEnabled = isEnabled
        secondTextField.isEnabled = isEnabled
        cardinalButton.isEnabled = isEnabled
    }
    
    func observableCoordinates() -> [Observable<String>] {
        let deg = self.degreeTextField.rx.text.orEmpty.distinctUntilChanged()
        let min = self.minuteTextField.rx.text.orEmpty.distinctUntilChanged()
        let sec = self.secondTextField.rx.text.orEmpty.distinctUntilChanged()
        
        return [deg, min, sec]
    }
    
    func fillCoordinates(with coordinates: (Int, Int, Float), zone: (String, String), auto: Bool = false) {
        self.degreeTextField.text = String(abs(coordinates.0))
        self.minuteTextField.text = String(abs(coordinates.1))
        self.secondTextField.text = String(format: "%.3f", abs(coordinates.2))
        self.cardinalButton.setTitle(zone.1, for: .normal)
        self.cardinalButton.accessibilityLabel = zone.0

        if auto { // If coordinates are filled by automatic method: useUserPosition, selectOnMap, ConnectedBuoy we need to send actions for rx
            self.degreeTextField.sendActions(for: .valueChanged)
            self.minuteTextField.sendActions(for: .valueChanged)
            self.secondTextField.sendActions(for: .valueChanged)
        }
    }
    
    func getCoordinate() -> Float? {
        guard let deg =  self.degreeTextField.text?.stringToFloat(),
            let min = self.minuteTextField.text?.stringToFloat(),
            let sec = self.secondTextField.text?.stringToFloat() else {
                return nil
        }
        let coordZone = Utils.coordinatesZone(zone: cardinalButton.titleLabel?.text)
        return (deg + ((sec / 60) + min) / 60) * Float(coordZone)
    }

    // MARK: - UITextFieldDelegate
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: textField)
        return false
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? ""
        
        if newString == "" { return true }
        
        // Check validity of the new string regarding pattern matching
        let isDecimal: Bool = textField == secondTextField
        if !Utils.checkValidity(string: newString, isDecimal: isDecimal) {
            return false
        }

        return validateDegreeMinuteSecond(newString: newString, for: textField, originalString: string)
    }
    
    //TODO: - 
    func validateDegreeMinuteSecond(newString: String, for textField: UITextField, originalString: String) -> Bool {
        // Then check the validity of the new string, regarding
        // The rules of the degrees minute second
        if textField == secondTextField {
            if let floatText = Float(newString) {
                // Latitude can't go higher than 90.000
                if isLatitude && degreeTextField.text == "90" {
                    return floatText < 0.001
                }
                // Longitude can't go high than 180.000
                if !isLatitude && degreeTextField.text == "180" {
                    return floatText < 0.001
                }
                if minuteTextField.text == "60" {
                    return floatText < 0.001
                }
                return floatText < 59.999
            } else {
                return originalString == "."
            }
        } else if textField == minuteTextField {
            if let floatText = Float(newString) {
                if floatText > 60.000 {
                    return false
                }
                if floatText == 60.000 {
                    return Float(secondTextField.text ?? "") ?? 0.0 < 0.001
                }
                if isLatitude && degreeTextField.text == "90" {
                    return floatText < 0.001
                }
                if !isLatitude && degreeTextField.text == "180" {
                    return floatText < 0.001
                }
                return true
            } else {
                return false
            }
        } else {
            if isLatitude {
                if let floatText = Float(newString) {
                    // Latitude can't go higher than 90.000
                    if floatText > 90.000 {
                        return false
                    }
                    if floatText == 90.000 {
                        return Float(minuteTextField.text ?? "") ?? 0.0 < 0.001
                    }
                    return true
                } else {
                    return false
                }
            } else {
                // Longitude can't go high than 180.000
                if let floatText = Float(newString) {
                    if floatText > 180.000 {
                        return false
                    }
                    if floatText == 180.000 {
                        return Float(minuteTextField.text ?? "") ?? 0.0 < 0.001
                    }
                    return true
                } else {
                    return false
                }
            }
        }
    }
}
