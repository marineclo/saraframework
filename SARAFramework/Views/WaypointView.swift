//
//  WaypointView.swift
//  SARAFramework
//
//  Created by Marine on 31.05.21.
//  Copyright © 2021 GraniteApps. All rights reserved.

import UIKit

class WaypointView: UIView {
    
    // MARK: - IBOutlets
    @IBOutlet var coordinatesView: CoordinatesView!
    @IBOutlet var sliderView: CustomSliderView!
    

    // MARK: - Variables
    var contentView: UIView!
    let bundle = Utils.bundle(anyClass: WaypointView.classForCoder())
    
    // MARK: -
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    // Set up xib
    func xibSetup() {
        contentView = loadViewFromNib()
        
        // Add border
        contentView.customizeView()
        // Buttons
//        setupButtons()
        // Coordinates
//        setupCoordinates()
        // Fill coordinates
//        if let gpsPos = gpsPosition {
//            fillCoordinates(with: gpsPos)
//        }
                
        // use bounds not frame or it'll be offset
        contentView.frame = bounds
        
        // Make the view stretch with containing view
        contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(contentView)
    }
    
    // Load desired view from nib
    //
    // - Returns: UIView
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return view
    }

    // Enable / Disable user interaction and accessibility
    func elements(isUserAccessibilityEnabled: Bool) {
        sliderView.elements(isUserAccessibilityEnabled: isUserAccessibilityEnabled)
        coordinatesView.elements(isUserEnabled: isUserAccessibilityEnabled)
    }
    
    // Enable / Disable elements
    func elements(isEnabled: Bool) {
        sliderView.elements(isEnabled: isEnabled)
        coordinatesView.elements(isEnabled: isEnabled)
    }
}
