//
//  SeascapeTableViewCell.swift
//  SARAFramework
//
//  Created by Marine on 19.07.21.
//

import UIKit

class SeascapeTableViewCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var lenghtLabel: UILabel!
    @IBOutlet var lenghtValue: UILabel!
    @IBOutlet var lenghtUnit: UILabel!
    @IBOutlet var azimutLabel: UILabel!
    @IBOutlet var azimutValue: UILabel!
    @IBOutlet var azimutUnit: UILabel!
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: SeascapeTableViewCell.classForCoder())
    
    var infos: (String?, String?, NavigationUnit?, String?, NavigationUnit?)? {
        didSet {
            if let safeInfos = infos {
                nameLabel.text = safeInfos.0
                lenghtLabel.text = NSLocalizedString("distance", bundle: bundle, comment: "")
                lenghtValue.text = safeInfos.1 ?? NSLocalizedString("unavailable_nav_symbol", bundle: bundle, comment: "")
                lenghtUnit.text = safeInfos.2?.abbreviation ?? NSLocalizedString("unavailable_nav_symbol", bundle: bundle, comment: "")
                lenghtValue.isAccessibilityElement = false
                lenghtUnit.isAccessibilityElement = false
                lenghtLabel.accessibilityLabel = "\(NSLocalizedString("", bundle: bundle, comment: "")) \(safeInfos.1 ?? NSLocalizedString("unavailable_nav", bundle: bundle, comment: "")) \(safeInfos.2?.description ?? "")"
                azimutLabel.text = NSLocalizedString("relative_bearing", bundle: bundle, comment: "")
                if safeInfos.3 == nil && safeInfos.4 == nil {
                    azimutValue.text = ""
                    azimutUnit.text = NSLocalizedString("unavailable_nav_symbol", bundle: bundle, comment: "")
                } else {
                    azimutValue.text = safeInfos.3
                    azimutUnit.text = safeInfos.4?.abbreviation
                }
                azimutValue.isAccessibilityElement = false
                azimutUnit.isAccessibilityElement = false
                azimutLabel.accessibilityLabel = "\(NSLocalizedString("", bundle: bundle, comment: "")) \(safeInfos.3 ?? NSLocalizedString("unavailable_nav", bundle: bundle, comment: "")) \(safeInfos.4?.description ?? "")"
            }
        }
    }
    
    // MARK: - Override
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - func
    func customizeCell() {
        self.backgroundColor = Utils.primaryColor
        self.nameLabel.textColor = Utils.reverseColor
        self.lenghtLabel.textColor = Utils.reverseColor
        self.lenghtValue.textColor = Utils.reverseColor
        self.lenghtUnit.textColor = Utils.reverseColor
        self.azimutLabel.textColor = Utils.reverseColor
        self.azimutValue.textColor = Utils.reverseColor
        self.azimutUnit.textColor = Utils.reverseColor
    }
    
}
