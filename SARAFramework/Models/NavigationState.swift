//
//  NavigationState.swift
//  SARA Croisiere
//
//  Created by Rose on 22/11/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import RealmSwift

public class NavigationState: Object {
    @objc dynamic var currentCourse: AnyCourse?
    @objc dynamic var recordingStatus: String = TraceManager.RecordingStatus.idle.rawValue
    
    public required convenience init(course: Course) {
        self.init()
        let realm = try! Realm()
        let anyCourse = realm.objects(AnyCourse.self).first(where: {$0.primaryKey == course.id})
        self.currentCourse = anyCourse
     }
    
    public func saveAndReplace(isSaved: Bool = true) {
        let realm = try! Realm()
        try! realm.write {
            let oldOnes = realm.objects(NavigationState.self)
            realm.delete(oldOnes)
            if isSaved {
                realm.add(self)
            }
        }
    }
    
    public static func removeAll() {
        let realm = try! Realm()
        try! realm.write {
            let oldOnes = realm.objects(NavigationState.self)
            realm.delete(oldOnes)
        }
    }
}
