//
//  Settings.swift
//  SARA Croisiere
//
//  Created by Rose on 07/12/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import RealmSwift

public class Settings: Object {
    
    @objc dynamic var minimumDistanceThreshold: Float = 100.0
    @objc dynamic var useCompass: Bool = false
    @objc dynamic var invertedColors: Bool = false
    @objc dynamic var saveLogs: Bool = false
    @objc dynamic var gpsActivationLevel: Int = 1
    @objc dynamic var averageHeadingTime: Int = 10
    @objc dynamic var averageSpeedTime: Int = 10
    @objc dynamic var bearingUnit: NavigationUnitRealm? = NavigationUnitRealm(rawUnit: NavigationUnit.degrees.rawValue)
    @objc dynamic var distanceUnit: NavigationUnitRealm? = NavigationUnitRealm(rawUnit: NavigationUnit.mile.rawValue)
    @objc public dynamic var speedUnit: NavigationUnitRealm? = NavigationUnitRealm(rawUnit: NavigationUnit.knots.rawValue)
    @objc dynamic var coordinatesUnit: NavigationUnitRealm? = NavigationUnitRealm(rawUnit: NavigationUnit.decimalDegrees.rawValue)
    @objc dynamic var traceSensitivity: Int = 50 // if this value changes, check load function
    @objc dynamic var saveTrace: Bool = false
    
    // GPS setting: phone gps or navigation center
    @objc dynamic var usePhoneGPS: Bool = true
    
    // NMEA Settings
    @objc dynamic var nmeaEnabled: Bool = false
    @objc dynamic var nmeaTCPEnabled: Bool = false
    @objc dynamic var nmeaHost: String?
    @objc dynamic var nmeaPort: Int = 10110 // Default NMEA port
    @objc dynamic var useSpeedoForTrueWind: Bool = true
    
    // Developer mode
    public var developerMode = false
    
    // Singleton
    public static let shared: Settings = Settings()
    
    // Default Profile
    public var defaultProfileTypes: [ProfileType] = []

    public func defaultProfile() -> Profile {
        var profileTypes: [ProfileType] = []
        self.defaultProfileTypes.forEach({
            if let pGPS = $0 as? ProfileGPS {
                let newPGPS = ProfileGPS(compass: pGPS.compass?.value.newCopy(), courseOverGround: pGPS.courseOverGround?.value.newCopy(), speedOverGround: pGPS.speedOverGround?.value.newCopy(), maxSpeedOverGround: pGPS.maxSpeedOverGround?.value.newCopy())
                profileTypes.append(newPGPS)
            } else if let pRoute = $0 as? ProfileRoute {
                let newPRoute = ProfileRoute(azimut: pRoute.azimut?.value.newCopy(), gisement: pRoute.gisement?.value.newCopy(), gisementTime: pRoute.gisementTime?.value.newCopy(), distance: pRoute.distance?.value.newCopy(), cmg: pRoute.cmg?.value.newCopy(), distanceToSegment: pRoute.distanceToSegment?.value.newCopy(), entry3Lengths: pRoute.entry3Lengths?.value.newCopy())
                profileTypes.append(newPRoute)
            } else if let pCentrale = $0 as? ProfileCentrale {
                let newPCentrale = ProfileCentrale(depth: pCentrale.depth?.value.newCopy(), headingMagnetic: pCentrale.headingMagnetic?.value.newCopy(), speedThroughWater: pCentrale.speedThroughWater?.value.newCopy(), trueWindSpeed: pCentrale.trueWindSpeed?.value.newCopy(), trueWindAngle: pCentrale.trueWindAngle?.value.newCopy(), apparentWindSpeed: pCentrale.apparentWindSpeed?.value.newCopy(), apparentWindAngle: pCentrale.apparentWindAngle?.value.newCopy(), trueWindDirection: pCentrale.trueWindDirection?.value.newCopy(), waterTemperature: pCentrale.waterTemperature?.value.newCopy(), airTemperature: pCentrale.airTemperature?.value.newCopy())
                profileTypes.append(newPCentrale)
            } else if let pTelltale = $0 as? ProfileTelltale {
                let newPTelltale = ProfileTelltale(angle: pTelltale.angle.value.newCopy(), mediane: pTelltale.mediane.value.newCopy(), average: pTelltale.average.value.newCopy(), standardDeviation: pTelltale.standardDeviation.value.newCopy(), variationMax: pTelltale.variationMax.value.newCopy(), state: pTelltale.state.value.newCopy())
                profileTypes.append(newPTelltale)
            } else if let pVoice = $0 as? ProfileVarious {
                let newPVoice = ProfileVarious(voice: pVoice.voice.value, vocalRate: pVoice.vocalRate.value, batteryLevel: pVoice.batteryLevel.value.newCopy())
                profileTypes.append(newPVoice)
            } else if let pCarto = $0 as? ProfileCarto {
                let newPCarto = ProfileCarto(enableBeacons: pCarto.enableBeacons.value, timeAnnounce: pCarto.timeAnnounce?.value.newCopy(), detectionDistance: pCarto.detectionDistance?.value.newCopy(), detectionNumber: pCarto.detectionNumber?.value.newCopy(), distance: pCarto.distance?.value.newCopy(), azimut: pCarto.azimut?.value.newCopy(), gisement: pCarto.gisement?.value.newCopy(), gisementTime: pCarto.gisementTime?.value.newCopy(), onlyFront: pCarto.onlyFront?.value.newCopy())
                profileTypes.append(newPCarto)
            }
        })
        return Profile(id: nil, name: "", profileTypes: profileTypes)
    }
    
    public func load(distanceUnit: NavigationUnitRealm? = NavigationUnitRealm(rawUnit: NavigationUnit.mile.rawValue), traceSensitivity: Int = 50) {
        let realm = try! Realm()
        if let settings = realm.objects(Settings.self).first {
            self.minimumDistanceThreshold = settings.minimumDistanceThreshold
            self.useCompass = settings.useCompass
            self.invertedColors = settings.invertedColors
            self.saveLogs = settings.saveLogs
            self.gpsActivationLevel = settings.gpsActivationLevel
            self.averageHeadingTime = settings.averageHeadingTime
            self.averageSpeedTime = settings.averageSpeedTime
            self.bearingUnit = settings.bearingUnit
            self.distanceUnit = settings.distanceUnit
            self.speedUnit = settings.speedUnit
            self.coordinatesUnit = settings.coordinatesUnit
            self.nmeaHost = settings.nmeaHost
            self.nmeaPort = settings.nmeaPort
            self.nmeaEnabled = settings.nmeaEnabled
            self.nmeaTCPEnabled = settings.nmeaTCPEnabled
            self.usePhoneGPS = settings.usePhoneGPS
            self.traceSensitivity = settings.traceSensitivity
            self.saveTrace = settings.saveTrace
            self.useSpeedoForTrueWind = settings.useSpeedoForTrueWind
            try! realm.write {
                realm.delete(settings)
            }
            
        } else {
            self.distanceUnit = distanceUnit
            self.traceSensitivity = traceSensitivity
        }
        try! realm.write {
            realm.add(self)
        }
    }
    
    public func update() {
        let realm = try! Realm()
        try! realm.write {
            let oldOnes = realm.objects(Settings.self)
            realm.delete(oldOnes)
            realm.add(self)
        }
    }
    
    public required convenience init(
            minimumDistanceThreshold: Float,
            useCompass: Bool,
            invertedColors: Bool,
            saveLogs: Bool,
            vocalRate: Double,
            gpsActivationLevel: Int,
            averageHeadingTime: Int,
            averageSpeedTime: Int,
            bearingUnit: NavigationUnitRealm,
            distanceUnit: NavigationUnitRealm,
            speedUnit: NavigationUnitRealm,
            coordinatesUnit: NavigationUnitRealm,
            nmeaEnabled: Bool,
            nmeaHost: String?,
            nmeaPort: Int,
            usePhoneGPS: Bool,
            traceSensitivity: Int,
            saveTrace: Bool,
            useSpeedoForTrueWind: Bool) {
        
        self.init()
        
        self.minimumDistanceThreshold = minimumDistanceThreshold
        self.useCompass = useCompass
        self.invertedColors = invertedColors
        self.gpsActivationLevel = gpsActivationLevel
        self.averageHeadingTime = averageHeadingTime
        self.averageSpeedTime = averageSpeedTime
        self.bearingUnit = bearingUnit
        self.distanceUnit = distanceUnit
        self.speedUnit = speedUnit
        self.coordinatesUnit = coordinatesUnit
        self.nmeaEnabled = nmeaEnabled
        self.nmeaHost = nmeaHost
        self.nmeaPort = nmeaPort
        self.usePhoneGPS = usePhoneGPS
        self.traceSensitivity = traceSensitivity
        self.saveTrace = saveTrace
        self.useSpeedoForTrueWind = useSpeedoForTrueWind
        self.developerMode = developerMode
    }
    
    public func setUsePhoneGPS(value: Bool) {
        let realm = try! Realm()
        try! realm.write {
            self.usePhoneGPS = value
        }
    }
    
    public func setNmeaTCPEnabled(value: Bool) {
        let realm = try! Realm()
        try! realm.write {
            self.nmeaTCPEnabled = value
        }
    }
    
    public func setUseSpeedoForTrueWind(value: Bool) {
        let realm = try! Realm()
        try! realm.write {
            self.useSpeedoForTrueWind = value
        }
    }
    
    public func setHost(value: String) {
        let realm = try! Realm()
        try! realm.write {
            self.nmeaHost = value
        }
    }
    
    public func setPort(value: Int) {
        let realm = try! Realm()
        try! realm.write {
            self.nmeaPort = value
        }
    }
    
    public func setNmeaEnabled(value: Bool) {
        let realm = try! Realm()
        try! realm.write {
            self.nmeaEnabled = value
        }
    }
    
    public var availableUnits: [UnitType] = []
    
    public func setUseCompass(value: Bool) {
        let realm = try! Realm()
        try! realm.write {
            self.useCompass = value
        }
    }
    
    public func setSaveTrace(value: Bool) {
        let realm = try! Realm()
        try! realm.write {
            self.saveTrace = value
        }
    }
    
    public func getSaveTrace() -> Bool {
        return self.saveTrace
    }
}
