//
//  Buoy.swift
//  SARA Croisiere
//
//  Created by Marine on 13.04.20.
//  Copyright © 2020 GraniteApps. All rights reserved.
//

import Foundation
import RealmSwift

@objc public enum BuoyType: Int, RealmEnum {
    case custom
    case startPin
    case startCommittee
    case finishCommittee
    case finishPin
    case windward
    case leeward
    case offsetMark
    case gatePort
    case gateStartboard
}

public class Buoy: Object {
    @objc dynamic var id: String = ""
    @objc public dynamic var name: String = ""
    @objc public dynamic var gpsPosition: GPSPosition?
    @objc private dynamic var toLetAt: String = ""
    public var type = RealmOptional<BuoyType>()
    
    public enum ToLetAt: String {
        public typealias RawValue = String
        
        case port = "port"
        case starboard = "starboard"
    }
    
    public override static func primaryKey() -> String? {
        return "id"
    }
    
    public convenience init(id: String? = nil, name: String, gpsPosition: GPSPosition?, toLetAt: ToLetAt, type: BuoyType? = nil) {
        self.init()
        if let safeId = id {
            self.id = safeId
        } else {
            self.id = UUID().uuidString
        }
        self.name = name
        self.gpsPosition = gpsPosition
        self.toLetAt = toLetAt.rawValue
        self.type.value = type
    }
    
    public func toLetAt(inversed: Bool? = false) -> String {
        if let inv = inversed, inv {
            return self.toLetAt == ToLetAt.port.rawValue ? ToLetAt.starboard.rawValue : ToLetAt.port.rawValue
        }
        return self.toLetAt
    }
    
    func duplicate(newUUID : Bool = false) -> Buoy {
        return Buoy(id: newUUID ? UUID().uuidString : self.id, name: self.name, gpsPosition: self.gpsPosition?.duplicate(newUUID: newUUID), toLetAt: Buoy.ToLetAt(rawValue: self.toLetAt) ?? ToLetAt.port, type: self.type.value)
    }
    
    public override func isEqual(_ object: Any?) -> Bool {
        if let obj = object as? Buoy, self.id == obj.id {
            return true
        }
        return false
    }
    
    static func decode(dictionary: [String : Any]) -> Buoy? {
        guard let id = dictionary["id"] as? String, let name = dictionary["name"] as? String, let toLetAt = dictionary["toLetAt"] as? String, let gpsPosDic = dictionary["gpsPosition"] as? [String : Any] else {
            return nil
        }
        let gpsPos = GPSPosition.decode(dictionary: gpsPosDic)
        guard let toLetAtType = ToLetAt(rawValue: toLetAt) else { return nil }
        if let type = dictionary["type"] as? Int { // SARA  Regatta
            return Buoy(id: id, name: name, gpsPosition: gpsPos, toLetAt: toLetAtType, type: BuoyType(rawValue: type))
        } else { // SARA Nav
            return Buoy(id: id, name: name, gpsPosition: gpsPos, toLetAt: toLetAtType)
        }
    }
}
