//
//  CoursePoint.swift
//  SARA Croisiere
//
//  Created by Rose on 27/07/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import CoreLocation
import FirebaseAuth

public class CoursePoint: PointRealm {
    
    @objc dynamic var gpsPosition: GPSPosition?
//    @objc dynamic var owner: String = ""
//    @objc dynamic var shared: Bool = false
    @objc dynamic var distanceThreshold: Float = 100.0
    
    convenience init(name: String, distanceThreshold: Float = -1, gpsPosition: GPSPosition?) {
        
        if distanceThreshold < 0 {
            self.init(id: UUID().uuidString, name: name, distanceThreshold: Settings.shared.minimumDistanceThreshold, gpsPosition: gpsPosition)
        } else {
            self.init(id: UUID().uuidString, name: name, distanceThreshold: distanceThreshold, gpsPosition: gpsPosition)
        }
        
    }
    
    convenience init(id: String, name: String, distanceThreshold: Float, gpsPosition: GPSPosition?) {
        self.init()
        
        self.id = id
        self.name = name
        self.distanceThreshold = distanceThreshold
        self.gpsPosition = gpsPosition
    }
    
    override public static func primaryKey() -> String? {
        return "id"
    }
    
    func update(with point: CoursePoint, shared: Bool) {
        let realm = try! Realm()
        
        try! realm.write {
            self.name = point.name
            self.distanceThreshold = point.distanceThreshold
            if let oldGPSPos = self.gpsPosition {
                oldGPSPos.delete()
                self.gpsPosition = nil
            }
            if let gpsPos = point.gpsPosition {
                self.gpsPosition = gpsPos
                realm.add(gpsPos, update: .modified)
            }
        }
    }
    
    func save() {
        let realm = try! Realm()
        try! realm.write {
            if let gpsPos = self.gpsPosition {
                realm.add(gpsPos)
            }
            realm.add(self)
            realm.add(AnyPoint(self))
        }
    }
    
    func saveMOB() {
        let realm = try! Realm()
        // Check if a mob exists in the DB
        if let mob = realm.objects(CoursePoint.self).filter({ $0.distanceThreshold == 0 }).first {
            self.id = mob.id
            mob.update(with: self, shared: false)
        } else {
            self.save()
        }
    }
    
    func routesUsing() -> [Course] {
        let realm = try! Realm()
        let routes = realm.objects(Course.self).filter({ (route) in
            return route.points.map({ $0.point.id }).contains(self.id)
        })
        return Array(routes)
    }
        
    static func getPointNameAtIndex(_ indexPath: IndexPath) -> String? {
        let realm = try! Realm()
        let points = realm.objects(CoursePoint.self)
        if points.count > indexPath.row {
            return points[indexPath.row].name
        }
        return nil
    }
    
    static func getPointAtIndex(_ indexPath: IndexPath) -> CoursePoint? {
        let realm = try! Realm()
        let points = realm.objects(CoursePoint.self)
        if points.count > indexPath.row {
            return points[indexPath.row]
        }
        return nil
    }
    
    static func numberOfPoints() -> Int {
        let realm = try! Realm()
        return realm.objects(CoursePoint.self).count 
    }
    
    static func getSortedCoursePointsByProximity() -> [CoursePoint]? {
        let realm = try! Realm()
        let points = realm.objects(CoursePoint.self)
        guard let currentPosition = LocationManager.shared.getCurrentPosition() else {
            return Array(points)
        }
        return points.sorted(by: { (p1, p2) -> Bool in
            let d1 = currentPosition.distance(from: CLLocation(latitude: CLLocationDegrees(p1.gpsPosition?.latitude ?? 0), longitude: CLLocationDegrees(p1.gpsPosition?.longitude ?? 0)))
            let d2 = currentPosition.distance(from: CLLocation(latitude: CLLocationDegrees(p2.gpsPosition?.latitude ?? 0), longitude: CLLocationDegrees(p2.gpsPosition?.longitude ?? 0)))
            return d1 <= d2
        })
    }
    
    static func deletePoint(at index: IndexPath) {
        let realm = try! Realm()
        guard let point = CoursePoint.getPointAtIndex(index) else {
            return
        }
        try! realm.write {
            realm.delete(point)
        }
    }
    
    func deleteCoursePoint() {
        let realm = try! Realm()
        self.gpsPosition?.delete()
        realm.delete(self)
    }
    
    static func decodePoint(dictionary: [String: Any]) -> CoursePoint? {
        guard let id = dictionary["id"] as? String,
            let gpsPosDic = dictionary["gpsPosition"] as? [String : Any],
            let name = dictionary["name"] as? String,
            let distance = dictionary["distanceThreshold"] as? Float else {
                return nil
        }
        let gpsPos = GPSPosition.decode(dictionary: gpsPosDic)
        return CoursePoint(id: id, name: name, distanceThreshold: distance, gpsPosition: gpsPos)
    }
    
    static func all() -> [CoursePoint] {
        let realm = try! Realm()
        return Array(realm.objects(CoursePoint.self))
    }
    
    public override func duplicate(newUUID: Bool = false) -> PointRealm {
        return CoursePoint(id: newUUID ? UUID().uuidString : self.id, name: self.name, distanceThreshold: self.distanceThreshold, gpsPosition: self.gpsPosition?.duplicate(newUUID: newUUID))
    }
    
    // Convert coursePoint to mutable dictionary for firebase
    override func toSimpleDictionary() -> NSMutableDictionary {
        let mutabledic = NSMutableDictionary()
        
        var properties = self.objectSchema.properties.map { $0.name }
        
        if let index = properties.firstIndex(where: { $0 == "gpsPosition" }) {
            properties.remove(at: index)
            let gpsPosition = (self["gpsPosition"] as? GPSPosition)?.toDictionary()
            mutabledic["gpsPosition"] = gpsPosition
        }
        
        // And automatically create a dictionary with its values
        let dictionary = self.dictionaryWithValues(forKeys: properties)
        mutabledic.setValuesForKeys(dictionary)
        
        return mutabledic
    }
}
