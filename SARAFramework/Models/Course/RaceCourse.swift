//
//  RaceCourse.swift
//  SARA Croisiere
//
//  Created by Marine on 15.04.20.
//  Copyright © 2020 GraniteApps. All rights reserved.
//

import Foundation
import RealmSwift

// Define the enum
@objc public enum RaceCourseType: Int, RealmEnum, CaseIterable {
    case banane = 1
    case bananeDogleg = 2
    case bananeLeewardGate = 3
    case bananeDoglegLeewardGate = 4
    case matchRace = 5
    case trainingLoop = 6
    case trainingStart = 7
    case trainingFinish = 8
    
    public var description: String {
        switch self {
        case .banane:
            return Utils.localizedString(forKey: "banane", app: "SARARegate")
        case .bananeDogleg:
            return Utils.localizedString(forKey: "bananeDogleg", app: "SARARegate")
        case .bananeLeewardGate:
            return Utils.localizedString(forKey: "bananeLeewardGate", app: "SARARegate")
        case .bananeDoglegLeewardGate:
            return Utils.localizedString(forKey: "bananeDoglegLeewardGate", app: "SARARegate")
        case .matchRace:
            return Utils.localizedString(forKey: "matchRace", app: "SARARegate")
        case .trainingLoop:
            return Utils.localizedString(forKey: "trainingLoop", app: "SARARegate")
        case .trainingStart:
            return Utils.localizedString(forKey: "trainingStart", app: "SARARegate")
        case .trainingFinish:
            return Utils.localizedString(forKey: "trainingFinish", app: "SARARegate")
        default:
            return ""
        }
    }
    
    public var marksForType: [Mark] {
        switch self {
        case .banane:
            var marks: [Mark] = []
            marks.append(Mark.markModel(type: .start))
            marks.append(Mark.markModel(type: .windwardP))
            marks.append(Mark.markModel(type: .leewardP))
            marks.append(Mark.markModel(type: .finish))
            return marks
        case .bananeDogleg:
            var marks: [Mark] = []
            marks.append(Mark.markModel(type: .start))
            marks.append(Mark.markModel(type: .windwardP))
            marks.append(Mark.markModel(type: .offsetMark))
            marks.append(Mark.markModel(type: .leewardP))
            marks.append(Mark.markModel(type: .finish))
            return marks
        case .bananeLeewardGate:
            var marks: [Mark] = []
            marks.append(Mark.markModel(type: .start))
            marks.append(Mark.markModel(type: .windwardP))
            marks.append(Mark.markModel(type: .gate))
            marks.append(Mark.markModel(type: .finish))
            return marks
        case .bananeDoglegLeewardGate:
            var marks: [Mark] = []
            marks.append(Mark.markModel(type: .start))
            marks.append(Mark.markModel(type: .windwardP))
            marks.append(Mark.markModel(type: .offsetMark))
            marks.append(Mark.markModel(type: .gate))
            marks.append(Mark.markModel(type: .finish))
            return marks
        case .matchRace:
            var marks: [Mark] = []
            let start = Mark.markModel(type: .start, isMatchRace: true)
            let buoys = [start.buoys.last!, start.buoys.first!] // The finish line is the inversed of the start line
            let end = Mark(name: NSLocalizedString("end", bundle: Utils.bundle(anyClass: RaceCourse.self), comment: ""), markType: .end, buoys: buoys)
            marks.append(start)
            marks.append(Mark.markModel(type: .windwardS, isMatchRace: true))
//            marks.append(end)
            marks.append(Mark.markModel(type: .finish, isMatchRace: true))
            return marks
        case .trainingLoop:
            var marks: [Mark] = []
            marks.append(Mark.markModel(type: .windwardP))
            marks.append(Mark.markModel(type: .leewardP))
            return marks
        case .trainingStart:
            return [Mark.markModel(type: .start)]
        case .trainingFinish:
            var marks: [Mark] = []
            marks.append(Mark.markModel(type: .leewardP))
            marks.append(Mark.markModel(type: .finish))
            return marks
        }
        return []
    }
}

public class RaceCourse: Course {
    public var type = RealmOptional<RaceCourseType>()
    public var laps = RealmOptional<Int>()
    @objc dynamic var autoCreation: AutoCreationParams?
        
    convenience init(id: String, name: String, points: [AnyPoint], status: CourseStatus = .none, type: RaceCourseType?, laps: Int?, autoCreation: AutoCreationParams?) {
        self.init()
        
        self.id = id
        self.name = name
        self.status = status
        self.points.append(objectsIn: points)
        self.type = RealmOptional<RaceCourseType>(type)
        self.laps = RealmOptional<Int>(laps)
        self.autoCreation = autoCreation
    }
    
    public override func removeCourse() {
        self.autoCreation?.delete()
        super.removeCourse()
    }
    
    // Save course: change ids and add star
    func saveCourseStar(course: RaceCourse) {
        // Check if there is name with a start already. If not save with a star, otherwise add a new one
        var name = course.name
        if let number = course.getRealmCourseWithName(course: course) {
            for n in 0...(number - 1)  {
                name += "*"
            }
        } else {
            name += "*"
        }
        // Points
        let refBuoyId: String? = course.autoCreation?.refBuoy?.id
        var _points: [PointRealm] = []
        var _anyPoints: [AnyPoint] = []
        var refBuoy: Buoy?
        self.points.map({$0.point}).forEach({
            if let mark = $0 as? Mark {
                let duplicate = mark.duplicate(newUUID: true)
                if let _refBuoyId = refBuoyId, let index = mark.buoys.firstIndex(where: {$0.id == _refBuoyId}) {
                    refBuoy = (duplicate as? Mark)?.buoys[index].duplicate()
                }
                _points.append(duplicate)
                _anyPoints.append(AnyPoint(duplicate))
            } else if let waypoint = $0 as? CoursePoint {
                let duplicate = waypoint.duplicate(newUUID: true)
                _points.append(duplicate)
                _anyPoints.append(AnyPoint(duplicate))
            }
        })
        var autoCrea: AutoCreationParams?
        if let autoParams = course.autoCreation { // AutoCreation
            autoCrea = AutoCreationParams(windAxis: autoParams.windAxis, startLength: autoParams.startLength, startWindwardLength: autoParams.startWindwardLength, finishLength: autoParams.finishLength, leewardFinishLength: autoParams.leewardFinishLength, offsetMarkLength: autoParams.offsetMarkLength.value, refBuoy: refBuoy)
        }
        let rc = RaceCourse(id: UUID().uuidString, name: name, points: _anyPoints, status: .none, type: self.type.value, laps: self.laps.value, autoCreation: autoCrea)
        rc.saveCourse(realmPoint: _points)
    }
    
    func saveCourse(realmPoint: [PointRealm]) {
        let realm = try! Realm()
        
        try! realm.write {
            realm.add(self, update: .all)
            realm.add(AnyCourse(self), update: .all)
            // Save marks 
            realmPoint.forEach({
                if let m = $0 as? Mark {
                    m.buoys.forEach({
                        if let gpsPos = $0.gpsPosition {
                            realm.add(gpsPos, update: .all)
                        }
                        realm.add($0, update: .all)
                    })
                    realm.add(m, update: .all)
                }
            })
        }
    }
    
    static func decode(dictionary: [String : Any]) -> RaceCourse? {
        guard let (course, points) = Course.decode(dictionary: dictionary, status: .subscribed) else {
            return nil
        }
        
        var raceCourseType: RaceCourseType? = nil
        if let type = dictionary["type"] as? Int {
            raceCourseType = RaceCourseType(rawValue: type)
        }
        let laps = dictionary["laps"] as? Int
        var autoCreation: AutoCreationParams? = nil
        if let autoCreationDic = dictionary["autoCreation"] as? [String : Any] {
            autoCreation = AutoCreationParams.decode(dictionary: autoCreationDic)
        }
        // Save course in the database in order to use it later
        let raceCourse = RaceCourse(id: course.id, name: course.name, points: Array(course.points), status: course.status, type: raceCourseType, laps: laps, autoCreation: autoCreation)
        raceCourse.saveCourse(realmPoint: points)
        return raceCourse
    }
}
