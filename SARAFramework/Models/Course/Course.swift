//
//  Course.swift
//  SARA Croisiere
//
//  Created by Rose on 27/07/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import RxSwift
import CoreLocation

// Define the enum
@objc public enum CourseStatus: Int, RealmEnum, CaseIterable {
    case shared
    case subscribed
    case none
}

public class Course: Object {
    
    @objc public dynamic var id: String = ""
    @objc public dynamic var name: String = ""
    @objc public dynamic var status: CourseStatus = .none
    public let points: List<AnyPoint> = List<AnyPoint>()
    public var date: String?
        
    public convenience init(id: String, name: String, points: [AnyPoint], status: CourseStatus = .none) {
        self.init()
        self.id = id
        self.name = name
        self.status = status
        self.points.append(objectsIn: points)
    }
    
    func update(with route: Course, shared: Bool) {
        let realm = try! Realm()
        try! realm.write {
            self.name = route.name
            self.points.removeAll()
            self.status = route.status
            
            for anyPoint in route.points {
                if let coursePoint = anyPoint.point as? CoursePoint {
                    let newCoursePoint = realm.create(CoursePoint.self, value: coursePoint, update: .all)
                    realm.add(newCoursePoint, update: .all)
                    self.points.append(AnyPoint(newCoursePoint))
                } else if let mark = anyPoint.point as? Mark {
                    let newMark = realm.create(Mark.self, value: mark, update: .all)
                    realm.add(newMark, update: .all)
                    self.points.append(AnyPoint(newMark))
                } else {
                    fatalError("Unknow point")
                }
            }
            realm.add(self, update: .all)
        }
    }
    
    override public static func primaryKey() -> String? {
        return "id"
    }
    
    public static func getRealmCourse(courseId: String) -> Course? {
        let realm = try! Realm()
        let anyCourse = realm.objects(AnyCourse.self).first(where: {$0.primaryKey == courseId})
        if anyCourse?.typeName == "RaceCourse" {
            return realm.objects(RaceCourse.self).first(where: {$0.id == courseId})
        } else if anyCourse?.typeName == "CruiseCourse" {
            return realm.objects(CruiseCourse.self).first(where: {$0.id == courseId})
        }
        return nil
    }
    
    // Get number of course which name is equal without the stars
    func getRealmCourseWithName(course: Course) -> Int? {
        let realm = try! Realm()
        if let _ = course as? RaceCourse {
            return realm.objects(RaceCourse.self).filter({ $0.name.filter({ $0 != "*"}) == course.name}).count
        } else if let _ = course as? CruiseCourse {
            return realm.objects(CruiseCourse.self).filter({ $0.name.filter({ $0 != "*"}) == course.name }).count
        }
        return nil
    }
    
    static func getCourseNameAtIndex(_ indexPath: IndexPath) -> String? {
        let realm = try! Realm()
        let routes = realm.objects(AnyCourse.self)
        if routes.count > indexPath.row {
            return routes[indexPath.row].course.name
        }
        return nil
    }
    
    static func getCourseAtIndex(_ indexPath: IndexPath) -> Course? {
        let realm = try! Realm()
        let routes = realm.objects(AnyCourse.self)
        if routes.count > indexPath.row {
            return routes[indexPath.row].course
        }
        return nil
    }
    
    public static func all() -> [Course] {
        let realm = try! Realm()
        return Array(realm.objects(AnyCourse.self).map({$0.course}))
    }
    
    static func last() -> Course? {
        let realm = try! Realm()
        let routes = realm.objects(AnyCourse.self)
        return routes.last?.course
    }
    
    public static func allSortedByProximityFirstPoint() -> [Course] {
        guard let currentPosition = LocationManager.shared.getCurrentPosition() else {
            return self.all()
        }
        return self.all().sorted { (c1, c2) -> Bool in
            guard let p1 = c1.points.first?.point else {
                return false
            }
            guard let p2 = c2.points.first?.point else {
                return true
            }
            var p1GPSPosition: GPSPosition?
            if let coursePoint = p1 as? CoursePoint {
                p1GPSPosition = coursePoint.gpsPosition
            } else if let mark = p1 as? Mark {
                p1GPSPosition = mark.middlePoint()
            }
            guard p1GPSPosition != nil else {
                return false
            }
            var p2GPSPosition: GPSPosition?
            if let coursePoint = p2 as? CoursePoint {
                p2GPSPosition = coursePoint.gpsPosition
            } else if let mark = p2 as? Mark {
                p2GPSPosition = mark.middlePoint()
            }
            guard p2GPSPosition != nil else {
                return true
            }
            let d1 = currentPosition.distance(from: CLLocation(latitude: CLLocationDegrees(p1GPSPosition!.latitude), longitude: CLLocationDegrees(p1GPSPosition!.longitude)))
            let d2 = currentPosition.distance(from: CLLocation(latitude: CLLocationDegrees(p2GPSPosition!.latitude), longitude: CLLocationDegrees(p2GPSPosition!.longitude)))
            return d1 <= d2
        }
    }
    
    public func allPointsHasCoordinates() -> Bool {
        for anyPoint in points {
            if anyPoint.point.getCoordinates(method: .middle) == nil {
                return false
            }
        }
        return true
    }
    
    public func removeCourse() {
        if self == NavigationManager.shared.route.value {
            NavigationManager.shared.route.accept(nil)
        }
        let realm = try! Realm()
        try! realm.write {
            if let anyCourse = realm.objects(AnyCourse.self).filter({$0.primaryKey == self.id}).first {
                realm.delete(anyCourse)
            }
            realm.delete(self)
        }
    }
    
    // Remove a course with the following points. Check if the point is used in another route. If yes, keep it, otherwise delete it
    public func removeCourseWithPoints(points: [PointRealm]) -> [PointRealm] {
        var pointUsedAnotherRoute: [PointRealm] = []
        let realm = try! Realm()
        for point in NSOrderedSet(array: points) { // Use of a set to have uniq point in a route.
            if realm.objects(AnyCourse.self).filter({$0.primaryKey != self.id && $0.course.points.map({$0.point}).contains(point as! PointRealm)}).count == 0 { // Delete point
                (point as! PointRealm).deletePoint()
            } else { // Keep point
                pointUsedAnotherRoute.append(point as! PointRealm)
            }
        }
        self.removeCourse()
        return pointUsedAnotherRoute
    }
    
    public func removeCourseWithPoints() {
        let realm = try! Realm()
        for anyPoint in NSOrderedSet(array: Array(self.points)) { // get unique point
            if realm.objects(Course.self).filter({$0 != self && $0.points.contains(anyPoint as! AnyPoint)}).count == 0 {
                (anyPoint as! AnyPoint).point.deletePoint()
            }
        }
        self.removeCourse()
    }
    
    // Return true if the first mark is a start
    public func isStartFirstMark() -> Bool {
        if let mark = points.first?.point as? Mark, mark.markType == Mark.MarkType.start.rawValue {
            return true
        }
        return false
    }
    
    public func isMatchRace() -> Bool {
        if let race = self as? RaceCourse, let type = race.type.value, type == .matchRace {
            return true
        }
        return false
    }
    
    func unshare() {
        let realm = try! Realm()
        try! realm.write {
            self.status = .none
        }
    }
    
    // Decodes a realm Course from a NSDictionary from Firebase
    //
    // - Parameter dictionary: Firebase json
    // - Returns: Course?
    public static func decode(dictionary: [String: Any], shared: Bool) -> (Course, [PointRealm])? {
        guard let id = dictionary["id"] as? String,
            let name = dictionary["name"] as? String,
            let pointsDic = dictionary["points"] as? [[String : Any]] else {
                return nil
        }

        // Get the realm coursepoints
//        let points = pointsId.map({ CoursePoint.decode(dictionary: $0) }).compactMap({ $0 })
//        return Course(id: id, name: name, points: points)
        let points = pointsDic.map({PointRealm.decode(dictionary: $0)}).compactMap({$0})
        let anyPoints = points.map({AnyPoint($0)})
        return (Course(id: id, name: name, points: anyPoints), points)
    }
    
    // Decodes a realm Course from a NSDictionary from Firebase
    //
    // - Parameter dictionary: Firebase json
    // - Returns: Course?
    static func decode(dictionary: [String: Any], status: CourseStatus) -> (Course, [PointRealm])? {
        guard let id = dictionary["id"] as? String,
            let name = dictionary["name"] as? String,
            let pointsDic = dictionary["points"] as? [[String : Any]] else {
                return nil
        }

        // Get the realm coursepoints
//        let points = pointsId.map({ CoursePoint.decode(dictionary: $0) }).compactMap({ $0 })
//        return Course(id: id, name: name, points: points)
        let points = pointsDic.map({PointRealm.decode(dictionary: $0)}).compactMap({$0})
        let anyPoints = points.map({AnyPoint($0)})
        return (Course(id: id, name: name, points: anyPoints, status: status), points)
    }
        
    // Create dictionary with all needed properties of the course for firebase
    //
    // - Returns: NSDictionary to store on Firebase
    func toSimpleDictionary() -> NSDictionary {
        let mutabledic = NSMutableDictionary()
        
        var properties = self.objectSchema.properties.map { $0.name }
       
        // Get all properties except the points
        if let index = properties.firstIndex(where: { $0 == "points" }) {
            properties.remove(at: index)
            let pointsDic = Array(self.points).map({ $0.point.toSimpleDictionary() })
            mutabledic["points"] = pointsDic
        }
        
        if let index = properties.firstIndex(where: { $0 == "shared" }) {
            properties.remove(at: index)
        }
        
        if let index = properties.firstIndex(where: {$0 == "autoCreation"}) {
            properties.remove(at: index)
            let autoCreationDic = (self as? RaceCourse)?.autoCreation?.toDictionary()
            mutabledic["autoCreation"] = autoCreationDic
        }
        
        // And automatically create a dictionary with its values
        let dictionary = self.dictionaryWithValues(forKeys: properties)
        mutabledic.setValuesForKeys(dictionary)
        
        return mutabledic
    }
}

class AnyCourse: Object {
    @objc dynamic var typeName: String = ""
    @objc dynamic var primaryKey: String = ""

    // A list of all subclasses that this wrapper can store
    static let supportedClasses: [Course.Type] = [
        CruiseCourse.self,
        RaceCourse.self
    ]

    override public static func primaryKey() -> String? {
        return "primaryKey"
    }
    // Construct the type-erased point method from any supported subclass
    convenience init(_ course: Course) {
        self.init()
        typeName = String(describing: type(of: course))
        guard let primaryKeyName = type(of: course).primaryKey() else {
            fatalError("`\(typeName)` does not define a primary key")
        }
        guard let primaryKeyValue = course.value(forKey: primaryKeyName) as? String else {
            fatalError("`\(typeName)`'s primary key `\(primaryKeyName)` is not a `String`")
        }
        primaryKey = primaryKeyValue
    }

    // Dictionary to lookup subclass type from its name
    static let methodLookup: [String : Course.Type] = {
        var dict: [String : Course.Type] = [:]
        for course in supportedClasses {
            dict[String(describing: course)] = course
        }
        return dict
    }()

    // Use to access the *actual* Course value, using `as` to upcast
    var course: Course {
        guard let type = AnyCourse.methodLookup[typeName] else {
            fatalError("Unknown course method `\(typeName)`")
        }
        guard let course = try! Realm().object(ofType: type, forPrimaryKey: primaryKey) else {
            fatalError("`\(typeName)` with primary key `\(primaryKey)` does not exist")
        }
        return course
    }
}
