//
//  AutoCreationParams.swift
//  SARA Croisiere
//
//  Created by Marine on 15.04.20.
//  Copyright © 2020 GraniteApps. All rights reserved.
//

import Foundation
import RealmSwift

public class AutoCreationParams: Object {
    @objc public dynamic var id: String = ""
    @objc public dynamic var windAxis: Int = 180
    @objc public dynamic var startLength: Int = 200
    @objc public dynamic var startWindwardLength: Int = 700
    @objc public dynamic var finishLength: Int = 200
    @objc public dynamic var leewardFinishLength: Int = 400
    public var offsetMarkLength = RealmOptional<Int>()
    @objc public dynamic var refBuoy: Buoy?
    
    public override static func primaryKey() -> String? {
        return "id"
    }
            
    convenience init(id: String? = nil, windAxis: Int, startLength: Int, startWindwardLength: Int, finishLength: Int, leewardFinishLength: Int, offsetMarkLength: Int?, refBuoy: Buoy?) {
        self.init()
        if let safeId = id {
            self.id = safeId
        } else {
            self.id = UUID().uuidString
        }
        self.windAxis = windAxis
        self.startLength = startLength
        self.startWindwardLength = startWindwardLength
        self.finishLength = finishLength
        self.leewardFinishLength = leewardFinishLength
        self.offsetMarkLength.value = offsetMarkLength
        self.refBuoy = refBuoy
    }
    
    func delete() {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(self)
        }
    }
    
    // Decode from firebase
    static func decode(dictionary: [String: Any]) -> AutoCreationParams? {
        guard let id = dictionary["id"] as? String,
            let windAxis = dictionary["windAxis"] as? Int,
            let startLength = dictionary["startLength"] as? Int,
            let startWindwardLength = dictionary["startWindwardLength"] as? Int,
            let finishLength = dictionary["finishLength"] as? Int,
            let leewardFinishLength = dictionary["leewardFinishLength"] as? Int else {
            return nil
        }
        var refBuoy: Buoy? = nil
        if let refBuoyDic = dictionary["refBuoy"] as? [String : Any] { // TODO:
            refBuoy = Buoy.decode(dictionary: refBuoyDic)
        }
        return AutoCreationParams(id: id, windAxis: windAxis, startLength: startLength, startWindwardLength: startWindwardLength, finishLength: finishLength, leewardFinishLength: leewardFinishLength, offsetMarkLength: dictionary["offsetMarkLength"] as? Int, refBuoy: refBuoy)
    }
}
