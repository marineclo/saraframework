//
//  CruiseCourse.swift
//  SARA Croisiere
//
//  Created by Marine on 15.04.20.
//  Copyright © 2020 GraniteApps. All rights reserved.
//

import Foundation
import RealmSwift

public class CruiseCourse: Course {
    @objc public dynamic var reversed: Bool = false
    
    convenience init(id: String, name: String, points: [AnyPoint], status: CourseStatus = .none, reversedRoute: Bool = false) {
        self.init()
        
        self.id = id
        self.name = name
        self.status = status
        self.reversed = reversedRoute
        self.points.append(objectsIn: points)
    }
        
    public required convenience init(name: String, points: [AnyPoint]) {
        self.init(id: UUID().uuidString, name: name, points: points)
    }
    
    public static func decode(dictionary: [String : Any], save: Bool = false) -> CruiseCourse? {
        guard let (course, points) = Course.decode(dictionary: dictionary, shared: false) else {
            return nil
        }
        
        let reversed = dictionary["reversed"] as? Bool ?? false
        
        // Save course in the database in order to use it later
        let cruiseCourse = CruiseCourse(id: course.id, name: course.name, points: Array(course.points), reversedRoute: reversed)
        if save {
            cruiseCourse.saveCourse(realmPoint: points)
        }
        return cruiseCourse
    }
    
    func saveCourse(realmPoint: [PointRealm]) {
        let realm = try! Realm()
        
        try! realm.write {
            realm.add(self, update: .all)
            realm.add(AnyCourse(self), update: .all)
            // Save marks
            realmPoint.forEach({
                if let m = $0 as? Mark {
                    m.buoys.forEach({
                        if let gpsPos = $0.gpsPosition {
                            realm.add(gpsPos, update: .all)
                        }
                        realm.add($0, update: .all)
                    })
                    realm.add(m, update: .all)
                } else if let p = $0 as? CoursePoint {
                    if let gpsPos = p.gpsPosition {
                        realm.add(gpsPos, update: .all)
                    }
                    realm.add(p, update: .all)
                }
            })
        }
    }
}
