//
//  ValidationMarkData.swift
//  SARA Croisiere
//
//  Created by Marine on 02.06.20.
//  Copyright © 2020 GraniteApps. All rights reserved.
//

import Foundation
import CoreLocation
import UTMConversion

public class ValidationMarkData: NSObject {
    var course: Course?
    
    var A: (Double, Double)? // previous point coordinates
    var B: (Double, Double)? // current point coordinates
    var C: (Double, Double)? // next point coordinates
    var utmCoordB: UTMCoordinate?
    
    var bearingCurrent: Int?
    var bearingNext: Int?
    
    var APrime: (Double, Double)?
    var CPrime: (Double, Double)?
    
    var validationLineCoord: CLLocationCoordinate2D?
    
    var toLetAt: String?
    var isFavourableCase: Bool?
    
    let markBisectorLenght = 10000.0
    
    var theta: Double {
        guard let (xA, yA) = A, let (xB, yB) = B else {
            return 0
        }
        let xDiff = xA - xB
        let yDiff = yA - yB
        if yA < yB {
            return -acos( xDiff / sqrt((xDiff * xDiff) + (yDiff * yDiff )))
        } else {
            return acos( xDiff / sqrt((xDiff * xDiff) + (yDiff * yDiff )))
        }
    }
    
    var alpha: Double = 0.0
    
    init(posA: GPSPosition?, posB: GPSPosition?, posC: GPSPosition?, toLetAt: String? = nil, course: Course?) {
        super.init()
        guard let positionB = posB else {
            return
        }
        self.course = course
        
        B = convertToUTM(pos: positionB, ref: true)
        if posA != nil {
            A = convertToUTM(pos: posA!)
            APrime = changeCoordSystem(coordToChange: A!)
        }
        if posC != nil {
            C = convertToUTM(pos: posC!)
            CPrime = changeCoordSystem(coordToChange: C!)
        }
        self.toLetAt = toLetAt
        bearingCurrent = NavigationHelper.segmentOrientation(position1: posA, position2: posB)
        bearingNext = NavigationHelper.segmentOrientation(position1: posB, position2: posC)
        determineFavourableCase()
        calculateAlpha()
    }
    
    func convertToUTM(pos: GPSPosition, ref: Bool = false) -> (Double, Double) {
        // TODO: the point needs to be in the same zone
        let UTMCoord = CLLocation(latitude: CLLocationDegrees(pos.latitude), longitude: CLLocationDegrees(pos.longitude)).utmCoordinate()
        if ref {
            utmCoordB = UTMCoord
        }
        let x1 = UTMCoord.easting
        let y1 = UTMCoord.northing
        return (x1, y1)
    }
    
    func changeCoordSystem(coordToChange: (Double, Double)) -> (Double, Double){
        let (x, y) = coordToChange
        guard let (xB, yB) = B else {
            return (0,0)
        }
        let xP = (x - xB) * cos(theta) + (y - yB) * sin(theta)
        let yP = (y - yB) * cos(theta) - (x - xB) * sin(theta)
        return (xP, yP)
    }
    
    func currentCoordSystem(newCoordSystem: (Double, Double)) -> (Double, Double)? {
        guard let (xB, yB) = B else {
            return nil
        }
        let x = xB + newCoordSystem.0 * cos(theta) - (newCoordSystem.1 * sin(theta))
        let y = yB + newCoordSystem.0 * sin(theta) + (newCoordSystem.1 * cos(theta))
        return (x, y)
    }
    
    func calculateAlpha() {
        guard let (xA, yA) = A, let (xB, yB) = B, let (xC, yC) = C, let _ = isFavourableCase else {
            self.alpha = 0.0
            return
        }
        self.alpha = acos((((xA - xB) * (xC - xB)) + ((yA - yB) * (yC - yB))) / (sqrt(( (xA - xB) * (xA - xB)) + (yA - yB) * (yA - yB)) * sqrt(( (xC - xB) * (xC - xB)) + (yC - yB) * (yC - yB))))
//        if isFavourable && self.alpha < .pi {
//            self.alpha = 2 * .pi - self.alpha
//        }
    }
    
    func validationLine() {
        guard let (xPrimeA, yPrimeA) = APrime else {
            return
        }
        if let (xPrimeC, yPrimeC) = CPrime {
            if xPrimeC == xPrimeA && yPrimeC == yPrimeA { // BANANA CASE
                let xPBissectrice = -markBisectorLenght
                guard let (x, y) = currentCoordSystem(newCoordSystem: (xPBissectrice, 0.0)) else {
                    return
                }
                guard let utmCoord = utmCoordB else {
                    return
                }
                validationLineCoord = UTMCoordinate(northing: y, easting: x, zone: utmCoord.zone, hemisphere: utmCoord.hemisphere).location().coordinate
            } else { // BISECTOR CASE
                guard let isFavourableCase = self.isFavourableCase else {
                        return
                }
                let xPBissectrice = markBisectorLenght
                var (xToUse, yToUse) = (0.0, 0.0)
//                var d = tan(alpha / 2) * xPBissectrice
                
                if isFavourableCase {
                    if let raceCourse = course as? RaceCourse { // For SARA Race use external bissectrice
                        (xToUse, yToUse) = (-xPBissectrice, -tan(alpha / 2) * -xPBissectrice)
                    } else {
                        if toLetAt == Buoy.ToLetAt.port.rawValue { // external bissectrice
                            (xToUse, yToUse) = (-xPBissectrice, -tan(alpha / 2) * -xPBissectrice)
                        } else { // internal bissectrice
                            (xToUse, yToUse) = (xPBissectrice, -tan(alpha / 2) * xPBissectrice)
                        }
                    }
                } else if !isFavourableCase {
                    if let race = course as? RaceCourse { // For SARA Race use external bissectrice
                        (xToUse, yToUse) = (-xPBissectrice, tan(alpha / 2) * -xPBissectrice)
                    } else {
                        if toLetAt == Buoy.ToLetAt.starboard.rawValue { // external bissectrice
                            (xToUse, yToUse) = (-xPBissectrice, tan(alpha / 2) * -xPBissectrice)
                        } else { // internal bissectrice
                            (xToUse, yToUse) = (xPBissectrice, tan(alpha / 2) * xPBissectrice)
                        }
                    }
                }
                
                guard let (x, y) = currentCoordSystem(newCoordSystem: (xToUse, yToUse)) else {
                    return
                }
                guard let utmCoord = utmCoordB else {
                    return
                }
                validationLineCoord = UTMCoordinate(northing: y, easting: x, zone: utmCoord.zone, hemisphere: utmCoord.hemisphere).location().coordinate
            }
        } else { // Perpendicular
            guard let (_, yA) = A, let (_, yB) = B else {
                return
            }
            var yPBissectrice = markBisectorLenght
            if yB > yA && toLetAt == Buoy.ToLetAt.starboard.rawValue {
                yPBissectrice = -yPBissectrice
            }
            guard let (x, y) = currentCoordSystem(newCoordSystem: (0.0, yPBissectrice)) else {
                return
            }
            guard let utmCoord = utmCoordB else {
                return
            }
            
            validationLineCoord = UTMCoordinate(northing: y, easting: x, zone: utmCoord.zone, hemisphere: utmCoord.hemisphere).location().coordinate
        }
    }
    
    //
    // Favourable case: 180° < angle < 360°
    // Unfavourable case: 0° < angle < 90°
    func determineFavourableCase() {
        guard let bearingAB = self.bearingCurrent, let bearingBC = self.bearingNext else {
            return
        }
        
        var bearingBA = bearingAB + 180
        if bearingBA > 360 {
            bearingBA = bearingBA - 360
        }
        
        var isFavourable = false
        if bearingBA > bearingAB {
            // BA < BC < 360° ou 0 < BC < AB
            if (bearingBC >= bearingBA && bearingBC <= 360) || (bearingBC >= 0 && bearingBC <= bearingAB) {
                isFavourable = true
            }
        } else {
            // BA < BC < AB
            if bearingBC >= bearingBA && bearingBC <= bearingAB {
                isFavourable = true
            }
        }
        
        self.isFavourableCase = isFavourable
    }
}
