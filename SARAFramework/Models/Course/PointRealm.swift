//
//  Point.swift
//  SARA Croisiere
//
//  Created by Marine on 13.04.20.
//  Copyright © 2020 GraniteApps. All rights reserved.
//

import Foundation
import RealmSwift
import CoreLocation

public class PointRealm: Object {
    @objc public dynamic var id: String = ""
    @objc public dynamic var name: String = ""
    
    public enum PositionSelection {
        case middle
        case toLetAt
    }
    
    static func ==(lhs: PointRealm, rhs: PointRealm) -> Bool {
        return lhs.id == rhs.id
    }
    
    func deletePoint() {
        let realm = try! Realm()
        try! realm.write {
            if let anyPoint = realm.objects(AnyPoint.self).filter({$0.primaryKey == self.id}).first {
                realm.delete(anyPoint)
            }
            if let coursePoint = self as? CoursePoint {
                coursePoint.deleteCoursePoint()
            } else if let mark = self as? Mark {
                mark.deleteMark()
            }
        }
    }
    
    static func getSortedPointsByProximity() -> [PointRealm] {
        let realm = try! Realm()
        let points = realm.objects(AnyPoint.self)
        guard let currentPosition = LocationManager.shared.getCurrentPosition() else {
            return points.map({$0.point})
        }
        let sortedAnyPoint =  points.sorted(by: { (p1, p2) -> Bool in
            var p1GPSPosition: GPSPosition?
            if let coursePoint = p1.point as? CoursePoint {
                p1GPSPosition = coursePoint.gpsPosition
            } else if let mark = p1.point as? Mark {
                p1GPSPosition = mark.middlePoint()
            }
            guard p1GPSPosition != nil else {
                return false
            }
            var p2GPSPosition: GPSPosition?
            if let coursePoint = p2.point as? CoursePoint {
                p2GPSPosition = coursePoint.gpsPosition
            } else if let mark = p2.point as? Mark {
                p2GPSPosition = mark.middlePoint()
            }
            guard p2GPSPosition != nil else {
                return true
            }
            let d1 = currentPosition.distance(from: CLLocation(latitude: CLLocationDegrees(p1GPSPosition!.latitude), longitude: CLLocationDegrees(p1GPSPosition!.longitude)))
            let d2 = currentPosition.distance(from: CLLocation(latitude: CLLocationDegrees(p2GPSPosition!.latitude), longitude: CLLocationDegrees(p2GPSPosition!.longitude)))
            return d1 <= d2
        })
        return sortedAnyPoint.map({$0.point})
    }
    
    public func getCoordinates(method: PositionSelection, toLetAt: String? = nil, route: Course? = NavigationManager.shared.route.value, isMatchRace: Bool = false) -> GPSPosition? {
        if let mark = self as? Mark {
            if mark.markType == Mark.MarkType.simple.rawValue {
//                print(mark.buoys.first)
                return mark.buoys.first?.gpsPosition
            } else {
                switch method {
                case .middle:
                    return mark.buoys.first?.gpsPosition?.middlePosition(gpsP2: mark.buoys.last?.gpsPosition)
                case .toLetAt: // Select the point position: in case of a double mark, take the one that is to be left on the same side as B. Except for match race, if the double mark is the start, use the pin buoy (on the port)
//                    if isMatchRace && mark.markType == Mark.MarkType.start.rawValue {
//                        print(mark.buoys.first)
//                        return mark.buoys[0].gpsPosition
//                    }
                    for buoy in mark.buoys {
                        if buoy.toLetAt(inversed: (route as? CruiseCourse)?.reversed) == toLetAt {
                            return buoy.gpsPosition
                        }
                    }
                }
            }
        } else if let coursePoint = self as? CoursePoint {
            return coursePoint.gpsPosition
        }
        return nil
    }
    
    static func all() -> [PointRealm] {
        let realm = try! Realm()
        return Array(realm.objects(AnyPoint.self).map({$0.point}))
    }
    
    // Return all the points: use ThreadSafeReference for background task
    static func allThreadSafe() -> [ThreadSafeReference<PointRealm>] {
        let realm = try! Realm()
        return Array(realm.objects(AnyPoint.self).map({ThreadSafeReference(to:$0.point)}))
    }
    
    public func anyPoint() -> AnyPoint? {
        let realm = try! Realm()
        return realm.object(ofType: AnyPoint.self, forPrimaryKey: self.id)
    }
    
    func inARoute() -> Bool {
        guard let anyPoint = anyPoint() else {
            return false
        }
        for course in Course.all() {
            if course.points.contains(anyPoint) {
                return true
            }
        }
        return false
    }
    
    public func duplicate(newUUID: Bool = false) -> PointRealm {
        return PointRealm(value: self)
    }
    
    func toSimpleDictionary() -> NSMutableDictionary {
        return NSMutableDictionary()
    }
    
    static func decode(dictionary: [String: Any]) -> PointRealm? {
        guard let id = dictionary["id"] as? String, let name = dictionary["name"] as? String else {
            return nil
        }
        if let mark = dictionary["buoys"] as? [[String : Any]] { // Mark
            return Mark.decodeMark(dictionary: dictionary)
        } else if let waypoint = dictionary["distanceThreshold"] as? Float { // Waypoint
            return CoursePoint.decodePoint(dictionary: dictionary)
        }
        return nil
    }
    
    public func isStartPoint() -> Bool {
        if let mark = self as? Mark {
            return mark.markType == Mark.MarkType.start.rawValue
        }
        return false
    }
}

public class AnyPoint: Object {
    @objc dynamic var typeName: String = ""
    @objc public dynamic var primaryKey: String = ""

    // A list of all subclasses that this wrapper can store
    static let supportedClasses: [PointRealm.Type] = [
        CoursePoint.self,
        Mark.self
    ]

    // Construct the type-erased point method from any supported subclass
    convenience init(_ point: PointRealm) {
        self.init()
        typeName = String(describing: type(of: point))
        guard let primaryKeyName = type(of: point).primaryKey() else {
            fatalError("`\(typeName)` does not define a primary key")
        }
        guard let primaryKeyValue = point.value(forKey: primaryKeyName) as? String else {
            fatalError("`\(typeName)`'s primary key `\(primaryKeyName)` is not a `String`")
        }
        primaryKey = primaryKeyValue
    }
    
    override public static func primaryKey() -> String? {
        return "primaryKey"
    }

    // Dictionary to lookup subclass type from its name
    static let methodLookup: [String : PointRealm.Type] = {
        var dict: [String : PointRealm.Type] = [:]
        for point in supportedClasses {
            dict[String(describing: point)] = point
        }
        return dict
    }()

    // Use to access the *actual* Point value, using `as` to upcast
    public var point: PointRealm {
        guard let type = AnyPoint.methodLookup[typeName] else {
            fatalError("Unknown point method `\(typeName)`")
        }
        guard let value = try! Realm().object(ofType: type, forPrimaryKey: primaryKey) else {
            fatalError("`\(typeName)` with primary key `\(primaryKey)` does not exist")
        }
        return value
    }
}
