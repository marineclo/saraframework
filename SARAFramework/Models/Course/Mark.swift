//
//  Mark.swift
//  SARA Croisiere
//
//  Created by Marine on 13.04.20.
//  Copyright © 2020 GraniteApps. All rights reserved.
//

import Foundation
import RealmSwift
import CoreLocation

public enum MarkModel: Int {
    case windwardP // windward port
    case windwardS // windward starboard
    case start
    case finish
    case leewardP // leeward port
    case leewardS // leeward starboard
    case offsetMark
    case gate
}

public class Mark: PointRealm {
    @objc dynamic var markType: String = ""
    public let buoys: List<Buoy> = List<Buoy>()
    
    convenience init(id: String? = nil, name: String, markType: MarkType, buoys: [Buoy]) {
        self.init()
        if let safeId = id {
            self.id = safeId
        } else {
            self.id = UUID().uuidString
        }
        self.name = name
        self.markType = markType.rawValue
        self.buoys.append(objectsIn: buoys)
    }
    
    public override static func primaryKey() -> String? {
        return "id"
    }
    
    public enum MarkType: String, CaseIterable {
        public typealias RawValue = String
        
        case simple = "SIMPLE"
        case double = "DOUBLE"
        case start = "START"
        case end = "END"
//        case gate = "GATE"
        
        var name: String {
            let bunle = Utils.bundle(anyClass: Mark.self)
            switch self {
            case .simple:
                return NSLocalizedString("simple", bundle: bunle, comment: "")
            case .double:
                return NSLocalizedString("double", bundle: bunle, comment: "")
            case .start:
                return NSLocalizedString("start", bundle: bunle, comment: "")
            case .end:
                return NSLocalizedString("end", bundle: bunle, comment: "")
            }
        }
    }
    
    func indexType() -> Int {
        switch markType {
        case MarkType.simple.rawValue:
            return 0
        case MarkType.double.rawValue:
            return 1
        case MarkType.start.rawValue:
            return 2
        case MarkType.end.rawValue:
            return 3
        default:
            return 0
        }
    }
    
    func middlePoint() -> GPSPosition? {
        guard buoys.count != 0 else {
            return nil
        }
        if buoys.count == 1 {
            return self.buoys.first?.gpsPosition
        } else {
            return self.buoys.first?.gpsPosition?.middlePosition(gpsP2: self.buoys[1].gpsPosition)
        }
    }
    
    static func getSortedMarksByProximity() -> [Mark]? {
        let realm = try! Realm()
        let marks = realm.objects(Mark.self)
        guard let currentPosition = LocationManager.shared.getCurrentPosition() else {
            return Array(marks)
        }
        return marks.sorted(by: { (m1, m2) -> Bool in
            let d1 = currentPosition.distance(from: CLLocation(latitude: CLLocationDegrees(m1.middlePoint()?.latitude ?? 0), longitude: CLLocationDegrees(m1.middlePoint()?.longitude ?? 0)))
            let d2 = currentPosition.distance(from: CLLocation(latitude: CLLocationDegrees(m2.middlePoint()?.latitude ?? 0), longitude: CLLocationDegrees(m2.middlePoint()?.longitude ?? 0)))
            return d1 <= d2
        })
    }
    
    func update(with mark: Mark, shared: Bool) {
        let realm = try! Realm()
        
        try! realm.write {
            self.name = mark.name
            self.markType = mark.markType
            // If there is less buoy, now it's a simple mark
            if self.buoys.count > mark.buoys.count {
                let buoyToDelete = self.buoys[1]
                if let gpsPos = buoyToDelete.gpsPosition {
                    realm.delete(gpsPos)
                }
                realm.delete(buoyToDelete)
            }
            
            self.buoys.removeAll()
            mark.buoys.forEach({
                if let gpsPos = $0.gpsPosition {
                    realm.add(gpsPos, update: .all)
                }
                realm.add($0, update: .all)
                self.buoys.append($0)
            })
        }
    }
    
    func save() {
       let realm = try! Realm()
        try! realm.write {
            self.buoys.forEach({
                if let gpsPos = $0.gpsPosition {
                    realm.add(gpsPos)
                }
                realm.add($0)
            })
            realm.add(self)
            realm.add(AnyPoint(self))
        }
    }
    
    func deleteMark() {
        let realm = try! Realm()
        self.buoys.forEach({
            $0.gpsPosition?.delete()
        })
        realm.delete(self.buoys)
        realm.delete(self)
    }
    
    public override func duplicate(newUUID: Bool = false) -> PointRealm {
        var buoys: [Buoy] = []
        self.buoys.forEach({
            buoys.append($0.duplicate(newUUID: newUUID))
        })
        return Mark(id: newUUID ? UUID().uuidString : self.id, name: self.name, markType: Mark.MarkType(rawValue: self.markType) ?? MarkType.simple, buoys: buoys)
    }
    
    // Retrun the type of the mark
    public func getMarkType() -> String {
        return self.markType
    }
    
    override func toSimpleDictionary() -> NSMutableDictionary {
        let mutabledic = NSMutableDictionary()
        
        var properties = self.objectSchema.properties.map { $0.name }
       
        // Get all properties except the buoys
        if let index = properties.firstIndex(where: { $0 == "buoys" }) {
            properties.remove(at: index)
            let buoysDic = Array(self.buoys).map({ $0.toDictionary()})
            mutabledic["buoys"] = buoysDic
        }
        
        // And automatically create a dictionary with its values
        let dictionary = self.dictionaryWithValues(forKeys: properties)
        mutabledic.setValuesForKeys(dictionary)
        
        return mutabledic
    }
    
    static func decodeMark(dictionary: [String : Any]) -> Mark? {
        let mark = Mark()
        guard let id = dictionary["id"] as? String, let name = dictionary["name"] as? String, let markType = dictionary["markType"] as? String, let buoysDic = dictionary["buoys"] as? [[String : Any]] else {
            return nil
        }
        guard let buoys = buoysDic.map({ Buoy.decode(dictionary: $0)}) as? [Buoy] else {
            return nil
        }
        guard let markTypeEnum = MarkType(rawValue: markType) else {
            return nil
        }
        return Mark(id: id, name: name, markType: markTypeEnum, buoys: buoys)
    }
    
    // MARK: - Mark models
    static public func markModel(type: MarkModel, isMatchRace: Bool = false) -> Mark{
        let bunle = Utils.bundle(anyClass: Mark.self)
        switch type {
        case .start:
            let nameBuoyPin = isMatchRace ? Utils.localizedString(forKey: "blue_buoy", app: "SARARegate") : NSLocalizedString("start_pin", bundle: bunle, comment: "")
            let buoyPin = Buoy(name: nameBuoyPin, gpsPosition: nil, toLetAt: .port, type: .startPin)
            let nameBuoyCommittee = isMatchRace ? Utils.localizedString(forKey: "yellow_buoy", app: "SARARegate") : NSLocalizedString("start_committee", bundle: bunle, comment: "")
            let buoyCommittee = Buoy(name: nameBuoyCommittee, gpsPosition: nil, toLetAt: .starboard, type: .startCommittee)
            let buoys: [Buoy] = [buoyPin, buoyCommittee]
            return Mark(name: NSLocalizedString("start", bundle: bunle, comment: ""), markType: .start, buoys: buoys)
        case .windwardP:
            let buoyLeeward = Buoy(name: NSLocalizedString("windward", bundle: bunle, comment: ""), gpsPosition: nil, toLetAt: .port, type: .windward)
            return Mark(name: NSLocalizedString("windward", bundle: bunle, comment: ""), markType: .simple, buoys: [buoyLeeward])
        case .leewardP:
            let buoyLeeward = Buoy(name: NSLocalizedString("leeward", bundle: bunle, comment: ""), gpsPosition: nil, toLetAt: .port, type: .leeward)
            return Mark(name: NSLocalizedString("leeward", bundle: bunle, comment: ""), markType: .simple, buoys: [buoyLeeward])
        case .windwardS:
            let nameWindward = isMatchRace ? Utils.localizedString(forKey: "red_buoy", app: "SARARegate") : NSLocalizedString("windward", bundle: bunle, comment: "")
            let buoyWindward = Buoy(name: nameWindward, gpsPosition: nil, toLetAt: .starboard, type: .windward)
            return Mark(name: nameWindward, markType: .simple, buoys: [buoyWindward])
        case .leewardS:
            let nameLeeward = isMatchRace ? Utils.localizedString(forKey: "blue_buoy", app: "SARARegate") : NSLocalizedString("leeward", bundle: bunle, comment: "")
            let buoyLeeward = Buoy(name: nameLeeward, gpsPosition: nil, toLetAt: .starboard, type: .leeward)
            return Mark(name: nameLeeward, markType: .simple, buoys: [buoyLeeward])
        case .finish:
            let nameBuoyPin = isMatchRace ? Utils.localizedString(forKey: "blue_buoy", app: "SARARegate") : NSLocalizedString("finish_pin", bundle: bunle, comment: "")
            let buoyPin = Buoy(name: nameBuoyPin, gpsPosition: nil, toLetAt: .starboard, type: .finishPin)
            let nameBuoyCommittee = isMatchRace ? Utils.localizedString(forKey: "yellow_buoy", app: "SARARegate") : NSLocalizedString("finish_committee", bundle: bunle, comment: "") 
            let buoyCommittee = Buoy(name: nameBuoyCommittee, gpsPosition: nil, toLetAt: .port, type: .finishCommittee)
            let buoys: [Buoy] = [buoyCommittee, buoyPin]
            return Mark(name: NSLocalizedString("end", bundle: bunle, comment: ""), markType: .end, buoys: buoys)
        case .offsetMark:
            let buoyOffset = Buoy(name: NSLocalizedString("offset_mark", bundle: bunle, comment: ""), gpsPosition: nil, toLetAt: .port, type: .offsetMark)
            return Mark(name: NSLocalizedString("offset_mark", bundle: bunle, comment: ""), markType: .simple, buoys: [buoyOffset])
        case .gate:
            let buoyPin = Buoy(name: NSLocalizedString("port_mark", bundle: bunle, comment: ""), gpsPosition: nil, toLetAt: .port, type: .gatePort)
            let buoyCommittee = Buoy(name: NSLocalizedString("starboard_mark", bundle: bunle, comment: ""), gpsPosition: nil, toLetAt: .starboard, type: .gateStartboard)
            let buoys: [Buoy] = [buoyPin, buoyCommittee]
            return Mark(name: NSLocalizedString("gate", bundle: bunle, comment: ""), markType: .double, buoys: buoys)
        }
    }
}
