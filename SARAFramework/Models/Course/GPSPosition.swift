//
//  GPSPosition.swift
//  SARA Croisiere
//
//  Created by Marine on 15.04.20.
//  Copyright © 2020 GraniteApps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftUI

public class GPSPosition: Object {
    @objc public dynamic var id: String = ""
    @objc public dynamic var latitude: Float = 0
    @objc public dynamic var longitude: Float = 0
    
    public override static func primaryKey() -> String? {
        return "id"
    }
    
    public convenience init(id: String? = nil, latitude: Float, longitude: Float) {
        self.init()
        if let safeId = id {
            self.id = safeId
        } else {
            self.id = UUID().uuidString
        }
        
        self.latitude = latitude
        self.longitude = longitude
    }
    
    func middlePosition(gpsP2: GPSPosition?) -> GPSPosition? {
        guard let gpsPos2 = gpsP2 else {
            return nil
        }
        let latitude = (self.latitude + gpsPos2.latitude) / 2
        let longitude = (self.longitude + gpsPos2.longitude) / 2
        return GPSPosition(id: "", latitude: latitude, longitude: longitude)
    }
    
    func delete() {
        let realm = try! Realm()
        realm.delete(self)
    }
    
    // Get coordinate moved from current to `distanceMeters` meters with azimuth `azimuth` [0, Double.pi)
    ///
    /// - Parameters:
    ///   - distanceMeters: the distance in meters
    ///   - azimuth: the azimuth (bearing)
    /// - Returns: new coordinate
    func shift(byDistance distanceMeters: Double, azimuth: Double) -> GPSPosition {
        let bearing = Float(azimuth)
        let origin = self
        let distRadians = Float(distanceMeters / (6372797.6)) // earth radius in meters
        
        let lat1 = origin.latitude * Float.pi / 180
        let lon1 = origin.longitude * Float.pi / 180
        
        let lat2 = asin(sin(lat1) * cos(distRadians) + cos(lat1) * sin(distRadians) * cos(bearing))
        let lon2 = lon1 + atan2(sin(bearing) * sin(distRadians) * cos(lat1), cos(distRadians) - sin(lat1) * sin(lat2))

        return GPSPosition(latitude: lat2 * 180 / Float.pi, longitude: lon2 * 180 / Float.pi)
        
    }
    
    func duplicate(newUUID: Bool = false) -> GPSPosition {
        return GPSPosition(id: newUUID ? UUID().uuidString : self.id, latitude: self.latitude, longitude: self.longitude)
    }
    
    // Decode from Firebase
    static func decode(dictionary: [String : Any]) -> GPSPosition? {
        guard let id = dictionary["id"] as? String, let latitude = dictionary["latitude"] as? Float, let longitude = dictionary["longitude"] as? Float else {
            return nil
        }
        return GPSPosition(id: id, latitude: latitude, longitude: longitude)
    }
}
