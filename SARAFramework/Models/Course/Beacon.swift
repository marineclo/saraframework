//
//  Beacon.swift
//  SARAFramework
//
//  Created by Marine on 31.05.21.
//  Copyright © 2021 GraniteApps. All rights reserved.
//

import Foundation
import RealmSwift
import CoreLocation

// Define the enum
@objc public enum BeaconType: Int, RealmEnum, CaseIterable {
    case portHandLateral = 1
    case starboardHandLateral = 2
    case northCardinal = 3
    case eastCardinal = 4
    case southCardinal = 5
    case westCardinal = 6
    case specialPurpose = 7
    case isolatedDanger = 8
    case safeWater = 9
    case lightSupport = 10
    case none = 11
    
    init?(string: String) {
        switch string {
        case "port-hand lateral mark":
            self = .portHandLateral
        case "starboard-hand lateral mark":
            self = .starboardHandLateral
        case "north cardinal mark":
            self = .northCardinal
        case "east cardinal mark":
            self = .eastCardinal
        case "south cardinal mark":
            self = .southCardinal
        case "west cardinal mark":
            self = .westCardinal
        case "special purpose/general":
            self = .specialPurpose
        case "isolated danger":
            self = .isolatedDanger
        case "safe water":
            self = .safeWater
        case "light support":
            self = .lightSupport
        default: return nil
        }
    }
    
    public var description: String {
        let bundle = Utils.bundle(anyClass: Beacon.self)
        switch self {
        case .portHandLateral:
            return NSLocalizedString("port_hand_lateral", bundle: bundle, comment: "")
        case .starboardHandLateral:
            return NSLocalizedString("starboard_hand_lateral", bundle: bundle, comment: "")
        case .northCardinal:
            return NSLocalizedString("north_cardinal", bundle: bundle, comment: "")
        case .eastCardinal:
            return NSLocalizedString("east_cardinal", bundle: bundle, comment: "")
        case .southCardinal:
            return NSLocalizedString("south_cardinal", bundle: bundle, comment: "")
        case .westCardinal:
            return NSLocalizedString("west_cardinal", bundle: bundle, comment: "")
        case .specialPurpose:
            return NSLocalizedString("special_purpose", bundle: bundle, comment: "")
        case .isolatedDanger:
            return NSLocalizedString("isolated_danger", bundle: bundle, comment: "")
        case .safeWater:
            return NSLocalizedString("safe_water", bundle: bundle, comment: "")
        case .lightSupport:
            return NSLocalizedString("light_support", bundle: bundle, comment: "")
        default:
            return ""
        }
    }
}

class Beacon: Object {
    @objc dynamic var id: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var gpsPosition: GPSPosition?
    @objc dynamic var type: BeaconType = .none
    public let downloadedZoneListId: List<String> = List<String>()
    
    override public static func primaryKey() -> String? {
        return "id"
    }
    
    func delete() {
        let realm = try! Realm()
        self.gpsPosition?.delete()
        realm.delete(self)
    }
    
    convenience init(id: String, name: String, gpsCoord: [Double], type: BeaconType, downloadedZoneId: [String]) {
        self.init()
        self.id = id
        self.name = name
        self.gpsPosition = GPSPosition(latitude: Float(gpsCoord[1]), longitude: Float(gpsCoord[0]))
        self.type = type
        self.downloadedZoneListId.append(objectsIn: downloadedZoneId)
    }
    
    convenience init(id: String, name: String, gpsPos: GPSPosition, type: BeaconType, downloadedZoneId: List<String>) {
        self.init()
        self.id = id
        self.name = name
        self.gpsPosition = GPSPosition(id: gpsPos.id, latitude: gpsPos.latitude, longitude: gpsPos.longitude)
        self.type = type
        self.downloadedZoneListId.append(objectsIn: downloadedZoneId)
    }
    
    public static func beaconWithId(id: String) -> Beacon? {
        let realm = try! Realm()
        if let b = realm.object(ofType: Beacon.self, forPrimaryKey: id) {
            return Beacon(id: b.id, name: b.name, gpsPos: b.gpsPosition!, type: b.type, downloadedZoneId: b.downloadedZoneListId)
        }
        return nil
    }
    
    func update() {
        let realm = try! Realm()
        try! realm.write {
            realm.add(self, update: .modified)
        }
    }
    
    func save() {
        let realm = try! Realm()
        try! realm.write {
            if self.gpsPosition != nil {
                realm.add(self.gpsPosition!)
            }
            realm.add(self)
        }
    }
    
    func distance(currentLoc: CLLocation) -> Float? {
        return NavigationHelper.getDistanceFrom(location: currentLoc, to: self.gpsPosition)
    }
    
    func bearing(currentLoc: CLLocation) -> [(Int, NavigationUnit?)?]? {
        return NavigationHelper.getBearingFrom(gpsPosition: self.gpsPosition, location: currentLoc)
    }
    
    static func getSortedBeaconsByProximity() -> [Beacon] {
        let realm = try! Realm()
        let beacons = realm.objects(Beacon.self)
        guard let currentPosition = LocationManager.shared.getCurrentPosition() else {
            return Array(beacons)
        }
        let sortedBeacons = beacons.sorted(by: { (b1, b2) -> Bool in
            guard let b1GPSPosition = b1.gpsPosition else {
                return false
            }
            guard let b2GPSPosition = b2.gpsPosition else {
                return false
            }
            let d1 = currentPosition.distance(from: CLLocation(latitude: CLLocationDegrees(b1GPSPosition.latitude), longitude: CLLocationDegrees(b1GPSPosition.longitude)))
            let d2 = currentPosition.distance(from: CLLocation(latitude: CLLocationDegrees(b2GPSPosition.latitude), longitude: CLLocationDegrees(b2GPSPosition.longitude)))
            return d1 <= d2
        })
        return Array(sortedBeacons.filter({$0.name != nil}))
    }
    
    static func all() -> [Beacon] {
        let realm = try! Realm()
        return Array(realm.objects(Beacon.self))
    }
    
    // Return all the beacons: use ThreadSafeReference for background task
    static func allThreadSafe() -> [ThreadSafeReference<Beacon>] {
        let realm = try! Realm()
        return Array(realm.objects(Beacon.self).map({ThreadSafeReference(to: $0)}))
    }
    
    static func aroundBeaconsFront() -> [Beacon] {
        let beacons = Beacon.all().filter({$0.name != nil})
        guard let currentPosition = LocationManager.shared.getCurrentPosition() else {
            return []
        }
        var inFrontBeacons = beacons.filter({ b in
            guard let bearing = b.bearing(currentLoc: currentPosition), let gisement = bearing[1] else {
                return false
            }
            if gisement.0 <= 100 {
                return true
            }
            return false
        })
        var inFront = inFrontBeacons.sorted(by: { (b1, b2) -> Bool in
            guard let d1 = b1.distance(currentLoc: currentPosition) else {
                return false
            }
            guard let d2 = b2.distance(currentLoc: currentPosition) else {
                return false
            }
            return d1 <= d2
        })
        if inFront.count > 7 {
            inFront = Array(inFront[...5])
        }
        
        return inFront
    }
    
    static func aroundBeaconsBehind() -> [Beacon] {
        let beacons = Beacon.all().filter({$0.name != nil})
        guard let currentPosition = LocationManager.shared.getCurrentPosition() else {
            return []
        }
        var behindBeacons = beacons.filter({ b in
            guard let bearing = b.bearing(currentLoc: currentPosition), let gisement = bearing[1] else {
                return false
            }
            if gisement.0 > 100 {
                return true
            }
            return false
        })
        var behind = behindBeacons.sorted(by: { (b1, b2) -> Bool in
            guard let d1 = b1.distance(currentLoc: currentPosition) else {
                return false
            }
            guard let d2 = b2.distance(currentLoc: currentPosition) else {
                return false
            }
            return d1 <= d2
        })
        if behind.count > 3 {
            behind = Array(behind[...1])
        }
        return behind
    }
}
