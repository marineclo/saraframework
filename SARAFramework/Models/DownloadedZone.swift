//
//  downloadedZone.swift
//  SARAFramework
//
//  Created by Marine on 31.05.21.
//

import Foundation
import RealmSwift

class DownloadedZone: Object {
    // MARK: - Variables
    @objc dynamic var id: String = ""
    @objc public dynamic var name: String = ""
    public let beacons: List<Beacon> = List<Beacon>()
        
    public convenience init(id: String, name: String, beacons: [Beacon]) {
        self.init()
        self.id = id
        self.name = name
//        beacons.forEach({
//            self.beacons.append(Beacon(id: $0.id, name: $0.name, gpsPos: $0.gpsPosition!, type: $0.type, downloadedZoneId: $0.downloadedZoneListId))
//        })
        self.beacons.append(objectsIn: beacons)
    }
    
    override public static func primaryKey() -> String? {
        return "id"
    }
    
    func delete() {
        let realm = try! Realm()
        try! realm.write {
            for beacon in self.beacons { // get all beacons
                if let index = beacon.downloadedZoneListId.index(of: self.id) { // remove the id of downloadedzone to delete
                    beacon.downloadedZoneListId.remove(at: index)
                }
                if beacon.downloadedZoneListId.count == 0 { // if there is no downloaded zone, delete the beacon
                    beacon.delete()
                }
            }
            realm.delete(self)
        }
    }
    
    func save() {
        let realm = try! Realm()
        try! realm.write {
            self.beacons.forEach({
                if $0.downloadedZoneListId.count > 1 {
                    realm.add($0, update: .modified)
                } else {
                    if $0.gpsPosition != nil {
                        realm.add($0.gpsPosition!)
                    }
                    realm.add($0)
                }
            })
            realm.add(self, update: .modified)
        }
    }
    
    static func all(in realm: Realm = try! Realm()) -> [DownloadedZone] {
      return Array(realm.objects(DownloadedZone.self))
    }
    
    static func deleteAll() {
        let realm = try! Realm()
        let allDownloadedZones = realm.objects(DownloadedZone.self)
        let allBeacons = realm.objects(Beacon.self)

        try! realm.write {
            allBeacons.forEach({
                if let gpsPos = $0.gpsPosition {
                    realm.delete(gpsPos)
                }
            })
            realm.delete(allBeacons)
            realm.delete(allDownloadedZones)
        }
    }
}
