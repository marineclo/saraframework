//
//  MapAnnotation.swift
//  SARA Croisiere
//
//  Created by Marine IL on 13.03.19.
//  Copyright © 2019 Rose. All rights reserved.
//

import UIKit
import MapKit

public class MapAnnotation: NSObject, MKAnnotation {
    public var title: String?
    public var subtitle: String?
    var latitude: Double
    var longitude:Double
    var type: MapAnnotationType?
    var isCurrent: Bool
    
    public enum MapAnnotationType: String {
        case coursePoint
        case mark
        case userLocation
        case mob
        case beacon
        case starboard
        case port
        case south
        case north
        case east
        case west
        case danger
        case special
        case lightSupport
        case safeWater
    }
    
    public var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    public init(latitude: Double, longitude: Double, type: MapAnnotationType?, isCurrent: Bool = false) {
        self.latitude = latitude
        self.longitude = longitude
        self.type = type
        self.isCurrent = isCurrent
    }
}
