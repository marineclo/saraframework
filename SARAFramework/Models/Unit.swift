//
//  NavigationUnit.swift
//  SARA Croisiere
//
//  Created by Rose on 05/01/2018.
//  Copyright © 2018 Rose. All rights reserved.
//

import Foundation
import RealmSwift

public enum UnitType: Int {
    case distance
    case speed
    case coordinates
    
    func availables() -> [NavigationUnit] {
        switch self {
        case .distance:
            return [.mile, .meter]
        case .speed:
            return [.kmPerHour, .knots]
        case .coordinates:
            return [.decimalDegrees, .minuteDecimalDegrees, .degreesMinuteSecond]
        }
    }
    
    func description() -> String {
        let bundle = NavigationManager.shared.bundle
        switch self {
        case .distance:
            return NSLocalizedString("distance", bundle: bundle, comment: "")
        case .speed:
            return NSLocalizedString("unit_speed", bundle: bundle, comment: "")
        case .coordinates:
            return NSLocalizedString("coordinates", bundle: bundle, comment: "")
        }
    }
    
    static func all() -> [UnitType] {
        return [.distance, .speed, .coordinates]
    }
}

public enum NavigationUnit: String {
    
    case meter // default
    case mile
    case kiloMeter
    case knots // default
    case kmPerHour
    case degrees
    case spDegrees
    case timeDegrees
    case decimalDegrees
    case minuteDecimalDegrees
    case degreesMinuteSecond
    case starboard
    case port
    case second
    case percent
    case north
    case south
    case east
    case west
    case none
    
    var abbreviation: String {
        let bundle = NavigationManager.shared.bundle
        switch self {
        case .mile:
            return NSLocalizedString("unit_abb_mile", bundle: bundle, comment: "")
        case .kiloMeter:
            return NSLocalizedString("unit_abb_km", bundle: bundle, comment: "")
        case .meter:
            return NSLocalizedString("unit_abb_meter", bundle: bundle, comment: "")
        case .knots:
            return NSLocalizedString("unit_abb_knots", bundle: bundle, comment: "")
        case .kmPerHour:
            return NSLocalizedString("unit_abb_km_h", bundle: bundle, comment: "")
        case .degrees:
            return "°"
        case .decimalDegrees:
            return NSLocalizedString("unit_abb_dd", bundle: bundle, comment: "")
        case .minuteDecimalDegrees:
            return NSLocalizedString("unit_abb_dmd", bundle: bundle, comment: "")
        case .degreesMinuteSecond:
            return NSLocalizedString("unit_abb_dms", bundle: bundle, comment: "")
        case .spDegrees:
            return ""
        case .starboard:
            return NSLocalizedString("unit_abb_starboard", bundle: bundle, comment: "")
        case .port:
            return NSLocalizedString("unit_abb_port", bundle: bundle, comment: "")
        case .timeDegrees:
            return NSLocalizedString("unit_abb_hour", bundle: bundle, comment: "")
        case .second:
            return NSLocalizedString("unit_abb_second", bundle: bundle, comment: "")
        case .percent:
            return "%"
        case .north:
            return NSLocalizedString("north_initial", bundle: bundle, comment: "")
        case .south:
            return NSLocalizedString("south_initial", bundle: bundle, comment: "")
        case .east:
            return NSLocalizedString("east_initial", bundle: bundle, comment: "")
        case .west:
            return NSLocalizedString("west_initial", bundle: bundle, comment: "")
        default:
            return ""
        }
    }
    
    var description: String {
        let bundle = NavigationManager.shared.bundle
        switch self {
        case .mile:
            return NSLocalizedString("unit_mile", bundle: bundle, comment: "")
        case .meter:
            return NSLocalizedString("unit_meter", bundle: bundle, comment: "")
        case .kiloMeter:
            return NSLocalizedString("unit_km", bundle: bundle, comment: "")
        case .knots:
            return NSLocalizedString("unit_knots", bundle: bundle, comment: "")
        case .kmPerHour:
            return NSLocalizedString("unit_km_h", bundle: bundle, comment: "")
        case .degrees:
            return NSLocalizedString("degrees", bundle: bundle, comment: "")
        case .decimalDegrees:
            return NSLocalizedString("unit_decimal_degrees", bundle: bundle, comment: "")
        case .minuteDecimalDegrees:
            return NSLocalizedString("unit_dcm", bundle: bundle, comment: "")
        case .degreesMinuteSecond:
            return NSLocalizedString("unit_dms", bundle: bundle, comment: "")
        case .spDegrees:
            return ""
        case .starboard:
            return NSLocalizedString("unit_starboard", bundle: bundle, comment: "")
        case .port:
            return NSLocalizedString("unit_port", bundle: bundle, comment: "")
        case .timeDegrees:
            return NSLocalizedString("unit_hour", bundle: bundle, comment: "")
        case .second:
            return NSLocalizedString("unit_second", bundle: bundle, comment: "")
        case .percent:
            return NSLocalizedString("unit_percent", bundle: bundle, comment: "")
        case .north:
            return NSLocalizedString("north", bundle: bundle, comment: "")
        case .south:
            return NSLocalizedString("south", bundle: bundle, comment: "")
        case .east:
            return NSLocalizedString("east", bundle: bundle, comment: "")
        case .west:
            return NSLocalizedString("west", bundle: bundle, comment: "")
        default:
            return ""
        }
    }
}

public class NavigationUnitRealm: Object {
    @objc dynamic var rawUnit: String = ""
    public var unit: NavigationUnit {
        if let a = NavigationUnit(rawValue: rawUnit) {
            return a
        }
        return .degrees
    }
    
    public convenience init(rawUnit: String) {
        self.init()
        self.rawUnit = rawUnit
    }
}
