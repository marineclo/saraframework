//
//  Announce.swift
//  SARA Croisiere
//
//  Created by Rose on 30/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation

public struct Announce {
    var text: String
    let priority: Int
    let type: AnnonceType
    let annonceState: (Any?, Date?)?
    
    public enum AnnonceType: String {
        case debug
        case courseOver
        case changePoint
        case `default`
        case mute
        // GPS
        case compass
        case courseOverGround
        case speedOverGround
        case maxSpeedOverGround
        // Route
        case azimut
        case gisement
        case gisementTime
        case distance
        case CMG
        case azimut1
        case gisement1
        case gisementTime1
        case gisementCombine1
        case distance1
        case CMG1
        case azimut2
        case gisement2
        case gisementTime2
        case gisementCombine2
        case distance2
        case CMG2
        case gapSegment
        case entry3Lengths
        // Navigation System
        case depth
        case headingMagnetic
        case speedThroughWater
        case trueWindSpeed
        case trueWindAngle
        case apparentWindSpeed
        case apparentWindAngle
        case windDirection
        case waterTemperature
        case airTemperature
        // Telltales
        case tellTaleAngle
        case tellTaleMediane
        case tellTaleAverage
        case tellTaleStandardVariation
        case tellTaleVariationMax
        case tellTaleState
        // Voice Profile
        case batteryLevel
        // More data
        case meanCOG
        case maxSOG10s
        case meanSOG
        case gpsPrecision
        case latitude
        case longitude
        // Carto
        case timeAnnounce
        case detectionDistance
        case detectionNumber
        case azimutBeacon
        case distanceBeacon
        case gisementBeacon
        case gisementTimeBeacon
        case onlyFront
        case carto
        // Race
        case start
        case axisCrossed
        case track
        case matchRaceU
        case startLaunch
    }
    
    public init(text: String, priority: Int, type: AnnonceType, annonceState: (Any?, Date?)?) {
        self.text = text
        self.priority = priority
        self.type = type
        self.annonceState = annonceState
    }
}
