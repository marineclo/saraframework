//
//  User.swift
//  SARA Croisiere
//
//  Created by Rose on 02/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import RealmSwift
import FirebaseAuth
import RxCocoa

public class User {
    
    public static var shared: User = User()
    
    public var email = BehaviorRelay<String?>(value: nil)
    var password: String?
    
    var profile: Profile
    var ownedCourses: [Course]?
    
    init(email: String? = nil, password: String? = nil, ownedCourses: [Course]? = nil) {
        self.email.accept(email ?? Auth.auth().currentUser?.email)
//        self.email = email ?? Auth.auth().currentUser?.email
        self.password = password
        let realm = try! Realm()
        // Look for a selected profile in the DB
        if let selectedProfile = realm.objects(ProfileRealm.self).first(where: { $0.isSelected }) {
            self.profile = selectedProfile.profile
        } else {
            // If no profile was found, create one
            let firstProfile = Settings.shared.defaultProfile()
            firstProfile.name = NSLocalizedString("profile_default_name", bundle: Utils.bundle(anyClass: User.self), comment: "")
            self.profile = firstProfile
            profile.save(asSelected: true)
        }
        
        self.ownedCourses = ownedCourses
    }
    
    func retrieveInfos() {
        // retrieve infos from db and shared courses
    }
}
