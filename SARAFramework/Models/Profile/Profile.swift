//
//  Profile.swift
//  SARA Croisiere
//
//  Created by Rose on 02/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import RxSwift
import RealmSwift
import RxCocoa

public class Profile {
    
    var id: String
    var name: String
    let profileTypes: [ProfileType]
    
    init(id: String? = nil, name: String, profileTypes: [ProfileType]) {
        self.name = name
        if let safeId = id {
            self.id = safeId
        } else {
            self.id = UUID().uuidString
        }
        self.profileTypes = profileTypes
    }
        
    func save(asSelected: Bool = false) {
        let realm = try! Realm()
        //Profile exists: update
        if let profile = realm.object(ofType: ProfileRealm.self, forPrimaryKey: self.id) {
            profile.update(newProfile: self)
            if profile.isSelected { // if the profile is the one selected, reset announce
                User.shared.profile = self
                AnnouncementManager.shared.resetCurrentState()
            }
        } else {
            // Create new profile
            let newProfile = ProfileRealm(profile: self)
            try! realm.write {
                newProfile.isSelected = asSelected
                self.profileTypes.forEach({
                    if let pGPS = $0 as? ProfileGPS {
                        let pGPSRealm = ProfileGPSRealm(profileGPS: pGPS)
                        realm.add(pGPSRealm, update: .all)
                    } else if let pRoute = $0 as? ProfileRoute {
                        let pRouteRealm = ProfileRouteRealm(profileRoute: pRoute)
                        realm.add(pRouteRealm, update: .all)
                    } else if let pCentrale = $0 as? ProfileCentrale {
                        let pCentraleRealm = ProfileCentraleRealm(profileCentrale: pCentrale)
                        realm.add(pCentraleRealm, update: .all)
                    } else if let pTelltale = $0 as? ProfileTelltale {
                        let pTelltaleRealm = ProfileTelltaleRealm(profileTelltale: pTelltale)
                        realm.add(pTelltaleRealm, update: .all)
                    } else if let pVarious = $0 as? ProfileVarious {
                        let pVoiceRealm = ProfileVariousRealm(profileVarious: pVarious)
                        realm.add(pVoiceRealm, update: .all)
                    } else if let pCarto = $0 as? ProfileCarto {
                        let pCartoRealm = ProfileCartoRealm(profileCarto: pCarto)
                        realm.add(pCartoRealm, update: .all)
                    } else {
                        fatalError("Unknown Profile")
                    }
                })
                realm.add(newProfile, update: .all)
            }
        }
    }
    
    class func autoDistanceThreshold(for distance: Int) -> Int {
        if distance < 10 {
            return 1
        } else if distance < 50 && distance >= 10 {
            return 5
        } else if distance < 100 && distance >= 50 {
            return 10
        } else if distance < 500 && distance >= 100 {
            return 50
        } else if distance < 1_000 && distance >= 500 {
            return 100
        } else if distance < 5_000 && distance >= 1_000 {
            return 500
        } else if distance < 10_000 && distance >= 5_000 {
            return 1_000
        } else if distance < 50_000 && distance >= 10_000 {
            return 5_000
        } else {
            return 10_000
        }
    }
    
    class func autoDepthThreshold(for depth: Float) -> Int {
        if depth < 10 {
            return 1
        } else if depth < 20 && depth >= 10 {
            return 2
        } else if depth < 50 && depth >= 20 {
            return 5
        } else if depth < 100 && depth >= 50 {
            return 10
        } else {
            return 50
        }
    }
    
    static func getProfileAtIndex(_ indexPath: IndexPath) -> Profile? {
        return getProfileRealmAtIndex(indexPath)?.profile
    }
    
    static func getProfileRealmAtIndex(_ indexPath: IndexPath) -> ProfileRealm? {
        let realm = try! Realm()
        let profiles = realm.objects(ProfileRealm.self)
        if profiles.count > indexPath.row {
            return profiles[indexPath.row]
        }
        return nil
    }
}
