//
//  ProfileTypeRealm.swift
//  SARA_Framework
//
//  Created by Marine IL on 04.03.20.
//  Copyright © 2020 Marine. All rights reserved.
//

import UIKit
import RealmSwift
import RxCocoa
import RxSwift

public class AnyProfileTypeRealm: Object {
    @objc dynamic var typeName: String = ""
    @objc dynamic var primaryKey: String = ""

    // A list of all subclasses that this wrapper can store
    static let supportedClasses: [ProfileTypeRealm.Type] = [
        ProfileGPSRealm.self,
        ProfileCentraleRealm.self,
        ProfileRouteRealm.self,
        ProfileTelltaleRealm.self,
        ProfileVariousRealm.self,
        ProfileCartoRealm.self
    ]

    // Construct the type-erased payment method from any supported subclass
    convenience init(_ profile: ProfileTypeRealm) {
        self.init()
        typeName = String(describing: type(of: profile))
        guard let primaryKeyName = type(of: profile).primaryKey() else {
            fatalError("`\(typeName)` does not define a primary key")
        }
        guard let primaryKeyValue = profile.value(forKey: primaryKeyName) as? String else {
            fatalError("`\(typeName)`'s primary key `\(primaryKeyName)` is not a `String`")
        }
        primaryKey = primaryKeyValue
    }

    // Dictionary to lookup subclass type from its name
    static let methodLookup: [String : ProfileTypeRealm.Type] = {
        var dict: [String : ProfileTypeRealm.Type] = [:]
        for method in supportedClasses {
            dict[String(describing: method)] = method
        }
        return dict
    }()

    // Use to access the *actual* ProfileTypeRealm value, using `as` to upcast
    var value: ProfileTypeRealm {
        guard let type = AnyProfileTypeRealm.methodLookup[typeName] else {
            fatalError("Unknown payment method `\(typeName)`")
        }
        guard let value = try! Realm().object(ofType: type, forPrimaryKey: primaryKey) else {
            fatalError("`\(typeName)` with primary key `\(primaryKey)` does not exist")
        }
        return value
    }
}

// MARK: - ProfileType
public class ProfileTypeRealm: Object {
    @objc dynamic var id: String = ""
    
    func delete() {
        
    }
}

// MARK: - GPS
public class ProfileGPSRealm: ProfileTypeRealm {
    @objc dynamic var compass: ProfileSetting? // Compas
    @objc dynamic var courseOverGround: ProfileSetting? // Cap fond
    @objc dynamic var speedOverGround: ProfileSetting? // Vitesse fond
    @objc dynamic var maxSpeedOverGround: ProfileSetting? // Vitesse fond max
    
    public convenience init(id: String, compass: ProfileSetting? = nil, courseOverGround: ProfileSetting? = nil, speedOverGround: ProfileSetting? = nil, maxSpeedOverGround: ProfileSetting? = nil) {
        self.init()
        self.id = id
        self.compass = compass
        self.courseOverGround = courseOverGround
        self.speedOverGround = speedOverGround
        self.maxSpeedOverGround = maxSpeedOverGround
    }
    
    public convenience init(profileGPS: ProfileGPS) {
        self.init(id: profileGPS.id, compass: profileGPS.compass?.value, courseOverGround: profileGPS.courseOverGround?.value, speedOverGround: profileGPS.speedOverGround?.value, maxSpeedOverGround: profileGPS.maxSpeedOverGround?.value)
    }
    
    public override class func primaryKey() -> String? {
        return "id"
    }
    
    func update(profileGPS: ProfileGPS) {
        if profileGPS.compass != nil {
            self.compass?.update(newSetting: profileGPS.compass!.value)
        }
        if profileGPS.courseOverGround != nil {
            self.courseOverGround?.update(newSetting: profileGPS.courseOverGround!.value)
        }
        if profileGPS.speedOverGround != nil {
            self.speedOverGround?.update(newSetting: profileGPS.speedOverGround!.value)
        }
        if profileGPS.maxSpeedOverGround != nil {
            self.maxSpeedOverGround?.update(newSetting: profileGPS.maxSpeedOverGround!.value)
        }
    }
    
    override func delete() {
        let realm = try! Realm()
        if self.compass != nil {
            realm.delete(self.compass!)
        }
        if self.courseOverGround != nil {
            realm.delete(self.courseOverGround!)
        }
        if self.speedOverGround != nil {
            realm.delete(self.speedOverGround!)
        }
        if self.maxSpeedOverGround != nil {
            realm.delete(self.maxSpeedOverGround!)
        }
        realm.delete(self)
    }
}

// MARK: - Route
public class ProfileRouteRealm: ProfileTypeRealm {
    @objc dynamic var azimut: ProfileSetting?
    @objc dynamic var gisement: ProfileSetting?
    @objc dynamic var gisementTime: ProfileSetting?
    @objc dynamic var distance: ProfileSetting?
    @objc dynamic var cmg: ProfileSetting?
    @objc dynamic var distanceToSegment: ProfileSetting?
    @objc dynamic var entry3Lengths: ProfileSetting?
    
    public convenience init(id: String, azimut: ProfileSetting? = nil, gisement: ProfileSetting? = nil, gisementTime: ProfileSetting? = nil, distance: ProfileSetting? = nil, cmg: ProfileSetting? = nil, distanceToSegment: ProfileSetting? = nil, entry3Lengths: ProfileSetting? = nil) {
        self.init()
        self.id = id
        self.azimut = azimut
        self.gisement = gisement
        self.gisementTime = gisementTime
        self.distance = distance
        self.cmg = cmg
        self.distanceToSegment = distanceToSegment
        self.entry3Lengths = entry3Lengths
    }
    
    public convenience init(profileRoute: ProfileRoute) {
        self.init(id: profileRoute.id, azimut: profileRoute.azimut?.value, gisement: profileRoute.gisement?.value, gisementTime: profileRoute.gisementTime?.value, distance: profileRoute.distance?.value, cmg: profileRoute.cmg?.value, distanceToSegment: profileRoute.distanceToSegment?.value, entry3Lengths: profileRoute.entry3Lengths?.value)
    }
    
    public override class func primaryKey() -> String? {
        return "id"
    }
        
    func update(profileRoute: ProfileRoute) {
        if profileRoute.azimut != nil {
            self.azimut?.update(newSetting: profileRoute.azimut!.value)
        }
        if profileRoute.gisement != nil {
            self.gisement?.update(newSetting: profileRoute.gisement!.value)
        }
        if profileRoute.gisementTime != nil {
            self.gisementTime?.update(newSetting: profileRoute.gisementTime!.value)
        }
        if profileRoute.distance != nil {
            self.distance?.update(newSetting: profileRoute.distance!.value)
        }
        if profileRoute.cmg != nil {
            self.cmg?.update(newSetting: profileRoute.cmg!.value)
        }
        if profileRoute.distanceToSegment != nil {
            self.distanceToSegment?.update(newSetting: profileRoute.distanceToSegment!.value)
        }
        if profileRoute.entry3Lengths != nil {
            self.entry3Lengths?.update(newSetting: profileRoute.entry3Lengths!.value)
        }
    }
    
    override func delete() {
        let realm = try! Realm()
        if self.azimut != nil {
            realm.delete(self.azimut!)
        }
        if self.gisement != nil {
            realm.delete(self.gisement!)
        }
        if self.gisementTime != nil {
            realm.delete(self.gisementTime!)
        }
        if self.distance != nil {
            realm.delete(self.distance!)
        }
        if self.cmg != nil {
            realm.delete(self.cmg!)
        }
        if self.distanceToSegment != nil {
            realm.delete(self.distanceToSegment!)
        }
        if self.entry3Lengths != nil {
            realm.delete(self.entry3Lengths!)
        }
        realm.delete(self)
    }
    
    // Change automatic calculation of the distance for annoncement to a percentage of the distance
    public class func updateDistancePercent() {
        let realm = try! Realm()
        let profiles = realm.objects(ProfileRouteRealm.self)
        profiles.forEach({ profile in
            guard let distanceSetting = profile.distance else { return }
            if distanceSetting.threshold == 0.0 {
                try! realm.write {
                    distanceSetting.threshold = 20.0
                    realm.add(profile, update: .modified)
                }
            }
        })
    }
}

// MARK: - NavigationCenter
public class ProfileCentraleRealm: ProfileTypeRealm {
    @objc dynamic var depth: ProfileSetting?
    @objc dynamic var headingMagnetic: ProfileSetting?
    @objc dynamic var speedThroughWater: ProfileSetting?
    @objc dynamic var trueWindSpeed: ProfileSetting?
    @objc dynamic var trueWindAngle: ProfileSetting?
    @objc dynamic var apparentWindSpeed: ProfileSetting?
    @objc dynamic var apparentWindAngle: ProfileSetting?
    @objc dynamic var trueWindDirection: ProfileSetting?
    @objc dynamic var waterTemperature: ProfileSetting?
    @objc dynamic var airTemperature: ProfileSetting?
    
    public convenience init(id: String, depth: ProfileSetting?, headingMagnetic: ProfileSetting?, speedThroughWater: ProfileSetting?, trueWindSpeed: ProfileSetting?, trueWindAngle: ProfileSetting?, apparentWindSpeed: ProfileSetting?, apparentWindAngle: ProfileSetting?, trueWindDirection: ProfileSetting?, waterTemperature: ProfileSetting?, airTemperature: ProfileSetting?) {
        self.init()
        self.id = id
        self.depth = depth
        self.headingMagnetic = headingMagnetic
        self.speedThroughWater = speedThroughWater
        self.trueWindSpeed = trueWindSpeed
        self.trueWindAngle = trueWindAngle
        self.apparentWindSpeed = apparentWindSpeed
        self.apparentWindAngle = apparentWindAngle
        self.trueWindDirection = trueWindDirection
        self.waterTemperature = waterTemperature
        self.airTemperature = airTemperature
    }
    
    public convenience init(profileCentrale: ProfileCentrale) {
        self.init(id: profileCentrale.id, depth: profileCentrale.depth?.value, headingMagnetic: profileCentrale.headingMagnetic?.value, speedThroughWater: profileCentrale.speedThroughWater?.value, trueWindSpeed: profileCentrale.trueWindSpeed?.value, trueWindAngle: profileCentrale.trueWindAngle?.value, apparentWindSpeed: profileCentrale.apparentWindSpeed?.value, apparentWindAngle: profileCentrale.apparentWindAngle?.value, trueWindDirection: profileCentrale.trueWindDirection?.value, waterTemperature: profileCentrale.waterTemperature?.value, airTemperature: profileCentrale.airTemperature?.value)
    }
    
    public override class func primaryKey() -> String? {
        return "id"
    }
    
    func update(profileCentrale: ProfileCentrale) {
        if profileCentrale.depth != nil {
            self.depth?.update(newSetting: profileCentrale.depth!.value)
        }
        if profileCentrale.headingMagnetic != nil {
            self.headingMagnetic?.update(newSetting: profileCentrale.headingMagnetic!.value)
        }
        if profileCentrale.speedThroughWater != nil {
            self.speedThroughWater?.update(newSetting: profileCentrale.speedThroughWater!.value)
        }
        if profileCentrale.trueWindSpeed != nil {
            self.trueWindSpeed?.update(newSetting: profileCentrale.trueWindSpeed!.value)
        }
        if profileCentrale.trueWindAngle != nil {
            self.trueWindAngle?.update(newSetting: profileCentrale.trueWindAngle!.value)
        }
        if profileCentrale.apparentWindSpeed != nil {
            self.apparentWindSpeed?.update(newSetting: profileCentrale.apparentWindSpeed!.value)
        }
        if profileCentrale.apparentWindAngle != nil {
            self.apparentWindAngle?.update(newSetting: profileCentrale.apparentWindAngle!.value)
        }
        if profileCentrale.trueWindDirection != nil {
            self.trueWindDirection?.update(newSetting: profileCentrale.trueWindDirection!.value)
        }
        if profileCentrale.waterTemperature != nil {
            self.waterTemperature?.update(newSetting: profileCentrale.waterTemperature!.value)
        }
        if profileCentrale.airTemperature != nil {
            self.airTemperature?.update(newSetting: profileCentrale.airTemperature!.value)
        }
    }
    
    override func delete() {
        let realm = try! Realm()
        if self.depth != nil {
            realm.delete(self.depth!)
        }
        if self.headingMagnetic != nil {
            realm.delete(self.headingMagnetic!)
        }
        if self.speedThroughWater != nil {
            realm.delete(self.speedThroughWater!)
        }
        if self.trueWindSpeed != nil {
            realm.delete(self.trueWindSpeed!)
        }
        if self.trueWindAngle != nil {
            realm.delete(self.trueWindAngle!)
        }
        if self.apparentWindSpeed != nil {
            realm.delete(self.apparentWindSpeed!)
        }
        if self.apparentWindAngle != nil {
            realm.delete(self.apparentWindAngle!)
        }
        if self.trueWindDirection != nil {
            realm.delete(self.trueWindDirection!)
        }
        if self.waterTemperature != nil {
            realm.delete(self.waterTemperature!)
        }
        if self.airTemperature != nil {
            realm.delete(self.airTemperature!)
        }
        realm.delete(self)
    }
}

// MARK: - Telltale
public class ProfileTelltaleRealm: ProfileTypeRealm {
    // Telltales
    @objc dynamic var angle: ProfileSetting?
    @objc dynamic var mediane: ProfileSetting?
    @objc dynamic var average: ProfileSetting?
    @objc dynamic var standardDeviation: ProfileSetting?
    @objc dynamic var variationMax: ProfileSetting?
    @objc dynamic var state: ProfileSetting?
    
    public convenience init(id: String, angle: ProfileSetting, mediane: ProfileSetting, average: ProfileSetting, standardDeviation: ProfileSetting, variationMax: ProfileSetting, state: ProfileSetting) {
        self.init()
        self.id = id
        self.angle = angle
        self.mediane = mediane
        self.average = average
        self.standardDeviation = standardDeviation
        self.variationMax = variationMax
        self.state = state
    }
    
    public convenience init(profileTelltale: ProfileTelltale) {
        self.init(id: profileTelltale.id, angle: profileTelltale.angle.value, mediane: profileTelltale.mediane.value, average: profileTelltale.average.value, standardDeviation: profileTelltale.standardDeviation.value, variationMax: profileTelltale.variationMax.value,  state: profileTelltale.state.value)
    }
    
    public override class func primaryKey() -> String? {
        return "id"
    }

    func update(profileTelltale: ProfileTelltale) {
        self.angle?.update(newSetting: profileTelltale.angle.value)
        self.mediane?.update(newSetting: profileTelltale.mediane.value)
        self.average?.update(newSetting: profileTelltale.average.value)
        self.standardDeviation?.update(newSetting: profileTelltale.standardDeviation.value)
        self.variationMax?.update(newSetting: profileTelltale.variationMax.value)
        self.state?.update(newSetting: profileTelltale.state.value)
    }
    
    override func delete() {
        let realm = try! Realm()
        if self.angle != nil {
            realm.delete(self.angle!)
        }
        if self.mediane != nil {
            realm.delete(self.mediane!)
        }
        if self.average != nil {
            realm.delete(self.average!)
        }
        if self.standardDeviation != nil {
            realm.delete(self.standardDeviation!)
        }
        if self.variationMax != nil {
            realm.delete(self.variationMax!)
        }
        if self.state != nil {
            realm.delete(self.state!)
        }
        realm.delete(self)
    }
}

// MARK: - Various
public class ProfileVariousRealm: ProfileTypeRealm {
    @objc dynamic var vocalRate: Float = 0.5
    @objc dynamic var voice: String = ""
    // Battery level announces
    @objc dynamic var batteryLevel: ProfileSetting?
    
    public convenience init(id: String, voice: String, vocalRate: Float, batteryLevel: ProfileSetting) {
        self.init()
        self.id = id
        self.voice = voice
        self.vocalRate = vocalRate
        self.batteryLevel = batteryLevel
    }
    
    public convenience init(profileVarious: ProfileVarious) {
        self.init(id: profileVarious.id, voice: profileVarious.voice.value, vocalRate: profileVarious.vocalRate.value, batteryLevel: profileVarious.batteryLevel.value)
    }
    
    public override class func primaryKey() -> String? {
        return "id"
    }
    
    func update(profileVarious: ProfileVarious) {
        self.voice = profileVarious.voice.value
        self.vocalRate = profileVarious.vocalRate.value
        self.batteryLevel?.update(newSetting: profileVarious.batteryLevel.value)
    }
    
    override func delete() {
        let realm = try! Realm()
        if self.batteryLevel != nil {
            realm.delete(self.batteryLevel!)
        }
        realm.delete(self)
    }
}

// MARK: - Carto
public class ProfileCartoRealm: ProfileTypeRealm {
    @objc dynamic var enableBeacons: Bool = false
    @objc dynamic var timeAnnounce: ProfileSetting?
    @objc dynamic var detectionDistance: ProfileSetting?
    @objc dynamic var detectionNumber: ProfileSetting?
    @objc dynamic var azimut: ProfileSetting?
    @objc dynamic var gisement: ProfileSetting?
    @objc dynamic var gisementTime: ProfileSetting?
    @objc dynamic var distance: ProfileSetting?
    @objc dynamic var onlyFront: ProfileSetting?
    
    public convenience init(id: String, enableBeacons: Bool, timeAnnounce: ProfileSetting? = nil, detectionDistance: ProfileSetting? = nil, detectionNumber: ProfileSetting? = nil, distance: ProfileSetting? = nil, azimut: ProfileSetting? = nil, gisement: ProfileSetting? = nil, gisementTime: ProfileSetting? = nil, onlyFront: ProfileSetting? = nil) {
        self.init()
        self.id = id
        self.enableBeacons = enableBeacons
        self.timeAnnounce = timeAnnounce
        self.detectionDistance = detectionDistance
        self.detectionNumber = detectionNumber
        self.distance = distance
        self.azimut = azimut
        self.gisement = gisement
        self.gisementTime = gisementTime
        self.onlyFront = onlyFront
    }
    
    public convenience init(profileCarto: ProfileCarto) {
        self.init(id: profileCarto.id, enableBeacons: profileCarto.enableBeacons.value, timeAnnounce: profileCarto.timeAnnounce?.value, detectionDistance: profileCarto.detectionDistance?.value, detectionNumber: profileCarto.detectionNumber?.value, distance: profileCarto.distance?.value, azimut: profileCarto.azimut?.value, gisement: profileCarto.gisement?.value, gisementTime: profileCarto.gisementTime?.value, onlyFront: profileCarto.onlyFront?.value)
    }
    
    public override class func primaryKey() -> String? {
        return "id"
    }
        
    func update(profileCarto: ProfileCarto) {
        self.enableBeacons = profileCarto.enableBeacons.value
        if profileCarto.timeAnnounce != nil {
            self.timeAnnounce?.update(newSetting: profileCarto.timeAnnounce!.value)
        }
        if profileCarto.detectionDistance != nil {
            self.detectionDistance?.update(newSetting: profileCarto.detectionDistance!.value)
        }
        if profileCarto.detectionNumber != nil {
            self.detectionNumber?.update(newSetting: profileCarto.detectionNumber!.value)
        }
        if profileCarto.distance != nil {
            self.distance?.update(newSetting: profileCarto.distance!.value)
        }
        if profileCarto.azimut != nil {
            self.azimut?.update(newSetting: profileCarto.azimut!.value)
        }
        if profileCarto.gisement != nil {
            self.gisement?.update(newSetting: profileCarto.gisement!.value)
        }
        if profileCarto.gisementTime != nil {
            self.gisementTime?.update(newSetting: profileCarto.gisementTime!.value)
        }
        if profileCarto.onlyFront != nil {
            self.onlyFront?.update(newSetting: profileCarto.onlyFront!.value)
        }
    }
    
    override func delete() {
        let realm = try! Realm()
        if self.timeAnnounce != nil {
            realm.delete(self.timeAnnounce!)
        }
        if self.detectionDistance != nil {
            realm.delete(self.detectionDistance!)
        }
        if self.detectionNumber != nil {
            realm.delete(self.detectionNumber!)
        }
        if self.distance != nil {
            realm.delete(self.distance!)
        }
        if self.azimut != nil {
            realm.delete(self.azimut!)
        }
        if self.gisement != nil {
            realm.delete(self.gisement!)
        }
        if self.gisementTime != nil {
            realm.delete(self.gisementTime!)
        }
        if self.onlyFront != nil {
            realm.delete(self.onlyFront!)
        }
        realm.delete(self)
    }
}
