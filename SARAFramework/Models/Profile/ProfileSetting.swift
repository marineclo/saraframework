//
//  ProfileSetting.swift
//  SARA Croisiere
//
//  Created by Rose on 02/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import RealmSwift

public class ProfileSetting: Object {
    @objc dynamic var id: String = ""
    @objc dynamic var isActivated: Bool = false
    @objc dynamic var threshold: Float = 0
    @objc dynamic var timeThreshold: Int = 0
    
    public static let defaultProfileSettingValues = (false, Float(10.0), 10)
    public static let defaultProfileActivatedSettingValues = (true, Float(10.0), 10)
    public static let defaultSpeedProfileSettingValues = (false, Float(1.0), 10)
    public static let defaultSpeedProfileActivatedSettingValues = (true, Float(1.0), 10)
            
    public convenience init(id: String? = nil, isActivated: Bool, threshold: Float, timeThreshold: Int) {
        self.init()
        if let safeId = id {
            self.id = safeId
        } else {
            self.id = UUID().uuidString
        }
        self.isActivated = isActivated
        self.threshold = threshold
        self.timeThreshold = timeThreshold
    }
    
    public convenience init(defaultValues: (Bool, Float, Int)) {
        self.init()
        self.id = UUID().uuidString
        self.isActivated = defaultValues.0
        self.threshold = defaultValues.1
        self.timeThreshold = defaultValues.2
    }
    
    public func newCopy() -> ProfileSetting {
        return ProfileSetting(isActivated: self.isActivated, threshold: self.threshold, timeThreshold: self.timeThreshold)
    }
    
    public override class func primaryKey() -> String? {
        return "id"
    }
    
    public func update(newSetting: ProfileSetting) {
        self.isActivated = newSetting.isActivated
        self.threshold = newSetting.threshold
        self.timeThreshold = newSetting.timeThreshold
    }
    
    func save() {
        let realm = try! Realm()
        try! realm.write {
            realm.add(self)
        }
    }
}
