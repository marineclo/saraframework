//
//  ProfileType.swift
//  SARA_Framework
//
//  Created by Marine IL on 03.03.20.
//  Copyright © 2020 Marine. All rights reserved.
//

import UIKit
import RealmSwift
import RxCocoa
import RxSwift

public class ProfileType {
    var id: String = ""
}

// MARK: - GPS
public class ProfileGPS: ProfileType {
    var compass: BehaviorRelay<ProfileSetting>? // Compas
    var courseOverGround: BehaviorRelay<ProfileSetting>? // Cap fond
    var speedOverGround: BehaviorRelay<ProfileSetting>? // Vitesse max
    var maxSpeedOverGround: BehaviorRelay<ProfileSetting>? // Vitesse fond max
    
    public init(id: String? = nil, compass: ProfileSetting?, courseOverGround: ProfileSetting?, speedOverGround: ProfileSetting?, maxSpeedOverGround: ProfileSetting?) {
        super.init()
        if let safeId = id {
            self.id = safeId
        } else {
            self.id = UUID().uuidString
        }
        if compass != nil {
            self.compass = BehaviorRelay(value: compass!)
        }
        if courseOverGround != nil {
            self.courseOverGround = BehaviorRelay(value: courseOverGround!)
        }
        if speedOverGround != nil {
            self.speedOverGround = BehaviorRelay(value: speedOverGround!)
        }
        if maxSpeedOverGround != nil {
            self.maxSpeedOverGround = BehaviorRelay(value: maxSpeedOverGround!)
        }
    }
    
    public init(profileRealm: ProfileGPSRealm) {
        super.init()
        self.id = profileRealm.id
        if profileRealm.compass != nil {
            self.compass = BehaviorRelay(value: profileRealm.compass!)
        }
        if profileRealm.courseOverGround != nil {
            self.courseOverGround = BehaviorRelay(value: profileRealm.courseOverGround!)
        }
        if profileRealm.speedOverGround != nil {
            self.speedOverGround = BehaviorRelay(value: profileRealm.speedOverGround!)
        }
        if profileRealm.maxSpeedOverGround != nil {
            self.maxSpeedOverGround = BehaviorRelay(value: profileRealm.maxSpeedOverGround!)
        }
    }
}

// MARK: - Route
public class ProfileRoute: ProfileType {
    var azimut: BehaviorRelay<ProfileSetting>?
    var gisement: BehaviorRelay<ProfileSetting>?
    var gisementTime: BehaviorRelay<ProfileSetting>?
    var distance: BehaviorRelay<ProfileSetting>?
    var cmg: BehaviorRelay<ProfileSetting>?
    var distanceToSegment: BehaviorRelay<ProfileSetting>?
    var entry3Lengths: BehaviorRelay<ProfileSetting>?
    
    public init(id: String? = nil, azimut: ProfileSetting? = nil, gisement: ProfileSetting? = nil, gisementTime: ProfileSetting? = nil, distance: ProfileSetting? = nil, cmg: ProfileSetting? = nil, distanceToSegment: ProfileSetting? = nil, entry3Lengths: ProfileSetting? = nil) {
        super.init()
        if let safeId = id {
            self.id = safeId
        } else {
            self.id = UUID().uuidString
        }
        if azimut != nil {
            self.azimut = BehaviorRelay(value: azimut!)
        }
        if gisement != nil {
            self.gisement = BehaviorRelay(value: gisement!)
        }
        if gisementTime != nil {
            self.gisementTime = BehaviorRelay(value: gisementTime!)
        }
        if distance != nil {
            self.distance = BehaviorRelay(value: distance!)
        }
        if cmg != nil {
            self.cmg = BehaviorRelay(value: cmg!)
        }
        if distanceToSegment != nil {
            self.distanceToSegment = BehaviorRelay(value: distanceToSegment!)
        }
        if entry3Lengths != nil {
            self.entry3Lengths = BehaviorRelay(value: entry3Lengths!)
        }
    }
    
    public init(profileRealm: ProfileRouteRealm) {
        super.init()
        self.id = profileRealm.id
        if profileRealm.azimut != nil {
            self.azimut = BehaviorRelay(value: profileRealm.azimut!)
        }
        if profileRealm.gisement != nil {
            self.gisement = BehaviorRelay(value: profileRealm.gisement!)
        }
        if profileRealm.gisementTime != nil {
            self.gisementTime = BehaviorRelay(value: profileRealm.gisementTime!)
        }
        if profileRealm.distance != nil {
            self.distance = BehaviorRelay(value: profileRealm.distance!)
        }
        if profileRealm.cmg != nil {
            self.cmg = BehaviorRelay(value: profileRealm.cmg!)
        }
        if profileRealm.distanceToSegment != nil {
            self.distanceToSegment = BehaviorRelay(value: profileRealm.distanceToSegment!)
        }
        if profileRealm.entry3Lengths != nil {
            self.entry3Lengths = BehaviorRelay(value: profileRealm.entry3Lengths!)
        }
    }
}

// MARK: - NavigationCenter
public class ProfileCentrale: ProfileType {
    var depth: BehaviorRelay<ProfileSetting>?
    var headingMagnetic: BehaviorRelay<ProfileSetting>?
    var speedThroughWater: BehaviorRelay<ProfileSetting>?
    var trueWindSpeed: BehaviorRelay<ProfileSetting>?
    var trueWindAngle: BehaviorRelay<ProfileSetting>?
    var trueWindDirection: BehaviorRelay<ProfileSetting>?
    var apparentWindSpeed: BehaviorRelay<ProfileSetting>?
    var apparentWindAngle: BehaviorRelay<ProfileSetting>?
    var waterTemperature: BehaviorRelay<ProfileSetting>?
    var airTemperature: BehaviorRelay<ProfileSetting>?
    
    public init(id: String? = nil, depth: ProfileSetting?, headingMagnetic: ProfileSetting?, speedThroughWater: ProfileSetting?, trueWindSpeed: ProfileSetting?, trueWindAngle: ProfileSetting?, apparentWindSpeed: ProfileSetting?, apparentWindAngle: ProfileSetting?, trueWindDirection: ProfileSetting?, waterTemperature: ProfileSetting?, airTemperature: ProfileSetting?) {
        super.init()
        if let safeId = id {
            self.id = safeId
        } else {
            self.id = UUID().uuidString
        }
        if depth != nil {
            self.depth = BehaviorRelay(value: depth!)
        }
        if headingMagnetic != nil {
            self.headingMagnetic = BehaviorRelay(value: headingMagnetic!)
        }
        if speedThroughWater != nil {
            self.speedThroughWater = BehaviorRelay(value: speedThroughWater!)
        }
        if trueWindSpeed != nil {
            self.trueWindSpeed = BehaviorRelay(value: trueWindSpeed!)
        }
        if trueWindAngle != nil {
            self.trueWindAngle = BehaviorRelay(value: trueWindAngle!)
        }
        if apparentWindSpeed != nil {
            self.apparentWindSpeed = BehaviorRelay(value: apparentWindSpeed!)
        }
        if apparentWindAngle != nil {
            self.apparentWindAngle = BehaviorRelay(value: apparentWindAngle!)
        }
        if trueWindDirection != nil {
            self.trueWindDirection = BehaviorRelay(value: trueWindDirection!)
        }
        if waterTemperature != nil {
            self.waterTemperature = BehaviorRelay(value: waterTemperature!)
        }
        if airTemperature != nil {
            self.airTemperature = BehaviorRelay(value: airTemperature!)
        }
    }
    
    public init(profileRealm: ProfileCentraleRealm) {
        super.init()
        self.id = profileRealm.id
        if profileRealm.depth != nil {
            self.depth = BehaviorRelay(value: profileRealm.depth!)
        }
        if profileRealm.headingMagnetic != nil {
            self.headingMagnetic = BehaviorRelay(value: profileRealm.headingMagnetic!)
        }
        if profileRealm.speedThroughWater != nil {
            self.speedThroughWater = BehaviorRelay(value: profileRealm.speedThroughWater!)
        }
        if profileRealm.trueWindSpeed != nil {
            self.trueWindSpeed = BehaviorRelay(value: profileRealm.trueWindSpeed!)
        }
        if profileRealm.trueWindAngle != nil {
            self.trueWindAngle = BehaviorRelay(value: profileRealm.trueWindAngle!)
        }
        if profileRealm.apparentWindSpeed != nil {
            self.apparentWindSpeed = BehaviorRelay(value: profileRealm.apparentWindSpeed!)
        }
        if profileRealm.apparentWindAngle != nil {
            self.apparentWindAngle = BehaviorRelay(value: profileRealm.apparentWindAngle!)
        }
        if profileRealm.trueWindDirection != nil {
            self.trueWindDirection = BehaviorRelay(value: profileRealm.trueWindDirection!)
        }
        if profileRealm.waterTemperature != nil {
            self.waterTemperature = BehaviorRelay(value: profileRealm.waterTemperature!)
        }
        if profileRealm.airTemperature != nil {
            self.airTemperature = BehaviorRelay(value: profileRealm.airTemperature!)
        }
    }
}

// MARK: - Telltale
public class ProfileTelltale: ProfileType {
    // Telltales
    var angle: BehaviorRelay<ProfileSetting>
    var mediane: BehaviorRelay<ProfileSetting>
    var average: BehaviorRelay<ProfileSetting>
    var standardDeviation: BehaviorRelay<ProfileSetting>
    var variationMax: BehaviorRelay<ProfileSetting>
    var state: BehaviorRelay<ProfileSetting>
    
    public init(id: String? = nil, angle: ProfileSetting, mediane: ProfileSetting, average: ProfileSetting, standardDeviation: ProfileSetting, variationMax: ProfileSetting, state: ProfileSetting) {
        self.angle = BehaviorRelay(value: angle)
        self.mediane = BehaviorRelay(value: mediane)
        self.average = BehaviorRelay(value: average)
        self.standardDeviation = BehaviorRelay(value: standardDeviation)
        self.variationMax = BehaviorRelay(value: variationMax)
        self.state = BehaviorRelay(value: state)
        super.init()
        if let safeId = id {
            self.id = safeId
        } else {
            self.id = UUID().uuidString
        }
    }
    
    public init(profileRealm: ProfileTelltaleRealm) {
        let defaultValues = ProfileSetting.defaultProfileSettingValues
        let defaultSpeedValues = ProfileSetting.defaultSpeedProfileSettingValues
        if profileRealm.angle != nil {
            self.angle = BehaviorRelay(value: profileRealm.angle!)
        } else {
            self.angle = BehaviorRelay(value: ProfileSetting(defaultValues: defaultSpeedValues))
        }
        if profileRealm.mediane != nil {
            self.mediane = BehaviorRelay(value: profileRealm.mediane!)
        } else {
            self.mediane = BehaviorRelay(value: ProfileSetting(defaultValues: defaultSpeedValues))
        }
        if profileRealm.average != nil {
            self.average = BehaviorRelay(value: profileRealm.average!)
        } else {
            self.average = BehaviorRelay(value: ProfileSetting(defaultValues: defaultSpeedValues))
        }
        if profileRealm.standardDeviation != nil {
            self.standardDeviation = BehaviorRelay(value: profileRealm.standardDeviation!)
        } else {
            self.standardDeviation = BehaviorRelay(value: ProfileSetting(defaultValues: defaultSpeedValues))
        }
        if profileRealm.variationMax != nil {
            self.variationMax = BehaviorRelay(value: profileRealm.variationMax!)
        } else {
            self.variationMax = BehaviorRelay(value: ProfileSetting(defaultValues: defaultSpeedValues))
        }
        if profileRealm.state != nil {
            self.state = BehaviorRelay(value: profileRealm.state!)
        } else {
            self.state = BehaviorRelay(value: ProfileSetting(defaultValues: defaultValues))
        }
        super.init()
        self.id = profileRealm.id
    }
}

// MARK: - Various
public class ProfileVarious: ProfileType {
    var vocalRate = BehaviorRelay<Float>(value: 0.5)
    var voice: BehaviorRelay<String>
    // Battery level announces
    var batteryLevel: BehaviorRelay<ProfileSetting>
    
    public init(id: String? = nil, voice: String, vocalRate: Float, batteryLevel: ProfileSetting) {
        self.voice = BehaviorRelay(value: voice)
        self.vocalRate = BehaviorRelay(value: vocalRate)
        self.batteryLevel = BehaviorRelay(value: batteryLevel)
        super.init()
        if let safeId = id {
            self.id = safeId
        } else {
            self.id = UUID().uuidString
        }
    }
    
    public init(profileRealm: ProfileVariousRealm) {
        self.voice = BehaviorRelay(value: profileRealm.voice)
        self.vocalRate = BehaviorRelay(value: profileRealm.vocalRate)
        if profileRealm.batteryLevel != nil {
            self.batteryLevel = BehaviorRelay(value: profileRealm.batteryLevel!)
        } else {
            let defaultValues = ProfileSetting.defaultProfileSettingValues
            self.batteryLevel = BehaviorRelay(value: ProfileSetting(defaultValues: defaultValues))
        }
        super.init()
        self.id = profileRealm.id
    }
}

// MARK: - Carto
public class ProfileCarto: ProfileType {
    var enableBeacons = BehaviorRelay<Bool>(value: false)
    var timeAnnounce: BehaviorRelay<ProfileSetting>? // minimum time between two announces
    var detectionDistance: BehaviorRelay<ProfileSetting>?
    var detectionNumber: BehaviorRelay<ProfileSetting>?
    var distance: BehaviorRelay<ProfileSetting>?
    var azimut: BehaviorRelay<ProfileSetting>?
    var gisement: BehaviorRelay<ProfileSetting>?
    var gisementTime: BehaviorRelay<ProfileSetting>?
    var onlyFront: BehaviorRelay<ProfileSetting>?
    
    public init(id: String? = nil, enableBeacons: Bool, timeAnnounce: ProfileSetting? = nil, detectionDistance: ProfileSetting? = nil, detectionNumber: ProfileSetting? = nil, distance: ProfileSetting? = nil, azimut: ProfileSetting? = nil, gisement: ProfileSetting? = nil, gisementTime: ProfileSetting? = nil, onlyFront: ProfileSetting? = nil) {
        super.init()
        if let safeId = id {
            self.id = safeId
        } else {
            self.id = UUID().uuidString
        }
        self.enableBeacons = BehaviorRelay(value: enableBeacons)
        if timeAnnounce != nil {
            self.timeAnnounce = BehaviorRelay(value: timeAnnounce!)
        }
        if detectionDistance != nil {
            self.detectionDistance = BehaviorRelay(value: detectionDistance!)
        }
        if detectionNumber != nil {
            self.detectionNumber = BehaviorRelay(value: detectionNumber!)
        }
        if distance != nil {
            self.distance = BehaviorRelay(value: distance!)
        }
        if azimut != nil {
            self.azimut = BehaviorRelay(value: azimut!)
        }
        if gisement != nil {
            self.gisement = BehaviorRelay(value: gisement!)
        }
        if gisementTime != nil {
            self.gisementTime = BehaviorRelay(value: gisementTime!)
        }
        if onlyFront != nil {
            self.onlyFront = BehaviorRelay(value: onlyFront!)
        }
    }
    
    public init(profileRealm: ProfileCartoRealm) {
        super.init()
        self.id = profileRealm.id
        self.enableBeacons = BehaviorRelay(value: profileRealm.enableBeacons)
        if profileRealm.timeAnnounce != nil {
            self.timeAnnounce = BehaviorRelay(value: profileRealm.timeAnnounce!)
        }
        if profileRealm.detectionDistance != nil {
            self.detectionDistance = BehaviorRelay(value: profileRealm.detectionDistance!)
        }
        if profileRealm.detectionNumber != nil {
            self.detectionNumber = BehaviorRelay(value: profileRealm.detectionNumber!)
        }
        if profileRealm.distance != nil {
            self.distance = BehaviorRelay(value: profileRealm.distance!)
        }
        if profileRealm.azimut != nil {
            self.azimut = BehaviorRelay(value: profileRealm.azimut!)
        }
        if profileRealm.gisement != nil {
            self.gisement = BehaviorRelay(value: profileRealm.gisement!)
        }
        if profileRealm.gisementTime != nil {
            self.gisementTime = BehaviorRelay(value: profileRealm.gisementTime!)
        }
        if profileRealm.onlyFront != nil {
            self.onlyFront = BehaviorRelay(value: profileRealm.onlyFront!)
        }
    }
}
