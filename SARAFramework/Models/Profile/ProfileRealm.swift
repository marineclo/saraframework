//
//  ProfileRealm.swift
//  SARA Croisiere
//
//  Created by Rose on 29/10/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift

public class ProfileRealm: Object {

    @objc dynamic var name: String = ""
    @objc dynamic var id: String = ""
    @objc dynamic var isSelected: Bool = false
    let profilesTypeRealm: List<AnyProfileTypeRealm> = List<AnyProfileTypeRealm>()
    
    convenience init(id: String, name: String, profileTypesRealmList: List<AnyProfileTypeRealm>) {
        self.init()
        self.name = name
        self.id = id
        self.profilesTypeRealm.append(objectsIn: profileTypesRealmList)
    }
    
    override public static func primaryKey() -> String? {
        return "id"
    }
    
    public convenience init(profile: Profile) {
        let profileTypeRealm: List<AnyProfileTypeRealm> = List<AnyProfileTypeRealm>()
        for profileType in profile.profileTypes {
            if let pGPS = profileType as? ProfileGPS {
                profileTypeRealm.append(AnyProfileTypeRealm(ProfileGPSRealm(profileGPS: pGPS)))
            } else if let pRoute = profileType as? ProfileRoute {
                profileTypeRealm.append(AnyProfileTypeRealm(ProfileRouteRealm(profileRoute: pRoute)))
            } else if let pCentrale = profileType as? ProfileCentrale {
                profileTypeRealm.append(AnyProfileTypeRealm(ProfileCentraleRealm(profileCentrale: pCentrale)))
            } else if let pTelltale = profileType as? ProfileTelltale {
                profileTypeRealm.append(AnyProfileTypeRealm(ProfileTelltaleRealm(profileTelltale: pTelltale)))
            } else if let pVarious = profileType as? ProfileVarious {
                profileTypeRealm.append(AnyProfileTypeRealm(ProfileVariousRealm(profileVarious: pVarious)))
            } else if let pCarto = profileType as? ProfileCarto {
                profileTypeRealm.append(AnyProfileTypeRealm(ProfileCartoRealm(profileCarto: pCarto)))
            }
        }
        self.init(id: profile.id, name: profile.name, profileTypesRealmList: profileTypeRealm)
    }

    public func update(newProfile: Profile) {
        let realm = try! Realm()
        try! realm.write {
            self.name = newProfile.name
            for (index, profileType) in newProfile.profileTypes.enumerated() {
                if let pGPS = profileType as? ProfileGPS, let pGPSR = self.profilesTypeRealm[index].value as? ProfileGPSRealm {
                    pGPSR.update(profileGPS: pGPS)
                } else if let pRoute = profileType as? ProfileRoute, let pRouteR = self.profilesTypeRealm[index].value as? ProfileRouteRealm {
                    pRouteR.update(profileRoute: pRoute)
                } else if let pCentrale = profileType as? ProfileCentrale, let pCentraleR = self.profilesTypeRealm[index].value as? ProfileCentraleRealm {
                    pCentraleR.update(profileCentrale: pCentrale)
                } else if let pTelltale = profileType as? ProfileTelltale, let pTelltaleR = self.profilesTypeRealm[index].value as? ProfileTelltaleRealm {
                    pTelltaleR.update(profileTelltale: pTelltale)
                } else if let pVarious = profileType as? ProfileVarious, let pVariousR = self.profilesTypeRealm[index].value as? ProfileVariousRealm {
                    pVariousR.update(profileVarious: pVarious)
                } else if let pCarto = profileType as? ProfileCarto, let pCartoR = self.profilesTypeRealm[index].value as? ProfileCartoRealm {
                    pCartoR.update(profileCarto: pCarto)
                }
            }
        }
    }
    
    public var profile: Profile {
        
        var profileTypes: [ProfileType] = []
        for profileTypeRealm in self.profilesTypeRealm {
            if let pGPSRealm = profileTypeRealm.value as? ProfileGPSRealm {
                profileTypes.append(ProfileGPS(profileRealm: pGPSRealm))
            } else if let pRouteRealm = profileTypeRealm.value as? ProfileRouteRealm {
                profileTypes.append(ProfileRoute(profileRealm: pRouteRealm))
            } else if let pCentraleRealm = profileTypeRealm.value as? ProfileCentraleRealm {
                profileTypes.append(ProfileCentrale(profileRealm: pCentraleRealm))
            } else if let pTelltaleRealm = profileTypeRealm.value as? ProfileTelltaleRealm {
                profileTypes.append(ProfileTelltale(profileRealm: pTelltaleRealm))
            } else if let pVariousRealm = profileTypeRealm.value as? ProfileVariousRealm {
                profileTypes.append(ProfileVarious(profileRealm: pVariousRealm))
            } else if let pCartoRealm = profileTypeRealm.value as? ProfileCartoRealm {
                profileTypes.append(ProfileCarto(profileRealm: pCartoRealm))
            }
        }
        
        let profile = Profile(id: self.id, name: self.name, profileTypes: profileTypes)
        return profile
    }
    
    // Delete the profile
    public func delete() {
        let realm = try! Realm()
        try! realm.write {
            self.profilesTypeRealm.forEach({
                if let pGPSRealm = $0.value as? ProfileGPSRealm {
                    pGPSRealm.delete()
                } else if let pRouteRealm = $0.value as? ProfileRouteRealm {
                    pRouteRealm.delete()
                } else if let pCentraleRealm = $0.value as? ProfileCentraleRealm {
                    pCentraleRealm.delete()
                } else if let pTelltaleRealm = $0.value as? ProfileTelltaleRealm {
                    pTelltaleRealm.delete()
                } else if let pVariousRealm = $0.value as? ProfileVariousRealm {
                    pVariousRealm.delete()
                } else if let pCartoRealm = $0.value as? ProfileCartoRealm {
                    pCartoRealm.delete()
                }
            })
            realm.delete(self.profilesTypeRealm)
            realm.delete(self)
        }
    }
}
