//
//  TellTale.swift
//  SARA Croisiere
//
//  Created by Tristan Derbanne on 11.07.19.
//  Copyright © 2019 Rose. All rights reserved.
//

import UIKit
import RealmSwift
import RxSwift

class TellTale: Object {
    
    static func ==(tt1: TellTale, tt2: TellTale) -> Bool {
        return tt1.id == tt2.id
    }
    
    enum Status: String {
        
        case stall = "sara_croisiere.telltale_status.stall"
        case attached = "sara_croisiere.telltale_status.attached"
        case turbublent = "sara_croisiere.telltale_status.turbulent"
        
        var description: String {
            let bunle = Utils.bundle(anyClass: TellTale.self)
            switch self {
            case .stall:
                return NSLocalizedString("stall", bundle: bunle, comment: "")
            case .attached:
                return NSLocalizedString("attached", bundle: bunle, comment: "")
            case .turbublent:
                return NSLocalizedString("turbublent", bundle: bunle, comment: "")
            }
        }
    }
    
    enum StatusMethod: String, CaseIterable {
        case mediane = "mediane"
        case average = "average"
        case standardDeviation = "standard_deviation"
        case variation = "variation"
        
        var description: String {
            let bunle = Utils.bundle(anyClass: TellTale.self)
            switch self {
            case .mediane:
                return NSLocalizedString("mediane", bundle: bunle, comment: "")
            case .average:
                return NSLocalizedString("average", bundle: bunle, comment: "")
            case .standardDeviation:
                return NSLocalizedString("standard_deviation", bundle: bunle, comment: "")
            case .variation:
                return NSLocalizedString("variation_max", bundle: bunle, comment: "")
            }
        }
    }
    
    // MARK: - Variables
    
    // Tell tale variables
    @objc dynamic var id: String = ""
    
    var angle: Float? {
        didSet {
            if angle != nil {
                if isEnabled && isConfigured && isOnline {
                    AnnouncementManager.shared.tellTaleAngleVariable.accept((self.id, Int(angle ?? 0)))
                }
                angleHistory[Date()] = angle
                self.updateMedianValue()
                self.updateAverageValue()
                self.updateStandardDeviation()
                self.updateVariationMaxValue()
            }
        }
    }
    
    var medianAngle : Float?
    var averageAngle : Float?
    var standardDeviationAngle : Float?
    var variationAngle : Float?
    
    var batteryTensionVolts: Float?
    
    var status: Status = .stall
    
    var isConfigured: Bool {
        return !self.name.isEmpty
    }
    
    var isOnline: Bool = false
    
    // Configuration variables
    @objc dynamic var name: String = ""
    @objc dynamic var idName: String = ""
    @objc dynamic var medianSamplingTime: Int = 10 {
        didSet {
            self.updateMedianValue()
            self.updateAverageValue()
            self.updateStandardDeviation()
            self.updateVariationMaxValue()
        }
    }
    @objc dynamic var statusMethod: String = StatusMethod.mediane.rawValue
    func statusMethodCal() -> StatusMethod {
        switch self.statusMethod {
        case StatusMethod.mediane.rawValue:
            return StatusMethod.mediane
        case StatusMethod.average.rawValue:
            return StatusMethod.average
        case StatusMethod.standardDeviation.rawValue:
            return StatusMethod.standardDeviation
        case StatusMethod.variation.rawValue:
            return StatusMethod.variation
        default:
            return StatusMethod.mediane
        }
    }
    @objc dynamic var attachedThreshold: Int = 4 {
        didSet {
            self.updateStatus()
        }
    }
    @objc dynamic var turbulentThreshold: Int = 8 {
        didSet {
            self.updateStatus()
        }
    }
    @objc dynamic var isEnabled: Bool = true
    
    @objc dynamic var angleComputationSlope: Float = 0.035
    @objc dynamic var angleComputationYIntercept: Float = 0
    
    // MARK: - Median value computation
    private var angleHistory: [Date: Float] = [:]
    
    private func updateMedianValue() {
        // Filtering angles history to keep only relevant values
        let now = Date()
        angleHistory = angleHistory.filter({ now.timeIntervalSince($0.key) <= Double(medianSamplingTime) })
        let sortedAngles = angleHistory.sorted(by: { $0.value <= $1.value }).map({ $0.value })
        if !sortedAngles.isEmpty {
            if sortedAngles.count == 1 {
                self.medianAngle = sortedAngles.first!
            }
            else if sortedAngles.count % 2 == 0 {
                self.medianAngle = (sortedAngles[-1 + sortedAngles.count / 2] + sortedAngles[sortedAngles.count / 2]) / 2
            }
            else {
                self.medianAngle = sortedAngles[sortedAngles.count / 2]
            }
        }
        else {
            self.medianAngle = nil
        }
        if isEnabled && isConfigured && isOnline {
            AnnouncementManager.shared.tellTaleMedianeVariable.accept((self.id, Int(medianAngle ?? 0)))
        }
        self.updateStatus()
    }
    
    private func updateAverageValue() {
        // Filtering angles history to keep only relevant values
        let now = Date()
        angleHistory = angleHistory.filter({ now.timeIntervalSince($0.key) <= Double(medianSamplingTime) })
        let array = angleHistory.map({$0.value})
        
        guard !array.isEmpty else {
            averageAngle = nil
            return
        }
        let sumValues = array.reduce(0, +)
        averageAngle = sumValues / Float(array.count)
        if isEnabled && isConfigured && isOnline {
            AnnouncementManager.shared.tellTaleAverageVariable.accept((self.id, Int(averageAngle ?? 0)))
        }
        self.updateStatus()
    }
    
    private func updateStandardDeviation() {
        let now = Date()
        angleHistory = angleHistory.filter({ now.timeIntervalSince($0.key) <= Double(medianSamplingTime) })
        let array = angleHistory.map({$0.value})
        guard !array.isEmpty, let average = averageAngle else {
            standardDeviationAngle = nil
            return
        }
        /// Calculate the sum of the squares of the differences of the values from the mean
        ///
        /// A calculation common for both sample and population standard deviations.
        ///
        /// - calculate mean
        /// - calculate deviation of each value from that mean
        /// - square that
        /// - sum all of those squares
        let sumSquaredDeviations = array.map {
            let difference = $0 - average
            return difference * difference
        }.reduce(Float(0), +)
        standardDeviationAngle = sqrt(sumSquaredDeviations / Float(array.count))
        if isEnabled && isConfigured && isOnline {
            AnnouncementManager.shared.tellTaleStandardVariationVariable.accept((self.id, Int(standardDeviationAngle ?? 0)))
        }
        self.updateStatus()
    }
    
    private func updateVariationMaxValue() {
        // Filtering angles history to keep only relevant values
        let now = Date()
        angleHistory = angleHistory.filter({ now.timeIntervalSince($0.key) <= Double(medianSamplingTime) })
        let array = angleHistory.map({$0.value})
        
        guard !array.isEmpty else {
            variationAngle = nil
            return
        }
        guard let max = array.max(), let min = array.min() else {
            variationAngle = nil
            return
        }
        variationAngle = max - min
        if isEnabled && isConfigured && isOnline {
            AnnouncementManager.shared.tellTaleVariationMaxVariable.accept((self.id, Int(variationAngle ?? 0)))
        }
        self.updateStatus()
    }
    
    private func updateStatus() {
        var _angle: Float?
        switch statusMethod {
        case StatusMethod.mediane.rawValue:
            _angle = self.medianAngle
        case StatusMethod.average.rawValue:
            _angle = self.averageAngle
        case StatusMethod.standardDeviation.rawValue:
            _angle = self.standardDeviationAngle
        case StatusMethod.variation.rawValue:
            _angle = self.variationAngle
        default:
            _angle = nil
        }
        guard let angleUsed = _angle else {
            self.status = .stall
            return
        }
        if angleUsed < Float(attachedThreshold) {
            self.status = .stall
        }
        else if angleUsed < Float(turbulentThreshold) {
            self.status = .attached
        }
        else {
            self.status = .turbublent
        }
        if isEnabled && isConfigured && isOnline {
            AnnouncementManager.shared.tellTaleStateVarible.accept((self.id, self.status.description))
//            if self.status == .attached {
//                AnnouncementManager.shared.playBeepTelltale()
//            }
        }
    }
    
    // MARK: - Constants
    static let medianSamplingTimeMin: Int = 3
    static let medianSamplingTimeMax: Int = 30
    static let medianSamplingTimeStep: Int = 1
    static let statusThresholdStep: Int = 1
    static let attachedThresholdMin: Int = -100
    static let attachedThresholdMax: Int = 100
    static let turbulentThresholdMin: Int = -100
    static let turbulentThresholdMax: Int = 100
    
    // MARK: - Initializers
    
    // TODO: make private once testing is over!!
    convenience init(id: String) {
        self.init()
        self.id = id
    }
    
    private convenience init(tellTaleUpdate: TellTalesManager.TellTaleUpdate) {
        self.init()
        self.id = tellTaleUpdate.eTellTaleId
        self.update(tellTaleUpdate)
    }


    func update(_ tellTaleUpdate: TellTalesManager.TellTaleUpdate) {
        guard tellTaleUpdate.eTellTaleId == self.id else {
            return
        }
        self.angle = self.tellTaleAngle(tellTaleUpdate.rawData)
        self.batteryTensionVolts = tellTaleUpdate.batteryTensionVolts
        self.isOnline = true
    }
    
    // MARK: - Helpers
    class func tellTale(tellTaleUpdate update: TellTalesManager.TellTaleUpdate) -> TellTale {
        let realm = try! Realm()
        if let tellTale = realm.objects(TellTale.self).first(where: { $0.id == update.eTellTaleId }) {
            return tellTale
        }
        // Tell Tale not found: creating a new one
        return TellTale(tellTaleUpdate: update)
    }
    
    private func tellTaleAngle(_ stringValue: String) -> Float? {
        // TODO: Change this when we know how to extract angle from raw data
        // exx: +0466
        guard stringValue.count >= 5 else { return nil }
        if let f = Float(stringValue) {
            return self.angleComputationSlope * f + self.angleComputationYIntercept
        }
        return nil
    }
}
