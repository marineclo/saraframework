//
//  Language.swift
//  SARA Croisiere
//
//  Created by Marine on 04.04.20.
//  Copyright © 2020 GraniteApps. All rights reserved.
//

import Foundation

// constants
let APPLE_LANGUAGE_KEY = "AppleLanguages"
/// Language
class Language {
/// get current Apple language
    class func currentAppleLanguage() -> String{
        let userdef = UserDefaults.standard
        let langArray = userdef.object(forKey: APPLE_LANGUAGE_KEY) as! NSArray
        let current = langArray.firstObject as! String
        return current
    }
    
    public static var availableManVoices = ["en", "fr", "nl", "zh"]
    class func isManVoiceAvailable() -> Bool {
        let language = Language.currentAppleLanguage()
        if Language.availableManVoices.contains(String(language.prefix(2))) {
            return true
        }
        return false
    }
    
    class func voiceCode() -> String {
        let language = Language.currentAppleLanguage()
        guard let voiceP = User.shared.profile.profileTypes.first(where: { $0 is ProfileVarious }) else {
            return ""
        }
        let voice = (voiceP as! ProfileVarious).voice.value
        if voice == "ManVoice" {
            if language.contains("fr") {
                return "fr-FR"
            } else if language.contains("nl") {
                return "nl-NL"
            }
            return "en-GB"
        } else {
            if language.contains("fr") {
                return "fr-CA"
            } else if language.contains("nl") {
                return "nl-BE"
            }
            return "en-US"
        }
    }
}
