//
//  NavigationManager.swift
//  SARA Croisiere
//
//  Created by Rose on 23/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import RxSwift
import RealmSwift
import CoreLocation
import RxCocoa

public class NavigationManager: NSObject {
    
    // MARK: - Variables
    public static let shared = NavigationManager()
    let bundle = Utils.bundle(anyClass: NavigationManager.self)
    public var route = BehaviorRelay<Course?>(value: nil)
    public var point = BehaviorRelay<AnyPoint?>(value: nil) // WARNING! The value should never beb updated outside of this class!!! User currentPointIndex instead
    public var isNotLastPoint = BehaviorRelay<Bool>(value: true)
    public var isNotFirstPoint = BehaviorRelay<Bool>(value: true)
    public var shouldReloadUnits = BehaviorRelay<Bool>(value: false)
    
    public var locationStartCourse: CLLocation?
    
    // Current point index
    public var currentPointIndex = BehaviorRelay<Int?>(value: nil)
    
    public var hasStarted = BehaviorRelay<Bool>(value: false)
    
    public var announcementDelegate: AnnouncementDelegate?
    
    let disposeBag = DisposeBag()
    
    // ----- GPS
    public var speedOverGround = BehaviorRelay<Float?>(value: nil)
    public var courseOverGround = BehaviorRelay<Int?>(value: nil)
    
    // ----- Route
    // Distances to current point and next point
    public var distanceToCurrentPoint = BehaviorRelay<((Float, NavigationUnit?)?,(Float, NavigationUnit?)?)?>(value: nil)
    public var azimutToCurrentPoint = BehaviorRelay<(Int?, Int?)?>(value: nil)
    public var gisementToCurrentPoint = BehaviorRelay<((Int, NavigationUnit?)?, (Int, NavigationUnit?)?)?>(value: nil)
    public var usingMiddlePoint: Bool = false
    public var middlePointChanged = BehaviorRelay<Bool>(value: false)
    //CMG
    public var cmgToCurrentPoint = BehaviorRelay<(Float?, Float?)?>(value: nil)
    //Gap
    public var previousPosition: CLLocation?
    public var gap = BehaviorRelay<((Float, NavigationUnit?)?,(Float, NavigationUnit?)?)?>(value: nil)
    
    public var distanceToNextPoint = BehaviorRelay<((Float, NavigationUnit?)?,(Float, NavigationUnit?)?)?>(value: nil)
    public var azimutNextPoint = BehaviorRelay<(Int?, Int?)?>(value: nil)
    public var gisementNextPoint = BehaviorRelay<((Int, NavigationUnit?)?, (Int, NavigationUnit?)?)?>(value: nil)
    
    public var updateNavView: Bool = false
    
    // For timer
    public var totalTime = 300 // in seconds
    public var totalTimeCollectiveStart = 0
    public var timer: Timer?
    public let incrementTimer = 30
        
    override init() {
        super.init()
        // Make sure we use Realm on the main queue
        DispatchQueue.main.async {
            self.setCurrentRoute(route: self.getCurrentRouteFromRealm())
            
            self.bindObjects()
            
            guard let course = self.route.value else {
                self.isNotLastPoint.accept(false)
                self.isNotFirstPoint.accept(false)
                return
            }
            self.checkIndex(route: course, index: 0)
        }
    
//        self.bindObjects()
//        
//        guard let course = self.route.value else {
//            self.isNotLastPoint.accept(false)
//            self.isNotFirstPoint.accept(false)
//            return
//        }
//        self.checkIndex(route: course, index: 0)
    }
    
    func bindObjects() {
//        self.route
//            .asObservable()
//            .map({ $0?.points.first })
//            .bind(to: point)
//            .disposed(by: disposeBag)
        
        self.route
            .withPrevious()
            .observe(on: MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] (previous, current) in
                if previous != current && current != nil && !current!.points.isEmpty {            self?.setFirstPoint()
                } else if current == nil {
                    self?.point.accept(nil)
                    self?.currentPointIndex.accept(nil)
                }
            }).disposed(by: disposeBag)
        
        self.currentPointIndex
            .asObservable()
//            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] (pointIndex) in
                guard let currentCourse = self?.route.value,
                let index = pointIndex,
                index < currentCourse.points.count else {
                        self?.isNotLastPoint.accept(false)
                        self?.isNotFirstPoint.accept(false)
                        return
                }
                self?.checkIndex(route: currentCourse, index: index)
                self?.point.accept(currentCourse.points[index])
                
                //TODO: Check if needed
//                if let location = LocationManager.shared.locationVariable.value {
//                    self?.updateRouteInfoWithLocation(location: location)
//                }
//                LocationManager.shared.locationVariable.accept(LocationManager.shared.locationVariable.value)
            }).disposed(by: disposeBag)
        
        LocationManager.shared.locationVariable
            .asObservable()
            .filterNil()
            .subscribe(onNext: { [weak self] (location) in
                guard let weak = self else {
                    return
                }
                // -------------- GPS -----------------
                // Course over ground
                weak.courseOverGround.accept(NavigationHelper.getCourseFrom(location: location))
                AnnouncementManager.shared.courseOverGroundVariable.accept(weak.courseOverGround.value)
                
                // Speed over ground
                weak.speedOverGround.accept(NavigationHelper.getSpeedFrom(location: location, unit: Settings.shared.speedUnit?.unit))
                AnnouncementManager.shared.speedOverGroundVariable.accept(weak.speedOverGround.value)
                
                
                // -------------- ROUTE -----------------
                weak.updateRouteInfoWithLocation(location: location)
            })
            .disposed(by: disposeBag)
        
        self.hasStarted
            .asObservable()
            .withPrevious()
            .subscribe(onNext: { (previousValue, newValue) in
                if previousValue != newValue {
                    if Settings.shared.saveTrace {
                        newValue ? TraceManager.shared.startManager() : TraceManager.shared.stopManager()
                        TraceManager.shared.recordingStatus = newValue ? .play : .idle
                    }
                }
            }).disposed(by: disposeBag)
    }
    
    func updateRouteInfoWithLocation(location: CLLocation) {
        var gpsPosition1: GPSPosition?
        var gpsPosition2: GPSPosition?
        if let coursePoint = self.point.value?.point as? CoursePoint {
            gpsPosition1 = coursePoint.gpsPosition
        } else if let mark = self.point.value?.point as? Mark {
            // In case of a double mark, if the distance between our position and the middle of the mark is superior to one third of the distance between the previous mark (or current location if first mark) and the middle of the mark, we use the middle of the mark for the calculation and announces.
            var previousPointPosition = self.getPreviousPointWithCoord()?.point.getCoordinates(method: .middle)
            if previousPointPosition == nil {
                if locationStartCourse == nil {
                    locationStartCourse = location
                    previousPointPosition = GPSPosition(latitude: Float(location.coordinate.latitude), longitude: Float(location.coordinate.longitude))
                } else {
                    previousPointPosition = GPSPosition(latitude: Float(locationStartCourse!.coordinate.latitude), longitude: Float(locationStartCourse!.coordinate.longitude))
                }
            }
            let middlePos = mark.getCoordinates(method: .middle)
            if mark.markType == Mark.MarkType.double.rawValue || mark.markType == Mark.MarkType.end.rawValue { // Use the distance to the line instead of the distance to the middle point.
                if let distanceBetweenPoints = NavigationHelper.getDistanceSegmentFrom(gpsPosition1: mark.buoys.first?.gpsPosition, gpsPosition2: mark.buoys.last?.gpsPosition, location: previousPointPosition), let distanceToCurrentPoint = NavigationHelper.getDistanceSegmentFrom(gpsPosition1: mark.buoys.first?.gpsPosition, gpsPosition2: mark.buoys.last?.gpsPosition, location: GPSPosition(latitude: Float(location.coordinate.latitude), longitude: Float(location.coordinate.longitude))) {
                    if distanceToCurrentPoint > distanceBetweenPoints / 3 {
                        gpsPosition1 = middlePos
                        if !usingMiddlePoint { // value will change: fire Notification
                            middlePointChanged.accept(true)
                        }
                        usingMiddlePoint = true
                    } else {
                        if usingMiddlePoint { // value will change: fire Notification
                            middlePointChanged.accept(true)
                            AnnouncementManager.shared.resetCurrentStateRoute() // reset announces when pass from middlepoint to double mark info
                        }
                        usingMiddlePoint = false
                        gpsPosition1 = mark.buoys.first?.gpsPosition
                        gpsPosition2 = mark.buoys.last?.gpsPosition
                    }
                }
            } else if mark.markType == Mark.MarkType.start.rawValue { // if it is a start use the pin and committee bouys
                usingMiddlePoint = false
                gpsPosition1 = mark.buoys.first?.gpsPosition
                gpsPosition2 = mark.buoys.last?.gpsPosition
            } else {
                usingMiddlePoint = false
                gpsPosition1 = mark.buoys.first?.gpsPosition
            }
        }
        self.routePosition(location: location, gpsPosition1: gpsPosition1, gpsPosition2: gpsPosition2)
        // Compute the distance to next point
        var gpsNextPosition1: GPSPosition?
        var gpsNextPosition2: GPSPosition?
        if let coursePoint = self.getNextPointWithCoord().value?.point as? CoursePoint {
            gpsNextPosition1 = coursePoint.gpsPosition
        } else if let mark = self.getNextPointWithCoord().value?.point as? Mark {
            gpsNextPosition1 = mark.buoys.first?.gpsPosition
            if mark.markType == Mark.MarkType.double.rawValue {
                gpsNextPosition2 = mark.buoys.last?.gpsPosition
            }
        }
        self.routeNextPosition(location: location, gpsNextPosition1: gpsNextPosition1, gpsNextPosition2: gpsNextPosition2)
    }
    
    func routePosition(location: CLLocation, gpsPosition1: GPSPosition?, gpsPosition2: GPSPosition?) {
        // Compute the distance in meters
        let distance1 = NavigationHelper.getDistanceFrom(location: location, to: gpsPosition1)
        let distance2 = NavigationHelper.getDistanceFrom(location: location, to: gpsPosition2)
        if distance1 != nil || distance2 != nil {
            self.distanceToCurrentPoint.accept((Utils.tupleDistance(floatDistance: distance1), Utils.tupleDistance(floatDistance: distance2)))
        } else {
            self.distanceToCurrentPoint.accept(nil)
        }
        AnnouncementManager.shared.distanceVariable.accept(distance1)
        if gpsPosition2 != nil {
            AnnouncementManager.shared.distanceVariable2.accept(distance2)
        }
        
        // Bearing
        let bearing1 = NavigationHelper.getBearingFrom(gpsPosition: gpsPosition1, location: location)
        let bearing2 = NavigationHelper.getBearingFrom(gpsPosition: gpsPosition2, location: location)
        if bearing1 != nil || bearing2 != nil {
            self.azimutToCurrentPoint.accept((bearing1?[0]?.0,bearing2?[0]?.0))
            self.gisementToCurrentPoint.accept((bearing1?[1], bearing2?[1]))
        } else {
            self.azimutToCurrentPoint.accept(nil)
            self.gisementToCurrentPoint.accept(nil)
        }
        AnnouncementManager.shared.azimutVariable.accept(bearing1?[0])
        AnnouncementManager.shared.gisementVariable.accept(bearing1?.suffix(2))
        if gpsPosition2 != nil {
            AnnouncementManager.shared.azimutVariable2.accept(bearing2?[0])
            AnnouncementManager.shared.gisementVariable2.accept(bearing2?.suffix(2))
        }
        
        // It's a coursePoint -> Go to next point if within the distance threshold
        if let safePoint = self.point.value?.point as? CoursePoint, safePoint.distanceThreshold != 0, distance1 != nil { // Float.greatestFiniteMagnitude // distance != 0, it's a mob
            if distance1! < safePoint.distanceThreshold {
                self.setNextPoint()
            }
        } else if let m = self.point.value?.point as? Mark {
            CourseManager.shared.activationNextMark()
            // Add annouce for starting line if the course is launched
            if m.markType == Mark.MarkType.start.rawValue  { // && self.hasStarted.value
                AnnouncementManager.shared.startAnnouncement()
            }
            // For simple and double, check for the entry in the 3 lengths
            if m.markType == Mark.MarkType.simple.rawValue || m.markType == Mark.MarkType.double.rawValue {
                if distance2 != nil && distance1 != nil {
                    let distanceToUse = distance1! < distance2! ? distance1 : distance2
                    AnnouncementManager.shared.entry3LengthsVariable.accept(distanceToUse)
                } else {
                    AnnouncementManager.shared.entry3LengthsVariable.accept(distance1)
                }
            }
        }

        // CMG
        let cmg1 = NavigationHelper.getCMGFrom(gpsPosition: gpsPosition1, location: location)
        let cmg2 = NavigationHelper.getCMGFrom(gpsPosition: gpsPosition2, location: location)
        if cmg1 != nil || cmg2 != nil {
            self.cmgToCurrentPoint.accept((cmg1, cmg2))
        } else {
            self.cmgToCurrentPoint.accept(nil)
        }
        AnnouncementManager.shared.CMGVariable.accept(cmg1)
        if gpsPosition2 != nil {
            AnnouncementManager.shared.CMGVariable2.accept(cmg2)
        }
        
        // Gap: in case of double mark: always use the middle of the mark so gap1 = gap2
        var gap1: Float?
        var gap2: Float?
        if gpsPosition2 != nil {
            let middlePos = gpsPosition1?.middlePosition(gpsP2: gpsPosition2)
            gap1 = NavigationHelper.getGapToRouteSegmentFrom(gpsPosition: middlePos, location: location)
            gap2 = gap1
        } else {
            gap1 = NavigationHelper.getGapToRouteSegmentFrom(gpsPosition: gpsPosition1, location: location)
        }
        if gap1 != nil || gap2 != nil {
            self.gap.accept((Utils.tupleDistance(floatDistance: gap1), Utils.tupleDistance(floatDistance: gap2)))
        } else {
            self.gap.accept(nil)
        }
        AnnouncementManager.shared.gapSegmentVariable.accept(gap1)
    }
    
    func routeNextPosition(location: CLLocation, gpsNextPosition1: GPSPosition?, gpsNextPosition2: GPSPosition?) {
        let distance1 = NavigationHelper.getDistanceFrom(location: location, to: gpsNextPosition1)
        let distance2 = NavigationHelper.getDistanceFrom(location: location, to: gpsNextPosition2)
        if distance1 != nil || distance2 != nil {
            self.distanceToNextPoint.accept((Utils.tupleDistance(floatDistance: distance1), Utils.tupleDistance(floatDistance: distance2)))
        } else {
            self.distanceToNextPoint.accept(nil)
        }
        
        let bearing1 = NavigationHelper.getBearingFrom(gpsPosition: gpsNextPosition1, location: location)
        let bearing2 = NavigationHelper.getBearingFrom(gpsPosition: gpsNextPosition2, location: location)
        if bearing1 != nil || bearing2 != nil {
            self.azimutNextPoint.accept((bearing1?[0]?.0,bearing2?[0]?.0))
            self.gisementNextPoint.accept((bearing1?[1], bearing2?[1]))
        } else {
            self.azimutNextPoint.accept(nil)
            self.gisementNextPoint.accept(nil)
        }
    }
    
    public func setCurrentRoute(route: Course?, isSaved: Bool = true) {
        CourseManager.shared.validationMarkData = nil
        CourseManager.shared.axisCourseValidation = nil
        usingMiddlePoint = false
        self.route.accept(route)
        if route != nil {
            AnnouncementManager.shared.startAnnoncesRoute()
        } else {
            AnnouncementManager.shared.stopAnnoncesRoute()
        }
        LocationManager.shared.resetAverages()
        NmeaManager.default.resetMaxSpeeds()
        locationStartCourse = LocationManager.shared.locationVariable.value
        
        guard let safeRoute = route else {
            NavigationState.removeAll()
            return
        }
        
        let navigationState = NavigationState(course: safeRoute)
        navigationState.saveAndReplace(isSaved: isSaved)
    }
        
    // Go to the next point in the navigation
    public func setNextPoint() {
        guard let currentRoute = self.route.value,
            let currentPoint = self.point.value else {
            return
        }
        CourseManager.shared.validationMarkData = nil
        CourseManager.shared.axisCourseValidation = nil
        usingMiddlePoint = false
        
        if var index = currentPointIndex.value {
            // Check if the next point has coordinates
            var pointsWithNoCoord: [AnyPoint] = []
            var nextPoint = getNextPoint()
            while( nextPoint != nil && nextPoint!.point.getCoordinates(method: .middle) == nil ) {
                pointsWithNoCoord.append(nextPoint!)
                index += 1
                nextPoint = getNextPoint(indexToUsed: index)
            }
            var noCoordMessage = ""
            pointsWithNoCoord.forEach({
                noCoordMessage += String(format: NSLocalizedString("announce_next_point_no_coordinate", bundle: bundle, comment: ""), $0.point.name)
            })
            // Add announcement of the passing of the point
            if currentRoute.points.count > index + 1 { // There is a next point
                print("=== Annonce next point")
                AnnouncementManager.shared.playBeepPoint()
                AnnouncementManager.shared.resetCurrentStateRoute()
                let text = announcementDelegate?.announcementPointValidation(nextPoint: nextPoint, noCoordMessage: noCoordMessage) ?? ""
                AnnouncementQueueManager.shared.addAnnounce(Announce(text: text, priority: 0, type: .changePoint, annonceState: nil))
                self.checkIndex(route: currentRoute, index: index + 1)
                self.currentPointIndex.accept(index + 1)
                if getNextPointWithCoord() == nil {
                    self.isNotLastPoint.accept(false)
                }
            } else {
                // If it was the last point of the route, stop the route and add the announce that the route was finished
                AnnouncementManager.shared.playBeepPoint()
                let text = announcementDelegate?.announcementEndRoute(noCoordMessage: noCoordMessage, noCoord: false) ?? ""
                AnnouncementQueueManager.shared.addAnnounce(Announce(text: text, priority: 0, type: .courseOver, annonceState: nil))
                self.setCurrentRoute(route: nil)
                self.isNotLastPoint.accept(false)
                self.isNotFirstPoint.accept(false)
            }
        } else {
            if currentRoute.points.count > 0 {
                self.setFirstPoint()
            }
        }
    }
    
    /// Get next point in the current route
    ///
    /// - Returns: CoursePoint?
    private func getNextPoint(indexToUsed: Int? = nil) -> AnyPoint? {
        guard let currentRoute = self.route.value else {
                return nil
        }
        
        var index: Int?
        if indexToUsed != nil {
            index = indexToUsed!
        } else {
            index = self.currentPointIndex.value
        }
        if index != nil {
            if currentRoute.points.count > index! + 1 {
              return currentRoute.points[index! + 1]
            }
        }
        
        return nil
    }
    
    public func getNextPointWithCoord() -> AnyPoint? {
        guard let currentRoute = self.route.value else {
                return nil
        }
        var index = currentPointIndex.value ?? 0
        while( getNextPoint(indexToUsed: index) != nil && getNextPoint(indexToUsed: index)!.point.getCoordinates(method: .middle) == nil && currentRoute.points.count > index) {
            index += 1
        }
        return getNextPoint(indexToUsed: index)
    }
    
    // Go back to previous point in the navigation
    public func setPreviousPoint() {
        guard let _ = self.route.value else {
                return
        }
        CourseManager.shared.validationMarkData = nil
        CourseManager.shared.axisCourseValidation = nil
        usingMiddlePoint = false
        
        if let index = self.currentPointIndex.value,
            index > 0 {
            if index == 1 {
                self.previousPosition = nil
            }
            var indexToUse = index
            while( getPreviousPoint() != nil && getPreviousPoint()!.point.getCoordinates(method: .middle) == nil && index > 0) {
                indexToUse -= 1
//                index -= 1
//                self.currentPointIndex.accept(index)
            }
            if index == indexToUse { // No point with no coordinate so decrement index
                indexToUse -= 1
            }
//            self.currentPointIndex.accept(index - 1)
            self.currentPointIndex.accept(indexToUse)
            if getPreviousPointWithCoord() == nil {
                self.isNotFirstPoint.accept(false)
            }
        }
    }
    
    private func getPreviousPoint(indexToUsed: Int? = nil) -> AnyPoint? {
        guard let currentRoute = self.route.value else {
                return nil
        }
        var index: Int?
        if indexToUsed != nil {
            index = indexToUsed!
        } else {
            index = self.currentPointIndex.value
        }
        if index != nil {
            if index! - 1 > -1 {
                return currentRoute.points[index! - 1]
            }
        }
        
        return nil
    }
    
    public func getPreviousPointWithCoord() -> AnyPoint? {
        var index = currentPointIndex.value ?? 0
        while( getPreviousPoint(indexToUsed: index) != nil && getPreviousPoint(indexToUsed: index)!.point.getCoordinates(method: .middle) == nil && index > 0) {
            index -= 1
        }
        return getPreviousPoint(indexToUsed: index)
    }
    
    public func getPreviousIndexWithCoord() -> Int? {
        var index = currentPointIndex.value ?? 0
        while( getPreviousPoint(indexToUsed: index) != nil && getPreviousPoint(indexToUsed: index)!.point.getCoordinates(method: .middle) == nil && index > 0) {
            index -= 1
        }
        if index - 1 > -1 {
            return index - 1
        }
        return nil
    }
    
    // Reset navigation to the first point of the route
    public func setFirstPoint() {
        guard let currentRoute = self.route.value else {
                return
        }
        // Check if point has coordinates
        var index = 0
        var pointsWithNoCoord: [AnyPoint] = []
        while(index < currentRoute.points.count && currentRoute.points[index].point.getCoordinates(method: .middle) == nil) {
            pointsWithNoCoord.append(currentRoute.points[index])
            index += 1
        }
        var noCoordMessage = ""
        pointsWithNoCoord.forEach({
            noCoordMessage += String(format: NSLocalizedString("announce_next_point_no_coordinate", bundle: bundle, comment: ""), $0.point.name)
        })
        if index == currentRoute.points.count { // All the points have no coordinates, end route
            print("=== SetFirstPoint: end route")
            let text = announcementDelegate?.announcementEndRoute(noCoordMessage: noCoordMessage, noCoord: true) ?? ""
            AnnouncementQueueManager.shared.addAnnounce(Announce(text: text, priority: 0, type: .courseOver, annonceState: nil))
            self.setCurrentRoute(route: nil)
            self.isNotLastPoint.accept(false)
            self.isNotFirstPoint.accept(false)
            return
        } else if noCoordMessage != "" { // first point with no coord, add announcement and use the next point with coordinates
            print("=== SetFirstPoint")
            AnnouncementQueueManager.shared.addAnnounce(Announce(text: noCoordMessage, priority: 0, type: .courseOver, annonceState: nil))
            self.currentPointIndex.accept(index)
            self.isNotFirstPoint.accept(false)
            return
        }
        // Set first point
        self.currentPointIndex.accept(index)
    }
    
    public func setPoint(with index: Int) {
        guard let currentRoute = self.route.value else {
                return
        }
        var indexToUse = index
//        self.currentPointIndex.accept(indexToUse)
        var pointsWithNoCoord: [AnyPoint] = []
        while(indexToUse < currentRoute.points.count && currentRoute.points[indexToUse].point.getCoordinates(method: .middle) == nil) {
            pointsWithNoCoord.append(currentRoute.points[indexToUse])
            indexToUse += 1
//            self.currentPointIndex.accept(indexToUse)
        }
        if indexToUse == currentRoute.points.count {
            // If it was the last point of the route, stop the route and add the announce that the route was finished
            NavigationManager.shared.setCurrentRoute(route: nil)
            self.isNotLastPoint.accept(false)
            self.isNotFirstPoint.accept(false)
        } else {
            self.currentPointIndex.accept(indexToUse)
            if getPreviousPointWithCoord() == nil { // It's the first point with coordinate
                self.isNotFirstPoint.accept(false)
            } else if indexToUse > currentRoute.points.count - 1 || self.getNextPointWithCoord() == nil {
//            else if indexToUse > currentRoute.points.count - 1 || self.getNextPoint(indexToUsed: indexToUse) == nil{ // It's the last point with coordinate
                self.isNotLastPoint.accept(false)
            }
        }
    }
    
    /// Start or stop the current route, depending on the context
    public func startStopRoute() {
        self.hasStarted.accept(!self.hasStarted.value) 
        
        if hasStarted.value == false { // Stop route
            CourseManager.shared.validationMarkData = nil
            CourseManager.shared.axisCourseValidation = nil
            usingMiddlePoint = false
            self.setFirstPoint()
        } else {
            if Settings.shared.saveTrace { // When start course, add announce if the traces are recorded.
                AnnouncementQueueManager.shared.addAnnounce(Announce(text: Utils.localizedString(forKey: "record_track", app: "SARARegate"), priority: 1, type: .track, annonceState: nil))
            }
        }
    }
    
    // To check if the current point is a start
    public func isStartPoint() -> Bool {
        return self.point.value != nil && self.point.value!.point.isStartPoint()
//        return (self.point.value?.point as? Mark)?.markType == Mark.MarkType.start.rawValue && self.hasStarted.value && self.timer != nil && self.totalTime > 0 ? true : false
    }
    
    public func initTotalTime() {
        if let raceCourse = NavigationManager.shared.route.value as? RaceCourse, let type = raceCourse.type.value, type == .matchRace { // Timer at 7 minutes for match race
            NavigationManager.shared.totalTime = 60 * 7
        } else { // Timer at 5 minutes for other race
            NavigationManager.shared.totalTime = 60 * 5
        }
    }
    
    public func addAnnouncementAxis() {
        guard let text = announcementDelegate?.announcementAxis?() else { return }
        AnnouncementQueueManager.shared.addAnnounce(Announce(text: text, priority: 0, type: .axisCrossed, annonceState: nil))
    }
    
    public func addAnnouncementStartLaunch() {
        guard let text = announcementDelegate?.announcementStartLaunch?() else { return }
        AnnouncementQueueManager.shared.addAnnounce(Announce(text: text, priority: 1, type: .startLaunch, annonceState: nil))
    }
    
    /// Update the variable of navigation
    ///
    /// - Parameters:
    ///   - route: Course
    ///   - index: Int
    private func checkIndex(route: Course, index: Int) {
        // Update is not last point variable
        self.isNotLastPoint.accept((index != route.points.count - 1) && route.points.count != 0)
        
        // Update is not first point variable
        self.isNotFirstPoint.accept(index != 0)
    }
    
    private func getCurrentRouteFromRealm() -> Course? {
        let realm = try! Realm()
        return realm.objects(NavigationState.self).first?.currentCourse?.course
    }
}
