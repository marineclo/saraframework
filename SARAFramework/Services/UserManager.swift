//
//  UserManager.swift
//  SARA Croisiere
//
//  Created by Rose on 02/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import FirebaseAuth

public class UserManager {
    
    public static let shared = UserManager()
    private var user: User? {
        get {
           return User.shared
        }
        set {
            User.shared = newValue ?? User()
        }
    }
    
    init() {}
    
    public func login(email: String, password: String, successHandler: @escaping () -> Void, errorHandler: @escaping (Error) -> Void) {
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if let safeError = error {
                UserManager.shared.user = nil
                errorHandler(safeError)
            } else {
                UserManager.shared.user = User(email: email, password: password)
                FirebaseManager.shared.checkSubscribedCourse()
                FirebaseManager.shared.retrieveAllCourses()
                successHandler()
            }
        }
    }
    
    public func createUser(email: String, password: String, successHandler: @escaping () -> Void, errorHandler: @escaping (Error) -> Void) {
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            if let safeError = error {
                UserManager.shared.user = nil
                errorHandler(safeError)
            } else {
                UserManager.shared.user = User(email: email, password: password)
                FirebaseManager.shared.checkSubscribedCourse()
                FirebaseManager.shared.retrieveAllCourses()
                successHandler()
            }
        }
    }
    
    public func logout(successHandler: @escaping () -> Void, errorHandler: @escaping (Error) -> Void) {
        do {
            // Remove obervers
            FirebaseManager.shared.removeAllOberservers()
            FirebaseManager.shared.firebaseCourses = []
            try Auth.auth().signOut()
        } catch let error {
            errorHandler(error)
            return
        }
        UserManager.shared.user = nil
        successHandler()
    }
    
    public func resetPassword(email: String, successHandler: @escaping () -> Void, errorHandler: @escaping (Error) -> Void) {
        Auth.auth().sendPasswordReset(withEmail: email, completion: { error in
            if let safeError = error {
                UserManager.shared.user = nil
                errorHandler(safeError)
            } else {
                print("success")
                successHandler()
            }
        })
    }
    
    public func deleteAccount(successHandler: @escaping () -> Void, errorHandler: @escaping (Error) -> Void) {
        let user = Auth.auth().currentUser
        
        user?.delete { error in
            if let error = error { // An error happened.
                errorHandler(error)
            } else { // Account deleted.
                successHandler()
            }
        }
    }
}

// MARK: - Extension AuthErrorCode
// Error messages for Login, sign up and logOut
extension AuthErrorCode {
    var errorMessage: String {
        let bundle = NavigationManager.shared.bundle
        switch self {
        case AuthErrorCode.userNotFound: // Login
            return NSLocalizedString("user_not_found", bundle: bundle, comment: "")
        case AuthErrorCode.invalidEmail: // Login, sign up
            return NSLocalizedString("invalid_email", bundle: bundle, comment: "")
        case AuthErrorCode.wrongPassword: // Login
            return NSLocalizedString("wrong_password", bundle: bundle, comment: "")
        case AuthErrorCode.emailAlreadyInUse: // Sign up
            return NSLocalizedString("email_already_in_use", bundle: bundle, comment: "")
        case AuthErrorCode.weakPassword: // Sign up
            return NSLocalizedString("weak_password", bundle: bundle, comment: "")
        case AuthErrorCode.keychainError:
            return NSLocalizedString("", bundle: bundle, comment: "")
        case AuthErrorCode.networkError:
            return NSLocalizedString("network_error", bundle: bundle, comment: "")
        default:
            return "Unknown error occurred"
        }
    }
}
