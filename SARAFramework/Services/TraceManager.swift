//
//  TraceManager.swift
//  SARA Croisiere
//
//  Created by Marine on 20.04.20.
//  Copyright © 2020 GraniteApps. All rights reserved.
//

import Foundation
import RealmSwift
import CoreLocation
import RxSwift

class TraceManager {
    static let shared = TraceManager()
    
    let disposeBag = DisposeBag()
    
    var fileName: String?
    
    var previousLocation: CLLocation?
    
    var traceFile: URL? {
        let dirPathNoScheme = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        guard let documentsDirectory = URL(string: dirPathNoScheme) else { return nil }

        let dataPath = documentsDirectory.appendingPathComponent("Traces")
        if !FileManager.default.fileExists(atPath: dataPath.absoluteString) {
            do {
                try FileManager.default.createDirectory(atPath: dataPath.absoluteString, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print(error.localizedDescription)
            }
        }
        // There is file:// at the begining
        guard let documentsDirectory2 = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }
        
        if let name = self.fileName {
            let dataPath2 = documentsDirectory2.appendingPathComponent("Traces")
            let fileName = "\(name).txt"
            return dataPath2.appendingPathComponent(fileName)
        }
        return nil
    }
    
    var recordingStatus: RecordingStatus = .idle
//        {
//        get {
//            let realm = try! Realm()
//            if let isRecording = realm.objects(NavigationState.self).first?.recordingStatus {
//                return RecordingStatus(rawValue: isRecording) ?? .idle
//            }
//            return .idle
//        }
//        set {
//            let realm = try! Realm()
//            try! realm.write {
//                var navigationState = realm.objects(NavigationState.self).first
//                if let course = NavigationManager.shared.route.value, course.id == "", navigationState == nil {
//                    navigationState = NavigationState(course: course)
//                    realm.add(navigationState!)
//                }
//                navigationState?.recordingStatus = newValue.rawValue
//            }
//        }
//    }
    
    enum RecordingStatus: String {
        case idle
        case play
        case pause
    }
    
    init() {
        LocationManager.shared.locationVariable  // save user position
            .withPrevious()
            .subscribe(onNext: { [weak self] (previousLocation, newLocation) in
                guard newLocation != nil && self != nil else {
                    return
                }
                if self!.previousLocation == nil {
                    self!.previousLocation = newLocation
                }
                if let previousPos = previousLocation, !Utils.equals(location1: newLocation, location2: previousPos) {
                    if TraceManager.shared.recordingStatus == .play, previousPos != nil, self!.previousLocation != nil {
                        let distance = newLocation!.distance(from: self!.previousLocation!)
                        if distance > Double(Settings.shared.traceSensitivity) {
                            self!.saveLocation(location: newLocation!)
                            self!.previousLocation = newLocation!
                        }
                    }
                }
            }).disposed(by: disposeBag)
    }
    
    func startManager() {
        self.previousLocation = LocationManager.shared.getCurrentPosition() // reset previous location with the cuurent position
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd_HH:mm:ss"
        let dateString = formatter.string(from: Date()) // string purpose I add here
        self.fileName = "\(dateString)"
        if let course = NavigationManager.shared.route.value as? RaceCourse { // Save mark in the file in SARA Regatta
            self.saveRouteInfo()
        }
    }
    
    func saveRouteInfo() {
        if let course = NavigationManager.shared.route.value {
            self.writeToFile("Name,\(course.name)")
            course.points.forEach({
                if let coursePoint = $0.point as? CoursePoint  {
                    let lat = coursePoint.gpsPosition?.latitude != nil ? "\(coursePoint.gpsPosition!.latitude)" : ""
                    let long = coursePoint.gpsPosition?.longitude != nil ? "\(coursePoint.gpsPosition!.longitude)" : ""
                    let stringToSave = "CP,\(coursePoint.name),\(lat),\(long)"
                    self.writeToFile(stringToSave)
                } else if let mark = $0.point as? Mark {
                    var stringToSave = "M,\(mark.name)"
                    mark.buoys.forEach({
                        let lat = $0.gpsPosition?.latitude != nil ? "\($0.gpsPosition!.latitude)" : ""
                        let long = $0.gpsPosition?.longitude != nil ? "\($0.gpsPosition!.longitude)" : ""
                        let str = ",\(lat),\(long),\($0.toLetAt(inversed: (NavigationManager.shared.route.value as? CruiseCourse)?.reversed))"
                        stringToSave.append(str)
                    })
                    self.writeToFile(stringToSave)
                }
            })
        }
    }
    
    func saveLocation(location: CLLocation?) {
        if let position = location?.coordinate {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'"
            let stringToSave = "Pos,\(position.latitude),\(position.longitude),\(formatter.string(from: Date())),\(location!.course),\(location!.speed)"
            self.writeToFile(stringToSave)
        }
    }
    
    func stopManager() {
        self.fileName = nil
    }
    
    func writeToFile(_ message: String)  {
        guard let logFile = traceFile else {
            return
        }
       
        guard let data = (message + "\n").data(using: .utf8) else {
            return
        }
        if FileManager.default.fileExists(atPath: logFile.path) {
            if let fileHandle = try? FileHandle(forWritingTo: logFile) {
                fileHandle.seekToEndOfFile()
                fileHandle.write(data)
                fileHandle.closeFile()
            }
        } else {
            do {
                try data.write(to: logFile, options: .atomicWrite)
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    func deleteFile(fileURL: URL) {
        do {
            try FileManager.default.removeItem(at: fileURL) // Delete txt file
            var dataPath = fileURL.deletingPathExtension() // Delete gpx file if exists
            dataPath.appendPathExtension("gpx")
            try FileManager.default.removeItem(at: dataPath)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func getAllTracesFiles() -> [URL]? {
        let dirPathNoScheme = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        guard let documentsDirectory = URL(string: dirPathNoScheme) else { return nil }
//        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }
        let dataPath = documentsDirectory.appendingPathComponent("Traces")
        if FileManager.default.fileExists(atPath: dataPath.absoluteString) {
            do {
                let directoryContents = try FileManager.default.contentsOfDirectory(at: dataPath, includingPropertiesForKeys: nil, options: []).filter{ $0.pathExtension == "txt" }

                // Print the urls of the files contained in the documents directory
//                print(directoryContents)
                return directoryContents.sorted(by: {$0.absoluteString > $1.absoluteString})
            } catch {
                print("Could not search for urls of files in documents directory: \(error)")
                return nil
            }
        }
        return nil
    }
    
    func readFile(fileURL: URL) -> [MapAnnotation]? {
        if let s1 = try? Data(contentsOf: fileURL) {
            // Convert the data back into a string
            if let savedString = String(data: s1, encoding: .utf8) {
                let stringArray = savedString.components(separatedBy: "\n").filter({!$0.isEmpty})
                var mapAnnotations: [MapAnnotation] = []
                stringArray.forEach({
                    let r = $0.components(separatedBy: ",")
                    switch r[0] {
                    case "CP":
                        if r.count == 4 {
                            if r[2] != "" && r[3] != "" {
                                let mapAnnotation = MapAnnotation(latitude: Double(r[2]) ?? 0, longitude: Double(r[3]) ?? 0, type: .coursePoint)
                                mapAnnotation.title = r[1]
                                mapAnnotations.append(mapAnnotation)
                            }
                        }
                        break
                    case "M":
                        if r.count >= 5 {
                            if r[2] != "" && r[3] != "" {
                                let mapAnnotation = MapAnnotation(latitude: Double(r[2]) ?? 0, longitude: Double(r[3]) ?? 0, type: .mark)
                                mapAnnotation.title = "\(r[1]) - \(r[4])"
                                mapAnnotations.append(mapAnnotation)
                            }
                            if r.count > 7 {
                                if r[5] != "" && r[6] != "" {
                                    let mapAnnotation = MapAnnotation(latitude: Double(r[5]) ?? 0, longitude: Double(r[6]) ?? 0, type: .mark)
                                    mapAnnotation.title = "\(r[1]) - \(r[7])"
                                    mapAnnotations.append(mapAnnotation)
                                }
                            }
                        }
                        break
                    case "Pos":
                        if r.count >= 3 {
                            if r[1] != "" && r[2] != "" {
                                let mapAnnotation = MapAnnotation(latitude: Double(r[1]) ?? 0, longitude: Double(r[2]) ?? 0, type: .userLocation)
                                mapAnnotations.append(mapAnnotation)
                            }
                        }
                        break
                    default:
                        break
                    }
                })
                return mapAnnotations
            }
        }
        return nil
    }
    
    func convertToGPX(fileURL: URL) -> URL?{
        if let s1 = try? Data(contentsOf: fileURL) {
            // Convert the data back into a string
            if let savedString = String(data: s1, encoding: .utf8) {
                let stringArray = savedString.components(separatedBy: "\n").filter({!$0.isEmpty})
                var stringToSave = "<?xml version='1.0' encoding='UTF-8' standalone='no' ?><gpx xmlns='http://www.topografix.com/GPX/1/1' xmlns:gpxtpx='http://www.garmin.com/xmlschemas/TrackPointExtension/v1' creator='sail and race audioguide' version='1.1' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:schemaLocation='http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd'>"
                var gpxPos = ""
                var name = ""
                stringArray.forEach({
                    let r = $0.components(separatedBy: ",")
                    switch r[0] {
                    case "Name":
                        if let range = $0.range(of: "Name,") {
                            name = String($0[range.upperBound...])
                        }
                    case "CP":
                        if r.count == 4 {
                            if r[2] != "" && r[3] != "" {
                                stringToSave.append("<wpt lat='\(r[2])' lon='\(r[3])'><name>\(r[1])</name><desc>Course point</desc></wpt>")
                            }
                        }
                        break
                    case "M":
                        if r.count >= 5 {
                            if r[2] != "" && r[3] != "" {
                                stringToSave.append("<wpt lat='\(r[2])' lon='\(r[3])'><name>\(r[1]) - \(r[4])</name><desc>Mark</desc></wpt>")
                            }
                            if r.count > 7 {
                                if r[5] != "" && r[6] != "" {
                                    stringToSave.append("<wpt lat='\(r[5])' lon='\(r[6])'><name>\(r[1]) - \(r[7])</name><desc>Mark</desc></wpt>")
                                }
                            }
                        }
                        break
                    case "Pos":
                        if r.count >= 4 {
                            if r[1] != "" && r[2] != "" && r[3] != "" {
                                gpxPos.append("<trkpt lat='\(r[1])' lon='\(r[2])'><time>\(r[3])</time></trkpt>")
                            }
                        }
                        if r.count == 3 { // The time was not set at the begining 
                            if r[1] != "" && r[2] != "" {
                                gpxPos.append("<trkpt lat='\(r[1])' lon='\(r[2])'><time></time></trkpt>")
                            }
                        }
                        break
                    default:
                        break
                    }
                })
                stringToSave.append("<trk><name>\(name)</name><trkseg>\(gpxPos)</trkseg></trk></gpx>")
                
                var dataPath = fileURL.deletingPathExtension()
                dataPath.appendPathExtension("gpx")
                do {
                    try stringToSave.data(using: .utf8)?.write(to: dataPath, options: .atomicWrite)
                    return dataPath
                } catch {
                    print(error.localizedDescription)
                    return nil
                }
            }
        }
        return nil
    }
}
