//
//  TellTalesManager.swift
//  SARA Croisiere
//
//  Created by Tristan Derbanne on 11.07.19.
//  Copyright © 2019 Rose. All rights reserved.
//

import Foundation
import RealmSwift

protocol TellTaleManagerObserver: AnyObject {
    func tellTaleManager(didUpdateTellTales updatedTellTales: [TellTale])
    func tellTaleManager(didDeleteTellTales deletedTellTales: [TellTale])
}

class TellTalesManager {
    
    // MARK: - Singleton and initializer
    // Singleton
    static let `default` = TellTalesManager()
    
    // Detected tell tales
    private var _tellTales: [TellTale] = []
    var tellTales: [TellTale] {
        get {
            return _tellTales
        }
    }
    
    private init() {
        // Loading local telltales
        let realm = try! Realm()
        _tellTales = Array(realm.objects(TellTale.self))
    }
    
    // MARK: - Telltales connectivity
    // Tell Tale message
    struct TellTaleUpdate {
        let eTellTaleId: String
        let batteryTensionVolts: Float
        let rawData: String
        
        init?(tellTaleMessage msg: String) {
            if msg.prefix(1) == "N" { // Telltale new sentence
                let splitValues = msg.split(separator: ";")
                guard splitValues.count >= 9 else {
                    return nil
                }
                eTellTaleId = String(splitValues[2])
                batteryTensionVolts = Float(splitValues[4]) ?? 0
                rawData = String(splitValues[3])
                return
            }
            guard let splitValues = msg.split(separator: "*").first?.split(separator: ",") else {
                return nil
            }
            guard splitValues.count >= 9 else {
                return nil
            }
            
            eTellTaleId = String(splitValues[2])
            batteryTensionVolts = Float(splitValues[4]) ?? 0
            rawData = String(splitValues[8])
        }
    }
    
    // Tracking online telltales to notice connection loss
    static let tellTaleConnectionLossTime: Double? = 60 // telltale considered to be lost if no update for 60 sexonds
    fileprivate var _tellTalesLastUpdate: [String: Date] = [:]
    fileprivate var _tellTalesCheckerTimer: Timer?
    @objc fileprivate func checkOnlineTelltales() {
        guard TellTalesManager.tellTaleConnectionLossTime != nil else { return }
        let now = Date()
        for id in _tellTalesLastUpdate.filter({ now.timeIntervalSince($0.value) > TellTalesManager.tellTaleConnectionLossTime! }).map({ $0.key }) {
            if let i = _tellTales.firstIndex(where: { $0.id == id }) {
                _tellTales[i].isOnline = false
                if !_tellTales[i].isConfigured {
                    self.observers.forEach {
                        $0.tellTaleManager(didDeleteTellTales: [_tellTales[i]])
                    }
                    _tellTales.remove(at: i)
                }   
                else {
                    self.observers.forEach {
                        $0.tellTaleManager(didUpdateTellTales: [_tellTales[i]])
                    }
                }
            }
            _tellTalesLastUpdate.removeValue(forKey: id)
        }
    }
    
    // Parsing TellTales messages
    func parse(tellTaleMessage msg: String) {
        guard msg.count > 6 else {
            return
        }
        guard msg[msg.index(after: msg.startIndex)..<msg.index(msg.startIndex, offsetBy: 6)] == "PENON" || msg.prefix(1) == "N" else {
            return
        }
        
        if let update = TellTaleUpdate(tellTaleMessage: msg) {
            // Trigger online telltales checker if not yet done
            if _tellTalesCheckerTimer == nil && TellTalesManager.tellTaleConnectionLossTime != nil {
                _tellTalesCheckerTimer = Timer.scheduledTimer(timeInterval: TellTalesManager.tellTaleConnectionLossTime!, target: self, selector: #selector(checkOnlineTelltales), userInfo: nil, repeats: true)
            }
            let updatedTellTale: TellTale
            if let i = self.tellTales.firstIndex(where: {$0.id == update.eTellTaleId}) {
                self.tellTales[i].update(update)
                self.tellTales[i].isOnline = true
                if TellTalesManager.tellTaleConnectionLossTime != nil {
                    self._tellTalesLastUpdate[update.eTellTaleId] = Date()
                }
                updatedTellTale = self.tellTales[i]
            }
            else {
                updatedTellTale = TellTale.tellTale(tellTaleUpdate: update)
                updatedTellTale.isOnline = true
                self._tellTales.append(updatedTellTale)
            }
            self.observers.forEach {
                $0.tellTaleManager(didUpdateTellTales: [updatedTellTale])
            }
        }
    }
    
    // MARK: - Updating telltales
    func enableTelltale(_ tellTale: TellTale, enable: Bool = true) {
        let realm = try! Realm()
        try! realm.write {
            tellTale.isEnabled = enable
        }
        self.observers.forEach {
            $0.tellTaleManager(didUpdateTellTales: [tellTale])
        }
    }
    
    func saveTelltale(_ tellTale: TellTale, settings: TellTaleConfigurationViewController.TelltaleSettings) {
        let realm = try! Realm()
        try! realm.write {
            tellTale.name = settings.name
            tellTale.idName = settings.idName
            tellTale.medianSamplingTime = settings.medianSamplingTime
            tellTale.attachedThreshold = settings.attachedThreshold
            tellTale.turbulentThreshold = settings.turbulentThreshold
            tellTale.angleComputationSlope = settings.angleComputationSlope
            tellTale.angleComputationYIntercept = settings.angleComputationYIntercept
            tellTale.statusMethod = settings.statusMethod
            realm.add(tellTale)
        }
        self.observers.forEach {
            $0.tellTaleManager(didUpdateTellTales: [tellTale])
        }
    }
    
    
    func deleteTellTale(_ tellTale: TellTale) {
        if let i = _tellTales.firstIndex(of: tellTale) {
            _tellTales.remove(at: i)
            self._tellTalesLastUpdate.removeValue(forKey: tellTale.id)
            self.observers.forEach {
                $0.tellTaleManager(didDeleteTellTales: [tellTale])
            }
        }
        let realm = try! Realm()
        try! realm.write {
            tellTale.name = ""
            realm.delete(tellTale)
        }
    }
    
    // TODO: REMOVE _WHEN TESTING OVER
    func fakeTelltale() -> TellTale {
        let id = String(UUID().uuidString.prefix(6))
        let tellTale = TellTale(id: id)
        tellTale.isOnline = true
        self._tellTales.append(tellTale)
        return tellTale
    }
    
    // MARK: - Observers
    
    // Observers
    private var observers: [TellTaleManagerObserver] = []
    
    func addObserver(_ observer: TellTaleManagerObserver) {
        if !self.observers.contains(where: {$0 === observer}) {
            observers.append(observer)
        }
    }
    
    func removeObserver(_ observer: TellTaleManagerObserver) {
        if let i = self.observers.firstIndex(where: {$0 === observer}) {
            self.observers.remove(at: i)
        }
    }
    
   
    
}

