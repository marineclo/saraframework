//
//  AnnouncementManager.swift
//  SARA Croisiere
//
//  Created by Rose on 30/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit
import RxSwift
import RealmSwift

public class AnnouncementQueueManager: NSObject {
    
    public static let shared = AnnouncementQueueManager()
    private let synth = AVSpeechSynthesizer()
    public var utterance = AVSpeechUtterance(string: "")
    
    let bundle = Utils.bundle(anyClass: AnnouncementQueueManager.self)
    
    private var announces: [Announce] = []
    fileprivate var done: Bool = true
    private let disposeBag = DisposeBag()
    var isRunLaunched: Bool = false
    var isTopPriority: Bool = true
    
    public var muted: Bool = false {
        didSet {
            self.clearFeed()
        }
    }
    
    override init() {
        super.init()
        
        self.synth.delegate = self
        //        self.run()
        
        let queue = DispatchQueue(label: "announceThread", qos: .userInitiated)
        queue.async {
            self.run2()
        }
        
        NavigationManager.shared.hasStarted
            .asObservable()
            .skip(1)
            .filter({ $0 == false })
            .subscribe(onNext: { [weak self] _ in
                self?.clearFeedExceptPriority()
            }).disposed(by: disposeBag)
        
    }
    
    public func addAnnounce(_ announce: Announce) {
        if !muted || announce.type == .mute {
            print("=== There was \(self.announces.count) before \(announce.text)")
            if announce.priority == 0 {
                // If it is a priority announce, remove all anounces except priority ones and add it
                clearFeedExceptPriority()
                var clearedAnnounces = self.announces
                clearedAnnounces.append(announce)
                self.announces = clearedAnnounces
            } else {
                // If it is a simple announce, remove all the ones with the same priority, so that we only have one of its type at once
                //                print("=== Annonces T: \(announce)")
                var clearedAnnounces: [Announce] = []
                if announce.type == .carto {
                    // Check if there is already an announce for this beacon, if yes replace it with the new one
                    if let indexToRemove = self.announces.firstIndex(where: {
                        if let beacon = $0.annonceState?.0 as? Beacon, let beaconNewAnnounce = announce.annonceState?.0 as? Beacon,  beacon.id == beaconNewAnnounce.id {
                            return true
                        }
                        return false
                    }) {
                        self.announces.remove(at: indexToRemove)
                    }
                    clearedAnnounces = self.announces
                } else {
                    clearedAnnounces = self.announces.filter({ $0.type != announce.type })
                }
                clearedAnnounces.append(announce)
                clearedAnnounces.sort { $0.priority < $1.priority }
                //                print("=== Annonces Clear: \(clearedAnnounces.count)")
                self.announces = clearedAnnounces
            }
            //            if !isRunLaunched {
            ////                print("launch run")
            //                isRunLaunched = true
            //                self.run()
            //            }
        }
    }
    
    /// Remove all announces from feed
    public func clearFeed() {
        self.announces = []
    }
    
    /// Remove all announces except priority ones
    public func clearFeedExceptPriority() {
        self.announces = announces.filter({ $0.priority == 0 })
    }
    
    public func run() {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let weakSelf = self else { return }
            //            while weakSelf.announces.isNotEmpty && weakSelf.done {
            //                let announce = weakSelf.announces.remove(at: 0)
            //                var vocalRate: Float = 0.5
            //                DispatchQueue.main.async {
            //                    guard let profileVoice = User.shared.profile.profileTypes.filter({$0 is ProfileVoice}).first as? ProfileVoice else {
            //                        return
            //                    }
            //                    vocalRate = profileVoice.vocalRate.value
            //                    weakSelf.done = false
            //                    print("=== Speak \(announce.text), Voice \(Language.voiceCode())")
            //                    weakSelf.utterance = AVSpeechUtterance(string: announce.text)
            //                    weakSelf.utterance.rate = vocalRate
            //                    weakSelf.utterance.voice = AVSpeechSynthesisVoice(language: Language.voiceCode())
            //                    weakSelf.synth.speak(weakSelf.utterance)
            //                }
            //                usleep(10)
            //                print("=== run: \(weakSelf.announces.count), done: \(weakSelf.done)")
            //            }
            //            weakSelf.isRunLaunched = false
            while true {
                if weakSelf.announces.isNotEmpty && weakSelf.done {
                    let announce = weakSelf.announces.remove(at: 0)
                    //if NavigationManager.shared.hasStarted.value || announce.priority == 0 {
                    var vocalRate: Float = 0.5
                    DispatchQueue.main.async {
                        guard let profileVoice = User.shared.profile.profileTypes.filter({$0 is ProfileVarious}).first as? ProfileVarious else {
                            return
                        }
                        vocalRate = profileVoice.vocalRate.value
                        weakSelf.done = false
                        print("Speak \(announce.text), Voice \(Language.voiceCode())")
                        weakSelf.utterance = AVSpeechUtterance(string: announce.text)
                        weakSelf.utterance.rate = vocalRate
                        weakSelf.utterance.voice = AVSpeechSynthesisVoice(language: Language.voiceCode())
                        weakSelf.synth.speak(weakSelf.utterance)
                    }
                }
                usleep(100000)
            }
        }
    }
    
    public func run2() {
        //            while weakSelf.announces.isNotEmpty && weakSelf.done {
        //                let announce = weakSelf.announces.remove(at: 0)
        //                var vocalRate: Float = 0.5
        //                DispatchQueue.main.async {
        //                    guard let profileVoice = User.shared.profile.profileTypes.filter({$0 is ProfileVoice}).first as? ProfileVoice else {
        //                        return
        //                    }
        //                    vocalRate = profileVoice.vocalRate.value
        //                    weakSelf.done = false
        //                    print("=== Speak \(announce.text), Voice \(Language.voiceCode())")
        //                    weakSelf.utterance = AVSpeechUtterance(string: announce.text)
        //                    weakSelf.utterance.rate = vocalRate
        //                    weakSelf.utterance.voice = AVSpeechSynthesisVoice(language: Language.voiceCode())
        //                    weakSelf.synth.speak(weakSelf.utterance)
        //                }
        //                usleep(10)
        //                print("=== run: \(weakSelf.announces.count), done: \(weakSelf.done)")
        //            }
        //            weakSelf.isRunLaunched = false
        while true {
            if self.announces.isNotEmpty && self.done {
                let announce = self.announces.remove(at: 0)
                //if NavigationManager.shared.hasStarted.value || announce.priority == 0 {
                var vocalRate: Float = 0.5
                DispatchQueue.main.async {
                    guard let profileVoice = User.shared.profile.profileTypes.filter({$0 is ProfileVarious}).first as? ProfileVarious else {
                        return
                    }
                    vocalRate = profileVoice.vocalRate.value / 100
                    self.done = false
                    self.isTopPriority = announce.priority == 0
                    let text = self.textForAnnonce(annonce: announce)
                    print("Speak \(text)")
                    self.utterance = AVSpeechUtterance(string: text)
                    self.utterance.rate = vocalRate
                    self.utterance.voice = AVSpeechSynthesisVoice(language: Language.voiceCode())
                    self.synth.speak(self.utterance)
                }
            }
            usleep(100000)
        }
        
    }
    
    func textForAnnonce(annonce: Announce) -> String {
        switch annonce.type {
        // GPS
        case .compass:
            return annonceAngleType(newValue: AnnouncementManager.shared.compassVariable.value, text: annonce.text)
        case .courseOverGround:
            return annonceAngleType(newValue: AnnouncementManager.shared.courseOverGroundVariable.value, text: annonce.text)
        case .speedOverGround:
            return annonceSpeedType(newValue: AnnouncementManager.shared.speedOverGroundVariable.value, announce:   annonce)
        case .maxSpeedOverGround:
            return annonceSpeedType(newValue: AnnouncementManager.shared.maxSpeedOverGroundVariable.value, announce:   annonce)
        // Route
        case .azimut1, .gisement1, .distance1, .CMG1, .gisementTime1, .gisementCombine1:
            var buoyNumber = 1
            if let course = NavigationManager.shared.route.value as? CruiseCourse, course.reversed {
                buoyNumber = 2
            }
            switch annonce.type {
            case .azimut1:
                return annonceAngleWithUnit(newValue: AnnouncementManager.shared.azimutVariable.value, text: annonce.text, buoyNumber: buoyNumber)
            case .gisement1:
                return annonceAngleWithUnit(newValue: AnnouncementManager.shared.gisementVariable.value?[0], text: annonce.text, buoyNumber: buoyNumber)
            case .gisementTime1:
                return annonceAngleWithUnit(newValue: AnnouncementManager.shared.gisementVariable.value?[1], text: annonce.text, buoyNumber: buoyNumber)
            case .gisementCombine1:
                return annonceAngleWithUnit(newValue: AnnouncementManager.shared.gisementVariable.value, text: annonce.text, buoyNumber: buoyNumber)
            case .distance1:
                return annonceDistanceType(newValue: AnnouncementManager.shared.distanceVariable.value, annonce: annonce, buoyNumber: buoyNumber)
            case .CMG1:
                return annonceSpeedType(newValue: AnnouncementManager.shared.CMGVariable.value, announce: annonce, buoyNumber: buoyNumber)
            default:
                return ""
            }
        case .azimut2, .gisement2, .distance2, .CMG2, .gisementTime2, .gisementCombine2:
            var buoyNumber = 2
            if let course = NavigationManager.shared.route.value as? CruiseCourse, course.reversed {
                buoyNumber = 1
            }
            switch annonce.type {
            case .azimut2:
                return annonceAngleWithUnit(newValue: AnnouncementManager.shared.azimutVariable2.value, text: annonce.text, buoyNumber: buoyNumber)
            case .gisement2:
                return annonceAngleWithUnit(newValue: AnnouncementManager.shared.gisementVariable2.value?[0], text: annonce.text, buoyNumber: buoyNumber)
            case .gisementTime2:
                return annonceAngleWithUnit(newValue: AnnouncementManager.shared.gisementVariable2.value?[1], text: annonce.text, buoyNumber: buoyNumber)
            case .gisementCombine2:
                return annonceAngleWithUnit(newValue: AnnouncementManager.shared.gisementVariable.value, text: annonce.text, buoyNumber: buoyNumber)
            case .distance2:
                return annonceDistanceType(newValue: AnnouncementManager.shared.distanceVariable2.value, annonce: annonce, buoyNumber: buoyNumber)
            case .CMG2:
                return annonceSpeedType(newValue: AnnouncementManager.shared.CMGVariable2.value, announce: annonce, buoyNumber: buoyNumber)
            default:
                return ""
            }
        case .gapSegment:
            return annonceDistanceType(newValue: AnnouncementManager.shared.gapSegmentVariable.value, annonce: annonce, buoyNumber: 1)
        case .entry3Lengths:
            return annonce.text
        // Navigation Central
        case .headingMagnetic, .speedThroughWater, .trueWindSpeed, .trueWindAngle, .apparentWindSpeed, .apparentWindAngle, .windDirection, .waterTemperature, .airTemperature:
            if !NmeaManager.default.connected.value {
                return "\(annonce.text) \(NSLocalizedString("unavailable_nav", bundle: self.bundle, comment: ""))"
            }
            switch annonce.type {
            case .headingMagnetic:
                return annonceAngleType(newValue: AnnouncementManager.shared.headingMagneticVariable.value, text: annonce.text)
            case .speedThroughWater:
                return annonceSpeedType(newValue: AnnouncementManager.shared.speedThroughWaterVariable.value, announce: annonce)
            case .trueWindSpeed:
                return annonceSpeedType(newValue: AnnouncementManager.shared.trueWindSpeedVariable.value, announce: annonce)
            case .trueWindAngle:
                return annonceAngleWithUnit(newValue: AnnouncementManager.shared.trueWindAngleVariable.value, text: annonce.text)
            case .apparentWindSpeed:
                return annonceSpeedType(newValue: AnnouncementManager.shared.apparentWindSpeedVariable.value, announce: annonce)
            case .apparentWindAngle:
                return annonceAngleWithUnit(newValue: AnnouncementManager.shared.apparentWindAngleVariable.value, text: annonce.text)
            case .windDirection:
                return annonceAngleType(newValue: AnnouncementManager.shared.windDirectionAngleVariable.value, text: annonce.text)
            case .waterTemperature:
                return annonceSpeedType(newValue: AnnouncementManager.shared.waterTemperatureVariable.value, announce: annonce, unit: NavigationUnit.degrees.description)
            case .airTemperature:
                return annonceSpeedType(newValue: AnnouncementManager.shared.airTemperatureVariable.value, announce: annonce, unit: NavigationUnit.degrees.description)
            default:
                return ""
            }
        case .depth:
            var value = ""
            if !NmeaManager.default.connected.value {
                value = NSLocalizedString("unavailable_nav", bundle: self.bundle, comment: "")
            } else if let depth = AnnouncementManager.shared.depthVariable.value {
                value = "\(Utils.formattedFloatAnnonce(float: depth, unit: NavigationUnit.meter.description))"
            } else {
                value = NSLocalizedString("unavailable_nav", bundle: self.bundle, comment: "")
            }
            return "\(NSLocalizedString("depth", bundle: self.bundle, comment: "")) \(value)"
        // Telltales
        case .tellTaleAngle:
            // Get the right value
            if let id = annonce.annonceState?.0 as? String, let telltale = TellTalesManager.default.tellTales.first(where: {$0.id == id}), let angle = telltale.angle {
                return annonceAngleType(newValue: Int(angle), text: annonce.text)
            }
            return ""
        case .tellTaleMediane:
            // Get the right value
            if let id = annonce.annonceState?.0 as? String, let telltale = TellTalesManager.default.tellTales.first(where: {$0.id == id}), let medianAngle = telltale.medianAngle {
                return annonceAngleType(newValue: Int(medianAngle), text: annonce.text)
            }
            return ""
        case .tellTaleAverage:
            // Get the right value
            if let id = annonce.annonceState?.0 as? String, let telltale = TellTalesManager.default.tellTales.first(where: {$0.id == id}), let averageAngle = telltale.averageAngle  {
                return annonceAngleType(newValue: Int(averageAngle), text: annonce.text)
            }
            return ""
        case .tellTaleStandardVariation:
            // Get the right value
            if let id = annonce.annonceState?.0 as? String, let telltale = TellTalesManager.default.tellTales.first(where: {$0.id == id}), let standardDeviationAngle = telltale.standardDeviationAngle {
                return annonceAngleType(newValue: Int(standardDeviationAngle), text: annonce.text)
            }
            return ""
        case .tellTaleVariationMax:
            // Get the right value
            if let id = annonce.annonceState?.0 as? String, let telltale = TellTalesManager.default.tellTales.first(where: {$0.id == id}), let variationAngle = telltale.variationAngle {
                return annonceAngleType(newValue: Int(variationAngle), text: annonce.text)
            }
            return ""
        case .tellTaleState:
            // Get the right value
            if let id = annonce.annonceState?.0 as? String, let telltale = TellTalesManager.default.tellTales.first(where: {$0.id == id}) {
                return "\(annonce.text) \(telltale.status.description)"
            }
            return ""
        // Carto
        case .carto:
            guard let profileCarto = User.shared.profile.profileTypes.first(where: {$0 is ProfileCarto}) as? ProfileCarto, let currentLoc = LocationManager.shared.getCurrentPosition() else {
                return ""
            }
            var text: String = ""
            if let beacon = annonce.annonceState?.0 as? Beacon {
                text += "\(beacon.type.description) \(beacon.name): "
                if let distanceOn = profileCarto.distance?.value.isActivated, distanceOn {
                    let distance = beacon.distance(currentLoc: currentLoc)
                    let tupleDistance = Utils.tupleDistance(floatDistance: distance)
                    if tupleDistance == nil {
                        let value = NSLocalizedString("unavailable_nav", bundle: bundle, comment: "")
                        text += "\(NSLocalizedString("distance", bundle: bundle, comment: "")) \(value), "
                    } else if tupleDistance!.1 == .meter {
                        text += "\(tupleDistance!.0.roundDown) \(tupleDistance!.1!.description), "
                    } else {
                        text += "\(Utils.formattedFloatAnnonce(float: tupleDistance!.0, unit: tupleDistance!.1!.description)), "
                    }
                }
                var bearing: [(Int, NavigationUnit?)?]?
                var bearingUnavailable: Bool = false
                if let gisementTimeOn = profileCarto.gisementTime?.value.isActivated, gisementTimeOn {
                    bearing = beacon.bearing(currentLoc: currentLoc)
                    let gisementTime = bearing?[2]
                    let unit = gisementTime?.1?.description ?? ""
                    var value = ""
                    if gisementTime != nil {
                        value = "\(NSLocalizedString("relative_bearing", bundle: bundle, comment: "")) \(gisementTime!.0) \(unit), "
                        bearingUnavailable = false
                    } else {
                        bearingUnavailable = true
                    }
                    text += "\(value)"
                }
                if let gisementOn = profileCarto.gisement?.value.isActivated, gisementOn {
                    if bearing == nil {
                        bearing = beacon.bearing(currentLoc: currentLoc)
                    }
                    let gisement = bearing?[1]
                    let unit = gisement?.1?.description ?? ""
                    var value = ""
                    if gisement != nil {
                        value = "\(gisement!.0) \(unit), "
                        bearingUnavailable = false
                    } else {
                        bearingUnavailable = true
                    }
                    text += "\(value)"
                }
                if bearingUnavailable { // Add only one time: relative bearing unavailable
                    text += "\(NSLocalizedString("relative_bearing", bundle: bundle, comment: "")) \(NSLocalizedString("unavailable_nav", bundle: bundle, comment: "")), "
                }
                if let azimutOn = profileCarto.azimut?.value.isActivated, azimutOn {
                    let azimut = beacon.bearing(currentLoc: currentLoc)?[0]
                    let unit = azimut?.1?.description ?? ""
                    var value = ""
                    if azimut != nil {
                        value = "\(azimut!.0) \(unit)"
                    } else {
                        value = "\(NSLocalizedString("azimuth", bundle: bundle, comment: "")) \(NSLocalizedString("unavailable_nav", bundle: bundle, comment: ""))"
                    }
                    text += "\(value)"
                }
                return text
            }
            return ""
        case .start:
            guard let gpsPosition1 = (NavigationManager.shared.point.value?.point as? Mark)?.buoys.first?.gpsPosition, let gpsPosition2 = (NavigationManager.shared.point.value?.point as? Mark)?.buoys.last?.gpsPosition, let location = LocationManager.shared.getCurrentPosition() else { return ""}
            
            let locGPSPos = GPSPosition(latitude: Float(location.coordinate.latitude), longitude: Float(location.coordinate.longitude))
            
            let distanceToStartingLine = Int(NavigationHelper.getDistanceSegmentFrom(gpsPosition1: gpsPosition1, gpsPosition2: gpsPosition2, location: locGPSPos) ?? 0)
            let boatPositionAnnouce = AnnouncementManager.shared.startMessage.joined(separator: " ")
            let distanceCom = Utils.tupleDistance(floatDistance: AnnouncementManager.shared.distanceVariable2.value)
            let distancePin = Utils.tupleDistance(floatDistance: AnnouncementManager.shared.distanceVariable.value)
            var textBearingDistance: String = ""
            var textDistanceU: String = ""
            let isMatchRace = NavigationManager.shared.route.value != nil && NavigationManager.shared.route.value!.isMatchRace()
            let namePin = isMatchRace ? Utils.localizedString(forKey: "blue_buoy", app: "SARARegate") : NSLocalizedString("pin", bundle: bundle, comment: "")
            let nameCommittee = isMatchRace ? Utils.localizedString(forKey: "yellow_buoy", app: "SARARegate") : NSLocalizedString("committee", bundle: bundle, comment: "")
            if AnnouncementManager.shared.startMessage.count == 1 && NavigationManager.shared.totalTime > 45 && ( !isMatchRace || (isMatchRace && NavigationManager.shared.totalTime < 240)) { // The boat is between the pin and committee or in match race after the 4min
                textBearingDistance = "\(nameCommittee) \(annonceBearing(bearing: AnnouncementManager.shared.gisementVariable2.value)), \(annonceDistanceStart(tupleDistance: distanceCom)). \(namePin) \(annonceBearing(bearing: AnnouncementManager.shared.gisementVariable.value)), \(annonceDistanceStart(tupleDistance: distancePin))"
            } else {
                let dist1 = AnnouncementManager.shared.distanceVariable.value // pin
                let dist2 = AnnouncementManager.shared.distanceVariable2.value // comittee
                if dist1 != nil && dist2 != nil {
                    if dist1! < dist2! { // Outside: close to the pin
                        textBearingDistance = "\(namePin) \(annonceBearing(bearing: AnnouncementManager.shared.gisementVariable.value)), \(annonceDistanceStart(tupleDistance: distancePin))"
                        if isMatchRace && NavigationManager.shared.totalTime > 240 {
                            let distanceU = Int(NavigationHelper.getDistanceSegmentFrom(gpsPosition1: gpsPosition1, gpsPosition2: CourseManager.shared.gpsPosUPin, location: locGPSPos) ?? 0)
                            textDistanceU = "\(NSLocalizedString("distanceU", bundle: bundle, comment: "")) \(distanceU)"
                        }
                    } else { // Outside: close to the committe
                        textBearingDistance = "\(nameCommittee) \(annonceBearing(bearing: AnnouncementManager.shared.gisementVariable2.value)), \(annonceDistanceStart(tupleDistance: distanceCom))"
                        // if matchRace add distance to the U before 4min
                        if isMatchRace && NavigationManager.shared.totalTime > 240 {
                            let distanceU = Int(NavigationHelper.getDistanceSegmentFrom(gpsPosition1: gpsPosition2, gpsPosition2: CourseManager.shared.gpsPosUCommitee, location: locGPSPos) ?? 0)
                            textDistanceU = "\(NSLocalizedString("distanceU", bundle: bundle, comment: "")) \(distanceU)"
                        }
                    }
                }
            }
            
            // In the last 15s, remove units to have short annouce
            if NavigationManager.shared.totalTime < 45 && NavigationManager.shared.totalTime > 10 {
                return "\(NSLocalizedString("time", bundle: bundle, comment: "")) \(NavigationManager.shared.totalTime) . \(boatPositionAnnouce) \(distanceToStartingLine) . \(textBearingDistance)."
            } else if NavigationManager.shared.totalTime < 10 && NavigationManager.shared.totalTime > 0 {
                return "\(NSLocalizedString("time", bundle: bundle, comment: "")) \(NavigationManager.shared.totalTime) . \(boatPositionAnnouce) \(distanceToStartingLine)."
            }
            let seconds: Int = NavigationManager.shared.totalTime % 60
            let minutes: Int = (NavigationManager.shared.totalTime / 60) % 60
            let textMin = minutes > 0 ? "\(minutes) \(NSLocalizedString("unit_minutes", bundle: bundle, comment: ""))" : "" // in the last 60s don't say the infos on the minutes
            // Add time only if the course has started and the timer is not finished
            let timeAnnonce = NavigationManager.shared.totalTime > 0 && NavigationManager.shared.hasStarted.value ? "\(NSLocalizedString("time", bundle: bundle, comment: "")) \(textMin) \(seconds == 0 ? " ." : String(seconds)) ." : ""
            if textDistanceU.isNotEmpty { // Add boat position
                return "\(timeAnnonce) \(boatPositionAnnouce)\(boatPositionAnnouce == "" ? "" : ",") \(textDistanceU) \(NSLocalizedString("unit_meter", bundle: bundle, comment: "")) . \(textBearingDistance)."
            } else {
                return "\(timeAnnonce) \(boatPositionAnnouce) \(distanceToStartingLine) \(NSLocalizedString("unit_meter", bundle: bundle, comment: "")) . \(textBearingDistance)."
            }
        case .axisCrossed:
            // Get coordinates of point to cross. In case of double mark, use middle
            let coordCrossPoint = NavigationManager.shared.point.value?.point.getCoordinates(method: .middle)
            guard let distance = NavigationHelper.getDistanceFrom(location: LocationManager.shared.getCurrentPosition(), to: coordCrossPoint) else { return "" }
            return "\(annonce.text) \(Int(distance)) \(NSLocalizedString("unit_meter", bundle: bundle, comment: ""))"
        default:
            return annonce.text
        }
    }
    
    public func annonceDistanceStart(tupleDistance: (Float, NavigationUnit?)?) -> String {
        if tupleDistance == nil {
            let value = NSLocalizedString("unavailable_nav", bundle: bundle, comment: "")
            return "\(NSLocalizedString("distance", bundle: bundle, comment: "")) \(value)"
        } else if tupleDistance!.1 == .meter {
            return "\(tupleDistance!.0.roundDown) \(tupleDistance!.1!.description)"
        } else {
            return "\(Utils.formattedFloatAnnonce(float: tupleDistance!.0, unit: tupleDistance!.1!.description))"
        }
    }
    
    func annonceBearing(bearing: [(Int, NavigationUnit?)?]?) -> String {
        if bearing == nil || bearing![0] == nil{
            let value = NSLocalizedString("unavailable_nav", bundle: bundle, comment: "")
            return "\(NSLocalizedString("gisement", bundle: bundle, comment: "")) \(value)"
        } else if bearing![0] != nil {
            return "\(bearing![0]!.0) \(bearing![0]!.1!.description)"
        }
        return ""
    }
    
    func annonceAngleType(newValue: Int?, text: String) -> String {
        var value = ""
        if newValue != nil {
            value = "\(newValue!) \(NSLocalizedString("unit_abb_degrees", bundle: bundle, comment: ""))"
        } else {
            value = NSLocalizedString("unavailable_nav", bundle: bundle, comment: "")
        }
        return "\(text) \(value)"
    }
    
    func annonceAngleWithUnit(newValue: [(Int, NavigationUnit?)?]?, text: String, buoyNumber: Int = 0) -> String {
        var value = ""
        var isAvailable: Bool = false
        if newValue != nil {
            newValue!.forEach({
                let unit = $0?.1?.description ?? ""
                if $0 != nil {
                    value += "\($0!.0) \(unit) "
                    isAvailable = true
                } else {
                    isAvailable = false
                }
            })
        }
        if !isAvailable {
            value = NSLocalizedString("unavailable_nav", bundle: bundle, comment: "")
        }
        if let currentPointName = NavigationManager.shared.point.value?.point.name {
            var buoyToLetA = ""
            if let mark = NavigationManager.shared.point.value?.point as? Mark, (mark.markType == Mark.MarkType.double.rawValue || mark.markType == Mark.MarkType.end.rawValue) {
                if NavigationManager.shared.usingMiddlePoint {
                    buoyToLetA = NSLocalizedString("middle", bundle: bundle, comment: "")
                } else {
                    // In match race use buoy blue and yellow instead of starboard and port
                    if let route = NavigationManager.shared.route.value, route.isMatchRace() {
                        buoyToLetA = buoyNumber == 2 ? Utils.localizedString(forKey: "blue_buoy", app: "SARARegate") : Utils.localizedString(forKey: "yellow_buoy", app: "SARARegate")
                    } else {
                        buoyToLetA = buoyNumber == 2 ? NSLocalizedString("unit_starboard", bundle: bundle, comment: "") : NSLocalizedString("unit_port", bundle: bundle, comment: "")
                    }
                }
            }
            return "\(text) \(currentPointName) \(buoyToLetA) \(value)"
        }
        return "\(text) \(value)"
    }
    
    func annonceAngleWithUnit(newValue: (Int, NavigationUnit?)?, text: String, buoyNumber: Int = 0) -> String {
        let unit = newValue?.1?.description ?? ""
        var value = ""
        if newValue != nil {
            value = "\(newValue!.0) \(unit)"
        } else {
            value = NSLocalizedString("unavailable_nav", bundle: bundle, comment: "")
        }
        if let currentPointName = NavigationManager.shared.point.value?.point.name, buoyNumber != 0 {
            var buoyToLetA = ""
            if let mark = NavigationManager.shared.point.value?.point as? Mark, (mark.markType == Mark.MarkType.double.rawValue || mark.markType == Mark.MarkType.end.rawValue) {
                if NavigationManager.shared.usingMiddlePoint {
                    buoyToLetA = NSLocalizedString("middle", bundle: bundle, comment: "")
                } else {
                    // In match race use buoy blue and yellow instead of starboard and port
                    if let route = NavigationManager.shared.route.value, route.isMatchRace() {
                        buoyToLetA = buoyNumber == 2 ? Utils.localizedString(forKey: "blue_buoy", app: "SARARegate") : Utils.localizedString(forKey: "yellow_buoy", app: "SARARegate")
                    } else if mark.markType == Mark.MarkType.end.rawValue {
                        // For finish mark, use finish pin / commitee instead of finish port / starboard
                        buoyToLetA = buoyNumber == 2 ? NSLocalizedString("finish_pin", bundle: bundle, comment: "") : NSLocalizedString("finish_committee", bundle: bundle, comment: "")
                        return "\(text) \(buoyToLetA) \(value)"
                    }
                    else {
                        buoyToLetA = buoyNumber == 2 ? NSLocalizedString("unit_starboard", bundle: bundle, comment: "") : NSLocalizedString("unit_port", bundle: bundle, comment: "")
                    }
                }
            }
            return "\(text) \(currentPointName) \(buoyToLetA) \(value)"
        }
        return "\(text) \(value)"
    }
    
    func annonceSpeedType(newValue: Float?, announce: Announce, unit: String = Settings.shared.speedUnit?.unit.description ?? NavigationUnit.knots.description, buoyNumber: Int = 0) -> String {
        var value = ""
        if newValue != nil {
            if unit == NavigationUnit.kmPerHour.description || announce.type == .apparentWindSpeed || announce.type == .trueWindSpeed { // For speed annonces in km/h, round the value in km/h
                value = "\(Int(newValue!)) \(unit)"
            } else {
                value = "\(Utils.formattedFloatAnnonce(float: newValue!, unit: unit))"
            }
        } else {
            value = NSLocalizedString("unavailable_nav", bundle: bundle, comment: "")
        }
        var buoyToLetA = ""
        if buoyNumber != 0 { // Need to add name in the announce
            if let mark = NavigationManager.shared.point.value?.point as? Mark, (mark.markType == Mark.MarkType.double.rawValue || mark.markType == Mark.MarkType.end.rawValue) {
                if NavigationManager.shared.usingMiddlePoint {
                    buoyToLetA = NSLocalizedString("middle", bundle: bundle, comment: "")
                } else {
                    // In match race use buoy blue and yellow instead of starboard and port
                    if let route = NavigationManager.shared.route.value, route.isMatchRace() {
                        buoyToLetA = buoyNumber == 2 ? "\(mark.name) \(Utils.localizedString(forKey: "blue_buoy", app: "SARARegate"))" : "\(mark.name) \(Utils.localizedString(forKey: "yellow_buoy", app: "SARARegate"))"
                    } else if mark.markType == Mark.MarkType.end.rawValue {
                        // For finish mark, use finish pin / commitee instead of finish port / starboard
                        buoyToLetA = buoyNumber == 2 ? NSLocalizedString("finish_pin", bundle: bundle, comment: "") : NSLocalizedString("finish_committee", bundle: bundle, comment: "")
                    } else {
                        buoyToLetA = buoyNumber == 2 ? "\(mark.name) \(NSLocalizedString("unit_starboard", bundle: bundle, comment: ""))" : "\(mark.name) \(NSLocalizedString("unit_port", bundle: bundle, comment: ""))"
                    }
                }
            } else if let name = NavigationManager.shared.point.value?.point.name{
                buoyToLetA = name
            }
        }
        return "\(announce.text) \(buoyToLetA) \(value)"
    }
    
    func annonceDistanceType(newValue: Float?, annonce: Announce, buoyNumber: Int = 0) -> String {
        let tupleDistance = Utils.tupleDistance(floatDistance: newValue)
        if var currentPointName = NavigationManager.shared.point.value?.point.name {
            var buoyToLetA = ""
            if let mark = NavigationManager.shared.point.value?.point as? Mark, (mark.markType == Mark.MarkType.double.rawValue || mark.markType == Mark.MarkType.end.rawValue) {
                if NavigationManager.shared.usingMiddlePoint || annonce.type == .gapSegment{ // Gap: in case of double mark: always use the middle of the mark.
                    buoyToLetA = NSLocalizedString("middle", bundle: bundle, comment: "")
                } else {
                    // In match race use buoy blue and yellow instead of starboard and port
                    if let route = NavigationManager.shared.route.value, route.isMatchRace() {
                        buoyToLetA = buoyNumber == 2 ? Utils.localizedString(forKey: "blue_buoy", app: "SARARegate") : Utils.localizedString(forKey: "yellow_buoy", app: "SARARegate")
                    } else if mark.markType == Mark.MarkType.end.rawValue {
                        // For finish mark, use finish pin / commitee instead of finish port / starboard
                        currentPointName = buoyNumber == 2 ? NSLocalizedString("finish_pin", bundle: bundle, comment: "") : NSLocalizedString("finish_committee", bundle: bundle, comment: "")
                    }
                    else {
                        buoyToLetA = buoyNumber == 2 ? NSLocalizedString("unit_starboard", bundle: bundle, comment: "") : NSLocalizedString("unit_port", bundle: bundle, comment: "")
                    }
                }
            }
            if tupleDistance == nil {
                let value = NSLocalizedString("unavailable_nav", bundle: bundle, comment: "")
                return "\(annonce.text) \(currentPointName) \(value)"
            } else if tupleDistance!.1 == .meter {
                return "\(annonce.text) \(currentPointName) \(buoyToLetA) \(tupleDistance!.0.roundDown) \(tupleDistance!.1!.description)"
            } else {
                return "\(annonce.text) \(currentPointName) \(buoyToLetA) \(Utils.formattedFloatAnnonce(float: tupleDistance!.0, unit: tupleDistance!.1!.description))"
            }
        }
        return ""
    }
}

extension AnnouncementQueueManager: AVSpeechSynthesizerDelegate {
    public func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        if self.isTopPriority { // If it'a a top priority, remove all the other annoucements in the queue to remove old ones.
            self.clearFeedExceptPriority()
            self.isTopPriority = false
            // There is a delay so try to reset the annoucement to have new one
            //AnnouncementManager.shared.resetCurrentState()
            AnnouncementManager.shared.resetCurrentStateRoute()
        }
        self.done = true
        print("Did finish talking")
    }
}
