//
//  CourseManager.swift
//  SARA Croisiere
//
//  Created by Marine on 27.05.20.
//  Copyright © 2020 GraniteApps. All rights reserved.
//

import Foundation
import CoreLocation
import UTMConversion

public class CourseManager {
    // MARK: - Variables
    public static let shared = CourseManager()
    let bundle = Utils.bundle(anyClass: CourseManager.self)
    
    var course: Course?
    
    var isComing = false
    var isComingU = false // Match Race: entry in the U
    var isEnterU = false // // Match Race: entry in the U
    var isComingLine = false // Match Race: crossing the line
    var isEnterUDone = false // Match Race: enter U done
    var isLineDone = false // Match Race: crossing line done
    var yPbanana = 0
    var boatStatus: [String] = [] // To store the position of the boat
    
    // Match Race
    var gpsPosUPin: GPSPosition?
    var gpsPosUCommitee: GPSPosition?
    
    var validationMarkData: ValidationMarkData? // crossing line
    var axisCourseValidation: ValidationMarkData? // axis of the course for SARA Regate
    var yAxisPrevious: Double = 0 // To store old value of the boat position for axis of the course
    
    // MARK: - Functions
    func resetValues() {
        self.isComingLine = false
        self.isComingU = false
        self.isComing = false
        self.isComingLine = false
        self.isLineDone = false
        self.isEnterUDone = false
        self.boatStatus = []
        self.yPbanana = 0
        self.yAxisPrevious = 0
    }
    
    func activationNextMark() {
        let mark = NavigationManager.shared.point.value?.point as! Mark
        switch mark.markType {
        case Mark.MarkType.simple.rawValue:
            simpleMark()
        case Mark.MarkType.double.rawValue:
            doubleMark()
        case Mark.MarkType.start.rawValue:
            doubleMark(isStart: true)
        case Mark.MarkType.end.rawValue:
            doubleMark()
        default:
            return
        }
    }
    
    func simpleMark() {
        // Get route and if it's a match race
        var isMatchRace: Bool = false
        if let _route = NavigationManager.shared.route.value {
            isMatchRace = _route.isMatchRace()
        }
        if validationMarkData == nil {
            // Reset values
            resetValues()
            
            let buoyB = (NavigationManager.shared.point.value?.point as! Mark).buoys[0]
            // First buoy: if it's the first one:
            //  - use current position as point A for SARA Nav
            //  - for SARA Race, if there is only one mark, use current position for first and next points
            //  - for SARA Race, if there is a next buoy, use this one for first and next points
            var posA: GPSPosition?
            var posC: GPSPosition?
            if let pointA = NavigationManager.shared.getPreviousPointWithCoord()?.point {
                    posA = pointA.getCoordinates(method: .toLetAt, toLetAt: buoyB.toLetAt(inversed: (NavigationManager.shared.route.value as? CruiseCourse)?.reversed), isMatchRace: isMatchRace)
//                print("posA: \(posA)")
            } else if let _ = NavigationManager.shared.route.value as? RaceCourse {
                // there is a next buoy, use this one for first and next points
                if let pointC = NavigationManager.shared.getNextPointWithCoord()?.point {
                    posA = pointC.getCoordinates(method: .toLetAt, toLetAt: buoyB.toLetAt(), isMatchRace: isMatchRace)
                    posC = pointC.getCoordinates(method: .toLetAt, toLetAt: buoyB.toLetAt(), isMatchRace: isMatchRace)
                } else {
                    // only one mark, use current position for first and next points
                    if let currentPos = LocationManager.shared.locationVariable.value?.coordinate {
                        posA = GPSPosition(latitude: Float(currentPos.latitude), longitude: Float(currentPos.longitude))
                        posC = GPSPosition(latitude: Float(currentPos.latitude), longitude: Float(currentPos.longitude))
                    }
                }
            } else if let currentPos = LocationManager.shared.locationVariable.value?.coordinate {
                posA = GPSPosition(latitude: Float(currentPos.latitude), longitude: Float(currentPos.longitude))
            }
            
            // Check if not the last buoy
            if let pointC = NavigationManager.shared.getNextPointWithCoord()?.point, posC == nil {
                posC = pointC.getCoordinates(method: .toLetAt, toLetAt: buoyB.toLetAt(inversed: (NavigationManager.shared.route.value as? CruiseCourse)?.reversed), isMatchRace: isMatchRace)
//                print("posC: \(posC)")
            } else if let _ = NavigationManager.shared.route.value as? RaceCourse, posC == nil {
                posC = posA  // For SARA Race, if there is no next mark, use previous one.
            }
            guard let posB = buoyB.gpsPosition else {
                return
            }
            
            validationMarkData = ValidationMarkData(posA: posA, posB: posB, posC: posC, toLetAt: buoyB.toLetAt(inversed: (NavigationManager.shared.route.value as? CruiseCourse)?.reversed), course: NavigationManager.shared.route.value)
            validationMarkData?.validationLine()
        }
                
        //Reference changement
        guard let UTMCoord = LocationManager.shared.locationVariable.value?.utmCoordinate() else {
            return
        }
        let x = UTMCoord.easting
        let y = UTMCoord.northing
        
        //Convert the current location coordinates in the new reference
        let (xP, yP) = validationMarkData!.changeCoordSystem(coordToChange: (x,y))
        guard let (xA2, yA2) = validationMarkData!.APrime else {
            return
        }
        
        if let (xC2, yC2) = validationMarkData!.CPrime {
            // If the previous buoy and the next one are the same it's a banana case
            if xC2 == xA2 && yC2 == yA2 {
                bananaCase(xP: xP, yP: yP)
            } else {
                bisectorCase(xP: xP, yP: yP)
            }
        } else { // There is no next buoy: use perpendicular method
            perpendicularCase(xP: xP, yP: yP)
        }
        
        // Check if it's a race course and if there is a previous point
        let race = NavigationManager.shared.route.value as? RaceCourse
        let previousPoint = NavigationManager.shared.getPreviousPointWithCoord()?.point
        if race != nil && previousPoint != nil {
            if axisCourseValidation == nil { // init axisCourseValidation
                // BuoyB is the current buoy
                let buoyB = (NavigationManager.shared.point.value?.point as! Mark).buoys[0]
                let posB = buoyB.gpsPosition
                // posA is the previous one. In case of a double or start use the middle
                let posA = previousPoint?.getCoordinates(method: .middle)
                axisCourseValidation = ValidationMarkData(posA: posA, posB: posB, posC: nil, course: NavigationManager.shared.route.value)
            }
            //Convert the current location coordinates in the new reference
            let (xP_axis, yP_axis) = axisCourseValidation!.changeCoordSystem(coordToChange: (x,y))
            guard let (xA2_axis, _) = axisCourseValidation!.APrime else {
                return
            }
            axisCourseCase(xP: xP_axis, yP: yP_axis, xA: xA2_axis)
        }
    }
    
    func doubleMark(isStart: Bool = false) {
        if validationMarkData == nil {
            // Reset values
            resetValues()
            
            // B : buoy to letAt port
            guard let buoyB = (NavigationManager.shared.point.value?.point as! Mark).buoys.first(where: {
                $0.toLetAt(inversed: (NavigationManager.shared.route.value as? CruiseCourse)?.reversed) == Buoy.ToLetAt.port.rawValue
            }) else {
                return
            }
            // A: buoy to letAt starboard
            guard let buoyA = (NavigationManager.shared.point.value?.point as! Mark).buoys.first(where: {
                $0.toLetAt(inversed: (NavigationManager.shared.route.value as? CruiseCourse)?.reversed) == Buoy.ToLetAt.starboard.rawValue
            }) else {
                return
            }
            
            // Get next point or nil
            let pointC = NavigationManager.shared.getNextPointWithCoord()?.point

            let posB = buoyB.gpsPosition
            let posA = buoyA.gpsPosition
            let posC = pointC?.getCoordinates(method: .toLetAt, toLetAt: buoyB.toLetAt(inversed: (NavigationManager.shared.route.value as? CruiseCourse)?.reversed))
            validationMarkData = ValidationMarkData(posA: posA, posB: posB, posC: posC, course: NavigationManager.shared.route.value)
            // Match race start: need to calculate U
            if let course = NavigationManager.shared.route.value, course.isMatchRace(), isStart {
                gpsPosUPin = getGPSPosU(pin: true)
                gpsPosUCommitee = getGPSPosU(pin: false)
            }
        }
        //Reference changement
        let UTMCoord = LocationManager.shared.locationVariable.value!.utmCoordinate()
        let x = UTMCoord.easting
        let y = UTMCoord.northing
        //Convert the current location coordinates in the new reference
        let (xP, yP) = validationMarkData!.changeCoordSystem(coordToChange: (x,y))
        if validationMarkData!.CPrime != nil {
            let (_, _) = validationMarkData!.CPrime!
        }
        let (xA2, _) = validationMarkData!.APrime!
        
        // Segment or Start procedure
        isStart ? startMark(xA2: xA2, xP: xP, yP: yP) : segmentCase(xA2: xA2, xP: xP, yP: yP)
        
        // Check if it's a race course and if there is a previous point
        let race = NavigationManager.shared.route.value as? RaceCourse
        let previousPoint = NavigationManager.shared.getPreviousPointWithCoord()?.point
        if race != nil && previousPoint != nil {
            if axisCourseValidation == nil { // init axisCourseValidation
                // posB is the position of the current buoy: use the middle for double mark
                let posB = (NavigationManager.shared.point.value?.point as! Mark).getCoordinates(method: .middle)
                // posA is the previous one. In case of a double or start use the middle
                let posA = previousPoint?.getCoordinates(method: .middle)
                axisCourseValidation = ValidationMarkData(posA: posA, posB: posB, posC: nil, course: NavigationManager.shared.route.value)
            }
            //Convert the current location coordinates in the new reference
            let (xP_axis, yP_axis) = axisCourseValidation!.changeCoordSystem(coordToChange: (x,y))
            guard let (xA2_axis, _) = axisCourseValidation!.APrime else {
                return
            }
            axisCourseCase(xP: xP_axis, yP: yP_axis, xA: xA2_axis)
        }
        
    }
    
    func segmentCase(xA2: Double, xP: Double, yP: Double) {
        let isInsideSegment = xP >= 0 && xP <= xA2
        if isInsideSegment && yP <= 0 {
            self.isComing = true
        } else if isInsideSegment && yP >= 0 && self.isComing {
            NavigationManager.shared.setNextPoint()
            self.isComing = false
        } else {
            self.isComing = false
        }
    }
    
    func startMark(xA2: Double, xP: Double, yP: Double) {
        AnnouncementManager.shared.startMessage = []
        let isInsideSegment = xP >= 0 && xP <= xA2
        if let race = NavigationManager.shared.route.value as? RaceCourse, let type = race.type.value, type == .matchRace, NavigationManager.shared.hasStarted.value {
            if NavigationManager.shared.totalTime > 240 { // before the 4min, announce entry in the U
                if xP < 0 || xP > xA2 || (isInsideSegment && yP <= 0) { // Outside U
                    self.isComingU = true
                    if self.isEnterU {
                        self.isEnterU = false
                        AnnouncementQueueManager.shared.addAnnounce(Announce(text: NSLocalizedString("exitU", bundle: bundle, comment: ""), priority: 1, type: .matchRaceU, annonceState: nil))
                    }
                } else { // Inside
                    if !self.isEnterU { // Enter U : add message
                        self.isComingU = false
                        self.isEnterU = true
                        AnnouncementQueueManager.shared.addAnnounce(Announce(text: NSLocalizedString("entryU", bundle: bundle, comment: ""), priority: 1, type: .matchRaceU, annonceState: nil))
                    }
                }
            } else if NavigationManager.shared.totalTime <= 240 && NavigationManager.shared.totalTime >= 120 { // Between 4 and 2min, need to enter the U and pass the line
                if xP < 0 || xP > xA2 || (isInsideSegment && yP <= 0) { // Outside U
                    self.isComingU = true
                } else { // Inside
                    if self.isEnterU { // At 4min, in the U. Premature entry
                        self.isEnterU = false
                        self.isEnterUDone = true // No need to entrer the U anymore
                        self.isComingLine = true
                        AnnouncementManager.shared.playWhistle()
                        AnnouncementQueueManager.shared.addAnnounce(Announce(text: NSLocalizedString("entryU_penality", bundle: bundle, comment: ""), priority: 1, type: .matchRaceU, annonceState: nil))
                    }
                    if self.isComingU { // Add message: entrée U
                        self.isComingU = false
                        self.isComingLine = true
                        if !self.isEnterUDone { // Add annoucement only the first time
                            self.isEnterUDone = true
                            AnnouncementManager.shared.playBeepPoint()
                            AnnouncementQueueManager.shared.addAnnounce(Announce(text:  NSLocalizedString("entryU", bundle: bundle, comment: ""), priority: 1, type: .matchRaceU, annonceState: nil))
                        }
                    }
                }
                if isInsideSegment && yP <= 0 && self.isComingLine { // Pass the line
                    self.isComingLine = false
                    if !self.isLineDone { // Add annoucement only the first time
                        self.isLineDone = true
                        AnnouncementManager.shared.playBeepPoint()
                        AnnouncementQueueManager.shared.addAnnounce(Announce(text: NSLocalizedString("entryU_line", bundle: bundle, comment: ""), priority: 1, type: .matchRaceU, annonceState: nil))
                    }
                }
            } else {
                if !self.isLineDone {
                    // entrée sous la ligne non faite aux 2 minutes , pénalité
                    AnnouncementManager.shared.playWhistle()
                    AnnouncementQueueManager.shared.addAnnounce(Announce(text: NSLocalizedString("no_entry_line", bundle: bundle, comment: ""), priority: 1, type: .matchRaceU, annonceState: nil))
                    self.isLineDone = true
                }
                
            }
        }
        
        if yP >= 0 { // Over
            AnnouncementManager.shared.startMessage.append(NSLocalizedString("over_line", bundle: bundle, comment: ""))
        } else { // Below
            AnnouncementManager.shared.startMessage.append(NSLocalizedString("below_line", bundle: bundle, comment: ""))
        }
        if !isInsideSegment { // Outside
            AnnouncementManager.shared.startMessage.append(NSLocalizedString("out_line", bundle: bundle, comment: ""))
        }
        
        // Add announcement when the boat crosses the line: over / below / out
        if self.boatStatus != AnnouncementManager.shared.startMessage {
            AnnouncementQueueManager.shared.addAnnounce(Announce(text: AnnouncementManager.shared.startMessage.joined(separator: " "), priority: 1, type: .changePoint, annonceState: nil))
            self.boatStatus = AnnouncementManager.shared.startMessage
        }
        
        if isInsideSegment && yP <= 0 {
            self.isComing = true
        } else if isInsideSegment && yP >= 0 && self.isComing && NavigationManager.shared.totalTime == 0 { // To validate the crossing of the sarting line, the timer has to end
            AnnouncementManager.shared.startMessage = []
            NavigationManager.shared.setNextPoint()
            self.isComingLine = false
            self.isComingU = false
            self.isComing = false
            self.isComingLine = false
            self.isLineDone = false
            self.isEnterUDone = false
        } else {
            self.isComing = false
        }
    }
    
    func bananaCase(xP: Double, yP: Double) {
        guard let validationMarkData = self.validationMarkData else {
            return
        }
        
        let yPCondition = validationMarkData.toLetAt == Buoy.ToLetAt.port.rawValue ? yP > 0 : yP < 0
        if xP < 0 && yPCondition {
            self.isComing = true
        } else if xP < 0 && !yPCondition && self.isComing {
            NavigationManager.shared.setNextPoint()
            self.isComing = false
        } else {
            self.isComing = false
        }
    }
    
    func bisectorCase(xP: Double, yP: Double) {
        guard let validationMarkData = self.validationMarkData, let isFavourableCase = validationMarkData.isFavourableCase else {
            return
        }
        // angle alpha ABC
        let alpha = validationMarkData.alpha
//        let d = tan(alpha / 2) * xP
        
        
        var xPyPCondition = false
        var yPComingCondition = false
        var yPValidationCondition = false
        if isFavourableCase {
            let d = -tan(alpha / 2) * xP // bissectrice
//            print("=== xP: \(xP), yP: \(yP), d: \(d)")
            if let race = NavigationManager.shared.route.value as? RaceCourse {
                xPyPCondition = xP < 0 && yP > 0
                yPComingCondition = validationMarkData.toLetAt == Buoy.ToLetAt.port.rawValue ? yP > d : yP < d
                yPValidationCondition = validationMarkData.toLetAt == Buoy.ToLetAt.port.rawValue ? yP <= d : yP >= d
            } else {
                xPyPCondition = validationMarkData.toLetAt == Buoy.ToLetAt.port.rawValue ? xP < 0 && yP > 0 : xP > 0 && yP < 0
                yPComingCondition = yP > d
                yPValidationCondition = yP <= d
            }
        } else {
            let d = tan(alpha / 2) * xP // bissectrice
//            print("===N xP: \(xP), yP: \(yP), d: \(d)")
            if let race = NavigationManager.shared.route.value as? RaceCourse {
                xPyPCondition = xP < 0 && yP < 0
                yPComingCondition = validationMarkData.toLetAt == Buoy.ToLetAt.port.rawValue ? yP > d : yP < d
                yPValidationCondition = validationMarkData.toLetAt == Buoy.ToLetAt.port.rawValue ? yP <= d : yP >= d
            } else {
                xPyPCondition = validationMarkData.toLetAt == Buoy.ToLetAt.port.rawValue ? xP > 0 && yP > 0 : xP < 0 && yP < 0
                yPComingCondition = yP <= d
                yPValidationCondition = yP >= d
            }
        }
        if yPComingCondition && xPyPCondition {
            self.isComing = true
        } else if yPValidationCondition && xPyPCondition && self.isComing { // Mark activated
            NavigationManager.shared.setNextPoint()
            self.isComing = false
        } else {
            self.isComing = false
        }
        
    }
    
    func perpendicularCase(xP: Double, yP: Double) {
        guard let buoyToLetAt = self.validationMarkData?.toLetAt else {
            return
        }
        var yCondition = false
        if buoyToLetAt == Buoy.ToLetAt.port.rawValue {
//            print("=== condition: \(yP >= 0.0)")
            yCondition = yP >= 0.0
        } else {
            yCondition = yP <= 0
        }
//        print("=== xP: \(xP), yP: \(yP)")
        
        if yCondition && xP >= 0 {
            self.isComing = true
        } else if yCondition && xP <= 0 && self.isComing {
//            print("=== PASSER PERPENDICULAR")
            NavigationManager.shared.setNextPoint()
            self.isComing = false
        } else {
            self.isComing = false
        }
    }
    
    func axisCourseCase(xP: Double, yP: Double, xA: Double) {
        guard let axisValidation = self.axisCourseValidation else {
            return
        }
        // if the sign of y it's different from previous y, the boat crosses the axis
        if (yP > 0 && yAxisPrevious < 0) || (yP < 0 && yAxisPrevious > 0) {
            if xP > 0 && xP < xA { // Check if the boat is between the 2 points
                NavigationManager.shared.addAnnouncementAxis()
            }
        }
        yAxisPrevious = yP
    }
    
    func getGPSPosU(pin: Bool) -> GPSPosition? {
        guard let validationMarkData = self.validationMarkData else {
            return nil
        }
        let xA = pin ? 0 : validationMarkData.APrime?.0
        guard xA != nil, let (x, y) = validationMarkData.currentCoordSystem(newCoordSystem: (xA!, 100)) else {
            return nil
        }

        guard let utmCoord = validationMarkData.utmCoordB else {
            return nil
        }
        
        let coord = UTMCoordinate(northing: y, easting: x, zone: utmCoord.zone, hemisphere: utmCoord.hemisphere).location().coordinate
        return GPSPosition(latitude: Float(coord.latitude), longitude: Float(coord.longitude))
    }
}
