//
//  MapHelper.swift
//  SARA Croisiere
//
//  Created by Marine on 27.07.20.
//  Copyright © 2020 GraniteApps. All rights reserved.
//

import Foundation
import MapKit
import RealmSwift

class MapHelper {
    
    // Just for debug: return the current date and time
    class func timeHelper() -> String{
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = Date()
        return dateFormatter.string(from: date)
    }
    
    // Use threadSafe to use elements of the database in background
    class func getAllMapAnnotationsThreadSafe(map: MKMapView, pointsDisplayed: [PointRealm]) {
        var annotations = [MapAnnotation]()
        let bundle = Utils.bundle(anyClass: MapHelper.self)
        
        // Load points from database: use of threadsafe
        let items = PointRealm.allThreadSafe()
        // Load beacons from database: use of threadsafe
        let beacons = Beacon.allThreadSafe()
        DispatchQueue(label: "background").async {
            autoreleasepool {
                let realm = try! Realm()
                for pointTh in items { //iterate and create annotations
                    guard let point = realm.resolve(pointTh) else { // Resolve realm
                        return // point was deleted
                    }
                    if pointsDisplayed.first(where: {$0.id == point.id}) == nil {
                        if let coursePoint = point as? CoursePoint, let gpsPosition = coursePoint.gpsPosition {
                            let type = coursePoint.distanceThreshold == 0 ? MapAnnotation.MapAnnotationType.mob : MapAnnotation.MapAnnotationType.coursePoint
                            let annotation = MapAnnotation(latitude: Double(gpsPosition.latitude), longitude: Double(gpsPosition.longitude), type: type)
                            annotation.title = "\(coursePoint.name)"
                            annotations.append(annotation)
                        } else if let mark = point as? Mark {
                            mark.buoys.forEach({
                                if let gpsPosition = $0.gpsPosition {
                                    let annotation = MapAnnotation(latitude: Double(gpsPosition.latitude), longitude: Double(gpsPosition.longitude), type: .mark)
                                    annotation.title = "\(mark.name) - \(NSLocalizedString("unit_" + $0.toLetAt(inversed: (NavigationManager.shared.route.value as? CruiseCourse)?.reversed), bundle: bundle, comment: ""))"
                                    annotations.append(annotation)
                                }
                            })
                        }
                    }
                }
                beacons.forEach({
                    guard let beacon = realm.resolve($0) else {  // Resolve realm
                        return // beacon was deleted
                    }
                    if let annotation = MapHelper.getAnnotationFromBeacon(beacon: beacon) {
                        annotations.append(annotation)
                    }
                })
                DispatchQueue.main.async { // go back to the main thread
                    map.addAnnotations(annotations)
                }
            }
        }
        
    }

    class func getAllMapAnnotations(map: MKMapView, pointsDisplayed: [PointRealm]) {
        var annotations = [MapAnnotation]()
        let bundle = Utils.bundle(anyClass: MapHelper.self)
        //iterate and create annotations
        let items = PointRealm.all()
        for point in items {
            if pointsDisplayed.first(where: {$0.id == point.id}) == nil {
                if let coursePoint = point as? CoursePoint, let gpsPosition = coursePoint.gpsPosition {
                    let type = coursePoint.distanceThreshold == 0 ? MapAnnotation.MapAnnotationType.mob : MapAnnotation.MapAnnotationType.coursePoint
                    let annotation = MapAnnotation(latitude: Double(gpsPosition.latitude), longitude: Double(gpsPosition.longitude), type: type)
                    annotation.title = "\(coursePoint.name)"
                    annotations.append(annotation)
                } else if let mark = point as? Mark {
                    mark.buoys.forEach({
                        if let gpsPosition = $0.gpsPosition {
                            let annotation = MapAnnotation(latitude: Double(gpsPosition.latitude), longitude: Double(gpsPosition.longitude), type: .mark)
                            annotation.title = "\(mark.name) - \(NSLocalizedString("unit_" + $0.toLetAt(inversed: (NavigationManager.shared.route.value as? CruiseCourse)?.reversed), bundle: bundle, comment: ""))"
                            annotations.append(annotation)
                        }
                    })
                }
            }
        }
        // Add beacons
        let beacons = Beacon.all().filter({$0.name != nil})
        beacons.forEach({
            if let annotation = MapHelper.getAnnotationFromBeacon(beacon: $0) {
                annotations.append(annotation)
            }
        })
        map.addAnnotations(annotations)
    }
    
    class func overlayRenderer(overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKPolyline {
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            switch overlay.title {
            case "crossingLineMark":
                polylineRenderer.strokeColor = Utils.crossingPointColor
                polylineRenderer.lineWidth = 5
                polylineRenderer.lineDashPattern = [0, 10]
            case "course":
                polylineRenderer.strokeColor = Utils.courseColor
                polylineRenderer.lineWidth = 5
            case "bisector":
                polylineRenderer.strokeColor = Utils.crossingPointColor
                polylineRenderer.lineWidth = 5
                polylineRenderer.lineDashPattern = [0, 10]
            case "trace":
                polylineRenderer.strokeColor = Utils.traceColor
                polylineRenderer.lineWidth = 4
//                polylineRenderer.lineDashPattern = [0, 10]
            case "userLocation":
                polylineRenderer.strokeColor = Utils.traceColor
                polylineRenderer.lineWidth = 4
//                polylineRenderer.lineDashPattern = [0, 10]
            case "courseAxis":
                polylineRenderer.strokeColor = UIColor.green
                polylineRenderer.lineWidth = 4
            case "U":
                polylineRenderer.strokeColor = Utils.crossingPointColor
                polylineRenderer.lineWidth = 4
                polylineRenderer.lineDashPattern = [0, 10]
            default:
                return MKOverlayRenderer()
            }
            return polylineRenderer
        } else if overlay is MKCircle {
            let circleRenderer = MKCircleRenderer(overlay: overlay)
            if overlay.title == "distanceThreshold" {
                circleRenderer.strokeColor = Utils.crossingPointColor
                circleRenderer.lineWidth = 5
            }else if overlay.title == "trace" {
                circleRenderer.fillColor = Utils.traceColor
            }
            return circleRenderer
        }
        return MKOverlayRenderer()
    }
    
    class func annotationView(mapView: MKMapView, annotation: MKAnnotation) -> MKAnnotationView? {
        let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "annotationView") ?? MKAnnotationView()
        var image: UIImage?
        let bundle = Utils.bundle(anyClass: MapHelper.self)
        if annotation is MKUserLocation {
            image = UIImage(named: "boat_user_loc", in: bundle, compatibleWith: nil)
        } else if annotation is MapAnnotation {
            let myAnnotation = annotation as! MapAnnotation
            if myAnnotation.type == .coursePoint {
                let color = myAnnotation.isCurrent ? Utils.courseColor : Utils.mapAnnotationPointColor
                image = UIImage(named: "course-point", in: bundle, compatibleWith: nil)?.maskWithColor(color: color)
//                annotationView.centerOffset = CGPoint(x: 0, y: -image.size.height / 2)
                annotationView.canShowCallout = true
            } else if myAnnotation.type == .mark {
                let color = myAnnotation.isCurrent ? Utils.courseColor : Utils.mapAnnotationMarkColor
                image = UIImage(named: "buoys", in: bundle, compatibleWith: nil)?.maskWithColor(color: color)
//                annotationView.centerOffset = CGPoint(x: 0, y: -image.size.height / 2)
                annotationView.canShowCallout = true
            } else if myAnnotation.type == .userLocation {
                image = UIImage(named: "boat_user_loc", in: bundle, compatibleWith: nil)
                annotationView.transform = CGAffineTransform(rotationAngle: CGFloat(NmeaManager.default.headingMagneticDegrees.value ?? 0) * .pi / 180)
            } else if myAnnotation.type == .mob {
                image = UIImage(named: "mob", in: bundle, compatibleWith: nil)?.maskWithColor(color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1))
            } else if myAnnotation.type == .beacon {
                image = UIImage(named: "beacon", in: bundle, compatibleWith: nil)?.maskWithColor(color: Utils.mapAnnotationPointColor)
                annotationView.canShowCallout = true
            } else if myAnnotation.type == .starboard {
                image = UIImage(named: "starboard", in: bundle, compatibleWith: nil)
                annotationView.canShowCallout = true
            } else if myAnnotation.type == .port {
                image = UIImage(named: "port", in: bundle, compatibleWith: nil)
                annotationView.canShowCallout = true
            } else if myAnnotation.type == .south {
                image = UIImage(named: "south", in: bundle, compatibleWith: nil)
                annotationView.canShowCallout = true
            } else if myAnnotation.type == .north {
                image = UIImage(named: "north", in: bundle, compatibleWith: nil)
                annotationView.canShowCallout = true
            } else if myAnnotation.type == .east {
                image = UIImage(named: "east", in: bundle, compatibleWith: nil)
                annotationView.canShowCallout = true
            } else if myAnnotation.type == .west {
                image = UIImage(named: "west", in: bundle, compatibleWith: nil)
                annotationView.canShowCallout = true
            } else if myAnnotation.type == .danger {
                image = UIImage(named: "danger", in: bundle, compatibleWith: nil)
                annotationView.canShowCallout = true
            } else if myAnnotation.type == .special {
                image = UIImage(named: "special", in: bundle, compatibleWith: nil)
                annotationView.canShowCallout = true
            } else if myAnnotation.type == .lightSupport {
                image = UIImage(named: "lighthouse", in: bundle, compatibleWith: nil)
                annotationView.canShowCallout = true
            } else if myAnnotation.type == .safeWater {
                image = UIImage(named: "safewater", in: bundle, compatibleWith: nil)
                annotationView.canShowCallout = true
            }
        }
        guard let imageToAdd = image else {
            return nil
        }
        annotationView.image = imageToAdd.resize()
        return annotationView
    }
    
    class func zoomToPolyLine(map: MKMapView, polyLine: MKPolyline, animated: Bool)
    {
        var regionRect = polyLine.boundingMapRect
        
        let wPadding = regionRect.size.width * 0.5
        let hPadding = regionRect.size.height * 0.5
        //Add padding to the region
        regionRect.size.width += wPadding
        regionRect.size.height += hPadding
        //Center the region on the line
        regionRect.origin.x -= wPadding / 2
        regionRect.origin.y -= hPadding / 2
        
        map.setRegion(MKCoordinateRegion(regionRect), animated: true)
    }
    
    class func centerMapOnLocation(map: MKMapView, location: CLLocation, regionRadius: Double = 3000) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        map.setRegion(coordinateRegion, animated: true)
    }
    
    class func getAnnotationFromBeacon(beacon: Beacon) -> MapAnnotation? {
        if let gpsPos = beacon.gpsPosition {
            let annotation = MapAnnotation(latitude: Double(gpsPos.latitude), longitude: Double(gpsPos.longitude), type: .beacon)
            switch beacon.type {
            case .starboardHandLateral:
                annotation.type = .starboard
            case .portHandLateral:
                annotation.type = .port
            case .northCardinal:
                annotation.type = .north
            case .southCardinal:
                annotation.type = .south
            case .eastCardinal:
                annotation.type = .east
            case .westCardinal:
                annotation.type = .west
            case .isolatedDanger:
                annotation.type = .danger
            case .specialPurpose:
                annotation.type = .special
            case .lightSupport:
                annotation.type = .lightSupport
            case .safeWater:
                annotation.type = .safeWater
            default:
                break
            }
            annotation.title = "\(beacon.type.description) - \(beacon.name)"
            return annotation
        }
        return nil
    }
    
    // Add user position on map when the user position is set manually
    class func userPositionManual(mapView: MKMapView, newLocation: CLLocation) {
        if mapView.showsUserLocation { // The setting has changed
            mapView.showsUserLocation = false
        }
        
        mapView.showsUserLocation = false
        // Remove annotation with previous position
        let previousPosAnnotations = mapView.annotations.filter({$0 is MapAnnotation && ($0 as! MapAnnotation).type == .userLocation})
        mapView.removeAnnotations(previousPosAnnotations)
        
        let annotation = MapAnnotation(latitude: Double(newLocation.coordinate.latitude), longitude: Double(newLocation.coordinate.longitude), type: .userLocation)
        mapView.addAnnotation(annotation)
    }
    
    // Add user position on map following settings of the app
    class func userPosition(mapView: MKMapView, newLocation: CLLocation) {
        if Settings.shared.developerMode {
            userPositionManual(mapView: mapView, newLocation: newLocation)
            return
        }
        if !Settings.shared.usePhoneGPS {
            userPositionManual(mapView: mapView, newLocation: newLocation)
        } else {
            if !mapView.showsUserLocation { // The setting has changed
                mapView.showsUserLocation = true
            }
        }
    }
}
