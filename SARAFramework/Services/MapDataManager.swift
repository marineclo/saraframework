//
//  MapDataManager.swift
//  SARAFramework
//
//  Created by Marine on 31.05.21.
//  Copyright © 2021 GraniteApps. All rights reserved.

import Foundation

protocol MapDataManagerObserver: AnyObject {
    func mapDataManager(didUpdateBeacons beaconsFront: [Beacon], beaconsBehind: [Beacon])
    func mapDataManager(didDeleteBeacons deletedBeacons: [Beacon])
}

class MapDataManager: NSObject {
    // MARK: - Variables
    static let shared = MapDataManager()
    fileprivate let fetchDataTimeout: TimeInterval = 10
    let serverString = "http://sara.orion-brest.com/wfs"
//    let serverString = "http://sara.rec.orion-brest.com/wfs" // server recette
    // Different Feature types for the request
    let featuresType = ["BCNLAT", "BOYLAT", "BCNCAR", "BOYCAR", "BCNSPP", "BOYSPP", "BCNSAW", "BOYSAW", "BCNISD", "BOYISD", "LNDMRK"]
    var messagesError: [String] = []
    var frontBeacons: [Beacon] = []
    var behindBeacons: [Beacon] = []
    // Observers
    private var observers: [MapDataManagerObserver] = []
    
    // MARK: - Init
    override init() {
        super.init()
        LocationManager.shared.locationVariable.asObservable()
            .bind(onNext: { location in
                let beaconsFront = Beacon.aroundBeaconsFront()
                let beaconsBehind = Beacon.aroundBeaconsBehind()
                self.observers.forEach({
                    $0.mapDataManager(didUpdateBeacons: beaconsFront, beaconsBehind: beaconsBehind)
                })
            })
    }
    
    // MARK: - Methods
    func downloadBeacons(coordinates: (Float, Float), radius: Float) -> DownloadedZone? {
        guard let serverUrl = URL(string: serverString) else {
            return nil
        }
        
        let group = DispatchGroup()
        let uuid = UUID().uuidString
        var beaconsList: [Beacon] = []
        for featureType in featuresType {
            group.enter()
            var components = URLComponents(url: serverUrl, resolvingAgainstBaseURL: false)!
            components.queryItems = [
                URLQueryItem(name: "INFO_FORMAT", value: "application/json"),
                URLQueryItem(name: "BBOX", value: "0,0,0,0"),
                URLQueryItem(name: "SRS", value: "EPSG:4326"),
                URLQueryItem(name: "FeatureType", value: featureType),
                URLQueryItem(name: "LAT", value: "\(coordinates.0)"),
                URLQueryItem(name: "LONG", value: "\(coordinates.1)"),
                URLQueryItem(name: "LENGTH", value: "\(radius)"),
                URLQueryItem(name: "SERVICE", value: "WFS"),
                URLQueryItem(name: "REQUEST", value: "GetFeature")
            ]
            
            URLSession.shared.dataTask(with: components.url!, completionHandler: { (data, response, error) -> Void in
                defer { group.leave() }
                print("json:\(featureType)")
                if error != nil {
                    self.messagesError.append(error!.localizedDescription)
                } else {
                    let statusCode = (response as! HTTPURLResponse).statusCode
                    if statusCode == 200 {
                        let jsonObject = try? JSONSerialization.jsonObject(with: data!, options: [])

                        guard let jsonInfo = jsonObject as? [String: Any] else {
                            return
                        }
                        guard let features = jsonInfo["features"] as? [[String: Any]] else {
                            return
                        }
                        for feature in features {
                            if let beacon = self.beacon(feature: feature, uuid: uuid){
                                beaconsList.append(beacon)
                            }
                        }
                    } else {
                        print(statusCode)
                        self.messagesError.append("Failed to load beacons from server (HTTP \(statusCode)")
                    }
                }
            }).resume()
            print(beaconsList.count)
            group.wait()
            print(beaconsList.count)
            if self.messagesError.count > 0 {
//                print("Error return nil")
                return nil
            }
        }
        print("Downloaded")
        print(beaconsList.count)
        // TODO: - Handle error message
        if self.messagesError.count == 0 { // Save downloaded zone
            let name = "Coord:\(coordinates.0.rounded(toPlaces: 2)),\(coordinates.1.rounded(toPlaces: 2)) Rayon:\(radius)\(NSLocalizedString("unit_abb_mile", bundle: Utils.bundle(anyClass: MapDataManager.self), comment: ""))"
            let downloadedZone = DownloadedZone(id: uuid, name: name, beacons: beaconsList)
//            downloadedZone.save()
            return downloadedZone
        } else {
            return nil
        }
    }
    
    // Parse beacon data
    func beacon(feature: [String: Any], uuid: String) -> Beacon? {

        guard let geometry = feature["geometry"] as? [String: Any] else {
            return nil
        }
        guard let gpsCoord = geometry["coordinates"] as? [Double] else {
            return nil
        }
        guard let properties = feature["properties"] as? [String: Any] else {
            return nil
        }
        guard let id = properties["foid"] as? String else {
            return nil
        }
        // Check if the beacon is already is the database
        if let beacon = Beacon.beaconWithId(id: id) {
            beacon.downloadedZoneListId.append(uuid)
            return beacon
        } else {
            var type: BeaconType?
            guard let name = properties["Object name"] as? String else { // Keep only beacon with name
                return nil
            }
            if let featureType = properties["featureType"] as? String {
                if featureType.contains("special purpose/general") {
                    type = BeaconType(string: "special purpose/general")
                } else if featureType.contains("lateral") {
                    if let typeLateral = properties["Category of lateral mark"] as? String {
                        type = BeaconType(string: typeLateral)
                    }
                } else if featureType.contains("cardinal") {
                    if let typeCardinal = properties["Category of cardinal mark"] as? String {
                        type = BeaconType(string: typeCardinal)
                    }
                } else if featureType.contains("isolated danger") {
                    type = BeaconType(string: "isolated danger")
                } else if featureType.contains("safe water") {
                    type = BeaconType(string: "safe water")
                } else if featureType.contains("Landmark") {
                    if let landmark = (properties["Function"] as? String), landmark.contains("light support") {
                        type = BeaconType(string: "light support")
                    } else if let landmark = (properties["Function"] as? [String]), landmark.contains("light support") {
                        type = BeaconType(string: "light support")
                    }
                }
            }
            guard let beaconType = type else {
                return nil
            }
            let beacon = Beacon(id: id, name: name, gpsCoord: gpsCoord, type: beaconType, downloadedZoneId: [uuid])
            return beacon
        }
    }
    
    // MARK: - Observers
    func addObserver(_ observer: MapDataManagerObserver) {
        if !self.observers.contains(where: {$0 === observer}) {
            observers.append(observer)
        }
    }
    
    func removeObserver(_ observer: MapDataManagerObserver) {
        if let i = self.observers.firstIndex(where: {$0 === observer}) {
            self.observers.remove(at: i)
        }
    }
}
