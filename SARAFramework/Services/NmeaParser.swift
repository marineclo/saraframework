//
//  NmeaParser.swift
//  SARA Croisiere
//
//  Created by Tristan Derbanne on 19.03.19.
//  Copyright © 2019 Rose. All rights reserved.
//

import Foundation
import CoreLocation

class NmeaParser {
    
    // MARK: - Public variables and functions
    // Errors
    enum NmeaParserError: Error {
        case checksum
        case unknownType
        case malformed
    }
    
    // Nmea manager to update values
    weak var nmeaManager: NmeaManager?
    
    // Parse th provided message and update appropriate values in NmeaManager
    func parse(nmeaMessage message: String) throws {
        if !self.verifyChecksum(message) {
            throw NmeaParserError.checksum
        }
        
        // Message type: characters at position 3 to 5
        let messageType = message[3..<6]
        switch messageType {
        case "DBT":
            try self.parseDBTMessage(message)
            break
        case "MTW":
            try self.parseMTWMessage(message)
            break
        case "VTG":
            try self.parseVTGMessage(message)
            break
        case "DPT":
            try self.parseDPTMessage(message)
            break
        case "VWR":
            try self.parseVWRMessage(message)
            break
        case "MWV":
            try self.parseMWVMessage(message)
            break
        case "VHW":
            try self.parseVHWMessage(message)
        case "HDG":
            try self.parseHDGMessage(message)
        case "XDR":
            try self.parseXDRMessage(message)
        case "MWD":
            try self.parseMWDMessage(message)
            break
        case "RMC":
            try self.parseRMCMessage(message)
            break
        case "GGA":
            try self.parseGGAMessage(message)
            break
        case "GLL":
            try self.parseGLLMessage(message)
            break
        default:
            throw NmeaParserError.unknownType
        }
    }
    
    // MARK: - Parsing methods
    
    // DBT (Depth below transducer)
    // $--DBT,x.x,f,x.x,M,x.x,F*hh
    // 1) Depth, feet 2) f = feet
    // 3) Depth, meters 4) M = meters
    // 5) Depth, Fathoms 6) F = Fathoms 7) Checksum
    private func parseDBTMessage(_ message: String) throws {
        guard let splitValues = message.split(separator: "*").first?.split(separator: ",", maxSplits: Int.max, omittingEmptySubsequences: false) else {
            throw NmeaParserError.malformed
        }
        guard splitValues.count > 5 else {
            throw NmeaParserError.malformed
        }
        
        if let depth = Float(splitValues[1]) {
            nmeaManager?.depthBelowTransducerFeet.accept(depth)
        }
        if let depth = Float(splitValues[3]) {
            nmeaManager?.depthBelowTransducerMeters.accept(depth)
            nmeaManager?.depthMeters.accept(depth)
            AnnouncementManager.shared.depthVariable.accept(depth)
        }
        if let depth = Float(splitValues[5]) {
            nmeaManager?.depthBelowTransducerFathoms.accept(depth)
        }
    }
    
    // DPT (Depth of Water)
    // $--DPT,x.x,x.x*hh
    // 1) Depth, meters
    // 2) Offset from transducer;
    // positive means distance from transducer to water line,
    // negative means distance from transducer to keel
    // 3) Checksum
    private func parseDPTMessage(_ message: String) throws {
        guard let splitValues = message.split(separator: "*").first?.split(separator: ",", maxSplits: Int.max, omittingEmptySubsequences: false) else {
            throw NmeaParserError.malformed
        }
        
        guard splitValues.count > 2 else {
            throw NmeaParserError.malformed
        }
        
        if let depth = Float(splitValues[1]) {
            nmeaManager?.depthMeters.accept(depth)
            AnnouncementManager.shared.depthVariable.accept(depth)
        }
        if let offset = Float(splitValues[2]) {
            nmeaManager?.offsetFromTransducer.accept(offset)
        }
    }
    
    // MTW (Water temperature)
    // $--MTW,x.x,C*hh
    // 1) Degrees
    // 2) Unit of Measurement, Celcius 3) Checksum
    private func parseMTWMessage(_ message: String) throws {
        guard let splitValues = message.split(separator: "*").first?.split(separator: ",", maxSplits: Int.max, omittingEmptySubsequences: false) else {
            throw NmeaParserError.malformed
        }
        guard splitValues.count > 1 else {
            throw NmeaParserError.malformed
        }
        
        if let temperature = Float(splitValues[1]) {
            nmeaManager?.waterTemperatureCelcius.accept(temperature)
            AnnouncementManager.shared.waterTemperatureVariable.accept(temperature)
        } else {
            nmeaManager?.waterTemperatureCelcius.accept(nil)
            AnnouncementManager.shared.waterTemperatureVariable.accept(nil)
        }
    }
    
    // VTG (Track made good and ground speed)
    // $--VTG,x.x,T,x.x,M,x.x,N,x.x,K*hh
    // 1) Track Degrees 2) T = True
    // 3) Track Degrees 4) M = Magnetic 5) Speed Knots 6) N = Knots
    // 7) Speed Kilometers Per Hour 8) K = Kilometres Per Hour 9) Checksum
    private func parseVTGMessage(_ message: String) throws {
        guard let splitValues = message.split(separator: "*").first?.split(separator: ",", maxSplits: Int.max, omittingEmptySubsequences: false) else {
            throw NmeaParserError.malformed
        }
        guard splitValues.count > 8 else {
            throw NmeaParserError.malformed
        }
        
        if let heading = Float(splitValues[1]) {
            //NOT USED
            nmeaManager?.headingTrueDegrees.accept(heading)
        }
        if let heading = Float(splitValues[3]) {
            nmeaManager?.headingMagneticDegrees.accept(heading)
        }
        if let speed = Float(splitValues[5]) {
            //NOT USED
            nmeaManager?.speedOverGroundKnots.accept(speed)
        }
        if let speed = Float(splitValues[7]) {
            //NOT USED
            nmeaManager?.speedOverGroundKmPerHour.accept(speed)
        }
    }
    
    // VWR (Relative Wind Speed and Angle)
    // $--VWR,x.x,a,x.x,N,x.x,M,x.x,K*hh
    // 1) Wind direction magnitude in degrees 2) Wind direction Left/Right of bow 3) Speed
    // 4) N = Knots
    // 5) Speed
    // 6) M = Meters Per Second 7) Speed
    // 8) K = Kilometers Per Hour 9) Checksum
    private func parseVWRMessage(_ message: String) throws {
        guard let splitValues = message.split(separator: "*").first?.split(separator: ",", maxSplits: Int.max, omittingEmptySubsequences: false) else {
            throw NmeaParserError.malformed
        }
        guard splitValues.count > 7 else {
            throw NmeaParserError.malformed
        }
        var windApparentAngle : Float?
        var windApparentspeed : Float?
        if let direction = Float(splitValues[1]) {
            let side = splitValues[2]
            if side == "L" {
                nmeaManager?.windRelativeAngleDegrees.accept((direction, NavigationUnit.port))
                AnnouncementManager.shared.apparentWindAngleVariable.accept((Int(direction), NavigationUnit.port))
                windApparentAngle = -direction
            }
            else if side == "R" {
                nmeaManager?.windRelativeAngleDegrees.accept((direction, NavigationUnit.starboard))
                AnnouncementManager.shared.apparentWindAngleVariable.accept((Int(direction), NavigationUnit.starboard))
                windApparentAngle = direction
            }
        }
        
        // speed in knots
        if let speed = Float(splitValues[3]) {
            if Settings.shared.speedUnit?.unit == .knots {
                nmeaManager?.windRelativeSpeedKnots.accept(speed.formatted)
                AnnouncementManager.shared.apparentWindSpeedVariable.accept(speed)
            }
            windApparentspeed = speed
        }
        
        // speed in km/h
        if let speed = Float(splitValues[7]) {
            if Settings.shared.speedUnit?.unit == NavigationUnit.kmPerHour {
                nmeaManager?.windRelativeSpeedKmPerHour.accept(speed.formatted)
                AnnouncementManager.shared.apparentWindSpeedVariable.accept(speed)
            }
        }
        // Calculate true wind from apparent wind.
        trueWindCalculation(windApparentspeed: windApparentspeed, windApparentAngle: windApparentAngle)
    }
    
    // MWV (True wind speed and angle)
    // $--MWV,x.x,a,x.x,a*hh
    // 1) Wind Angle, 0 to 360 degrees
    // 2) Reference, R = Relative, T = True
    // 3) Wind Speed
    // 4) Wind Speed Units, K/M/N
    // 5) Status, A = Data Valid
    // 6) Checksum
    private func parseMWVMessage(_ message: String) throws {
        guard let splitValues = message.split(separator: "*").first?.split(separator: ",", maxSplits: Int.max, omittingEmptySubsequences: false) else {
            throw NmeaParserError.malformed
        }
        guard splitValues.count > 5 else {
            throw NmeaParserError.malformed
        }
        
        guard splitValues[5] == "A" else {
            return
        }
        
        let reference = splitValues[2]
        var windApparentAngle : Float?
        var windApparentspeed : Float?
        if let angle = Float(splitValues[1]) {
            if reference == "R" {
                if angle <= 180 {
                    nmeaManager?.windRelativeAngleDegrees.accept((angle, NavigationUnit.starboard))
                    AnnouncementManager.shared.apparentWindAngleVariable.accept((Int(angle), NavigationUnit.starboard))
                    windApparentAngle = angle
                } else {
                    nmeaManager?.windRelativeAngleDegrees.accept((360 - angle, NavigationUnit.port))
                    AnnouncementManager.shared.apparentWindAngleVariable.accept((Int(360 - angle), NavigationUnit.port))
                    windApparentAngle = angle - 360
                }
            }
            else if reference == "T" {
//                if angle <= 180 {
//                    nmeaManager?.windTrueAngleDegrees.accept((angle, NavigationUnit.starboard))
//                    AnnouncementManager.shared.windTrueAngleVariable.accept((Int(angle), NavigationUnit.starboard))
//                } else {
//                    nmeaManager?.windTrueAngleDegrees.accept((360 - angle, NavigationUnit.port))
//                    AnnouncementManager.shared.windTrueAngleVariable.accept((Int(360 - angle), NavigationUnit.port))
//                }
            }
        }
        if let speed = Float(splitValues[3]) {
            let unit = splitValues[4]
            switch unit {
            case "N":
                if reference == "R" {
                    if Settings.shared.speedUnit?.unit == .knots {
                        nmeaManager?.windRelativeSpeedKnots.accept(speed.formatted)
                        AnnouncementManager.shared.apparentWindSpeedVariable.accept(speed)
                        windApparentspeed = speed
                    } else {
                        nmeaManager?.windRelativeSpeedKmPerHour.accept(speed.knotsToKmh())
                        AnnouncementManager.shared.apparentWindSpeedVariable.accept(speed.knotsToKmh())
                    }
                }
                else if reference == "T" {
//                    if Settings.shared.speedUnit?.unit == .knots {
//                        nmeaManager?.windTrueSpeedKnots.accept(speed.formatted)
//                        AnnouncementManager.shared.speedTrueWindVariable.accept(speed)
//                    } else {
//                        nmeaManager?.windTrueSpeedKmPerHour.accept(speed.knotsToKmh())
//                    AnnouncementManager.shared.speedTrueWindVariable.accept(speed.knotsToKmh())
//                    }
                }
                break
            case "M":
                if reference == "R" {
                    if Settings.shared.speedUnit?.unit == NavigationUnit.knots {
                        nmeaManager?.windRelativeSpeedKnots.accept(speed.msToKnots())
                    AnnouncementManager.shared.apparentWindSpeedVariable.accept(speed.msToKnots())
                    } else if Settings.shared.speedUnit?.unit == NavigationUnit.kmPerHour {
                        nmeaManager?.windRelativeSpeedKmPerHour.accept(speed.msToKmh())
                    AnnouncementManager.shared.apparentWindSpeedVariable.accept(speed.msToKmh())
                    }
                }
                else if reference == "T" {
//                    if Settings.shared.speedUnit?.unit == NavigationUnit.knots {
//                        nmeaManager?.windTrueSpeedKnots.accept(speed.msToKnots())
//                    AnnouncementManager.shared.speedTrueWindVariable.accept(speed.msToKnots())
//                    } else if Settings.shared.speedUnit?.unit == NavigationUnit.kmPerHour {
//                        nmeaManager?.windTrueSpeedKmPerHour.accept(speed.msToKmh())
//                    AnnouncementManager.shared.speedTrueWindVariable.accept(speed.msToKmh())
//                    }
                }
                break
            case "K":
                if reference == "R" {
                    if Settings.shared.speedUnit?.unit == .knots {
                        nmeaManager?.windRelativeSpeedKnots.accept(speed.knotsToKmh())
                    AnnouncementManager.shared.apparentWindSpeedVariable.accept(speed.knotsToKmh())
                        
                    } else {
                        nmeaManager?.windRelativeSpeedKmPerHour.accept(speed.formatted)
                        AnnouncementManager.shared.apparentWindSpeedVariable.accept(speed)
                    }
                }
                else if reference == "T" {
//                    if Settings.shared.speedUnit?.unit == .knots {
//                        nmeaManager?.windTrueSpeedKnots.accept(speed.knotsToKmh())
//                    AnnouncementManager.shared.speedTrueWindVariable.accept(speed.knotsToKmh())
//                    } else {
//                        nmeaManager?.windTrueSpeedKmPerHour.accept(speed.formatted)
//                        AnnouncementManager.shared.speedTrueWindVariable.accept(speed)
//                    }
                }
                break
            default:
                break
            }
            
            // Calculate true wind from apparent wind.
            trueWindCalculation(windApparentspeed: windApparentspeed, windApparentAngle: windApparentAngle)
        }
    }
    
    /* VHW Water Speed and Heading
     $--VHW,x.x,T,x.x,M,x.x,N,x.x,K*hh
     1) Degress True
     2) T = True
     3) Degrees Magnetic
     4) M = Magnetic
     5) Knots (speed of vessel relative to the water)
     6) N = Knots
     7) Kilometers (speed of vessel relative to the water)
     8) K = Kilometres
     9) Checksum
     */
    private func parseVHWMessage(_ message: String) throws {
        guard let splitValues = message.split(separator: "*").first?.split(separator: ",", maxSplits: Int.max, omittingEmptySubsequences: false) else {
            throw NmeaParserError.malformed
        }
        guard splitValues.count > 8 else {
            throw NmeaParserError.malformed
        }
        
        if let headingMagnetic = Float(splitValues[3]) {
            nmeaManager?.headingMagneticDegrees.accept(headingMagnetic)
            AnnouncementManager.shared.headingMagneticVariable.accept(Int(headingMagnetic))

        }
        if let speedKnots = Float(splitValues[5]) {
            if Settings.shared.speedUnit?.unit == .knots {
                nmeaManager?.speedThroughWaterKnots.accept(speedKnots.formatted)
                AnnouncementManager.shared.speedThroughWaterVariable.accept(speedKnots)
            }
        }
        if let speedKmPerHour = Float(splitValues[7]) {
            if Settings.shared.speedUnit?.unit == NavigationUnit.kmPerHour {
                nmeaManager?.speedThroughWaterKmPerHour.accept(speedKmPerHour.formatted)
                AnnouncementManager.shared.speedThroughWaterVariable.accept(speedKmPerHour)
            }
        }
    }
    
    /* HDG Heading – Deviation & Variation
     $--HDG,x.x,x.x,a,x.x,a*hh
     1) Magnetic Sensor heading in degrees
     2) Magnetic Deviation, degrees
     3) Magnetic Deviation direction, E = Easterly, W = Westerly
     4) Magnetic Variation degrees
     5) Magnetic Variation direction, E = Easterly, W = Westerly
     6) Checksum
     */
    private func parseHDGMessage(_ message: String) throws {
        guard let splitValues = message.split(separator: "*").first?.split(separator: ",", maxSplits: Int.max, omittingEmptySubsequences: false) else {
            throw NmeaParserError.malformed
        }
        guard splitValues.count > 5 else {
            throw NmeaParserError.malformed
        }
        
        if let headingMagnetic = Float(splitValues[3]) {
            nmeaManager?.headingMagneticDegrees.accept(headingMagnetic)
            AnnouncementManager.shared.headingMagneticVariable.accept(Int(headingMagnetic))

        }
    }
    
    /* XDR Heading - Transducer Values
     $--XDR,a,x.x,a,c--c, ..... *hh
     1) Transducer Type
     2) Measurement Data
     3) Units of measurement
     4) Name of transducer
     x) More of the same
     n) Checksum
     
     Currently, OpenCPN recognizes the following transducers:
     Measured Value | Transducer Type | Measured Data   | Unit of measure | Transducer Name
     ------------------------------------------------------------------------------------------------------
     barometric     | "P" pressure    | 0.8..1.1 or 800..1100           | "B" bar         | "Barometer"
     air temperature| "C" temperature |   2 decimals                    | "C" celsius     | "TempAir" or "ENV_OUTAIR_T"
     pitch          | "A" angle       |-180..0 nose down 0..180 nose up | "D" degrees     | "PTCH" or "PITCH"
     rolling        | "A" angle       |-180..0 L         0..180 R       | "D" degrees     | "ROLL"
     water temp     | "C" temperature |   2 decimals                    | "C" celsius     | "ENV_WATER_T"
     -----------------------------------------------------------------------------------------------------
     */
    private func parseXDRMessage(_ message: String) throws {
        guard let splitValues = message.split(separator: "*").first?.split(separator: ",", maxSplits: Int.max, omittingEmptySubsequences: false) else {
            throw NmeaParserError.malformed
        }
        guard splitValues.count > 4 else {
            throw NmeaParserError.malformed
        }
        
        let transducerType = splitValues[1]
        
        // For now we only handle air temperature (type 'C')
        if transducerType != "C" {
            throw NmeaParserError.unknownType
        }
        
        guard let airTemperatureCelcius = Float(splitValues[2]) else {
            throw NmeaParserError.malformed
        }
        
        nmeaManager?.airTemperatureCelcius.accept(airTemperatureCelcius)
        AnnouncementManager.shared.airTemperatureVariable.accept(airTemperatureCelcius)
    }
    
    /* MWD Wind Direction & Speed
     $--MWD,x.x,T,x.x,M,x.x,N,x.x,M*hh
     1) Wind direction: True, degree
     3) Wind direction: Magnetic, degree
     5) Wind speed, knots
     7) Wind speed, meter/second
     9) Checksum
     */
    private func parseMWDMessage(_ message: String) throws {
        guard let splitValues = message.split(separator: "*").first?.split(separator: ",", maxSplits: Int.max, omittingEmptySubsequences: false) else {
            throw NmeaParserError.malformed
        }
        guard splitValues.count > 8 else {
            throw NmeaParserError.malformed
        }
        
        if let windDirection = Float(splitValues[3]) {
            nmeaManager?.windDirection.accept(windDirection)
            AnnouncementManager.shared.windDirectionAngleVariable.accept(Int(windDirection))
        }
    }
    
    /*
     RMC Recommended Minimum Navigation Information
     12 1 23 45 67891011| ||||||||||||
     $--RMC,hhmmss.ss,A,llll.ll,a,yyyyy.yy,a,x.x,x.x,xxxx,x.x,a*hh
     1) Time (UTC)
     2) Status, V = Navigation receiver warning
     3) Latitude
     4) N or S
     5) Longitude
     6) E or W
     7) Speed over ground, knots
     8) Track made good, degrees true
     9) Date, ddmmyy
     10) Magnetic Variation, degrees 11) E or W
     12) Checksum
     
     We will only parse those message if the GPS source is configured to be
     the navigation center and not the phone.
     
     Latitude / Longitude: value in Degree Minute : DDmm.mmm
     To convert in Decimal Degree (DD) -> DD + (mm.mmm/60)
     
    */
    
    private func parseRMCMessage(_ message: String) throws {
        guard !Settings.shared.usePhoneGPS else { return }
        
        guard let splitValues = message.split(separator: "*").first?.split(separator: ",", maxSplits: Int.max, omittingEmptySubsequences: false) else {
            throw NmeaParserError.malformed
        }
        guard splitValues.count > 11 else {
            throw NmeaParserError.malformed
        }
        
        guard let latitude = Double(splitValues[3]) else {
            throw NmeaParserError.malformed
        }
        if latitude == 0 {
            throw NmeaParserError.malformed
        }
        var latitudeDD = latitude.decimalDegree
        
        let latitudeNorS = splitValues[4]
        if latitudeNorS == "S" {
            latitudeDD = -latitudeDD
        }
        guard let longitude = Double(splitValues[5]) else {
            throw NmeaParserError.malformed
        }
        var longitudeDD = longitude.decimalDegree
        let longitudeWorE = splitValues[6]
        if longitudeWorE == "W" {
            longitudeDD = -longitudeDD
        }
        
        guard let speed = Float(splitValues[7]) else {
            throw NmeaParserError.malformed
        }
        
        guard let course = Double(splitValues[8]) else {
            throw NmeaParserError.malformed
        }
        
        // Let's build a CLLocation object and update the current one
        let location = CLLocation(coordinate: CLLocationCoordinate2D(latitude: latitudeDD, longitude: longitudeDD), altitude: -1, horizontalAccuracy: 10, verticalAccuracy: -1, course: CLLocationDirection(course), speed: CLLocationSpeed(speed.knotsToMs()), timestamp: Date())
        LocationManager.shared.locationVariable.accept(location)
    }
    
    /*
     GGA Global Positioning System Fix Data. Time, Position and fix related data for a GPS receiver
     11
     1 2 34 5678 910|121314 15
     ||||||||||||||| $--GGA,hhmmss.ss,llll.ll,a,yyyyy.yy,a,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx*hh
     1) Time (UTC)
     2) Latitude
     3) N or S (North or South)
     4) Longitude
     5) E or W (East or West)
     6) GPS Quality Indicator,
     0 - fix not available, 1 - GPS fix,
     2 - Differential GPS fix
     7) Number of satellites in view, 00 - 12
     8) Horizontal Dilution of precision
     9) Antenna Altitude above/below mean-sea-level (geoid)
     10) Units of antenna altitude, meters
     11) Geoidal separation, the difference between the WGS-84 earth
     ellipsoid and mean-sea-level (geoid), "-" means mean-sea-level below ellipsoid
     12) Units of geoidal separation, meters
     13) Age of differential GPS data, time in seconds since last SC104
     type 1 or 9 update, null field when DGPS is not used
     14) Differential reference station ID, 0000-1023
     15) Checksum
     
     We will only parse those message if the GPS source is configured to be
     the navigation center and not the phone.
    */
    private func parseGGAMessage(_ message: String) throws {
        guard !Settings.shared.usePhoneGPS else { return }
        
        guard let splitValues = message.split(separator: "*").first?.split(separator: ",", maxSplits: Int.max, omittingEmptySubsequences: false) else {
            throw NmeaParserError.malformed
        }
        guard splitValues.count > 14 else {
            throw NmeaParserError.malformed
        }
        
        guard let latitude = Double(splitValues[2]) else {
            throw NmeaParserError.malformed
        }
        var latitudeDD = latitude.decimalDegree
        let latitudeNorS = splitValues[3]
        if latitudeNorS == "S" {
            latitudeDD = -latitudeDD
        }
        guard let longitude = Double(splitValues[4]) else {
            throw NmeaParserError.malformed
        }
        var longitudeDD = longitude.decimalDegree
        let longitudeWorE = splitValues[5]
        if longitudeWorE == "W" {
            longitudeDD = -longitudeDD
        }

        // Let's build a CLLocation object and update the current one
        let location = CLLocation(coordinate: CLLocationCoordinate2D(latitude: latitudeDD, longitude: longitudeDD), altitude: -1, horizontalAccuracy: 10, verticalAccuracy: -1, course: -1, speed: -1, timestamp: Date())
        LocationManager.shared.locationVariable.accept(location)
    }
    
    /*
     GLL Geographic Position – Latitude/Longitude
     1234567
     ||||||| $--GLL,llll.ll,a,yyyyy.yy,a,hhmmss.ss,A*hh
     1) Latitude
     2) N or S (North or South)
     3) Longitude
     4) E or W (East or West)
     5) Time (UTC)
     6) Status A - Data Valid, V - Data Invalid
     7) Checksum
     
     We will only parse those message if the GPS source is configured to be
     the navigation center and not the phone.
    */
    private func parseGLLMessage(_ message: String) throws {
        guard !Settings.shared.usePhoneGPS else { return }
        
        guard let splitValues = message.split(separator: "*").first?.split(separator: ",", maxSplits: Int.max, omittingEmptySubsequences: false) else {
            throw NmeaParserError.malformed
        }
        guard splitValues.count > 6 else {
            throw NmeaParserError.malformed
        }
        
        guard splitValues[6] == "A" else {
            // Data invalid
            return
        }
        
        guard let latitude = Double(splitValues[1]) else {
            throw NmeaParserError.malformed
        }
        
        var latitudeDD = latitude.decimalDegree
        let latitudeNorS = splitValues[2]
        if latitudeNorS == "S" {
            latitudeDD = -latitudeDD
        }
        guard let longitude = Double(splitValues[3]) else {
            throw NmeaParserError.malformed
        }
        if longitude == 0 {
            throw NmeaParserError.malformed
        }
        var longitudeDD = longitude.decimalDegree
        let longitudeWorE = splitValues[4]
        if longitudeWorE == "W" {
            longitudeDD = -longitudeDD
        }
        
        // Let's build a CLLocation object and update the current one
        let location = CLLocation(coordinate: CLLocationCoordinate2D(latitude: latitudeDD, longitude: longitudeDD), altitude: -1, horizontalAccuracy: 10, verticalAccuracy: -1, course: -1, speed: -1, timestamp: Date())
        LocationManager.shared.locationVariable.accept(location)
    }
    
    // MARK: - Checksum verification
    private func verifyChecksum(_ message: String) -> Bool {
        var checksum: UInt8 = 0
        
        guard message.count >= 6 else {
            return false
        }
        
        let strippedMessage = message[1..<(message.count - 4)]
        
        for char in strippedMessage.utf8.map({ UInt8($0) }) {
            checksum = checksum ^ char
        }
        
        let reference = UInt8(message[(message.count - 3)..<(message.count - 1)], radix: 16)
        
        return (reference == checksum)
    }
    
    // MARK: - True Wind Calculation and wind direction
    func trueWindCalculation(windApparentspeed: Float?, windApparentAngle: Float?) {
        guard windApparentspeed != nil, windApparentAngle != nil else {
            return
        }
        let speed = Settings.shared.useSpeedoForTrueWind ? nmeaManager?.speedThroughWaterKnots.value ?? 0.0 : NavigationManager.shared.speedOverGround.value ?? 0.0
        let windTrueSpeed = sqrtf((speed * speed) + (windApparentspeed! * windApparentspeed!) - 2 * speed * windApparentspeed! * cos(windApparentAngle! * .pi / 180))
        if windTrueSpeed == 0 {
            // TODO
            return
//            nmeaManager?.windTrueAngleDegrees.accept((windTrueAngle, NavigationUnit.starboard))
//            AnnouncementManager.shared.trueWindAngleVariable.accept((Int(windTrueAngle), NavigationUnit.starboard))
        }
        if Settings.shared.speedUnit?.unit == .knots {
            nmeaManager?.windTrueSpeedKnots.accept(windTrueSpeed.formatted)
            AnnouncementManager.shared.trueWindSpeedVariable.accept(windTrueSpeed)
        } else {
            nmeaManager?.windTrueSpeedKmPerHour.accept(windTrueSpeed.knotsToKmh())
            AnnouncementManager.shared.trueWindSpeedVariable.accept(windTrueSpeed.knotsToKmh())
        }
        let sign = windApparentAngle!.sign == .plus ? 1 : -1
        let windApparentAngleRad = windApparentAngle! * .pi / 180
        let windTrueAngle = (windApparentAngle! / 2) + Float(sign) * 90 - (atanf(((windApparentspeed! - speed) / (windApparentspeed! + speed)) * (1 / tanf(windApparentAngleRad / 2))) * 180 / .pi)
        if windTrueAngle == Float.greatestFiniteMagnitude {
            return
        }
        if windTrueAngle >= 0 {
            nmeaManager?.windTrueAngleDegrees.accept((windTrueAngle, NavigationUnit.starboard))
            AnnouncementManager.shared.trueWindAngleVariable.accept((Int(windTrueAngle), NavigationUnit.starboard))
        } else {
            nmeaManager?.windTrueAngleDegrees.accept((abs(windTrueAngle), NavigationUnit.port))
            AnnouncementManager.shared.trueWindAngleVariable.accept((Int(abs(windTrueAngle)), NavigationUnit.port))
        }
        if let heading = NmeaManager.default.headingMagneticDegrees.value { // Surface heading
            var windWeather = windTrueAngle + Float(heading)
            if windWeather > 360 {
                windWeather = windWeather - 360
            } else if windWeather < 0 {
                windWeather = 360 + windWeather
            }
            nmeaManager?.windDirection.accept(windWeather)
            AnnouncementManager.shared.windDirectionAngleVariable.accept(Int(windWeather))
        }
        
    }
    
    // MARK: - Debug trace file
    func parsePosition(msg: String) {
        guard var splitValues = msg.split(separator: "*").first?.split(separator: ",", maxSplits: Int.max, omittingEmptySubsequences: false), splitValues.count >= 4 else {
            return
        }
        guard let lat = Double(splitValues[1]), let long = Double(splitValues[2]) else {
            return
        }
        if splitValues.count > 4 {
            if splitValues[4].prefix(1) == " " {
                splitValues[4].removeFirst() // remove space: remove for next version
            }
            if splitValues[5].prefix(1) == " " {
                splitValues[5].removeFirst() // remove space: remove for next version
            }
            let speedString = splitValues[5].dropLast(2) // remove last 2 characters
            if let course = Double(splitValues[4]), let speed = Double(speedString) {
                let location = CLLocation(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: long), altitude: -1, horizontalAccuracy: 10, verticalAccuracy: -1, course: course, speed: speed, timestamp: Date())
                LocationManager.shared.locationVariable.accept(location)
            }
        } else { // No speed and course
            // Let's build a CLLocation object and update the current one
            let location = CLLocation(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: long), altitude: -1, horizontalAccuracy: 10, verticalAccuracy: -1, course: -1, speed: -1, timestamp: Date())
            LocationManager.shared.locationVariable.accept(location)
        }
    }
}
