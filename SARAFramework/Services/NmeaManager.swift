//
//  NmeaManager.swift
//  SARA Croisiere
//
//  Created by Tristan Derbanne on 19.03.19.
//  Copyright © 2019 Rose. All rights reserved.
//

import UIKit
import RxSwift
import CocoaAsyncSocket
import SystemConfiguration.CaptiveNetwork
import RxCocoa
import Reachability

public protocol NmeaManagerDelegate {
    func displayWarningGPS()
}

public class NmeaManager: NSObject, GCDAsyncUdpSocketDelegate, GCDAsyncSocketDelegate {
    // MARK: - Variables and initialization
    // Singleton
    public static let `default` = NmeaManager()
    // delegate
    public var nmeaDelegate: NmeaManagerDelegate?
    
    //bundle
    let bundle = Utils.bundle(anyClass: NmeaManager.self)
    
    var connected = BehaviorRelay<Bool>(value: false)
    var currentSSID = BehaviorRelay<String?>(value: nil)
    var connexionStatus = BehaviorRelay<String?>(value: nil)
    
    // Logger
    weak var logger: NmeaLogger?
    
    // Socket
    private var udpSocket: GCDAsyncUdpSocket?
    private var tcpSocket: GCDAsyncSocket?
    
    // Reachability
    var reachability: Reachability?
    
    // Protocol
    // TODO: Get this from settings
    enum IpProtocol {
        case udp, tcp
    }
    var ipProtocol: IpProtocol = .udp {
        didSet {
            if _connected  || updConnected || tcpConnected {
//                print("=== DIS protocol")
                self.disconnect()
            }
            if Settings.shared.nmeaEnabled {
//                print("=== CONNECT PROTOCOL")
                self.connect()
            }
        }
    }
    
    var host: String? = Settings.shared.nmeaHost {
        didSet {
            self.observerHostReachability()
            if _connected  || updConnected || tcpConnected {
//                print("=== DIS HOST")
                self.disconnect()
            }
            if Settings.shared.nmeaEnabled {
//                print("=== CONNECT HOST")
                self.connect()
            }
        }
    }
    
    // Default NMEA port: 10110
    var port: UInt16 = UInt16(Settings.shared.nmeaPort) {
        didSet {
            if _connected || updConnected || tcpConnected {
//                print("=== DIS PORT")
                self.disconnect()
            }
            if Settings.shared.nmeaEnabled {
//                print("=== CONNECT PORT")
                self.connect()
            }
        }
    }
    
    // Parser
    private var parser = NmeaParser()
    
    //
    let disposeBag = DisposeBag()
    
    // Recording nmea log
    var isLoggingOn: Bool = false
    var fileName: String?
    
    var nmeaLogFile: URL? {
        let dirPathNoScheme = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        guard let documentsDirectory = URL(string: dirPathNoScheme) else { return nil }

        let dataPath = documentsDirectory.appendingPathComponent("NMEA")
        if !FileManager.default.fileExists(atPath: dataPath.absoluteString) {
            do {
                try FileManager.default.createDirectory(atPath: dataPath.absoluteString, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print(error.localizedDescription)
            }
        }
        // There is file:// at the begining
        guard let documentsDirectory2 = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }
        
        if let name = self.fileName {
            let dataPath2 = documentsDirectory2.appendingPathComponent("NMEA")
            let fileName = "\(name).txt"
            return dataPath2.appendingPathComponent(fileName)
        }
        return nil
    }
    
    override init() {
        super.init()
        self.parser.nmeaManager = self

        self.observerHostReachability()
        
        if Settings.shared.nmeaTCPEnabled {
            self.ipProtocol = .tcp
        }
        
        self.setUpObserver()
        self.currentSSID.accept(getCurrentSSID())
//        print("=== ssid: \(self.currentSSID.value)")
        
        // If the application comes to foreground, if the navigation center
        // is enabbled, and if we are disconnected, we will attempt to reconnect
        NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification, object: nil, queue: nil) { (_) in
            if Settings.shared.nmeaEnabled {
                if !self.connected.value {
                    print("=== CONNECT didBecomeActiveNotification")
                    self.connectionAttemptsCount = 1
                    self.connect()
                }
            } else if !Settings.shared.usePhoneGPS {
                self.nmeaDelegate?.displayWarningGPS()
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func startLogs() {
        if fileName == nil {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd_HH:mm:ss"
            let dateString = formatter.string(from: Date()) // string purpose I add here
            self.fileName = "\(dateString)"
        }
    }
    
    func stopLogs() {
        self.fileName = nil
    }
    
    func writeToFile(_ message: String)  {
        guard let logFile = nmeaLogFile else {
            return
        }
       
        guard let data = (message + "\n").data(using: .utf8) else {
            return
        }
        if FileManager.default.fileExists(atPath: logFile.path) {
            if let fileHandle = try? FileHandle(forWritingTo: logFile) {
                fileHandle.seekToEndOfFile()
                fileHandle.write(data)
                fileHandle.closeFile()
            }
        } else {
            do {
                try data.write(to: logFile, options: .atomicWrite)
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    // MARK: - NMEA Variables
    
    // Depth (DBT)
    var depthBelowTransducerFeet = BehaviorRelay<Float?>(value: nil)
    var depthBelowTransducerMeters = BehaviorRelay<Float?>(value: nil)
    var depthBelowTransducerFathoms = BehaviorRelay<Float?>(value: nil)
    
    // Wather temperature (MTW)
    var waterTemperatureCelcius = BehaviorRelay<Float?>(value: nil)
    
    // Track Made Good and Ground Speed (VTG) -> vitesse fond
    var headingTrueDegrees = BehaviorRelay<Float?>(value: nil)
    var headingMagneticDegrees = BehaviorRelay<Float?>(value: nil)
    var speedOverGroundKnots = BehaviorRelay<Float?>(value: nil)
    var speedOverGroundKmPerHour = BehaviorRelay<Float?>(value: nil)
    
    // Water Speed and Heading (VHW) -> vitesse surface : Speed Through Water
    var speedThroughWaterKnots = BehaviorRelay<Float?>(value: nil)
    var speedThroughWaterKmPerHour = BehaviorRelay<Float?>(value: nil)
    
    // Max Speed Though Water Boat
    var maxSpeedThroughWaterKnots = BehaviorRelay<Float?>(value: nil)
    var maxSpeedThroughWater10sKnots = BehaviorRelay<Float?>(value: nil)
    private var speedThroughWaterHistoryKnots = [(Date, Float)]()
    var maxSpeedThroughWaterKmPerHour = BehaviorRelay<Float?>(value: nil)
    var maxSpeedThroughWater10sKmPerHour = BehaviorRelay<Float?>(value: nil)
    private var speedThroughWaterHistoryKmPerHour = [(Date, Float)]()
    
    // Depth – (DPT)
    var depthMeters = BehaviorRelay<Float?>(value: nil)
    var offsetFromTransducer = BehaviorRelay<Float>(value: 0)
    
    // Relative wind speed and angle (VWR)
    var windRelativeAngleDegrees = BehaviorRelay<(Float, NavigationUnit)?>(value: nil)
    var windRelativeSpeedKnots = BehaviorRelay<Float?>(value: nil)
    var windRelativeSpeedKmPerHour = BehaviorRelay<Float?>(value: nil)
    
    // True wind speed and angle (MWV)
    var windTrueAngleDegrees = BehaviorRelay<(Float, NavigationUnit)?>(value: nil)
    var windTrueSpeedKnots = BehaviorRelay<Float?>(value: nil)
    var windTrueSpeedKmPerHour = BehaviorRelay<Float?>(value: nil)
    
    // Max Speed True Wind
    var maxSpeedWind10sKnots = BehaviorRelay<Float?>(value: nil)
    private var speedHistoryWindKnots = [(Date, Float)]()
    var maxSpeedWind10sKmPerHour = BehaviorRelay<Float?>(value: nil)
    private var speedWindHistoryKmPerHour = [(Date, Float)]()
    var maxSpeedWindKnots = BehaviorRelay<Float?>(value: nil)
    var maxSpeedWindKmPerHour = BehaviorRelay<Float?>(value: nil)
    
    // Wind direction
    var windDirection = BehaviorRelay<Float?>(value: nil)
    
    // TODO: Air temperature
    var airTemperatureCelcius = BehaviorRelay<Float?>(value: nil)
    // TODO: Cross Track Error – Dead Reckoning (XDR)
    
    // TODO: Recommended Minimum Navigation Information (RMC)
    
    // MARK: - Computed variables
    
    private func setUpObserver() {
        // Offset for the depth
        self.offsetFromTransducer
            .asObservable()
            .subscribe(onNext: {[weak self] (offset) in
                guard let weak = self, let depth = self?.depthMeters.value else {
                    return
                }
                if depth > 0 {
                    weak.depthMeters.accept(weak.depthMeters.value! + weak.offsetFromTransducer.value)
                }
            }).disposed(by: disposeBag)
        
        //Update max speed Knots
        self.speedThroughWaterKnots
            .asObservable()
            .subscribe(onNext: {[weak self] (speed) in
                // Update max speed
                if speed != nil {
                    if speed! > (self?.maxSpeedThroughWaterKnots.value ?? 0) {
                        self?.maxSpeedThroughWaterKnots.accept(speed)
                    }
                    self?.updateSpeedHistory(with: speed!, for: 0)
                }
            }).disposed(by: disposeBag)
        
        //Update max speed Knots
        self.speedThroughWaterKmPerHour
            .asObservable()
            .subscribe(onNext: {[weak self] (speed) in
                // Update max speed
                if speed != nil {
                    if speed! > (self?.maxSpeedThroughWaterKmPerHour.value ?? 0) {
                        self?.maxSpeedThroughWaterKmPerHour.accept(speed)
                    }
                    self?.updateSpeedHistory(with: speed!, for: 1)
                }
            }).disposed(by: disposeBag)
        
        //Update max speed True Wind Knots
        self.windTrueSpeedKnots
            .asObservable()
            .subscribe(onNext: {[weak self] (speed) in
                // Update max speed if needed
                if speed != nil {
                    if speed! > (self?.maxSpeedWindKnots.value ?? 0) {
                        self?.maxSpeedWindKnots.accept(speed)
                    }
                    self?.updateSpeedHistory(with: speed!, for: 2)
                }
            }).disposed(by: disposeBag)
        
        //Update max speed True Wind KmPerHour
        self.windTrueSpeedKmPerHour
            .asObservable()
            .subscribe(onNext: {[weak self] (speed) in
                // Update max speed if needed
                if speed != nil {
                    if speed! > (self?.maxSpeedWindKmPerHour.value ?? 0) {
                        self?.maxSpeedWindKmPerHour.accept(speed)
                    }
                    self?.updateSpeedHistory(with: speed!, for: 3)
                }
            }).disposed(by: disposeBag)
    }
    
    private func updateSpeedHistory(with speed: Float, for type: Int) {
        let now = Date()
        var speedHistory = [(Date, Float)]()
        switch type {
        case 0: //speed knots
            speedHistory = speedThroughWaterHistoryKnots
        case 1:
            speedHistory = speedThroughWaterHistoryKmPerHour
        case 2:
            speedHistory = speedHistoryWindKnots
        case 3:
            speedHistory = speedThroughWaterHistoryKmPerHour
        default:
            speedHistory = speedThroughWaterHistoryKnots
        }
        
        speedHistory.append((now, speed))
        if speedHistory.count == 1 {
            switch type {
            case 0: //speed knots
                maxSpeedThroughWater10sKnots.accept(speed)
            case 1:
                maxSpeedThroughWater10sKmPerHour.accept(speed)
            case 2:
                maxSpeedWind10sKnots.accept(speed)
            case 3:
                maxSpeedWind10sKmPerHour.accept(speed)
            default:
                maxSpeedThroughWater10sKnots.accept(speed)
            }
            return
        }
        var index = speedHistory.count - 1
        var maxSpeed: Float = 0
        var within10s = true
        while index >= 0 && within10s {
            if speedHistory[index].0.timeIntervalSince(now) >= -10 {
                if maxSpeed < speedHistory[index].1 {
                    maxSpeed = speedHistory[index].1
                }
                index -= 1
            } else {
                within10s = false
            }
        }
        if !within10s {
            speedHistory.removeSubrange(0...index)
        }
        switch type {
        case 0: //speed knots
            maxSpeedThroughWater10sKnots.accept(maxSpeed)
            speedThroughWaterHistoryKnots = speedHistory
        case 1:
            maxSpeedThroughWater10sKmPerHour.accept(maxSpeed)
            speedThroughWaterHistoryKmPerHour = speedHistory
        case 2:
            maxSpeedWind10sKnots.accept(maxSpeed)
            speedHistoryWindKnots = speedHistory
        case 3:
            maxSpeedWind10sKmPerHour.accept(maxSpeed)
            speedThroughWaterHistoryKmPerHour = speedHistory
        default:
            maxSpeedThroughWater10sKnots.accept(maxSpeed)
            speedThroughWaterHistoryKnots = speedHistory
        }
    }
    
    func resetMaxSpeeds() {
        self.maxSpeedThroughWaterKnots.accept(0)
        self.maxSpeedThroughWaterKmPerHour.accept(0)
        self.maxSpeedThroughWater10sKnots.accept(0)
        self.maxSpeedThroughWater10sKmPerHour.accept(0)
        self.maxSpeedWindKmPerHour.accept(0)
        self.maxSpeedWind10sKnots.accept(0)
        self.maxSpeedWind10sKmPerHour.accept(0)
        self.maxSpeedWindKnots.accept(0)
        self.maxSpeedWindKmPerHour.accept(0)
    }
    
    func resetValues() {
        
        self.depthBelowTransducerFeet.accept(nil)
        self.depthBelowTransducerMeters.accept(nil)
        self.depthBelowTransducerFathoms.accept(nil)
        
        // Wather temperature (MTW)
        self.waterTemperatureCelcius.accept(nil)
        
        // Track Made Good and Ground Speed (VTG) -> vitesse fond
        self.headingTrueDegrees.accept(nil)
        self.headingMagneticDegrees.accept(nil)
        self.speedOverGroundKnots.accept(nil)
        self.speedOverGroundKmPerHour.accept(nil)
        
        // Water Speed and Heading (VHW) -> vitesse surface
        self.speedThroughWaterKnots.accept(nil)
        self.speedThroughWaterKmPerHour.accept(nil)
        
        // Max Speed Boat
        self.maxSpeedThroughWaterKnots.accept(nil)
        self.maxSpeedThroughWater10sKnots.accept(nil)
        self.speedThroughWaterHistoryKnots = []
        self.maxSpeedThroughWaterKmPerHour.accept(nil)
        self.maxSpeedThroughWater10sKmPerHour.accept(nil)
        self.speedThroughWaterHistoryKmPerHour = []
        
        // Depth – (DPT)
        self.depthMeters.accept(nil)
        self.offsetFromTransducer.accept(0)
        
        // Relative wind speed and angle (VWR)
        self.windRelativeAngleDegrees.accept(nil)
        self.windRelativeSpeedKnots.accept(nil)
        self.windRelativeSpeedKmPerHour.accept(nil)
        
        // True wind speed and angle (MWV)
        self.windTrueAngleDegrees.accept(nil)
        self.windTrueSpeedKnots.accept(nil)
        self.windTrueSpeedKmPerHour.accept(nil)
        
        // Max Speed True Wind
        self.maxSpeedWind10sKnots.accept(nil)
        self.speedHistoryWindKnots = []
        self.maxSpeedWind10sKmPerHour.accept(nil)
        self.speedWindHistoryKmPerHour = []
        self.maxSpeedWindKnots.accept(nil)
        self.maxSpeedWindKmPerHour.accept(nil)
        
        // Wind direction
        self.windDirection.accept(nil)
        
        // Air temperature
        self.airTemperatureCelcius.accept(nil)
    }

    
    // MARK: - Connection
    private var connectionAttemptsCount = 1
    
    private var _connected: Bool = false {
        didSet {
            self.connected.accept(_connected)
        }
    }
    
    func connect() {
        
//        if _connected && (!tcpConnected || !updConnected){
//            self.disconnect()
//        }
//        else {
        guard let _ = self.currentSSID.value else {
            return
        }
//        print("=== CONNECT: _connected: \(_connected), tcpConnected: \(tcpConnected), \(updConnected), status = \(self.reachability?.connection)")
        if self.ipProtocol == .tcp && !tcpConnected {
            if _connected {
//                print("=== DIS Connect tcp")
                self.disconnect()
            }
//            print("=== CONNECT TCP: \(self.connectionAttemptsCount)")
            self.connectTcp()
//            self.connectionAttemptsCount += 1
//            print("=== CONNECT TCP: \(self.connectionAttemptsCount)")
        }
        else if self.ipProtocol == .udp && !updConnected {
            if _connected {
//                print("=== DIS Connect udp")
                self.disconnect()
            }
//            print("=== CONNECT UPD")
            self.connectUdp()
            self.connectionAttemptsCount += 1
        }
            
//        }
    }
    
    public func disconnect() {
        self.resetValues()
        self.connectionAttemptsCount = 1
//        print("=== DISCONNECT")
        self.connexionStatus.accept(NSLocalizedString("disconnected", bundle: bundle, comment: ""))
        _connected = false
        if udpSocket != nil {
//            print("=== DISCONNECT UDP")
            udpSocket!.close()
            udpSocket = nil
            receivedData = false
//            _connected = false
        }
        if tcpSocket != nil {
//            print("=== DISCONNECT TCP")
            tcpSocket!.disconnect()
            tcpSocket = nil
        }
        stopLogs()
    }
    
    var updConnected: Bool = false
    private func connectUdp() {
        updConnected = true
        // TODO: Wo we really wanna parse in the main queue?
        udpSocket = GCDAsyncUdpSocket(delegate: self, delegateQueue: DispatchQueue.main)
        udpSocket?.setIPv4Enabled(true)
        udpSocket?.setIPv6Enabled(false)
        
        do {
            try udpSocket?.bind(toPort: port)
            try udpSocket?.enableBroadcast(true)
            _connected = true
            try udpSocket?.beginReceiving()
            self.connexionStatus.accept("\(NSLocalizedString("try_connexion", bundle: bundle, comment: "")) \(self.connectionAttemptsCount)")
//            udpSocket?.send(<#T##data: Data##Data#>, withTimeout: <#T##TimeInterval#>, tag: <#T##Int#>)
            let _ = Timer.scheduledTimer(withTimeInterval: 30.0, repeats: false) { timer in
                print("Timer fired!")
                if !self.receivedData && !Settings.shared.usePhoneGPS { // if Centrale GPS used add a warning
                    self.nmeaDelegate?.displayWarningGPS()
                }
            }
        } catch {
            print("=== UDP Socket error: " + error.localizedDescription)
            if !Settings.shared.usePhoneGPS { // if Centrale GPS used add a warning
                self.nmeaDelegate?.displayWarningGPS()
            }
        }
    }
    
    var tcpConnected: Bool = false
    var receivedData: Bool = false
    private func connectTcp() {
        tcpConnected = true
        guard let host = self.host else {
            // TODO: Handle that differently for example through delegation to alert user of the missing configuration
            print("=== Missing host")
            self.connexionStatus.accept("\(NSLocalizedString("warning_empty_host", bundle: bundle, comment: ""))")
            if !Settings.shared.usePhoneGPS { // if Centrale GPS used add a warning
                self.nmeaDelegate?.displayWarningGPS()
            }
            return
        }
        // TODO: Wo we really wanna parse in the main queue?
        tcpSocket = GCDAsyncSocket(delegate: self, delegateQueue: DispatchQueue.main)
        
        do {
//            try tcpSocket?.connect(toHost: host, onPort: self.port)
            try tcpSocket?.connect(toHost: host, onPort: self.port, withTimeout: 30)
            self.connexionStatus.accept("\(NSLocalizedString("try_connexion", bundle: bundle, comment: "")) \(self.connectionAttemptsCount)")
//            logger?.add("Try to connect... \n ")
        }
        catch {
            print("=== TCP Socket error: " + error.localizedDescription)
            logger?.add("TCP Socket error: \(error.localizedDescription) \n ")
        }
    }
    
    // Reachability
    private func observerHostReachability() {
        if self.host != nil {
            try? self.reachability = Reachability(hostname: self.host!)
            NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(_:)), name: NSNotification.Name.reachabilityChanged, object: nil)
            try? self.reachability?.startNotifier()
        }
    }
    
    @objc private func reachabilityChanged(_ note: Notification) {
        //TODO: Check if ok
        let newSSID = self.getCurrentSSID()
        let status = self.reachability!.connection
        self.currentSSID.accept(newSSID)
        if status == .wifi {
            if Settings.shared.nmeaEnabled /*&& self.currentSSID.value != newSSID*/ {
//                print("=== reachabilityChanged")
                self.connect()
            }
        } else {
//            if self.currentSSID.value != newSSID {
//                print("=== DIS Reachability")
            self.disconnect()
            if newSSID == nil && !Settings.shared.usePhoneGPS { // if Centrale GPS used add a warning
                self.nmeaDelegate?.displayWarningGPS()
            }
//            }
        }
        
//        print("=== ssidr: \(self.currentSSID.value)")
    }
    
    // Returns the current ssid if connected to some wifi
    private func getCurrentSSID() ->  String? {
        if let interfaces = CNCopySupportedInterfaces() {
            for i in 0..<CFArrayGetCount(interfaces){
                let interfaceName: UnsafeRawPointer = CFArrayGetValueAtIndex(interfaces, i)
                let rec = unsafeBitCast(interfaceName, to: AnyObject.self)
                let unsafeInterfaceData = CNCopyCurrentNetworkInfo("\(rec)" as CFString)
                
                if let unsafeInterfaceData = unsafeInterfaceData as? Dictionary<AnyHashable, Any> {
                    return unsafeInterfaceData["SSID"] as? String
                }
            }
        }
        return nil
    }
    
    // MARK: - GCDAsyncUdpSocketDelegate implementation
    
    public func udpSocket(_ sock: GCDAsyncUdpSocket, didReceive data: Data, fromAddress address: Data, withFilterContext filterContext: Any?) {
        self.receivedData = true
        if self.connexionStatus.value != NSLocalizedString("connected", bundle: bundle, comment: "") {
            self.connexionStatus.accept(NSLocalizedString("connected", bundle: bundle, comment: ""))
        }
        if let msg = String(bytes: data, encoding: String.Encoding.utf8) {
            self.connectionAttemptsCount = 1
            logger?.add(msg)
            writeToFile(msg)
            let nmeaSentences = msg.components(separatedBy: "\n")
            nmeaSentences.forEach({
                do {
                    try self.parser.parse(nmeaMessage: $0)
                }
                catch NmeaParser.NmeaParserError.unknownType {
    //                print("=== Unknown message, skipping: " + msg)
                    TellTalesManager.default.parse(tellTaleMessage: $0)
                }
                catch NmeaParser.NmeaParserError.checksum {
    //                print("=== Checksum failed: " + msg)
                    if msg.prefix(1) == "N" { // Check if it's a telltale like this: N;110;41-4D-D5;+0022;3,034;+24,16;00;00;+000;+000;+0022
                        TellTalesManager.default.parse(tellTaleMessage: $0)
                    }
                }
                catch NmeaParser.NmeaParserError.malformed {
    //                print("=== Malformed message: " + msg)
                }
                catch {
                    print("=== Failed to parse NMEA message: " + error.localizedDescription)
                }
            })
        }
    }
    public func udpSocketDidClose(_ sock: GCDAsyncUdpSocket, withError error: Error?) {
//        print("=== CLOSE UPD")
        if !tcpConnected {
            if let error = error?.localizedDescription {
                self.connexionStatus.accept(String(format: NSLocalizedString("disconnected_error", bundle: bundle, comment: ""), error))
            } else {
                self.connexionStatus.accept(NSLocalizedString("disconnected", bundle: bundle, comment: ""))
            }
        }
        _connected = false
        updConnected = false
        self.receivedData = false
    }
    
    // MARK: - GCDAsyncSocketDelegate implementation
    private let EOL = "\r\n".data(using: .utf8)!

    private func readNext(from socket: GCDAsyncSocket) {
        socket.readData(to: EOL, withTimeout: -1, tag: 0)
    }
    public func socket(_ sock: GCDAsyncSocket, didConnectToHost host: String, port: UInt16) {
//        print("=== didConnectToHost")
        _connected = true
//        logger?.add("Connected... \(_connected), nb attempt: \(connectionAttemptsCount)\n")
        self.connexionStatus.accept(NSLocalizedString("connected", bundle: bundle, comment: ""))
        self.readNext(from: sock)
    }
    
    public func socket(_ sock: GCDAsyncSocket, didRead data: Data, withTag tag: Int) {
        if let msg = String(data: data, encoding: .utf8) {
            self.connectionAttemptsCount = 1
            logger?.add(msg)
            writeToFile(msg)
            do {
                try self.parser.parse(nmeaMessage: msg)
            }
            catch NmeaParser.NmeaParserError.unknownType {
//                print("=== Unknown message, skipping: " + msg)
                TellTalesManager.default.parse(tellTaleMessage: msg)
           }
            catch NmeaParser.NmeaParserError.checksum {
//                print("=== Checksum failed: " + msg)
                if msg.prefix(1) == "N" { // Check if it's a telltale like this: N;110;41-4D-D5;+0022;3,034;+24,16;00;00;+000;+000;+0022
                    TellTalesManager.default.parse(tellTaleMessage: msg)
                }
                if msg.prefix(3) == "Pos" {
                    self.parser.parsePosition(msg: msg)
                }
            }
            catch NmeaParser.NmeaParserError.malformed {
//                print("=== Malformed message: " + msg)
            }
            catch {
//                print("=== Failed to parse NMEA message: " + error.localizedDescription)
            }
        }
        self.readNext(from: sock)
    }
    
    public func socketDidDisconnect(_ sock: GCDAsyncSocket, withError err: Error?) {

        _connected = false
        tcpConnected = false
//        logger?.add("Disconnect... \(_connected), nb attempt: \(connectionAttemptsCount)\n ")
//        print("=== socketDidDisconnect GCDAsyncSocket")
        if Settings.shared.nmeaEnabled && self.ipProtocol == .tcp && self.currentSSID.value != nil {
            // Attempting to reconnect
//            logger?.add("DISCONNECT:... \(_connected), nb attempt: \(connectionAttemptsCount)\n ")
//            print("=== DISCONNECT because of failure: \(String(describing: err?.localizedDescription)), nb attempt: \(connectionAttemptsCount)" )
            if let error = err?.localizedDescription {
                self.connexionStatus.accept(String(format: NSLocalizedString("disconnected_error", bundle: bundle, comment: ""), error))
            } else {
                self.connexionStatus.accept(NSLocalizedString("disconnected", bundle: bundle, comment: ""))
            }
            if self.connectionAttemptsCount <= 3 {
//                print("=== RECONNECT because of failure: \(String(describing: err?.localizedDescription)), nb attempt: \(connectionAttemptsCount)" )
//                logger?.add("Try to connect2... \(_connected), nb attempt: \(connectionAttemptsCount)\n ")
//                print("=== socketDidDisconnect")
                self.connectionAttemptsCount += 1
                self.connect()
            } else {
                if !Settings.shared.usePhoneGPS { // if Centrale GPS used add a warning
                    self.nmeaDelegate?.displayWarningGPS()
                }
//                print("=== DISCONNECT nb attempt: \(connectionAttemptsCount)" )
                self.disconnect()
            }
        }
    }
}

