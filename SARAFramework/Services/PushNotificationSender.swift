//
//  PushNotificationSender.swift
//  SARAFramework
//
//  Created by Marine on 13.09.22.
//

import UIKit

public class PushNotificationSender {
    public static let shared = PushNotificationSender()
    
    public func sendPushNotification(to token: String, title: String, body: String, data: [String:Any]) {
        print("Send push notification")
        let urlString = "https://fcm.googleapis.com/fcm/send"
        let url = NSURL(string: urlString)!
        let paramString: [String : Any] = ["to" : token,
                                           "notification" : ["title" : title, "body" : body, "click_action" : "CollectiveStart"],
                                           "data" : data]

        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject:paramString, options: [.prettyPrinted])
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("key=AAAAD6pVyHI:APA91bF6rjbk8ikL6ZS8feFj1ewRSCra8sI3aGMtZ_HIsHqHK6v7l0qR8ac5RwDp4AXlfaS50XiCa2jAUP3Zsi3b44eZSzUef3zbBj_TtumPshrpLd4K12pKp8h3ZjeAl4kuaIY9OPk2", forHTTPHeaderField: "Authorization")

        let task =  URLSession.shared.dataTask(with: request as URLRequest)  { (data, response, error) in
            do {
                if let jsonData = data {
                    if let jsonDataDict  = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: AnyObject] {
                        NSLog("Received data:\n\(jsonDataDict))")
                    }
                }
            } catch let err as NSError {
                print(err.debugDescription)
            }
        }
        task.resume()
    }
}
