//
//  NavigationHelper.swift
//  SARA Croisiere
//
//  Created by Marine IL on 15.04.19.
//  Copyright © 2019 Rose. All rights reserved.
//

import Foundation
import CoreLocation
import UTMConversion

public class NavigationHelper {
    
    class func getCourseFrom(location: CLLocation?) -> Int? {
        if Settings.shared.developerMode { // In developper mode COG = 0
            return 0
        }
        guard let course = location?.course, course >= 0 else {
            return nil
        }
        return Int(course)
    }
    
    class func getCompass() -> Int? {
        guard let compass = LocationManager.shared.compassVariable.value?.trueHeading, compass >= 0 else {
            return nil
        }
        return Int(compass)
    }
    
    class func getSpeedFrom(location: CLLocation?, unit: NavigationUnit?) -> Float? {
        // The speed given is now in meter per second
        if Settings.shared.developerMode { // In developper mode SOG = 0
            return 0
        }
        
        guard let speed = location?.speed, speed > 0 else {
            return nil
        }
        
        // Transform the value to the current unit
        guard unit != nil else {
            return Float(speed).msToKnots()
        }
        
        // Compute speed and update announcement variable
        switch unit! {
        case .kmPerHour:
            return Float(speed).msToKmh()
        case .knots:
            return Float(speed).msToKnots()
        default:
            return Float(speed).msToKnots()
        }
    }
    
    class func getDistanceFrom(location: CLLocation?, to point: CoursePoint?) -> Float? {
        guard let safePoint = point.value, location != nil else {
            return nil
        }
        let pointLocation = CLLocation(latitude: CLLocationDegrees(safePoint.gpsPosition?.latitude ?? 0), longitude: CLLocationDegrees(safePoint.gpsPosition?.longitude ?? 0))
        
        // Compute the distance in meters
        let distance = location!.distance(from: pointLocation)
        return Float(distance)
    }
    
    public class func getDistanceFrom(location: CLLocation?, to gpsPosition: GPSPosition?) -> Float? {
        guard let safePosition = gpsPosition, location != nil else {
            return nil
        }
        let pointLocation = CLLocation(latitude: CLLocationDegrees(safePosition.latitude), longitude: CLLocationDegrees(safePosition.longitude))
        
        // Compute the distance in meters
        let distance = location!.distance(from: pointLocation)
        return Float(distance)
    }
    
    class func getDistanceFrom(gpsPosition1: GPSPosition?, to gpsPosition2: GPSPosition?) -> Float? {
        guard let pos1 = gpsPosition1, let pos2 = gpsPosition2 else {
            return nil
        }
        let clLoc1 = CLLocation(latitude: CLLocationDegrees(pos1.latitude), longitude: CLLocationDegrees(pos1.longitude))
        let clLoc2 = CLLocation(latitude: CLLocationDegrees(pos2.latitude), longitude: CLLocationDegrees(pos2.longitude))
        
        // Compute the distance in meters
        let distance = clLoc1.distance(from: clLoc2)
        return Float(distance)
    }
        
    class func getBearingFrom(gpsPosition: GPSPosition?, location: CLLocation?) -> [(Int, NavigationUnit?)?]? {
        guard let safePosition = gpsPosition, location != nil else {
            return nil
        }
        let pLat = safePosition.latitude.radians
        let pLong = safePosition.longitude.radians
        let lLat = Float(location!.coordinate.latitude).radians
        let lLong = Float(location!.coordinate.longitude).radians
        
        //convert the bearing in degrees
        var bearing = atan2(sinf(pLong - lLong) * cosf(pLat), cosf(lLat) * sinf(pLat) - sinf(lLat) * cosf(pLat) * cosf(pLong - lLong)).degrees
        
        if bearing < 0 {
            bearing = 360 + bearing
        }
        let bearingInt = Int(bearing)
        //Store in the first position azimut in degrees and in the second one in gisement
        var bearingsArray:[(Int, NavigationUnit?)?] = []
        bearingsArray.append((bearingInt, .degrees))
        // If we want to use port and starboard
        // Further computations is needed
        var valueToUsed: Int
        if Settings.shared.useCompass { // We can use the course and the compass to calculate the relative bearing
            guard let compass = getCompass() else {
                bearingsArray.append(nil) // for starboard / port value
                bearingsArray.append(nil) // for time value
                return bearingsArray
            }
            valueToUsed = compass
        } else {
            if Settings.shared.developerMode { // In developper mode COG = 0
                valueToUsed = 0
            } else {
                guard let currentCourse = LocationManager.shared.getCurrentPosition()?.course, currentCourse > -1 else {
                    bearingsArray.append(nil) // for starboard / port value
                    bearingsArray.append(nil) // for time value
                    return bearingsArray
                }
                valueToUsed = Int(currentCourse)
            }
        }
        
        var bearingTB = bearingInt - valueToUsed
        
        if bearingTB < 0 {
            bearingTB += 360
        }
        let bearingDegree = bearingTB
        if bearingTB < 180 {
            bearingTB = abs(bearingTB)
            bearingsArray.append((bearingTB, .starboard))
        } else {
            bearingTB = abs(360 - bearingTB)
            bearingsArray.append((bearingTB, .port))
        }
        // Convert bearing in time
        let bearingTime = Utils.degreeToTime(val1: bearingDegree)
        bearingsArray.append((bearingTime, .timeDegrees))
        return bearingsArray
    }
        
    class func getCMGFrom(gpsPosition: GPSPosition?, location: CLLocation?) -> Float? {
        guard let safePosition = gpsPosition, location != nil else {
            return nil
        }
        let bearing = getBearingFrom(gpsPosition: safePosition, location: location)
        let speed = getSpeedFrom(location: location, unit: Settings.shared.speedUnit?.unit)
        let course = getCourseFrom(location: location)
        if course == nil || speed == nil || bearing == nil {
            return nil
        }
        var cmgInfo:(Int, Float) = (0, 0)
        // If the bearing was already computed with starboard/port
        // We can send it direclty like that
        if Settings.shared.bearingUnit?.unit == .spDegrees {
            guard bearing![1] != nil else {
                return nil
            }
            cmgInfo = (bearing![1]!.0, speed!)
        } else {
            // If not, we have to compute the value in the right unit before sending it to CMG computation
            var bearingTB = 0
            guard bearing![0] != nil else {
                return nil
            }
            bearingTB = bearing![0]!.0 - course!
            if bearingTB < 0 {
                bearingTB += 360
            }
            if bearingTB < 180 {
                bearingTB = labs(bearingTB)
            } else {
                bearingTB = 360 - bearingTB
                bearingTB = labs(bearingTB)
            }
            cmgInfo = (bearingTB, speed!)
        }
        
        var cmgValue = Float(Double(cmgInfo.1) * cos(abs(cmgInfo.0).degreesToRadians())).formatted
        if cmgValue == -0.0 {
            cmgValue = 0.0
        }
        return cmgValue
    }
        
    class func getGapToRouteSegmentFrom(gpsPosition: GPSPosition?, location: CLLocation?) -> Float? {
        // init with current point
        if let point1  = NavigationManager.shared.getPreviousPointWithCoord()?.point {
            var gpsPositionPrevious: GPSPosition?
            if let coursePoint = point1 as? CoursePoint {
                gpsPositionPrevious = coursePoint.gpsPosition
            } else if let mark = point1 as? Mark {
                gpsPositionPrevious = mark.middlePoint()
            }
            guard let gpsPosPrevious = gpsPositionPrevious else {
                return nil
            }
            NavigationManager.shared.previousPosition = CLLocation(latitude: CLLocationDegrees(gpsPosPrevious.latitude), longitude: CLLocationDegrees(gpsPosPrevious.longitude))
        } else {
            if NavigationManager.shared.previousPosition == nil && LocationManager.shared.getCurrentPosition() != nil {
                NavigationManager.shared.previousPosition = LocationManager.shared.getCurrentPosition()
            }
        }

        if let p1 = NavigationManager.shared.previousPosition , let p2 = gpsPosition, let currentPoint = location {
            let UTMCoor1 = CLLocation(latitude: CLLocationDegrees(p2.latitude), longitude: CLLocationDegrees(p2.longitude)).utmCoordinate()
            let x1 = UTMCoor1.easting
            let y1 = UTMCoor1.northing

            let UTMCoor2 = p1.utmCoordinate()
            let x2 = UTMCoor2.easting
            let y2 = UTMCoor2.northing

            //Reference changement
            let UTMCoorCurrentPoint = currentPoint.utmCoordinate()
            let x = UTMCoorCurrentPoint.easting
            let y = UTMCoorCurrentPoint.northing

            var theta = 0.0 //radian
            let xDiff = x1 - x2
            let yDiff = y1 - y2
            if y1 < y2 {
                theta = -acos( xDiff / sqrt((xDiff * xDiff) + (yDiff * yDiff )))
            } else {
                theta = acos( xDiff / sqrt((xDiff * xDiff) + (yDiff * yDiff )))
            }

            //Coordinates of the projected point of the boat on (BA) in (B,I,J) : (xp, 0)
            let xP = (x - x2) * cos(theta) + (y - y2) * sin(theta)
            //Get the GPS coordinates of the projected point
            let xZ = x2 + xP * cos(theta) - (0.0) * sin(theta)
            let yZ = y2 + xP * sin(theta) + (0.0) * cos(theta)

            let utmCoordinate = UTMCoordinate(northing: yZ, easting: xZ, zone: UTMCoorCurrentPoint.zone , hemisphere: UTMCoorCurrentPoint.hemisphere)
            let location = utmCoordinate.location()
            var d = location.distance(from: currentPoint)
            if d.isNaN {
                d = 0
            }
            return Float(d)
        }
        return nil
    }
    
    class func getDistanceSegmentFrom(gpsPosition1: GPSPosition?, gpsPosition2: GPSPosition?, location: GPSPosition?) -> Float? {
        if let p1 = gpsPosition1, let p2 = gpsPosition2, let currentPoint = location {
            let UTMCoor1 = CLLocation(latitude: CLLocationDegrees(p2.latitude), longitude: CLLocationDegrees(p2.longitude)).utmCoordinate()
            let x1 = UTMCoor1.easting
            let y1 = UTMCoor1.northing

            let UTMCoor2 = CLLocation(latitude: CLLocationDegrees(p1.latitude), longitude: CLLocationDegrees(p1.longitude)).utmCoordinate()
            let x2 = UTMCoor2.easting
            let y2 = UTMCoor2.northing

            //Reference changement
            let currentCLLocation = CLLocation(latitude: CLLocationDegrees(currentPoint.latitude), longitude: CLLocationDegrees(currentPoint.longitude))
            let UTMCoorCurrentPoint = currentCLLocation.utmCoordinate()
            let x = UTMCoorCurrentPoint.easting
            let y = UTMCoorCurrentPoint.northing

            var theta = 0.0 //radian
            let xDiff = x1 - x2
            let yDiff = y1 - y2
            if y1 < y2 {
                theta = -acos( xDiff / sqrt((xDiff * xDiff) + (yDiff * yDiff )))
            } else {
                theta = acos( xDiff / sqrt((xDiff * xDiff) + (yDiff * yDiff )))
            }

            //Coordinates of the projected point of the boat on (BA) in (B,I,J) : (xp, 0)
            let xP = (x - x2) * cos(theta) + (y - y2) * sin(theta)
            //Get the GPS coordinates of the projected point
            let xZ = x2 + xP * cos(theta) - (0.0) * sin(theta)
            let yZ = y2 + xP * sin(theta) + (0.0) * cos(theta)

            let utmCoordinate = UTMCoordinate(northing: yZ, easting: xZ, zone: UTMCoorCurrentPoint.zone , hemisphere: UTMCoorCurrentPoint.hemisphere)
            let location = utmCoordinate.location()
            var d = location.distance(from: currentCLLocation)
            if d.isNaN {
                d = 0
            }
//            print("=== \(Float(d))")
            return Float(d)
        }
        return nil
    }
    
    class func segmentOrientation(p1Location: CLLocation, p2Postion: GPSPosition) -> Int {
        
        // Compute the location of the point given
        let currentPointLat = Float(p1Location.coordinate.latitude).radians
        let currentPointLong = Float(p1Location.coordinate.longitude).radians
        let nextPointLat = p2Postion.latitude.radians
        let nextPointLong = p2Postion.longitude.radians
        
        //convert the bearing in degrees
        var bearing = atan2(sinf(nextPointLong - currentPointLong) * cosf(nextPointLat), cosf(currentPointLat) * sinf(nextPointLat) - sinf(currentPointLat) * cosf(nextPointLat) * cosf(nextPointLong - currentPointLong)).degrees
        
        if bearing < 0 {
            bearing = 360 + bearing
        }
        return Int(bearing)
    }
    
    class func segmentOrientation(position1: GPSPosition?, position2: GPSPosition?) -> Int {
        
        guard position1 != nil, position2 != nil else {
            return 0
        }
        // Compute the location of the point given
        let currentPointLat = position1!.latitude.radians
        let currentPointLong = position1!.longitude.radians
        let nextPointLat = position2!.latitude.radians
        let nextPointLong = position2!.longitude.radians
        
        //convert the bearing in degrees
        var bearing = atan2(sinf(nextPointLong - currentPointLong) * cosf(nextPointLat), cosf(currentPointLat) * sinf(nextPointLat) - sinf(currentPointLat) * cosf(nextPointLat) * cosf(nextPointLong - currentPointLong)).degrees
        
        if bearing < 0 {
            bearing = 360 + bearing
        }
        return Int(bearing)
    }
}
