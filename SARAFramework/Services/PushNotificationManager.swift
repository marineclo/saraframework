//
//  PushNotificationManager.swift
//  SARAFramework
//
//  Created by Marine on 13.09.22.
//

//import FirebaseFirestore
import FirebaseMessaging
import UIKit
import UserNotifications

public class PushNotificationManager: NSObject, MessagingDelegate, UNUserNotificationCenterDelegate {
    let userID: String
    public init(userID: String) {
        self.userID = userID
        super.init()
        //Add custom actions
//        let acceptAction = UNNotificationAction(identifier: "view_now",
//                                                title: "Xem ngay",
//                                                options: .foreground)
//
//        let skipAction = UNNotificationAction(identifier: "skip",
//                                                title: "Bỏ qua",
//                                                options: .foreground)
//
//        // Define the notification type
//        let viewCategory =
//              UNNotificationCategory(identifier: "CollectiveStart", //category name
//              actions: [acceptAction, skipAction],
//              intentIdentifiers: [],
//              hiddenPreviewsBodyPlaceholder: "",
//              options: .customDismissAction)
//
//        // Register the notification type.
//        UNUserNotificationCenter.current().setNotificationCategories([viewCategory])
    }

    public func registerForPushNotifications() {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM)
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }

        UIApplication.shared.registerForRemoteNotifications()
        updateFirestorePushTokenIfNeeded()
    }

    func updateFirestorePushTokenIfNeeded() {
        if let token = Messaging.messaging().fcmToken {
//            let usersRef = Firestore.firestore().collection("users_table").document(userID)
//            usersRef.setData(["fcmToken": token], merge: true)
        }
    }

    public func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Messaging")
        updateFirestorePushTokenIfNeeded()
    }
    
    // Receive displayed notifications for iOS 10 devices.
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("Will present")
        completionHandler([.alert, .badge, .sound])
    }

    public func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        if let timer = userInfo["timer"] as? String, let timestamp = userInfo["timestamp"] as? String, let courseId = userInfo["id"] as? String {
            NavigationManager.shared.setCurrentRoute(route: Course.getRealmCourse(courseId: courseId))
            FirebaseManager.shared.collectiveStart.accept(["timer": Int(timer), "timestamp": Int(timestamp), "id" : courseId])
        }
        completionHandler()
    }
}
