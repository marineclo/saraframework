//
//  FirebaseManager.swift
//  SARA Croisiere
//
//  Created by Rose on 14/12/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import FirebaseCore
import FirebaseAuth
import FirebaseDatabase
import FirebaseMessaging
import Realm
import RealmSwift
import RxSwift
import RxCocoa
import Reachability

public protocol FirebaseManagerDelegate {
    func courseChanged(course: Course?)
    func courseRemoved(courseId: String)
    func courseAdded(course: Course?)
}

public class FirebaseManager {
    public static let shared = FirebaseManager()
    public let reference: DatabaseReference
    public let coursesRef: DatabaseReference
    public var delegate: FirebaseManagerDelegate?
    
    public var removedRouteName: BehaviorRelay<String> = BehaviorRelay(value: "")
    public var collectiveStart: BehaviorRelay<[String : Any]> = BehaviorRelay(value: [:])
    public var firebaseCourses: [Course] = []
    public var lastUpdateTime: String?
    
    let bundle = Utils.bundle(anyClass: FirebaseManager.self)
    
    let realm = try! Realm()
    
    //declare this property where it won't go out of scope relative to your listener
    public let reachability: Reachability
    
    init() {
        // Get root reference of the base
        self.reference = Database.database().reference()
        self.coursesRef = self.reference.child("/courses")
        
        // To check internet connection
        reachability = try! Reachability()
        
        do {
            try reachability.startNotifier()
        } catch{
            print("could not start reachability notifier")
        }
    }
    
    // Get subscribed route and add observers to it
    public func checkSubscribedCourse() {
        if let subscribedCourse = Course.all().first(where: { $0.status == .subscribed}) {
            // Check if the course still exists
            let courseId = subscribedCourse.id
            let name = subscribedCourse.name
            subscribedCourse.removeCourseWithPoints()
            
            self.retrieveCourse(courseId: courseId, completion: { firebaseCourse in
                if firebaseCourse == nil {
                    let title = NSLocalizedString("course_deleted", bundle: self.bundle, comment: "")
                    let message = String(format: NSLocalizedString("subscribed_course_deleted", bundle: self.bundle, comment: ""), name)
                    if let topController = UIApplication.topViewController() {
                        let alert = Utils.showAlert(with: title, message: message)
                        topController.present(alert, animated: false)
                    }
                } else {
                    self.addObservers(course: firebaseCourse!)
                }
            })
        }
    }
    
    // Get all routes on firebase
    public func retrieveAllCourses() {
        self.coursesRef.observe(.childAdded, with: { snapshot in
            if let routeData = snapshot.value as? [String: Any] {
                if let reversed = routeData["reversed"] as? Bool { // SARA Nav
                    guard let course = CruiseCourse.decode(dictionary: routeData) else {
                        return
                    }
                    if let date = routeData["lastUpdateTime"] as? String {
                        course.date = date
                    }
                    
                    if let ownerId = routeData["owner"] as? String, let currentUser = Auth.auth().currentUser?.uid, ownerId == currentUser {
                        course.status = .shared
                    }
                    self.firebaseCourses.append(course)
                    self.delegate?.courseAdded(course: course)
                } else { // SARA Regatta
                    if let ownerId = routeData["owner"] as? String, let currentUser = Auth.auth().currentUser?.uid, ownerId != currentUser {
                        let course = Course()
                        if let id = routeData["id"] as? String {
                            course.id = id
                        }
                        if let name = routeData["name"] as? String {
                            course.name = name
                        }
                        if let subscribedCourse = Course.all().first(where: { $0.status == .subscribed}), subscribedCourse.id == course.id {
                            course.status = .subscribed
                        }
                        self.firebaseCourses.append(course)
                        self.delegate?.courseAdded(course: course)
                    }
                }
            }
        })
        // Add observer when route deleted
        self.coursesRef.observe(.childRemoved, with: { snapshot in
            if let routeData = snapshot.value as? [String: Any] {
                if let reversed = routeData["reversed"] as? Bool { // SARA Nav
                    if let name = routeData["name"] as? String {
                        if let index = FirebaseManager.shared.firebaseCourses.firstIndex(where: { $0.name == name}) {
                            self.firebaseCourses.remove(at: index)
                            self.delegate?.courseRemoved(courseId: name)
                        }
                    }
                } else { // SARA Regatta
                    if let ownerId = routeData["owner"] as? String, let currentUser = Auth.auth().currentUser?.uid, ownerId != currentUser {
                        let subscribedCourse = Course.all().first(where: { $0.status == .subscribed})
                        if let id = routeData["id"] as? String {
                            if let index = self.firebaseCourses.firstIndex(where: { $0.id == id}) {
                                self.firebaseCourses.remove(at: index)
                            }
                            if (subscribedCourse == nil || (subscribedCourse != nil && subscribedCourse!.id != id)) {
                                // Not subscribe course delete in firebase. Just update the courses
                                self.delegate?.courseRemoved(courseId: id)
                            } else if subscribedCourse != nil && subscribedCourse!.id == id{ // Subscribed course delete
                                self.removedRouteName.accept(subscribedCourse!.name)
                                // Reinit last time update
                                self.lastUpdateTime = nil
                                if NavigationManager.shared.route.value == subscribedCourse {
                                    NavigationManager.shared.setCurrentRoute(route: nil) // Disable route
                                }
                                // Delete route from database and disable course if needed
                                subscribedCourse!.removeCourseWithPoints()
                                self.delegate?.courseRemoved(courseId: id)
                            }
                        }
                    }
                }
            }
        })
    }
    
    // Get route from firebase when subscribe and save it
    public func retrieveCourse(courseId: String, completion: @escaping (Course?) -> Void) {
        self.coursesRef.child(courseId).observeSingleEvent(of: .value, with: { (snapshot) in
            if let courseDict = snapshot.value as? [String: Any] {
                if let reversed = courseDict["reversed"] as? Bool { // SARA Nav
                    completion(CruiseCourse.decode(dictionary: courseDict, save: true))
                } else { // SARA Regatta
                    completion(RaceCourse.decode(dictionary: courseDict))
                }
            } else {
                completion(nil)
            }
        })
    }
    
    // Add observers for a subscribed course
    func addObservers(course: Course) {
        let courseName = course.name
        let courseId = course.id
        // Observer when the course is modified
        self.coursesRef.child(course.id).observe(.childChanged, with: { (snapshot) in
            if snapshot.key == "lastCollectiveStart" { // Collective start
                if let snapshotValue = snapshot.value as? [String:Any] {
                    if let timer = snapshotValue["timer"] as? String, let timestamp = snapshotValue["timestamp"] as? String {
                        let state = UIApplication.shared.applicationState
                        let dict = ["id": courseId, "timer": Int(timer), "timestamp" : Int(timestamp)] as [String : Any]
                        if state == .background { // Add notification
                            if let userToken = Messaging.messaging().fcmToken {
                                let title =  Utils.localizedString(forKey: "shared_course", app: "SARARegate")
                                let message = Utils.localizedString(forKey: "collective_start_subscribers", app: "SARARegate")
                                PushNotificationSender.shared.sendPushNotification(to: userToken, title: title, body: message, data: dict)
                            }
                        } else if state == .active {
                            self.collectiveStart.accept(dict)
                        }
                    }
                }
            } else {
                // Course changed
                self.coursesRef.child(course.id).child("lastUpdateTime").observe(.value, with: { snapshot in
                    if let timeString = snapshot.value as? String {
                        if self.lastUpdateTime == nil || (self.lastUpdateTime != nil && self.lastUpdateTime! != timeString) { // Add lastUpdate to avoid to receive multiple notifs when different things are changed in the route
                            self.lastUpdateTime = timeString
                            self.retrieveCourse(courseId: course.id, completion: { firebaseCourse in
                                // Update view
                                self.delegate?.courseChanged(course: firebaseCourse)
                            })
                            let state = UIApplication.shared.applicationState
                            if state == .active, let topController = UIApplication.topViewController() {
                                let title = NSLocalizedString("course_changed", bundle: self.bundle, comment: "")
                                let message = String(format: NSLocalizedString("subscribed_course_changed", bundle: self.bundle, comment: ""), courseName)
                                let alert = Utils.showAlert(with: title, message: message)
                                topController.present(alert, animated: false)
                                // Dismiss alert after 10s
                                Timer.scheduledTimer(withTimeInterval: 10.0, repeats: false, block: { _ in alert.dismiss(animated: true, completion: nil)} )
                            }
                        }
                    }
                })
            }
        })
    }
    
    // Remove observers on a object on Firebase
    func removeObservers(path: String) {
        // Reinit last time update
        self.lastUpdateTime = nil
        self.coursesRef.child(path).removeAllObservers()
    }
    
    // Remove all observers
    func removeAllOberservers() {
        self.lastUpdateTime = nil
        self.coursesRef.removeAllObservers()
    }

    /// Save course on Firebase
    ///
    /// - Parameter course: Course to save
    public func saveCourse(course: Course, update: Bool, successHandler: @escaping () -> Void, errorHandler: @escaping (Error) -> Void) {
        // Create storable dictionary of the course
        let courseDictionary = course.toSimpleDictionary()
        courseDictionary.setValue(Auth.auth().currentUser?.uid, forKey: "owner")
        let formatter = DateFormatter() // Add last update time
        formatter.dateFormat = "yyyy-MM-dd_HH:mm:ss"
        courseDictionary.setValue(formatter.string(from: Date()), forKey: "lastUpdateTime")
        if let raceCourse = course as? RaceCourse {
            if update {
                // Add collective start value to get update 
                courseDictionary.setValue(["timer":NavigationManager.shared.totalTime, "timestamp":0], forKey: "lastCollectiveStart")
                if let courseDict = courseDictionary as? [String : Any] {
                    self.coursesRef.child(course.id).updateChildValues(courseDict, withCompletionBlock: { (error, _) in
                        print("error: \(error)")
                        error != nil ? errorHandler(error!) : successHandler()
                    })
                }
            } else {
                self.coursesRef.child(course.id).setValue(courseDictionary, withCompletionBlock: { (error, _) in
                    print("error: \(error)")
                    error != nil ? errorHandler(error!) : successHandler()
                })
            }
        } else if let cruiseCourse = course as? CruiseCourse {
            if !update {
                // Check if there is already a course with the same name
                self.coursesRef.queryOrdered(byChild: "name").queryEqual(toValue: course.name).observeSingleEvent(of: .value, with: { snapshot in
                    if !snapshot.exists() { // There is no course
                        self.coursesRef.child(cruiseCourse.name).setValue(courseDictionary, withCompletionBlock: { (error, _) in
                            error != nil ? errorHandler(error!) : successHandler()
                        })
                    }
                    if let dict = snapshot.value as? [String: Any], let key = dict.keys.first, let courseDict = dict[key] as? [String: Any] { // There is a course, check if we are the owner
                        if let owner = courseDict["owner"] as? String, owner == Auth.auth().currentUser?.uid {
                            errorHandler(CustomError.firebaseNameAlreadyExistsOwner)
                        } else {
                            errorHandler(CustomError.firebaseNameAlreadyExists)
                        }
                    }
                  }) { error in
                      errorHandler(error)
                  }
            } else {
                self.coursesRef.child(cruiseCourse.name).setValue(courseDictionary, withCompletionBlock: { (error, _) in
                    error != nil ? errorHandler(error!) : successHandler()
                })
            }
        }
    }
    
    // Unshare course
    public func unshareCourse(course: Course, successHandler: @escaping () -> Void, errorHandler: @escaping (Error) -> Void) {
        
        if let raceCourse = course as? RaceCourse {
            course.unshare()
            self.coursesRef.child(course.id).removeValue(completionBlock: { (error, _) in
                error != nil ? errorHandler(error!) : successHandler()
            })
        } else if let cruiseCourse = course as? CruiseCourse {
            self.coursesRef.child(cruiseCourse.name).removeValue(completionBlock: { (error, _) in
                error != nil ? errorHandler(error!) : successHandler()
            })
        }
    }
    
    public func collectiveStartMaster() {
        let course = NavigationManager.shared.route.value
        self.coursesRef.child(course!.id).child("lastCollectiveStart").updateChildValues([ "timer" : "\(NavigationManager.shared.totalTime)" , "timestamp" : "\(Int(Date().timeIntervalSince1970))"])
    }
}

extension Object {
    
    /// Generic method for any object to be stored on Firebase
    ///
    /// - Returns: NSDictionary with all the key/values pairs of the object
    func toDictionary() -> NSMutableDictionary {
        let mutabledic = NSMutableDictionary()
        var properties = self.objectSchema.properties.map { $0.name }
        if let index = properties.firstIndex(where: { $0 == "shared" }) {
            properties.remove(at: index)
        }
        if let index = properties.firstIndex(where: { $0 == "refBuoy" }) {
            properties.remove(at: index)
            let refBuoy = (self["refBuoy"] as? Buoy)?.toDictionary()
            mutabledic["refBuoy"] = refBuoy
        }
        
        if let index = properties.firstIndex(where: { $0 == "gpsPosition" }) {
            properties.remove(at: index)
            let gpsPosition = (self["gpsPosition"] as? GPSPosition)?.toDictionary()
            mutabledic["gpsPosition"] = gpsPosition
        }
        
        let dictionary = self.dictionaryWithValues(forKeys: properties)
        
        mutabledic.setValuesForKeys(dictionary)

        return mutabledic
    }
}

public enum CustomError: Error, Equatable {
    // Throw when a route with the same name already exist on Firebase
    case firebaseNameAlreadyExists

    // Throw when a route with the same name already exist on Firebase and we are the owner
    case firebaseNameAlreadyExistsOwner

    // Throw in all other cases
    case unexpected(code: Int)
}

extension UIApplication {
    public class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}
