//
//  AnnouncementManager.swift
//  SARA Croisiere
//
//  Created by Rose on 30/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import AVFoundation

// As the announcements are different for SARA Nav and SARA Regatta, we use a protocol
@objc public protocol AnnouncementDelegate {
    func announcementPointValidation(nextPoint: AnyPoint?, noCoordMessage: String?) -> String
    func announcementEndRoute(noCoordMessage: String?, noCoord: Bool) -> String
    @objc optional func announcementAxis() -> String
    @objc optional func announcementStartLaunch() -> String
    func profileRouteTitle() -> String
    func deleteMarkUsedRouteMessage() -> String
}

public class AnnouncementManager: MapDataManagerObserver {
    
    // MARK: - Variables
    public static let shared = AnnouncementManager()
    private var state = CurrentAnnouncesState()
    public var announcementDelegate: AnnouncementDelegate?
    
    let disposeBag = DisposeBag()
    var disposables: [Disposable] = []
    var disposablesGPS: [Disposable] = []
    var disposablesRoute: [Disposable] = []
    
    // GPS
    let compassVariable = BehaviorRelay<Int?>(value: nil)
    let courseOverGroundVariable = BehaviorRelay<Int?>(value: nil)
    let speedOverGroundVariable = BehaviorRelay<Float?>(value: nil)
    let maxSpeedOverGroundVariable = BehaviorRelay<Float?>(value: nil)
    // Route
    let azimutVariable = BehaviorRelay<(Int, NavigationUnit?)?>(value: nil) // port
    let gisementVariable = BehaviorRelay<[(Int, NavigationUnit?)?]?>(value: nil)
    let distanceVariable = BehaviorRelay<Float?>(value: nil)
    let CMGVariable = BehaviorRelay<Float?>(value: nil)
    let gapSegmentVariable = BehaviorRelay<Float?>(value: nil)
    let azimutVariable2 = BehaviorRelay<(Int, NavigationUnit?)?>(value: nil) // starboard
    let gisementVariable2 = BehaviorRelay<[(Int, NavigationUnit?)?]?>(value: nil)
    let distanceVariable2 = BehaviorRelay<Float?>(value: nil)
    let CMGVariable2 = BehaviorRelay<Float?>(value: nil)
    let entry3LengthsVariable = BehaviorRelay<Float?>(value: nil)
    // Navigation system
    let depthVariable = BehaviorRelay<Float?>(value: nil)
    let headingMagneticVariable = BehaviorRelay<Int>(value: 0)
    let speedThroughWaterVariable = BehaviorRelay<Float>(value: 0) // STW
    let trueWindSpeedVariable = BehaviorRelay<Float>(value: 0) // TWS
    let trueWindAngleVariable = BehaviorRelay<(Int, NavigationUnit)>(value: (0,NavigationUnit.port)) //TWA
    let windDirectionAngleVariable = BehaviorRelay<Int>(value: 0)
    let apparentWindSpeedVariable = BehaviorRelay<Float>(value: 0) // AWS
    let apparentWindAngleVariable = BehaviorRelay<(Int, NavigationUnit)>(value: (0,NavigationUnit.port)) // AWA
    let waterTemperatureVariable = BehaviorRelay<Float?>(value: nil)
    let airTemperatureVariable = BehaviorRelay<Float>(value: 0)
    //Telltales
    let tellTaleAngleVariable = BehaviorRelay<(String,Int)>(value: ("", 0))
    let tellTaleMedianeVariable = BehaviorRelay<(String,Int)>(value: ("", 0))
    let tellTaleAverageVariable = BehaviorRelay<(String,Int)>(value: ("", 0))
    let tellTaleStandardVariationVariable = BehaviorRelay<(String,Int)>(value: ("", 0))
    let tellTaleVariationMaxVariable = BehaviorRelay<(String,Int)>(value: ("", 0))
    let tellTaleStateVarible = BehaviorRelay<(String,String)>(value: ("", ""))
    
    // Battery level
    let batteryLevel = BehaviorRelay<Float>(value: -1)
    
    // Start message
    var startMessage: [String] = []
    
    var player: AVAudioPlayer?
    
    let bundle = Utils.bundle(anyClass: AnnouncementManager.self)
    
    // MARK: - Functions
    init() {
        NavigationManager.shared.shouldReloadUnits
            .asObservable()
            .subscribe(onNext: {_ in
                self.resetCurrentState()
            })
            .disposed(by: disposeBag)
        initGPS()
        initRoute()
        initCentrale()
        initTelltale()
        initVoice()
        initCarto()
    }
    
    func annonceParameters(annonceType: Announce.AnnonceType, id: String? = nil) -> Announce {
        var annonceState: (Any?, Date?)? = (nil,nil)
        var annonceName: String = ""
        var priority: Int = 0
        switch annonceType {
        // GPS
        case .compass:
            annonceState = self.state.compass
            annonceName = NSLocalizedString("compass", bundle: bundle, comment: "")
            priority = 4
        case .courseOverGround:
            annonceState = self.state.courseOverGround
            annonceName = NmeaManager.default.connected.value ? NSLocalizedString("course_over_ground", bundle: bundle, comment: "") : NSLocalizedString("course", bundle: bundle, comment: "")
            priority = 6
        case .speedOverGround:
            annonceState = self.state.speedOverGround
            annonceName = NmeaManager.default.connected.value ? NSLocalizedString("speed_over_ground", bundle: bundle, comment: "") : NSLocalizedString("speed", bundle: bundle, comment: "")
            priority = 6
        case .maxSpeedOverGround:
            annonceState = self.state.maxSpeedOverGround
            annonceName = NmeaManager.default.connected.value ? NSLocalizedString("max_speed_over_ground", bundle: bundle, comment: "") : NSLocalizedString("max_speed", bundle: bundle, comment: "")
            priority = 6
        // Course
        case .azimut1:
            // if the course is reversed and the mark is a double mark, inverse value 1 and 2
            if let course = NavigationManager.shared.route.value as? CruiseCourse, course.reversed, let mark = NavigationManager.shared.point.value?.point as? Mark, mark.buoys.count > 1, !NavigationManager.shared.usingMiddlePoint {
                annonceState = self.state.bearing2
            } else {
                annonceState = self.state.bearing
            }
            annonceName = NSLocalizedString("azimuth", bundle: bundle, comment: "")
            priority = 3
        case .azimut2:
            // if the course is reversed and the mark is a double mark, inverse value 1 and 2
            if let course = NavigationManager.shared.route.value as? CruiseCourse, course.reversed, let mark = NavigationManager.shared.point.value as? Mark, mark.buoys.count > 1, !NavigationManager.shared.usingMiddlePoint {
                annonceState = self.state.bearing
            } else {
                annonceState = self.state.bearing2
            }
            annonceName = NSLocalizedString("azimuth", bundle: bundle, comment: "")
            priority = 3
        case .gisement1:
            // if the course is reversed and the mark is a double mark and not using middle point, inverse value 1 and 2
            if let course = NavigationManager.shared.route.value as? CruiseCourse, course.reversed, let mark = NavigationManager.shared.point.value?.point as? Mark, mark.buoys.count > 1, !NavigationManager.shared.usingMiddlePoint {
                annonceState = self.state.gisement2
            } else {
                annonceState = self.state.gisement
            }
            annonceName = NSLocalizedString("relative_bearing", bundle: bundle, comment: "")
            priority = 3
        case .gisement2:
            // if the course is reversed and the mark is a double mark, inverse value 1 and 2
            if let course = NavigationManager.shared.route.value as? CruiseCourse, course.reversed, let mark = NavigationManager.shared.point.value?.point as? Mark, mark.buoys.count > 1, !NavigationManager.shared.usingMiddlePoint {
                annonceState = self.state.gisement
            } else {
                annonceState = self.state.gisement2
            }
            annonceName = NSLocalizedString("relative_bearing", bundle: bundle, comment: "")
            priority = 3
        case .gisementTime1:
            // if the course is reversed and the mark is a double mark and not using middle point, inverse value 1 and 2
            if let course = NavigationManager.shared.route.value as? CruiseCourse, course.reversed, let mark = NavigationManager.shared.point.value?.point as? Mark, mark.buoys.count > 1, !NavigationManager.shared.usingMiddlePoint {
                annonceState = self.state.gisementTime2
            } else {
                annonceState = self.state.gisementTime
            }
            annonceName = NSLocalizedString("relative_bearing", bundle: bundle, comment: "")
            priority = 3
        case .gisementTime2:
            // if the course is reversed and the mark is a double mark, inverse value 1 and 2
            if let course = NavigationManager.shared.route.value as? CruiseCourse, course.reversed, let mark = NavigationManager.shared.point.value?.point as? Mark, mark.buoys.count > 1, !NavigationManager.shared.usingMiddlePoint {
                annonceState = self.state.gisementTime
            } else {
                annonceState = self.state.gisementTime2
            }
            annonceName = NSLocalizedString("relative_bearing", bundle: bundle, comment: "")
            priority = 3
        case .distance1:
            // if the course is reversed and the mark is a double mark, inverse value 1 and 2
            if let course = NavigationManager.shared.route.value as? CruiseCourse, course.reversed, let mark = NavigationManager.shared.point.value?.point as? Mark, mark.buoys.count > 1, !NavigationManager.shared.usingMiddlePoint {
                annonceState = self.state.distance2
            } else {
                annonceState = self.state.distance
            }
            annonceName = NSLocalizedString("distance", bundle: bundle, comment: "")
            priority = 3
        case .distance2:
            // if the course is reversed and the mark is a double mark, inverse value 1 and 2
            if let course = NavigationManager.shared.route.value as? CruiseCourse, course.reversed, let mark = NavigationManager.shared.point.value?.point as? Mark, mark.buoys.count > 1, !NavigationManager.shared.usingMiddlePoint {
                annonceState = self.state.distance
            } else {
                annonceState = self.state.distance2
            }
            annonceName = NSLocalizedString("distance", bundle: bundle, comment: "")
            priority = 3
        case .CMG1:
            // if the course is reversed and the mark is a double mark, inverse value 1 and 2
            if let course = NavigationManager.shared.route.value as? CruiseCourse, course.reversed, let mark = NavigationManager.shared.point.value?.point as? Mark, mark.buoys.count > 1, !NavigationManager.shared.usingMiddlePoint {
                annonceState = self.state.CMG2
            } else {
                annonceState = self.state.CMG
            }
            annonceName = NSLocalizedString("CMG", bundle: bundle, comment: "")
            priority = 5
        case .CMG2:
            // if the course is reversed and the mark is a double mark, inverse value 1 and 2
            if let course = NavigationManager.shared.route.value as? CruiseCourse, course.reversed, let mark = NavigationManager.shared.point.value?.point as? Mark, mark.buoys.count > 1, !NavigationManager.shared.usingMiddlePoint {
                annonceState = self.state.CMG
            } else {
                annonceState = self.state.CMG2
            }
            annonceName = NSLocalizedString("CMG", bundle: bundle, comment: "")
            priority = 5
        case .gapSegment:
            annonceState = self.state.gapToSegment
            annonceName = NSLocalizedString("gap_route", bundle: bundle, comment: "")
            priority = 3
        case .entry3Lengths:
            annonceState = self.state.entry3Lengths
            if let route = NavigationManager.shared.route.value, route.isMatchRace() { // For match race, 2 hull lengths
                annonceName = self.state.entry3Lengths.0 ? NSLocalizedString("entry_2_lengths", bundle: bundle, comment: "") : NSLocalizedString("exit_2_lengths", bundle: bundle, comment: "")
            } else {
                annonceName = self.state.entry3Lengths.0 ? NSLocalizedString("entry_3_lengths", bundle: bundle, comment: "") : NSLocalizedString("exit_3_lengths", bundle: bundle, comment: "")
            }
            priority = 2
        // Navigation centrale
        case .depth:
            annonceState = self.state.depth
            annonceName = NSLocalizedString("depth", bundle: bundle, comment: "")
            priority = 3
        case .headingMagnetic:
            annonceState = self.state.headingMagnetic
            annonceName = NSLocalizedString("surface_heading", bundle: bundle, comment: "")
            priority = 5
        case .speedThroughWater:
            annonceState = self.state.speedThroughWater
            annonceName = NSLocalizedString("speed_through_water", bundle: bundle, comment: "")
            priority = 5
        case .trueWindSpeed:
            annonceState = self.state.trueWindSpeed
            annonceName = NSLocalizedString("true_wind_speed", bundle: bundle, comment: "")
            priority = 5
        case .trueWindAngle:
            annonceState = self.state.trueWindAngle
            annonceName = NSLocalizedString("true_wind_angle", bundle: bundle, comment: "")
            priority = 5
        case .apparentWindSpeed:
            annonceState = self.state.apparentWindSpeed
            annonceName = NSLocalizedString("apparent_wind_speed", bundle: bundle, comment: "")
            priority = 5
        case .apparentWindAngle:
            annonceState = self.state.apparentWindAngle
            annonceName = NSLocalizedString("apparent_wind_angle", bundle: bundle, comment: "")
            priority = 5
        case .windDirection:
            annonceState = self.state.windDirection
            annonceName = NSLocalizedString("wind_direction", bundle: bundle, comment: "")
            priority = 5
        case .waterTemperature:
            annonceState = self.state.waterTemperature
            annonceName = NSLocalizedString("water_temperature", bundle: bundle, comment: "")
            priority = 5
        case .airTemperature:
            annonceState = self.state.airTemperature
            annonceName = NSLocalizedString("air_temperature", bundle: bundle, comment: "")
            priority = 5
        // Telltale
        case .tellTaleAngle:
            if let _id = id, let value = self.state.tellTaleAngle[_id] {
                annonceState = value
            } else {
                annonceState = nil
            }
            annonceName = NSLocalizedString("angle", bundle: self.bundle, comment: "")
            priority = 5
        case .tellTaleMediane:
            if let _id = id, let value = self.state.tellTaleMediane[_id] {
                annonceState = value
            } else {
                annonceState = nil
            }
            annonceName = NSLocalizedString("mediane", bundle: self.bundle, comment: "")
            priority = 5
        case .tellTaleAverage:
            if let _id = id, let value = self.state.tellTaleAverage[_id] {
                annonceState = value
            } else {
                annonceState = nil
            }
            annonceName = NSLocalizedString("average", bundle: self.bundle, comment: "")
            priority = 5
        case .tellTaleStandardVariation:
            if let _id = id, let value = self.state.tellTaleStandardDeviation[_id] {
                annonceState = value
            } else {
                annonceState = nil
            }
            annonceName = NSLocalizedString("standard_deviation", bundle: self.bundle, comment: "")
            priority = 5
        case .tellTaleVariationMax:
            if let _id = id, let value = self.state.tellTaleVariationMax[_id] {
                annonceState = value
            } else {
                annonceState = nil
            }
            annonceName = NSLocalizedString("variation_max", bundle: self.bundle, comment: "")
            priority = 5
        // Carto
        case .distanceBeacon:
            if let _id = id, let value = self.state.distanceBeacon[_id] {
                annonceState = value
            } else {
                annonceState = nil
            }
            priority = 3
            break
        case .azimutBeacon:
            if let _id = id, let value = self.state.bearingBeacons[_id] {
                annonceState = value
            } else {
                annonceState = nil
            }
            priority = 3
            break
        case .gisementBeacon:
            if let _id = id, let value = self.state.gisementBeacon[_id] {
                annonceState = value
            } else {
                annonceState = nil
            }
            priority = 3
            break
        case .gisementTimeBeacon:
            if let _id = id, let value = self.state.gisementTimeBeacon[_id] {
                annonceState = value
            } else {
                annonceState = nil
            }
            priority = 3
            break
        default:
            break
        }
        return Announce(text: annonceName, priority: priority, type: annonceType, annonceState: annonceState)
    }
    
    func annonceDistancePercentType(newValue: Float?, annonceType: Announce.AnnonceType, timeThreshold: Int, id: String? = nil) -> Bool {
        var annonce = annonceParameters(annonceType: annonceType, id: id)
        if let annonceState = annonce.annonceState {
            if (newValue == nil && annonceState.0 != nil) || (newValue != nil && annonceState.0 == nil) || (newValue != nil) && (annonceState.0 != nil) && ((newValue! <= (annonceState.0 as! (Float, Float)).0) || (newValue! >= (annonceState.0 as! (Float, Float)).1)) {
                if annonceState.1 == nil || (Int(Date().timeIntervalSince(annonceState.1!))) > timeThreshold {
                    // Add announce except for announce for beacons
                    if let _ = NavigationManager.shared.point.value?.point.name, annonceType != .distanceBeacon {
                        AnnouncementQueueManager.shared.addAnnounce(annonce)
                    }
                    return true
                }
            }
            return false
        } else {
            return true
        }
        return true
    }
    
    func annonceDistanceType(newValue: Float?, annonceType: Announce.AnnonceType, timeThreshold: Int, id: String? = nil) -> Bool {
        var annonce = annonceParameters(annonceType: annonceType, id: id)
        if let annonceState = annonce.annonceState {
            if (newValue == nil && annonceState.0 != nil) || (newValue != nil && annonceState.0 == nil) || (newValue != nil) && (annonceState.0 != nil) && Int(abs(newValue! - ((annonceState.0!) as! Float))) > Profile.autoDistanceThreshold(for: Int(annonceState.0! as! Float)) {
                if annonceState.1 == nil || (Int(Date().timeIntervalSince(annonceState.1!))) > timeThreshold {
                    // Add announce except for announce for beacons
                    if let _ = NavigationManager.shared.point.value?.point.name, annonceType != .distanceBeacon {
                        AnnouncementQueueManager.shared.addAnnounce(annonce)
                    }
                    return true
                }
            }
            return false
        } else {
            return true
        }
    }
    
    // Take into account the fact that the angle changed to 350° to 2°
    func diffAngle(val1: Int, val2: Int) -> Int{
        let method1 = 360 - abs(val1 - val2)
        let method2 = abs(val2 - val1)
        return method1 > method2 ? method2 : method1
    }
    
    func annonceAngleType(newValue: Int?, annonceType: Announce.AnnonceType, valueThreshold: Int, timeThreshold: Int) -> Bool {
        var annonce = annonceParameters(annonceType: annonceType)
        let annonceState = annonce.annonceState!
        if (newValue == nil && annonceState.0 != nil) || (newValue != nil && annonceState.0 == nil) || (newValue != nil) && (diffAngle(val1: newValue!, val2: (annonceState.0) as! Int? ?? Int.max) > valueThreshold) {
            if annonceState.1 == nil || (Int(Date().timeIntervalSince(annonceState.1!))) > timeThreshold {
                AnnouncementQueueManager.shared.addAnnounce(annonce)
                return true
            }
        }
        return false
    }
    
    func annonceSpeedType(newValue: Float?, annonceType: Announce.AnnonceType, valueThreshold: Float, timeThreshold: Int, unit: String = Settings.shared.speedUnit?.unit.description ?? NavigationUnit.knots.description) -> Bool {
        var annonce = annonceParameters(annonceType: annonceType)
        let annonceState = annonce.annonceState!
        if (newValue == nil && annonceState.0 != nil) || (newValue != nil && annonceState.0 == nil) || (newValue != nil) && (abs(Float(newValue!) - ((annonceState.0) as! Float? ?? Float.greatestFiniteMagnitude)).formatted > valueThreshold) {
            if annonceState.1 == nil || (Int(Date().timeIntervalSince(annonceState.1!))) > timeThreshold {
                AnnouncementQueueManager.shared.addAnnounce(annonce)
                return true
            }
        }
        return false
    }
    
    func diffAngleWithUnit(val1: (Int, NavigationUnit?), val2: (Int?, NavigationUnit?)) -> Int{
        if val1.1 == NavigationUnit.degrees {
            return diffAngle(val1: val1.0, val2: (val2.0 ?? Int.max))
        } else {
            var val1Degree = val1.0
            if val1.1 == NavigationUnit.port {
                val1Degree = 360 - val1.0
            }
            var val2Degree = val2.0
            if val2.0 != nil && val2.1 == NavigationUnit.port {
                val2Degree = 360 - val2.0!
            }
            return diffAngle(val1: val1Degree, val2: (val2Degree ?? Int.max))
        }
    }
        
    func annonceAngleWithUnit(newValue: (Int, NavigationUnit?)?, annonceType: Announce.AnnonceType, valueThreshold: Int, timeThreshold: Int, id: String? = nil) -> Bool {
        var annonce = annonceParameters(annonceType: annonceType, id: id)
        if let annonceState = annonce.annonceState {
            let annonceValue = annonceState.0 as! (Int?, NavigationUnit?)
            // If the value has changed enough or the bearint type has changed
            if (newValue == nil && annonceValue.0 != nil) || (newValue != nil && annonceValue.0 == nil) || ((newValue != nil) && (diffAngleWithUnit(val1: newValue!, val2: annonceValue) > valueThreshold)) {
                if annonceState.1 == nil || (Int(Date().timeIntervalSince(annonceState.1!))) > timeThreshold {
                    if let _ = NavigationManager.shared.point.value?.point.name, id == nil { // if id != nil, for carto announcement
                        AnnouncementQueueManager.shared.addAnnounce(annonce)
                    }
                    if annonceType == .trueWindAngle || annonceType == .apparentWindAngle {
                        AnnouncementQueueManager.shared.addAnnounce(annonce)
                    }
                    return true
                }
            }
            return false
        } else {
            return true
        }
    }
    
    func diffAngleTime(val1: Int, val2: Int) -> Int{
        // Take into account the fact that the angle changed to 12h to 1h
        let method1 = 12 - abs(val1 - val2)
        let method2 = abs(val2 - val1)
        return method1 > method2 ? method2 : method1
    }
    
    func annonceAngleTime(newValue: Int?, annonceType: Announce.AnnonceType, valueThreshold: Int, timeThreshold: Int, id: String? = nil) -> Bool {
        var annonce = annonceParameters(annonceType: annonceType, id: id)
        if let annonceState = annonce.annonceState {
            let annonceValue = annonceState.0 as! Int?
            // If the value has changed enough or the bearint type has changed
            if (newValue == nil && annonceValue != nil) || (newValue != nil && annonceValue == nil) || ((newValue != nil) && (diffAngleTime(val1: newValue!, val2: annonceValue!) > valueThreshold)) {
                if annonceState.1 == nil || (Int(Date().timeIntervalSince(annonceState.1!))) > timeThreshold {
                    if let _ = NavigationManager.shared.point.value?.point.name, id == nil { // if id != nil, for carto announcement
                        AnnouncementQueueManager.shared.addAnnounce(annonce)
                    }
                    return true
                }
            }
            return false
        } else {
            return true
        }
    }
    
    func checkUnitSpeed(value: Float) -> Float {
        return Settings.shared.speedUnit?.unit == NavigationUnit.kmPerHour ? value.knotsToKmh() : value
    }
    
    func annonceTelltale(newValue: (String, Int), annonceType: Announce.AnnonceType, valueThreshold: Int, timeThreshold: Int) -> Bool {
        var annonce = annonceParameters(annonceType: annonceType, id: newValue.0)
        let annonceState = annonce.annonceState
        if annonceState != nil {
            let annonceValue = annonceState!.0 as! Int
            if abs(newValue.1 - annonceValue) > valueThreshold {
                if annonceState!.1 == nil || (Int(Date().timeIntervalSince(annonceState!.1!))) > timeThreshold {
                    if let i = TellTalesManager.default.tellTales.firstIndex(where: {$0.id == newValue.0}) {
                        AnnouncementQueueManager.shared.addAnnounce(Announce(text: "\(TellTalesManager.default.tellTales[i].name) \(annonce.text)", priority: annonce.priority, type: annonceType, annonceState: (newValue.0, nil)))
                        return true
                    }
                }
            }
        } else {
            if let i = TellTalesManager.default.tellTales.firstIndex(where: {$0.id == newValue.0}) {
                AnnouncementQueueManager.shared.addAnnounce(Announce(text: "\(TellTalesManager.default.tellTales[i].name) \(annonce.text)", priority: annonce.priority, type: annonceType, annonceState: (newValue.0, nil)))
                return true
            }
        }
        return false
    }
    
    //MARK: - GPS
    func initGPS() {
        guard let profile = User.shared.profile.profileTypes.first(where: {$0 is ProfileGPS}) else {
            return
        }
        let profileGPS = profile as! ProfileGPS
        // Compass
        if profileGPS.compass != nil {
            let disposable = self.compassVariable
                .asObservable()
                .bind { (compass) in
                    // No announcement if start procedure is launched, compass profile not activated, and threshold value and time not good
                    if !NavigationManager.shared.isStartPoint() && profileGPS.compass!.value.isActivated && self.annonceAngleType(newValue: compass, annonceType: .compass, valueThreshold: Int(profileGPS.compass!.value.threshold), timeThreshold: profileGPS.compass!.value.timeThreshold){
                        self.state.compass = (compass, Date())
                    }
                }
//            self.disposables.append(disposable)
            self.disposablesGPS.append(disposable)
            disposable.disposed(by: disposeBag)
        }
        // Course over ground
        if profileGPS.courseOverGround != nil {
            let disposable = self.courseOverGroundVariable
                .asObservable()
                .bind { (course) in
                    if !NavigationManager.shared.isStartPoint() && profileGPS.courseOverGround!.value.isActivated && self.annonceAngleType(newValue: course, annonceType: .courseOverGround, valueThreshold: Int(profileGPS.courseOverGround!.value.threshold), timeThreshold: profileGPS.courseOverGround!.value.timeThreshold){
                        self.state.courseOverGround = (course, Date())
                    }
                }
            self.disposables.append(disposable)
            self.disposablesGPS.append(disposable)
            disposable.disposed(by: disposeBag)
        }
        // Speed over ground
        if profileGPS.speedOverGround != nil {
            let disposable = self.speedOverGroundVariable
                .asObservable()
                .bind { (speed) in
                    let valueThreshold = self.checkUnitSpeed(value: profileGPS.speedOverGround!.value.threshold)
                    if !NavigationManager.shared.isStartPoint() && profileGPS.speedOverGround!.value.isActivated && self.annonceSpeedType(newValue: speed, annonceType: .speedOverGround, valueThreshold: valueThreshold, timeThreshold: profileGPS.speedOverGround!.value.timeThreshold) {
                        self.state.speedOverGround = (speed, Date())
                    }
                }
//            self.disposables.append(disposable)
            self.disposablesGPS.append(disposable)
            disposable.disposed(by: disposeBag)
        }
        // Max speed over ground
        if profileGPS.maxSpeedOverGround != nil {
            let disposable = self.maxSpeedOverGroundVariable
                .asObservable()
                .bind { (maxSpeed) in
                    let valueThreshold = self.checkUnitSpeed(value: profileGPS.maxSpeedOverGround!.value.threshold)
                    if !NavigationManager.shared.isStartPoint() && profileGPS.maxSpeedOverGround!.value.isActivated && self.annonceSpeedType(newValue: maxSpeed, annonceType: .maxSpeedOverGround, valueThreshold: valueThreshold, timeThreshold: profileGPS.maxSpeedOverGround!.value.timeThreshold) {
                        self.state.maxSpeedOverGround = (maxSpeed, Date())
                    }
                }
//            self.disposables.append(disposable)
            self.disposablesGPS.append(disposable)
            disposable.disposed(by: disposeBag)
        }
    }
    
    //MARK: - Course
    func initRoute() {
        guard let profileRoute = User.shared.profile.profileTypes.first(where: {$0 is ProfileRoute}) as? ProfileRoute else {
            return
        }
        // Azimut in degree
        if let profileAzimut = profileRoute.azimut {
            let disposable = self.azimutVariable
                .asObservable()
                .bind(onNext: { [weak self] (azimut) in
                    guard let weakSelf = self else { return }
                    if !NavigationManager.shared.isStartPoint() && profileAzimut.value.isActivated && weakSelf.annonceAngleWithUnit(newValue: azimut, annonceType: .azimut1, valueThreshold: Int(profileAzimut.value.threshold), timeThreshold: profileAzimut.value.timeThreshold) {
                        weakSelf.state.bearing = ((azimut?.0, azimut?.1), Date())
                    }
                    
                })
            self.disposablesRoute.append(disposable)
//            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
            let disposable2 = self.azimutVariable2
                .asObservable()
                .bind(onNext: { [weak self] (azimut) in
                    guard let weakSelf = self else { return }
                    if !NavigationManager.shared.isStartPoint() && profileAzimut.value.isActivated && weakSelf.annonceAngleWithUnit(newValue: azimut, annonceType: .azimut2, valueThreshold: Int(profileAzimut.value.threshold), timeThreshold: profileAzimut.value.timeThreshold) {
                        weakSelf.state.bearing2 = ((azimut?.0, azimut?.1), Date())
                    }
                    
                })
            self.disposablesRoute.append(disposable2)
//            self.disposables.append(disposable2)
            disposable2.disposed(by: disposeBag)
        }
        // Gisement
        if let profileGisement = profileRoute.gisement, let profileGisementTime = profileRoute.gisementTime {
            let disposable = self.gisementVariable
                .asObservable()
                .bind(onNext: { [weak self] (gisement) in
                    guard let weakSelf = self else { return }
                    let gisementSP = gisement?[0]
                    let gisementTime = gisement?[1]
                    if !NavigationManager.shared.isStartPoint() && profileGisement.value.isActivated && profileGisementTime.value.isActivated && ( weakSelf.annonceAngleWithUnit(newValue: gisementSP, annonceType: .gisement1, valueThreshold: Int(profileGisement.value.threshold), timeThreshold: profileGisement.value.timeThreshold, id: "") || weakSelf.annonceAngleTime(newValue: gisementTime?.0, annonceType: .gisementTime1, valueThreshold: Int(profileGisementTime.value.threshold), timeThreshold: profileGisementTime.value.timeThreshold, id: "")) {
                        AnnouncementQueueManager.shared.addAnnounce(Announce(text: NSLocalizedString("relative_bearing", bundle: weakSelf.bundle, comment: ""), priority: 3, type: .gisementCombine1, annonceState: nil))
                        weakSelf.state.gisement = ((gisementSP?.0, gisementSP?.1), Date())
                        weakSelf.state.gisementTime = (gisementTime?.0, Date())
                    } else if !NavigationManager.shared.isStartPoint() && profileGisement.value.isActivated && !profileGisementTime.value.isActivated && weakSelf.annonceAngleWithUnit(newValue: gisementSP, annonceType: .gisement1, valueThreshold: Int(profileGisement.value.threshold), timeThreshold: profileGisement.value.timeThreshold) {
                        weakSelf.state.gisement = ((gisementSP?.0, gisementSP?.1), Date())
                    } else if !NavigationManager.shared.isStartPoint() && !profileGisement.value.isActivated && profileGisementTime.value.isActivated && weakSelf.annonceAngleTime(newValue: gisementTime?.0, annonceType: .gisementTime1, valueThreshold: Int(profileGisementTime.value.threshold), timeThreshold: profileGisementTime.value.timeThreshold) {
                        weakSelf.state.gisementTime = (gisementTime?.0, Date())
                    }
                })
            self.disposablesRoute.append(disposable)
//            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
            let disposable2 = self.gisementVariable2
                .asObservable()
                .bind(onNext: { [weak self] (gisement) in
                    guard let weakSelf = self else { return }
                    let gisementSP = gisement?[0]
                    let gisementTime = gisement?[1]
                    if !NavigationManager.shared.isStartPoint() && profileGisement.value.isActivated && profileGisementTime.value.isActivated && ( weakSelf.annonceAngleWithUnit(newValue: gisementSP, annonceType: .gisement2, valueThreshold: Int(profileGisement.value.threshold), timeThreshold: profileGisement.value.timeThreshold) || weakSelf.annonceAngleTime(newValue: gisementTime?.0, annonceType: .gisementTime2, valueThreshold: Int(profileGisementTime.value.threshold), timeThreshold: profileGisementTime.value.timeThreshold)) {
                      AnnouncementQueueManager.shared.addAnnounce(Announce(text: NSLocalizedString("relative_bearing", bundle: weakSelf.bundle, comment: ""), priority: 3, type: .gisementCombine2, annonceState: nil))
                        weakSelf.state.gisement2 = ((gisementSP?.0, gisementSP?.1), Date())
                        weakSelf.state.gisementTime2 = (gisementTime?.0, Date())
                    } else if !NavigationManager.shared.isStartPoint() && profileGisement.value.isActivated && !profileGisementTime.value.isActivated && weakSelf.annonceAngleWithUnit(newValue: gisementSP, annonceType: .gisement2, valueThreshold: Int(profileGisement.value.threshold), timeThreshold: profileGisement.value.timeThreshold) {
                        weakSelf.state.gisement2 = ((gisementSP?.0, gisementSP?.1), Date())
                    } else if !NavigationManager.shared.isStartPoint() && !profileGisement.value.isActivated && profileGisementTime.value.isActivated && weakSelf.annonceAngleTime(newValue: gisementTime?.0, annonceType: .gisementTime2, valueThreshold: Int(profileGisementTime.value.threshold), timeThreshold: profileGisementTime.value.timeThreshold) {
                        weakSelf.state.gisementTime2 = (gisementTime?.0, Date())
                    }
                })
            self.disposablesRoute.append(disposable2)
//            self.disposables.append(disposable2)
            disposable2.disposed(by: disposeBag)
        }
        // Distance
        if let profileDistance = profileRoute.distance {
            let disposable = self.distanceVariable
                .asObservable()
                .bind(onNext: { [weak self] (distance) in
                    guard let weakSelf = self else { return }
                    if !NavigationManager.shared.isStartPoint() && profileDistance.value.isActivated && weakSelf.annonceDistancePercentType(newValue: distance, annonceType: .distance1, timeThreshold: profileDistance.value.timeThreshold){
                        if distance == nil {
                            weakSelf.state.distance = (nil, Date())
                        } else {
                            // Take into account if we go closer or move away to the point
                            let newDistanceThrDown = distance! * (100 - profileDistance.value.threshold) / 100
                            let newDistanceThrUp = distance! * (100 + profileDistance.value.threshold) / 100
                            weakSelf.state.distance = ((newDistanceThrDown, newDistanceThrUp), Date())
                        }
                    }
//                    if !NavigationManager.shared.isStartPoint() && profileDistance.value.isActivated && weakSelf.annonceDistanceType(newValue: distance, annonceType: .distance1, timeThreshold: profileDistance.value.timeThreshold){
//                        weakSelf.state.distance = (distance, Date())
//                    }
                })
            self.disposablesRoute.append(disposable)
//            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
            let disposable2 = self.distanceVariable2
                .asObservable()
                .bind(onNext: { [weak self] (distance) in
                    guard let weakSelf = self else { return }
                    if !NavigationManager.shared.isStartPoint() && profileDistance.value.isActivated && weakSelf.annonceDistancePercentType(newValue: distance, annonceType: .distance2, timeThreshold: profileDistance.value.timeThreshold){
                        if distance == nil {
                            weakSelf.state.distance2 = (nil, Date())
                        } else {
                            // Take into account if we go closer or move away to the point
                            let newDistanceThrDown = distance! * (100 - profileDistance.value.threshold) / 100
                            let newDistanceThrUp = distance! * (100 + profileDistance.value.threshold) / 100
                            weakSelf.state.distance2 = ((newDistanceThrDown, newDistanceThrUp), Date())
                        }
                    }
//                    if !NavigationManager.shared.isStartPoint() && profileDistance.value.isActivated && weakSelf.annonceDistanceType(newValue: distance, annonceType: .distance2, timeThreshold: profileDistance.value.timeThreshold){
//                        weakSelf.state.distance2 = (distance, Date())
//                    }
                })
            self.disposablesRoute.append(disposable2)
//            self.disposables.append(disposable2)
            disposable2.disposed(by: disposeBag)
        }
        // CMG
        if let profileCMG = profileRoute.cmg {
            let valueThreshold = self.checkUnitSpeed(value: profileCMG.value.threshold)
            let disposable = self.CMGVariable
                .asObservable()
                .bind(onNext: { [weak self] (cmg) in
                    guard let weakSelf = self else { return }
                    if !NavigationManager.shared.isStartPoint() && profileRoute.cmg!.value.isActivated && weakSelf.annonceSpeedType(newValue: cmg, annonceType: .CMG1, valueThreshold: valueThreshold, timeThreshold: profileCMG.value.timeThreshold) {
                        weakSelf.state.CMG = (cmg, Date())
                    }
                })
            self.disposablesRoute.append(disposable)
//            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
            let disposable2 = self.CMGVariable2
                .asObservable()
                .bind(onNext: { [weak self] (cmg) in
                    guard let weakSelf = self else { return }
                    if !NavigationManager.shared.isStartPoint() && profileRoute.cmg!.value.isActivated && weakSelf.annonceSpeedType(newValue: cmg, annonceType: .CMG2, valueThreshold: valueThreshold, timeThreshold: profileCMG.value.timeThreshold) {
                        weakSelf.state.CMG2 = (cmg, Date())
                    }
                })
            self.disposablesRoute.append(disposable2)
//            self.disposables.append(disposable2)
            disposable2.disposed(by: disposeBag)
        }
        // Distance to segment
        if let profileDistanceToSegment = profileRoute.distanceToSegment {
            let disposable = self.gapSegmentVariable
                .asObservable()
                .bind(onNext: { [weak self] (distance) in
                    guard let weakSelf = self else { return }
                    //We don't use a static value for the distance threshold
                    //But adjust it depending on the current distance to the point
                    if !NavigationManager.shared.isStartPoint() && profileDistanceToSegment.value.isActivated && weakSelf.annonceDistanceType(newValue: distance, annonceType: .gapSegment, timeThreshold: profileDistanceToSegment.value.timeThreshold){
                        weakSelf.state.gapToSegment = (distance, Date())
                    }
                })
            self.disposablesRoute.append(disposable)
//            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
        }
        // Entry 3 lengths
        if let profile3Length = profileRoute.entry3Lengths {
            let disposable = self.entry3LengthsVariable
                .asObservable()
                .bind(onNext: { distance in
                    let isActivated = profile3Length.value.isActivated
                    var sizeZone: Float = 3
                    if let route = NavigationManager.shared.route.value, route.isMatchRace() { // For match race, the room is 2 lengths
                        sizeZone = 2
                    }
                    let threshold = profile3Length.value.threshold * sizeZone
                    if isActivated && distance != nil {
                        if distance! <= threshold && !self.state.entry3Lengths.0 { // Announcement entry
                            print("===== Entrée")
                            self.state.entry3Lengths = (true, Date())
                            let announcement = self.annonceParameters(annonceType: .entry3Lengths)
                            AnnouncementQueueManager.shared.addAnnounce(announcement)
                        } else if distance! > threshold && self.state.entry3Lengths.0 { // Announcement exit
                            print("===== Sortie")
                            self.state.entry3Lengths = (false, Date())
                            let announcement = self.annonceParameters(annonceType: .entry3Lengths)
                            AnnouncementQueueManager.shared.addAnnounce(announcement)
                        }
                    }
                })
            self.disposablesRoute.append(disposable)
            disposable.disposed(by: disposeBag)
        }
    }
    
    //MARK: - Navigation Center
    func initCentrale() {
        
        guard let profile = User.shared.profile.profileTypes.first(where: {$0 is ProfileCentrale}) else {
            return
        }
        let profileCentrale = profile as! ProfileCentrale
        // Depth
        if let profileDepth = profileCentrale.depth {
            let disposable = self.depthVariable
                .asObservable()
                .bind{ (depth) in  // We don't use a static value for the depth threshold
                    if NmeaManager.default.connected.value && profileDepth.value.isActivated {
                        var annonce = self.annonceParameters(annonceType: .depth)
                        let annonceState = annonce.annonceState! as! (Float?, Date?)
                        if ((depth == nil && annonceState.0 != nil) || (depth != nil) && (annonceState.0 == nil) || (depth != nil) && (annonceState.0 != nil) && Int(abs(depth! - (annonceState.0!))) > Profile.autoDepthThreshold(for: annonceState.0!)) {
                            if annonceState.1 == nil || (Int(Date().timeIntervalSince(annonceState.1!))) > profileDepth.value.timeThreshold {
                                AnnouncementQueueManager.shared.addAnnounce(annonce)
                                self.state.depth = (depth, Date())
                            }
                        }
                    }
                }
            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
        }
        // Surface heading
        if let profileHeading = profileCentrale.headingMagnetic {
            let disposable = self.headingMagneticVariable
                .asObservable()
                .bind { (headingMagnetic) in
                    if NmeaManager.default.connected.value && profileHeading.value.isActivated && self.annonceAngleType(newValue: headingMagnetic, annonceType: .headingMagnetic, valueThreshold: Int(profileHeading.value.threshold), timeThreshold: profileHeading.value.timeThreshold) {
                        self.state.headingMagnetic = (headingMagnetic, Date())
                    }
                }
            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
        }
        // Speed from the central
        if let profileSTW = profileCentrale.speedThroughWater {
            let disposable = self.speedThroughWaterVariable
                .asObservable()
                .bind { (stw) in
                    let valueThreshold = self.checkUnitSpeed(value: profileSTW.value.threshold)
                    if NmeaManager.default.connected.value && profileCentrale.speedThroughWater!.value.isActivated && self.annonceSpeedType(newValue: stw, annonceType: .speedThroughWater, valueThreshold: valueThreshold, timeThreshold: profileSTW.value.timeThreshold){
                        self.state.speedThroughWater = (stw, Date())
                    }
                }
            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
        }
        // True wind speed
        if let profileWindSpeed = profileCentrale.trueWindSpeed {
            let disposable = self.trueWindSpeedVariable
                .asObservable()
                .bind { (speedTrueWind) in
                    let valueThreshold = self.checkUnitSpeed(value: profileWindSpeed.value.threshold)
                    if NmeaManager.default.connected.value && profileCentrale.trueWindSpeed!.value.isActivated && self.annonceSpeedType(newValue: speedTrueWind, annonceType: .trueWindSpeed, valueThreshold: valueThreshold, timeThreshold: profileWindSpeed.value.timeThreshold) {
                        self.state.trueWindSpeed = (speedTrueWind, Date())
                    }
                }
            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
        }
        
        if let profileTrueWind = profileCentrale.trueWindAngle {
            let disposable = self.trueWindAngleVariable
                .asObservable()
                .bind { (windTrueAngle) in
                    if profileTrueWind.value.isActivated && NmeaManager.default.connected.value && self.annonceAngleWithUnit(newValue: windTrueAngle, annonceType: .trueWindAngle, valueThreshold: Int(profileTrueWind.value.threshold), timeThreshold: profileTrueWind.value.timeThreshold) {
                        self.state.trueWindAngle = (windTrueAngle, Date())
                    }
                }
            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
            
        }
        // Wind relative speed
        if let profileWindSpeed = profileCentrale.apparentWindSpeed {
            let disposable = self.apparentWindSpeedVariable
                .asObservable()
                .bind { (windRelativeSpeed) in
                    let valueThreshold = self.checkUnitSpeed(value: profileWindSpeed.value.threshold)
                    if NmeaManager.default.connected.value && profileCentrale.apparentWindSpeed!.value.isActivated && self.annonceSpeedType(newValue: windRelativeSpeed, annonceType: .apparentWindSpeed, valueThreshold: valueThreshold, timeThreshold: profileWindSpeed.value.timeThreshold){
                        self.state.apparentWindSpeed = (windRelativeSpeed, Date())
                    }
                }
            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
        }
        // Wind relative angle
        if let profileApparentWind = profileCentrale.apparentWindAngle {
            let disposable = self.apparentWindAngleVariable
                .asObservable()
                .bind { (windRelativeAngle) in
                    if profileApparentWind.value.isActivated && NmeaManager.default.connected.value && self.annonceAngleWithUnit(newValue: windRelativeAngle, annonceType: .apparentWindAngle, valueThreshold: Int(profileApparentWind.value.threshold), timeThreshold: profileApparentWind.value.timeThreshold, id: "") {
                        self.state.apparentWindAngle = (windRelativeAngle, Date())
                    }
                }
            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
            
        }
        
        if let profileWindAngle = profileCentrale.trueWindDirection {
            let disposable = self.windDirectionAngleVariable
                .asObservable()
                .bind { (windRelativeCardinalAngle) in
                    if NmeaManager.default.connected.value && profileWindAngle.value.isActivated && self.annonceAngleType(newValue: windRelativeCardinalAngle, annonceType: .windDirection, valueThreshold: Int(profileWindAngle.value.threshold), timeThreshold: profileWindAngle.value.timeThreshold) {
                         self.state.windDirection = (windRelativeCardinalAngle, Date())
                    }
                }
            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
            
        }
        // Water temperature
        if let profileWater = profileCentrale.waterTemperature {
            let disposable = self.waterTemperatureVariable
                .asObservable()
                .bind { (waterTemperature) in
                    if NmeaManager.default.connected.value && profileCentrale.waterTemperature!.value.isActivated && self.annonceSpeedType(newValue: waterTemperature, annonceType: .waterTemperature, valueThreshold: profileWater.value.threshold, timeThreshold: profileWater.value.timeThreshold, unit: NavigationUnit.degrees.description) {
                        self.state.waterTemperature = (waterTemperature, Date())
                    }
                }
            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
        }
        // Air temperature
        if let profileAir = profileCentrale.airTemperature {
            let disposable = self.airTemperatureVariable
                .asObservable()
                .bind { (airTemperature) in
                    if NmeaManager.default.connected.value && profileCentrale.airTemperature!.value.isActivated && self.annonceSpeedType(newValue: airTemperature, annonceType: .airTemperature, valueThreshold: profileAir.value.threshold, timeThreshold: profileAir.value.timeThreshold, unit: NavigationUnit.degrees.description) {
                        self.state.airTemperature = (airTemperature, Date())
                    }
                }
            self.disposables.append(disposable)
            disposable.disposed(by: disposeBag)
        }
    }
    
    //MARK: - Telltale
    func initTelltale() {
        
        guard let profile = User.shared.profile.profileTypes.first(where: {$0 is ProfileTelltale}) else {
            return
        }
        let profileTelltale = profile as! ProfileTelltale
        
        let disposable = self.tellTaleAngleVariable
            .asObservable()
            .bind { (id, angle) in
                if NmeaManager.default.connected.value && profileTelltale.angle.value.isActivated && self.annonceTelltale(newValue: (id, angle), annonceType: .tellTaleAngle, valueThreshold: Int(profileTelltale.angle.value.threshold), timeThreshold: profileTelltale.angle.value.timeThreshold) {
                    self.state.tellTaleAngle[id] = (angle, Date())
                }
            }
        self.disposables.append(disposable)
        disposable.disposed(by: disposeBag)
        
        let disposable2 = self.tellTaleMedianeVariable
            .asObservable()
            .bind { (id, mediane) in
                if NmeaManager.default.connected.value && profileTelltale.mediane.value.isActivated && self.annonceTelltale(newValue: (id, mediane), annonceType: .tellTaleMediane, valueThreshold: Int(profileTelltale.mediane.value.threshold), timeThreshold: profileTelltale.mediane.value.timeThreshold) {
                    self.state.tellTaleMediane[id] = (mediane, Date())
                }
            }
        self.disposables.append(disposable2)
        disposable2.disposed(by: disposeBag)
        
        let disposable3 = self.tellTaleAverageVariable
            .asObservable()
            .bind { (id, average) in
                if NmeaManager.default.connected.value && profileTelltale.average.value.isActivated && self.annonceTelltale(newValue: (id, average), annonceType: .tellTaleAverage, valueThreshold: Int(profileTelltale.average.value.threshold), timeThreshold: profileTelltale.average.value.timeThreshold) {
                    self.state.tellTaleAverage[id] = (average, Date())
                }
            }
        self.disposables.append(disposable3)
        disposable3.disposed(by: disposeBag)
    
        let disposable4 = self.tellTaleStandardVariationVariable
            .asObservable()
            .bind { (id, std) in
                if NmeaManager.default.connected.value && profileTelltale.standardDeviation.value.isActivated && self.annonceTelltale(newValue: (id, std), annonceType: .tellTaleStandardVariation, valueThreshold: Int(profileTelltale.standardDeviation.value.threshold), timeThreshold: profileTelltale.standardDeviation.value.timeThreshold) {
                    self.state.tellTaleStandardDeviation[id] = (std, Date())
                }
            }
        self.disposables.append(disposable4)
        disposable4.disposed(by: disposeBag)
        
        let disposable5 = self.tellTaleVariationMaxVariable
            .asObservable()
            .bind { (id, variation) in
                if NmeaManager.default.connected.value && profileTelltale.variationMax.value.isActivated && self.annonceTelltale(newValue: (id, variation), annonceType: .tellTaleVariationMax, valueThreshold: Int(profileTelltale.variationMax.value.threshold), timeThreshold: profileTelltale.variationMax.value.timeThreshold) {
                    self.state.tellTaleVariationMax[id] = (variation, Date())
                }
            }
        self.disposables.append(disposable5)
        disposable5.disposed(by: disposeBag)
        
        let disposable6 = self.tellTaleStateVarible
            .asObservable()
            .bind { (id, state) in
                if NmeaManager.default.connected.value && profileTelltale.state.value.isActivated {
                    if self.state.tellTaleState[id] != nil {
                        if state != self.state.tellTaleState[id]!.0 {
                            if (Int(Date().timeIntervalSince(self.state.tellTaleState[id]!.1))) > profileTelltale.state.value.timeThreshold {
                                if let i = TellTalesManager.default.tellTales.firstIndex(where: {$0.id == id}) {
                                    AnnouncementQueueManager.shared.addAnnounce(Announce(text: "\(TellTalesManager.default.tellTales[i].name) \(state)", priority: 5, type: .tellTaleState, annonceState: (id, nil)))
                                    self.state.tellTaleState[id] = (state, Date())
                                }
                            }
                        }
                    } else {
                        if let i = TellTalesManager.default.tellTales.firstIndex(where: {$0.id == id}) {
                            AnnouncementQueueManager.shared.addAnnounce(Announce(text: "\(TellTalesManager.default.tellTales[i].name) \(state)", priority: 5, type: .tellTaleState, annonceState: (id, nil)))
                            self.state.tellTaleState[id] = (state, Date())
                        }
                    }
                }
            }
        self.disposables.append(disposable6)
        disposable6.disposed(by: disposeBag)
        
    }
    
    // MARK: - Battery level
    func initVoice() {
        
        guard let profile = User.shared.profile.profileTypes.first(where: {$0 is ProfileVarious}) else {
            return
        }
        let profileVoice = profile as! ProfileVarious
        
        UIDevice.current.isBatteryMonitoringEnabled = true
        let disposable = self.batteryLevel.asObservable().bind { level in
            if profileVoice.batteryLevel.value.isActivated && level > 0 {
                let levelPercent = Int(100 * level)
                let thresholdPercent = Int(profileVoice.batteryLevel.value.threshold)
                if !NavigationManager.shared.isStartPoint() && levelPercent % thresholdPercent == 0 && self.state.batteryLevel != levelPercent {
                    AnnouncementQueueManager.shared.addAnnounce(Announce(text: NSLocalizedString("battery_level", bundle: self.bundle, comment: "") + " \(levelPercent)%", priority: 3, type: .batteryLevel, annonceState: nil))
                    self.state.batteryLevel = levelPercent
                }
            }
        }
        self.disposables.append(disposable)
        disposable.disposed(by: disposeBag)
        
        NotificationCenter.default.addObserver(forName: UIDevice.batteryLevelDidChangeNotification, object: nil, queue: nil) { (_) in
            self.batteryLevel.accept(UIDevice.current.batteryLevel)
        }
    }
    
    // MARK: - Carto
    func initCarto() {
        MapDataManager.shared.addObserver(self)
    }
    
    // MARK: - MapDataManagerObserver
    func mapDataManager(didUpdateBeacons beaconsFront: [Beacon], beaconsBehind: [Beacon]) {
        
        guard let profileCarto = User.shared.profile.profileTypes.first(where: {$0 is ProfileCarto}) as? ProfileCarto, let currentLoc = LocationManager.shared.getCurrentPosition(), profileCarto.enableBeacons.value else { // if enableBeacons false, do nothing
            return
        }
        
        guard let timeAnnounce = profileCarto.timeAnnounce?.value else {
            return
        }

        var beacons = beaconsFront
        if let profileOnlyFront = profileCarto.onlyFront, !profileOnlyFront.value.isActivated {
            beacons.append(contentsOf: beaconsBehind)
            // Sort beacons by proximity
            beacons.sort(by: { (b1, b2) -> Bool in
                guard let d1 = b1.distance(currentLoc: currentLoc) else {
                    return false
                }
                guard let d2 = b2.distance(currentLoc: currentLoc) else {
                    return false
                }
                return d1 <= d2
            })
        }
        if let detectionNumberProfile = profileCarto.detectionNumber?.value, detectionNumberProfile.isActivated {
            let detectionNumber = Int(detectionNumberProfile.threshold)
            if beacons.count > detectionNumber {
                beacons = Array(beacons[...(detectionNumber - 1)])
            }
        }
        if let detectionDistanceProfile = profileCarto.detectionDistance?.value, detectionDistanceProfile.isActivated {
            let detectionDistance = detectionDistanceProfile.threshold
            var beaconsToKeep: [Beacon] = []
            beacons.forEach({
                if let distance = $0.distance(currentLoc: currentLoc), distance < detectionDistance {
                    beaconsToKeep.append($0)
                }
            })
            beacons = beaconsToKeep
        }
        
        beacons.forEach({
            var launchAnnouncement: Bool = false
            if let profileDistance = profileCarto.distance {
                let distance = $0.distance(currentLoc: currentLoc)
                if profileDistance.value.isActivated && self.annonceDistanceType(newValue: distance, annonceType: .distanceBeacon, timeThreshold: Int(timeAnnounce.threshold), id: $0.id){
                    self.state.distanceBeacon[$0.id] = (distance, Date())
                    launchAnnouncement = true
                }
            }
            if let profileAzimut = profileCarto.azimut {
                let azimut = $0.bearing(currentLoc: currentLoc)?[0]
                if profileAzimut.value.isActivated && self.annonceAngleWithUnit(newValue: azimut, annonceType: .azimutBeacon, valueThreshold: Int(profileAzimut.value.threshold), timeThreshold: Int(timeAnnounce.threshold), id: $0.id) {
                    self.state.bearingBeacons[$0.id] = ((azimut?.0, azimut?.1), Date())
                    launchAnnouncement = true
                }
            }
            if let profileGisement = profileCarto.gisement {
                let gisement = $0.bearing(currentLoc: currentLoc)?[1]
                if profileGisement.value.isActivated && self.annonceAngleWithUnit(newValue: gisement, annonceType: .gisementBeacon, valueThreshold: Int(profileGisement.value.threshold), timeThreshold: Int(timeAnnounce.threshold), id: $0.id) {
                    self.state.gisementBeacon[$0.id] = ((gisement?.0, gisement?.1), Date())
                    launchAnnouncement = true
                }
            }
            if let profileGisementTime = profileCarto.gisementTime {
                let gisementTime = $0.bearing(currentLoc: currentLoc)?[2]
                if profileGisementTime.value.isActivated && self.annonceAngleTime(newValue: gisementTime?.0, annonceType: .gisementTimeBeacon, valueThreshold: Int(profileGisementTime.value.threshold), timeThreshold: Int(timeAnnounce.threshold), id: $0.id) {
                    self.state.gisementTimeBeacon[$0.id] = (gisementTime?.0, Date())
                    launchAnnouncement = true
                }
            }
            if launchAnnouncement { // If there is at least one true, launch annonce.
                // Put the same time for all the values
                self.state.distanceBeacon[$0.id]?.1 = Date()
                self.state.bearingBeacons[$0.id]?.1 = Date()
                self.state.gisementBeacon[$0.id]?.1 = Date()
                self.state.gisementTimeBeacon[$0.id]?.1 = Date()
                AnnouncementQueueManager.shared.addAnnounce(Announce(text: "", priority: 3, type: .carto, annonceState: ($0, Date())))
            }
        })
        
    }
    
    func mapDataManager(didDeleteBeacons deletedBeacons: [Beacon]) {
        
    }
    
    // MARK: - Start Announcement
    func startAnnouncement() {
        var announcementInterval: Int
        if NavigationManager.shared.totalTime > 45 {
            announcementInterval = 8
        } else if NavigationManager.shared.totalTime < 45 && NavigationManager.shared.totalTime > 10 {
            announcementInterval = 5
        } else {
            announcementInterval = 2
        }
//        let announcementInterval = NavigationManager.shared.totalTime > 45 ? 8 : 3 // Interval between announcement: 8s if timer > 15s otherwise 3s
        if self.state.startAnnonce == nil || (self.state.startAnnonce != nil && (Int(Date().timeIntervalSince(self.state.startAnnonce!))) > announcementInterval) {
            self.state.startAnnonce = Date()
            AnnouncementQueueManager.shared.addAnnounce(Announce(text: "", priority: 2, type: .start, annonceState: nil))
        }
    }
    
    // MARK: - Reset
    func resetCurrentState() {
        print("Reset")
        AnnouncementQueueManager.shared.clearFeed()
        state = CurrentAnnouncesState()
        // Dispose previous disposables
        self.disposables.forEach {
            $0.dispose()
        }
        self.disposables.removeAll(keepingCapacity: true)
        self.disposablesGPS.forEach {
            $0.dispose()
        }
        self.disposablesGPS.removeAll(keepingCapacity: true)
        self.disposablesRoute.forEach {
            $0.dispose()
        }
        self.disposablesRoute.removeAll(keepingCapacity: true)
        
        MapDataManager.shared.removeObserver(self)
        
        initGPS()
        initRoute()
        initCentrale()
        initTelltale()
        initVoice()
        initCarto()
    }
    
    func resetCurrentStateGPS() {
        print("==== Reset GPS")
        state.compass = (nil, nil)
        state.courseOverGround = (nil, nil)
        state.speedOverGround = (nil, nil)
        state.maxSpeedOverGround = (nil, nil)
        
        self.disposablesGPS.forEach {
            $0.dispose()
        }
        self.disposablesGPS.removeAll(keepingCapacity: true)
        initGPS()
    }
    
    func resetCurrentStateRoute() {
        print("==== Reset Route")
        state.bearing = ((nil, nil), nil)
        state.gisement = ((nil, nil), nil)
        state.gisementTime = (nil, nil)
        state.distance = (nil, nil)
        state.CMG = (nil, nil)
        state.gapToSegment = (nil, nil)
        state.bearing2 = ((nil, nil), nil)
        state.gisement2 = ((nil, nil), nil)
        state.gisementTime2 = (nil, nil)
        state.distance2 = (nil, nil)
        state.CMG2 = (nil, nil)
        state.entry3Lengths = (false, nil)
    }
    
    func startAnnoncesRoute() {
        stopAnnoncesRoute()
        initRoute()
    }
    
    func stopAnnoncesRoute(){
        print("=== Stop announce")
        self.resetCurrentStateRoute()
        self.disposablesRoute.forEach {
            $0.dispose()
        }
        self.disposablesRoute.removeAll(keepingCapacity: true)
    }
    
    /// Class to store the current state of all the variables
    /// To be able to compare them to incoming values
    // (last value, date when we stored the last value)
    class CurrentAnnouncesState {
        // GPS
        var compass: (Int?, Date?) = (nil, nil)
        var courseOverGround: (Int?, Date?) = (nil, nil)
        var speedOverGround: (Float?, Date?) = (nil, nil)
        var maxSpeedOverGround: (Float?, Date?) = (nil, nil)
        // Route
        var bearing: ((Int?, NavigationUnit?), Date?) = ((nil, nil), nil)
        var gisement: ((Int?, NavigationUnit?), Date?) = ((nil, nil), nil)
        var gisementTime: (Int?, Date?) = (nil, nil)
        var distance: ((Float, Float)?, Date?) = (nil, nil)
        var CMG: (Float?, Date?) = (nil, nil)
        var gapToSegment: (Float?, Date?) = (nil, nil)
        var entry3Lengths: (Bool, Date?) = (false, nil)
        var bearing2: ((Int?, NavigationUnit?), Date?) = ((nil, nil), nil)
        var gisement2: ((Int?, NavigationUnit?), Date?) = ((nil, nil), nil)
        var gisementTime2: (Int?, Date?) = (nil, nil)
        var distance2: ((Float, Float)?, Date?) = (nil, nil)
        var CMG2: (Float?, Date?) = (nil, nil)
        // Navigation System
        var depth: (Float?, Date?) = (nil, nil)
        var headingMagnetic: (Int, Date?) = (0, nil)
        var speedThroughWater: (Float, Date?) = (0, nil) // Speed Through Water
        var trueWindSpeed: (Float, Date?) = (0, nil) // TWS
        var trueWindAngle: ((Int?, NavigationUnit?), Date?) = ((nil, nil), nil) // TWA
        var apparentWindSpeed: (Float, Date?) = (0, nil) // AWS
        var apparentWindAngle: ((Int?, NavigationUnit?), Date?) = ((nil, nil), nil) // AWA
        var windDirection: (Int, Date?) = (0, nil)
        var waterTemperature: (Float?, Date?) = (nil, nil)
        var airTemperature: (Float, Date?) = (0, nil)
        // Telltale
        var tellTaleAngle: [String:(Int, Date)] = [:]
        var tellTaleMediane: [String:(Int, Date)] = [:]
        var tellTaleAverage: [String:(Int, Date)] = [:]
        var tellTaleStandardDeviation: [String:(Int, Date)] = [:]
        var tellTaleVariationMax: [String:(Int, Date)] = [:]
        var tellTaleState: [String:(String, Date)] = [:]
        // Batterry
        var batteryLevel: Int = 0
        // Seascape
        var distanceBeacon: [String:(Float?, Date)] = [:]
        var gisementBeacon: [String:((Int?, NavigationUnit?), Date?)] = [:]
        var gisementTimeBeacon: [String: (Int?, Date?)] = [:]
        var bearingBeacons: [String:((Int?, NavigationUnit?), Date?)] = [:]
        // Start procedure
        var startAnnonce: Date?
    }
    
    public func playBeepPoint() {
        if let path = bundle.path(forResource: "beep-07", ofType: "wav") {
            playBeep(path: path)
        }
    }
    
    public func playFoghorn() {
        if let path = bundle.path(forResource: "pouetCourt", ofType: "wav") {
            playBeep(path: path)
        }
    }
    
    public func playWhistle() {
        if let path = bundle.path(forResource: "sifflet", ofType: "wav") {
            playBeep(path: path)
        }
    }
    
    public func playBeepTelltale() {
        if !AnnouncementQueueManager.shared.muted { // TODO: - check if we want to have the beep when mutes
            if let _ = bundle.path(forResource: "beep_penon", ofType: "wav") {
//                playBeep(path: path)
            }
        }
    }
    
    private func playBeep(path: String) {
        let url = URL(fileURLWithPath: path)
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .spokenAudio, options: [.interruptSpokenAudioAndMixWithOthers])
            try AVAudioSession.sharedInstance().setActive(true)
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.wav.rawValue)
            player?.play()
        } catch {
            print("=== ERROR")
        }
    }
}
