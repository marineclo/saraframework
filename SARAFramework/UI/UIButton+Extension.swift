//
//  UIButton+Extension.swift
//  SARA Croisiere
//
//  Created by Rose on 10/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    public func customizePrimary() {
        self.layer.borderWidth = 1
        self.layer.borderColor = Utils.primaryColor.cgColor
        self.backgroundColor = Utils.primaryColor
        self.tintColor = Utils.reverseColor
        self.customize()
    }
    
    func tintColorDarkMode() {
        if #available(iOS 13, *) {
            self.tintColor = UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                if UITraitCollection.userInterfaceStyle == .dark {
                    /// Return the color for Dark Mode
                    return #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                } else {
                    /// Return the color for Light Mode
                    return Utils.primaryColor
                }
            }
        } else {
            /// Return a fallback color for iOS 12 and lower.
            self.tintColor = Utils.primaryColor
        }
    }
    
    public func customizeSecondary() {
        self.layer.borderWidth = 1
        self.layer.borderColor = Utils.secondaryColor.cgColor
        self.tintColorDarkMode()
        self.customize()
    }
    
    public func customizeReverse() {
        self.layer.borderWidth = 1
        self.layer.borderColor = Utils.reverseColor.cgColor
        self.tintColor = Utils.reverseColor
        self.customize()
    }
    
    func customize() {
        self.layer.cornerRadius = 10
        self.titleLabel?.font = UIFont.preferredFont(forTextStyle: .body)
        self.isAccessibilityElement = true
        self.accessibilityLabel = self.titleLabel?.text
    }
    
    func customizeSliderButton(borderColor: CGColor = Utils.secondaryColor.cgColor) {
        self.layer.cornerRadius = 0.5 * self.frame.size.width
        self.layer.borderWidth = 1
        self.layer.borderColor = borderColor
        self.tintColorDarkMode()
        self.layer.masksToBounds = true
    }
    
    public func moveImageLeftTextCenter(imagePadding: CGFloat = 30.0){
        self.imageView?.contentMode = .scaleAspectFit
        guard let imageViewWidth = self.imageView?.frame.width else{return}
        guard let titleLabelWidth = self.titleLabel?.intrinsicContentSize.width else{return}
        let buttonViewHeight = self.frame.height
        imageEdgeInsets = UIEdgeInsets(top: 0.0, left: imagePadding - imageViewWidth / 2, bottom: 0.0, right: 0.0)
        if titleLabelWidth > self.frame.width { // title longer than the button size
            if imageViewWidth > buttonViewHeight { // on iphone 12 mini
                titleEdgeInsets = UIEdgeInsets(top: 0.0, left: (imageViewWidth - buttonViewHeight) / 2 - buttonViewHeight, bottom: 0.0, right: imagePadding)
            } else {
                titleEdgeInsets = UIEdgeInsets(top: 0.0, left: imagePadding, bottom: 0.0, right: imagePadding)
            }
        } else {
            titleEdgeInsets = UIEdgeInsets(top: 0.0, left: (bounds.width - titleLabelWidth) / 2 - imageViewWidth, bottom: 0.0, right: imagePadding)
        }
    }
    
    public func addRightImage(imagePadding: CGFloat = 30.0, image: UIImage?) {
        if let rightImage = image {
            rightImage.withRenderingMode(.alwaysTemplate)
            let rightImageView = UIImageView(image: rightImage)
            rightImageView.tintColor = Utils.secondaryColor
            rightImageView.tag = 100
            
            let height = (self.imageView?.frame.height ?? 0) - 10 //* 0.2
            let width = height
            let xPos = self.frame.width - width - imagePadding
            let yPos = (self.frame.height - height) / 2
            
            rightImageView.frame = CGRect(x: xPos, y: yPos, width: width, height: height)
            self.addSubview(rightImageView)
        }
    }
    
    public func alignTextUnderImage(spacing: CGFloat = 6.0)
    {
        let imageSize: CGFloat = 32
        if let uiImage = self.imageView, let image = uiImage.image
        {
            let titleSize = self.titleLabel!.bounds.size
            self.imageView?.contentMode = .scaleAspectFit
            if image.size.width > imageSize { // iphone 12 mini
                let scale = imageSize / image.size.width
                self.imageView?.layer.transform = CATransform3DMakeScale(scale, scale, scale)
                self.imageEdgeInsets = UIEdgeInsets(top: -titleSize.height, left: 0.0, bottom: 0.0, right: -titleSize.width )
                self.titleEdgeInsets = UIEdgeInsets(top: spacing, left: -image.size.width, bottom: -imageSize, right: 0.0)
            } else {
                self.titleEdgeInsets = UIEdgeInsets(top: imageSize, left: -imageSize, bottom: 0, right: 0)
                self.imageEdgeInsets = UIEdgeInsets(top: -titleSize.height, left: 0, bottom: 0, right: -titleSize.width)
            }
        }
    }
}
