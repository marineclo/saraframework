//
//  Float+Extension.swift
//  SARA Croisiere
//
//  Created by Rose on 20/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation

extension Float {
    var coordinates: (Int, Float) {
        let degrees = Int(self)
        let minutes = (self - Float(degrees)) * 60
        
        return (degrees, minutes)
    }
    
    var coordinatesDMS: (Int, Int, Float) {
        let degrees = Int(self)
        let degreeDecimal = (self - Float(degrees)) * 60
        let minutes = Int(degreeDecimal)
        let seconds = (degreeDecimal - Float(minutes)) * 60
        return (degrees, minutes, seconds)
    }
    
    func meterToMile() -> Float {
        return (self / 1_852).formatted
    }
    
    func mileToMeter() -> Float {
        return (self * 1_852).formatted
    }
    
    func msToKmh() -> Float {
        return (self * 3.6).formatted
    }
    
    func msToKnots() -> Float {
        return (self * 1.943_844).formatted
    }
    
    func kmhToKnots() -> Float {
        return (self / 1.852).formatted
    }
    
    func knotsToKmh() -> Float {
        return (self * 1.852).formatted
    }
    
    func knotsToMs() -> Float {
        return (self * 1.852 / 3.6)
    }
    
    var radians: Float { return self * .pi / 180 }
    var degrees: Float { return self * 180 / .pi }
    
    var formatted: Float {
        return Float(String(format: "%.1f", self)) ?? 0.0
    }
    var roundDown: Int {
        if self > 50 {
            return Int(10 * Float(self/10).rounded(.down))
        } else {
            return Int(self)
        }
    }
    
    // Rounds the float to decimal places value
    func rounded(toPlaces places:Int) -> Float {
        let divisor = pow(10.0, Float(places))
        return (self * divisor).rounded() / divisor
    }
}
