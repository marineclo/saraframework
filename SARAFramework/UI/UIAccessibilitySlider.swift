//
//  UIAccessibilitySlider.swift
//  SARA Croisiere
//
//  Created by Rose on 21/02/2018.
//  Copyright © 2018 Rose. All rights reserved.
//

import Foundation
import UIKit

public class UIAccessibilitySlider: UISlider {
    
    var step: Double = 1.0
    
    public override func accessibilityIncrement() {
        self.value += Float(step)
        sendActions(for: .valueChanged)
    }
    
    public override func accessibilityDecrement() {
        self.value -= Float(step)
        sendActions(for: .valueChanged)
    }
    
    func distanceSliderSetUp(valueLabel: UILabel, value: Float) {
        // Since the distance is stored in meters, make the conversion if necessary
        if Settings.shared.distanceUnit?.unit != NavigationUnit.meter {
            let mileDistance = value.meterToMile()
            
            self.value = min(mileDistance, 1)
            valueLabel.text = String(format: "%.1f", min(mileDistance, 1))
            self.step = 0.1
            self.maximumValue = 1
            self.minimumValue = 0.1
        } else {
            let meterValue = min(value, 1_852)
            
            valueLabel.text = "\(Int(meterValue))"
            self.step = 10
            self.maximumValue = 1_852
            self.minimumValue = 5
        }
        
        // reload the steps and domains of the slider
        self.reloadInputViews()
        
        // Set the current value, after the reload, to keep the right value on the slider
        if Settings.shared.distanceUnit?.unit != NavigationUnit.meter {
            let mileDistance = value.meterToMile()
            self.value = min(mileDistance, 1)
        } else {
            let meterValue = min(value, 1_852)
            self.value = meterValue
        }
    }
    
    func distanceValidationSliderSetUp(valueLabel: UILabel, value: Float) {
        
        let meterValue = min(value, 1000)
        
        valueLabel.text = "\(Int(meterValue))"
        self.step = 10
        self.maximumValue = 1000
        self.minimumValue = value == 0 ? 0 : 1 // If value == 0 it's a MOB
        
        // reload the steps and domains of the slider
        self.reloadInputViews()
        
        // Set the current value, after the reload, to keep the right value on the slider
        self.value = meterValue
        
    }
}
