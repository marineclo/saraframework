//
//  CoursesViewModel.swift
//  SARA Croisiere
//
//  Created by Rose on 10/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import RealmSwift

public class CoursesViewModel {
    let realm = try! Realm()
    
    func getCourseNameAtIndex(_ indexPath: IndexPath) -> String? {
        return Course.getCourseNameAtIndex(indexPath)
    }
    
    func getCourseAtIndex(_ indexPath: IndexPath) -> Course? {
        return Course.getCourseAtIndex(indexPath)
    }
    
    func removeCourseAtIndex(_ index: IndexPath) {
        guard let course = self.getCourseAtIndex(index) else {
            return
        }
        if course == NavigationManager.shared.route.value {
            NavigationManager.shared.route.accept(nil)
        }
        try! realm.write {
            realm.delete(course)
        }
        
    }
    
    func numberOfCourses() -> Int {
        return realm.objects(Course.self).count
    }
}
