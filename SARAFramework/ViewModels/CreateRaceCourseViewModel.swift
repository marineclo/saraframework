//
//  CreateRaceCourseViewModel.swift
//  SARAFramework
//
//  Created by Marine on 17.04.21.
//

import Foundation
import RealmSwift
import RxSwift
import RxCocoa

public class CreateRaceCourseViewModel: CreateCourseViewModel {
    public var course: RaceCourse
    public var type: BehaviorRelay<RaceCourseType?>
    public var laps: BehaviorRelay<Int?>
    public var autoCreationParams: AutoCreationParams?
    public var buoyRef = BehaviorRelay<Buoy?>(value: nil)
    public var windAxis: Int = 180
    public var startLength: Int = 200
    public var startWindwardLength: Int = 700
    public var finishLength: Int = 200
    public var leewardFinishLength: Int = 400
    public var offsetMarkLength: Int?
    
    public init(course: RaceCourse?) {
        if let safeCourse = course {
            self.course = safeCourse
            self.type = BehaviorRelay<RaceCourseType?>(value: safeCourse.type.value)
            self.laps = BehaviorRelay<Int?>(value: safeCourse.laps.value)
            if safeCourse.autoCreation != nil {
                self.autoCreationParams = safeCourse.autoCreation
            } else {
                self.autoCreationParams = AutoCreationParams()
            }
            
            self.buoyRef.accept(safeCourse.autoCreation?.refBuoy?.duplicate())
            if let autoCreation = safeCourse.autoCreation {
                self.windAxis = autoCreation.windAxis
                self.startLength = autoCreation.startLength
                self.startWindwardLength = autoCreation.startWindwardLength
                self.finishLength = autoCreation.finishLength
                self.leewardFinishLength = autoCreation.leewardFinishLength
                self.offsetMarkLength = autoCreation.offsetMarkLength.value
            }
            super.init(points: Array(safeCourse.points.map({$0.point.duplicate()})))
            self.status = safeCourse.status
        } else {
            self.course = RaceCourse()
            self.autoCreationParams = AutoCreationParams()
            self.laps = BehaviorRelay<Int?>(value: 1)
            self.type = BehaviorRelay<RaceCourseType?>(value: nil)
            super.init(points: [])
            self.status = .none
        }
    }
    
    public func getCourseName() -> String? {
        return self.course.name.isEmpty ? nil : self.course.name
    }
    
    public func setType(type: RaceCourseType?) {
        self.type.accept(type)
    }
    
    // Check if the name used is not already used. Not possible to have 2 courses with the same name.
    public func checkIfNameExists(name: String) -> Bool {
        let realm = try! Realm()
        if realm.objects(RaceCourse.self).first(where: {$0.name == name && $0.id != self.course.id}) != nil {
            return true
        }
        return false
    }
    
    // Check if there is a firebase route with the same name (without star)
    public func checkIfNameAlreadyExists(name: String) -> Bool {
        return FirebaseManager.shared.firebaseCourses.contains(where: { $0.name.filter({ $0 != "*"}) == name.filter({ $0 != "*"}) && $0.id != self.course.id})
    }
    
    // Check if the start mark is the first mark of the course
    public func startMarkFirst() -> Bool {
        if let index = self.getTempPoints().firstIndex(where: { $0 is Mark && ($0 as! Mark).markType == Mark.MarkType.start.rawValue }), index != 0 {
            return false
        }
        return true
    }
    
    // Check if the finish mark is the last mark of the course
    public func finishMarkLast() -> Bool {
        if let index = self.getTempPoints().firstIndex(where: { $0 is Mark && ($0 as! Mark).markType == Mark.MarkType.end.rawValue }), index != (self.getTempPoints().count - 1) {
            return false
        }
        return true
    }
    
    // Check before saving
    public func checkSave(name: String) -> [String]? {
        var errorMessages: [String] = []
        if self.status == .shared && self.checkIfNameAlreadyExists(name: name) {
            errorMessages.append(Utils.localizedString(forKey: "warning_same_name_message_firebase", app: "SARARegate"))
        }
        if self.checkIfNameExists(name: name) {
            errorMessages.append(Utils.localizedString(forKey: "warning_same_name_message", app: "SARARegate"))
        }
        if self.consecutivelyPoints() {
            errorMessages.append(NSLocalizedString("warning_same_point_consecutively", bundle: Utils.bundle(anyClass: CreateRaceCourseViewModel.self), comment: ""))
        }
        if !startMarkFirst() {
            errorMessages.append(Utils.localizedString(forKey: "warning_start_not_first", app: "SARARegate"))
        }
        if !finishMarkLast() {
            errorMessages.append(Utils.localizedString(forKey: "warning_finish_not_last", app: "SARARegate"))
        }
        return errorMessages.isEmpty ? nil : errorMessages
    }
    
    // Save or update course
    public func saveRoute(name: String, update: Bool = false, successHandler: @escaping () -> Void, errorHandler: @escaping ([String]) -> Void) {
        if let errorMessages = checkSave(name: name) {
            errorHandler(errorMessages)
            return
        }
        let realm = try! Realm()
        // Save or update mark
        self.getTempPoints().forEach({
            if let m = $0 as? Mark {
                if let mt = realm.object(ofType: Mark.self, forPrimaryKey: m.id) {
                    let ma = Mark(id: mt.id, name: m.name, markType: Mark.MarkType(rawValue: m.markType)!, buoys: Array(m.buoys))
                    mt.update(with: ma, shared: false)
                } else {
                    m.save()
                }
            }
        })
        // Delete in database the marks that have been deleted by the user. Get a list of unique marks. Compare with the TempsPoints to know wich one to delete
        for pointRealm in self.pointRealmUnique(points: self.course.points.map({$0.point})) {
            if !self.getTempPoints().contains(where: {$0.id == pointRealm.id}) {
                pointRealm.deletePoint()
            }
        }

        try! realm.write {
            self.course.points.removeAll()
            self.getTempPoints().forEach({
                if let point = realm.object(ofType: AnyPoint.self, forPrimaryKey: $0.id) {
                    self.course.points.append(point)
                }
            })
            self.course.name = name
            if self.course.id == "" {
                self.course.id = UUID().uuidString
            }
            self.course.status = status
            self.course.type.value = type.value
            self.course.laps.value = self.laps.value
            if self.buoyRef.value != nil {
                // Get buoy ref
                self.autoCreationParams?.refBuoy = self.course.points.map({($0.point as! Mark).buoys}).flatMap({$0}).first(where: {$0.type.value == buoyRef.value?.type.value})
                if self.autoCreationParams!.id == "" {
                    self.autoCreationParams!.id = UUID().uuidString
                }
                self.autoCreationParams?.windAxis = windAxis
                self.autoCreationParams?.startLength = startLength
                self.autoCreationParams?.startWindwardLength = startWindwardLength
                self.autoCreationParams?.finishLength = finishLength
                self.autoCreationParams?.leewardFinishLength = leewardFinishLength
                self.autoCreationParams?.offsetMarkLength.value = offsetMarkLength
                
                self.course.autoCreation = autoCreationParams
            } else {
                self.course.autoCreation = nil
            }
            
            if !update {
                realm.add(self.course)
                realm.add(AnyCourse(self.course))
            } else {
                realm.add(self.course, update: .modified)
            }
        }
        self.saveOnFirebase(update: update, successHandler: {
            successHandler()
        }, errorHandler: { (error) in
            errorHandler([error.localizedDescription])
        })
    }
    
    // Check if modifications should be saved on Firebase
    public func saveOnFirebase(update: Bool, successHandler: @escaping () -> Void, errorHandler: @escaping (Error) -> Void) {
        if self.course.status == .shared {
            if User.shared.email != nil {
                FirebaseManager.shared.saveCourse(course: self.course, update: update, successHandler: {
                    successHandler()
                }, errorHandler: { (error) in
                    errorHandler(error)
                })
            }
        } else {
            successHandler()
        }
    }
        
    // Calculate the middle of the line based on ref buoy and then the other buoys position based on the middle of the line
    public func constructCourseBased(on buoy: Buoy) {
        var middleStart: GPSPosition?
        var azimut = 0
        switch buoy.type.value {
        case .windward:
            azimut = windAxis.oppositeWind()
            middleStart = buoy.gpsPosition?.shift(byDistance: Double(startWindwardLength), azimuth: azimut.degreesToRadians())
        case .startPin:
            azimut = windAxis.plus90Wind()
            middleStart = buoy.gpsPosition?.shift(byDistance: Double(startLength / 2), azimuth: azimut.degreesToRadians())
        case .startCommittee:
            azimut = windAxis.minus90Wind()
            middleStart = buoy.gpsPosition?.shift(byDistance: Double(startLength / 2), azimuth: azimut.degreesToRadians())
        case .leeward:
            azimut = windAxis.plus90Wind()
            middleStart = buoy.gpsPosition?.shift(byDistance: Double(startLength / 2), azimuth: azimut.degreesToRadians())
        case .finishPin:
            if self.type.value == .matchRace { // Finish line: inverse of the departure line
                azimut = windAxis.plus90Wind()
                middleStart = buoy.gpsPosition?.shift(byDistance: Double(startLength / 2), azimuth: azimut.degreesToRadians())
            } else {
                azimut = windAxis
                let finishCommittee = buoy.gpsPosition?.shift(byDistance: Double(finishLength), azimuth: azimut.degreesToRadians())
                azimut = windAxis.minus90Wind()
                middleStart = finishCommittee?.shift(byDistance: Double(leewardFinishLength - (startLength / 2)), azimuth: azimut.degreesToRadians())
            }
        case .finishCommittee:
            azimut = windAxis.minus90Wind()
            middleStart = buoy.gpsPosition?.shift(byDistance: Double(leewardFinishLength - (startLength / 2)), azimuth: azimut.degreesToRadians())
        case .offsetMark:
            azimut = windAxis.plus90Wind()
            let windmark = buoy.gpsPosition?.shift(byDistance: Double(offsetMarkLength.value ?? 0), azimuth: azimut.degreesToRadians())
            azimut = windAxis.oppositeWind()
            middleStart = windmark?.shift(byDistance: Double(startWindwardLength), azimuth: azimut.degreesToRadians())
        default:
            middleStart = nil
            return
        }
        
        guard middleStart != nil else {
            return
        }

        var distance = 0
        let buoys = self.getTempPoints().map({($0 as! Mark).buoys}).flatMap({$0}) // get list of buoys
        buoys.sorted(by: {$0.type.value!.rawValue < $1.type.value!.rawValue}).forEach({
            if $0.type.value != buoy.type.value { // no need for reference buoy
                var gpsPos = GPSPosition()
                switch $0.type.value {
                case .windward:
                    azimut = windAxis
                    distance = startWindwardLength
                    gpsPos = middleStart!.shift(byDistance: Double(distance), azimuth: azimut.degreesToRadians())
                case .startPin:
                    azimut = windAxis.minus90Wind()
                    distance = startLength / 2
                    gpsPos = middleStart!.shift(byDistance: Double(distance), azimuth: azimut.degreesToRadians())
                case .startCommittee:
                    azimut = windAxis.plus90Wind()
                    distance = startLength / 2
                    gpsPos = middleStart!.shift(byDistance: Double(distance), azimuth: azimut.degreesToRadians())
                case .leeward:
                    azimut = windAxis.minus90Wind()
                    distance = startLength / 2
                    gpsPos = middleStart!.shift(byDistance: Double(distance), azimuth: azimut.degreesToRadians())
                case .finishPin:
                    if self.type.value == .matchRace {
                        azimut = windAxis.minus90Wind()
                        distance = startLength / 2
                        gpsPos = middleStart!.shift(byDistance: Double(distance), azimuth: azimut.degreesToRadians())
                    } else {
                        azimut = windAxis.oppositeWind()
                        distance = finishLength
                        let finishCommittee = buoys.first(where: {$0.type.value == .finishCommittee})
                        gpsPos = finishCommittee!.gpsPosition!.shift(byDistance: Double(distance), azimuth: azimut.degreesToRadians())
                    }
                case .finishCommittee:
                    if self.type.value == .matchRace {
                        azimut = windAxis.plus90Wind()
                        distance = startLength / 2
                        gpsPos = middleStart!.shift(byDistance: Double(distance), azimuth: azimut.degreesToRadians())
                    } else {
                        azimut = windAxis.plus90Wind()
                        distance = leewardFinishLength - (startLength / 2)
                        gpsPos = middleStart!.shift(byDistance: Double(distance), azimuth: azimut.degreesToRadians())
                    }
                case .offsetMark:
                    azimut = windAxis.minus90Wind()
                    distance = offsetMarkLength!
                    let windward = buoys.first(where: {$0.type.value == .windward})
                    gpsPos = windward!.gpsPosition!.shift(byDistance: Double(distance), azimuth: azimut.degreesToRadians())
                default:
                    break
                }
//                print("\($0.name) \(gpsPos.latitude) \(gpsPos.longitude)")
                $0.gpsPosition = GPSPosition(id: $0.gpsPosition?.id, latitude: gpsPos.latitude, longitude: gpsPos.longitude)
            } else {
                guard let gpsPos = buoy.gpsPosition else { return }
                $0.gpsPosition = GPSPosition(id: $0.gpsPosition?.id, latitude: gpsPos.latitude, longitude: gpsPos.longitude)
            }
        })
//        print(buoys)
    }
    
    // add marks in the route when the laps slider changes
    public func addMarksLaps() {
        let marks = self.getTempPoints().filter({$0 is Mark && ($0 as! Mark).markType != Mark.MarkType.start.rawValue && ($0 as! Mark).markType != Mark.MarkType.end.rawValue }) // get all the marks except start and end
//        let marksUnique = Array(NSOrderedSet(array: marks))
        let marksUnique = pointRealmUnique(points: marks)
        if self.type.value == .matchRace {
            if marksUnique.count == 1 {
                // Add coordinate for the blue bouy if the other marks have coordinates.
                let blueMarkLeeward = Mark.markModel(type: .leewardS, isMatchRace: true)
                let blueMarkStartGpsPos = (self.getTempPoints().first as? Mark)?.buoys.first?.gpsPosition
                if let latitude = blueMarkStartGpsPos?.latitude, let longitude = blueMarkStartGpsPos?.longitude {
                    blueMarkLeeward.buoys.first?.gpsPosition = GPSPosition(latitude: latitude, longitude: longitude)
                }
                addToRoute(blueMarkLeeward, at: IndexPath(row: marks.count + 1, section: 0))
                addToRoute(marksUnique.first as! PointRealm, at: IndexPath(row: marks.count + 2, section: 0))
            } else {
                marksUnique.reversed().enumerated().forEach({
                    addToRoute($1 as! PointRealm, at: IndexPath(row: marks.count + 1 + $0, section: 0))
                })
            }
        } else if self.type.value == .trainingLoop {
            marksUnique.enumerated().forEach({
                addToRoute($1 as! PointRealm, at: IndexPath(row: marks.count + $0, section: 0))
            })
        } else {
            marksUnique.enumerated().forEach({
                addToRoute($1 as! PointRealm, at: IndexPath(row: marks.count + 1 + $0, section: 0))
            })
        }
    }
    
    // Get an array of unique points
    func pointRealmUnique(points: [PointRealm]) -> [PointRealm]{
        var unique: [PointRealm] = []
        points.forEach({ point in
            if !unique.contains(where: {$0.id == point.id}) {
                unique.append(point)
            }
        })
        return unique
    }
    
    // remove marks in the route when the laps slider changes
    public func removeMarksLaps() {
        let marks = self.getTempPoints().filter({$0 is Mark && ($0 as! Mark).markType != Mark.MarkType.start.rawValue && ($0 as! Mark).markType != Mark.MarkType.end.rawValue }) // get all the marks except start and end
//        let marksUniqueNb = Array(NSOrderedSet(array: marks)).count
        let marksUniqueNb = pointRealmUnique(points: marks).count
        let numberOfPoints = self.numberOfPoints() ?? 0
        if self.type.value == .trainingLoop {
            (0..<marksUniqueNb).forEach{
                removeFromRoute(at: IndexPath(row: numberOfPoints - 1 - $0, section: 0)) // remove the last mark
            }
        } else {
            (0..<marksUniqueNb).forEach{
                removeFromRoute(at: IndexPath(row: numberOfPoints - 2 - $0, section: 0)) // remove the last mark before the end mark
            }
        }
        
    }
    
    public func updateMarkWith(mark: Mark) {
        if self.getTempPoints().filter({ $0.id == mark.id}).count != 0 {
            self.getTempPoints().filter({ $0.id == mark.id}).forEach({
                if let _ = $0 as? Mark {
                    ($0 as! Mark).name = mark.name
                    ($0 as! Mark).markType = mark.markType
                    ($0 as! Mark).buoys.removeAll()
                    ($0 as! Mark).buoys.append(objectsIn: mark.buoys)
                }
            })
        } else {
            self.addToRoute(mark)
        }
    }
    
    public func updateViewModel(course: RaceCourse) {
        self.course = course
        self.type.accept(course.type.value)
        self.laps.accept(course.laps.value)
        self.autoCreationParams = course.autoCreation
        self.buoyRef.accept(course.autoCreation?.refBuoy?.duplicate())
        if let autoCreation = course.autoCreation {
            self.windAxis = autoCreation.windAxis
            self.startLength = autoCreation.startLength
            self.startWindwardLength = autoCreation.startWindwardLength
            self.finishLength = autoCreation.finishLength
            self.leewardFinishLength = autoCreation.leewardFinishLength
            self.offsetMarkLength = autoCreation.offsetMarkLength.value
        }
        let points = Array(course.points.map({$0.point.duplicate()}))
        self.setTempPoints(points: points)
        temporaryPointsB.accept(points)
        self.status = course.status
    }
}
