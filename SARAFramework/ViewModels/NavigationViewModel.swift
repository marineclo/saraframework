//
//  NavigationViewModel.swift
//  SARANav
//
//  Created by Marine on 06.03.21.
//  Copyright © 2021 GraniteApps. All rights reserved.
//

import Foundation
import UIKit

class NavigationViewModel {
    
    let bundle = Utils.bundle(anyClass: NavigationViewModel.self)
    var dataSources: [[(String?, String?, NavigationUnit?, String?, NavigationUnit?)]] = []
    var sectionHeaders: [Int: (String, String?, String?, String?)] = [:]
    var dictTypeIndex: [Announce.AnnonceType: (Int, Int)] = [:]
    var messageText: String?
    
    // Set up GPS information view
    func setUpGPS() {
        var gpsDataSource: [(String?, String?, NavigationUnit?, String?, NavigationUnit?)] = []
        // Data source (0,0)
        gpsDataSource.append(averageCOG())
        dictTypeIndex[.meanCOG] = (0,0)
        // Data source (0,1)
        gpsDataSource.append(averageSpeed())
        dictTypeIndex[.meanSOG] = (0,1)
        // Data source (0,2)
        if LocationManager.shared.maxSpeed10sVariable.value != nil {
            gpsDataSource.append((NSLocalizedString("max_speed_over_ground_10s", bundle: bundle, comment: ""), "\(LocationManager.shared.maxSpeed10sVariable.value!)", Settings.shared.speedUnit?.unit ?? .knots, nil, nil))
        } else {
            gpsDataSource.append((NSLocalizedString("max_speed_over_ground_10s", bundle: bundle, comment: ""), nil, Settings.shared.speedUnit?.unit ?? .knots, nil, nil))
        }
        dictTypeIndex[.maxSOG10s] = (0,2)
        // Data source (0,3)
        gpsDataSource.append(maxSpeed())
        dictTypeIndex[.maxSpeedOverGround] = (0,3)
        // Data source (0,4)
        gpsDataSource.append(gpsPrecision())
        dictTypeIndex[.gpsPrecision] = (0,4)
        
        // Data source (0,5) and (0,6)
        if let coordinate = LocationManager.shared.locationVariable.value?.coordinate {
            let latitude = Float(coordinate.latitude)
            let longitude = Float(coordinate.longitude)
            switch Settings.shared.coordinatesUnit?.unit {
            case .minuteDecimalDegrees:
                let latToUse = "\(abs(latitude.coordinates.0))\(NSLocalizedString("unit_abb_degrees", bundle: bundle, comment: "")) \(abs(latitude.coordinates.1.rounded(toPlaces: 3)))\(NSLocalizedString("unit_abb_minutes", bundle: bundle, comment: ""))"
                let latUnit = latitude.coordinates.0 > 0 ? NavigationUnit.north : NavigationUnit.south
                gpsDataSource.append((NSLocalizedString("latitude", bundle: bundle, comment: ""), latToUse, latUnit, nil, nil))
                let longToUse = "\(abs(longitude.coordinates.0))\(NSLocalizedString("unit_abb_degrees", bundle: bundle, comment: "")) \(abs(longitude.coordinates.1.rounded(toPlaces: 3)))\(NSLocalizedString("unit_abb_minutes", bundle: bundle, comment: ""))"
                let longUnit = longitude.coordinates.0 > 0 ? NavigationUnit.east : NavigationUnit.west
                gpsDataSource.append((NSLocalizedString("longitude", bundle: bundle, comment: ""), longToUse, longUnit, nil, nil))
            case .degreesMinuteSecond:
                let latDMS = latitude.coordinatesDMS
                let longDMS = longitude.coordinatesDMS
                let latToUse = "\(abs(latDMS.0))\(NSLocalizedString("unit_abb_degrees", bundle: bundle, comment: "")) \(abs(latDMS.1))\(NSLocalizedString("unit_abb_minutes", bundle: bundle, comment: "")) \(abs(latDMS.2.rounded(toPlaces: 3)))\(NSLocalizedString("unit_abb_secondes", bundle: bundle, comment: ""))"
                let latUnit = latDMS.0 > 0 ? NavigationUnit.north : NavigationUnit.south
                gpsDataSource.append((NSLocalizedString("latitude", bundle: bundle, comment: ""), latToUse, latUnit, nil, nil))
                let longToUse = "\(abs(longDMS.0))\(NSLocalizedString("unit_abb_degrees", bundle: bundle, comment: "")) \(abs(longDMS.1))\(NSLocalizedString("unit_abb_minutes", bundle: bundle, comment: "")) \(abs(longDMS.2.rounded(toPlaces: 3)))\(NSLocalizedString("unit_abb_secondes", bundle: bundle, comment: ""))"
                let longUnit = longDMS.0 > 0 ? NavigationUnit.east : NavigationUnit.west
                gpsDataSource.append((NSLocalizedString("longitude", bundle: bundle, comment: ""), longToUse, longUnit, nil, nil))
            default:
                let latToUse = "\(abs(latitude.rounded(toPlaces: 4)))\(NSLocalizedString("unit_abb_degrees", bundle: bundle, comment: ""))"
                let latUnit = latitude > 0 ? NavigationUnit.north : NavigationUnit.south
                gpsDataSource.append((NSLocalizedString("latitude", bundle: bundle, comment: ""), latToUse, latUnit, nil, nil))
                let longToUse = "\(abs(longitude.rounded(toPlaces: 4)))\(NSLocalizedString("unit_abb_degrees", bundle: bundle, comment: ""))"
                let longUnit = longitude > 0 ? NavigationUnit.east : NavigationUnit.west
                gpsDataSource.append((NSLocalizedString("longitude", bundle: bundle, comment: ""), longToUse, longUnit, nil, nil))
            }
        } else {
            gpsDataSource.append((NSLocalizedString("latitude", bundle: bundle, comment: ""), nil, .degrees, nil, nil))
            gpsDataSource.append((NSLocalizedString("longitude", bundle: bundle, comment: ""), nil, .degrees, nil, nil))
        }
        dictTypeIndex[.latitude] = (0,5)
        dictTypeIndex[.longitude] = (0,6)
        dataSources.append(gpsDataSource)
    }
    
    public func nextSegment() -> ((Float, NavigationUnit?)?, Int)?{
        if let currentPoint = NavigationManager.shared.point.value, let nextPoint = NavigationManager.shared.getNextPointWithCoord().value {
            // Compute the location of the point given
            var gpsPosition: GPSPosition?
            if let coursePoint = currentPoint.point as? CoursePoint {
                gpsPosition = coursePoint.gpsPosition
            } else if let mark = currentPoint.point as? Mark {
                gpsPosition = mark.middlePoint()
            }
            var gpsNextPosition: GPSPosition?
            if let coursePoint = nextPoint.point as? CoursePoint {
                gpsNextPosition = coursePoint.gpsPosition
            } else if let mark = nextPoint.point as? Mark {
                gpsNextPosition = mark.middlePoint()
            }
            let distance = Utils.tupleDistance(floatDistance: NavigationHelper.getDistanceFrom(gpsPosition1: gpsPosition, to: gpsNextPosition))
            
            let orientation = NavigationHelper.segmentOrientation(position1: gpsPosition, position2: gpsNextPosition)
            return (distance, orientation)
        }
        return nil
    }
    
    // Set up route information view
    func setUpRouteDataSource() {
        messageText = nil
        guard let _ = NavigationManager.shared.route.value else {
            messageText = Utils.localizedString(forKey: "choose_route_or_point", app: "SARANav")
            return
        }
        var currentPointDataSource: [(String?, String?, NavigationUnit?, String?, NavigationUnit?)] = []
        // Data source (0,0)
        currentPointDataSource.append(gisement())
        dictTypeIndex[.gisement] = (0,0)
        
        // Data source (0,1)
        if NavigationManager.shared.azimutToCurrentPoint.value != nil {
            var bearing1: String? = nil
            var bearing2: String? = nil
            if let b1 = NavigationManager.shared.azimutToCurrentPoint.value!.0 {
                bearing1 = "\(b1)"
            }
            if let b2 = NavigationManager.shared.azimutToCurrentPoint.value!.1 {
                bearing2 = "\(b2)"
            }
            currentPointDataSource.append((NSLocalizedString("azimuth", bundle: bundle, comment: ""), bearing1, .degrees, bearing2, bearing2 == nil ? nil : .degrees))
        } else {
            let point = NavigationManager.shared.point.value?.point
            if let mark = point as? Mark, mark.buoys.count == 2 {
                currentPointDataSource.append((NSLocalizedString("azimuth", bundle: bundle, comment: ""), nil, .degrees, nil, .degrees))
            } else {
                currentPointDataSource.append((NSLocalizedString("azimuth", bundle: bundle, comment: ""), nil, .degrees, nil, nil))
            }
        }

        // Data source (0,2)
        currentPointDataSource.append(distance())
        dictTypeIndex[.distance] = (0,2)

        // Data source (0,3)
        currentPointDataSource.append(vmc())
        dictTypeIndex[.CMG] = (0,3)
        
        // Data source (0,4)
        let tupleGap = NavigationManager.shared.gap.value
        if tupleGap != nil {
            if tupleGap!.0!.1 == .meter {
                var d2: String? = nil
                if tupleGap!.1 != nil, tupleGap!.1!.1 == .meter {
                    d2 = "\(tupleGap!.1!.0.roundDown)"
                } else if tupleGap!.1 != nil {
                    d2 = "\(tupleGap!.1!.0)"
                }
                currentPointDataSource.append((NSLocalizedString("gap_segment_route", bundle: bundle, comment: ""), "\(tupleGap!.0!.0.roundDown)", tupleGap!.0!.1 ?? .meter, d2, tupleGap!.1?.1 ?? nil))
            }
            else {
                var d2: String? = nil
                if tupleGap!.1 != nil, tupleGap!.1!.1 == .meter {
                    d2 = "\(tupleGap!.1!.0.roundDown)"
                } else if tupleGap!.1 != nil {
                    d2 = "\(tupleGap!.1!.0)"
                }
                currentPointDataSource.append((NSLocalizedString("gap_segment_route", bundle: bundle, comment: ""), "\(tupleGap!.0!.0)", tupleGap!.0!.1 ?? .meter, d2, tupleGap!.1?.1 ?? nil))
            }
        } else {
            let point = NavigationManager.shared.point.value?.point
            if let mark = point as? Mark, mark.buoys.count == 2 {
                currentPointDataSource.append((NSLocalizedString("gap_segment_route", bundle: bundle, comment: ""), nil, .meter, nil, .meter))
            } else {
                currentPointDataSource.append((NSLocalizedString("gap_segment_route", bundle: bundle, comment: ""), nil, .meter, nil, nil))
            }
        }

        let point = NavigationManager.shared.point.value?.point
        if let name = point?.name {
            if let mark = point as? Mark {
                var mark1toLetAt: String?
                var mark2toLetAt: String?
                if mark.buoys.first?.toLetAt(inversed: (NavigationManager.shared.route.value as? CruiseCourse)?.reversed) == Buoy.ToLetAt.port.rawValue {
                    mark1toLetAt = NSLocalizedString("unit_port", bundle: bundle, comment: "")
                } else {
                    mark1toLetAt = NSLocalizedString("unit_starboard", bundle: bundle, comment: "")
                }
                if mark.markType == Mark.MarkType.double.rawValue {
                    if mark.buoys[1].toLetAt(inversed: (NavigationManager.shared.route.value as? CruiseCourse)?.reversed) == Buoy.ToLetAt.port.rawValue {
                        mark2toLetAt = NSLocalizedString("unit_port", bundle: bundle, comment: "")
                    } else {
                        mark2toLetAt = NSLocalizedString("unit_starboard", bundle: bundle, comment: "")
                    }
                }
                sectionHeaders[0] = ("\(NSLocalizedString("current_point_title", bundle: bundle, comment: "")):\n \(name)", mark1toLetAt, mark2toLetAt, mark.markType)
            } else if let _ = point as? CoursePoint {
                sectionHeaders[0] = ("\(NSLocalizedString("current_point_title", bundle: bundle, comment: "")):\n \(name)", nil, nil, nil)
            }
        } else {
            sectionHeaders[0] = (NSLocalizedString("current_point_title", bundle: bundle, comment: ""), nil, nil, nil)
        }

        dataSources.append(currentPointDataSource)

        if NavigationManager.shared.isNotLastPoint.value {
            var nextSegmentDataSource: [(String?, String?, NavigationUnit?, String?, NavigationUnit?)] = []
            // Data source (1,0)
            if let name_current = NavigationManager.shared.point.value?.point.name, let name_next = NavigationManager.shared.getNextPointWithCoord().value?.point.name {
                nextSegmentDataSource.append(("\(NSLocalizedString("from", bundle: bundle, comment: "")) \(name_current) \(NSLocalizedString("to", bundle: bundle, comment: "")) \(name_next)", "", nil, nil, nil) )
            } else {
                nextSegmentDataSource.append(("\(NSLocalizedString("from", bundle: bundle, comment: "")) - \(NSLocalizedString("to", bundle: bundle, comment: "")) - ", "", nil, nil, nil) )
            }
            // Data source (1,1)
            let nextSegmentInfos = nextSegment()
            let tuple = nextSegmentInfos?.0
            if tuple != nil {
                if tuple!.1 == .meter {
                    nextSegmentDataSource.append((NSLocalizedString("next_segment_length", bundle: bundle, comment: ""), "\(tuple!.0.roundDown)", tuple!.1 ?? .meter, nil, nil))
                } else {
                    nextSegmentDataSource.append((NSLocalizedString("next_segment_length", bundle: bundle, comment: ""), "\(tuple!.0)", tuple!.1 ?? .meter, nil, nil))
                }
            } else {
                nextSegmentDataSource.append((NSLocalizedString("next_segment_length", bundle: bundle, comment: ""), nil, .meter, nil, nil))
            }
            let segmentOrientation = nextSegmentInfos?.1 == nil ? nil : "\(nextSegmentInfos!.1)"
            nextSegmentDataSource.append((NSLocalizedString("next_cardinal_orientation", bundle: bundle, comment: ""), segmentOrientation, .degrees, nil, nil))
            sectionHeaders[1] = (NSLocalizedString("next_segment", bundle: bundle, comment: ""), nil, nil, nil)
            dataSources.append(nextSegmentDataSource)

            var nextPointDataSource: [(String?, String?, NavigationUnit?, String?, NavigationUnit?)] = []
            // Data source (2,0)
            if NavigationManager.shared.gisementNextPoint.value != nil {
                var bearing1: String? = nil
                var bearing2: String? = nil
                if let b1 = NavigationManager.shared.gisementNextPoint.value!.0?.0 {
                    bearing1 = "\(b1)"
                }
                if let b2 = NavigationManager.shared.gisementNextPoint.value!.1?.0 {
                    bearing2 = "\(b2)"
                }
                nextPointDataSource.append((NSLocalizedString("next_relative_bearing", bundle: bundle, comment: ""), bearing1, NavigationManager.shared.gisementNextPoint.value!.0?.1 ?? .spDegrees, bearing2, NavigationManager.shared.gisementNextPoint.value!.0?.1 ?? nil))
            } else {
                let point = NavigationManager.shared.point.value?.point
                if let mark = point as? Mark, mark.buoys.count == 2 {
                    nextPointDataSource.append((NSLocalizedString("next_relative_bearing", bundle: bundle, comment: ""), nil, .spDegrees, nil, .spDegrees))
                } else {
                    nextPointDataSource.append((NSLocalizedString("next_relative_bearing", bundle: bundle, comment: ""), nil, .spDegrees, nil, nil))
                }
            }
            // Data source (2,1)
            if NavigationManager.shared.azimutNextPoint.value != nil {
                var bearing1: String? = nil
                var bearing2: String? = nil
                if let b1 = NavigationManager.shared.azimutNextPoint.value!.0 {
                    bearing1 = "\(b1)"
                }
                if let b2 = NavigationManager.shared.azimutNextPoint.value!.1 {
                    bearing2 = "\(b2)"
                }
                nextPointDataSource.append((NSLocalizedString("next_azimuth", bundle: bundle, comment: ""), bearing1, .degrees, bearing2, bearing2 == nil ? nil : .degrees))
            } else {
                let point = NavigationManager.shared.point.value?.point
                if let mark = point as? Mark, mark.buoys.count == 2 {
                    nextPointDataSource.append((NSLocalizedString("next_azimuth", bundle: bundle, comment: ""), nil, .degrees, nil, .degrees))
                } else {
                    nextPointDataSource.append((NSLocalizedString("next_azimuth", bundle: bundle, comment: ""), nil, .degrees, nil, nil))
                }
            }
            // Data source (2,2)
            let tupleDistanceNextPoint = NavigationManager.shared.distanceToNextPoint.value
            if tupleDistanceNextPoint != nil {
                if tupleDistanceNextPoint!.0 != nil && tupleDistanceNextPoint!.0!.1 == .meter {
                    var d2: String? = nil
                    if tupleDistanceNextPoint!.1 != nil, tupleDistanceNextPoint!.1!.1 == .meter {
                        d2 = "\(tupleDistanceNextPoint!.1!.0.roundDown)"
                    } else if tupleDistanceNextPoint!.1 != nil {
                        d2 = "\(tupleDistanceNextPoint!.1!.0)"
                    }
                    nextPointDataSource.append((NSLocalizedString("next_distance", bundle: bundle, comment: ""), "\(tupleDistanceNextPoint!.0!.0.roundDown)", tupleDistanceNextPoint!.0?.1 ?? .meter, d2, tupleDistanceNextPoint!.1?.1 ?? nil))
                }
                else {
                    var d2: String? = nil
                    if tupleDistanceNextPoint!.1 != nil, tupleDistanceNextPoint!.1!.1 == .meter {
                       d2 = "\(tupleDistanceNextPoint!.1!.0.roundDown)"
                    } else if tupleDistanceNextPoint!.1 != nil {
                       d2 = "\(tupleDistanceNextPoint!.1!.0)"
                    }
                    nextPointDataSource.append((NSLocalizedString("next_distance", bundle: bundle, comment: ""), "\(tupleDistanceNextPoint!.0!.0)", tupleDistanceNextPoint!.0?.1 ?? .meter, d2, tupleDistanceNextPoint!.1?.1 ?? nil))
                }
            } else {
                let point = NavigationManager.shared.point.value?.point
                if let mark = point as? Mark, mark.buoys.count == 2 {
                    nextPointDataSource.append((NSLocalizedString("next_distance", bundle: bundle, comment: ""), nil, .meter, nil, .meter))
                } else {
                    nextPointDataSource.append((NSLocalizedString("next_distance", bundle: bundle, comment: ""), nil, .meter, nil, nil))
                }
            }
            let nextPoint = NavigationManager.shared.getNextPointWithCoord().value?.point
            if let name = nextPoint?.name {
                if let mark = nextPoint as? Mark {
                    var mark1toLetAt: String?
                    var mark2toLetAt: String?
                    if mark.buoys.first?.toLetAt(inversed: (NavigationManager.shared.route.value as? CruiseCourse)?.reversed) == Buoy.ToLetAt.port.rawValue {
                        mark1toLetAt = NSLocalizedString("unit_port", bundle: bundle, comment: "")
                    } else {
                        mark1toLetAt = NSLocalizedString("unit_starboard", bundle: bundle, comment: "")
                    }
                    if mark.markType == Mark.MarkType.double.rawValue {
                        if mark.buoys[1].toLetAt(inversed: (NavigationManager.shared.route.value as? CruiseCourse)?.reversed) == Buoy.ToLetAt.port.rawValue {
                            mark2toLetAt = NSLocalizedString("unit_port", bundle: bundle, comment: "")
                        } else {
                            mark2toLetAt = NSLocalizedString("unit_starboard", bundle: bundle, comment: "")
                        }
                    }
                    sectionHeaders[2] = ("\(NSLocalizedString("next_point_title", bundle: bundle, comment: "")):\n \(name)", mark1toLetAt, mark2toLetAt, mark.markType)
                } else if let _ = nextPoint as? CoursePoint {
                    sectionHeaders[2] = ("\(NSLocalizedString("next_point_title", bundle: bundle, comment: "")): \(name)", nil, nil, nil)
                }
            } else {
                sectionHeaders[2] = (NSLocalizedString("next_point_title", bundle: bundle, comment: ""), nil, nil, nil)
            }
            dataSources.append(nextPointDataSource)
        }
    }
    
    // Set up navigation center information view
    func setUpNavigationCenter() {
        messageText = nil
        if NmeaManager.default.currentSSID.value == nil {
            messageText = "WIFI " + NSLocalizedString("not_connected", bundle: bundle, comment: "")
            return
        }
        if NmeaManager.default.connected.value == false {
            messageText = NSLocalizedString("no_data_available", bundle: bundle, comment: "")
            return
        }
//        self.type = .navigationCenter
        self.dataSources = []
        
        // Data source (0,0)
        var navigationCenterDataSource: [(String?, String?, NavigationUnit?, String?, NavigationUnit?)] = []
        if NmeaManager.default.depthMeters.value != nil {
            navigationCenterDataSource.append((NSLocalizedString("depth", bundle: bundle, comment: ""), "\(NmeaManager.default.depthMeters.value!)", .meter, nil, nil))
        } else {
            navigationCenterDataSource.append((NSLocalizedString("depth", bundle: bundle, comment: ""), nil, .meter, nil, nil))
        }
        
        // Data source (0,1)
        if NmeaManager.default.headingMagneticDegrees.value != nil {
            navigationCenterDataSource.append((NSLocalizedString("surface_heading", bundle: bundle, comment: ""), "\(Int(NmeaManager.default.headingMagneticDegrees.value!))", .degrees, nil, nil))
        } else {
            navigationCenterDataSource.append((NSLocalizedString("surface_heading", bundle: bundle, comment: ""), nil, .degrees, nil, nil))
        }
        
        // Data source (0,2) and (0,3)
        if Settings.shared.speedUnit?.unit == NavigationUnit.kmPerHour {
            let stwKmPerHour = NmeaManager.default.speedThroughWaterKmPerHour.value != nil ? "\(NmeaManager.default.speedThroughWaterKmPerHour.value!)" : nil
            navigationCenterDataSource.append((NSLocalizedString("speed_through_water", bundle: bundle, comment: ""), stwKmPerHour, .kmPerHour, nil, nil))
            let maxSpeed10sKmPerHour = NmeaManager.default.maxSpeedThroughWater10sKmPerHour.value != nil ? "\(NmeaManager.default.maxSpeedThroughWater10sKmPerHour.value!)" : nil
            navigationCenterDataSource.append((NSLocalizedString("speed_through_water_10s", bundle: bundle, comment: ""), maxSpeed10sKmPerHour, .kmPerHour, nil, nil))
            let maxWaterSpeedKmPerHour = NmeaManager.default.maxSpeedThroughWaterKmPerHour.value != nil ? "\(NmeaManager.default.maxSpeedThroughWaterKmPerHour.value!)" : nil
            navigationCenterDataSource.append((NSLocalizedString("speed_through_water_max", bundle: bundle, comment: ""), maxWaterSpeedKmPerHour, .kmPerHour, nil, nil))
        }
        else {
            let stwKnots = NmeaManager.default.speedThroughWaterKnots.value != nil ? "\(NmeaManager.default.speedThroughWaterKnots.value!)" : nil
            navigationCenterDataSource.append((NSLocalizedString("speed_through_water", bundle: bundle, comment: ""), stwKnots, .knots, nil, nil))
            let maxSpeed10sKnots = NmeaManager.default.maxSpeedThroughWater10sKnots.value != nil ? "\(NmeaManager.default.maxSpeedThroughWater10sKnots.value!)" : nil
            navigationCenterDataSource.append((NSLocalizedString("speed_through_water_10s", bundle: bundle, comment: ""), maxSpeed10sKnots, .knots, nil, nil))
            let maxWaterSpeedKnots = NmeaManager.default.maxSpeedThroughWaterKnots.value != nil ? "\(NmeaManager.default.maxSpeedThroughWaterKnots.value!)" : nil
            navigationCenterDataSource.append((NSLocalizedString("speed_through_water_max", bundle: bundle, comment: ""), maxWaterSpeedKnots, .knots, nil, nil))
        }
        self.dataSources.append(navigationCenterDataSource)
        self.sectionHeaders[0] = (NSLocalizedString("instrument", bundle: bundle, comment: ""), nil, nil, nil)

        var trueWindDataSource: [(String?, String?, NavigationUnit?, String?, NavigationUnit?)] = []
        // Data source (1,0)
        if NmeaManager.default.windDirection.value != nil {
            trueWindDataSource.append((NSLocalizedString("wind_direction", bundle: bundle, comment: ""), "\(Int(NmeaManager.default.windDirection.value!))", .degrees, nil, nil))
        } else {
            trueWindDataSource.append((NSLocalizedString("wind_direction", bundle: bundle, comment: ""), nil, .degrees, nil, nil))
        }
        
        // Data source (1,1)
        if Settings.shared.speedUnit?.unit == .kmPerHour {
            if NmeaManager.default.windTrueSpeedKmPerHour.value != nil {
                trueWindDataSource.append((NSLocalizedString("true_wind_speed", bundle: bundle, comment: ""), "\(NmeaManager.default.windTrueSpeedKmPerHour.value!)", .kmPerHour, nil, nil))
            } else {
                trueWindDataSource.append((NSLocalizedString("true_wind_speed", bundle: bundle, comment: ""), nil, .kmPerHour, nil, nil))
            }
        } else {
            if NmeaManager.default.windTrueSpeedKnots.value != nil {
                trueWindDataSource.append((NSLocalizedString("true_wind_speed", bundle: bundle, comment: ""), "\(NmeaManager.default.windTrueSpeedKnots.value!)", .knots, nil, nil))
            } else {
                trueWindDataSource.append((NSLocalizedString("true_wind_speed", bundle: bundle, comment: ""), nil, .knots, nil, nil))
            }
        }
        // Data source (1,2)
        if NmeaManager.default.windTrueAngleDegrees.value != nil {
            trueWindDataSource.append((NSLocalizedString("true_wind_angle", bundle: bundle, comment: ""), "\(Int(NmeaManager.default.windTrueAngleDegrees.value!.0))", NmeaManager.default.windTrueAngleDegrees.value!.1, nil, nil))
        } else {
            trueWindDataSource.append((NSLocalizedString("true_wind_angle", bundle: bundle, comment: ""), nil, nil, nil, nil))
        }
        
        // Data source (1,3) and (1,4)
        if Settings.shared.speedUnit?.unit == .kmPerHour {
            let maxSpeedWind10sKmPerHour = NmeaManager.default.maxSpeedWind10sKmPerHour.value != nil ? "\(NmeaManager.default.maxSpeedWind10sKmPerHour.value!)" : nil
            trueWindDataSource.append((NSLocalizedString("true_max_wind_speed_10s", bundle: bundle, comment: ""), maxSpeedWind10sKmPerHour, .kmPerHour, nil, nil))
            let maxSpeedWindKmPerHour = NmeaManager.default.maxSpeedWindKmPerHour.value != nil ? "\(NmeaManager.default.maxSpeedWindKmPerHour.value!)" : nil
            trueWindDataSource.append((NSLocalizedString("true_max_wind_speed", bundle: bundle, comment: ""), maxSpeedWindKmPerHour, .kmPerHour, nil, nil))
        } else {
            let maxSpeedWind10sKnots = NmeaManager.default.maxSpeedWind10sKnots.value != nil ? "\(NmeaManager.default.maxSpeedWind10sKnots.value!)" : nil
            trueWindDataSource.append((NSLocalizedString("true_max_wind_speed_10s", bundle: bundle, comment: ""), maxSpeedWind10sKnots, .knots, nil, nil))
            let maxSpeedWindKnots = NmeaManager.default.maxSpeedWindKnots.value != nil ? "\(NmeaManager.default.maxSpeedWindKnots.value!)" : nil
            trueWindDataSource.append((NSLocalizedString("true_max_wind_speed", bundle: bundle, comment: ""), maxSpeedWindKnots, .knots, nil, nil))
        }
        self.dataSources.append(trueWindDataSource)
        self.sectionHeaders[1] = (NSLocalizedString("true_wind", bundle: bundle, comment: ""), nil, nil, nil)
        
        var apparentWindDataSource: [(String?, String?, NavigationUnit?, String?, NavigationUnit?)] = []
        // Data source (2,0)
        if Settings.shared.speedUnit?.unit == .kmPerHour {
            let windRelativeSpeedKmPerHour = NmeaManager.default.windRelativeSpeedKmPerHour.value != nil ? "\(NmeaManager.default.windRelativeSpeedKmPerHour.value!)" : nil
            apparentWindDataSource.append((NSLocalizedString("apparent_wind_speed", bundle: bundle, comment: ""), windRelativeSpeedKmPerHour, .kmPerHour, nil, nil))
        }
        else {
            let windRelativeSpeedKnots = NmeaManager.default.windRelativeSpeedKnots.value != nil ? "\(NmeaManager.default.windRelativeSpeedKnots.value!)" : nil
            apparentWindDataSource.append((NSLocalizedString("apparent_wind_speed", bundle: bundle, comment: ""), windRelativeSpeedKnots, .knots, nil, nil))
        }
        // Data source (2,1)
        if NmeaManager.default.windRelativeAngleDegrees.value != nil {
            apparentWindDataSource.append((NSLocalizedString("apparent_wind_angle", bundle: bundle, comment: ""), "\(Int(NmeaManager.default.windRelativeAngleDegrees.value!.0))", NmeaManager.default.windRelativeAngleDegrees.value!.1, nil, nil))
        } else {
            apparentWindDataSource.append((NSLocalizedString("apparent_wind_angle", bundle: bundle, comment: ""), nil, nil, nil, nil))
        }
        
        self.dataSources.append(apparentWindDataSource)
        self.sectionHeaders[2] = (NSLocalizedString("apparent_wind", bundle: bundle, comment: ""), nil, nil, nil)
        
        var temperatureDataSource: [(String?, String?, NavigationUnit?, String?, NavigationUnit?)] = []
        // Data source (3,0)
        if NmeaManager.default.waterTemperatureCelcius.value != nil {
            temperatureDataSource.append((NSLocalizedString("water_temperature", bundle: bundle, comment: ""), "\(NmeaManager.default.waterTemperatureCelcius.value!)", .degrees, nil, nil))
        } else {
            temperatureDataSource.append((NSLocalizedString("water_temperature", bundle: bundle, comment: ""), nil, .degrees, nil, nil))
        }
        // Data source (3,1)
        let air = NmeaManager.default.airTemperatureCelcius.value != nil ? "\(NmeaManager.default.airTemperatureCelcius.value!)" : nil
        temperatureDataSource.append((NSLocalizedString("air_temperature", bundle: bundle, comment: ""), air, .degrees, nil, nil))
        
        self.dataSources.append(temperatureDataSource)
        self.sectionHeaders[3] = (NSLocalizedString("temperature", bundle: bundle, comment: ""), nil, nil, nil)
    }
    
    // Set up telltales information view
    func setUpTelltales() {
        self.dataSources = []
        self.sectionHeaders = [:]
        self.messageText = nil
        
        if NmeaManager.default.connected.value == false || TellTalesManager.default.tellTales.isEmpty || self.dataSources.isEmpty {
            messageText = NSLocalizedString("no_data_available", bundle: bundle, comment: "")
            return
        }
    }
    
    // Set up geographic information view
    func setUpGeographics() {
        self.messageText = nil
        self.dataSources = []
        self.sectionHeaders = [:]
        
        if DownloadedZone.all().count == 0 {
            messageText = NSLocalizedString("no_download_data", bundle: bundle, comment: "")
            return
        }

        self.dataSources.append([(nil, nil, nil, nil, nil)])
        self.sectionHeaders[0] = (NSLocalizedString("infront", bundle: bundle, comment: ""), nil, nil, nil)
        self.dataSources.append([(nil, nil, nil, nil, nil)])
        self.sectionHeaders[1] = (NSLocalizedString("behind", bundle: bundle, comment: ""), nil, nil, nil)
    }
    
    func setUpStatistic() {
        var statDataSource: [(String?, String?, NavigationUnit?, String?, NavigationUnit?)] = []
        statDataSource.append(averageCOG())
        dictTypeIndex[.meanCOG] = (0,0)
        statDataSource.append(averageSpeed())
        dictTypeIndex[.meanSOG] = (0,1)
        statDataSource.append(maxSpeed())
        dictTypeIndex[.maxSpeedOverGround] = (0,2)
        if NavigationManager.shared.route.value == nil { // Add gps precision on statistic page if no course
            statDataSource.append(gpsPrecision())
            dictTypeIndex[.gpsPrecision] = (0,3)
        }
        self.dataSources.append(statDataSource)
    }
    
    func setUpCurrent() {
        messageText = nil
        guard let _ = NavigationManager.shared.route.value else {
            messageText = NSLocalizedString(Utils.localizedString(forKey: "choose_courses_list", app: "SARARegate"), comment: "")
            return
        }
        var currentPointDataSource: [(String?, String?, NavigationUnit?, String?, NavigationUnit?)] = []
        currentPointDataSource.append(gisement())
        dictTypeIndex[.gisement] = (0,0)
        currentPointDataSource.append(distance())
        dictTypeIndex[.distance] = (0,1)
        currentPointDataSource.append(vmc())
        dictTypeIndex[.CMG] = (0,2)
        if NavigationManager.shared.route.value != nil { // Add gps precision on current page if there is a course
            currentPointDataSource.append(gpsPrecision())
            dictTypeIndex[.gpsPrecision] = (0,3)
        }
        let point = NavigationManager.shared.point.value?.point
        if let name = point?.name {
            if let mark = point as? Mark {
                var mark1toLetAt: String?
                var mark2toLetAt: String?
                if let route = NavigationManager.shared.route.value, route.isMatchRace(), mark.buoys.count > 1 {
                    let buoyName1 = mark.buoys[0].name.split(separator: " ")
                    let buoyName2 = mark.buoys[1].name.split(separator: " ")
                    mark1toLetAt = "\(buoyName1[0])\n\(buoyName1[1])"
                    mark2toLetAt = "\(buoyName2[0])\n\(buoyName2[1])"
                } else {
                    if mark.buoys.first?.toLetAt() == Buoy.ToLetAt.port.rawValue {
                        mark1toLetAt = NSLocalizedString("unit_port", bundle: bundle, comment: "")
                    } else {
                        mark1toLetAt = NSLocalizedString("unit_starboard", bundle: bundle, comment: "")
                    }
                    if mark.buoys.count > 1 { // double, start, end
                        if mark.buoys[1].toLetAt() == Buoy.ToLetAt.port.rawValue {
                            mark2toLetAt = "\(NSLocalizedString("unit_port", bundle: bundle, comment: ""))"
                        } else {
                            mark2toLetAt = "\(NSLocalizedString("unit_starboard", bundle: bundle, comment: ""))"
                        }
                        // For start and end mark add name of the buoy (pin / commitee)
                        let buoyName1 = mark.buoys[0].name.split(separator: " ").first(where: { $0 != NSLocalizedString("start", bundle: bundle, comment: "") && $0 != NSLocalizedString("end", bundle: bundle, comment: "")})
                        let buoyName2 = mark.buoys[1].name.split(separator: " ").first(where: { $0 != NSLocalizedString("start", bundle: bundle, comment: "") && $0 != NSLocalizedString("end", bundle: bundle, comment: "")})
                        mark1toLetAt = mark.markType != Mark.MarkType.double.rawValue && buoyName1 != nil  ? "\(buoyName1!)\n\(mark1toLetAt!)" : mark1toLetAt
                        mark2toLetAt = mark.markType != Mark.MarkType.double.rawValue && buoyName2 != nil ? "\(buoyName2!)\n\(mark2toLetAt!)" : mark2toLetAt
                    }
                }
                sectionHeaders[0] = (name, mark1toLetAt, mark2toLetAt, mark.markType)
            }
        } else {
            sectionHeaders[0] = ("", nil, nil, nil)
        }
        self.dataSources.append(currentPointDataSource)
        
        if NavigationManager.shared.isNotLastPoint.value {
            var nextSegmentDataSource: [(String?, String?, NavigationUnit?, String?, NavigationUnit?)] = []
            // Data source (1,0)
            if let name_current = NavigationManager.shared.point.value?.point.name, let name_next = NavigationManager.shared.getNextPointWithCoord().value?.point.name {
                nextSegmentDataSource.append(("\(NSLocalizedString("from", bundle: bundle, comment: "")) \(name_current) \(NSLocalizedString("to", bundle: bundle, comment: "")) \(name_next)", "", nil, nil, nil) )
            } else {
                nextSegmentDataSource.append(("\(NSLocalizedString("from", bundle: bundle, comment: "")) - \(NSLocalizedString("to", bundle: bundle, comment: "")) - ", "", nil, nil, nil) )
            }
            // Data source (1,1)
            let nextSegmentInfos = nextSegment()
            let tuple = nextSegmentInfos?.0
            if tuple != nil {
                if tuple!.1 == .meter {
                    nextSegmentDataSource.append((NSLocalizedString("next_segment_length", bundle: bundle, comment: ""), "\(tuple!.0.roundDown)", tuple!.1 ?? .meter, nil, nil))
                } else {
                    nextSegmentDataSource.append((NSLocalizedString("next_segment_length", bundle: bundle, comment: ""), "\(tuple!.0)", tuple!.1 ?? .meter, nil, nil))
                }
            } else {
                nextSegmentDataSource.append((NSLocalizedString("next_segment_length", bundle: bundle, comment: ""), nil, .meter, nil, nil))
            }
            let segmentOrientation = nextSegmentInfos?.1 == nil ? nil : "\(nextSegmentInfos!.1)"
            nextSegmentDataSource.append((NSLocalizedString("next_cardinal_orientation", bundle: bundle, comment: ""), segmentOrientation, .degrees, nil, nil))
            sectionHeaders[1] = (NSLocalizedString("next_segment", bundle: bundle, comment: ""), nil, nil, nil)
            dataSources.append(nextSegmentDataSource)
        }
    }
    
    // MARK: - Helper funtions
    func averageCOG() -> (String?, String?, NavigationUnit?, String?, NavigationUnit?) {
        if LocationManager.shared.averageCourseVariable.value != nil {
            return (NSLocalizedString("average_course_over_ground", bundle: bundle, comment: ""), "\(LocationManager.shared.averageCourseVariable.value!)", .degrees, nil, nil)
        } else {
            return (NSLocalizedString("average_course_over_ground", bundle: bundle, comment: ""), nil, .degrees, nil, nil)
        }
    }
    
    func averageSpeed() -> (String?, String?, NavigationUnit?, String?, NavigationUnit?) {
        if LocationManager.shared.averageSpeedVariable.value != nil {
            return(NSLocalizedString("average_speed_over_ground", bundle: bundle, comment: ""), "\(LocationManager.shared.averageSpeedVariable.value!)", Settings.shared.speedUnit?.unit ?? .knots, nil, nil)
        } else {
            return (NSLocalizedString("average_speed_over_ground", bundle: bundle, comment: ""), nil, Settings.shared.speedUnit?.unit ?? .knots, nil, nil)
        }
    }
    
    func maxSpeed() -> (String?, String?, NavigationUnit?, String?, NavigationUnit?) {
        if LocationManager.shared.maxSpeedVariable.value != nil {
            return (NSLocalizedString("max_speed_over_ground", bundle: bundle, comment: ""), "\(LocationManager.shared.maxSpeedVariable.value!)", Settings.shared.speedUnit?.unit ?? .knots, nil, nil)
        } else {
            return (NSLocalizedString("max_speed_over_ground", bundle: bundle, comment: ""), nil, Settings.shared.speedUnit?.unit ?? .knots, nil, nil)
        }
    }
    
    func gpsPrecision() -> (String?, String?, NavigationUnit?, String?, NavigationUnit?) {
        if LocationManager.shared.accuracyVariable.value != nil {
            return (NSLocalizedString("gps_precision", bundle: bundle, comment: ""), "\(LocationManager.shared.accuracyVariable.value!)", .meter, nil, nil)
        } else {
            return (NSLocalizedString("gps_precision", bundle: bundle, comment: ""), nil, .meter, nil, nil)
        }
    }
    
    func gisement() -> (String?, String?, NavigationUnit?, String?, NavigationUnit?) {
        if NavigationManager.shared.gisementToCurrentPoint.value != nil {
            var bearing1: String? = nil
            var bearing2: String? = nil
            if let b1 = NavigationManager.shared.gisementToCurrentPoint.value!.0?.0 {
                bearing1 = "\(b1)"
            }
            if let b2 = NavigationManager.shared.gisementToCurrentPoint.value!.1?.0 {
                bearing2 = "\(b2)"
            }
            return (NSLocalizedString("relative_bearing", bundle: bundle, comment: ""), bearing1, NavigationManager.shared.gisementToCurrentPoint.value!.0?.1 ?? .spDegrees, bearing2, NavigationManager.shared.gisementToCurrentPoint.value!.1?.1 ?? .spDegrees)
        } else {
            let point = NavigationManager.shared.point.value?.point
            if let mark = point as? Mark, mark.buoys.count == 2 {
                return (NSLocalizedString("relative_bearing", bundle: bundle, comment: ""), nil, .spDegrees, nil, .spDegrees)
            } else {
                return (NSLocalizedString("relative_bearing", bundle: bundle, comment: ""), nil, .spDegrees, nil, nil)
            }
        }
    }
    
    func distance() -> (String?, String?, NavigationUnit?, String?, NavigationUnit?) {
        let tupleDistance = NavigationManager.shared.distanceToCurrentPoint.value
        if tupleDistance != nil {
            if tupleDistance!.0 != nil && tupleDistance!.0!.1 == .meter {
                var d2: String? = nil
                if tupleDistance!.1 != nil, tupleDistance!.1!.1 == .meter {
                    d2 = "\(tupleDistance!.1!.0.roundDown)"
                } else if tupleDistance!.1 != nil {
                    d2 = "\(tupleDistance!.1!.0)"
                }
                return (NSLocalizedString("distance", bundle: bundle, comment: ""), "\(tupleDistance!.0!.0.roundDown)", tupleDistance!.0!.1 ?? .meter, d2, tupleDistance!.1?.1 ?? nil)
            }
            else {
                var d2: String? = nil
                if tupleDistance!.1 != nil, tupleDistance!.1!.1 == .meter {
                    d2 = "\(tupleDistance!.1!.0.roundDown)"
                } else if tupleDistance!.1 != nil {
                    d2 = "\(tupleDistance!.1!.0)"
                }
                return (NSLocalizedString("distance", bundle: bundle, comment: ""), "\(tupleDistance!.0!.0)", tupleDistance!.0!.1 ?? .meter, d2, tupleDistance!.1?.1 ?? nil)
            }
        } else { // No distance
            let point = NavigationManager.shared.point.value?.point
            if let mark = point as? Mark, mark.buoys.count == 2 { // Double mark
                return (NSLocalizedString("distance", bundle: bundle, comment: ""), nil, .meter, nil, .meter)
            } else {
                return (NSLocalizedString("distance", bundle: bundle, comment: ""), nil, .meter, nil, nil)
            }
        }
    }
    
    func vmc() -> (String?, String?, NavigationUnit?, String?, NavigationUnit?) {
        if NavigationManager.shared.cmgToCurrentPoint.value != nil {
            var vmc1: String? = nil
            var vmc2: String? = nil
            if let c1 = NavigationManager.shared.cmgToCurrentPoint.value!.0 {
                vmc1 = "\(c1)"
            }
            if let c2 = NavigationManager.shared.cmgToCurrentPoint.value!.1 {
                vmc2 = "\(c2)"
            }
            return (NSLocalizedString("CMG", bundle: bundle, comment: ""), vmc1, Settings.shared.speedUnit?.unit ?? .knots, vmc2, Settings.shared.speedUnit?.unit ?? nil)
        } else {
            let point = NavigationManager.shared.point.value?.point
            if let mark = point as? Mark, mark.buoys.count == 2 {
                return (NSLocalizedString("CMG", bundle: bundle, comment: ""), nil, Settings.shared.speedUnit?.unit ?? .knots, nil, Settings.shared.speedUnit?.unit ?? .knots)
            } else {
                return (NSLocalizedString("CMG", bundle: bundle, comment: ""), nil, Settings.shared.speedUnit?.unit ?? .knots, nil, nil)
            }
        }
    }
}
