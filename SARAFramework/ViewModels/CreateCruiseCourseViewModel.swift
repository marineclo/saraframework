//
//  CreateCruiseCourseViewModel.swift
//  SARAFramework
//
//  Created by Marine on 17.04.21.
//

import Foundation
import RealmSwift

public class CreateCruiseCourseViewModel: CreateCourseViewModel {
    public var route: CruiseCourse
    private var _reversedRoute: Bool
    public var reversedRoute: Bool {
        get {
            return _reversedRoute
        }
        set(newValue) {
            _reversedRoute = newValue
        }
    }
    
    public init(route: CruiseCourse?) {
        if let safeRoute = route {
            self.route = safeRoute
            self._reversedRoute = safeRoute.reversed
            super.init(points: Array(safeRoute.points.map({$0.point})))
        } else {
            self.route = CruiseCourse()
            self._reversedRoute = false
            super.init(points: [])
        }
    }
    
    public func getCourseName() -> String? {
        return self.route.name.isEmpty ? nil : self.route.name
    }
    
    public func saveRoute(name: String) {
        let realm = try! Realm()
        try! realm.write {
            self.route.points.removeAll()
            self.getTempPoints().forEach({
                if let point = realm.object(ofType: AnyPoint.self, forPrimaryKey: $0.id) {
                    self.route.points.append(point)
                }
            })
            self.route.name = name
            if self.route.id == "" {
                self.route.id = UUID().uuidString
            }
            self.route.reversed = self.reversedRoute
            realm.add(self.route)
            realm.add(AnyCourse(self.route))
        }
    }
    
    public func updateRoute(name: String) {
        if self.route.name != name { // The user changes the name. Delete route and create a new one with a new ID.
            self.route.removeCourse()
            self.route = CruiseCourse()
            self.saveRoute(name: name)
        } else {
            let realm = try! Realm()
            try! realm.write {
                self.route.points.removeAll()
                self.getTempPoints().forEach({
                    if let point = realm.object(ofType: AnyPoint.self, forPrimaryKey: $0.id) {
                        self.route.points.append(point)
                    }
                })
                self.route.name = name
                self.route.reversed = self.reversedRoute
                realm.add(self.route, update: .modified)
            }
        }
    }
    
    // Check if modifications should be saved on Firebase
    public func saveOnFirebase(update: Bool = false) {
        if self.route.status == .shared {
            if User.shared.email != nil {
                FirebaseManager.shared.saveCourse(course: self.route, update: update, successHandler: {
//                    successHandler()
                }, errorHandler: { (error) in
//                    errorHandler(error)
                })
            }
        }
    }
}
