//
//  CreateRouteViewModel.swift
//  SARA Croisiere
//
//  Created by Rose on 09/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import RxSwift
import RxCocoa

public class CreateCourseViewModel {
//    public var route: CruiseCourse
    public var temporaryPointsB: BehaviorRelay<[PointRealm]> // Used for SARA Race, for Rx table view
    private var temporaryPoints: [PointRealm]
    public var pointsWithCoord: [PointRealm]?
    private var _status: CourseStatus = .none
    public var status: CourseStatus {
        get {
            return _status
        }
        set(newValue) {
            _status = newValue
        }
    }
    
    private var _currentPointIndex: Int?
    public var currentPointIndex: Int? {
        get {
            return _currentPointIndex
        }
        set(newCurrentIndex) {
            _currentPointIndex = newCurrentIndex
        }
    }
        
    init(points: [PointRealm]) {
        self.temporaryPoints = points
        temporaryPointsB = BehaviorRelay<[PointRealm]>(value: points)
    }
    
    @discardableResult
    public func addToRoute(_ point: PointRealm, at index: IndexPath? = nil) -> Bool {
        if let safeIndex = index?.row {
            self.temporaryPoints.insert(point, at: safeIndex)
            self.temporaryPointsB.accept(self.temporaryPoints)
        } else {
            if self.temporaryPoints.last?.id != point.id {
                self.temporaryPoints.append(point)
                self.temporaryPointsB.accept(self.temporaryPoints)
            } else {
                return false
            }
        }
        return true
    }
    
    public func removeFromRoute(at index: IndexPath) {
        if self.temporaryPoints.count > index.row {
            self.temporaryPoints.remove(at: index.row)
            self.temporaryPointsB.accept(self.temporaryPoints)
        }
    }
    
    public func getPointNameAtIndexPath(_ indexPath: IndexPath) -> String? {
        return self.getPointAtIndexPath(indexPath)?.name
    }
    
    public func getPointAtIndexPath(_ indexPath: IndexPath) -> PointRealm? {
        if self.temporaryPoints.count > indexPath.row {
            return self.temporaryPoints[indexPath.row]
        }
        return nil
    }
    
    public func getPointWithCoord(_ index: Int) -> PointRealm? {
        guard index >= 0 else { return nil }
        var indexToUsed = index
        while indexToUsed < self.temporaryPoints.count {
            if self.temporaryPoints[indexToUsed].getCoordinates(method: .middle) != nil {
                return self.temporaryPoints[indexToUsed]
            }
            indexToUsed += 1
        }
        return nil
    }
    
    public func getPointsWithCoord() {
        self.pointsWithCoord = []
        self.temporaryPoints.forEach({
            if $0.getCoordinates(method: .middle) != nil {
                self.pointsWithCoord?.append($0)
            }
        })
    }
    
    public func allPointsHasCoordinates() -> Bool {
        getPointsWithCoord()
        if self.temporaryPoints.count == self.pointsWithCoord?.count {
            return true
        }
        return false
    }

    public func numberOfPoints() -> Int? {
        return self.temporaryPoints.count
    }
    
//    public func getCourseName() -> String? {
//        return self.route.name.isEmpty ? nil : self.route.name
//    }
//
//    public func saveRoute(name: String) {
//        let realm = try! Realm()
//        try! realm.write {
//            self.route.points.removeAll()
//            self.temporaryPoints.forEach({
//                if let point = realm.object(ofType: AnyPoint.self, forPrimaryKey: $0.id) {
//                    self.route.points.append(point)
//                }
//            })
//            self.route.name = name
//            if self.route.id == "" {
//                self.route.id = UUID().uuidString
//            }
//            self.route.reversed = self.reversedRoute
//            realm.add(self.route)
//            realm.add(AnyCourse(self.route))
//        }
//        self.saveOnFirebase()
//    }
//
//    public func updateRoute(name: String) {
//        let realm = try! Realm()
//        try! realm.write {
//            self.route.points.removeAll()
//            self.temporaryPoints.forEach({
//                if let point = realm.object(ofType: AnyPoint.self, forPrimaryKey: $0.id) {
//                    self.route.points.append(point)
//                }
//            })
//            self.route.name = name
//            self.route.reversed = self.reversedRoute
//            realm.add(self.route, update: .modified)
//        }
//        self.saveOnFirebase()
//    }
//
//    // Check if modifications should be saved on Firebase
//    public func saveOnFirebase() {
//        if self.route.shared {
//            if User.shared.email != nil {
//                FirebaseManager.shared.saveCourse(course: self.route)
//            }
//        }
//    }

    public func reversePoints() {
        self.temporaryPoints.reverse()
    }
    
    public func getTempPoints() -> [PointRealm] {
        return self.temporaryPoints
    }
    
    public func setTempPoints(points: [PointRealm]) {
        self.temporaryPoints = points
    }
    
    // Check that there is not same point consecutively when clicking on validate
    public func consecutivelyPoints() -> Bool{
        var previousPoint: PointRealm?
        for point in temporaryPoints {
            if previousPoint != nil && point.id == previousPoint!.id {
                return true
            }
            previousPoint = point
        }
        return false
    }
}
