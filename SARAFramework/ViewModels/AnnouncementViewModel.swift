//
//  AnnouncementViewModel.swift
//  SARA Croisiere
//
//  Created by Rose on 11/11/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

public class AnnouncementViewModel {
    public enum SettingsType {
        case unknown, voice, gps, route, navigationCenter, telltales, batteryLevel, carto
    }
    var profile: Profile?
}

extension Profile {
        
    public func announcementParameters(profileSettingsType type: AnnouncementViewModel.SettingsType) -> [AnnounceParameter] {
        let bundle = Utils.bundle(anyClass: Profile.self)
        switch type {
        case .gps:
            guard let pGPS = self.profileTypes.first(where: { $0 is ProfileGPS}) as? ProfileGPS else {
                return []
            }
            var annoncesGPS = [AnnounceParameter]()
            if pGPS.compass != nil {
                annoncesGPS.append(AnnounceParameter(name: NSLocalizedString("compass", bundle: bundle, comment: ""), annonceType: .compass, settingVariable: pGPS.compass!))
            }
            if pGPS.courseOverGround != nil {
                annoncesGPS.append(AnnounceParameter(name: NSLocalizedString("course_over_ground", bundle: bundle, comment: ""), annonceType: .courseOverGround, settingVariable: pGPS.courseOverGround!))
            }
            if pGPS.speedOverGround != nil {
                annoncesGPS.append(AnnounceParameter(name: NSLocalizedString("speed_over_ground", bundle: bundle, comment: ""), annonceType: .speedOverGround, settingVariable: pGPS.speedOverGround!))
            }
            if pGPS.maxSpeedOverGround != nil {
                annoncesGPS.append(AnnounceParameter(name: NSLocalizedString("max_speed_over_ground", bundle: bundle, comment: ""), annonceType: .maxSpeedOverGround, settingVariable: pGPS.maxSpeedOverGround!))
            }
            return annoncesGPS
        case .route:
            guard let pRoute = self.profileTypes.first(where: { $0 is ProfileRoute}) as? ProfileRoute else {
                return []
            }
            var annoncesRoute = [AnnounceParameter]()
            if pRoute.azimut != nil {
                annoncesRoute.append(AnnounceParameter(name: NSLocalizedString("azimuth", bundle: bundle, comment: ""), annonceType: .azimut, settingVariable: pRoute.azimut!))
            }
            if pRoute.gisement != nil {
                annoncesRoute.append(AnnounceParameter(name: NSLocalizedString("relative_bearing", bundle: bundle, comment: ""), annonceType: .gisement, settingVariable: pRoute.gisement!))
            }
            if let gisementTime = pRoute.gisementTime {
                annoncesRoute.append(AnnounceParameter(name: NSLocalizedString("clock_bearing", bundle: bundle, comment: ""), annonceType: .gisementTime, settingVariable: gisementTime))
            }
            if pRoute.distance != nil {
                annoncesRoute.append(AnnounceParameter(name: NSLocalizedString("distance", bundle: bundle, comment: ""), annonceType: .distance, settingVariable: pRoute.distance!))
            }
            if pRoute.cmg != nil {
                annoncesRoute.append(AnnounceParameter(name: NSLocalizedString("CMG", bundle: bundle, comment: ""), annonceType: .CMG, settingVariable: pRoute.cmg!))
            }
            if pRoute.distanceToSegment != nil {
                annoncesRoute.append(AnnounceParameter(name: NSLocalizedString("gap_segment_route", bundle: bundle, comment: ""), annonceType: .gapSegment, settingVariable: pRoute.distanceToSegment!))
            }
            if pRoute.entry3Lengths != nil {
                annoncesRoute.append(AnnounceParameter(name: NSLocalizedString("3_lengths", bundle: bundle, comment: ""), annonceType: .entry3Lengths, settingVariable: pRoute.entry3Lengths!))
            }
            return annoncesRoute
        case .navigationCenter:
            guard let pCentral = self.profileTypes.first(where: { $0 is ProfileCentrale}) as? ProfileCentrale else {
                return []
            }
            var annoncesCentral = [AnnounceParameter]()
            if pCentral.depth != nil {
                annoncesCentral.append(AnnounceParameter(name: NSLocalizedString("depth", bundle: bundle, comment: ""), annonceType: .depth, settingVariable: pCentral.depth!))
            }
            if pCentral.headingMagnetic != nil {
                annoncesCentral.append(AnnounceParameter(name: NSLocalizedString("surface_heading", bundle: bundle, comment: ""), annonceType: .headingMagnetic, settingVariable: pCentral.headingMagnetic!))
            }
            if pCentral.speedThroughWater != nil {
                annoncesCentral.append(AnnounceParameter(name: NSLocalizedString("speed_through_water", bundle: bundle, comment: ""), annonceType: .speedThroughWater, settingVariable: pCentral.speedThroughWater!))
            }
            if pCentral.trueWindSpeed != nil {
                annoncesCentral.append(AnnounceParameter(name: NSLocalizedString("true_wind_speed", bundle: bundle, comment: ""), annonceType: .trueWindSpeed, settingVariable: pCentral.trueWindSpeed!))
            }
            if pCentral.trueWindAngle != nil {
                annoncesCentral.append(AnnounceParameter(name: NSLocalizedString("true_wind_angle", bundle: bundle, comment: ""), annonceType: .trueWindAngle, settingVariable: pCentral.trueWindAngle!))
            }
            if pCentral.apparentWindSpeed != nil {
                annoncesCentral.append(AnnounceParameter(name: NSLocalizedString("apparent_wind_speed", bundle: bundle, comment: ""), annonceType: .apparentWindSpeed, settingVariable: pCentral.apparentWindSpeed!))
            }
            if pCentral.apparentWindAngle != nil {
                annoncesCentral.append(AnnounceParameter(name: NSLocalizedString("apparent_wind_angle", bundle: bundle, comment: ""), annonceType: .apparentWindAngle, settingVariable: pCentral.apparentWindAngle!))
            }
            if pCentral.trueWindDirection != nil {
                annoncesCentral.append(AnnounceParameter(name: NSLocalizedString("wind_direction", bundle: bundle, comment: ""), annonceType: .windDirection, settingVariable: pCentral.trueWindDirection!))
            }
            if pCentral.waterTemperature != nil {
                annoncesCentral.append(AnnounceParameter(name: NSLocalizedString("water_temperature", bundle: bundle, comment: ""), annonceType: .waterTemperature, settingVariable: pCentral.waterTemperature!))
            }
            if pCentral.airTemperature != nil {
                annoncesCentral.append(AnnounceParameter(name: NSLocalizedString("air_temperature", bundle: bundle, comment: ""), annonceType: .airTemperature, settingVariable: pCentral.airTemperature!))
            }
            return annoncesCentral
        case .telltales:
            guard let pTelltales = self.profileTypes.first(where: { $0 is ProfileTelltale}) as? ProfileTelltale else {
                return []
            }
            return [AnnounceParameter(name: NSLocalizedString("angle", bundle: bundle, comment: ""), annonceType: .tellTaleAngle, settingVariable: pTelltales.angle),
                    AnnounceParameter(name: NSLocalizedString("mediane", bundle: bundle, comment: ""), annonceType: .tellTaleMediane, settingVariable: pTelltales.mediane),
                     AnnounceParameter(name: NSLocalizedString("average", bundle: bundle, comment: ""), annonceType: .tellTaleMediane, settingVariable: pTelltales.average),
                      AnnounceParameter(name: NSLocalizedString("standard_deviation", bundle: bundle, comment: ""), annonceType: .tellTaleMediane, settingVariable: pTelltales.standardDeviation),
                       AnnounceParameter(name: NSLocalizedString("variation_max", bundle: bundle, comment: ""), annonceType: .tellTaleMediane, settingVariable: pTelltales.variationMax),
                AnnounceParameter(name: NSLocalizedString("state", bundle: bundle, comment: ""), annonceType: .tellTaleState, settingVariable: pTelltales.state)]
        case .batteryLevel:
            guard let pVoice = self.profileTypes.first(where: { $0 is ProfileVarious}) as? ProfileVarious else {
                return []
            }
            return [
                AnnounceParameter(name: NSLocalizedString("battery_level", bundle: bundle, comment: ""), annonceType: .batteryLevel, settingVariable: pVoice.batteryLevel)
            ]
        case .carto:
            guard let pCarto = self.profileTypes.first(where: { $0 is ProfileCarto}) as? ProfileCarto else {
                return []
            }
            var annoncesCarto = [AnnounceParameter]()
            if let timeAnnounce = pCarto.timeAnnounce {
                annoncesCarto.append(AnnounceParameter(name: NSLocalizedString("time_announce", bundle: bundle, comment: ""), annonceType: .timeAnnounce, settingVariable: timeAnnounce))
            }
            if let detectionDistance = pCarto.detectionDistance {
                annoncesCarto.append(AnnounceParameter(name: NSLocalizedString("detection_distance", bundle: bundle, comment: ""), annonceType: .detectionDistance, settingVariable: detectionDistance))
            }
            if let detectionNumber = pCarto.detectionNumber {
                annoncesCarto.append(AnnounceParameter(name: NSLocalizedString("detection_number", bundle: bundle, comment: ""), annonceType: .detectionNumber, settingVariable: detectionNumber))
            }
            if let distance = pCarto.distance {
                annoncesCarto.append(AnnounceParameter(name: NSLocalizedString("distance", bundle: bundle, comment: ""), annonceType: .distanceBeacon, settingVariable: distance))
            }
            if let gisement = pCarto.gisement {
                annoncesCarto.append(AnnounceParameter(name: NSLocalizedString("relative_bearing", bundle: bundle, comment: ""), annonceType: .gisementBeacon, settingVariable: gisement))
            }
            if let gisementTime = pCarto.gisementTime {
                annoncesCarto.append(AnnounceParameter(name: NSLocalizedString("clock_bearing", bundle: bundle, comment: ""), annonceType: .gisementTimeBeacon, settingVariable: gisementTime))
            }
            if let azimut = pCarto.azimut {
                annoncesCarto.append(AnnounceParameter(name: NSLocalizedString("azimuth", bundle: bundle, comment: ""), annonceType: .azimutBeacon, settingVariable: azimut))
            }
            if let onlyFront = pCarto.onlyFront {
                annoncesCarto.append(AnnounceParameter(name: NSLocalizedString("only_front", bundle: bundle, comment: ""), annonceType: .onlyFront, settingVariable: onlyFront))
            }
            return annoncesCarto
        default:
            return []
        }
    }
}

public struct AnnounceParameter {
    let name: String
    let settingVariable: BehaviorRelay<ProfileSetting>
    let unit: NavigationUnit
    let step: Float
    let minValue: Float
    let maxValue: Float
    let shouldDisplayInt: Bool
    let valueDisabled: Bool
    let valueDisplayed: Bool
    var valueTitle: String = ""
    let timeDisplayed: Bool
    var shouldDisplaySwitch: Bool = true
    let bundle = Utils.bundle(anyClass: Profile.self)
        
    init(name: String, annonceType: Announce.AnnonceType, settingVariable: BehaviorRelay<ProfileSetting>) {
        self.valueTitle = NSLocalizedString("announce_threshold", bundle: bundle, comment: "")
        self.name = name
        self.settingVariable = settingVariable
        switch annonceType {
        case .compass, .courseOverGround, .azimut, .gisement, .headingMagnetic, .waterTemperature, .airTemperature:
            self.unit = NavigationUnit.degrees
            self.step = 1
            self.minValue = 1
            self.maxValue = 30
            self.shouldDisplayInt = true
            self.valueDisabled = false
            self.valueDisplayed = true
            self.timeDisplayed = true
        case .speedOverGround, .maxSpeedOverGround, .CMG, .speedThroughWater:
            self.unit = Settings.shared.speedUnit?.unit ?? .knots
            self.step = Settings.shared.speedUnit?.unit == .knots ? 0.1 : 1
            self.minValue = 0.1
            self.maxValue = 5
            self.shouldDisplayInt = false
            self.valueDisabled = false
            self.valueDisplayed = true
            self.timeDisplayed = true
        case .distance:
            self.unit = NavigationUnit.percent
            self.step = 1
            self.minValue = 1
            self.maxValue = 40
            self.shouldDisplayInt = true
            self.valueDisabled = false
            self.valueDisplayed = true
            self.timeDisplayed = true
        case .gapSegment:
//        case .distance, .gapSegment:
            self.unit = Settings.shared.distanceUnit?.unit ?? .meter
            self.step = Settings.shared.distanceUnit?.unit == .meter ? 1 : 0.1
            self.minValue = 0
            self.maxValue = 1
            self.shouldDisplayInt = false
            self.valueDisabled = true
            self.valueDisplayed = true
            self.timeDisplayed = true
        case .gisementTime:
            self.unit = .none
            self.step = 1
            self.minValue = 0
            self.maxValue = 1
            self.shouldDisplayInt = true
            self.valueDisabled = true
            self.valueDisplayed = false
            self.timeDisplayed = true
        case .depth:
            self.unit = NavigationUnit.meter
            self.step = 1
            self.minValue = 0
            self.maxValue = 1
            self.shouldDisplayInt = false
            self.valueDisabled = true
            self.valueDisplayed = true
            self.timeDisplayed = true
        case .trueWindSpeed, .apparentWindSpeed:
            self.unit = Settings.shared.speedUnit?.unit ?? .knots
            self.step = Settings.shared.speedUnit?.unit == .knots ? 1 : 1
            self.minValue = 1
            self.maxValue = 10
            self.shouldDisplayInt = true
            self.valueDisabled = false
            self.valueDisplayed = true
            self.timeDisplayed = true
        case .trueWindAngle, .apparentWindAngle, .windDirection:
            self.unit = NavigationUnit.degrees
            self.step = 1
            self.minValue = 1
            self.maxValue = 100
            self.shouldDisplayInt = true
            self.valueDisabled = false
            self.valueDisplayed = true
            self.timeDisplayed = true
        case .tellTaleAngle:
            self.unit = NavigationUnit.degrees
            self.step = 1
            self.minValue = 1
            self.maxValue = 15
            self.shouldDisplayInt = true
            self.valueDisabled = false
            self.valueDisplayed = true
            self.timeDisplayed = true
        case .tellTaleMediane:
            self.unit = NavigationUnit.degrees
            self.step = 1
            self.minValue = 1
            self.maxValue = 10
            self.shouldDisplayInt = true
            self.valueDisabled = false
            self.valueDisplayed = true
            self.timeDisplayed = true
        case .tellTaleState:
            self.unit = NavigationUnit.degrees
            self.step = 1
            self.minValue = 1
            self.maxValue = 10
            self.shouldDisplayInt = true
            self.valueDisabled = true
            self.valueDisplayed = false
            self.timeDisplayed = true
        case .batteryLevel:
            self.unit = NavigationUnit.percent
            self.step = 5
            self.minValue = 5
            self.maxValue = 20
            self.shouldDisplayInt = true
            self.valueDisabled = false
            self.valueDisplayed = true
            self.timeDisplayed = false
        case .entry3Lengths:
            self.unit = .meter
            self.step = 1
            self.minValue = 1
            self.maxValue = 100
            self.shouldDisplayInt = true
            self.valueDisabled = false
            self.valueDisplayed = true
            self.valueTitle = NSLocalizedString("boat_length", bundle: bundle, comment: "")
            self.timeDisplayed = false
        case .timeAnnounce:
            self.unit = .second
            self.step = 1
            self.minValue = 1
            self.maxValue = 60
            self.shouldDisplayInt = true
            self.valueDisabled = false
            self.valueDisplayed = true
            self.timeDisplayed = false
            self.shouldDisplaySwitch = false
        case .detectionDistance:
            self.unit = .meter
            self.step = 100
            self.minValue = 100
            self.maxValue = 4000
            self.shouldDisplayInt = true
            self.valueDisabled = false
            self.valueDisplayed = true
            self.timeDisplayed = false
        case .detectionNumber:
            self.unit = .none
            self.step = 1
            self.minValue = 1
            self.maxValue = 8
            self.shouldDisplayInt = true
            self.valueDisabled = false
            self.valueDisplayed = true
            self.timeDisplayed = false
        case .distanceBeacon:
            self.unit = Settings.shared.distanceUnit?.unit ?? .meter
            self.step = Settings.shared.distanceUnit?.unit == .meter ? 1 : 0.1
            self.minValue = 0
            self.maxValue = 1
            self.shouldDisplayInt = false
            self.valueDisabled = true
            self.valueDisplayed = true
            self.timeDisplayed = false
        case .azimutBeacon, .gisementBeacon:
            self.unit = NavigationUnit.degrees
            self.step = 10
            self.minValue = 10
            self.maxValue = 360
            self.shouldDisplayInt = true
            self.valueDisabled = false
            self.valueDisplayed = true
            self.timeDisplayed = false
        case .gisementTimeBeacon:
            self.unit = .timeDegrees
            self.step = 1
            self.minValue = 1
            self.maxValue = 12
            self.shouldDisplayInt = true
            self.valueDisabled = false
            self.valueDisplayed = true
            self.timeDisplayed = false
        case .onlyFront:
            self.unit = .none
            self.step = 1
            self.minValue = 0
            self.maxValue = 0
            self.shouldDisplayInt = true
            self.valueDisabled = true
            self.valueDisplayed = false
            self.timeDisplayed = false
        default:
            self.unit = NavigationUnit.degrees
            self.step = 5
            self.minValue = 1
            self.maxValue = 10
            self.shouldDisplayInt = false
            self.valueDisabled = false
            self.valueDisplayed = true
            self.timeDisplayed = true
        }
    }
}
