//
//  PlotPointsViewModel.swift
//  SARA Croisiere
//
//  Created by Rose on 14/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import CoreLocation
import RealmSwift

public class PlotPointsViewModel {
    
    public var location: CLLocation?
    
    func updateCoordinates(id: String, latitude: Float, longitude: Float) {
        let realm = try! Realm()
        let _ = realm.objects(CoursePoint.self).first(where: { $0.id == id })
        try! realm.write {
//            point?.latitude = latitude
//            point?.longitude = longitude
        }
    }
    
    func updateLocation(at indexPath: IndexPath, with location: CLLocation) {
        guard let point = CoursePoint.getPointAtIndex(indexPath) else {
            return
        }
        self.updateCoordinates(id: point.id, latitude: Float(location.coordinate.latitude), longitude: Float(location.coordinate.longitude))
    }
}
