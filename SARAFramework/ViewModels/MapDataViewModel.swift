//
//  MapDataViewModel.swift
//  SARAFramework
//
//  Created by Marine on 02.06.21.
//

import Foundation
import RxCocoa

class MapDataViewModel {
    var downloadedZones: BehaviorRelay<[DownloadedZone]>
    
    init() {
        downloadedZones = BehaviorRelay<[DownloadedZone]>(value: DownloadedZone.all())
    }
    
    func downloadBeacons(coordinates: (Float, Float), radius: Float) -> DownloadedZone? {
        return MapDataManager.shared.downloadBeacons(coordinates: coordinates, radius: radius)
//        guard let downloadedZone = MapDataManager.shared.downloadBeacons(coordinates: coordinates, radius: radius) else {
//            return
//        }
//        downloadedZones.add(element: downloadedZone)
    }
    
    func saveDownloadedBeacons(downloadedZone: DownloadedZone?) {
        guard downloadedZone != nil, downloadedZone!.beacons.count > 0 else { // if there is no beacons, don't save the download zone
            return
        }
        downloadedZone!.save()
        downloadedZones.add(element: downloadedZone!)
    }
    
    func deleteAll() {
        DownloadedZone.deleteAll()
        var newValue = downloadedZones.value
        newValue.removeAll()
        downloadedZones.accept(newValue)
    }
    
    func deleteZone(at indexPath: IndexPath) {
        var value = downloadedZones.value
        let zone = value[indexPath.row]
        zone.delete()
        value.remove(at: indexPath.row)
        downloadedZones.accept(value)
    }
}

extension BehaviorRelay where Element: RangeReplaceableCollection {

    func add(element: Element.Element) {
        var array = self.value
        array.append(element)
        self.accept(array)
    }
}
