//
//  LocationManager.swift
//  SARA Croisiere
//
//  Created by Rose on 14/08/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import Foundation
import CoreLocation
import RxSwift
import UTMConversion
import RxCocoa

public class LocationManager: NSObject {
    public static let shared = LocationManager()
    public let manager = CLLocationManager()
    let disposeBag = DisposeBag()
    
    public var locationVariable = BehaviorRelay<CLLocation?>(value: nil)
    public var compassVariable = BehaviorRelay<CLHeading?>(value: nil)
    public var accuracyVariable = BehaviorRelay<Int?>(value: nil)
    
    // Variables to compute average speed
    private var speedValuesCount: Int = 0
    private var speedValuesSum: Float = 0 {
        didSet {
            if speedValuesCount > 0 {
                averageSpeedVariable.accept((speedValuesSum / Float(speedValuesCount)).formatted)
            }
        }
    }
    
    // Variables to compute average course
    private var courseValuesCount: Int = 0
    private var courseValuesSum: Int = 0 {
        didSet {
            if courseValuesCount > 0 {
                averageCourseVariable.accept(courseValuesSum / courseValuesCount)
            }
        }
    }
    
    // Average variables
    public var averageSpeedVariable = BehaviorRelay<Float?>(value: nil)
    public var averageCourseVariable = BehaviorRelay<Int?>(value: nil)
    public var maxSpeedVariable = BehaviorRelay<Float?>(value: nil)
    public var maxSpeed10sVariable = BehaviorRelay<Float?>(value: nil)
    
    private var speedHistory = [(Date, Float)]()
    
    private func updateSpeedHistory(with speed: Float) {
        let now = Date()
        speedHistory.append((now, speed))
        if speedHistory.count == 1 {
            maxSpeed10sVariable.accept(speed)
            return
        }
        var index = speedHistory.count - 1
        var maxSpeed: Float = 0
        var within10s = true
        while index >= 0 && within10s {
            if speedHistory[index].0.timeIntervalSince(now) >= -10 {
                if maxSpeed < speedHistory[index].1 {
                    maxSpeed = speedHistory[index].1
                }
                index -= 1
            } else {
                within10s = false
            }
        }
        maxSpeed10sVariable.accept(maxSpeed)
        
        if !within10s {
            speedHistory.removeSubrange(0...index)
        }
    }
    
    public func load() {
        
        self.manager.requestAlwaysAuthorization()
        
        self.manager.delegate = self
        self.manager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        self.manager.allowsBackgroundLocationUpdates = true
        self.manager.startUpdatingLocation()
        self.manager.startUpdatingHeading()
        
        // Update average speed with conditions set in the settings
        self.locationVariable
            .asObservable()
            .map({ (location) -> Double? in
                guard let speed = location?.speed else {
                    return nil
                }
                guard speed > 0 else {
                    return nil
                }
                return speed
            })
            //TODO: REMOVE COMMENT
            .throttle(.seconds(Settings.shared.averageSpeedTime), scheduler: MainScheduler.instance)
            .subscribe(onNext: {[weak self] (speed) in
                if speed != nil {
                    var realSpeed = Float(speed!).msToKnots().formatted
                    if Settings.shared.speedUnit?.unit == .kmPerHour {
                        realSpeed = Float(speed!).msToKmh().formatted
                    }
                    // Store the number of values received, and their sum
                    // To avoid storing all values
                    self?.speedValuesCount += 1
                    self?.speedValuesSum += abs(realSpeed)
                }
            }).disposed(by: disposeBag)
        
        //Update max speed
        self.locationVariable
            .asObservable()
            .map({ (location) -> Double? in
                guard let speed = location?.speed else {
                    return nil
                }
                guard speed > 0 else {
                    return nil
                }
                return speed
            })
            .subscribe(onNext: {[weak self] (speed) in
                if speed != nil {
                    // Update max speed if needed
                    var realSpeed = Float(speed!).msToKnots().formatted
                    if Settings.shared.speedUnit?.unit == .kmPerHour {
                        realSpeed = Float(speed!).msToKmh().formatted
                    }
                    
                    if realSpeed > (self?.maxSpeedVariable.value ?? 0) {
                        self?.maxSpeedVariable.accept(realSpeed)
                        AnnouncementManager.shared.maxSpeedOverGroundVariable.accept(realSpeed)
                    }
                }
            }).disposed(by: disposeBag)
        
        //Update max speed in the last 10s
        self.locationVariable
            .asObservable()
            .map({ (location) -> Double? in
                guard let speed = location?.speed else {
                    return nil
                }
                guard speed > 0 else {
                    return nil
                }
                return speed
            })
            .subscribe(onNext: {[weak self] (speed) in
                if speed != nil {
                    // Update max speed if needed
                    var realSpeed = Float(speed!).msToKnots().formatted
                    if Settings.shared.speedUnit?.unit == .kmPerHour {
                        realSpeed = Float(speed!).msToKmh().formatted
                    }
                
                    self?.updateSpeedHistory(with: realSpeed)
                }
            }).disposed(by: disposeBag)
        
        // Compute and update average course over ground
        self.locationVariable
            .asObservable()
            .map({ (location) -> Double? in
                guard let course = location?.course else {
                    return nil
                }
                guard course > 0 else {
                    return nil
                }
                return course
            })
            //TODO REMOVE
            .throttle(.seconds(Settings.shared.averageHeadingTime), scheduler: MainScheduler.instance)
            .subscribe(onNext: {[weak self] (course) in
                if course != nil {
                    let intCourse = Int(course!)
                    
                    // Store the number of values received, and their sum
                    // To avoid storing all values
                    self?.courseValuesCount += 1
                    self?.courseValuesSum += abs(intCourse)
                    
                    //TODO: check if needed
//                    AnnouncementManager.shared.courseOverGroundVariable.accept(intHeading)
                }
            }).disposed(by: disposeBag)
    
    }
    
    public func resetAverages() {
        courseValuesSum = 0
        courseValuesCount = 0
        
        speedValuesSum = 0
        speedValuesCount = 0
        
        if let speed = locationVariable.value?.speed, speed >= 0 {
            maxSpeedVariable.accept(0)
            maxSpeed10sVariable.accept(0)
        } else {
            maxSpeedVariable.accept(nil)
            maxSpeed10sVariable.accept(nil)
        }
    }
    
    /// Returns directly the current position given by the manager
    ///
    /// - Returns: CLLocation?
    public func getCurrentPosition() -> CLLocation? {
        if Settings.shared.developerMode { // Developper mode
            return locationVariable.value
        }
        if !Settings.shared.usePhoneGPS { // Use the GPS of the central
            return locationVariable.value
        }
        return self.manager.location
    }
}

extension LocationManager: CLLocationManagerDelegate {
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        // We dont want to update the location if the user decided to use the navigation center GPS
        guard Settings.shared.usePhoneGPS else {
            return
        }

        DispatchQueue.main.async {
            if Settings.shared.developerMode { // In developper set the variable with the one set in developper mode. Like this the views are updated with the position
                self.locationVariable.accept(self.getCurrentPosition())
            } else {
                self.locationVariable.accept(locations.last)
                if let location = locations.last {
                    self.accuracyVariable.accept(Int(location.horizontalAccuracy))
                }
            }
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        
        DispatchQueue.main.async {
            self.compassVariable.accept(newHeading)
            AnnouncementManager.shared.compassVariable.accept(Int(newHeading.trueHeading))
        }
    }
}
