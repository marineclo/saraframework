Pod::Spec.new do |spec|
  spec.name         = 'SARAFramework'
  spec.summary      = 'SARAFramework'
  spec.homepage     = 'https://bitbucket.org/marineclo/sara_framework/'
  spec.version      = '0.1.0'
  spec.license      = { :type => 'MIT' }
  spec.authors      = { 'Marine Clogenson' => 'marine.clogenson@graniteapps.ch' }
  spec.source = { :path => "/Users/marine/Documents/SARA/iOS/SARAFramework" }
#  spec.source       = { :git => 'https://github.com/KeepSafe/iOS.git', :tag => "MyAmazingFramework_v#{spec.version}" }
  spec.source_files = "SARAFramework/**/*.{swift}", "SARAFramework/**/**/*.{swift}"
  spec.ios.deployment_target = '12.0'
  spec.swift_version = '5.0'

  spec.resource_bundles = {"SARAFrameworkRessources" => ["SARAFramework/Resources/*.{xcassets,storyboard,xcstrings,wav,png,jpeg}", "SARAFramework/Resources/*.lproj/*.{strings,xcstrings,storyboard}"]}

  #spec.resources  = "SARAFramework/**/Framework.xcassets/*", "SARAFramework/**/*.xib", "SARAFramework/**/*.storyboard"
  spec.resources  = "SARAFramework/**/*.{xib,storyboard}"

  spec.requires_arc = true
  
  spec.dependency "Firebase/Auth"
  spec.dependency "Firebase/Database"

  spec.dependency "RealmSwift"
  spec.dependency "RxSwift"
  spec.dependency "RxCocoa"
  spec.dependency "RxDataSources"
  spec.dependency "RxOptional"

  spec.dependency "UTMConversion"
  spec.dependency "CocoaAsyncSocket"
  spec.dependency "ReachabilitySwift"
end